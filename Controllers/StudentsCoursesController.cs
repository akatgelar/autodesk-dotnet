using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using autodesk.Code.Models;

// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/StudentsCourses")]
    public class StudentsCoursesController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private readonly MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        private readonly Audit auditLog;
        private readonly IDataServices dataServices;
        private readonly MySQLContext _mySQLContext;
        public StudentsCoursesController(MySqlDb sqlDb, MySQLContext mySQLContext)
        {
            oDb = sqlDb;
            auditLog = new Audit(oDb);
            _mySQLContext = mySQLContext;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from CourseStudent ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch (Exception e)
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }



        // [HttpGet("{id}")]
        // public IActionResult WhereId(int id)
        // { 
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from StudentsCourses where StudentsCourses_id = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // } 


        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {

            var o1 = JObject.Parse(obj);
            string query = "";
            string userID = String.Empty;
            query += "select * from CourseStudent ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("CourseId"))
                {
                    query += "CourseId = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("StudentID"))
                {
                    query += "StudentID = '" + property.Value.ToString() + "' ";
                    userID = property.Value.ToString();
                }

                i = i + 1;
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                //ds = dataServices.EscapeSpecialChar(ds);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                if (ds.Tables.Count > 0)
                {

                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());

                    result = JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented,
                        settings);
                }
                else
                {
                    result = "Not found";
                }
                //result = MySqlDb.GetJSONArrayString(ds.Tables[0]);


            }
            return Ok(result);
        }

        [HttpGet("allcourse/{studentid}")]
        public async Task<IActionResult> WhereAllCourse(string studentid)
        {

            JArray arr1;
            try
            {
                string query = "";
                if (studentid == "admin")
                {
                    query =
                        "select GROUP_CONCAT(CourseStudent.StudentID) as StudentID, CourseStudent.SurveyTaken, CourseStudent.`Status`, Courses.TrainingTypeKey, Courses.ProjectTypeKey, Courses.EventTypeKey, Courses.FYIndicatorKey, " +
                        "Courses.CourseId, Courses.CompletionDate, DATE_FORMAT(Courses.StartDate,'%d-%M-%Y') AS StartDate, Courses.`Name`, Contacts_All.ContactId, Contacts_All.ContactName, " +
                        "Main_site.SiteName, Main_site.SiteAddress1, Courses.EvaluationQuestionId, Courses.CertificateId " +
                        "from CourseStudent join Courses on CourseStudent.CourseId = Courses.CourseId " +
                        "join Contacts_All on Courses.ContactId = Contacts_All.ContactId " +
                        // "join Main_site on Contacts_All.PrimarySiteId = Main_site.SiteId "+ /* ubah join dari zwe */
                        "join Main_site on Courses.SiteId = Main_site.SiteId " +
                        "GROUP BY Courses.CourseId ORDER BY Courses.StartDate DESC";
                }
                else
                {
                    query =
                        "SELECT cs.StudentID, cs.SurveyTaken, cs.`Status`, c.TrainingTypeKey, c.ProjectTypeKey, c.EventTypeKey, c.FYIndicatorKey," +
                        "c.CourseId, c.PartnerType, c.CompletionDate, DATE_FORMAT(c.StartDate,'%d-%M-%Y') AS StartDate, c.`Name`,c.EnrollmentExpireDate, ca.ContactId, ca.InstructorId,ca.ContactName," +
                        "s.SiteName, s.SiteAddress1, c.EvaluationQuestionId, c.CertificateId " +
                        ", c.PartnerType " + /* tambah partner type */
                        "FROM CourseStudent cs " +
                        "INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                        "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                        // "INNER JOIN SiteContactLinks scl ON ca.ContactId = scl.ContactId " +
                        // "INNER JOIN Main_site s ON scl.SiteId = s.SiteId " +
                        "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                        "WHERE  cs.StudentID = " + studentid + " GROUP BY c.CourseId ORDER BY c.StartDate DESC ";
                }

                Console.WriteLine(query);

                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = await Task.Run(()=> oDb.getDataSetFromSP(sqlCommand));

                // if (ds.Tables[0].Rows.Count != 0)
                // {
                //     //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                //     ds = MainOrganizationController.EscapeSpecialChar(ds);
                // }
                if (ds.Tables[0].Rows.Count != 0)
                {
                    // ds = dataServices.EscapeSpecialChar(ds);
                    //// result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                    // result= JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented);
                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());

                    result = JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented,
                        settings);
                    arr1 = JArray.Parse(result);
                    DateTime now = DateTime.Now;
                    for (int i = 0; i < arr1.Count; i++)
                    {
                        string ExpireDates = arr1[i]["EnrollmentExpireDate"].ToString();
                        DateTime datedb = DateTime.Parse(arr1[i]["StartDate"].ToString());
                        //DateTime nextMonth = DateTime.Parse(arr1[i]["EnrollmentExpireDate"].ToString()).AddDays(1).AddMonths(2).AddDays(-1);
                        DateTime nextMonth;
                        if (string.IsNullOrEmpty(ExpireDates))
                        {
                            nextMonth = now;
                        }
                        else
                        {
                            nextMonth = DateTime.Parse(arr1[i]["EnrollmentExpireDate"].ToString()).AddDays(1).AddMonths(2).AddDays(-1);
                        }
                        if (arr1[i]["PartnerType"].ToString() == "AAP Event")
                        {

                            arr1[i]["activesurveylink"] = "null";
                        }
                        else
                        {
                            if (datedb > now && arr1[i]["SurveyTaken"].ToString() == "0")
                            {
                                arr1[i]["activesurveylink"] = "Not Started";

                            }

                            else if (now > nextMonth && arr1[i]["SurveyTaken"].ToString() == "0")
                            {
                                arr1[i]["activesurveylink"] = "Expired";
                            }
                            else
                            {
                                arr1[i]["activesurveylink"] = "Active";
                            }
                        }
                    }
                }
                else
                {
                    arr1 = null;
                    return Ok(JsonConvert.DeserializeObject("[]"));
                }


            }
            catch
            {
                arr1 = null;
                return Ok(JsonConvert.DeserializeObject("[]"));
            }
            finally
            {
            }

            return Ok(arr1);
        }

        [HttpGet("mycourse/{studentid}")]
        public async Task<IActionResult> WhereMyCourse(string studentid)
        {

            JArray arr1;
            var id = int.Parse(studentid);
            try
            {
                var studentCourse = await Task.Run(()=> (from cs in _mySQLContext.CourseStudent
                                     join c in _mySQLContext.Courses on cs.CourseId equals c.CourseId
                                     join ca in _mySQLContext.ContactsAll on c.ContactId equals ca.ContactId
                                     join s in _mySQLContext.MainSite on c.SiteId equals s.SiteId
                                    
                                     select new
                                     {
                                         cs.StudentId,
                                         cs.SurveyTaken,
                                         cs.CourseTaken,
                                         c.CourseId,
                                         c.Name,
                                         s.SiteName,
                                         s.SiteAddress1,
                                         c.TrainingTypeKey,
                                         c.ProjectTypeKey,
                                         c.EventTypeKey,
                                         StartDate = c.StartDate,
                                         CompletionDate = c.CompletionDate,

                                         ca.ContactId,
                                         ca.ContactName,
                                         c.FyindicatorKey
                                     }

                                     ).Where(cs =>((cs.CourseTaken.HasValue && cs.CourseTaken.Value.ToString() == "0") || (cs.SurveyTaken.HasValue && cs.SurveyTaken.Value.ToString() == "0")) && cs.StudentId == id).ToList());
                result = JsonConvert.SerializeObject(studentCourse);
                arr1 = JArray.Parse(result);
                if (arr1.Count>0)
                {
                    DateTime now = DateTime.Now;
                    for (int i = 0; i < arr1.Count; i++)
                    {
                        DateTime datedb = DateTime.Parse(arr1[i]["CompletionDate"].ToString());
                        DateTime nextMonth = datedb.AddDays(1).AddMonths(2).AddDays(-1);
                        if (datedb < now && now < nextMonth)
                        {
                            arr1[i]["activesurveylink"] = true;
                        }
                        else
                        {
                            arr1[i]["activesurveylink"] = false;
                        }
                    }
                    return Ok(arr1);
                }
                else
                {
                    return Ok(JsonConvert.DeserializeObject("[]"));
                }



                //sb = new StringBuilder();
                //sb.AppendFormat("SELECT cs.StudentID,cs.SurveyTaken,cs.CourseTaken,c.CourseId,c.`NAME`,s.SiteName,s.SiteAddress1,c.TrainingTypeKey,c.ProjectTypeKey,c.EventTypeKey," +
                //    "DATE_FORMAT( c.StartDate, '%Y-%m-%d' ) AS StartDate,DATE_FORMAT( c.CompletionDate, '%Y-%m-%d' ) AS CompletionDate,cc.ContactId,cc.ContactName,c.FYIndicatorKey " +
                //    "FROM Courses c,Contacts_All cc,Main_site s,CourseStudent cs WHERE c.ContactId = cc.ContactId AND c.SiteId = s.SiteId AND c.CourseId = cs.CourseId AND cs.StudentID = '" + studentid + "' AND ( cs.CourseTaken = '0' OR cs.SurveyTaken = '0' ) GROUP BY c.CourseId");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);
                //if (ds.Tables[0].Rows.Count != 0)
                //{
                //    var settings = new JsonSerializerSettings
                //    {

                //    };
                //    settings.Converters.Add(new DataSetConverter());

                //    result = JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented,
                //        settings);
                //    //ds = dataServices.EscapeSpecialChar(ds);
                //    //result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                //    arr1 = JArray.Parse(result);
                //    DateTime now = DateTime.Now;
                //    for (int i = 0; i < arr1.Count; i++)
                //    {
                //        DateTime datedb = DateTime.Parse(arr1[i]["CompletionDate"].ToString());
                //        DateTime nextMonth = datedb.AddDays(1).AddMonths(2).AddDays(-1);
                //        if (datedb < now && now < nextMonth)
                //        {
                //            arr1[i]["activesurveylink"] = true;
                //        }
                //        else
                //        {
                //            arr1[i]["activesurveylink"] = false;
                //        }
                //    }
                //}
                //else
                //{
                //    arr1 = null;
                //    return Ok(null);
                //}



            }
            catch(Exception e)
            {
                arr1 = null;
                return Ok(JsonConvert.DeserializeObject("[]"));
            }
           
        }

        [HttpGet("nextcourse/{studentid}")]
        public async Task<IActionResult> WhereNextCourse(string studentid)
        {
            var id = studentid.Replace("{", string.Empty).Replace("}", string.Empty);
            JArray arr1;
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("SELECT cs.StudentID, cs.SurveyTaken, cs.`Status`, c.CourseId, DATE_FORMAT( c.CompletionDate, '%d-%M-%Y' ) as CompletionDate, DATE_FORMAT( c.StartDate, '%d-%M-%Y' ) as StartDate, c.`Name`, ca.ContactId, ca.ContactName, s.SiteName, s.SiteAddress1, c.EvaluationQuestionId, c.CertificateId " +
                    "FROM CourseStudent cs INNER JOIN Courses c ON cs.CourseId = c.CourseId INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    // "WHERE cs.StudentID = '"+studentid+"' AND c.CompletionDate >= NOW( ) AND cs.SurveyTaken = '0' AND cs.CourseTaken = '0' "+
                    "WHERE cs.StudentID = " + id + " AND c.StartDate > NOW() AND cs.SurveyTaken = '0' AND cs.CourseTaken = '0' " +
                    "GROUP BY c.CourseId ORDER BY c.StartDate DESC");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = await Task.Run(()=> oDb.getDataSetFromSP(sqlCommand));
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count != 0)
                {
                    //ds = dataServices.EscapeSpecialChar(ds);
                    //result = MySqlDb.GetJSONArrayString(ds.Tables[0]);

                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());

                    result = JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented,
                        settings);
                    arr1 = JArray.Parse(result);
                    DateTime now = DateTime.Now;
                    for (int i = 0; i < arr1.Count; i++)
                    {
                        DateTime datedb = DateTime.Parse(arr1[i]["CompletionDate"].ToString());
                        TimeSpan due = datedb - now;
                        if (due.Days > 0)
                        {
                            arr1[i]["Due"] = due.Days;
                        }
                        else
                        {
                            arr1[i]["Due"] = "";
                        }
                    }

                }
                else
                {
                    arr1 = null;

                    return Ok(JsonConvert.DeserializeObject("[]"));

                }




            }
            catch (Exception e)
            {
                arr1 = null;

                return Ok(JsonConvert.DeserializeObject("[]"));
            }
            finally
            {

            }

            return Ok(arr1);
        }

        [HttpGet("CheckCourse/{course_id}")]
        public IActionResult SiteCourse(string course_id)
        {

            string query = "";
            JObject resultfinal;

            query += "SELECT SiteId,CourseId,CourseTitle,c.ContactId,ContactName,StartDate,CompletionDate,EndDate, CONCAT(CourseId,' | ',ContactName) AS Keyword,EmailAddress,EmailAddress2 FROM  " +
                "(SELECT CourseId,`Name` AS CourseTitle,SiteId,ContactId,DATE_FORMAT(StartDate,'%d/%m/%Y') AS StartDate,DATE_FORMAT(CompletionDate,'%d/%m/%Y') AS CompletionDate,DATE_FORMAT(CompletionDate,'%Y-%m-%d') AS EndDate FROM Courses WHERE CourseId LIKE '%" + course_id + "%' LIMIT 100) AS c " +
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId";

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());

                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());

                    result = "{" +
                             "\"results\":" + JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented,
                                 settings) + "" +
                             "}";
                }

                //result =
                //   "{" +
                //   "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                //   "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }
            return Ok(resultfinal);
        }


        [HttpGet("historycourse/{studentid}")]
        public IActionResult WhereHistoryCourse(string studentid)
        {


            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select CourseStudent.StudentID, CourseStudent.SurveyTaken, CourseStudent.CourseTaken, Courses.FYIndicatorKey,Courses.TrainingTypeKey,Courses.ProjectTypeKey,Courses.EventTypeKey, " +
                    "Courses.CourseId, Courses.PartnerType, DATE_FORMAT(Courses.CompletionDate,'%d-%M-%Y') AS CompletionDate, Courses.`Name`, Contacts_All.ContactId, Contacts_All.InstructorId,Contacts_All.ContactName, " +
                    "Main_site.SiteName, Main_site.SiteAddress1, Courses.EvaluationQuestionId, Courses.CertificateId " +
                    "from CourseStudent join Courses on CourseStudent.CourseId = Courses.CourseId " +
                    "join Contacts_All on Courses.ContactId = Contacts_All.ContactId " +
                    // "join SiteContactLinks sc on Contacts_All.ContactId = sc.ContactId "+
                    // "join Main_site on sc.SiteId = Main_site.SiteId "+
                    "join Main_site on Courses.SiteId = Main_site.SiteId " +
                    "where StudentID = '" + studentid + "' AND case when Courses.PartnerType <> 'AAP Event' then CourseStudent.CourseTaken = '1' else NOW() >= Courses.CompletionDate end AND case when Courses.PartnerType <> 'AAP Event' then CourseStudent.SurveyTaken = '1' else NOW() >= Courses.CompletionDate end GROUP BY Courses.CourseId");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());

                    result = JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented,
                        settings);
                }
                else
                {
                    result = "[]"; /* add condition when data not found */
                }

            }
            catch
            {
                result = "Not found";
                return Ok(JsonConvert.DeserializeObject("[]"));
            }
            finally
            {
            }

            return Ok(JsonConvert.DeserializeObject(result));
        }

        [HttpGet("EventHandle/{student_id}")]
        public IActionResult CourseEventHandling(int student_id)
        {

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("SELECT cs.CourseId,DATE_FORMAT(CompletionDate,'%Y-%m-%d') AS end_date FROM CourseStudent cs INNER JOIN Courses c ON cs.CourseId = c.CourseId WHERE StudentID = " + student_id + " AND c.EventTypeKey IS NOT NULL AND (CourseTaken = 0 AND SurveyTaken = 0)");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables.Count > 0)
                {
                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());

                    result = JsonConvert.SerializeObject(ds.Tables[0], Formatting.Indented,
                        settings);
                }

            }
            catch
            {
                result = "Not found";
            }
            finally
            {
            }

            return Ok(result);
        }

        [HttpPost("EventHandle_2")]
        public IActionResult EventHandle_2([FromBody] dynamic data)
        {


            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "UPDATE CourseStudent SET CourseTaken = 1, SurveyTaken = 1 " +
                    "WHERE CourseId = '" + data.course_id + "' AND StudentID = " + data.student_id + ""
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Update Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            res =
                "{" +
                "\"code\":\"1\"," +
                "\"message\":\"Update Data Success\"" +
                "}";

            return Ok(res);

        }

        // [HttpPost]
        // public IActionResult Insert([FromBody] dynamic data)
        // {    

        //     int count1 = 0;
        //     int count2 = 0;
        //     JObject o1;
        //     JObject o2;
        //     // JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Insert Data Failed\""+
        //         "}";  

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from StudentsCourses;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o1 = JObject.Parse(result); 
        //     count1 = Int32.Parse(o1["count"].ToString());
        //     Console.WriteLine(count1); 

        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "insert into StudentsCourses "+
        //             "( StudentsCourses_category, StudentsCourses_name, description, `unique`, cuid, cdate, muid, mdate, status) "+
        //             "values ( '"+data.StudentsCourses_category+"','"+data.StudentsCourses_name+"','"+data.description+"','"+data.unique+"','"+data.cuid+"','"+data.cdate+"','"+data.muid+"','"+data.mdate+"','"+data.status+"');"
        //         );

        //     Console.WriteLine(sb.ToString());
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Insert Data Failed\""+
        //             "}";  
        //     }
        //     finally
        //     { 
        //     }

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from StudentsCourses;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);   

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o2 = JObject.Parse(result); 
        //     count2 = Int32.Parse(o2["count"].ToString());
        //     Console.WriteLine(count2);

        //     if(count2 > count1)
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Insert Data Success\""+
        //             "}";  
        //     } 

        //     return Ok(res);  

        // }

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, [FromBody] dynamic data)
        // {  
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Update Data Failed\""+
        //         "}";  

        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update StudentsCourses set "+ 
        //             "StudentsCourses_category='"+data.StudentsCourses_category+"', "+
        //             "StudentsCourses_name='"+data.StudentsCourses_name+"', "+
        //             "description='"+data.description+"', "+
        //             "`unique`='"+data.unique+"', "+
        //             "cuid='"+data.cuid+"', "+
        //             "cdate='"+data.cdate+"', "+
        //             "muid='"+data.muid+"', "+
        //             "mdate='"+data.mdate+"', "+ 
        //             "status='"+data.status+"' "+
        //             "where StudentsCourses_id='"+id+"'"
        //         ); 
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Update Data Failed\""+
        //             "}";  
        //     }
        //     finally
        //     {  
        //     }

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select * from StudentsCourses where StudentsCourses_id ="+id+";");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o3 = JObject.Parse(result);  

        //     if(o3["StudentsCourses_name"].ToString() == data.StudentsCourses_name.ToString())
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Update Data Success\""+
        //             "}"; 
        //     } 

        //     return Ok(res);  
        // } 

        // [HttpPut("status/{id}")]
        // public IActionResult UpdateSatus(int id)
        // {  
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  

        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update StudentsCourses set "+ 
        //             "status='Deleted' "+
        //             "where StudentsCourses_id='"+id+"'"
        //         ); 
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Delete Data Failed\""+
        //             "}";  
        //     }
        //     finally
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Delete Data Success\""+
        //             "}";  
        //     }

        //     return Ok(res);  
        // } 


        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // { 
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("delete from StudentsCourses where StudentsCourses_id = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Delete Data Failed\""+
        //             "}"; 
        //     }
        //     finally
        //     { 
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from StudentsCourses where StudentsCourses_id  ="+id+";");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);

        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //         o3 = JObject.Parse(result);  
        //         Console.WriteLine(o3.Count);
        //         if(o3.Count < 1){ 
        //             res =
        //                 "{"+
        //                 "\"code\":\"1\","+
        //                 "\"message\":\"Delete Data Success\""+
        //                 "}"; 
        //         }
        //     }

        //     return Ok(res);
        // } 

    }
}