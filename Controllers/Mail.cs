using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using System.Security.Cryptography;
using Newtonsoft.Json;
using autodesk.Code.Models;
// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/Mail")]
    public class Mail : Controller
    {
        private readonly IEmailSender _emailSender;
        private readonly IEmailService _emailService;

        public Mail(IEmailSender emailSender, IEmailService emailService)
        {
            _emailSender = emailSender;
            _emailService = emailService;
        }

        [HttpPost()]
        public async Task<IActionResult> Index([FromBody] dynamic data)
        {
            // if (ModelState.IsValid)
            // {
            Console.WriteLine(data.MailTo);
            Console.WriteLine(data.Subject);
            Console.WriteLine(data.Message);

            string mail = data.MailTo;
            string subject = data.Subject;
            string message = data.Message;

            // await _emailSender.SendEmailAsync(mail, subject, message);

            Boolean res = await _emailService.SendMailAsync(mail, subject, message);
            // Console.WriteLine(await _emailSender.SendEmailAsync(mail, subject, message));

            // Console.WriteLine("message");
            // return Ok("message");
            if (res == true)
            {
                return Ok("{\"code\":\"1\",\"msg\":\"Message sent\"}");
            }
            else
            {
                return Ok("{\"code\":\"0\",\"msg\":\"Message not sent\"}");
            }


            // Console.WriteLine();
            // await _emailSender.SendEmailAsync();

            // ViewBag.Succes = true;
            // }

        }

        [HttpPost]
        [Route("TestSend")]
        public async Task<IActionResult> TestSendMail([FromBody] dynamic data)
        {
            string email = "cuongnx88@gmail.com";
            using (var _mySQLContext = new MySQLContext())
            {

                var emailBodyType = _mySQLContext.EmailBodyType.FirstOrDefault(x => x.EmailBodyTypeName == "Email Certificate");
                if (emailBodyType != null)
                {
                    var emailBody = _mySQLContext.EmailBody.FirstOrDefault(x => x.BodyType == emailBodyType.EmailBodyTypeId.ToString());
                    if (emailBody != null)
                    {
                        string subject = emailBody.Subject;
                        string body = emailBody.BodyEmail;
                        var studentFullName = _mySQLContext.TblStudents.FirstOrDefault(x => x.Email == email);
                        if (studentFullName != null)
                        {
                            body = body.Replace("[FullName]", studentFullName.Firstname + " " + studentFullName.Lastname);
                        }
                        else
                        {
                            body = body.Replace("[FullName]", email);
                        }

                        //await _emailService.SendMailAsync(email, subject, body, null, listAttachment);
                        bool isSent = await _emailService.SendMailAsync(email, subject, body);
                        return Ok(isSent);
                    }
                }
            }
            return Ok();


        }

    }
}