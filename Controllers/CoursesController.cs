
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;
using Microsoft.AspNetCore.Http;
using System.IO;
using System;
using System.Text.RegularExpressions;
using autodesk.Code.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using autodesk.Code.AuditLogServices;

namespace autodesk.Controllers
{
    [Route("api/Courses")]
    public class CoursesController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        string mainURL = AppConfig.Config["Auth:mainURL"];
        JObject cert;
        private IDataServices dataService;
        private MySQLContext db;

        private readonly IEmailService _emailSender;

        public Audit _audit;//= new Audit();
        private readonly IAuditLogServices _auditLogServices;
        public CoursesController(IEmailService emailSender,MySqlDb sqlDb,MySQLContext _db, IAuditLogServices auditLogServices)
        {
          _emailSender = emailSender;
            oDb = sqlDb;
            db = _db;
            dataService=new DataServices(oDb,db);
            _audit =new Audit(oDb);
            _auditLogServices = auditLogServices;

        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT CourseId, c.NAME, s.SiteName, c.TrainingTypeKey, c.ProjectTypeKey, c.EventTypeKey, DATE_FORMAT( c.StartDate, '%Y-%m-%d' ) AS StartDate, DATE_FORMAT( c.CompletionDate, '%Y-%m-%d' ) AS CompletionDate, cc.ContactName, cc.InstructorId "+
                    "FROM Courses c, Contacts_All cc, Main_site s WHERE c.ContactId = cc.ContactId AND c.SiteId = s.SiteId AND "+
                    "(( c.TrainingTypeKey IS NOT NULL AND c.TrainingTypeKey <> '' ) OR ( c.ProjectTypeKey IS NOT NULL AND c.ProjectTypeKey <> '' ) OR ( c.EventTypeKey IS NOT NULL AND c.EventTypeKey <> '' ) OR ( ATCComputer IS NOT NULL AND ATCFacility IS NOT NULL )) "+
                    "GROUP BY c.CourseId");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        //Ganti logic untuk delete course 01/08/2018
        [HttpGet("isSurveyTaken/{course_id}")]
        public IActionResult isSurveyTaken(string course_id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT StudentID FROM EvaluationAnswer WHERE CourseId = '" + course_id + "'");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("TrainingHour/{id}")]
        public IActionResult TrainingHour(string id)
        {
            string query =
                "SELECT DISTINCT HoursTraining FROM Courses where HoursTraining <> '' ";

            try
            {
                if(id == "1"){
                    query += "AND TrainingTypeKey = '1' ";
                }
                else if(id == "2"){
                    query += "AND ProjectTypeKey = '1' ";
                }
                else{
                    query += "AND EventTypeKey = '1' ";
                }
                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("ATC_Course")]
        public IActionResult SelectATC()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT c.CourseId, c.NAME, s.SiteName, c.TrainingTypeKey, c.ProjectTypeKey, c.EventTypeKey, DATE_FORMAT( c.StartDate, '%Y-%m-%d' ) AS StartDate, DATE_FORMAT( c.CompletionDate, '%Y-%m-%d' ) AS CompletionDate, cc.ContactName, cc.InstructorId "+
                    "FROM Courses c, Contacts_All cc, Main_site s WHERE TrainingTypeKey = '' AND ProjectTypeKey = '' AND EventTypeKey = '' AND c.ContactId = cc.ContactId AND c.SiteId = s.SiteId GROUP BY c.CourseId");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Courses where CourseId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("DetailCourseForEnroll/{id}/")]
        public IActionResult DetailCourseForEnroll(string id)
        { 
            try
            {  
                sb = new StringBuilder();

                // sb.AppendFormat(
                //     "SELECT c.CourseId, c.`Name`, ca.ContactName, DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate, DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate, p.productName, pv.Version, c.HoursTraining, case when ctl.`Update` = '1' then 'Update' else '' end as `Update`, case when ctl.Essentials = '1' then 'Essentials' else '' end as Essentials, case when ctl.Intermediate = '1' then 'Intermediate' else '' end as Intermediate, case when ctl.Advanced = '1' then 'Advanced' else '' end as Advanced, case when ctl.Customized = '1' then 'Customized' else '' end as Customized, case when ctl.Other = '1' then concat('Other : ', ctl.`Comment`) else '' end as ctl_Other from "+
                //     // "(select CourseId, `Name`, StartDate, CompletionDate, HoursTraining, ContactId from Courses where CourseId = '"+id+"') c "+
                //     "(select CourseId, `Name`, StartDate, CompletionDate, HoursTraining, ContactId from Courses where CASE WHEN PartnerType != 'AAP Event' THEN CourseId = '"+id+"' and CompletionDate + INTERVAL 30 DAY >= NOW() ELSE CourseId = '"+id+"' and CompletionDate >= NOW() END) c "+
                //     "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId "+
                //     "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId "+
                //     "LEFT JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
                //     "LEFT JOIN Products p ON cs.productId = p.productId "+
                //     "LEFT JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId GROUP BY c.CourseId"
                //     );

                /* add message (expired) */
                
                    sb.AppendFormat(
                        "SELECT c.CourseId, c.`Name`, ca.ContactName, DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate, DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate, p.productName, pv.Version, c.HoursTraining, case when ctl.`Update` = '1' then 'Update' else '' end as `Update`, case when ctl.Essentials = '1' then 'Essentials' else '' end as Essentials, case when ctl.Intermediate = '1' then 'Intermediate' else '' end as Intermediate, case when ctl.Advanced = '1' then 'Advanced' else '' end as Advanced, case when ctl.Customized = '1' then 'Customized' else '' end as Customized, case when ctl.Other = '1' then concat('Other : ', ctl.`Comment`) else '' end as ctl_Other " +
                        ", Message " +
                        "from (" +
                        "select CourseId, `Name`, StartDate, CompletionDate, HoursTraining, ContactId,PartnerType, CASE WHEN DATE(EnrollmentExpireDate) < DATE(NOW()) THEN 'Expired' ELSE 'NotExpired' END AS Message from Courses where CourseId = '" +
                        id + "'" + /* AAP Expired 1 day after completion */
                        ") c INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId LEFT JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId LEFT JOIN Products p ON cs.productId = p.productId LEFT JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId GROUP BY c.CourseId"
                    );

                
                /* add message (expired) */

                    // Console.WriteLine(sb.ToString());

                    sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("StudentEvaluation/{id}")]
        public IActionResult GetStudentEvaluationData(string id)
        {
            try
            {
                // "SELECT ea.CourseId,s.StudentID,s.LanguageID,c.FYIndicatorKey,c.TrainingTypeKey,c.ProjectTypeKey,c.EventTypeKey,ea.EvaluationAnswerId,CONCAT(s.FirstName,' ',s.LastName) AS StudentName,s.Email,DATE_FORMAT(c.StartDate,'%Y-%m-%d') AS StartDate,DATE_FORMAT(c.CompletionDate,'%Y-%m-%d') AS CompletionDate," +
                // "cs.productId,cs.ProductVersionsId,cs.productsecondaryId,cs.ProductVersionsSecondaryId,c.LocationName,c.Institution FROM EvaluationAnswer ea " +
                // "INNER JOIN CourseStudent cst ON ea.StudentId = cst.StudentID " +
                // "INNER JOIN tblStudents s ON cst.StudentID = s.StudentID " +
                // "INNER JOIN Courses c ON cst.CourseId = c.CourseId " +
                // "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId " +
                // "WHERE cst.SurveyTaken = 1 AND ea.CourseId = '" + id + "' GROUP BY ea.CourseId ORDER BY ea.EvaluationAnswerId ASC"
                
                //year dan language id sementara di hardcode karena di tabel student ada yang ga punya language id dan di tabel sertifikat cuma ada tahun 2019
                sb = new StringBuilder();
                // sb.AppendFormat(
                //     "SELECT StudentEvaluationID,EvaluationAnswerId,ts.CourseId,StudentId,StudentName,Email,'35' AS LanguageID,'2018' AS FYIndicatorKey,TrainingTypeKey,ProjectTypeKey,EventTypeKey,StartDate,CompletionDate,LocationName,Institution,productId,ProductVersionsId,productsecondaryId,ProductVersionsSecondaryId,PartnerType FROM " +
                //     "(SELECT StudentEvaluationID,c.StudentId,CONCAT( FirstName, ' ', LastName ) AS StudentName,Email,LanguageID,CourseId,EvaluationAnswerId,FYIndicatorKey,TrainingTypeKey,ProjectTypeKey,EventTypeKey,StartDate,CompletionDate,LocationName,Institution,PartnerType FROM " +
                //     "(SELECT EvaluationAnswerId,StudentEvaluationID,ea.CourseId,StudentId,FYIndicatorKey,TrainingTypeKey,ProjectTypeKey,EventTypeKey,DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate,DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate,LocationName,Institution,PartnerType FROM " +
                //     "(SELECT EvaluationAnswerId,CourseId,StudentId,StudentEvaluationID FROM EvaluationAnswer WHERE CourseId = '" + id + "') AS ea " +
                //     "INNER JOIN Courses c ON ea.CourseId = c.CourseId) AS c " +
                //     "INNER JOIN tblStudents s ON c.StudentId = s.StudentID) AS ts " +
                //     "INNER JOIN CourseSoftware cs ON ts.CourseId = cs.CourseId " +
                //     "GROUP BY EvaluationAnswerId ORDER BY StudentId"
                // );

                /* issue 08102018 - student cannot download certificate (old courses) */

                sb.AppendFormat(
                    "SELECT StudentEvaluationID,EvaluationAnswerId,ts.CourseId,StudentId,StudentName,Email,'35' AS LanguageID,FYIndicatorKey,TrainingTypeKey,ProjectTypeKey,EventTypeKey,StartDate,CompletionDate,LocationName,Institution,productId,ProductVersionsId,productsecondaryId,ProductVersionsSecondaryId,PartnerType FROM " +
                    "(SELECT StudentEvaluationID,c.StudentId,CONCAT( FirstName, ' ', LastName ) AS StudentName,Email,LanguageID,CourseId,EvaluationAnswerId,FYIndicatorKey,TrainingTypeKey,ProjectTypeKey,EventTypeKey,StartDate,CompletionDate,LocationName,Institution,PartnerType FROM " +
                    "(SELECT EvaluationAnswerId,StudentEvaluationID,ea.CourseId,StudentId,FYIndicatorKey,TrainingTypeKey,ProjectTypeKey,EventTypeKey,DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate,DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate,LocationName,Institution,PartnerType FROM " +
                    "(SELECT EvaluationAnswerId,CourseId,StudentId,StudentEvaluationID FROM EvaluationAnswer WHERE CourseId = '" + id + "') AS ea " +
                    "INNER JOIN Courses c ON ea.CourseId = c.CourseId) AS c " +
                    "INNER JOIN tblStudents s ON c.StudentId = s.StudentID) AS ts " +
                    "INNER JOIN CourseSoftware cs ON ts.CourseId = cs.CourseId " +
                    "GROUP BY EvaluationAnswerId ORDER BY StudentId"
                );
                
                /* end line issue 08102018 - student cannot download certificate (old courses) */

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("StudentAAPEvent/{id}")]
        public IActionResult GetStudentAAPEvent(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "SELECT DISTINCT '' AS StudentEvaluationID, '' AS EvaluationAnswerId, c.CourseId, ts.StudentId, CONCAT( FirstName, ' ', LastName ) AS StudentName, Email, LanguageID," +
                    "FYIndicatorKey,TrainingTypeKey,ProjectTypeKey,EventTypeKey, CASE WHEN NOW() >= c.CompletionDate THEN 'TRUE' ELSE 'FALSE' END AS isShow, DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate,DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate, LocationName,Institution, productId, ProductVersionsId, productsecondaryId, ProductVersionsSecondaryId, PartnerType " +
                    "FROM Courses c " +
                    "INNER JOIN CourseStudent cst ON cst.CourseId = c.CourseId " +
                    "INNER JOIN tblStudents ts ON cst.StudentID = ts.StudentId " +
                    "INNER JOIN CourseSoftware cf ON cf.CourseId = c.CourseId " +
                    "WHERE c.CourseId = '" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("Detail/{id}")]
        public IActionResult DetailCourse(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "SELECT c.CourseId,c.ContactId,cc.InstructorId,cc.ContactName,c.SiteId,c.`Name`,DATE_FORMAT(c.StartDate,'%Y-%m-%d') AS StartDate,DATE_FORMAT(c.CompletionDate,'%Y-%m-%d') AS CompletionDate,DATE_FORMAT(c.EnrollmentExpireDate,'%Y-%m-%d') AS EnrollmentExpireDate,c.HoursTraining,c.HoursTrainingOther, " +
                    "c.LocationName,c.FYIndicatorKey,c.TrainingTypeKey,c.ProjectTypeKey,c.EventTypeKey,c.EventType2,c.ATCFacility,c.ATCComputer,c.PartnerType,c.Institution,c.InstitutionLogo,c.LogoName,c.LogoType,c.`Status`,cs.productId,cs.ProductVersionsId," +
                    "cs.productsecondaryId,cs.ProductVersionsSecondaryId,ctl.`Update`,ctl.Essentials,ctl.Intermediate,ctl.Advanced,ctl.Customized,ctl.Other	AS ctl_Other,ctl.`Comment` AS ctl_Comment," +
                    "ctf.InstructorLed,ctf.`Online`,ctm.Autodesk,ctm.AAP AS ctm_AAP,ctm.ATC AS ctm_ATC,ctm.Independent,ctm.IndependentOnline,ctm.ATCOnline,ctm.Other AS ctm_Other,ctm.`Comment` AS ctm_Comment,r.RoleId  " +
                    "FROM Courses c " +
                    "LEFT JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
                    "LEFT JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId " +
                    "LEFT JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId " +
                    "LEFT JOIN CourseSoftware cs ON c.CourseId = cs.CourseId " +
                    "LEFT JOIN Contacts_All cc ON c.ContactId = cc.ContactId " +
                      " Left Join Roles r on r.RoleCode =c.PartnerType  " +
                    "WHERE c.CourseId = '" + id + "' GROUP BY c.CourseId"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                cert = JObject.Parse(result);

                //get available institution logo from database (only for project certificate type)
               
                if (ds.Tables[0].Rows[0]["InstitutionLogo"] != null && ds.Tables[0].Rows[0]["InstitutionLogo"].GetType() != DBNull.Value.GetType())
                {
                    byte[] bytes = (byte[])ds.Tables[0].Rows[0]["InstitutionLogo"];
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    // string imageUrl = "data:image/png;base64," + base64String;

                    cert["InstitutionLogo"] = base64String;
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                // result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                result = cert.ToString();
            }

            return Ok(result);
        }

        [HttpGet("InstructorContact/{id}/{siteId}")]
        public IActionResult ContactInstructor(string id,string siteId)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select SiteContactLinks.SiteId,ContactName from Contacts_All "+
                    "left join SiteContactLinks on Contacts_All.ContactId = SiteContactLinks.ContactId "+
                    "left join Main_site on SiteContactLinks.SiteId = Main_site.SiteId "+
                    "where Contacts_All.ContactId = '"+id+"' and SiteContactLinks.SiteId = '"+siteId+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            string lastId = "";
            string courseid = "";
            byte[] imageBytes;
            string logoName = "";
            string logoType = "";
            JObject o1;
            JObject o2;
            JObject o3;
            JObject o4;
            JObject o5;
            int isAdded=0;
            string locationame = "";
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Courses;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            
            if(data.InstitutionLogo != null){
                logoName = data.InstitutionLogo["filename"].ToString();
                logoType = data.InstitutionLogo["filetype"].ToString();
                imageBytes = Convert.FromBase64String(data.InstitutionLogo["value"].ToString());
            } else{
                imageBytes = null;
            }
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select DISTINCT ContactId from Contacts_All where InstructorId = '"+data.ContactId+"' AND `Status` = 'A' ORDER BY CAST(ContactId AS UNSIGNED) DESC LIMIT 1"); /* Ada duplicate instructorid di contactid yg beda, jadi di giniin */
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o4 = JObject.Parse(result);
                
                sb = new StringBuilder();
                sb.AppendFormat("SELECT MAX(CourseId) AS CoursesId FROM (select CAST(RIGHT(CourseId,7) AS UNSIGNED) as CourseId from Courses) AS c");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o5 = JObject.Parse(result);
                
                int idcount=0;
                idcount = Int32.Parse(o5["CoursesId"].ToString()) + 1;
                
                courseid = data.SiteId + "0" + idcount.ToString("0000000");
                
                lastId = courseid;

                locationame = data.LocationName;
               

                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Courses "+
                     "(CourseId, Name, ATCFacility, ATCComputer,SiteId, ContactId, StartDate, CompletionDate,EnrollmentExpireDate, FYIndicatorKey, LocationName, TrainingTypeKey, ProjectTypeKey, EventTypeKey, HoursTraining,HoursTrainingOther, EvaluationQuestionId, CertificateId, Institution, PartnerType, EventType2, InstitutionLogo, LogoName, LogoType) " +
                    //"(CourseId, Name, ATCFacility, ATCComputer,SiteId, ContactId, StartDate, CompletionDate, FYIndicatorKey, LocationName, TrainingTypeKey, ProjectTypeKey, EventTypeKey, HoursTraining, EvaluationQuestionId, CertificateId, Institution, PartnerType, EventType2, InstitutionLogo, LogoName, LogoType) " +

                    //"values ('"+courseid+"','"+data.CourseTitle+"',"+data.CourseFacility+","+data.CourseEquipment+",'"+data.SiteId+"','"+o4["ContactId"]+"','"+data.StartDate+"','"+data.EndDate+"','"+data.Survey+"','"+data.LocationName+"','"+data.TrainingType+"','"+data.ProjectType+"','"+data.EventType+"','"+data.TrainingHours+"','"+data.EvaTemplate+"','"+data.CertTemplate+"','"+data.Institution+"','" + data.PartnerType + "');"+
                    // "values ('" + courseid + "','" + data.CourseTitle + "'," + data.CourseFacility + "," + data.CourseEquipment + ",'" + data.SiteId + "','" + data.ContactId + "','" + data.StartDate + "','" + data.EndDate + "','" + data.Survey + "','" + data.LocationName + "','" + data.TrainingType + "','" + data.ProjectType + "','" + data.EventType + "','" + data.TrainingHours + "','" + data.EvaTemplate + "','" + data.CertTemplate + "','" + data.Institution + "','" + data.PartnerType + "');" +
                   //// "values ('" + courseid + "','" + data.CourseTitle + "'," + data.CourseFacility + "," + data.CourseEquipment + ",'" + data.SiteId + "','" + o4["ContactId"] + "','" + data.StartDate + "','" + data.EndDate + "','" + data.Survey + "','" + data.LocationName + "','" + data.TrainingType + "','" + data.ProjectType + "','" + data.LearnerType + "','" + data.TrainingHours + "','" + data.EvaTemplate + "','" + data.CertTemplate + "','" + data.Institution + "','" + data.PartnerType + "','" + data.EventType + "',@logo,'" + logoName + "','" + logoType + "');"

                "values ('" + lastId + "','" + data.CourseTitle + "'," + data.CourseFacility + "," + data.CourseEquipment + ",'" + data.SiteId + "','" + o4["ContactId"] + "','" + data.StartDate + "','" + data.EndDate + "',(DATE ('" + data.EndDate + "')+ INTERVAL (Select DataValue from LookUp Where ControlName= 'EnrollmentExpireDate') DAY),'" + data.Survey + "','" + locationame + "','" + data.TrainingType + "','" + data.ProjectType + "','" + data.LearnerType + "','" + data.TrainingHours + "','" + data.HoursTrainingOther + "',0,0,'" + data.Institution + "','" + data.PartnerType + "','" + data.EventType + "',@logo,'" + logoName + "','" + logoType + "');"

                );

                sqlCommand = new MySqlCommand(sb.ToString());
                sqlCommand.Parameters.Add(new MySqlParameter("@logo", imageBytes));
                // Console.WriteLine(sb.ToString());
                isAdded = oDb.temporaryUpdateFunction(sqlCommand);
                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            catch (Exception e)
            { 
            }
            finally
            { 
            }


            //sb = new StringBuilder();
            //sb.AppendFormat("select count(*) as count from Courses;");
            //sqlCommand = new MySqlCommand(sb.ToString());
            //ds = oDb.getDataSetFromSP(sqlCommand);   

            //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //o2 = JObject.Parse(result); 
            //count2 = Int32.Parse(o2["count"].ToString());
            
            if(isAdded==1)
            {
                //string description = "Insert data Courses" + "<br/>" +
                //    "Course Id = "+ courseid + "<br/>" +
                //    "Partner Type = "+ data.PartnerType + "<br/>" +
                //    "Course Range Date = "+ data.StartDate + " to "+ data.EndDate + " <br/>" +
                //    "Course Name = "+ data.CourseTitle;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                sb = new StringBuilder();
                sb.AppendFormat("select * from Courses where CourseId = '" + lastId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                JObject OldData = new JObject();
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Courses, data.UserId.ToString(), data.cuid.ToString(), " where CourseId = " + lastId + "");


                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\","+
                    "\"lastInsertId\":\""+lastId+"\""+
                    "}";
            }
            else
            {
                res = "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed (Please make sure there is no Special Characters).\"" +
                    "}";
            }
 
            return Ok(res);  
        }

        [HttpPost("CourseTeachingLevel")]
        public IActionResult InsertCTL([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CourseTeachingLevel;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            //Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into CourseTeachingLevel "+
                    "(CourseId, `Update`, Essentials, Intermediate, Advanced, Customized, Other, `Comment`, `Status`) "+
                    "values ('"+data.CourseId+"',"+data.Update+","+data.Essentials+","+data.Intermediate+","+data.Advanced+","+data.Customized+","+data.Other+",'"+data.Komen+"','"+data.Status+"');"
                );
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CourseTeachingLevel;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            //Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
        }

        [HttpPost("CourseSoftware")]
        public IActionResult InsertCourseSoftware([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CourseSoftware;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            //Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into CourseSoftware "+
                    "(CourseId, productId, ProductVersionsId, productsecondaryId, ProductVersionsSecondaryId, `Status`) "+
                    "values ('"+data.CourseId+"','"+data.Product1+"',"+data.Version1+",'"+data.Product2+"',"+data.Version2+",'"+data.Status+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CourseSoftware;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            //Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
        }

        [HttpPost("CourseTrainingFormat")]
        public IActionResult InsertCTF([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CourseTrainingFormat;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            //Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into CourseTrainingFormat "+
                    "(CourseId, InstructorLed, `Online`, `Status`) "+
                    "values ('"+data.CourseId+"',"+data.InstructorLed+","+data.Online+",'"+data.Status+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CourseTrainingFormat;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            //Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
        }

        [HttpPost("CourseTrainingMaterial")]
        public IActionResult InsertCTM([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CourseTrainingMaterial;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            //Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into CourseTrainingMaterial "+
                    "(CourseId, Autodesk, AAP, ATC, Independent, IndependentOnline, ATCOnline, Other, Comment, Status) "+
                    "values ('"+data.CourseId+"',"+data.Autodesk+","+data.AAP+","+data.ATC+","+data.Independent+","+data.IndependentOnline+","+data.ATCOnline+","+data.Other5+",'"+data.Komen5+"','"+data.Status+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CourseTrainingMaterial;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            //Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
        }

        [HttpPost("CourseStudent")]
        // public IActionResult InsertCourseStudent([FromBody] dynamic data)
        public async Task<IActionResult> InsertCourseStudent([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            JObject o3 = new JObject();
            JObject NewData = new JObject();
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select count(*) as count from CourseStudent;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o1 = JObject.Parse(result); 
                count1 = Int32.Parse(o1["count"].ToString());

                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into CourseStudent "+
                    "(CourseId, StudentID,CourseTaken,SurveyTaken,`Status`) "+
                    "values ('"+data.CourseId+"','"+data.StudentID+"',"+data.CourseTaken+","+data.SurveyTaken+",'"+data.Status+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("select count(*) as count from CourseStudent;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);   

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o2 = JObject.Parse(result); 
                count2 = Int32.Parse(o2["count"].ToString());

                sb = new StringBuilder();
                sb.AppendFormat(
                    "SELECT c.CourseId, c.`Name` AS CourseName, CONCAT( c.StartDate, ' - ', c.CompletionDate ) AS CoursePeriod, ca.ContactName AS InstructorName, ca.EmailAddress, ms.SiteName, s.StudentID, CONCAT( s.Firstname, ' ', s.Lastname ) AS StudentName, s.Email AS StudentEmail, eb.`subject`, eb.body_email "+
                    "FROM (select CourseId, StudentID FROM CourseStudent WHERE CourseId = '"+data.CourseId+"' AND StudentID = '"+data.StudentID+"') as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId INNER JOIN tblStudents s ON cs.StudentID = s.StudentID INNER JOIN Main_site ms ON c.SiteId = ms.SiteId INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId LEFT JOIN EmailBody eb ON CASE WHEN ca.PrimaryLanguage NOT IN ('35') THEN '35' ELSE ca.PrimaryLanguage END = eb.`Key` AND eb.body_type = '1' AND ca.Status='A' "
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);   

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                string mailto = o3["EmailAddress"].ToString();
                string subject = o3["subject"].ToString();
                string message = ""; 

                message += o3["body_email"].ToString();

                message = message.Replace("[CourseCode]",o3["CourseId"].ToString());
                message = message.Replace("[CourseName]",o3["CourseName"].ToString());
                message = message.Replace("[CoursePeriod]",o3["CoursePeriod"].ToString());
                message = message.Replace("[InstructorName]",o3["InstructorName"].ToString());
                message = message.Replace("[SiteName]",o3["SiteName"].ToString());
                message = message.Replace("[StudentId]",o3["StudentID"].ToString());
                message = message.Replace("[StudentName]",o3["StudentName"].ToString());
                message = message.Replace("[StudentEmail]",o3["StudentEmail"].ToString());
                 
                // Boolean resmail = await _emailSender.SendEmailAsync(mailto, subject, message); 
                
                // if(resmail == true){  
                //     Console.WriteLine("Email send");
                // }
                // else{ 
                //     Console.WriteLine("Email not send");  
                // }
            }
            catch (Exception e)
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Insert Data Failed\""+
                    "}";
                return Ok(res);
            }
           
            // Console.WriteLine(count2 + " > " + count1);
            if(count2 > count1)
            { 

                //string description = "Enroll Course <br/>" +
                //        "Student Id = "+ data.StudentID +" <br/>"+
                //        "Course Id = "+ data.CourseId;

                //_audit.ActionPost(data.StudentID.ToString(), description, data.cuid.ToString());
                JObject OldData = new JObject();
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.CourseStudent, data.StudentID.ToString(), data.cuid.ToString(), " where CourseId = " + o3["CourseId"].ToString() + "And StudentID = " + o3["StudentID"].ToString() + "");

                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
        }

        [HttpGet("Certificate")]
        public IActionResult SelectCertificate()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Certificate ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("InstructorStudent/{site_id}/{instructor_id}/{completion_date}")]
        public IActionResult FinishStudent(string site_id, string instructor_id, string completion_date)
        {     
            //var o1 = JObject.Parse(obj);
            string query = "";

            query += 
                "SELECT ts.StudentId,CONCAT(ts.FirstName,' ',ts.LastName) AS StudentName, EvaluationAnswerId,SiteName,DATE_FORMAT(c.CompletionDate,'%d-%M-%Y') AS CourseDate FROM EvaluationAnswer ea " +
                "INNER JOIN Courses c ON ea.CourseId = c.CourseId " +
                "INNER JOIN tblStudents ts ON ea.StudentId = ts.StudentId " +
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                "WHERE c.SiteId = '" + site_id + "' AND ca.InstructorId = '" + instructor_id + "' AND c.CompletionDate = '" + completion_date + "' GROUP BY ts.StudentID,c.CourseId ORDER BY StudentId ASC"; /* issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */
            
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpGet("FinishStudentCourse/{course_id}")]
        public IActionResult FinishStudent(string course_id)
        {     
            //var o1 = JObject.Parse(obj);
            string query = "";

            query += "SELECT DISTINCT cs.StudentID,CONCAT( ts.Firstname, ' ', ts.Lastname ) AS StudentName,ea.EvaluationQuestionCode FROM CourseStudent cs " +
                "INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                "LEFT JOIN tblStudents ts ON cs.StudentID = ts.StudentID " +
                "INNER JOIN EvaluationAnswer ea ON cs.CourseId = ea.CourseId " +
                "WHERE cs.CourseTaken = 1 AND cs.SurveyTaken = 1 AND cs.CourseId = '"+course_id+"'";
            
            //int i = 0;
            //JObject json = JObject.FromObject(o1);
            //foreach (JProperty property in json.Properties())
            //{ 
            //    if(property.Name.Equals("CourseId")){   
            //        if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
            //            if(i==0 || i>0){query += " AND ";} 
            //            query += "cs.CourseId = '"+property.Value.ToString()+"' "; 
            //            i=i+1;
            //        }
            //    } 
            //} 
            
            try
            {  
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpGet("where/{obj}/{sortBy}/{sortDirection}/{orgId}/{joinType}")]
        public IActionResult WhereObj(string obj,string sortBy,string sortDirection,string orgId,string instructorId,string joinType)
        {     
            var o1 = JObject.Parse(obj);
            string query = "";
            string result;
            
            // query += 
            //     "SELECT CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,s.ContactId,ContactName,InstructorId," +
            //     "CONCAT('FY',CAST((RIGHT(FYIndicatorKey,2)) AS UNSIGNED) + 1,' (',FYIndicatorKey,')') AS FYIndicatorKey,SiteId,SiteName,OrgId,COUNT(EvaluationAnswerId) AS Evals FROM " +
            //     "(SELECT EvaluationAnswerId,CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,ContactId,FYIndicatorKey,c.SiteId,SiteName,OrgId FROM " +
            //     "(SELECT EvaluationAnswerId,c.CourseId,`Name` AS CourseTitle,PartnerType,DATE_FORMAT( c.StartDate, '%d/%m/%Y' ) AS StartDate," +
            //     "DATE_FORMAT( c.CompletionDate, '%d/%m/%Y' ) AS CompletionDate,SiteId,ContactId,FYIndicatorKey FROM " +
            //     "(SELECT EvaluationAnswerId,CourseId FROM EvaluationAnswer) AS ea " + joinType +
            //     " JOIN Courses c ON ea.CourseId = c.CourseId ";

            // CONCAT('FY',CAST(RIGHT(FYIndicatorKey,2) AS UNSIGNED) + 1,' (',FYIndicatorKey,')') AS FYIndicatorKey

            query += 
                "SELECT CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,s.ContactId,ContactName,InstructorId, " +
                "CONCAT('FY',CAST(RIGHT(FYIndicatorKey,2) AS UNSIGNED) + 1,' (',FYIndicatorKey,')') AS FYIndicatorKey,SiteId,SiteName,OrgId,COUNT(EvaluationAnswerId) AS Evals FROM " +
                "(SELECT EvaluationAnswerId,CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,ContactId,FYIndicatorKey,ea.SiteId,SiteName,OrgId FROM " +
                "(SELECT EvaluationAnswerId,c.CourseId,`Name` AS CourseTitle,PartnerType,DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate, " +
                "DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate,SiteId,ContactId,FYIndicatorKey FROM " +
                "(SELECT * FROM Courses ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        if (property.Value.ToString() == "\"AAP\"")
                        {
                            query += "PartnerType LIKE '%AAP%' ";
                        }
                        else
                        {
                            query += "PartnerType IN (" + property.Value.ToString() + ") ";
                        }
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CourseId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CourseId LIKE '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "SiteId LIKE '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteIdArr"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "SiteId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            
                if (property.Name.Equals("CourseTitle"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "`Name` LIKE '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CourseStartDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "StartDate >= '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CourseEndDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CompletionDate <= '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "FYIndicatorKey LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("InstructorId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "ContactId IN (SELECT ContactId FROM Contacts_All WHERE InstructorId LIKE '%" + property.Value.ToString().Trim() + "%') ";
                        i = i + 1;
                    }
                }
            }

            /* test */
            // int limit = 50000;
            // string finalresult = "";
            // string PartnerType = "";
            // string CourseId = "";
            // string SiteId = "";
            // string SiteIdArr = "";
            // string CourseTitle = "";
            // string CourseStartDate = "";
            // string CourseEndDate = "";
            // string SurveyYear = "";
            // string InstructorId = "";
            // int i = 0;
            // JObject json = JObject.FromObject(o1);
            // foreach (JProperty property in json.Properties())
            // {
            //     if (property.Name.Equals("PartnerType"))
            //     {
            //         if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //         {
            //             if (property.Value.ToString() == "\"AAP\"")
            //             {
            //                 PartnerType += "PartnerType LIKE '%AAP%' ";
            //             }
            //             else
            //             {
            //                 PartnerType += "PartnerType IN (" + property.Value.ToString() + ") ";
            //             }
            //             i = i + 1;
            //         }
            //     }
            //     if (property.Name.Equals("CourseId"))
            //     {
            //         if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //         {
            //             CourseId += "AND CourseId LIKE '%" + property.Value.ToString().Trim() + "%' ";
            //             i = i + 1;
            //         }
            //     }
                
            //     if (property.Name.Equals("SiteId"))
            //     {
            //         if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //         {
            //             SiteId += "AND SiteId LIKE '%" + property.Value.ToString().Trim() + "%' ";
            //             i = i + 1;
            //         }
            //     }

            //     if (property.Name.Equals("SiteIdArr"))
            //     {
            //         if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //         {
            //             SiteIdArr += "AND SiteId in (" + property.Value.ToString() + ") ";
            //             i = i + 1;
            //         }
            //     }
            
            //     if (property.Name.Equals("CourseTitle"))
            //     {
            //         if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //         {
            //             CourseTitle += "AND `Name` LIKE '%" + property.Value.ToString().Trim() + "%' ";
            //             i = i + 1;
            //         }
            //     }

            //     if (property.Name.Equals("CourseStartDate"))
            //     {
            //         if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //         {
            //             CourseStartDate += "AND StartDate >= '" + property.Value.ToString() + "' ";
            //             i = i + 1;
            //         }
            //     }

            //     if (property.Name.Equals("CourseEndDate"))
            //     {
            //         if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //         {
            //             CourseEndDate += "AND CompletionDate <= '" + property.Value.ToString() + "' ";
            //             i = i + 1;
            //         }
            //     }

            //     if (property.Name.Equals("SurveyYear"))
            //     {
            //         if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //         {
            //             SurveyYear += "AND FYIndicatorKey LIKE '%" + property.Value.ToString() + "%' ";
            //             i = i + 1;
            //         }
            //     }

            //     if (property.Name.Equals("InstructorId"))
            //     {
            //         if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //         {
            //             InstructorId += "AND ContactId IN (SELECT ContactId FROM Contacts_All WHERE InstructorId LIKE '%" + property.Value.ToString().Trim() + "%') ";
            //             i = i + 1;
            //         }
            //     }
            // }

            // string query_count = "SELECT COUNT(*) AS count FROM Courses WHERE "+PartnerType+""+CourseId+""+SiteId+""+SiteIdArr+""+CourseTitle+""+CourseStartDate+""+CourseEndDate+""+SurveyYear+""+InstructorId+"";
            // sb.AppendFormat(query_count);
            // sqlCommand = new MySqlCommand(sb.ToString());
            // ds = oDb.getDataSetFromSP(sqlCommand);
            // string result2 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            // JObject objcount = JObject.Parse(result2);
            
            // int count = Int32.Parse(objcount["count"].ToString());
            // int div = count / limit;
            // int mod = count % limit;

            // if(div > 0){
            //     for(int j=1;j<=div+1;j++){
            //         query="";
            //         if(j == 1){
            //             query += 
            //                 "SELECT CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,s.ContactId,ContactName,InstructorId, " +
            //                 "CONCAT('FY',CAST(RIGHT(FYIndicatorKey,2) AS UNSIGNED) + 1,' (',FYIndicatorKey,')') AS FYIndicatorKey,SiteId,SiteName,OrgId,COUNT(EvaluationAnswerId) AS Evals FROM " +
            //                 "(SELECT EvaluationAnswerId,CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,ContactId,FYIndicatorKey,ea.SiteId,SiteName,OrgId FROM " +
            //                 "(SELECT EvaluationAnswerId,c.CourseId,`Name` AS CourseTitle,PartnerType,DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate, " +
            //                 "DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate,SiteId,ContactId,FYIndicatorKey FROM " +
            //                 "(SELECT * FROM Courses WHERE "+PartnerType+""+CourseId+""+SiteId+""+SiteIdArr+""+CourseTitle+""+CourseStartDate+""+CourseEndDate+""+SurveyYear+""+InstructorId+" AND CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533')";
            //             query += "LIMIT "+limit+" ";
            //             query += 
            //                 ") AS c " +
            //                 "LEFT JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId ) AS ea " +
            //                 "INNER JOIN Main_site s ON ea.SiteId = s.SiteId WHERE s.`Status` IN ('A','H') ";
            //             if(orgId != "null"){
            //                 if(orgId.Substring(0,1) == "\""){
            //                     query += " AND OrgId IN (" + orgId.Trim() + ")) AS s ";
            //                 }else{
            //                     query += " AND OrgId LIKE '%" + orgId.Trim() + "%') AS s ";
            //                 }
            //             } else{
            //                 query += ") AS s ";
            //             }
            //             query += "INNER JOIN Contacts_All ca ON s.ContactId = ca.ContactId GROUP BY CourseId ORDER BY " + sortBy + " " + sortDirection;

            //             sb = new StringBuilder();
            //             sb.AppendFormat(query);
            //             sqlCommand = new MySqlCommand(sb.ToString());
            //             ds = oDb.getDataSetFromSP(sqlCommand);
            //             if (ds.Tables[0].Rows.Count != 0)
            //             {
            //                 //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
            //                 ds = MainOrganizationController.EscapeSpecialChar(ds);
            //             }
            //             // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            //             result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            //             result = result.Replace("]","");
            //             finalresult = result;
            //         }else if(j == div+1){
            //             query += 
            //                 "SELECT CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,s.ContactId,ContactName,InstructorId, " +
            //                 "CONCAT('FY',CAST(RIGHT(FYIndicatorKey,2) AS UNSIGNED) + 1,' (',FYIndicatorKey,')') AS FYIndicatorKey,SiteId,SiteName,OrgId,COUNT(EvaluationAnswerId) AS Evals FROM " +
            //                 "(SELECT EvaluationAnswerId,CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,ContactId,FYIndicatorKey,ea.SiteId,SiteName,OrgId FROM " +
            //                 "(SELECT EvaluationAnswerId,c.CourseId,`Name` AS CourseTitle,PartnerType,DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate, " +
            //                 "DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate,SiteId,ContactId,FYIndicatorKey FROM " +
            //                 "(SELECT * FROM Courses WHERE "+PartnerType+""+CourseId+""+SiteId+""+SiteIdArr+""+CourseTitle+""+CourseStartDate+""+CourseEndDate+""+SurveyYear+""+InstructorId+" AND CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533')";
            //             query += " LIMIT "+((limit*(j-1))+mod)+" OFFSET "+limit*(j-1)+" ";
            //             query += 
            //                 ") AS c " +
            //                 "LEFT JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId ) AS ea " +
            //                 "INNER JOIN Main_site s ON ea.SiteId = s.SiteId WHERE s.`Status` IN ('A','H') ";
            //             if(orgId != "null"){
            //                 if(orgId.Substring(0,1) == "\""){
            //                     query += " AND OrgId IN (" + orgId.Trim() + ")) AS s ";
            //                 }else{
            //                     query += " AND OrgId LIKE '%" + orgId.Trim() + "%') AS s ";
            //                 }
            //             } else{
            //                 query += ") AS s ";
            //             }
            //             query += "INNER JOIN Contacts_All ca ON s.ContactId = ca.ContactId GROUP BY CourseId ORDER BY " + sortBy + " " + sortDirection;

            //             sb = new StringBuilder();
            //             sb.AppendFormat(query);
            //             sqlCommand = new MySqlCommand(sb.ToString());
            //             ds = oDb.getDataSetFromSP(sqlCommand);
            //             if (ds.Tables[0].Rows.Count != 0)
            //             {
            //                 //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
            //                 ds = MainOrganizationController.EscapeSpecialChar(ds);
            //             }
            //             // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            //             result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            //             if(div+1 == 2){
            //                 result = result.Replace("[",",");
            //             }else{
            //                 result = result.Replace("[","");
            //             }
            //             finalresult += result;
            //         }
            //         else{
            //             query += 
            //                 "SELECT CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,s.ContactId,ContactName,InstructorId, " +
            //                 "CONCAT('FY',CAST(RIGHT(FYIndicatorKey,2) AS UNSIGNED) + 1,' (',FYIndicatorKey,')') AS FYIndicatorKey,SiteId,SiteName,OrgId,COUNT(EvaluationAnswerId) AS Evals FROM " +
            //                 "(SELECT EvaluationAnswerId,CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,ContactId,FYIndicatorKey,ea.SiteId,SiteName,OrgId FROM " +
            //                 "(SELECT EvaluationAnswerId,c.CourseId,`Name` AS CourseTitle,PartnerType,DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate, " +
            //                 "DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate,SiteId,ContactId,FYIndicatorKey FROM " +
            //                 "(SELECT * FROM Courses WHERE "+PartnerType+""+CourseId+""+SiteId+""+SiteIdArr+""+CourseTitle+""+CourseStartDate+""+CourseEndDate+""+SurveyYear+""+InstructorId+" AND CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533')";
            //             query += " LIMIT "+limit*(j-1)+" OFFSET "+limit*j+" ";
            //             query += 
            //                 ") AS c " +
            //                 "LEFT JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId ) AS ea " +
            //                 "INNER JOIN Main_site s ON ea.SiteId = s.SiteId WHERE s.`Status` IN ('A','H') ";
            //             if(orgId != "null"){
            //                 if(orgId.Substring(0,1) == "\""){
            //                     query += " AND OrgId IN (" + orgId.Trim() + ")) AS s ";
            //                 }else{
            //                     query += " AND OrgId LIKE '%" + orgId.Trim() + "%') AS s ";
            //                 }
            //             } else{
            //                 query += ") AS s ";
            //             }
            //             query += "INNER JOIN Contacts_All ca ON s.ContactId = ca.ContactId GROUP BY CourseId ORDER BY " + sortBy + " " + sortDirection;

            //             sb = new StringBuilder();
            //             sb.AppendFormat(query);
            //             sqlCommand = new MySqlCommand(sb.ToString());
            //             ds = oDb.getDataSetFromSP(sqlCommand);
            //             if (ds.Tables[0].Rows.Count != 0)
            //             {
            //                 //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
            //                 ds = MainOrganizationController.EscapeSpecialChar(ds);
            //             }
            //             // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            //             result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            //             result = result.Replace("[","");
            //             result = result.Replace("]","");
            //             finalresult += ","+result+",";
            //         }
            //     }
            // }else{
            //     query += 
            //         "SELECT CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,s.ContactId,ContactName,InstructorId, " +
            //         "CONCAT('FY',CAST(RIGHT(FYIndicatorKey,2) AS UNSIGNED) + 1,' (',FYIndicatorKey,')') AS FYIndicatorKey,SiteId,SiteName,OrgId,COUNT(EvaluationAnswerId) AS Evals FROM " +
            //         "(SELECT EvaluationAnswerId,CourseId,CourseTitle,PartnerType,StartDate,CompletionDate,ContactId,FYIndicatorKey,ea.SiteId,SiteName,OrgId FROM " +
            //         "(SELECT EvaluationAnswerId,c.CourseId,`Name` AS CourseTitle,PartnerType,DATE_FORMAT( c.StartDate, '%m/%d/%Y' ) AS StartDate, " +
            //         "DATE_FORMAT( c.CompletionDate, '%m/%d/%Y' ) AS CompletionDate,SiteId,ContactId,FYIndicatorKey FROM " +
            //         "(SELECT * FROM Courses WHERE "+PartnerType+""+CourseId+""+SiteId+""+SiteIdArr+""+CourseTitle+""+CourseStartDate+""+CourseEndDate+""+SurveyYear+""+InstructorId+" AND CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533')";
            //     query += 
            //         ") AS c " +
            //         "LEFT JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId ) AS ea " +
            //         "INNER JOIN Main_site s ON ea.SiteId = s.SiteId WHERE s.`Status` IN ('A','H') ";
            //     if(orgId != "null"){
            //         if(orgId.Substring(0,1) == "\""){
            //             query += " AND OrgId IN (" + orgId.Trim() + ")) AS s ";
            //         }else{
            //             query += " AND OrgId LIKE '%" + orgId.Trim() + "%') AS s ";
            //         }
            //     } else{
            //         query += ") AS s ";
            //     }
            //     query += "INNER JOIN Contacts_All ca ON s.ContactId = ca.ContactId GROUP BY CourseId ORDER BY " + sortBy + " " + sortDirection;

            //     sb = new StringBuilder();
            //     sb.AppendFormat(query);
            //     sqlCommand = new MySqlCommand(sb.ToString());
            //     ds = oDb.getDataSetFromSP(sqlCommand);
            //     if (ds.Tables[0].Rows.Count != 0)
            //     {
            //         //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
            //         ds = MainOrganizationController.EscapeSpecialChar(ds);
            //     }
            //     // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            //     result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            //     finalresult=result;
            // }
            /* end test */

            if(i<2){
                query += "limit 1000 ";
            }

            query += 
                ") AS c " +
                "LEFT JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId ) AS ea " +
                // "INNER JOIN Main_site s ON c.SiteId = s.SiteId WHERE s.`Status` IN ('A','H') ";
                "INNER JOIN Main_site s ON ea.SiteId = s.SiteId WHERE s.`Status` IN ('A','H') ";

            if(orgId != "null"){
                if(orgId.Substring(0,1) == "\""){
                    query += " AND OrgId IN (" + orgId.Trim() + ")) AS s ";
                }else{
                    query += " AND OrgId LIKE '%" + orgId.Trim() + "%') AS s ";
                }
            } else{
                query += ") AS s ";
            }

            query += "INNER JOIN Contacts_All ca ON s.ContactId = ca.ContactId";

            //if(instructorId != "null"){
                //query += " WHERE InstructorId LIKE '%" + instructorId + "%' ";
            //}

            query += " GROUP BY CourseId ORDER BY " + sortBy + " " + sortDirection;

            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                // if (ds.Tables[0].Rows.Count != 0)
                // {
                //     //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                //     ds = MainOrganizationController.EscapeSpecialChar(ds);
                // }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }
            
            return Ok(result);  
            // return Ok(finalresult);  
        }

        // [HttpGet("whereNew/{obj}/{sortBy}/{sortDirection}/{orgId}/{joinType}")]
        // public IActionResult whereNew(string obj,string sortBy,string sortDirection,string orgId,string instructorId,string joinType)
        // {     
        //     var o1 = JObject.Parse(obj);
        //     string query = "";
        //     string result;
            
        //     query += 
        //         "SELECT count(*) as count FROM Courses C INNER JOIN Main_site s ON C.SiteId = s.SiteId INNER JOIN Contacts_All ca ON C.ContactId = ca.ContactId ";

        //     int i = 0;
        //     JObject json = JObject.FromObject(o1);
        //     foreach (JProperty property in json.Properties())
        //     {
        //         if (property.Name.Equals("PartnerType"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 if (property.Value.ToString() == "\"AAP\"")
        //                 {
        //                     query += "PartnerType LIKE '%AAP%' ";
        //                 }
        //                 else
        //                 {
        //                     query += "PartnerType IN (" + property.Value.ToString() + ") ";
        //                 }
        //                 i = i + 1;
        //             }
        //         }
        //         if (property.Name.Equals("CourseId"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "CourseId LIKE '%" + property.Value.ToString().Trim() + "%' ";
        //                 i = i + 1;
        //             }
        //         }
                
        //         if (property.Name.Equals("SiteId"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "s.SiteId LIKE '%" + property.Value.ToString().Trim() + "%' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("SiteIdArr"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "s.SiteId in (" + property.Value.ToString() + ") ";
        //                 i = i + 1;
        //             }
        //         }
            
        //         if (property.Name.Equals("CourseTitle"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "`Name` LIKE '%" + property.Value.ToString().Trim() + "%' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("CourseStartDate"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "StartDate >= '" + property.Value.ToString() + "' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("CourseEndDate"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "CompletionDate <= '" + property.Value.ToString() + "' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("SurveyYear"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "FYIndicatorKey LIKE '%" + property.Value.ToString() + "%' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("InstructorId"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "ContactId IN (SELECT ContactId FROM Contacts_All WHERE InstructorId LIKE '%" + property.Value.ToString().Trim() + "%') ";
        //                 i = i + 1;
        //             }
        //         }
        //     }
            
        //     if(orgId != "null"){
        //         if(orgId.Substring(0,1) == "\""){
        //             query += "AND s.OrgId IN (" + orgId.Trim() + ") ";
        //         }else{
        //             query += "AND s.OrgId LIKE '%" + orgId.Trim() + "%' ";
        //         }
        //     } 

        //     // Console.WriteLine(query);

        //     try
        //     {  
        //         sb.AppendFormat(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not Found";
        //     }
        //     finally
        //     {
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     }
            
        //     return Ok(result);  
        // }

        // [HttpGet("whereNew/{obj}/{sortBy}/{sortDirection}/{orgId}/{joinType}/{limit}/{ofset}")]
        // public IActionResult whereNew(string obj,string sortBy,string sortDirection,string orgId,string instructorId,string joinType,int limit, int ofset)
        // {     
        //     var o1 = JObject.Parse(obj);
        //     string query = "";
        //     string result;
            
        //     query += 
        //         "SELECT C.CourseId,`Name` AS CourseTitle,InstructorId,PartnerType,DATE_FORMAT( C.StartDate, '%m/%d/%Y' ) AS StartDate,DATE_FORMAT( C.CompletionDate, '%m/%d/%Y' ) AS CompletionDate,s.SiteId,s.SiteName,C.ContactId,ca.ContactName,CONCAT( 'FY', CAST( RIGHT ( FYIndicatorKey, 2 ) AS UNSIGNED ) + 1, ' (', FYIndicatorKey, ')' ) AS FYIndicatorKey,( SELECT count( * ) FROM EvaluationAnswer tea WHERE tea.courseid = C.CourseId ) evalno "+
        //         "FROM Courses C "+
        //         "INNER JOIN Main_site s ON C.SiteId = s.SiteId "+
        //         "INNER JOIN Contacts_All ca ON C.ContactId = ca.ContactId  ";

        //     int i = 0;
        //     JObject json = JObject.FromObject(o1);
        //     foreach (JProperty property in json.Properties())
        //     {
        //         if (property.Name.Equals("PartnerType"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 if (property.Value.ToString() == "\"AAP\"")
        //                 {
        //                     query += "PartnerType LIKE '%AAP%' ";
        //                 }
        //                 else
        //                 {
        //                     query += "PartnerType IN (" + property.Value.ToString() + ") ";
        //                 }
        //                 i = i + 1;
        //             }
        //         }
        //         if (property.Name.Equals("CourseId"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "CourseId LIKE '%" + property.Value.ToString().Trim() + "%' ";
        //                 i = i + 1;
        //             }
        //         }
                
        //         if (property.Name.Equals("SiteId"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "s.SiteId LIKE '%" + property.Value.ToString().Trim() + "%' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("SiteIdArr"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "s.SiteId in (" + property.Value.ToString() + ") ";
        //                 i = i + 1;
        //             }
        //         }
            
        //         if (property.Name.Equals("CourseTitle"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "`Name` LIKE '%" + property.Value.ToString().Trim() + "%' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("CourseStartDate"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "StartDate >= '" + property.Value.ToString() + "' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("CourseEndDate"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "CompletionDate <= '" + property.Value.ToString() + "' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("SurveyYear"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "FYIndicatorKey LIKE '%" + property.Value.ToString() + "%' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("InstructorId"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if (i == 0) { query += " WHERE "; }
        //                 if (i > 0) { query += " AND "; }
        //                 query += "ContactId IN (SELECT ContactId FROM Contacts_All WHERE InstructorId LIKE '%" + property.Value.ToString().Trim() + "%') ";
        //                 i = i + 1;
        //             }
        //         }
        //     }

        //     if(orgId != "null"){
        //         if(orgId.Substring(0,1) == "\""){
        //             query += " AND s.OrgId IN (" + orgId.Trim() + ") ";
        //         }else{
        //             query += " AND s.OrgId LIKE '%" + orgId.Trim() + "%' ";
        //         }
        //     } 

        //     if(limit != 0 || ofset != 0){
        //         query += " limit "+limit+" OFFSET "+ofset+" ";
        //     }

        //     Console.WriteLine(query);

        //     try
        //     {  
        //         sb.AppendFormat(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not Found";
        //     }
        //     finally
        //     {
        //         result = MySqlDb.SerializeDataTable(ds.Tables[0]);
        //     }
            
        //     return Ok(result);  
        // }

        [HttpPost("FilterCourseCount")]
        public IActionResult FilterCourseCount([FromBody] dynamic data)
        {     
            string query = "";
            string result = "";
            
            query += 
                "SELECT count(*) as count FROM Courses C INNER JOIN Main_site s ON C.SiteId = s.SiteId and s.`Status` != 'X' INNER JOIN Contacts_All ca ON C.ContactId = ca.ContactId and ca.`Status` != 'X' ";

            int i = 0;
            JObject json = JObject.FromObject(data);

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        if (property.Value.ToString() == "\"AAP\"")
                        {
                            query += "PartnerType LIKE '%AAP%' ";
                        }
                        else
                        {
                            query += "PartnerType IN (" + property.Value.ToString() + ") ";
                        }
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CourseId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CourseId LIKE '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId LIKE '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteIdArr"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            
                if (property.Name.Equals("CourseTitle"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "`Name` LIKE '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CourseStartDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "StartDate >= '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CourseEndDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CompletionDate <= '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "FYIndicatorKey LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("InstructorId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "InstructorId LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString().Trim() + ") ";
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteCountryCode IN ( select sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" ))";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("FilterOrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "OrgId LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("FilterInst"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "InstructorId LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
            }

            // Console.WriteLine(query);

            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            
            return Ok(result);  
        }

        [HttpPost("FilterCourse")]
        public IActionResult FilterCourse([FromBody] dynamic data)
        {     
            string query = "";
            string result;
            int limit = 0;
            int ofset = 0;
            string sortby="";
            string sortvalue="";
            
            query += 
                "SELECT C.CourseId,`Name` AS CourseTitle,InstructorId,PartnerType,DATE_FORMAT( C.StartDate, '%d-%M-%Y' ) AS StartDate,DATE_FORMAT( C.CompletionDate, '%d-%M-%Y' ) AS CompletionDate,s.SiteId,s.SiteName,C.ContactId,ca.ContactName,CONCAT( 'FY', CAST( RIGHT ( FYIndicatorKey, 2 ) AS UNSIGNED ) + 1, ' (', FYIndicatorKey, ')' ) AS FYIndicatorKey,( SELECT count( * ) FROM EvaluationAnswer tea WHERE tea.courseid = C.CourseId ) evalno,( SELECT count( * ) FROM CourseStudent cst WHERE cst.courseid = C.CourseId ) enroll_no, CONCAT(p.productName,' ',IFNULL(pv.Version,'')) AS PrimaryProduct,CONCAT(p2.productName,' ',IFNULL(pv2.Version,'')) AS SecondaryProduct,Territory_Name AS Territory "+
                "FROM Courses C " +
                "LEFT JOIN Main_site s ON C.SiteId = s.SiteId and s.`Status` != 'X' " +
                "LEFT JOIN Contacts_All ca ON C.ContactId = ca.ContactId and ca.`Status` != 'X' " +
                "LEFT JOIN CourseSoftware cs ON C.CourseId = cs.CourseId " +
                "LEFT JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
                "LEFT JOIN ProductVersions pv2 ON cs.ProductVersionsSecondaryId = pv2.ProductVersionsId " +
                "LEFT JOIN Products p ON cs.productId = p.productId "+
                "LEFT JOIN Products p2 ON cs.productsecondaryId = p2.productId "+
                "INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " +
                "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId ";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        if (property.Value.ToString() == "\"AAP\"")
                        {
                            query += "PartnerType LIKE '%AAP%' ";
                        }
                        else
                        {
                            query += "PartnerType IN (" + property.Value.ToString() + ") ";
                        }
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CourseId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "C.CourseId LIKE '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId LIKE '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteIdArr"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            
                if (property.Name.Equals("CourseTitle"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "`Name` LIKE '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CourseStartDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "StartDate >= '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CourseEndDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CompletionDate <= '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "FYIndicatorKey LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("InstructorId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "InstructorId LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteCountryCode IN ( select sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" ))";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("FilterOrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "OrgId LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("FilterInst"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "InstructorId LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString().Trim() + ") ";
                    }
                }

                if (property.Name.Equals("Limit"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        limit = Int32.Parse(property.Value.ToString());
                    }else{
                        limit = 0;
                    }
                }

                if (property.Name.Equals("Offset"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        ofset = Int32.Parse(property.Value.ToString());
                    }else{
                        ofset = 0;
                    }
                }

                if (property.Name.Equals("SortBy"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        sortby = property.Value.ToString();
                    }else{
                        sortby = "";
                    }
                }

                if (property.Name.Equals("SortValue"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        sortvalue = property.Value.ToString();
                    }else{
                        sortvalue = "";
                    }
                }
            }

            if(sortvalue != "" && sortby != ""){
                query += " ORDER BY "+sortvalue+" "+sortby+" ";
            }

            if(limit != 0 || ofset != 0){
                query += " limit "+limit+" OFFSET "+ofset+" ";
            }

            Console.WriteLine(query);
            List<dynamic> dynamicModel = new List<dynamic>();
            
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                    DataTable dt = ds.Tables[0];
                    dynamicModel = dataService.ToDynamic(dt);
                }
            }
            catch
            {
                result = "Not Found";
            }
            //finally
            //{
            //    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            //}
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }
            return Ok(result);  
        }

        [HttpGet("Student/{id}")]
        public IActionResult GetStudentCourse(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "SELECT CourseStudent.StudentID,Firstname,Lastname,Email,Phone,CourseTaken,Courses.`Name`,Courses.CourseCode FROM tblStudents "+
                    "INNER JOIN CourseStudent ON CourseStudent.StudentID = tblStudents.StudentID "+
                    "INNER JOIN Courses ON Courses.CourseId = CourseStudent.CourseId "+
                    "WHERE CourseStudent.CourseId = '"+id+"' and tblStudents.Status != 'X' GROUP BY StudentID"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                /* issue 07092018 escape char " , on EM3033503253 . bener kudu di pasang di kabeh API, terus hapus replace di angular . hmmm */
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCourses(string id, [FromBody] dynamic data)
        {  
            JObject o3;
            byte[] imageBytes;
            string logoName = "";
            string logoType = "";

            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";

            #region olddata_beforeupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from Courses where CourseId ='" + id + "';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion

            if (data.InstitutionLogo != null){
                logoName = data.InstitutionLogo["filename"].ToString();
                logoType = data.InstitutionLogo["filetype"].ToString();
                imageBytes = Convert.FromBase64String(data.InstitutionLogo["value"].ToString());
            } else{
                imageBytes = null;
            }
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("UPDATE Courses SET TrainingTypeKey = null, ProjectTypeKey = null, EventTypeKey = null, EventType2 = null, ATCFacility = 0, ATCComputer = 0 WHERE CourseId = '"+id+"'"); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                //Sebenernya instructor id dari form angular adalah contact id
                sb = new StringBuilder();
                sb.AppendFormat(
                    "UPDATE Courses SET "+
                    "`Name`='"+data.CourseTitle+"',"+
                    "ATCFacility="+data.CourseFacility+","+
                    "ATCComputer="+data.CourseEquipment+","+
                    "ContactId='"+data.InstructorId+"',"+
                    "StartDate='"+data.StartDate+"',"+
                    "CompletionDate='"+data.EndDate+"',"+
                    "EnrollmentExpireDate="+ "(DATE ('" + data.EndDate + "')+ INTERVAL (Select DataValue from LookUp Where ControlName= 'EnrollmentExpireDate') DAY),"+
                    "FYIndicatorKey='" +data.Survey+"',"+
                    "LocationName='"+data.LocationName+"',"+
                    "TrainingTypeKey='"+data.TrainingType+"',"+
                    "ProjectTypeKey='"+data.ProjectType+"',"+
                    "EventTypeKey='"+data.LearnerType+"',"+
                    "EventType2='" + data.EventType + "'," +
                    "HoursTraining='" +data.TrainingHours+"',"+
                    "EvaluationQuestionId='"+data.EvaTemplate+"',"+
                    "CertificateId='"+data.CertTemplate+"',"+
                    "InstitutionLogo= @logo,"+
                    "LogoName='"+logoName+"',"+
                    "LogoType='"+logoType+"',"+
                    "Institution='"+data.Institution+"' "+
                    "where CourseId = '"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                sqlCommand.Parameters.Add(new MySqlParameter("@logo", imageBytes));
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }

            //string description = "Update data Courses" + "<br/>" +
            //    "Course Id = "+ id + "<br/>" +
            //    "Partner Type = "+ data.PartnerType + "<br/>" +
            //    "Course Range Date = "+ data.StartDate + " to "+ data.EndDate + " <br/>" +
            //    "Course Name = "+ data.CourseTitle;

            //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
            sb = new StringBuilder();
            sb.AppendFormat("select * from Courses where CourseId = '" + id + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Courses, data.UserId.ToString(), data.cuid.ToString(), " where CourseId = " + id + "");

            res =
                "{"+
                "\"code\":\"1\","+
                "\"message\":\"Update Data Success\""+
                "}"; 
 
            return Ok(res);  
        }

        [HttpPut("CourseTeachingLevel/{id}")]
        public IActionResult UpdateCTL(string id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "UPDATE CourseTeachingLevel SET "+
                    "`Update`="+data.Update+","+
                    "Essentials="+data.Essentials+","+
                    "Intermediate="+data.Intermediate+","+
                    "Advanced="+data.Advanced+","+
                    "Customized="+data.Customized+","+
                    "Other="+data.Other+","+
                    "`Comment`='"+data.Comment+"',"+
                    "`Status`='"+data.Status+"'"+

                    " where CourseId = '"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                //Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from CourseTeachingLevel where CourseId = '"+id+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            //} 
 
            return Ok(res);  
        }

        [HttpPut("CourseSoftware/{id}")]
        public IActionResult UpdateCourseSoftware(string id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "UPDATE CourseSoftware SET "+
                    "productId='"+data.Product1+"',"+
                    "ProductVersionsId='"+data.Version1+"',"+
                    "productsecondaryId='"+data.Product2+"',"+
                    "ProductVersionsSecondaryId='"+data.Version2+"',"+
                    "`Status`='"+data.Status+"'"+

                    " where CourseId = '"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                //Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from CourseSoftware where CourseId = '"+id+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            //} 
 
            return Ok(res);  
        }

        [HttpPut("CourseTrainingFormat/{id}")]
        public IActionResult UpdateCTF(string id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "UPDATE CourseTrainingFormat SET "+
                    "InstructorLed="+data.InstructorLed+","+
                    "`Online`="+data.Online+","+
                    "`Status`='"+data.Status+"'"+

                    " where CourseId = '"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                //Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from CourseTrainingFormat where CourseId = '"+id+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            //} 
 
            return Ok(res);  
        }

        [HttpPut("CourseTrainingMaterial/{id}")]
        public IActionResult UpdateCTM(string id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "UPDATE CourseTrainingMaterial SET "+
                    "Autodesk="+data.Autodesk+","+
                    "AAP="+data.AAP+","+
                    "ATC="+data.ATC+","+
                    "Independent="+data.Independent+","+
                    "IndependentOnline="+data.IndependentOnline+","+
                    "ATCOnline="+data.ATCOnline+","+
                    "Other="+data.Other5+","+
                    "`Comment`='"+data.Komen5+"',"+
                    "`Status`='"+data.Status+"'"+

                    " where CourseId = '"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                //Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from CourseTrainingMaterial where CourseId = '"+id+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            //} 
 
            return Ok(res);  
        }

        [HttpPut("StudentAttendance")]
        // [HttpPut("StudentAttendance/where/{obj}")]
        public async Task<IActionResult> UpdateStudentAttendance([FromBody] dynamic data)
        {  
            // var o1 = JObject.Parse(obj);
            string query = "";
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            query += "UPDATE CourseStudent SET CourseTaken="+data.CourseTaken+" WHERE CourseId = '"+data.CourseId+"' AND StudentID = '"+data.StudentID+"'";

            // int i = 0;
            // JObject json = JObject.FromObject(o1);
            // foreach (JProperty property in json.Properties())
            // { 
            //     if(property.Name.Equals("CourseId")){   
            //         if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
            //             if(i==0){query += " WHERE ";}
            //             if(i>0){query += " AND ";} 
            //             query += "CourseId = '"+property.Value.ToString()+"' "; 
            //             i=i+1;
            //         }
            //     }

            //     if(property.Name.Equals("StudentID")){   
            //         if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
            //             if(i==0){query += " WHERE ";}
            //             if(i>0){query += " AND ";} 
            //             query += "StudentID = '"+property.Value.ToString()+"' "; 
            //             i=i+1;
            //         }
            //     }
            // }
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(query); 
                sqlCommand = new MySqlCommand(sb.ToString());
                //Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // Console.WriteLine("success");
            }
            catch
            {
                // Console.WriteLine("failed");  
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";
            }
            finally
            {  
                string mailto = data.Email;
                string subject = "Autodesk Survey Reminder";
                string message = ""; 

                message += "Hi " + data.FirstName + " " + data.LastName + ",";
                message += "<br/>";
                message += "Congratulations!";
                message += "<br/>";
                message += "One step closer before you finish <b>"+data.CourseTitle+"</b> Course,";
                message += "<br/>";
                message += "Please login into "+ mainURL +"/login-student";
                message += "<br/>";
                message += "Then choose My Course menu and click 'Take Survey' Link";
                message += "<br/>";
                message += "Your answer is very meaningful to us";
                message += "<br/>"; 
                message += "Thank you."; 
                message += "<br/>"; 
                message += "Autodesk Learning Partner Team";
                message += "<br/>"; 
                message += "Please do not reply to this e-mail message. This address is not monitored.";
                
                // Console.WriteLine(data.Email);
                // Console.WriteLine("Autodesk Survey Reminder");
                // Console.WriteLine(message);
                 
                Boolean resmail = await _emailSender.SendMailAsync(mailto, subject, message); 
                
                if(resmail == true){ 
                    // return Ok("{\"code\":\"1\",\"msg\":\"Message sent\"}");   
                    Console.WriteLine("{\"code\":\"1\",\"msg\":\"Message sent\"}");
                }
                else{
                    // return Ok("{\"code\":\"0\",\"msg\":\"Message not sent\"}");  
                    Console.WriteLine("{\"code\":\"0\",\"msg\":\"Message not sent\"}");  
                }
            }

            res =
                "{"+
                "\"code\":\"1\","+
                "\"message\":\"Update Data Success\""+
                "}"; 
        
 
            return Ok(res);  
        }

        [HttpDelete("CourseStudent/where/{obj}")]
        public IActionResult DeleteWhereObj(string obj)
        { 
            var o1 = JObject.Parse(obj);
            string query = "";
            string res = "Delete CourseStudent <br/> ";
            string id = string.Empty;
            string des= 
            
                "{" +
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";

            query += "DELETE FROM CourseStudent ";

            int i = 0;
            JObject json = JObject.FromObject(o1);

            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " WHERE ";
                }
                if(i>0){
                    query += " AND ";
                }

                if(property.Name.Equals("CourseId")){
                    id = property.Value.ToString();
                    query += "CourseId LIKE '%"+ id + "%' ";
                    des += " CourseId : "+ id + " - ";
                   
                }
                if(property.Name.Equals("StudentID")){
                    id = property.Value.ToString();

                    query += "StudentID LIKE '%"+ id + "%' ";
                    des += " StudentID : " + id;

                }

                i =i+1;
            }

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                //Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            {
                var admin = HttpContext.User.GetUserId();
                _auditLogServices.LogHistory(new History { Admin = admin, Date = DateTime.Now, Description = des, Id = id });
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Delete Data Success\""+
                    "}";
            }

            return Ok(res);
        }

        [HttpPost("UploadInstitutionLogo")]
        public async Task UploadLogoInstitution(IFormFile file)
        {
            if (file == null) throw new Exception("File is null");
            if (file.Length == 0) throw new Exception("File is empty");

            var filePath = "../autodesk-angular/src/assets/institution_logo/" + file.FileName;
            var filePathdb = "assets/institution_logo/" + file.FileName;

            Console.WriteLine("file path" + filePath);

            // using (var stream = new FileStream(filePath, FileMode.Create))
            // {
            //     await file.CopyToAsync(stream);
            // }
        }

        // [HttpDelete("DeleteCourse/{id}")]
        // public IActionResult DeleteCourse(string id)
        // {
        //     JObject o3;
        //     string res =
        //         "{" +
        //         "\"code\":\"0\"," +
        //         "\"message\":\"Delete Data Failed\"" +
        //         "}";
        //     try
        //     {
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "DELETE c FROM Courses c " +
        //             "LEFT JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
        //             "LEFT JOIN CourseSoftware cs ON c.CourseId = cs.CourseId " +
        //             "LEFT JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId " +
        //             "LEFT JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId " +
        //             "LEFT JOIN CourseStudent cst ON c.CourseId = cst.CourseId " +
        //             "WHERE c.CourseId = '" + id + "'"
        //             );
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         Console.WriteLine(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         res = "Not found";
        //     }
        //     finally
        //     {
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from Courses where CourseId = '" + id + "';");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);

        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //         o3 = JObject.Parse(result);
        //         // Console.WriteLine(o3.Count);
        //         if (o3.Count < 1)
        //         {
        //             // string description = "Delete data Courses" + "<br/>" +
        //             // "CourseId = "+ id;

        //             // _audit.ActionPost(UserId.ToString(), description, cuid.ToString());


        //             res =
        //                 "{" +
        //                 "\"code\":\"1\"," +
        //                 "\"message\":\"Delete Data Success\"" +
        //                 "}";
        //         }
        //     }

        //     return Ok(res);
        // }

        [HttpDelete("DeleteCourse/{id}/{cuid}/{UserId}")]
        public IActionResult DeleteCourse(string id, string cuid, string UserId)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";

            #region olddata_beforeupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from Courses where CourseId = '" + id + "';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "DELETE FROM Courses WHERE CourseId = '" + id + "';" +
                    "DELETE FROM CourseSoftware WHERE CourseId = '" + id + "';" +
                    "DELETE FROM CourseTeachingLevel WHERE CourseId = '" + id + "';" +
                    "DELETE FROM CourseTrainingFormat WHERE CourseId = '" + id + "';" +
                    "DELETE FROM CourseTrainingMaterial WHERE CourseId = '" + id + "';"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res = "Not found";
            }
            finally
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Courses where CourseId = '" + id + "';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);

                if (o3.Count < 1)
                {
                    //string description = "Delete data Courses" + "<br/>" +
                    //    "Course Id = "+ id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());
                    JObject NewData = new JObject();
                    _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Courses, UserId.ToString(), cuid.ToString(), " where CourseId  =" + id + "");

                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        [HttpPost("SurveyCheck")]
        public IActionResult SurveyCheck([FromBody] dynamic data)
        {
            string StudentID = "";
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT StudentID FROM EvaluationAnswer WHERE CourseId = '" + data.CourseId + "' and StudentID ='" + data.StudentID + "' limit 1;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                StudentID = ds.Tables[0].Rows[0]["StudentID"].ToString();

            }
            catch
            {
                result =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"You can't delete now!\"" +
                    "}";
            }
            finally
            {
                if (!string.IsNullOrEmpty(StudentID))
                {
                    result =
                   "{" +
                   "\"code\":\"0\"," +
                   "\"message\":\"You can not delete a student record who has completed the evaluation survey!\"" +
                   "}";
                }
                else
                {
                    result =
                  "{" +
                  "\"code\":\"1\"," +
                  "\"message\":\"You can delete it!\"" +
                  "}";
                }
               
            }

            return Ok(result);

        }

        [HttpPost("EncrollCheck")]
        public IActionResult EncrollCheck([FromBody] dynamic data)
        {
            string StudentID = "";
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT StudentID FROM CourseStudent WHERE CourseId = '" + data.CourseId + "' and StudentID ='" + data.StudentID + "' limit 1;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                StudentID = ds.Tables[0].Rows[0]["StudentID"].ToString();

            }
            catch
            {
                result =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"You can't answer this survey!\"" +
                    "}";
            }
            finally
            {
                if (!string.IsNullOrEmpty(StudentID))
                {
                    result =
                   "{" +
                   "\"code\":\"1\"," +
                   "\"message\":\"You can answer this survey now!\"" +
                   "}";
                }
                else
                {
                    result =
                  "{" +
                  "\"code\":\"0\"," +
                  "\"message\":\"You can't answer this survey!\"" +
                  "}";
                }

            }

            return Ok(result);

        }
        
    }
}