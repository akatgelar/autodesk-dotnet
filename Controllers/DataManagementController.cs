﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using autodesk.Code;
using autodesk.Code.BackUpDbContext;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using autodesk.Code.ViewModel;
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Office2013.Drawing.ChartStyle;
using Microsoft.AspNetCore.Mvc;
using ObjectsComparer;
using Certificate = autodesk.Code.Models.Certificate;
using LanguagePack = autodesk.Code.Models.LanguagePack;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace autodesk.Controllers
{
 [Route("api/DataManagement")]
 public class DataManagementController : Controller
 {

  private readonly DataServices dataServices;

  private MySQLContext dbContext;

  private readonly backupdbContext backupdbContext;

  private MySqlDb sqlDb;

  private Comparer objectComparer;

  public DataManagementController(MySQLContext _dbContext,MySqlDb _sqlDb, backupdbContext _backupdbContext)
  {
   dbContext = _dbContext;
   sqlDb = _sqlDb;
   this.backupdbContext = _backupdbContext;
   this.dataServices = new DataServices(sqlDb,dbContext);
   this.objectComparer = new Comparer(new ComparisonSettings { UseDefaultIfMemberNotExist = true });
  }

  [HttpGet("GetCertificateChanges")]
  public IActionResult GetCertificateChanges()
  {
   dynamic changes = string.Empty;
   bool isSucceed = false;
   string message = string.Empty;

   try
   {
    changes = dataServices.CertificateChanges();
    isSucceed = true;
    message = "Succeeded";
   }
   catch (Exception e)
   {
    isSucceed = false;
    message = "Oops!, something went wrong while processing your request";
   }
   
   return Ok(new {Code = isSucceed, Model = changes, Message = message});
  }

  [HttpGet("GetEvalQuestionChanges")]
  public IActionResult GetEvalQuestionChanges()
  {
   dynamic changes = string.Empty;
   bool isSucceed = false;
   string message = string.Empty;

   try
   {
    changes = dataServices.EvalQuestionChanges();
    isSucceed = true;
    message = "Succeeded";
   }
   catch (Exception e)
   {
    isSucceed = false;
    message = "Oops!, something went wrong while processing your request";
   }
   
   return Ok(new {Code = isSucceed, Model = changes, Message = message});
  }

  [HttpGet("GetLanguagePackChanges")]
  public IActionResult GetLanguagePackChanges()
  {
   dynamic changes = string.Empty;
   bool isSucceed = false;
   string message = string.Empty;

   try
   {
    changes = dataServices.LanguageChanges();
    isSucceed = true;
    message = "Succeeded";
   }
   catch (Exception e)
   {
    isSucceed = false;
    message = "Oops!, something went wrong while processing your request";
   }
   
   return Ok(new {Code = isSucceed, Model = changes, Message = message});
  }

  [HttpGet("GetDataManagementTables/{ContactID}")]
  public IActionResult GetDataManagementTables(string ContactID)
  {
   List<DictionaryViewModel> dataList = new List<DictionaryViewModel>();
   bool isSucceed = false;
   string message = string.Empty;

   try
   {
    dataList = dataServices.GetListFromDictionaryByParent(AutodeskConst.DataManagement.DataManagementKey);
    isSucceed = true;
    message = "Succeeded";
   }
   catch (Exception e)
   {
    isSucceed = false;
    message = "Oops!, something went wrong while processing your request";
   }
   
   return Ok(new {Code = isSucceed, Model = dataList, Message = message});
  }

  [HttpPut("RollBackData/{ContactID}/{ID}/{TableType}")]
  public IActionResult RollBackData(string ContactID, string ID, string TableType)
  {
   var isSucceed = false;
   var message = string.Empty;

   if (!string.IsNullOrEmpty(ContactID) && !string.IsNullOrEmpty(ID) && !string.IsNullOrEmpty(TableType))
   {
    if (TableType.Equals("Certificate"))
    {
     var backUpDB = backupdbContext.Certificate.SingleOrDefault(x => x.CertificateId == Convert.ToInt32(ID));
     var liveDB = dbContext.Certificate.SingleOrDefault(x => x.CertificateId == Convert.ToInt32(ID));

     liveDB = new Certificate
     {
      Name = backUpDB.Name,
      Code = backUpDB.Code,
      Status = backUpDB.Status,
      Year = backUpDB.Year,
      CertificateTypeId = backUpDB.CertificateTypeId,
      CertificateBackgroundId = backUpDB.CertificateBackgroundId,
      Htmldesign = backUpDB.Htmldesign,
      LanguageCertificate = backUpDB.LanguageCertificate,
      PartnerTypeId = backUpDB.PartnerTypeId,
      TitleCertificateNo = backUpDB.TitleCertificateNo,
      TitleCourseTitle = backUpDB.TitleCourseTitle,
      TitleDate = backUpDB.TitleDate,
      TitleDuration = backUpDB.TitleDuration,
      TitleEvent = backUpDB.TitleEvent,
      TitleHeaderDescription = backUpDB.TitleHeaderDescription,
      TitleInstitution = backUpDB.TitleInstitution,
      TitleInstructor = backUpDB.TitleInstructor,
      TitleLocation = backUpDB.TitleLocation,
      TitlePartner = backUpDB.TitlePartner,
      TitleProduct = backUpDB.TitleProduct,
      TitleStudentName = backUpDB.TitleStudentName,
      TitleTextHeader = backUpDB.TitleTextHeader,
      TitleTextFooter = backUpDB.TitleTextFooter
     };
     try
     {
      dbContext.Update(liveDB);
      dbContext.SaveChanges();
      isSucceed = true;
      message = "Succeed";
     }
     catch (Exception e)
     {
      isSucceed = false;
      message = "Oops!, something went wrong while processing your request!";
     }
    }
    else if (TableType.Equals("Evaluation Question"))
    {
     var backUpDB =
      backupdbContext.EvaluationQuestion.SingleOrDefault(x => x.EvaluationQuestionId == Convert.ToInt32(ID));
     var liveDB = dbContext.EvaluationQuestion.SingleOrDefault(x => x.EvaluationQuestionId == Convert.ToInt32(ID));    
     liveDB.CountryCode = backUpDB.CountryCode;
     liveDB.CourseId = backUpDB.CourseId;
     liveDB.CertificateTypeId = backUpDB.CertificateTypeId;
     liveDB.CreatedBy = backUpDB.CreatedBy;
     liveDB.CreatedDate = backUpDB.CreatedDate;
     liveDB.EvaluationQuestionCode = backUpDB.EvaluationQuestionCode;
     liveDB.EvaluationQuestionJson = backUpDB.EvaluationQuestionJson;
     liveDB.EvaluationQuestionTemplate = backUpDB.EvaluationQuestionTemplate;
     liveDB.LanguageId = backUpDB.LanguageId;
     liveDB.PartnerType = backUpDB.PartnerType;
     liveDB.Year = backUpDB.Year;  
     try
     {
      dbContext.Update(liveDB);
      dbContext.SaveChanges();
      isSucceed = true;
      message = "Succeed";
     }
     catch (Exception e)
     {
      isSucceed = false;
      message = "Oops!, something went wrong while processing your request!";
     }
    }
    else if (TableType.Equals("Languages"))
    {
     var backUpDB = backupdbContext.LanguagePack.SingleOrDefault(x => x.LanguagePackId == Convert.ToInt32(ID));
     var liveDB = dbContext.LanguagePack.SingleOrDefault(x => x.LanguagePackId == Convert.ToInt32(ID));

     liveDB = new LanguagePack
     {
      Status = backUpDB.Status,
      CreatedDate = backUpDB.CreatedDate,
      CreatedBy = backUpDB.CreatedBy,
      LanguageCountry = backUpDB.LanguageCountry,
      LanguageJson = backUpDB.LanguageJson
     };
     try
     {
      dbContext.Update(liveDB);
      dbContext.SaveChanges();
      isSucceed = true;
      message = "Succeed";
     }
     catch (Exception e)
     {
      isSucceed = false;
      message = "Oops!, something went wrong while processing your request!";
     }
    }
   }
   return Ok(new {code = isSucceed, Message = message});
   }

 }
}
