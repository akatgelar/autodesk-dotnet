using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;
 

namespace autodesk.Controllers
{
    [Route("api/Roles")]
    public class RolesController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit;//= new Audit();

        public RolesController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            _audit=new Audit(oDb);
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Roles ORDER BY RoleName ASC");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("Status")]
        public IActionResult SelectStatus()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Dictionary where Parent = 'SiteStatus' and Status = 'A' order by KeyValue");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("CoursePartner")]
        public IActionResult GetCoursePartner()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select RoleId,RoleCode,RoleName from Roles where RoleId in (1,58)");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CheckCode/{code}")]
        public IActionResult CheckCode(string code)
        {
            Console.WriteLine("Test");
            try
            {
                sb = new StringBuilder();
                sb.Append("select RoleCode from Roles where RoleCode = '"+code+"'");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("GetJobRole")]
        public IActionResult JobRole()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select RoleId, RoleName from Roles where RoleType = 'Contact'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("WithoutStatus/{code}")]
        public IActionResult WithoutStatus(string code)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Dictionary where Parent = 'SiteStatus' and Status = 'A' and not `Key` = '"+code+"' order by KeyValue ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("SiteRoles/{id}")]
        public IActionResult SelectSiteRoles(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append ("select ActSiteRole.*, SiteRoles.*, RoleName, KeyValue, ParamValue  From (select RoleId, max(SiteRoleId) maxid from SiteRoles where SiteId =  '" + id + "' group by RoleId)  ActSiteRole inner join " +
                " SiteRoles on ActSiteRole.maxid = SiteRoles.SiteRoleId " +
                " join Roles on SiteRoles.RoleId = Roles.RoleId " +
                " inner join RoleParams on SiteRoles.RoleParamId = RoleParams.RoleParamId " +
                " inner join Dictionary on SiteRoles.`Status` = Dictionary.`Key` and Dictionary.Parent = 'SiteStatus'" );
              
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("ListWithNot/{id}")]
        public IActionResult ListWithNot(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Roles where RoleType = 'Company' and not RoleId in (select RoleId from SiteRoles where SiteId = '"+id+ "' and status <> 'X' ) ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("NotShowAddedPartnerType/{id}")]
        public IActionResult NotShowAddedPartnerType(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Roles where RoleType = 'Company' and (not RoleId in (select RoleId from SiteRoles where   SiteId = (select SiteId from SiteRoles where SiteRoleId = '" + id + "' ) and status <> 'X') or RoleId  = (select RoleId from SiteRoles where SiteRoleId ='" + id +"'))");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }




        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Roles where RoleId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 
        

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from Roles ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("RoleType")){   
                    query += "RoleType = '"+property.Value.ToString()+"' "; 
                }

                if(property.Name.Equals("RoleTypeJustATCAAP")){   
                    query += "RoleType = '"+property.Value.ToString()+"' and RoleId in ('1','58') "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                if (ds.Tables.Count > 0)
                {
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                }
                else
                {
                    result = "[]";
                }
               
            }
            return Ok(result);  
        }

        [HttpGet("wheresub/{obj}")]
        public IActionResult WhereSubObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select RoleParamId as RoleId ,ParamValue as RoleName from RoleParams ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("RoleCode"))
                {
                    query += "RoleCode = '" + property.Value.ToString() + "' ";
                }

                if (property.Name.Equals("RoleTypeJustATCAAP"))
                {
                    query += "RoleCode = '" + property.Value.ToString() + "' and RoleId in ('1','58') ";
                }

                i = i + 1;
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                if (ds.Tables.Count > 0)
                {
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                }
                else
                {
                    result = "[]";
                }

            }
            return Ok(result);
        }

        [HttpGet("whererolesub/{obj}")]
        public IActionResult WhereRoleSub(string obj)
        {
            obj = obj.ToString().Replace("\"", "");
            
            var o1 = JObject.Parse(obj);
            
            string query = "";

            query += "select Distinct ParamValue as RoleName from RoleParams  rp inner join Roles r on r.RoleCode =rp.RoleCode  ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("RoleCode"))
                {
                    property.Value = property.Value.ToString().Replace("\'", "");
                    query += "RoleId in (" + property.Value.ToString() + " )";
                }

                if (property.Name.Equals("RoleTypeJustATCAAP"))
                {
                    property.Value = property.Value.ToString().Replace("\'", "");
                    query += "RoleId in (" + property.Value.ToString() + ") and RoleId in ('1','58') ";
                }

                i = i + 1;
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                if (ds.Tables.Count > 0)
                {
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                }
                else
                {
                    result = "[]";
                }

            }
            return Ok(result);
        }

        [HttpGet("ReportEva")]
        public IActionResult ReportEva()
        {     
            string query = "select * from Roles  where RoleType = 'Company' and RoleId in ('1','58')";
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }
        
        

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Roles;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Roles "+
                    //"( RoleId, RoleCode, RoleName, SiebelName, FrameworkName, RoleType, RoleSubType, Description) "+
                    "( RoleCode, RoleName, SiebelName, FrameworkName, RoleType, RoleSubType, Description) " +
                     //"values ( '" +data.RoleId+"','"+data.RoleCode+"','"+data.RoleName+"','"+data.SiebelName+"','"+data.FrameworkName+"','"+data.RoleType+"','"+data.RoleSubType+"','"+data.Description+"');"
                     "values ( '" + data.RoleCode + "','" + data.RoleName + "','" + data.SiebelName + "','" + data.FrameworkName + "','" + data.RoleType + "','" + data.RoleSubType + "','" + data.Description + "');"
                     );

            Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Roles;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_region ORDER BY region_id DESC limit 1; ");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            if (count2 > count1)
            { 
                //string description = "Insert data Partner Type" + "<br/>" +
                //    "Partner Type Code = "+ data.RoleCode + "<br/>" +
                //    "Partner Type Name = "+ data.RoleName;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Roles, data.UserId.ToString(), data.cuid.ToString(), "where Partner Type Code =" + data.RoleCode + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }
            } 
 
            return Ok(res);  
            
        }


        

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Roles where RoleId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Roles set "+ 
                    "RoleCode='"+data.RoleCode+"', "+
                    "RoleName='"+data.RoleName+"', "+
                    "SiebelName='"+data.SiebelName+"', "+
                    "FrameworkName='"+data.FrameworkName+"', "+
                    "CRBApprovedName='"+data.CRBApprovedName+"', "+
                    "RoleType='"+data.RoleType+"', "+
                    "RoleSubType='"+data.RoleSubType+"', "+
                    "Description='"+data.Description+"' "+ 
                    "where RoleId='"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                Console.WriteLine(data.SubPartnerType.Count);
                Console.WriteLine(data.RoleCertificate.Count);

                for(int i=0; i<data.SubPartnerType.Count;i++){
                    if(data.SubPartnerType[i][1] == "insert"){
                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "insert into RoleParams "+
                            "(RoleCode, ParamName, ParamValue, Status)"+
                            "values ('"+data.RoleCode+"','SubType', '"+data.SubPartnerType[i][0]+"', 'A');"
                        );
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }
                    else if(data.SubPartnerType[i][1] == "delete"){
                        sb = new StringBuilder();
                        sb.AppendFormat("delete from RoleParams where RoleParamId = '"+data.SubPartnerType[i][2]+"'");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }
                }

                for(int i=0; i<data.RoleCertificate.Count;i++){
                    // if(data.RoleCertificate[i][1] == "insert"){
                    //     sb = new StringBuilder();
                    //     sb.AppendFormat(
                    //         "insert into RoleCertificate "+
                    //         "(RoleCode, CertificateName, CertificateType, Status)"+
                    //         "values ('"+data.RoleCode+"','CertificateType', '"+data.RoleCertificate[i][0]+"', 'A');"
                    //     );
                    //     sqlCommand = new MySqlCommand(sb.ToString());
                    //     ds = oDb.getDataSetFromSP(sqlCommand);
                    // }
                    // else if(data.RoleCertificate[i][1] == "delete"){
                    //     sb = new StringBuilder();
                    //     sb.AppendFormat("delete from RoleCertificate where RoleCertificateId = '"+data.RoleCertificate[i][2]+"'");
                    //     sqlCommand = new MySqlCommand(sb.ToString());
                    //     ds = oDb.getDataSetFromSP(sqlCommand);
                    // }

                    if(data.RoleCertificate[i][1] == "insert"){
                        sb = new StringBuilder();
                        sb.AppendFormat("update Dictionary set Status = 'A' where KeyValue = '"+data.RoleCertificate[i][0]+"' and Parent = '"+data.RoleCode+"CertificateType'");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }
                    else if(data.RoleCertificate[i][1] == "delete"){
                        sb = new StringBuilder();
                        sb.AppendFormat("update Dictionary set Status = 'X' where KeyValue = '"+data.RoleCertificate[i][0]+"' and Parent = '"+data.RoleCode+"CertificateType'");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }
                }
            }
            catch
            {
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}"; 
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from Roles where RoleId ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            if (o3["RoleCode"].ToString() == data.RoleCode.ToString())
            { 
                //string descriptionU = "Update data Partner Type" + "<br/>" +
                //    "Partner Type Code = "+ data.RoleCode + "<br/>" +
                //    "Partner Type Name = "+ data.RoleName;

                //_audit.ActionPost(data.UserId.ToString(), descriptionU, data.cuid.ToString());
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Roles, data.UserId.ToString(), data.cuid.ToString(), "where RoleId =" + id + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }
            } 
 
            return Ok(res);  
        } 

        
        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Roles where RoleId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from RoleParams where RoleCode in (select RoleCode from Roles where RoleId = '"+id+"')");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("delete from Roles where RoleId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from Roles where RoleId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){

                    //string description = "Delete data Partner Type" + "<br/>" +
                    //"Partner Type ID = "+ id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());
                    JObject NewData = new JObject();
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Roles, UserId.ToString(), cuid.ToString(), "where RoleId =" + id + ""))
                    {
                        res =
                         "{" +
                         "\"code\":\"1\"," +
                         "\"message\":\"Delete Data Success\"" +
                         "}";
                    }
                }
            }

            return Ok(res);
        } 

         


    }
}