using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;
 

namespace autodesk.Controllers
{
    [Route("api/RoleSubType")]
    public class RoleSubType : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit;//= new Audit();

        public RoleSubType(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            _audit=new Audit(oDb);

        }
        // [HttpGet]
        // public IActionResult Select()
        // {
        //     try
        //     {
        //         sb = new StringBuilder();
        //         sb.Append("select * from RoleSubType ");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "{}";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
        //     }

        //     return Ok(result);                  
        // }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append(
                    "select RoleParamId, RoleParams.RoleCode, RoleName, ParamValue from RoleParams "+
                    "join Roles on RoleParams.RoleCode = Roles.RoleCode "+
                    "where RoleParams.`Status` = 'A'"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

         

        // [HttpGet("{id}")]
        // public IActionResult WhereId(int id)
        // { 
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from RoleSubType where RoleSubTypeId = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "{}";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // } 

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "select RoleParamId, RoleParams.RoleCode, RoleName, ParamValue from RoleParams "+
                    "join Roles on RoleParams.RoleCode = Roles.RoleCode "+
                    "where RoleParams.`Status` = 'A' and RoleParamId = '"+id+"'"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 
        
        

        // [HttpGet("where/{obj}")]
        // public IActionResult WhereObj(string obj)
        // {     
        //     var o1 = JObject.Parse(obj);
        //     string query = ""; 

        //     query += "select * from RoleSubType ";
        //     int i = 0;
        //     JObject json = JObject.FromObject(o1);
        //     foreach (JProperty property in json.Properties())
        //     { 
        //         if(i==0){
        //             query += " where ";
        //         }
        //         if(i>0){
        //             query += " and ";
        //         }

        //         if(property.Name.Equals("RoleSubType")){   
        //             query += "RoleSubType = '"+property.Value.ToString()+"' "; 
        //         }

        //         i=i+1;
        //     } 
 
        //     try
        //     {  
        //         sb.AppendFormat(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "[]";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
        //     }
        //     return Ok(result);  
        // }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 
            int i = 0;
            JObject json = JObject.FromObject(o1);

            query +=
                "select RoleParamId, RoleParams.RoleCode, RoleName, ParamValue from RoleParams "+
                "join Roles on RoleParams.RoleCode = Roles.RoleCode ";
                
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("RoleCode")){   
                    query += "RoleParams.RoleCode = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 

            if(i>0){
                query += "and RoleParams.`Status` = 'A'";
            }
            else{
                query += "where RoleParams.`Status` = 'A'";
            }
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }
        
        

        // [HttpPost]
        // public IActionResult Insert([FromBody] dynamic data)
        // {    
            
        //     int count1 = 0;
        //     int count2 = 0;
        //     JObject o1;
        //     JObject o2;
        //     // JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Insert Data Failed\""+
        //         "}";  
             
        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from RoleSubType;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);
         
        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o1 = JObject.Parse(result); 
        //     count1 = Int32.Parse(o1["count"].ToString());
        //     Console.WriteLine(count1); 
            
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "insert into RoleSubType "+
        //             "( RoleSubTypeId, RoleSubType) "+
        //             "values ( '"+data.RoleSubTypeId+"','"+data.RoleSubType+"');"
        //         );

        //     Console.WriteLine(sb.ToString());
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //     }
        //     finally
        //     { 
        //     }

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from RoleSubType;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);   

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o2 = JObject.Parse(result); 
        //     count2 = Int32.Parse(o2["count"].ToString());
        //     Console.WriteLine(count2);
            
        //     if(count2 > count1)
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Insert Data Success\""+
        //             "}";  
        //     } 
 
        //     return Ok(res);  
            
        // }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from RoleParams;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into RoleParams "+
                    "( RoleCode, ParamValue, ParamName, Status) "+
                    "values ( '"+data.PartnerType+"','"+data.RoleSubType+"', 'SubType', 'A');"
                );

                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from RoleParams;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            sb = new StringBuilder();
            sb.AppendFormat("select * from RoleParams ORDER BY RoleParamId DESC limit 1; ");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            if (count2 > count1)
            { 
                //string description = "Insert data Sub Partner Type" + "<br/>" +
                //    "Partner Type = "+ data.PartnerType + "<br/>" +
                //    "Sub Partner Type Name = "+ data.RoleSubType;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.RoleParams, data.UserId.ToString(), data.cuid.ToString(), "where RoleCode =" + data.PartnerType + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }
            } 
 
            return Ok(res);  
            
        }


        

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, [FromBody] dynamic data)
        // {  
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Update Data Failed\""+
        //         "}";  
                
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update RoleSubType set "+  
        //             "RoleSubType='"+data.RoleSubType+"' "+ 
        //             "where RoleSubTypeId='"+id+"'"
        //         ); 
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //         Console.WriteLine("success");
        //     }
        //     catch
        //     {
        //         Console.WriteLine("failed");  
        //     }
        //     finally
        //     {  
        //     }
 
        //     sb = new StringBuilder();
        //     sb.AppendFormat("select * from RoleSubType where RoleSubTypeId ="+id+";");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);
        
        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o3 = JObject.Parse(result);  

        //     if(o3["RoleSubType"].ToString() == data.RoleSubType.ToString())
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Update Data Success\""+
        //             "}"; 
        //     } 
 
        //     return Ok(res);  
        // } 

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            JObject NewData = new JObject();
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from RoleParams where RoleParamId='" + id + "'; ");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update RoleParams set "+  
                    "RoleCode='"+data.PartnerType+"', "+ 
                    "ParamValue='"+data.RoleSubType+"' "+ 
                    "where RoleParamId='"+id+"'"
                ); 
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("select * from RoleParams where RoleParamId='"+ id +"'; ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            }
            catch
            {
            }
            finally
            {  
            }

            //string descriptionU = "Update data Sub Partner Type" + "<br/>" +
            //        "Partner Type = "+ data.PartnerType + "<br/>" +
            //        "Sub Partner Type Name = "+ data.RoleSubType;

            //_audit.ActionPost(data.UserId.ToString(), descriptionU, data.cuid.ToString());
            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.RoleParams, data.UserId.ToString(), data.cuid.ToString(), "where RoleParamId =" + id + ""))
            {
                res =
                 "{" +
                 "\"code\":\"1\"," +
                 "\"message\":\"Update Data Success\"" +
                 "}";
            }
 
            return Ok(res);  
        } 

        
        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from RoleParams where RoleParamId='" + id + "'; ");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from RoleParams where RoleParamId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from RoleParams where RoleParamId  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){

                    //string description = "Delete data Sub Partner Type" + "<br/>" +
                    //"Sub Partner Type ID = "+ id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());

                    JObject NewData = new JObject();
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.RoleParams, UserId.ToString(), cuid.ToString(), "where RoleParamId =" + id + ""))
                    {
                        res =
                         "{" +
                         "\"code\":\"1\"," +
                         "\"message\":\"Update Data Success\"" +
                         "}";
                    }

                    res =
                        "{"+
                        "\"code\":\"1\","+
                        "\"message\":\"Delete Data Success\""+
                        "}"; 
                }
            }

            return Ok(res);
        } 

         


    }
}