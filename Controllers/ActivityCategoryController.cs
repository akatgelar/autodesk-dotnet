using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;
 

namespace autodesk.Controllers
{
    [Route("api/ActivityCategory")]
    public class ActivityCategoryController : Controller
    {
        StringBuilder sb = new StringBuilder();
       private MySqlDb oDb ;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        public ActivityCategoryController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select distinct ActivityType from JournalActivities where ActivityType <> 'Site Accreditation' ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }
 

        [HttpGet("{ActivityType}")]
        public IActionResult WhereId(string ActivityType)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from JournalActivities where ActivityType = '"+ActivityType+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 

         

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from Activity_category ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("activity_category")){   
                    query += "activity_category = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);    
            }
            return Ok(result);  
        }



        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Activity_category;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Activity_category "+
                    "(activity_category_id, activity_category, cuid, cdate, muid, mdate) "+
                    "values ('"+data.activity_category_id+"','"+data.activity_category+"','"+data.cuid+"','"+data.cdate+"','"+data.muid+"','"+data.mdate+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Activity_category;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
            
        }

 
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Activity_category set "+
                    "activity_category='"+data.activity_category+"', "+
                    "cuid='"+data.cuid+"', "+
                    "cdate='"+data.cdate+"', "+
                    "muid='"+data.muid+"', "+
                    "mdate='"+data.mdate+"' "+ 
                    "where activity_category_id='"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from Activity_category where activity_category_id ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            if(o3["activity_category"].ToString() == data.activity_category.ToString())
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            } 
 
            return Ok(res);  
        } 

         
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";  
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from Activity_category where activity_category_id = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from Activity_category where activity_category_id  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){ 
                    res =
                        "{"+
                        "\"code\":\"1\","+
                        "\"message\":\"Delete Data Success\""+
                        "}"; 
                }
            }

            return Ok(res);
        } 

        [HttpGet("Unique/{obj}")]
        public IActionResult WhereObjUnique(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 
            Console.WriteLine("Tes");
            query += "select activity_name, count(*) from Activity ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("ActivityName")){   
                    query += "activity_name = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
            query += "group by activity_name having count(*) > 1";
            Console.WriteLine(query);

            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

    }
}