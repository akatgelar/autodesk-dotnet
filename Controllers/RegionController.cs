using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/Region")]
    public class RegionController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit;//= new Audit();


        public RegionController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            _audit=new Audit(oDb);

        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Ref_region ORDER BY region_name ASC ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Ref_region where region_id = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("RegionFilter/{id}/{isDistributor}/{org}")]
        public IActionResult WhereIdGeo(int id, Boolean isDistributor, string org)
        { 
            string query = "";

            //Fix isu no.263
            if(isDistributor == true){
                query =
                "select region_id,region_code,region_name from Ref_region where geo_code in " +
                "(select geo_code from Ref_geo where geo_id = '"+id+"') " +
                "and TerritoryId in (SELECT TerritoryId FROM Main_organization o INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code WHERE OrgId = '" + org + "')";
            } else{
                query = 
                "select region_id,region_code,region_name from Ref_region where geo_code in "+
                "(select geo_code from Ref_geo where geo_id = '"+id+"')";
            }

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }
        
        [HttpGet("RegionFilter/{id}")]
        public IActionResult RegionFilterByGeo(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Ref_region  where geo_code = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand); 
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("filter/{id}")]
        public IActionResult WhereIdGeo(string id)
        { 
            try
            {  
                if(id!=""){
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Ref_region  where geo_code in ( "+id+" );");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand); 
                }
                else{
                    result = "";
                }
                
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from Ref_region ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("geo_code")){   
                    query += "geo_code = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpGet("TerritoryRef/{obj}")]
        public IActionResult WhereObjTerritory(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from Ref_region ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(property.Name.Equals("RegionCode")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "region_code like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                }
            } 
            query += "group by territory_code ";
            try
            {  
                sb.AppendFormat(query);
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_region;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Ref_region "+
                    "(geo_code, region_code, region_name, cuid, cdate) "+
                    "values ('"+data.geo_code+"','"+data.region_code+"','"+data.region_name+"','"+data.cuid+"',NOW());"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_region;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_region ORDER BY region_id DESC limit 1; ");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            if (count2 > count1)
            { 
                //string description = "Insert data Region" + "<br/>" +
                //    "Geo Code = "+ data.geo_code + "<br/>" +
                //    "Region Code = "+ data.region_code + "<br/>" +
                //    "Region Name = "+ data.region_name;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_region, data.UserId.ToString(), data.cuid.ToString(), "where Region Code =" + data.region_code + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }

            } 
 
            return Ok(res);  
            
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_region where region_id =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Ref_region set "+
                    "geo_code='"+data.geo_code+"',"+
                    // "region_code='"+data.region_code+"',"+
                    "region_name='"+data.region_name+"',"+
                    "muid='"+data.muid+"',"+
                    "mdate=NOW()"+
                    "where region_id='"+id+"'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_region where region_id ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 

            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_region, data.UserId.ToString(), data.cuid.ToString(), "where region_id =" + id + ""))
            {
                res =
                 "{" +
                 "\"code\":\"1\"," +
                 "\"message\":\"Update Data Success\"" +
                 "}";
            }
           
            //} 
 
            return Ok(res);  
        } 



        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_region where region_id  =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from Ref_region where region_id = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from Ref_region where region_id  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){

                    //string description = "Delete data Region" + "<br/>" +
                    //"Region ID = "+ id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());
                    JObject NewData = new JObject();
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_region,UserId.ToString(), cuid.ToString(), "where region_id =" + id + ""))
                    {
                        res =
                         "{" +
                         "\"code\":\"1\"," +
                         "\"message\":\"Delete Data Success\"" +
                         "}";
                    }

                }
            }

            return Ok(res);
        } 


    }
}
