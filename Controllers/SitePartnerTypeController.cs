using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.Models;
// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/SitePartnerType")]
    public class SitePartnerType : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        private Audit _audit ;
        private readonly MySQLContext _mySQLContext;
        public SitePartnerType(MySqlDb sqlDb, MySQLContext mySQLContext)
        {
            oDb = sqlDb;
            _audit = new Audit(oDb);
            _mySQLContext = mySQLContext;
        }

        // [HttpGet]
        // public IActionResult Select()
        // {
        //     try
        //     {
        //         sb = new StringBuilder();
        //         sb.Append("select * from SiteRoles ");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not Found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
        //     }

        //     return Ok(result);                  
        // }



        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select SiteIdInt, SiteRoles.* from SiteRoles join Main_site on SiteRoles.SiteId = Main_site.SiteId where SiteRoleId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 
        
        

        // [HttpGet("where/{obj}")]
        // public IActionResult WhereObj(string obj)
        // {     
        //     var o1 = JObject.Parse(obj);
        //     string query = ""; 

        //     query += "select * from RoleSubType ";
        //     int i = 0;
        //     JObject json = JObject.FromObject(o1);
        //     foreach (JProperty property in json.Properties())
        //     { 
        //         if(i==0){
        //             query += " where ";
        //         }
        //         if(i>0){
        //             query += " and ";
        //         }

        //         if(property.Name.Equals("RoleSubType")){   
        //             query += "RoleSubType = '"+property.Value.ToString()+"' "; 
        //         }

        //         i=i+1;
        //     } 
 
        //     try
        //     {  
        //         sb.AppendFormat(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "[]";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
        //     }
        //     return Ok(result);  
        // }
        
        

        [HttpPost]
        public IActionResult InsertPartnerType([FromBody] dynamic data)
        {    
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            JObject o1;
            JObject o2;
            JObject roles;
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from SiteRoles;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            count3 = count1 + 1;
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into SiteRoles "+
                    "(SiteId, RoleId, DateAdded, Status, CSN, ExternalId)"+
                    "values ('"+data.SiteId+"','"+data.PartnerType+"', NOW(), '"+data.Status+"', '"+data.CSN+"','"+data.ExternalId+"');"
                );
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                //Check RoleCode From Roles Table
                sb = new StringBuilder();
                sb.AppendFormat("select RoleCode from Roles where RoleId = "+data.PartnerType+"");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                roles = JObject.Parse(result);

                if(roles["RoleCode"].ToString() == "MED" || roles["RoleCode"].ToString() == "RED"){
                    sb = new StringBuilder();
                    sb.AppendFormat("insert into SiteCountryDistributor (SiteID,CountryCode,EDistributorType,PrimaryD) values ('"+data.SiteId+"',"+
                        "(select SiteCountryCode as CountryCode from Main_site where SiteId = '"+data.SiteId+"'),'"+roles["RoleCode"].ToString()+"',1)");
                    // Console.WriteLine(sb.ToString());
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                sb = new StringBuilder();
                sb.AppendFormat("update Main_site set LastAdminBy='" + data.cuid.ToString() + "' where SiteId='" + data.SiteId + "';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Insert Data Failed\""+
                    "}"; 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from SiteRoles;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            
            if(count2 > count1)
            { 

                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM SiteRoles sr INNER JOIN Roles r ON sr.RoleId = r.RoleId WHERE SiteRoleId in (SELECT MAX(SiteRoleId) FROM SiteRoles) LIMIT 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
               string restmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject resobj = JObject.Parse(restmp);

                //string description = "Insert data Partner Type <br/>" +
                //        "SiteId = "+ data.SiteId +" <br/>" +
                //        "Status = "+ data.Status +" <br/>" +
                //        "Partner Type = "+ resobj["RoleCode"].ToString();

                JObject OldData = new JObject();
               
                _audit.AuditLog(OldData, resobj, AutodeskConst.AuditInfoEnum.SiteRoles, data.UserId.ToString(), data.cuid.ToString(), " where SiteRoleId = " + resobj["SiteRoleId"] + "");
              //  _audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
            
        }


        

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            #region olddata_beforeupdate

            sb = new StringBuilder();
            sb.AppendFormat("SELECT * FROM SiteRoles sr INNER JOIN Roles r ON sr.RoleId = r.RoleId WHERE SiteRoleId = '" + id + "' LIMIT 1");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            #endregion
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update SiteRoles set "+ 
                    "RoleId='"+data.PartnerType+"', "+ 
                    "Status='"+data.Status+"', "+ 
                    "CSN='"+data.CSN+"', "+  
                    "ExternalId='"+data.ExternalId+"' "+ 
                    "where SiteRoleId='"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
               
            }
            catch
            {
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            {
                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM SiteRoles sr INNER JOIN Roles r ON sr.RoleId = r.RoleId WHERE SiteRoleId = '"+id+"' LIMIT 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);   

                string restmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject resobj = JObject.Parse(restmp);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(restmp.ToString());
                sb = new StringBuilder();
                sb.AppendFormat("update Main_site set LastAdminBy='" + data.cuid.ToString() + "' where SiteId='" + resobj["SiteId"]+"';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                //string description = "Update data Partner Type <br/>" +
                //        "SiteId = "+ resobj["SiteId"] +" <br/>" +
                //        "Status = "+ resobj["Status"] +" <br/>" +
                //        "Partner Type = "+ resobj["RoleCode"].ToString();

                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.SiteRoles, data.UserId.ToString(), data.cuid.ToString(), " where SiteRoleId = " + id  + "");

                //   _audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());


                #region Update Status for Main_Site
                UpdateMainSiteStatus(resobj["SiteId"].ToString());

                sb = new StringBuilder();
                sb.AppendFormat("SELECT OrgId FROM Main_site WHERE SiteId = '" + resobj["SiteId"].ToString() + "' LIMIT 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                string result2 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject data2 = JsonConvert.DeserializeObject<JObject>(result2.ToString());

                UpdateOrgStatus(data2["OrgId"].ToString());
                //  UpdateOrgStatus()
                //StringBuilder sb2 = new StringBuilder();
                //sb2.AppendFormat("select * from SiteRoles where status ='A' and SiteId = '" + resobj["SiteId"] + "'");
                //sqlCommand = new MySqlCommand(sb2.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);
                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    data.Status = "A";
                //}


                //sb2 = new StringBuilder();
                //sb2.AppendFormat(
                //    "update Main_site set " +
                //    "Status='" + data.Status + "'" +
                //    "where SiteId='" + resobj["SiteId"] + "'"
                //);
                //sqlCommand = new MySqlCommand(sb2.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);

                #endregion




                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}";  
            }

            return Ok(res);  
        }

        public void UpdateMainSiteStatus(string SiteId)
        {
            try
            {
                var SiteActiveList = _mySQLContext.SiteRoles.Where(x => x.Status.Equals("A") && x.SiteId.Equals(SiteId)).ToList();

                var tmpSitestatus = "";
                var Sitestatus = "";
                if (SiteActiveList.Count > 0)
                {
                    Sitestatus = "A";
                }
                else
                {
                    var SiteLists = _mySQLContext.SiteRoles.Where(x => x.SiteId.Equals(SiteId)).ToList();

                    for (int j = 0; j < SiteLists.Count; j++)
                    {
                        if (j == 0)
                        {
                            tmpSitestatus = SiteLists[j].Status.ToString();
                        }
                        else
                            tmpSitestatus = tmpSitestatus + "," + SiteLists[j].Status.ToString();
                    }
                    if (!String.IsNullOrEmpty(tmpSitestatus))
                    {
                        string[] orgstatuscount = tmpSitestatus.Split(',');
                        if (orgstatuscount.Length == 1)
                        {
                            Sitestatus = tmpSitestatus;
                        }
                        else if (orgstatuscount.Length > 1 && !tmpSitestatus.Contains("A"))
                        {
                            for (int i = 0; i < orgstatuscount.Length; i++)
                            {
                                if (orgstatuscount[i] != orgstatuscount[0])
                                {
                                    Sitestatus = "I";
                                    break;
                                }
                                else
                                {
                                    Sitestatus = orgstatuscount[0];
                                }
                            }

                        }
                        else
                        {
                            if (tmpSitestatus.Contains("A"))
                            {
                                Sitestatus = "A";
                            }
                            else
                            {
                                Sitestatus = "I";
                            }
                        }
                    }

                }


                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Main_site set " +
                     "Status='" + Sitestatus + "' " +
                      "where SiteId='" + SiteId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch (Exception ex)
            {

            }



        }

        public void UpdateOrgStatus(string OrgId)
        {
            try
            {
                var OrgActiveList = _mySQLContext.MainSite.Where(x => x.Status.Equals("A") && x.OrgId.Equals(OrgId)).ToList();

                var tmpOrgstatus = "";
                var Orgstatus = "";
                if (OrgActiveList.Count > 0)
                {
                    Orgstatus = "A";
                }
                else
                {
                    var OrgLists = _mySQLContext.MainSite.Where(x => x.OrgId.Equals(OrgId)).ToList();

                    for (int j = 0; j < OrgLists.Count; j++)
                    {
                        if (j == 0)
                        {
                            tmpOrgstatus = OrgLists[j].Status.ToString();
                        }
                        else
                            tmpOrgstatus = tmpOrgstatus + "," + OrgLists[j].Status.ToString();
                    }
                    if (!String.IsNullOrEmpty(tmpOrgstatus))
                    {
                        string[] orgstatuscount = tmpOrgstatus.Split(',');
                        if (orgstatuscount.Length == 1)
                        {
                            Orgstatus = tmpOrgstatus;
                        }
                        else if (orgstatuscount.Length > 1 && !tmpOrgstatus.Contains("A"))
                        {
                            for (int i = 0; i < orgstatuscount.Length; i++)
                            {
                                if (orgstatuscount[i] != orgstatuscount[0])
                                {
                                    Orgstatus = "I";
                                    break;
                                }
                                else
                                {
                                    Orgstatus = orgstatuscount[0];
                                }
                            }

                        }
                        else
                        {
                            if (tmpOrgstatus.Contains("A"))
                            {
                                Orgstatus = "A";
                            }
                            else
                            {
                                Orgstatus = "I";
                            }
                        }
                    }

                }


                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Main_organization set " +
                     "Status='" + Orgstatus + "' " +
                      "where OrgId='" + OrgId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch (Exception ex)
            {

            }



        }

        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // { 
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("delete from RoleSubType where RoleSubTypeId = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //     }
        //     finally
        //     { 
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from RoleSubType where RoleSubTypeId  ="+id+";");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);

        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //         o3 = JObject.Parse(result);  
        //         Console.WriteLine(o3.Count);
        //         if(o3.Count < 1){ 
        //             res =
        //                 "{"+
        //                 "\"code\":\"1\","+
        //                 "\"message\":\"Delete Data Success\""+
        //                 "}"; 
        //         }
        //     }

        //     return Ok(res);
        // } 




    }
}