using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/EmailBody")]
    public class EmailBodyController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit;//= new Audit();

        public EmailBodyController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            _audit = new Audit(oDb);

        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT body_id,EmailBodyTypeName as body_type,body_email,e.`Key`,`subject`,d.KeyValue FROM EmailBody e INNER JOIN Dictionary d ON e.`Key`= d.`Key` INNER JOIN EmailBodyType ebt ON e.body_type = ebt.EmailBodyTypeId WHERE d.Parent = 'Languages'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpGet("Type")]
        public IActionResult Type()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT * FROM EmailBodyType");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            JObject o1;
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM EmailBody WHERE body_id = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                result = result.Replace("[","{");
                result = result.Replace("]","}");
            }

            return Ok(result);
        }

        [HttpGet("CheckLanguage/{type}/{key}")]
        public IActionResult CheckLanguage(string type, string key)
        { 
            try
            {  
                if(key == "null"){
                    key = "";
                }

                sb = new StringBuilder();
                sb.AppendFormat("select * from Dictionary d where Parent = 'Languages' AND `Status` = 'A' AND `Key` not in (select `Key` from EmailBody where body_type = '"+type+"' AND `Key` <> '"+key+"')");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("SELECT COUNT(*) AS count FROM EmailBody");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());

            string query = "INSERT INTO EmailBody "+
                    "(body_type, body_email, `subject`, `Key`, AddedBy, DateAdded) "+
                    "VALUES ( '"+data.body_type+"','"+data.body_email+"','"+data.subject+"','"+data.Key+"','"+data.AddedBy+"',NOW());";
                    
            query = query.Replace("{","[");
            query = query.Replace("}","]");

            Console.WriteLine(query);
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Insert Data Failed\""+
                    "}";  
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("SELECT COUNT(*) as count FROM EmailBody;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            
            if(count2 > count1)
            { 
                //string description = "Insert data Email Body" + "<br/>" +
                //    "Body Type = "+ data.body_type + "<br/>" +
                //    "Body Email = "+ data.body_email + "<br/>" +
                //    "Subject = "+ data.subject;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                JObject OldData = new JObject();
                sb = new StringBuilder();
                sb.AppendFormat("select * from EmailBody where body_type ='" + data.body_type + "' and EmailBody.Key ='" + data.Key +"'; ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.EmailBody, data.UserId.ToString(), data.cuid.ToString(), "where Body Type =" + data.body_type + ""))
                {
                    res =
                       "{" +
                       "\"code\":\"1\"," +
                       "\"message\":\"Insert Data Success\"" +
                       "}";
                }
            } 
 
            return Ok(res);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("SELECT * FROM EmailBody WHERE body_id =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                string query = 
                    "UPDATE EmailBody SET "+ 
                    "body_type='"+data.body_type+"', "+
                    "body_email='"+data.body_email+"', "+
                    "`subject`='"+data.subject+"', "+
                    "`Key`='"+data.Key+"', "+
                    "LastAdminBy='"+data.LastAdminBy+"', "+
                    "DateLastAdmin=NOW() "+
                    "WHERE body_id='"+id+"'";
                    
                query = query.Replace("{","[");
                query = query.Replace("}","]");
                
                sb = new StringBuilder();
                sb.AppendFormat(query); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("SELECT * FROM EmailBody WHERE body_id ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            o3 = JObject.Parse(result);  

            //string descriptionU = "Update data Email Body" + "<br/>" +
            //    "Body Type = "+ data.body_type + "<br/>" +
            //    "Body Email = "+ data.body_email + "<br/>" +
            //    "Subject = "+ data.subject;

            //_audit.ActionPost(data.UserId.ToString(), descriptionU, data.cuid.ToString());
            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.EmailBody, data.UserId.ToString(), data.cuid.ToString(), "where body_id =" + id + ""))
            {
                res =
                   "{" +
                   "\"code\":\"1\"," +
                   "\"message\":\"Update Data Success\"" +
                   "}";
            }
           
            return Ok(res);  
        }

        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("SELECT * FROM EmailBody WHERE body_id =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("DELETE FROM EmailBody WHERE body_id = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Delete Data Failed\""+
                    "}"; 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM EmailBody WHERE body_id ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){ 

                    //string description = "Delete data Email Body" + "<br/>" +
                    //"Email Body ID = "+ id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());

                    JObject NewData = new JObject();
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.EmailBody, UserId.ToString(), cuid.ToString(), "where body_id =" + id + ""))
                    {
                        res =
                           "{" +
                           "\"code\":\"1\"," +
                           "\"message\":\"Delete Data Success\"" +
                           "}";
                    }
                   
                }
            }

            return Ok(res);
        }
    }
}