﻿using autodesk.Code;
using autodesk.Code.Models.Dto;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace autodesk.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]

    //[Authorize]
    public class AgreementsController : ControllerBase
    {
        private readonly IAgreementService _agreementService;

        public AgreementsController(IAgreementService agreementService)
        {
            _agreementService = agreementService;
        }
        [HttpPost("GetListMaster")]
        public ResponseModel<List<AgreementDto>> GetListAgreementMaster([FromBody] AgreementSearchDto data)
        {
            if (data != null)
            {
                try
                {
                    //var dtoData = JsonConvert.DeserializeObject<AgreementSearchDto>(data.ToString());
                    var result = _agreementService.GetAgreementMasters(data);
                    if (result != null)
                    {
                        return new ResponseModel<List<AgreementDto>> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = result.AgreementMasters, TotalCount = result.TotalCount };
                    }
                    //await Task.Run(() => _agreementService.GenerateAgreementMaster(data.FY));
                    return new ResponseModel<List<AgreementDto>> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.NotFound, Data = null };
                }
                catch (System.Exception ex)
                {
                    return new ResponseModel<List<AgreementDto>> { Code = (int)HttpStatusCode.InternalServerError, Message = AutodeskConst.ResponseMessage.Error, Data = null };
                }
            }
            return new ResponseModel<List<AgreementDto>> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = null };

        }

        [HttpGet("GetListFY")]
        public async Task<IActionResult> GetListFY()
        {
            var result = await _agreementService.GetListFY();
            if (result?.Count > 0)
            {
                return Ok(result);
            }
            return Ok();
        }
        [HttpGet("GetListPartnerType")]
        public async Task<IActionResult> GetListPartnerType()
        {
            var result = await _agreementService.GetListPartnerType();
            if (result?.Count > 0)
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DefaultContractResolver();

                var res = JsonConvert.SerializeObject(result.Select(x => new { RoleId = x.Item1, RoleName = x.Item2 }).ToList(), Formatting.Indented,
                    settings);

                return Ok(res);
            }
            return Ok();
        }
        [HttpGet("GetListTerritory")]
        public async Task<IActionResult> GetListTerritory()
        {
            var result = await _agreementService.GetListTerritory();
            if (result?.Count > 0)
            {
                return Ok(result);
            }
            return Ok();
        }
        [HttpGet("GetListCountry")]
        public async Task<IActionResult> GetListCountry(int territoryId)
        {
            var result = await _agreementService.GetListCountry(territoryId);
            if (result?.Count > 0)
            {
                return Ok(result);
            }
            return Ok();
        }
        [HttpGet("GetListOrg")]
        public async Task<IActionResult> GetListOrg(int territoryId, string countryCode)
        {
            var result = await _agreementService.GetListOrg(territoryId, countryCode);
            if (result?.Count > 0)
            {
                return Ok(result);
            }
            return Ok();
        }
        [HttpGet("OrganizationInfo")]
        public async Task<ResponseModel<OrganizationInfoDto>> OrganizationInfo(int agreementMasterId)
        {
            try
            {
                var result = await _agreementService.GetOrganizationInfo(agreementMasterId);
                if (result != null)
                {
                    return new ResponseModel<OrganizationInfoDto> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = result };
                }
                return new ResponseModel<OrganizationInfoDto> { Code = (int)HttpStatusCode.NotFound, Message = AutodeskConst.ResponseMessage.NotFound, Data = null };
            }
            catch (System.Exception ex)
            {
                return new ResponseModel<OrganizationInfoDto> { Code = (int)HttpStatusCode.InternalServerError, Message = AutodeskConst.ResponseMessage.Error, Data = null };
            }
        }
        [HttpPost("ExecuteAgreement")]
        public async Task<ResponseModel> ExecuteAgreement([FromBody]AgreementAddDto agreementAddDto)
        {
            var result = await _agreementService.ExecuteAgreement(agreementAddDto);
            if (result > 0)
            {
                return new ResponseModel { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success };
            }
            return new ResponseModel { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail };
        }
        [HttpGet("GetAgreementDetail")]
        public async Task<ResponseModel<AgreementDetailDto>> GetAgreementDetail(int agreementMasterId)
        {
            var result = await _agreementService.GetAgreementDetail(agreementMasterId);
            if (result != null)
            {
                return new ResponseModel<AgreementDetailDto> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = result, TotalCount = 1 };
            }
            return new ResponseModel<AgreementDetailDto> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = null, TotalCount = 0 };

        }
        [HttpPost("RejectAgreement")]
        public async Task<ResponseModel> RejectAgreement([FromBody]AgreementRejectDto agreementRejectDto)
        {
            var result = await _agreementService.RejectAgreement(agreementRejectDto);
            if (result)
            {
                return new ResponseModel { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success };
            }
            return new ResponseModel { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail };
        }

        [HttpPost("ApproveAgreement")]
        public async Task<ResponseModel> ApproveAgreement([FromBody]AgreementApproveDto agreementApproveDto)
        {
            var result = await _agreementService.ApproveAgreement(agreementApproveDto);
            if (result)
            {
                return new ResponseModel { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success };
            }
            return new ResponseModel { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail };

        }

        [HttpGet("GetOrgName")]
        public async Task<IActionResult> GetOrgName(string orgId, string userId)
        {
            var result = await _agreementService.GetOrgName(orgId, userId);
            if (!string.IsNullOrEmpty(result))
            {
                return Ok(new { OrgName = result });
            }
            return Ok(new { OrgName = string.Empty });
        }

    }
}