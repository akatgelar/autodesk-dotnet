using System;
using Microsoft.AspNetCore.Mvc;
// using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using autodesk.Code;
using System.IO;
using MySql.Data.MySqlClient;
using System.Data;
using System.Linq;
using System.Text;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using autodesk.Code.AuditLogServices;
using autodesk.Code.Models.Dto.LanguageDto;
using System.Text.RegularExpressions;

namespace autodesk.Controllers
{
    [Produces("application/json")]
    [Route("api/Language")]
    public class LanguageController : ControllerBase
    {
        String result = "";
        private bool statusCode = false;
        private string msg;
        JObject resultObj;
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        private Audit _audit;
        private IDataServices _dataServices;
        private MySQLContext context;
        private readonly IAuditLogServices _auditLogServices;
        public LanguageController(MySqlDb sqlDb, MySQLContext _context, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _audit = new Audit(oDb);
            context = _context;
            _dataServices = new DataServices(oDb, context);
            _auditLogServices = auditLogServices;

        }

        /*  [HttpGet("LanguageList/{LanguageName}")]
          public IActionResult Change(string LanguageName)
          {
              try
              {
                  LanguagePack languagePack = context.LanguagePack.FirstOrDefault(x => x.LanguageCountry.Equals(LanguageName));
                  if (languagePack != null)
                  {
                      var listPropertiesName = _dataServices.GetThreeNestedStageJsonLabel<LanguageJsonDisplayModel>(languagePack.LanguageJson);
                      if (listPropertiesName.Count > 0)
                      {
                          statusCode = true;
                          result = JsonConvert.SerializeObject(listPropertiesName);
                          msg = "Success";

                      }
                  }
                  else
                  {
                      statusCode = false;
                      result = null;
                      msg = "Sorry We haven't found any records for selected language in our system.";
                  }
              }
              catch (Exception e)
              {
                  statusCode = false;
                  result = null;
                  msg = "Oops!, Something went wrong while processing your request. Please try again later.";
              }



              return Ok(new{code=statusCode,data=result,message=msg});

          }*/
        [HttpGet]
        public IActionResult Select()
        {
            DirectoryInfo d = new DirectoryInfo("Assets/Language");//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles("*.json"); //Getting Text files
            string[] str = new string[Files.Length];
            try
            {
                int index = 0;
                foreach (FileInfo file in Files)
                {
                    str[index] = Path.GetFileNameWithoutExtension(file.Name);
                    index++;
                }
            }
            catch
            {
                str = null;
            }
            finally
            {
            }

            return Ok(str);
        }

        [HttpGet("RoleAccess")]
        public IActionResult GetRoleAccess()
        {
            string result_string;
            try
            {
                //get json file as string
                result_string = System.IO.File.ReadAllText("Assets/RoleAccess/role_access.json");
            }
            catch
            {
                result_string = null;
            }
            finally
            {
            }

            return Ok(result_string);
        }

        // [HttpGet("{language}")]
        // public IActionResult SelectLanguage(string language)
        // {
        //     try
        //     {
        //         //get json file as string
        //         string result_string = System.IO.File.ReadAllText("Assets/Language/"+language+".json");
        //         resultObj = JObject.Parse(result_string);
        //     }
        //     catch
        //     {
        //         resultObj = null;
        //     }
        //     finally
        //     { 
        //     }

        //     return Ok(resultObj);                  
        // }

        [HttpGet("{language}")]
        public IActionResult SelectLanguage(string language)
        {
            var languageJson = "";
            try
            {

                languageJson = context.LanguagePack.Where(x => x.LanguageCountry == language).Select(x => x.LanguageJson).SingleOrDefault();
                /*
                 * Commented by Zwe on 10/1/2019
                 * /
                /*
                sb = new StringBuilder();
                sb.AppendFormat("select LanguageJson from LanguagePack where LanguageCountry = '" + language + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                */
                // if (ds.Tables[0].Rows.Count != 0)
                // {
                //     //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                //     ds = MainOrganizationController.EscapeSpecialChar(ds);
                // }
            }
            catch
            {
                resultObj = null;
            }
            finally
            {
                if (!string.IsNullOrEmpty(languageJson))
                {
                    resultObj = JObject.Parse(languageJson);
                }

            }

            return Ok(resultObj);
        }

        [HttpGet("update/{language}/{value}/{editvalue}")]
        public IActionResult Select(string language, string value, string editvalue)
        {
            try
            {
                //get json file as string
                string json = System.IO.File.ReadAllText("Assets/Language/" + language + ".json");

                //get update string data
                result = json.Replace(value, editvalue);

                //write (update) string file
                System.IO.File.WriteAllText("Assets/Language/" + language + ".json", result);

                //get file as string (after update/write)
                result = System.IO.File.ReadAllText("Assets/Language/" + language + ".json");
            }
            catch
            {
                result = "Error Update";
            }
            finally
            {
            }

            return Ok(result);
        }

        private KeyValuePair<string, string> GetLanguageValue(string value)
        {

            var list = value.Split(':');
            Regex regex = new Regex("[^\\\"]+");
            var matchKey = regex.Match(list[0]);
            var matchValue = regex.Match(list[1]);
            if (matchKey.Success && matchValue.Success)
            {
                return new KeyValuePair<string, string>(matchKey.Groups[0].ToString(), matchValue.Groups[0].ToString());
            }
            return new KeyValuePair<string, string>();
        }

       
        [HttpPut("{language}")]
        public IActionResult Update(string language, [FromBody] dynamic data)
        {
           
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            LanguageEditDto dto = JsonConvert.DeserializeObject<LanguageEditDto>(data.ToString());

            var languageEntity = context.LanguagePack.FirstOrDefault(x => x.LanguageCountry == language);
            if (languageEntity != null)
            {
                var langJson = languageEntity.LanguageJson;
                var value = GetLanguageValue(dto.Value);
                var editValue = GetLanguageValue(dto.EditValue);

                string oldvalue = "" + value.Key + "\": \"" + value.Value + "";
                string newValue = "" + editValue.Key + "\": \"" + editValue.Value + "";
                var contain = langJson.Contains(oldvalue);


                if (contain)
                {
                    string _res = langJson.Replace(oldvalue, newValue);
                    languageEntity.LanguageJson = _res;
                    context.SaveChanges();
                }



            }









            //sb = new StringBuilder();
            //sb.AppendFormat("select * from LanguagePack where LanguageCountry = '" + language + "'");
            //sqlCommand = new MySqlCommand(sb.ToString());
            //ds = oDb.getDataSetFromSP(sqlCommand);
            ////result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //result = JsonConvert.SerializeObject(ds.Tables[0]);
            ////JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //string json;
            //try
            //{
            //    // //get json file as string
            //    // string json = System.IO.File.ReadAllText("Assets/Language/"+language+".json").ToString();

            //    // //get update string data
            //    // result = json.Replace(data.value.ToString(),data.editvalue.ToString());

            //    // //write (update) string file
            //    // System.IO.File.WriteAllText("Assets/Language/"+language+".json", result);

            //    // //get file as string (after update/write)
            //    // result = System.IO.File.ReadAllText("Assets/Language/"+language+".json");

            //    sb = new StringBuilder();
            //    sb.AppendFormat("select LanguageJson from LanguagePack where LanguageCountry = '" + language + "'");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);
            //    json = ds.Tables[0].Rows[0].ItemArray[0].ToString();

            //    result = json.Replace(data.value.ToString(), data.editvalue.ToString());
            //    result = result.Replace("\'", "\\\'");
            //    string sql =
            //        "update LanguagePack set " +
            //            "LanguageJson='" + result + "' " +
            //            "where LanguageCountry='" + language + "'";
            //    sqlCommand = new MySqlCommand(sql);
            //    // Console.WriteLine(sql);
            //    ds = oDb.getDataSetFromSP(sqlCommand);
            //}
            //catch
            //{
            //    res =
            //        "{" +
            //        "\"code\":\"0\"," +
            //        "\"message\":\"Update Data Failed\"" +
            //        "}";
            //}
            //finally
            {

                /* autodesk plan 10 oct - complete all history log */

                //string description = "Update data Language" + "<br/>" +
                //    "Language = " + language + "<br/>" +
                //    "Update Content = " + data.value + " to " + data.editvalue;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                /* end line autodesk plan 10 oct - complete all history log */
                //sb = new StringBuilder();
                //sb.AppendFormat("select * from LanguagePack where LanguageCountry = '" + language + "'");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);
                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                //JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                //if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.LanguagePack, data.UserId.ToString(), data.cuid.ToString(), "where LanguageCountry =" + language + ""))
                //{

                _auditLogServices.LogHistory(new History
                {
                    Admin = HttpContext.User.GetUserId(),
                    Date = DateTime.Now,
                    Description = "Update Language.<br/> Data:" + JsonConvert.SerializeObject(dto),
                });
                res =
                 "{" +
                 "\"code\":\"1\"," +
                 "\"message\":\"Update Data Success\"" +
                 "}";
                //}


            }

            return Ok(res);

        }


        [HttpGet("GetAllLanguagePacks")]
        public IActionResult GetAllLanguagePacks()
        {
            var returnValue = context.LanguagePack.Where(s => s.LanguageJson != null).ToList()
                .Select(s => new
                {
                    s.LanguagePackId,
                    s.LanguageCountry,
                    LanguageJson = JObject.Parse(s.LanguageJson),
                    s.Status,
                    s.CreatedBy,
                    s.CreatedDate
                });

            return Ok(returnValue);
        }

        [HttpPost("SaveAllLanguagePacks")]
        public ResponseModel SaveAllLanguagePacks([FromBody] List<LanguagePackToUpdate> languagePacks)
        {
            languagePacks.ForEach(pack =>
            {
                var languagePackEntity = context.LanguagePack.FirstOrDefault(f => f.LanguagePackId == pack.LanguagePackId);
                languagePackEntity.LanguageJson = pack.LanguageJson;
                context.Update(languagePackEntity);
            });
            context.SaveChanges();

            return new ResponseModel { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success };
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";
            Boolean added = false;

            try
            {
                //create new file
                if (!System.IO.File.Exists("Assets/Language/" + data.Language + ".json"))
                {
                    using (StreamWriter w = System.IO.File.AppendText("Assets/Language/" + data.Language + ".json")) ;

                    //get json file english as default language
                    string englishjson = System.IO.File.ReadAllText("Assets/Language/English.json");

                    //set new json file as eglish json (default)
                    System.IO.File.WriteAllText("Assets/Language/" + data.Language + ".json", englishjson);

                    //get file as string (after update/write)
                    result = System.IO.File.ReadAllText("Assets/Language/" + data.Language + ".json");

                    //set name and flag
                    result = result.Replace("\"language_name\": \"English\"", "\"language_name\": \"" + data.Language + "\"");
                    result = result.Replace("\"language_flag\": \"flag-icon-gb\"", "\"language_flag\": \"" + data.FlagLanguage + "\"");
                    System.IO.File.WriteAllText("Assets/Language/" + data.Language + ".json", result);

                    added = true;
                }
                else
                {
                    added = false;
                }
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }
            finally
            {
                if (added)
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Insert Data Success\"" +
                        "}";
                }
                else
                {
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Languange Already Exist\"" +
                        "}";
                }

            }

            return Ok(res);

        }



        // [HttpGet("{id}")]
        // public IActionResult WhereId(int id)
        // { 
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from Activity where activity_id = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // } 

        // [HttpGet("where/{obj}")]
        // public IActionResult WhereObj(string obj)
        // {     
        //     var o1 = JObject.Parse(obj);
        //     string query = ""; 

        //     query += "select * from Activity ";
        //     int i = 0;
        //     JObject json = JObject.FromObject(o1);
        //     foreach (JProperty property in json.Properties())
        //     { 
        //         if(i==0){
        //             query += " where ";
        //         }
        //         if(i>0){
        //             query += " and ";
        //         }

        //         if(property.Name.Equals("activity_category")){   
        //             query += "activity_category = '"+property.Value.ToString()+"' "; 
        //         }

        //         i=i+1;
        //     } 

        //     try
        //     {  
        //         sb.AppendFormat(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
        //     }
        //     return Ok(result);  
        // }

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, [FromBody] dynamic data)
        // {  
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Update Data Failed\""+
        //         "}";  

        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update Activity set "+ 
        //             "activity_category='"+data.activity_category+"', "+
        //             "activity_name='"+data.activity_name+"', "+
        //             "description='"+data.description+"', "+
        //             "`unique`='"+data.unique+"', "+
        //             "cuid='"+data.cuid+"', "+
        //             "cdate='"+data.cdate+"', "+
        //             "muid='"+data.muid+"', "+
        //             "mdate='"+data.mdate+"', "+ 
        //             "status='"+data.status+"' "+
        //             "where activity_id='"+id+"'"
        //         ); 
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Update Data Failed\""+
        //             "}";  
        //     }
        //     finally
        //     {  
        //     }

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select * from Activity where activity_id ="+id+";");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o3 = JObject.Parse(result);  

        //     if(o3["activity_name"].ToString() == data.activity_name.ToString())
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Update Data Success\""+
        //             "}"; 
        //     } 

        //     return Ok(res);  
        // } 

        // [HttpPut("status/{id}")]
        // public IActionResult UpdateSatus(int id)
        // {  
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  

        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update Activity set "+ 
        //             "status='Deleted' "+
        //             "where activity_id='"+id+"'"
        //         ); 
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Delete Data Failed\""+
        //             "}";  
        //     }
        //     finally
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Delete Data Success\""+
        //             "}";  
        //     }

        //     return Ok(res);  
        // } 

        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // { 
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("delete from Activity where activity_id = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Delete Data Failed\""+
        //             "}"; 
        //     }
        //     finally
        //     { 
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from Activity where activity_id  ="+id+";");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);

        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //         o3 = JObject.Parse(result);  
        //         Console.WriteLine(o3.Count);
        //         if(o3.Count < 1){ 
        //             res =
        //                 "{"+
        //                 "\"code\":\"1\","+
        //                 "\"message\":\"Delete Data Success\""+
        //                 "}"; 
        //         }
        //     }

        //     return Ok(res);
        // } 

    }

    public class LanguagePackToUpdate
    {
        public int LanguagePackId { get; set; }
        public string LanguageJson { get; set; }
    }
}