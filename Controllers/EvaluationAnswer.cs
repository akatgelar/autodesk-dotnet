using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.CommonServices;
using autodesk.Code.DataServices;
using autodesk.Code.ExcelExportService;
using autodesk.Code.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.IO;
using Microsoft.Net.Http.Headers;
using ClosedXML.Excel;
using System.Linq;
using autodesk.Code.AuditLogServices;

namespace autodesk.Controllers
{
    [Route("api/EvaluationAnswer")]
    public class EvaluationAnswer : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        private readonly IExcelExportServices iexportService;

        private readonly IEmailService _emailSender;
        private IDataServices dataServices;
        private readonly MySQLContext db;
        private readonly IAuditLogServices _auditLogServices;
        
        public EvaluationAnswer(IEmailService emailSender, MySqlDb sqlDb, MySQLContext _db, IAuditLogServices auditLogServices)
        {
            _emailSender = emailSender;
            oDb = sqlDb;
            db = _db;
            dataServices = new DataServices(oDb, db);
            _auditLogServices = auditLogServices;


        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from EvaluationAnswer ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }



        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from EvaluationAnswer where EvaluationAnswerId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }



        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select * from EvaluationAnswer ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("EvaluationAnswerId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "EvaluationAnswerId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("EvaluationQuestionCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "EvaluationQuestionCode like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CourseId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "CourseId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "CountryCode like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("StudentId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "StudentId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Status"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "Status like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally

            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("OrganizationReport")]
        public IActionResult OrgRptUnderEva([FromBody] dynamic data)
        {
            string query = "";

            query +=

            /* issue 16102018 - org report eva: there are 2 sites active displayed, but their are few instructor id which do not belong to both the site id */

            //"SELECT InstructorId,s.SiteId,SiteName,COUNT(EvaluationReportId) AS Evals,er.FYIndicatorKey,countries_name,er.RoleId," +
            //"CASE WHEN SUM(NbAnswerIQ*100.00) = 0 THEN 0.00 ELSE CAST(SUM(ScoreIQ)/ SUM(NbAnswerIQ*100.00) * 100 AS DECIMAL(7,2)) END WDT," +
            //"CASE WHEN er.RoleId = '1' THEN 'ATC' WHEN er.RoleId = '58' THEN 'AAP' END AS PartnerType FROM EvaluationReport er " +
            //"INNER JOIN Courses c ON er.CourseId = c.CourseId " +
            //"INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            //"INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId AND sc.`Status` = 'A' "+ 
            //"INNER JOIN Contacts_All ca ON sc.ContactId = ca.ContactId AND c.ContactId = ca.ContactId AND ca.Status <> 'D' "+
            //"INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code ";

            /* end line issue 16102018 - org report eva: there are 2 sites active displayed, but their are few instructor id which do not belong to both the site id */

            // Aek Sorry ya I need to roll back your above query
            // it correct, Ritu don't understand the logic behind and she think some instructor are not belonging to the Site, actually in Site report we don't show any delete/inactive status instructor, we only show
            // active one, but in report we should show everything

            //roll back query

            // "SELECT InstructorId,s.SiteId,SiteName,COUNT(StudentID) AS Evals,er.FYIndicatorKey,countries_name,er.RoleId," +
            // "CASE WHEN SUM(NbAnswerIQ*100.00) = 0 THEN 0.00 ELSE CAST(SUM(ScoreIQ)/ SUM(NbAnswerIQ*100.00) * 100 AS DECIMAL(7,2)) END WDT," +
            // "CASE WHEN er.RoleId = '1' THEN 'ATC' WHEN er.RoleId = '58' THEN 'AAP' END AS PartnerType FROM EvaluationReport er " +
            // "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
            // "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            // "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
            // "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code ";

            //end rollback query

            /* issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            // "SELECT *, COUNT(Eval) AS Evals FROM ("+
            // "SELECT InstructorId,s.SiteId,SiteName,COUNT(StudentID) AS Eval,er.FYIndicatorKey,countries_name,er.RoleId," +
            // "CPerformanceReportASE WHEN SUM(NbAnswerIQ*100.00) = 0 THEN 0.00 ELSE CAST(SUM(ScoreIQ)/ SUM(NbAnswerIQ*100.00) * 100 AS DECIMAL(7,2)) END WDT," +
            // "CASE WHEN er.RoleId = '1' THEN 'ATC' WHEN er.RoleId = '58' THEN 'AAP' END AS PartnerType FROM EvaluationReport er " +
            // "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
            // "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            // "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
            // "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code ";

            /* end line issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            /* issue 27112018 - ORG and Instructor Performance Report - data should be an exact match */

            "SELECT *, COUNT(Eval) AS Evals, CASE WHEN SUM(NbAnswerIQ*100.00) = 0 THEN 0.00 ELSE CAST(SUM(ScoreIQ)/ SUM(NbAnswerIQ*100.00) * 100 AS DECIMAL(7,2)) END WDT FROM (" +
            "SELECT NbAnswerIQ,ScoreIQ,InstructorId,s.SiteId,SiteName,COUNT(er.StudentID) AS Eval,er.FYIndicatorKey,countries_name,er.RoleId," +
            "CASE WHEN er.RoleId = '1' THEN 'ATC' WHEN er.RoleId = '58' THEN 'AAP' END AS PartnerType FROM EvaluationReport er " +
            "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
            "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
            "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
            "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
            "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code ";

            /* end line issue 27112018 - ORG and Instructor Performance Report - data should be an exact match */

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Date"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() != "ALL"))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "er.Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.`Status` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        // query += "er.CertificateType IN (" + property.Value.ToString() + ") ";

                        /* fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */

                        query += "(er.RoleId IN ('1','58') OR er.CertificateType IN (" + property.Value.ToString() + ")) ";

                        /* end line fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId IN (SELECT SiteId FROM Main_site WHERE OrgId = '" + property.Value.ToString() + "') ";
                        i = i + 1;
                    }
                }

                /* new request nambah user role -__ */

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                /* new request nambah user role -__ */
            }


            // i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("EventType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        // query += "c.EventType2 IN (" + property.Value.ToString() + ") ";

                        /* fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */

                        query += "(er.RoleId IN ('1','58') OR er.CertificateType IN ('1','2','3','4') OR c.EventType2 IN (" + property.Value.ToString() + ")) ";

                        /* end line fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */

                        i = i + 1;
                    }
                }
            }

            // query += " GROUP BY InstructorId,er.RoleId";

            /* issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            query += " GROUP BY er.StudentID,er.CourseId) AS a GROUP BY InstructorId,RoleId ORDER BY InstructorId ASC";

            /* end line issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        //[HttpGet("ATC_Site/{org_id}/{affiliated}")]
        //public IActionResult Satellite(string org_id, string affiliated)
        [HttpPost("ATC_Site")]
        public IActionResult Satellite(string org_id, string affiliated, [FromBody] dynamic data)
        {
            JObject json = JObject.FromObject(data);
            string sites = "";
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("siteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        sites = "AND s.SiteId IN (" + property.Value.ToString() + ")";
                    }
                }
            }


            try
            {
                sb = new StringBuilder();
                sb.Append(
                    "SELECT DISTINCT ATCSiteId, s.SiteId,countries_name FROM Main_site s " +
                    "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId " +
                    "INNER JOIN SiteRoleParams sp ON sr.SiteId = sp.SiteId " +
                    "INNER JOIN  Ref_countries rc ON rc.countries_code = s.SiteCountryCode "+
                    // "WHERE s.OrgId LIKE '%" + org_id + "%' AND (s.`Status` = 'A' AND sr.`Status` = 'A') AND sr.RoleId = 1 " + sites + " ORDER BY ATCSiteId"
                    "WHERE s.OrgId = '" + data.orgId + "' AND (s.`Status` = 'A' AND sr.`Status` = 'A') " + sites + " ORDER BY ATCSiteId"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetOrgName/{org_id}")]
        public IActionResult OrgName(string org_id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT OrganizationId, OrgName FROM Main_organization WHERE OrgId LIKE '%" + org_id + "%'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        //Saya pakai yg ini dulu untuk report organization under eva
        [HttpPost("PerformanceReportOrg")]
        public IActionResult PerformRpt_Org([FromBody] dynamic data)
        {
            string query = "";

            query +=
            "SELECT " +
            "CASE WHEN SUM( NbAnswerFR * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreFR ) / SUM( NbAnswerFR * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 ) ) END FR, " +
            "CASE WHEN SUM( NbAnswerIQ * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreIQ ) / SUM( NbAnswerIQ * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END IQ, " +
            "CASE WHEN SUM( NbAnswerCCM * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreCCM ) / SUM( NbAnswerCCM * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END CCM, " +
            "CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END OE, " +
            "CASE WHEN SUM( NbAnswerOP * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOP ) / SUM( NbAnswerOP * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END OP, " +
            "CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END COE, " +
            "CASE WHEN RoleId = '1' THEN 'ATC' WHEN RoleId = '58' THEN 'AAP' END AS PartnerType " +
            "FROM EvaluationReport er " +
            "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            "INNER JOIN Main_organization o ON s.OrgId = o.OrgId ";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Date"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() != "ALL"))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "er.Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.`Status` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                string roleID = "";
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ") ";
                        roleID = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    string cerType = "";
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (roleID == "58")
                        {
                            cerType = " er.CertificateType IN (" + property.Value.ToString() + ") ";
                        }
                        else
                        {
                            cerType = " (er.CertificateType IN (" + property.Value.ToString() + ",0) or er.CertificateType is NULL) ";
                        }

                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += cerType;
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                /* new request nambah user role -__ */

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                /* new request nambah user role -__ */
            }

            query += " GROUP BY o.OrganizationId";

            try
            {
                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost("PerformanceReport")]
        public IActionResult PerformanceRpt([FromBody] dynamic data)
        {
            string query = "";
            string territory = "";

            int i = 0;
            JObject json = JObject.FromObject(data);
            //foreach (JProperty property in json.Properties())
            //{
            //    if (property.Name.Equals("Distributor"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            territory = "WHERE s.SiteCountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + property.Value.ToString() + " ))";
            //            i = i + 1;
            //        }
            //    }
            //}

            query +=
                //"SELECT ContactId,SiteIdInt,CountryCode,countries_name,OrgId,s.CourseId,COUNT(Evals) AS Evals,s.SiteId,s.FYIndicatorKey,RoleId,s.SiteName,StateName,`Month`," +
                "SELECT SiteIdInt,CountryCode,countries_name,OrgId,COUNT(Evals) AS Evals,s.SiteId,s.FYIndicatorKey,s.RoleId,s.SiteName,StateName,`Month`," +
                "CASE WHEN SUM( NbAnswerFR * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreFR ) / SUM( NbAnswerFR * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 ) ) END FR," +
                "CASE WHEN SUM( NbAnswerIQ * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreIQ ) / SUM( NbAnswerIQ * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END IQ," +
                "CASE WHEN SUM( NbAnswerCCM * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreCCM ) / SUM( NbAnswerCCM * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END CCM," +
                "CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END OE," +
                "CASE WHEN SUM( NbAnswerOP * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOP ) / SUM( NbAnswerOP * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END OP," +
                "CASE WHEN s.RoleId = '1' THEN 'ATC' WHEN s.RoleId = '58' THEN 'AAP' END AS PartnerType FROM " +

                //"(SELECT COUNT(StudentID) AS Evals,SiteName,SiteIdInt,StateName,OrgId,er.CountryCode,countries_name,CourseId,StudentID,er.SiteId,FYIndicatorKey,RoleId,`Month`,NbAnswerCCM,ScoreCCM,NbAnswerFR,ScoreFR,NbAnswerIQ,ScoreIQ,NbAnswerOE,ScoreOE,NbAnswerOP,ScoreOP FROM " +
                "(SELECT COUNT(er.StudentID) AS Evals,SiteName,SiteIdInt,StateName,OrgId,er.CountryCode,countries_name, er.CourseId,er.SiteId,er.FYIndicatorKey,er.RoleId,`Month`,NbAnswerCCM,ScoreCCM,NbAnswerFR,ScoreFR,NbAnswerIQ,ScoreIQ,NbAnswerOE,ScoreOE,NbAnswerOP,ScoreOP " +
                //"(SELECT * FROM EvaluationReport ";
                "FROM EvaluationReport er " +
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
                " INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId AND er.RoleId = sr.RoleId " +
                "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " +
                "LEFT OUTER JOIN States ss ON s.SiteStateProvince = ss.StateCode " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId ";

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteCountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + property.Value.ToString() + " ))";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {

                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }

                }
                if (property.Name.Equals("SiteId"))
                {

                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }

                }
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CountryCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Date"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() != "ALL"))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "er.Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        //query += "`Status` = '" + property.Value.ToString() + "' ";
                        if (property.Value.ToString() != "A")
                        {
                            query += "s.`Status` <> 'A' ";
                        }
                        else
                        {
                            query += "s.`Status` = '" + property.Value.ToString() + "' ";
                        }
                        i = i + 1;
                    }
                }

                //if (property.Name.Equals("PartnerType"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        if (i == 0) { query += " WHERE "; }
                //        if (i > 0) { query += " AND "; }
                //        query += "RoleId IN (" + property.Value.ToString() + ") ";
                //        i = i + 1;
                //    }
                //}

                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                string roleID = "";
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ") ";
                        roleID = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    string cerType = "";
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (roleID == "58")
                        {
                            cerType = " CertificateType IN (" + property.Value.ToString() + ") ";
                        }
                        else
                        {
                            cerType = " (CertificateType IN (" + property.Value.ToString() + ",0) or CertificateType is NULL) ";
                        }

                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += cerType;
                        i = i + 1;
                    }
                }

                // if (property.Name.Equals("SiteId"))
                // {
                //     if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //     {
                //         if (i == 0) { query += " WHERE "; }
                //         if (i > 0) { query += " AND "; }
                //         query += "SiteId = '" + property.Value.ToString() + "' ";
                //         i = i + 1;
                //     }
                // }

                if (property.Name.Equals("GroupByContact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != "true"))
                    {
                        query += " GROUP BY c.ContactId";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrderBySiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != "true"))
                    {
                        query += " ORDER BY s.SiteId";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Product"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += " cs.productId = " + property.Value.ToString() + " ";
                        i = i + 1;
                    }

                }

                /* issue 15112018 */

                if (property.Name.Equals("Product2"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += " cs.productsecondaryId = " + property.Value.ToString() + " ";
                        i = i + 1;
                    }

                }
            }

            query += "GROUP BY er.StudentID , er.CourseId) AS s ";
            //query += "GROUP BY er.CourseId ORDER BY er.CourseId";

            // query += " ) AS er " +
            //     "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            //     "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " +
            //     "LEFT OUTER JOIN States ss ON s.SiteStateProvince = ss.StateCode " + territory + " GROUP BY EvaluationReportId) AS s " +
            //     "LEFT OUTER JOIN Courses c ON s.CourseId = c.CourseId " +
            //     " Inner Join CourseSoftware cs on c.CourseId = cs.CourseId";

            /* issue 15112018 */

            //query += 
            //    " ) AS er " +
            //    "INNER JOIN Main_site s ON er.SiteId = s.SiteId ";

            //json = JObject.FromObject(data);
            //foreach (JProperty property in json.Properties())
            //{
            //    if (property.Name.Equals("OrgId"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            query += "AND s.OrgId IN (" + property.Value.ToString() + ") ";
            //        }

            //    }
            //    if (property.Name.Equals("SiteId"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            query += "AND s.SiteId IN (" + property.Value.ToString() + ") ";
            //        }

            //    }
            //}

            //query +=
            //    "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " +
            //    "LEFT OUTER JOIN States ss ON s.SiteStateProvince = ss.StateCode " + territory + " GROUP BY EvaluationReportId) AS s " +
            //    "LEFT OUTER JOIN Courses c ON s.CourseId = c.CourseId " +
            //    " Inner Join CourseSoftware cs on c.CourseId = cs.CourseId";

            /* end line - issue 15112018 */

            //i = 0;
            //json = JObject.FromObject(data);
            //foreach (JProperty property in json.Properties())
            //{
            //    /* No need event type for now - Nakarin
            //    if (property.Name.Equals("EventType"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            if (i == 0) { query += " WHERE "; }
            //            if (i > 0) { query += " AND "; }
            //            query += "c.EventType2 IN (" + property.Value.ToString() + ") ";
            //            i = i + 1;
            //        }
            //    }
            //    */

            //    if (property.Name.Equals("Product"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            if (i == 0) { query += " WHERE "; }
            //            if (i > 0) { query += " AND "; }
            //            query += " cs.productId = " + property.Value.ToString() + " ";
            //            i = i + 1;
            //        }

            //    }

            //    /* issue 15112018 */

            //    if (property.Name.Equals("Product2"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            if (i == 0) { query += " WHERE "; }
            //            if (i > 0) { query += " AND "; }
            //            query += " cs.productsecondaryId = " + property.Value.ToString() + " ";
            //            i = i + 1;
            //        }

            //    }

            //    /* end line - issue 15112018 */

            //}

            query += " GROUP BY SiteId,RoleId";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                // Console.WriteLine(result);
            }
            return Ok(result);
        }

        [HttpPost("GetCOE")]
        public IActionResult GetCOE([FromBody] dynamic data)
        {
            string sites = "";
            string year = "";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        sites = property.Value.ToString();
                        i = i + 1;
                    }
                }


                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        year = property.Value.ToString();
                        i = i + 1;
                    }
                }
            }

            string query = "";

            query += "SELECT COUNT(StudentID) AS total_eval,SiteId From EvaluationReport WHERE FYIndicatorKey = '" + year + "' and SiteId IN (" + sites + ") GROUP BY SiteId";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("GetOE")]
        public IActionResult GetOE([FromBody] dynamic data)
        {
            string sites = "";
            string year = "";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        sites = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        year = property.Value.ToString();
                        i = i + 1;
                    }
                }
            }

            string query = "";

            query += "SELECT SUM(CASE WHEN ScoreOE IN ('75','100') THEN 1 ELSE 0 END) AS scoreOE,SiteId,RoleId From EvaluationReport WHERE ScoreOE IN ('75','100') AND FYIndicatorKey = '" + year + "' and SiteId IN (" + sites + ") GROUP BY SiteId,RoleId";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("InstructorCourse")]
        public IActionResult InstCourse([FromBody] dynamic data)
        {
            string query = "";

            query +=
                // "SELECT DATE_FORMAT(c.CompletionDate,'%d-%M-%Y') AS CourseDate,DATE_FORMAT(c.CompletionDate,'%Y-%m-%d') AS CompletionDate,COUNT(StudentID) AS TotalStudent FROM EvaluationReport er " +
                // "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                // "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                // "INNER JOIN CourseSoftware cs on c.CourseId = cs.CourseId";

                /* issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

                "SELECT *, count(Total) AS TotalStudent FROM (" +
                "SELECT DATE_FORMAT(c.CompletionDate,'%d-%M-%Y') AS CourseDate,DATE_FORMAT(c.CompletionDate,'%Y-%m-%d') AS CompletionDate,COUNT(StudentID) AS Total FROM EvaluationReport er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                "INNER JOIN CourseSoftware cs on c.CourseId = cs.CourseId";

            /* end line issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Instructor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "ca.InstructorId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Month"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "er.Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId = " + property.Value.ToString() + " ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ProductID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "cs.productId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

            }

            // query += " GROUP BY CourseDate ORDER BY MONTH(c.CompletionDate) ASC";

            /* issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            query += "GROUP BY er.StudentID,er.CourseId) as a GROUP BY CourseDate ORDER BY MONTH(CompletionDate) ASC";

            /* end line issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("InstructorCourse_1/{course_id}")]
        public IActionResult CariInstructorCourse(string course_id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "SELECT s.CourseId,s.SiteId,s.SiteName,ContactId,CourseDate,SAPShipTo_retired,COUNT( DISTINCT cs.StudentID ) AS Evals,COUNT( DISTINCT cs.StudentID ) AS TotalStudent,EvaluationQuestionCode FROM " +
                    "(SELECT CourseId,s.SiteId,ContactId,CourseDate,SAPShipTo_retired,s.SiteName FROM " +
                    "(SELECT CourseId,SiteId,ContactId,DATE_FORMAT( StartDate, '%m/%d/%Y' ) AS CourseDate FROM Courses WHERE CourseId = '" + course_id + "') As c " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId) AS s " +
                    "INNER JOIN CourseStudent cs ON s.CourseId = cs.CourseId " +
                    "INNER JOIN EvaluationAnswer ea ON ea.StudentId = cs.StudentID " +
                    "WHERE cs.CourseTaken = 1 AND cs.SurveyTaken = 1 ORDER BY s.CourseId"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost("ChannelPerformance")]
        public IActionResult CP([FromBody] dynamic data)
        {
            string query = "";
            string year = "";
            string ifOrg = "";
            string ifDistributor = "";


            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        year = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        // if (i == 0) { query += " WHERE "; }
                        // if (i > 0) { query += " AND "; }
                        ifOrg += "INNER JOIN (SELECT SiteId FROM Main_site WHERE OrgId IN (" + property.Value.ToString() + ")) AS os ON s.SiteId = os.SiteId ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        // if (i == 0) { query += " WHERE "; }
                        // if (i > 0) { query += " AND "; }
                        ifDistributor += "INNER JOIN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN (" + property.Value.ToString() + ")) AS d ON rc.countries_code = d.CountryCode ";
                        i = i + 1;
                    }
                }
            }

            query +=
            //// "SELECT SiteId,p.RoleId,SiteName,Territory_Name,MarketType,ParamValue,SiteManagerEmailAddress,FYIndicatorKey,`Month`,CourseId," +
            //// "SUM(CASE WHEN `Quarter` = 3 THEN 1 ELSE 0 END) EvalsQ1," +
            //// "SUM(CASE WHEN `Quarter` = 2 THEN 1 ELSE 0 END) EvalsQ2," +
            //// "SUM(CASE WHEN `Quarter` = 1 THEN 1 ELSE 0 END) EvalsQ3," +
            //// "SUM(CASE WHEN `Quarter` = 4 THEN 1 ELSE 0 END) EvalsQ4,MultiplierOverride FROM " +
            //// "(SELECT er.SiteId,SiteName,CountryCode,er.RoleId,SiteManagerEmailAddress,TerritoryId,MarketTypeId,RoleParamId,`Quarter`,MultiplierOverride,FYIndicatorKey,`Month`,CourseId FROM EvaluationReport er " +
            //// "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            //// "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " +
            //// "INNER JOIN SiteRoleParams srp ON s.SiteId = srp.SiteId ";

            //"SELECT s.SiteId, SiteName,er.CountryCode, er.RoleId,SiteManagerEmailAddress,Territory_Name,MarketType,StationsQ1,StationsQ2,StationsQ3,StationsQ4," +
            //"SUM(CASE WHEN `Quarter` = 1 THEN 1 ELSE 0 END) AS EvalsQ1," +
            //"SUM(CASE WHEN `Quarter` = 2 THEN 1 ELSE 0 END) AS EvalsQ2," +
            //"SUM(CASE WHEN `Quarter` = 3 THEN 1 ELSE 0 END) EvalsQ3," +
            //"SUM(CASE WHEN `Quarter` = 4 THEN 1 ELSE 0 END) EvalsQ4," +
            //"er.FYIndicatorKey,`Month`,er.CourseId,MultiplierOverride," +
            //"COUNT(er.StudentID) AS num_of_eval," +
            //"(SELECT ParamValue FROM SiteRoles sr INNER JOIN SiteRoleParams srp ON sr.SiteId = srp.SiteId INNER JOIN RoleParams rp ON srp.RoleParamId = rp.RoleParamId WHERE sr.SiteId = s.SiteId AND sr.`Status` = 'A' GROUP BY s.SiteId) AS ParamValue " +
            //"FROM EvaluationReport er " +
            //"INNER JOIN Main_site s ON er.SiteId = s.SiteId INNER JOIN SiteRoles ss on  ss.SiteId = s.SiteId " + ifOrg +
            //"INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " + ifDistributor +
            //"INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
            //"INNER JOIN MarketType mt ON rc.MarketTypeId = mt.MarketTypeId " +
            //"INNER JOIN Main_site_stations ms ON (s.SiteId = ms.SiteId AND ms.`Year` = '" + year + "') ";
            "SELECT s.SiteId, s.SiteName, CountryCode, s.RoleId, SiteManagerEmailAddress, Territory_Name, MarketType, StationsQ1, StationsQ2, StationsQ3,StationsQ4, " +
            "SUM(CASE WHEN `Quarter` = 1 THEN 1 ELSE 0 END) AS EvalsQ1, " +
            "SUM(CASE WHEN `Quarter` = 2 THEN 1 ELSE 0 END) AS EvalsQ2, " +
            "SUM(CASE WHEN `Quarter` = 3 THEN 1 ELSE 0 END) EvalsQ3, " +
            "SUM(CASE WHEN `Quarter` = 4 THEN 1 ELSE 0 END) EvalsQ4, " +
            "s.FYIndicatorKey, `Month`, s.CourseId, MultiplierOverride, COUNT(Evals) AS num_of_eval, " +
            "(SELECT  ParamValue FROM SiteRoles sr INNER JOIN SiteRoleParams srp ON sr.SiteId = srp.SiteId INNER JOIN RoleParams rp ON srp.RoleParamId = rp.RoleParamId WHERE sr.SiteId = s.SiteId AND sr.`Status` = 'A' GROUP BY s.SiteId) AS ParamValue " +
            "FROM(SELECT COUNT(er.StudentID) AS Evals, SiteName, SiteIdInt, StateName, OrgId, er.CountryCode, countries_name, er.CourseId, er.SiteId, " +
            "er.FYIndicatorKey, er.RoleId, `Month`, StationsQ1, StationsQ2, StationsQ3, StationsQ4, er.`Quarter`, SiteManagerEmailAddress, rt.Territory_Name, mt.MarketType, rc.MultiplierOverride " +
            "FROM EvaluationReport er INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
            "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
            "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId AND er.RoleId = sr.RoleId " +
            "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " +
            "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
            "INNER JOIN MarketType mt ON rc.MarketTypeId = mt.MarketTypeId " +
            "INNER JOIN Main_site_stations ms ON(s.SiteId = ms.SiteId AND ms.`Year` = '2018') " +
            "LEFT OUTER JOIN States ss ON s.SiteStateProvince = ss.StateCode " +
            "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
            "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId ";
         
            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.Countrycode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        // query += "eq.`Year` = '" + property.Value.ToString() + "' ";
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        //query += "er.`Status` = '" + property.Value.ToString() + "' ";
                        if (property.Value.ToString() != "A")
                        {
                            //query += "s.`Status` <> 'A' ";
                            query += " sr.`Status` <> 'A' ";
                        }
                        else
                        {
                            query += " sr.`Status` = 'A' and s.`Status` = '" + property.Value.ToString() + "' ";
                        }
                        i = i + 1;
                    }
                }
                string roleID = "";
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ") ";
                        roleID = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    string cerType = "";
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (roleID == "58")
                        {
                            cerType = " er.CertificateType IN (" + property.Value.ToString() + ") ";
                        }
                        else
                        {
                            cerType = " (er.CertificateType IN (" + property.Value.ToString() + ",0) or er.CertificateType is NULL) ";
                        }

                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += cerType;
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("GeoCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rt.TerritoryId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (SELECT SiteId FROM SiteContactLinks WHERE ContactId = '" + property.Value.ToString() + "') ";
                        i = i + 1;
                    }
                }
            }

            
           
            //query += "GROUP BY s.SiteId";
            query += " GROUP BY er.StudentID, er.CourseId) AS s  GROUP BY SiteId , RoleId;";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }


        [HttpPost("Station")]
        public IActionResult StationChannelPerformance([FromBody] dynamic data)
        {
            string query = "";

            query +=
            "SELECT s.SiteId,StationsQ1,StationsQ2,StationsQ3,StationsQ4 FROM EvaluationReport er " +
            "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            "INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " +
            "LEFT JOIN Main_site_stations ms ON s.SiteId = ms.SiteId ";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.Countrycode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        // query += "eq.`Year` = '" + property.Value.ToString() + "' ";
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.`Status` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CertificateType IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("GeoCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }

            query += "GROUP BY s.SiteId";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("SubmissionByLanguage")]
        public IActionResult SubLanguage([FromBody] dynamic data)
        {
            string query = "";

            query +=
            "SELECT COUNT(EvaluationReportId) AS Evals, Text FROM " +
            "(SELECT EvaluationReportId, ts.StudentID, LanguageID FROM " +
            "(SELECT DISTINCT EvaluationReportId, StudentID FROM EvaluationReport ";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CountryCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("GeoCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "RoleId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CertificateType IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }

            query += " ) AS e " +
            "INNER JOIN tblStudents ts ON e.StudentID = ts.StudentID) AS t " +
            "INNER JOIN tblTranslatedLanguages tl ON t.LanguageID = tl.FromLanguageID " +
            "WHERE tl.ToLanguageID = 7 " + //di set 7 dari aplikasi eksistingnya (old system)
            "GROUP BY t.LanguageID,tl.Text ORDER BY Text";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        // [HttpPost("ProductTrained")]
        // public IActionResult ProductTrainedRpt([FromBody] dynamic data)
        // {            
        //     string query1 = 
        //         "SELECT SiteId,COUNT(cs.ProductVersionsId) AS Evals,CONCAT(productName,' ',Version) AS product FROM " +
        //         "(SELECT EvaluationReportId,SiteId,s.CourseId,SiteName,RoleId,productId,ProductVersionsId FROM " +
        //         "(SELECT EvaluationReportId,e.SiteId,CourseId,SiteName,RoleId FROM " +
        //         "(SELECT EvaluationReportId,SiteId,CourseId,RoleId FROM EvaluationReport ";

        //     string query2 = 
        //         "SELECT SiteId,REPLACE ( REPLACE ( REPLACE ( SiteName, '\"', '\\\\\"' ), '\\\\', '\\\\\\\\' ), '\\\\\"', '\\\"' ) as SiteName, case when RoleId = '1' then 'ATC' else 'AAP' end as PartnerType FROM " +
        //         "(SELECT EvaluationReportId,SiteId,s.CourseId,SiteName,RoleId,productId,ProductVersionsId FROM " +
        //         "(SELECT EvaluationReportId,e.SiteId,CourseId,SiteName,RoleId FROM " +
        //         "(SELECT EvaluationReportId,SiteId,CourseId,RoleId FROM EvaluationReport ";

        //     int i = 0;
        //     JObject json = JObject.FromObject(data);
        //     foreach (JProperty property in json.Properties())
        //     {
        //         if (property.Name.Equals("CountryCode"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if(i==0){query1 += " WHERE ";query2 += " WHERE ";}
        //                 if(i>0){query1 += " AND ";query2 += " AND ";}
        //                 query1 += "CountryCode IN (" + property.Value.ToString() + ") ";
        //                 query2 += "CountryCode IN (" + property.Value.ToString() + ") ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("GeoCode"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if(i==0){query1 += " WHERE ";query2 += " WHERE ";}
        //                 if(i>0){query1 += " AND ";query2 += " AND ";}
        //                 query1 += "GeoCode IN (" + property.Value.ToString() + ") ";
        //                 query2 += "GeoCode IN (" + property.Value.ToString() + ") ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("Year"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if(i==0){query1 += " WHERE ";query2 += " WHERE ";}
        //                 if(i>0){query1 += " AND ";query2 += " AND ";}
        //                 query1 += "FYIndicatorKey = '" + property.Value.ToString() + "' ";
        //                 query2 += "FYIndicatorKey = '" + property.Value.ToString() + "' ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("PartnerType"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if(i==0){query1 += " WHERE ";query2 += " WHERE ";}
        //                 if(i>0){query1 += " AND ";query2 += " AND ";}
        //                 query1 += "RoleId IN (" + property.Value.ToString() + ") ";
        //                 query2 += "RoleId IN (" + property.Value.ToString() + ") ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("CertificateType"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if(i==0){query1 += " WHERE ";query2 += " WHERE ";}
        //                 if(i>0){query1 += " AND ";query2 += " AND ";}
        //                 query1 += "CertificateType IN (" + property.Value.ToString() + ") ";
        //                 query2 += "CertificateType IN (" + property.Value.ToString() + ") ";
        //                 i = i + 1;
        //             }
        //         }

        //         if (property.Name.Equals("SiteActive"))
        //         {
        //             if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
        //             {
        //                 if(i==0){query1 += " WHERE ";query2 += " WHERE ";}
        //                 if(i>0){query1 += " AND ";query2 += " AND ";}
        //                 query1 += "`Status` = '" + property.Value.ToString() + "' ";
        //                 query2 += "`Status` = '" + property.Value.ToString() + "' ";
        //                 i = i + 1;
        //             }
        //         }
        //     }

        //     query1 += " ) AS e " +
        //         "INNER JOIN Main_site s ON e.SiteId = s.SiteId) AS s " +
        //         "INNER JOIN CourseSoftware cs ON s.CourseId = cs.CourseId) AS cs " +
        //         "INNER JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
        //         "INNER JOIN Products p ON pv.productId = p.productId " +
        //         "GROUP BY SiteId,pv.ProductVersionsId ORDER BY p.productName ASC";

        //     query2 += " ) AS e " +
        //         "INNER JOIN Main_site s ON e.SiteId = s.SiteId) AS s " +
        //         "INNER JOIN CourseSoftware cs ON s.CourseId = cs.CourseId) AS cs " +
        //         "INNER JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
        //         "INNER JOIN Products p ON pv.productId = p.productId " +
        //         "GROUP BY SiteId";

        //     JArray arr1;
        //     JArray arr2;
        //     string result1 = "";
        //     string result2 = "";

        //     try
        //     {
        //         sb = new StringBuilder();
        //         sb.AppendFormat(query1);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //         result1 = MySqlDb.GetJSONArrayString(ds.Tables[0]);
        //         arr1 = JArray.Parse(result1);

        //         sb = new StringBuilder();
        //         sb.AppendFormat(query2);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //         result2 = MySqlDb.GetJSONArrayString(ds.Tables[0]);
        //         arr2 = JArray.Parse(result2);

        //         foreach (var itemarr1 in arr1)
        //             {
        //                 foreach (var itemarr2 in arr2)
        //                 {
        //                     itemarr2[itemarr1["product"].ToString()] = "0";
        //                 }

        //                 foreach (var itemarr2 in arr2)
        //                 {
        //                     if(itemarr1["SiteId"].ToString() == itemarr2["SiteId"].ToString()){
        //                         itemarr2[itemarr1["product"].ToString()] = itemarr1["Evals"];
        //                     }
        //                 }
        //             }

        //         // Console.WriteLine(arr2);

        //         // if (ds.Tables[0].Rows.Count != 0)
        //         // {
        //         //     //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
        //         //     ds = MainOrganizationController.EscapeSpecialChar(ds);
        //         // }
        //     }
        //     catch
        //     {
        //         arr2 = null;
        //     }
        //     finally
        //     {
        //     }
        //     return Ok(arr2);
        // }

        [HttpPost("TotalProductStatistic")]
        public IActionResult TotalRecordProductTrainedStatistic([FromBody] dynamic data)
        {
            string query = "";
            string range = "";
            string ifOrg = "";
            string ifDistributor = "";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Range"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        range = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        // if (i == 0) { query += " WHERE "; }
                        // if (i > 0) { query += " AND "; }
                        ifOrg += "INNER JOIN (SELECT SiteId FROM Main_site WHERE OrgId IN (" + property.Value.ToString() + ")) AS os ON er.SiteId = os.SiteId ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        // if (i == 0) { query += " WHERE "; }
                        // if (i > 0) { query += " AND "; }
                        ifDistributor += "INNER JOIN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN (" + property.Value.ToString() + ")) AS d ON rc.countries_code = d.CountryCode ";
                        i = i + 1;
                    }
                }
            }

            query +=
                "SELECT COUNT(EvaluationReportId) AS Total " +
                "FROM EvaluationReport er " + ifOrg +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " + ifDistributor +
                "INNER JOIN CourseSoftware cs ON er.CourseId = cs.CourseId " +
                "INNER JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
                "INNER JOIN Products p ON pv.productId = p.productId " +
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                "LEFT JOIN ProductVersions pv2 ON cs.ProductVersionsSecondaryId = pv2.ProductVersionsId " +
                "LEFT JOIN Products p2 ON pv2.productId = p2.productId ";

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Country"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CountryCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rc.TerritoryId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (range == "SurveyYear")
                        {
                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("StartDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (range == "SubmittedDate")
                        {
                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += "( er.DateSubmitted between '" + property.Value.ToString() + "' AND ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("CompletionDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (range == "SubmittedDate")
                        {
                            query += "'" + property.Value.ToString() + "' ) ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId IN (SELECT SiteId FROM SiteContactLinks WHERE ContactId = '" + property.Value.ToString() + "' AND `Status` = 'A') ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Primary"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += " and p.productId= '" + property.Value.ToString() + "' ";
                    }

                    i = i + 1;
                }

                if (property.Name.Equals("Secondary"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += " and p2.productId= '" + property.Value.ToString() + "' ";
                    }
                }
            }

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("ProductTrainedStatistic")]
        public IActionResult ProductTrainedStatisticRpt([FromBody] dynamic data)
        {
            string query = "";
            string range = "";
            string ifOrg = "";
            string ifDistributor = "";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Range"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        range = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        // if (i == 0) { query += " WHERE "; }
                        // if (i > 0) { query += " AND "; }
                        ifOrg += "INNER JOIN (SELECT SiteId FROM Main_site WHERE OrgId IN (" +
                                 property.Value.ToString() + ")) AS os ON er.SiteId = os.SiteId ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        // if (i == 0) { query += " WHERE "; }
                        // if (i > 0) { query += " AND "; }
                        ifDistributor +=
                            "INNER JOIN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN (" +
                            property.Value.ToString() + ")) AS d ON rc.countries_code = d.CountryCode ";
                        i = i + 1;
                    }
                }
            }

            /* fix product trained statistic format issue 04/09/2018 */

            query += "SELECT  primaryProduct,primaryversion,secondaryProduct,secondaryversion ,PartnerType,";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        string resulttmp = "";
                        JArray geos;

                        sb = new StringBuilder();
                        sb.AppendFormat("SELECT geo_code, geo_name FROM Ref_geo WHERE geo_code IN (" +
                                        property.Value.ToString() + ")");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        resulttmp = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                        geos = JArray.Parse(resulttmp);

                        for (int j = 0; j < geos.Count; j++)
                        {
                            query += "Convert(SUM(`Geo " + geos[j]["geo_name"] + "`),signed) AS `Geo " + geos[j]["geo_name"] + "`,";
                        }
                    }
                }

                if (property.Name.Equals("Country"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        string resulttmp = "";
                        JArray countries;

                        sb = new StringBuilder();
                        sb.AppendFormat("SELECT countries_name FROM Ref_countries WHERE countries_code IN (" +
                                        property.Value.ToString() + ")");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        resulttmp = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                        countries = JArray.Parse(resulttmp);

                        for (int j = 0; j < countries.Count; j++)
                        {
                            query += "Convert(SUM(`" + countries[j]["countries_name"] + "`),signed) AS `" +
                                     countries[j]["countries_name"] + "`,";
                        }
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        string resulttmp = "";
                        JArray territories;

                        sb = new StringBuilder();
                        sb.AppendFormat("SELECT TerritoryId, Territory_Name FROM Ref_territory WHERE TerritoryId IN (" +
                                        property.Value.ToString() + ")");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        resulttmp = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                        territories = JArray.Parse(resulttmp);

                        for (int j = 0; j < territories.Count; j++)
                        {
                            query += "Convert(SUM(`Territory " + territories[j]["Territory_Name"] + "`),signed) AS `Territory " +
                                     territories[j]["Territory_Name"] + "`,";
                        }
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        string resulttmp = "";
                        JArray markettypes;

                        sb = new StringBuilder();
                        sb.AppendFormat("SELECT MarketTypeId, MarketType FROM MarketType WHERE MarketTypeId IN (" +
                                        property.Value.ToString() + ")");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        resulttmp = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                        markettypes = JArray.Parse(resulttmp);

                        for (int j = 0; j < markettypes.Count; j++)
                        {
                            query += "Convert(SUM(`" + markettypes[j]["MarketType"] + "`),signed) AS `" + markettypes[j]["MarketType"] +
                                     "`,";
                        }
                    }
                }
            }

            query +=
                "Convert(SUM(WorldWide),signed) AS WorldWide FROM (SELECT  p.productId,pv.DisplayOrder,pv.ProductVersionsId, p.productName as primaryProduct, pv.Version as primaryversion, p2.productName as secondaryProduct,pv2.Version as secondaryversion, ";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        string resulttmp = "";
                        JArray geos;

                        sb = new StringBuilder();
                        sb.AppendFormat("SELECT geo_code, geo_name FROM Ref_geo WHERE geo_code IN (" +
                                        property.Value.ToString() + ")");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        resulttmp = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                        geos = JArray.Parse(resulttmp);

                        for (int j = 0; j < geos.Count; j++)
                        {
                            query +=
                                "IFNULL((SELECT COUNT(pv.ProductVersionsId) from ProductVersions pv1 WHERE pv.ProductVersionsId = pv1.ProductVersionsId AND rc.geo_code = '" +
                                geos[j]["geo_code"] + "'),0) AS `Geo " + geos[j]["geo_name"] + "`,";
                        }
                    }
                }

                if (property.Name.Equals("Country"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        string resulttmp = "";
                        JArray countries;

                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "SELECT countries_code, countries_name FROM Ref_countries WHERE countries_code IN (" +
                            property.Value.ToString() + ")");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        resulttmp = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                        countries = JArray.Parse(resulttmp);

                        for (int j = 0; j < countries.Count; j++)
                        {
                            query +=
                                "IFNULL((SELECT COUNT(pv.ProductVersionsId) from ProductVersions pv1 WHERE pv.ProductVersionsId = pv1.ProductVersionsId AND rc.countries_code = '" +
                                countries[j]["countries_code"] + "'),0) AS `" + countries[j]["countries_name"] + "`,";
                        }
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        string resulttmp = "";
                        JArray territories;

                        sb = new StringBuilder();
                        sb.AppendFormat("SELECT TerritoryId, Territory_Name FROM Ref_territory WHERE TerritoryId IN (" +
                                        property.Value.ToString() + ")");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        resulttmp = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                        territories = JArray.Parse(resulttmp);

                        for (int j = 0; j < territories.Count; j++)
                        {
                            query +=
                                "IFNULL((SELECT COUNT(pv.ProductVersionsId) from ProductVersions pv1 WHERE pv.ProductVersionsId = pv1.ProductVersionsId AND rc.TerritoryId = '" +
                                territories[j]["TerritoryId"] + "'),0) AS `Territory " +
                                territories[j]["Territory_Name"] + "`,";
                        }
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        string resulttmp = "";
                        JArray markettypes;

                        sb = new StringBuilder();
                        sb.AppendFormat("SELECT MarketTypeId, MarketType FROM MarketType WHERE MarketTypeId IN (" +
                                        property.Value.ToString() + ")");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        resulttmp = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                        markettypes = JArray.Parse(resulttmp);

                        for (int j = 0; j < markettypes.Count; j++)
                        {
                            query +=
                                "IFNULL((SELECT COUNT(pv.ProductVersionsId) from ProductVersions pv1 WHERE pv.ProductVersionsId = pv1.ProductVersionsId AND rc.MarketTypeId = '" +
                                markettypes[j]["MarketTypeId"] + "'),0) AS `" + markettypes[j]["MarketType"] + "`,";
                        }
                    }
                }



            }

            query +=
                        "CASE WHEN RoleId = 1 THEN 'ATC' ELSE 'AAP' END AS PartnerType, COUNT(pv.ProductVersionsId) AS WorldWide " +
                        "FROM EvaluationReport er " + ifOrg +
                        "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                        "INNER JOIN CourseSoftware cs ON er.CourseId = cs.CourseId " +
                        "INNER JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
                        "INNER JOIN Products p ON pv.productId = p.productId " +
                        "LEFT JOIN ProductVersions pv2 ON cs.ProductVersionsSecondaryId = pv2.ProductVersionsId " +
                        "LEFT JOIN Products p2 ON pv2.productId = p2.productId " +
                        "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                        "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                        "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                        "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " + ifDistributor;

            query +=
                "WHERE rc.geo_code IN (SELECT geo_code FROM Ref_geo) AND rc.MarketTypeId IN (SELECT MarketTypeId FROM MarketType WHERE MarketTypeId <> '1') ";

            i = 1;

            /* fix product trained statistic format issue 04/09/2018 */

            // query +=
            //     "SELECT pv.ProductVersionsId,productName,Version,CASE WHEN RoleId = 1 THEN 'ATC' ELSE 'AAP' END AS PartnerType, COUNT(pv.ProductVersionsId) AS WorldWide " +
            //     "FROM EvaluationReport er " + ifOrg +
            //     "INNER JOIN CourseSoftware cs ON er.CourseId = cs.CourseId " +
            //     "INNER JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
            //     "INNER JOIN Products p ON pv.productId = p.productId " +
            //     "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " + ifDistributor;

            // i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                // if (property.Name.Equals("Geo"))
                // {
                //     if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //     {
                //         if(i==0){query += " WHERE ";}
                //         if(i>0){query += " AND ";} 
                //         query += "er.GeoCode IN (" + property.Value.ToString() + ") ";
                //         i = i + 1;
                //     }
                // }

                // if (property.Name.Equals("Country"))
                // {
                //     if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //     {
                //         if(i==0){query += " WHERE ";}
                //         if(i>0){query += " AND ";} 
                //         query += "er.CountryCode IN (" + property.Value.ToString() + ") ";
                //         i = i + 1;
                //     }
                // }

                // if (property.Name.Equals("Territory"))
                // {
                //     if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //     {
                //         if(i==0){query += " WHERE ";}
                //         if(i>0){query += " AND ";} 
                //         query += "rc.TerritoryId IN (" + property.Value.ToString() + ") ";
                //         i = i + 1;
                //     }
                // }

                // if (property.Name.Equals("MarketType"))
                // {
                //     if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //     {
                //         if(i==0){query += " WHERE ";}
                //         if(i>0){query += " AND ";} 
                //         query += "rc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                //         i = i + 1;
                //     }
                // }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (range == "SurveyYear")
                        {
                            if (i == 0)
                            {
                                query += " WHERE ";
                            }

                            if (i > 0)
                            {
                                query += " AND ";
                            }

                            query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                            i = i + 1;
                        }
                    }
                }
                if (property.Name.Equals("Primary"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += " and p.productId= '" + property.Value.ToString() + "' ";
                    }

                    i = i + 1;
                }

                if (property.Name.Equals("Secondary"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += " and p2.productId= '" + property.Value.ToString() + "' ";
                    }
                }
                if (property.Name.Equals("StartDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (range == "SubmittedDate")
                        {
                            if (i == 0)
                            {
                                query += " WHERE ";
                            }

                            if (i > 0)
                            {
                                query += " AND ";
                            }

                            query += "( er.DateSubmitted between '" + property.Value.ToString() +
                                     "' AND ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("CompletionDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (range == "SubmittedDate")
                        {
                            query += "'" + property.Value.ToString() + "' ) ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0)
                        {
                            query += " WHERE ";
                        }

                        if (i > 0)
                        {
                            query += " AND ";
                        }

                        query += "er.SiteId IN (SELECT SiteId FROM SiteContactLinks WHERE ContactId = '" +
                                 property.Value.ToString() + "' AND `Status` = 'A') ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0)
                        {
                            query += " WHERE ";
                        }

                        if (i > 0)
                        {
                            query += " AND ";
                        }

                        query += "er.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

            }

            // query += " GROUP BY p.productId,pv.ProductVersionsId,er.RoleId ORDER BY p.productId,pv.DisplayOrder DESC";

            /* fix product trained statistic format issue 04/09/2018 */

            query +=
                " GROUP BY p.productId , pv.ProductVersionsId, p2.productId, pv2.productVersionsId , er.RoleId , countries_code DESC) AS productstat GROUP BY primaryProduct,primaryversion,secondaryProduct,secondaryversion ,PartnerType ORDER BY productId,DisplayOrder DESC";

            /* fix product trained statistic format issue 04/09/2018 */
            List<dynamic> dynamicList = new List<dynamic>();
            try
            {

                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataServices.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
            }

            return Ok(result);
        }

        [HttpPost("TotalProductTrained")]
        public IActionResult TotalProductTrained([FromBody] dynamic data)
        {
            string query = "";

            query +=
            //    "SELECT COUNT(EvaluationReportId) AS Jumlah FROM EvaluationReport er " +
            //     "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
            //    "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            //    "INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " +
            //    "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
            //"INNER JOIN CourseSoftware cs ON er.CourseId = cs.CourseId " +
            //  "INNER JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
            //  "INNER JOIN Products p ON pv.productId = p.productId " +
            //  "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
            //  "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
            //  "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
            //  "LEFT JOIN ProductVersions pv2 ON cs.ProductVersionsSecondaryId = pv2.ProductVersionsId " +
            //  "LEFT JOIN Products p2 ON pv2.productId = p2.productId ";
            "SELECT COUNT(Eval) AS Jumlah FROM(SELECT eva.CourseId as Eval " +
            "FROM EvaluationReport eva " +
            "INNER JOIN EvaluationAnswer ea ON eva.CourseId = ea.CourseId AND eva.StudentID = ea.StudentId " +
            "INNER JOIN Main_site s ON s.SiteId = eva.SiteId " +
            "INNER JOIN SiteRoles sr ON eva.SiteId = sr.SiteId AND eva.RoleId = sr.RoleId " +
            "INNER JOIN Courses c ON eva.CourseId = c.CourseId " +
            "INNER JOIN Ref_countries rc ON eva.CountryCode = rc.countries_code " +
            "left JOIN CourseSoftware cs ON eva.CourseId = cs.CourseId " +
            "left JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
            "left JOIN Products p ON pv.productId = p.productId " +
            "left JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
            "LEFT JOIN ProductVersions pv2 ON cs.ProductVersionsSecondaryId = pv2.ProductVersionsId " +
            "LEFT JOIN Products p2 ON pv2.productId = p2.productId ";


            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CountryCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("GeoCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                string roleID = "";
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.RoleId IN (" + property.Value.ToString() + ") ";
                        roleID = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    string cerType = "";
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (roleID == "58")
                        {
                            cerType = " eva.CertificateType IN (" + property.Value.ToString() + ") ";
                        }
                        else
                        {
                            cerType = " (eva.CertificateType IN (" + property.Value.ToString() + ",0) or eva.CertificateType is NULL) ";
                        }

                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += cerType;
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        if (property.Value.ToString() != "A")
                        {
                            query += "s.`Status` <> 'A' ";
                        }
                        else
                        {
                            query += "s.`Status` = '" + property.Value.ToString() + "' ";
                        }
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rt.TerritoryId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }
            query += " GROUP BY eva.StudentID , eva.CourseId) AS c;";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = null;
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("ProductTrained")]
        public IActionResult ProductTrainedRpt([FromBody] dynamic data)
        {
            int i = 0;
            JObject json = JObject.FromObject(data);
            string result = "";

            string query =
                "SET SESSION group_concat_max_len = (10000 * 1024); " +
                "SET @SQL = NULL; " +
                "SELECT GROUP_CONCAT(DISTINCT CONCAT('sum(case when b.ProductVersionsId = ''', ProductVersionsId, ''' then b.Evals else 0 end) AS `', product, '`')) INTO @SQL " +
                "FROM (SELECT s.SiteId as dt, COUNT(cs.ProductVersionsId) AS Evals, CONCAT(productName, ' ', Version) AS product, p.productId, pv.ProductVersionsId " +
                "FROM EvaluationReport er " +
                "INNER JOIN Main_site s ON  s.SiteId =er.SiteId " +
                "INNER JOIN CourseSoftware cs ON er.CourseId = cs.CourseId " +
                "INNER JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
                "INNER JOIN Products p ON pv.productId = p.productId ";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CountryCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("GeoCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        // query += "er.CertificateType IN (" + property.Value.ToString() + ") ";

                        /* fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */

                        query += "(er.RoleId IN (1,58) OR er.CertificateType IN (" + property.Value.ToString() + ")) ";

                        /* end line fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.`Status` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.ContactId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteCountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + property.Value.ToString() + " ))";
                        i = i + 1;
                    }
                }
            }

            // if (i == 0) { query += " WHERE "; }
            // if (i > 0) { query += " AND "; }

            // query += "er.`Status` = 'A' ";

            i = 0;
            query +=
                "GROUP BY s.SiteId , pv.ProductVersionsId limit 10000000000 ) d; " +
                "SET @SQL = CONCAT('select b.SiteId, b.SiteName, b.PartnerType, ', @SQL, ' " +
                "from ";
            //"( SELECT Distinct SiteId FROM EvaluationReport ";

            //foreach (JProperty property in json.Properties())
            //{
            //    if (property.Name.Equals("CountryCode"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            if(i==0){query += " WHERE ";}
            //            if(i>0){query += " AND ";}
            //            query += "CountryCode IN (" + property.Value.ToString() + ") ";
            //            i = i + 1;
            //        }
            //    }

            //    if (property.Name.Equals("GeoCode"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            if(i==0){query += " WHERE ";}
            //            if(i>0){query += " AND ";}
            //            query += "GeoCode IN (" + property.Value.ToString() + ") ";
            //            i = i + 1;
            //        }
            //    }

            //    if (property.Name.Equals("Year"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            if(i==0){query += " WHERE ";}
            //            if(i>0){query += " AND ";}
            //            query += "FYIndicatorKey = \\'" + property.Value.ToString() + "\\' ";
            //            i = i + 1;
            //        }
            //    }

            //    if (property.Name.Equals("PartnerType"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            if(i==0){query += " WHERE ";}
            //            if(i>0){query += " AND ";}
            //            query += "RoleId IN (" + property.Value.ToString() + ") ";
            //            i = i + 1;
            //        }
            //    }

            //    if (property.Name.Equals("CertificateType"))
            //    {
            //        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
            //        {
            //            if(i==0){query += " WHERE ";}
            //            if(i>0){query += " AND ";}
            //            query += "CertificateType IN (" + property.Value.ToString() + ") ";
            //            i = i + 1;
            //        }
            //    }
            //}

            //if(i==0){query += " WHERE ";}
            //if(i>0){query += " AND ";}

            //query += "`Status` = \\'A\\' ";

            //i = 0;
            query +=
                // "limit 10000 ) as a inner join "+
                "( SELECT s.SiteId, s.SiteName, COUNT(cs.ProductVersionsId) AS Evals, pv.ProductVersionsId, CONCAT(productName, \\' \\', Version) AS product, RoleCode as PartnerType,  p.productId " +
                "FROM EvaluationReport er " +
                "INNER join Roles r on r.RoleId = er.RoleId " +
                "INNER JOIN Main_site s ON  s.SiteId =er.SiteId " +
                "INNER JOIN CourseSoftware cs ON er.CourseId = cs.CourseId " +
                "INNER JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
                "INNER JOIN Products p ON pv.productId = p.productId ";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CountryCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("GeoCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = \\'" + property.Value.ToString() + "\\' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        // query += "er.CertificateType IN (" + property.Value.ToString() + ") ";

                        /* fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */

                        query += "(er.RoleId IN (1,58) OR er.CertificateType IN (" + property.Value.ToString() + ")) ";

                        /* end line fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.`Status` = \\'" + property.Value.ToString() + "\\' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.ContactId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }

                        if (i > 0) { query += " AND "; }
                        query += "s.SiteCountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + property.Value.ToString() + " ))";
                        i = i + 1;
                    }
                }
            }

            // if (i == 0) { query += " WHERE "; }
            // if (i > 0) { query += " AND "; }

            // query += "er.`Status` = \\'A\\' ";

            query +=
                "GROUP BY s.SiteId , pv.ProductVersionsId )  as b " +
                "GROUP by b.SiteId limit 1000000 ; ' ); " +
                "PREPARE stmt FROM @SQL; " +
                "EXECUTE stmt; " +
                "DEALLOCATE PREPARE stmt;";
            List<dynamic> dynamicModel = new List<dynamic>();
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                    DataTable dt = ds.Tables[0];
                    dynamicModel = dataServices.ToDynamic(dt);

                }
            }
            catch
            { }
            finally
            {
                var settings = new JsonSerializerSettings { };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                settings);

                //if (ds.Tables.Count == 0)
                //{
                //    result = "hah...kossssongg??";
                //}
                //else
                //{
                //    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                //}

            }
            return Ok(result);
        }

        [HttpPost("TotalEvaluation")]
        public IActionResult TotalEva([FromBody] dynamic data)
        {
            string query = "";

            query +=
                "SELECT COUNT(EvaluationReportId) AS Jumlah FROM EvaluationReport er " +
                "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
                "INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " +
                "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId ";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CountryCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("GeoCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CertificateType IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.`Status` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rt.TerritoryId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = null;
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("TotalEvaluationChannel")]
        public IActionResult TotalEvaChannel([FromBody] dynamic data)
        {
            string query = "";

            query +=
                //"SELECT COUNT(EvaluationReportId) AS Jumlah FROM EvaluationReport er " +
                //"INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
                //"INNER JOIN SiteRoles ss on s.SiteId = ss.SiteId " +
                //"INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " +
                //"INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId ";
                "SELECT COUNT(Jumlah) AS Jumlah FROM " +
                "(SELECT COUNT(er.EvaluationReportId) AS Jumlah " +
                "FROM EvaluationReport er " +
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
                "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId AND er.RoleId = sr.RoleId " +
                "INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " +
                "LEFT OUTER JOIN States ss ON s.SiteStateProvince = ss.StateCode " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId " ;
 
            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CountryCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("GeoCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                string roleID = "";
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ") ";
                        roleID = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    //if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    //{
                    //    if (i == 0) { query += " WHERE "; }
                    //    if (i > 0) { query += " AND "; }
                    //    query += "CertificateType IN (" + property.Value.ToString() + ") ";

                    //    i = i + 1;
                    //}
                    string cerType = "";
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (roleID == "58")
                        {
                            cerType = " er.CertificateType IN (" + property.Value.ToString() + ") ";
                        }
                        else
                        {
                            cerType = " (er.CertificateType IN (" + property.Value.ToString() + ",0) or er.CertificateType is NULL) ";
                        }

                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += cerType;
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteActive"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        //query += "er.`Status` = '" + property.Value.ToString() + "' ";
                        if (property.Value.ToString() != "A")
                        {
                            query += "sr.`Status` <> 'A' ";
                        }
                        else
                        {
                            query += "s.`Status` = '" + property.Value.ToString() + "' ";
                        }
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "rt.TerritoryId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0 || i > 0) { query += " AND "; }
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }

            query += " GROUP BY er.StudentID, er.CourseId) AS s";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = null;
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("TotalDownloadSite")]
        public IActionResult TotalRecordDownloadSite([FromBody] dynamic data)
        {
            string query = "";
            int limit = 0;
            int ofset = 0;

            /* OPTIMASI 2 (From nakarin) */
            string aapcert = "";
            string rangedate = "";
            string UserRole = "";
            string SiteId = "";
            string OrgId = "";

            int i = 0;
            JObject json = JObject.FromObject(data);

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("DateRange"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        rangedate = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("AAP_Course"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        aapcert = "'AAP Course'";
                    }
                }

                if (property.Name.Equals("AAP_Project"))
                {
                    if (!string.IsNullOrEmpty(aapcert))
                    {
                        aapcert += ",'AAP Project'";
                    }
                    else
                    {
                        aapcert += "'AAP Project'";
                    }

                }

            }

            query +=
                // "Select Count(er.EvaluationReportId) as count " +
                // "from EvaluationReport er " +
                // "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                // "INNER JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
                // "INNER JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId " +
                // "INNER JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId " +
                // "INNER JOIN CourseSoftware cf ON c.CourseId = cf.CourseId " +
                // "INNER JOIN Products p ON cf.productId = p.productId  " +
                // "INNER JOIN ProductVersions pv ON cf.ProductVersionsId = pv.ProductVersionsId " +
                // "LEFT JOIN  Products p2 ON cf.productsecondaryId = p2.productId " +
                // "LEFT JOIN  ProductVersions pv2 ON cf.ProductSecondaryId = pv2.ProductVersionsId     " +
                // "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                // "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                // "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                // "INNER JOIN Ref_countries sc ON s.SiteCountryCode = sc.countries_code " +
                // "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                // "LEFT JOIN States ss ON st.StateID = ss.StateID ";

                /* issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

                "SELECT COUNT(Evals) AS count FROM (" +
                "SELECT COUNT(er.EvaluationReportId) AS Evals " +
                "FROM EvaluationReport er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "LEFT JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
                "LEFT JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId " +
                "LEFT JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId " +
                "INNER JOIN CourseSoftware cf ON c.CourseId = cf.CourseId " +
                "INNER JOIN Products p ON cf.productId = p.productId  " +
                "INNER JOIN ProductVersions pv ON cf.ProductVersionsId = pv.ProductVersionsId " +
                "LEFT JOIN  Products p2 ON cf.productsecondaryId = p2.productId " +
                "LEFT JOIN  ProductVersions pv2 ON cf.ProductSecondaryId = pv2.ProductVersionsId     " +
                "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                "INNER JOIN Ref_countries sc ON s.SiteCountryCode = sc.countries_code " +
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                "LEFT JOIN States ss ON st.StateID = ss.StateID ";

            /* end line issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "sc.geo_code IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Country"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "sc.countries_code IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "sc.TerritoryId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "sc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Organization"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Sites"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate != "radSubmittedDate")
                        {
                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += "c.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                            i = i + 1;
                        }
                    }
                }
                if (property.Name.Equals("Primary"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        query += " and p.productId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Secondary"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        query += " and p2.productId = '" + property.Value.ToString() + "' ";
                        i = i + 1;

                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        if (property.Value.ToString() == "\"1\"")
                        {
                            query += "c.PartnerType = 'ATC' ";
                        }
                        else if (property.Value.ToString() == "\"58\"")
                        {
                            //query += "c.PartnerType = 'AAP " + aapcert + "' ";
                            if (!string.IsNullOrEmpty(aapcert))
                            {
                                query += "c.PartnerType in (" + aapcert + ") ";
                            }
                            else
                            {
                                query += "c.PartnerType in ('AAP Event') ";
                            }

                        }
                        else
                        {
                            query += "(c.PartnerType = 'ATC' OR c.PartnerType in (" + aapcert + ") ";
                        }
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("StartDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate == "radCourseEndDate")
                        {
                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += "( c.CompletionDate between '" + property.Value.ToString() + "' and ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("CompletionDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate == "radCourseEndDate")
                        {
                            query += "'" + property.Value.ToString() + "' ) ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("StartDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate == "radSubmittedDate")
                        {
                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += "( er.DateSubmitted between '" + property.Value.ToString() + "' and ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("CompletionDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate == "radSubmittedDate")
                        {
                            query += "'" + property.Value.ToString() + "' ) ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("Limit"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        limit = Int32.Parse(property.Value.ToString());
                    }
                    else
                    {
                        limit = 0;
                    }
                }

                if (property.Name.Equals("Offset"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        ofset = Int32.Parse(property.Value.ToString());
                    }
                    else
                    {
                        ofset = 0;
                    }
                }


                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        UserRole = property.Value.ToString();

                    }
                }
                if (property.Name.Equals("SiteID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        SiteId = property.Value.ToString();
                    }

                }
                if (property.Name.Equals("OrgID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        OrgId = property.Value.ToString();
                    }
                }

            }

            if (UserRole.Contains("ORGANIZATION"))
            {
                query += " AND s.OrgId IN ('" + OrgId.Replace(",", "','") + "') ";
            }

            /* issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            query += "GROUP BY er.StudentID,er.CourseId ) as a ";

            /* end line issue Evalaution count from 1 feb 2018 to 31 jan 2019 (evalaution submitted) is not same as organisation report */

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                if (ds.Tables.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                }
                else
                {
                    result = "[]";
                }
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("SiteEvaluation/{pType}")]
        /*[HttpPost("SiteEvaluation")]
        public IActionResult SiteEvaluation([FromBody] dynamic data)
        */
        public async Task<IActionResult> SiteEvaluation(string pType, [FromBody] dynamic data)
        {
            string query = "";
            int limit = 0;
            int ofset = 0;
            ExcelExportService exportService = new ExcelExportService();
            /* OPTIMASI 2 (From nakarin) */
            string aapcert = "";
            string rangedate = "";
            string UserRole = "";
            string SiteId = "";
            string OrgId = "";

            int i = 0;
            JObject json = JObject.FromObject(data);

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("DateRange"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        rangedate = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("AAP_Course"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // aapcert = " Course";
                        aapcert = "'AAP Course'";
                    }
                }

                if (property.Name.Equals("AAP_Project"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        //aapcert = " Project";
                        if (!string.IsNullOrEmpty(aapcert))
                        {
                            aapcert += ",'AAP Project'";
                        }
                        else
                        {
                            aapcert += "'AAP Project'";
                        }

                    }
                }
                if (property.Name.Equals("AAP_Event"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        //aapcert = " Project";
                        if (!string.IsNullOrEmpty(aapcert))
                        {
                            aapcert += ",'AAP Event'";
                        }
                        else
                        {
                            aapcert += "'AAP Event'";
                        }

                    }
                }
            }

            #region Commented JASON EXTRACT Query

            /*
                        query +=
                            "Select ea.StudentEvaluationID AS EvalID, CONCAT(st.Firstname, ' ', st.Lastname) AS StudentName, ca.InstructorId, ca.ContactName AS IntructorName, s.SiteId, s.SiteName, c.CourseId,c.`Name` AS CourseTitle, p.productName, pv.Version, p2.productName as SecondaryProductName, pv2.Version as SecondaryProductVersion, DATE_FORMAT(c.StartDate, '%d-%M-%Y') AS CourseStartDate," +
                            "DATE_FORMAT(c.CompletionDate, '%d-%M-%Y') AS CourseCompletionDate, DATE_FORMAT(er.DateSubmitted, '%d-%M-%Y') AS DateSubmitted, st.Company,IFNULL(st.Phone,'- private -') AS Phone,st.Email,IFNULL(st.Address,'- private -') AS Address," +
                            /*"IFNULL(st.Address2,'- private -') AS Address2,st.City,sc.countries_name AS Country,IFNULL(StateName,'- private -') AS StateName,IFNULL(st.PostalCode,'- private -') AS PostalCode,EvaluationAnswerJson," +
                            "IFNULL(CAST(ATCFacility AS UNSIGNED),'0') AS ATCFacility,IFNULL(CAST(ATCComputer AS UNSIGNED),'0') AS ATCComputer,HoursTraining,`Update`,Essentials,Intermediate,Advanced,Customized,ctl.Other AS OtherCTL,ctl.`Comment` AS CommentCTL,InstructorLed,`Online`," +
                            "Autodesk,AAP,ATC,Independent,IndependentOnline,ATCOnline,ctm.Other AS OtherCTM,ctm.`Comment` AS CommentCTM, CONCAT(p.productName,' ',pv.Version) AS primaryProduct " +
                            "from EvaluationReport er " +
                            "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                            "INNER JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
                            "INNER JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId " +
                            "INNER JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId " +
                            "INNER JOIN CourseSoftware cf ON c.CourseId = cf.CourseId " +
                            "INNER JOIN Products p ON cf.productId = p.productId  " +
                            "INNER JOIN ProductVersions pv ON cf.ProductVersionsId = pv.ProductVersionsId " +
                            "LEFT JOIN  Products p2 ON cf.productsecondaryId = p2.productId " +
                            "LEFT JOIN  ProductVersions pv2 ON cf.ProductSecondaryId = pv2.ProductVersionsId     " +
                            "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                            "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                            "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                            "INNER JOIN Ref_countries sc ON s.SiteCountryCode = sc.countries_code " +
                            "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                            "LEFT JOIN States ss ON st.StateID = ss.StateID ";*/

            /*
                    //new added by Soe
                    "IFNULL(st.Address2,'- private -') AS Address2,st.City,sc.countries_name AS Country,IFNULL(StateName,'- private -') AS StateName,IFNULL(st.PostalCode,'- private -') AS PostalCode,";

                    if (pType == "1")
                    {
                    query +=
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q1')) AS Q1_1," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q2')) AS Q1_2," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q3')) AS Q1_3," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question1')) AS Q1_4," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q5.OE')), signed) AS Q2_1," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q5.FR_1')), signed) AS Q2_2," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q5.FR_2')), signed) AS Q2_3," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q5.IQ')), signed) AS Q2_5," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q5.CCM')), signed) AS Q2_6," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q6.CCM')), signed) AS Q3_1," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q6.IQ')), signed) AS Q3_2," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q6.FR')), signed) AS Q3_3," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q7.CCM')), signed) AS Q4," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q8')) AS Q5," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q8.Comment')) AS Q6," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q10')) AS Q7," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q11')) AS Q8," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q12.rq')), signed) AS Q9_1," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q12.r2')), signed) AS Q9_2," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q12.r3')), signed) AS Q9_3," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q12.r4')), signed) AS Q9_4," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.q13')) AS Q10," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.comment')) AS Q11,";
                }

                else if (pType == "58")
                {
                    query +=
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question1')) AS Q1_1," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question2')) AS Q1_2," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question3')) AS Q1_3," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question4')) AS Q1_4," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question5.OE')), signed) AS Q2_1," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question5.FR_1')), signed) AS Q2_2," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question5.FR_2')), signed) AS Q2_3," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question5.IQ')), signed) AS Q2_5," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question5.CCM')), signed) AS Q2_6," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question6.CCM')), signed) AS Q3_1," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question6.IQ')), signed) AS Q3_2," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question6.FR')), signed) AS Q3_3," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question7.CCM')), signed) AS Q4," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question8')) AS Q5," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question9')) AS Q6," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question10')) AS Q7," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question11.rq')), signed) AS Q8_1," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question11.r2')), signed) AS Q8_2," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question11.r3')), signed) AS Q8_3," +
                    "Convert(JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question11.r4')), signed) AS Q8_4," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.question12')) AS Q9," +
                    "JSON_UNQUOTE(JSON_EXTRACT(REPLACE(REPLACE( ea.EvaluationAnswerJson, '\\r', ''), '\\n', ''), '$.Comments')) AS Q10,";
                }
                query+= " IFNULL(CAST(ATCFacility AS UNSIGNED),'0') AS ATCFacility,IFNULL(CAST(ATCComputer AS UNSIGNED),'0') AS ATCComputer,HoursTraining,`Update`,Essentials,Intermediate,Advanced,Customized,ctl.Other AS OtherCTL,ctl.`Comment` AS CommentCTL,InstructorLed,`Online`," +
                    "Autodesk,AAP,ATC,Independent,IndependentOnline,ATCOnline,ctm.Other AS OtherCTM,ctm.`Comment` AS CommentCTM, CONCAT(p.productName,' ',pv.Version) AS primaryProduct " +
                    "from EvaluationReport er " +
                    "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                    "INNER JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
                    "INNER JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId " +
                    "INNER JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId " +
                    "INNER JOIN CourseSoftware cf ON c.CourseId = cf.CourseId " +
                    "INNER JOIN Products p ON cf.productId = p.productId  " +
                    "INNER JOIN ProductVersions pv ON cf.ProductVersionsId = pv.ProductVersionsId " +
                    "LEFT JOIN  Products p2 ON cf.productsecondaryId = p2.productId " +
                    "LEFT JOIN  ProductVersions pv2 ON cf.ProductSecondaryId = pv2.ProductVersionsId     " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                    "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                    "INNER JOIN Ref_countries sc ON s.SiteCountryCode = sc.countries_code " +
                    "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                    "LEFT JOIN States ss ON st.StateID = ss.StateID ";

                */



            #endregion

            query += "Select ea.StudentEvaluationID AS EvalID, CONCAT(st.Firstname, ' ', st.Lastname) AS StudentName, ca.InstructorId, ca.ContactName AS IntructorName, s.SiteId, s.SiteName, c.CourseId,c.`Name` AS CourseTitle, p.productName, DATE_FORMAT(c.StartDate, '%d-%M-%Y') AS CourseStartDate," +
                "DATE_FORMAT(c.CompletionDate, '%d-%M-%Y') AS CourseEndDate, DATE_FORMAT(er.DateSubmitted, '%d-%M-%Y') AS DateSubmitted, st.Company,IFNULL(st.Phone,'- private -') AS Phone,st.Email,IFNULL(st.Address,'- private -') AS Address," +
                "IFNULL(st.Address2,'- private -') AS Address2,st.City,sc.countries_name AS Country,IFNULL(StateName,'- private -') AS StateName,IFNULL(st.PostalCode,'- private -') AS PostalCode,EvaluationAnswerJson,ea.EvaluationQuestionCode," +
                "SUBSTRING(CONCAT(CASE WHEN `Update` = 1 THEN 'Update, ' ELSE '' END, CASE WHEN Essentials = 1 THEN 'Level 1 : Essentials, ' ELSE '' END, CASE WHEN Intermediate = 1 THEN 'Level 2 : Intermediate, ' ELSE '' END, " +
                "CASE WHEN Advanced = 1 THEN 'Level 3 : Advanced, ' ELSE '' END, CASE WHEN Customized = 1 THEN 'Customized, ' ELSE '' END, CASE WHEN ctl.Other = 1 THEN CONCAT('Other : ', ctl.`Comment`, ', ') ELSE '' END), 1, " +
                "CHAR_LENGTH(CONCAT(CASE WHEN `Update` = 1 THEN 'Update, ' ELSE '' END, CASE WHEN Essentials = 1 THEN 'Level 1 : Essentials, ' ELSE '' END, CASE WHEN Intermediate = 1 THEN 'Level 2 : Intermediate, ' ELSE '' END, " +
                "CASE WHEN Advanced = 1 THEN 'Level 3 : Advanced, ' ELSE '' END, CASE WHEN Customized = 1 THEN 'Customized, ' ELSE '' END, CASE WHEN ctl.Other = 1 THEN CONCAT('Other : ', ctl.`Comment`, ', ') ELSE '' END)) - 2) AS QP1, " +
                "CONCAT(p.productName, ' ', pv.Version) AS QP2, HoursTraining AS QP3, " +
                "SUBSTRING(CONCAT(CASE WHEN InstructorLed = 1 THEN 'Instructor-led in the classroom, ' ELSE '' END, CASE WHEN `Online` = 1 THEN 'Online or e-learning, ' ELSE '' END), 1, " +
                "CHAR_LENGTH(CONCAT(CASE WHEN InstructorLed = 1 THEN 'Instructor-led in the classroom, ' ELSE '' END, CASE WHEN `Online` = 1 THEN 'Online or e-learning, ' ELSE '' END)) - 2) AS QP4, " +
                "SUBSTRING(CONCAT(CASE WHEN Autodesk = 1 THEN 'Autodesk Official Courseware (any Autodesk branded material), ' ELSE '' END, CASE WHEN AAP = 1 THEN 'Autodesk Authors and Publishers (AAP) Program Courseware, ' ELSE '' END, " +
                "CASE WHEN ATC = 1 THEN 'Course material developed by the ATC, ' ELSE '' END, CASE WHEN Independent = 1 THEN 'Course material developed by an independent vendor or instructor, ' ELSE '' END, " +
                "CASE WHEN IndependentOnline = 1 THEN 'Online e-learning course material developed by an independent vendor or instructor, ' ELSE '' END, CASE WHEN ATCOnline = 1 THEN 'Online e-learning course material developed by the ATC, ' ELSE '' END, " +
                "CASE WHEN ctm.Other = 1 THEN CONCAT('Other : ', ctm.`Comment`, ', ') ELSE '' END), 1, CHAR_LENGTH(CONCAT(CASE WHEN Autodesk = 1 THEN 'Autodesk Official Courseware (any Autodesk branded material), ' ELSE '' END, " +
                "CASE WHEN AAP = 1 THEN 'Autodesk Authors and Publishers (AAP) Program Courseware, ' ELSE '' END, CASE WHEN ATC = 1 THEN 'Course material developed by the ATC, ' ELSE '' END, " +
                "CASE WHEN Independent = 1 THEN 'Course material developed by an independent vendor or instructor, ' ELSE '' END, CASE WHEN IndependentOnline = 1 THEN 'Online e-learning course material developed by an independent vendor or instructor, ' ELSE '' END, " +
                "CASE WHEN ATCOnline = 1 THEN 'Online e-learning course material developed by the ATC, ' ELSE '' END, CASE WHEN ctm.Other = 1 THEN CONCAT('Other : ', ctm.`Comment`, ', ') ELSE '' END)) - 2) AS QP5 " +
                "from EvaluationReport er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "LEFT JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
                "LEFT JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId " +
                "LEFT JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId " +
                "INNER JOIN CourseSoftware cf ON c.CourseId = cf.CourseId " +
                "INNER JOIN Products p ON cf.productId = p.productId  " +
                "INNER JOIN ProductVersions pv ON cf.ProductVersionsId = pv.ProductVersionsId " +
                "LEFT JOIN  Products p2 ON cf.productsecondaryId = p2.productId " +
                "LEFT JOIN  ProductVersions pv2 ON cf.ProductSecondaryId = pv2.ProductVersionsId     " +
                "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                "INNER JOIN Ref_countries sc ON er.CountryCode = sc.countries_code " +
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                "LEFT JOIN States ss ON st.StateID = ss.StateID ";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "sc.geo_code IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Country"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "sc.countries_code IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "sc.TerritoryId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "sc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Organization"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Sites"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate != "radSubmittedDate")
                        {
                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += "c.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                            i = i + 1;
                        }
                    }
                }
                if (property.Name.Equals("Primary"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        query += " and p.productId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Secondary"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        query += " and p2.productId = '" + property.Value.ToString() + "' ";
                        i = i + 1;

                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        if (property.Value.ToString() == "\"1\"")
                        {
                            query += "c.PartnerType = 'ATC' ";
                        }
                        else if (property.Value.ToString() == "\"58\"")
                        {
                            //query += "c.PartnerType = 'AAP " + aapcert + "' ";
                            if (!string.IsNullOrEmpty(aapcert))
                            {
                                query += "c.PartnerType in (" + aapcert + ") ";
                            }
                            else
                            {
                                query += "c.PartnerType in ('AAP Event') ";
                            }

                        }
                        else
                        {
                            query += "(c.PartnerType = 'ATC' OR c.PartnerType in (" + aapcert + ") ";
                        }
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("StartDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate == "radCourseEndDate")
                        {
                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += "( c.CompletionDate between '" + property.Value.ToString() + "' and ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("CompletionDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate == "radCourseEndDate")
                        {
                            query += "'" + property.Value.ToString() + "' ) ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("StartDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate == "radSubmittedDate")
                        {
                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += "( er.DateSubmitted between '" + property.Value.ToString() + "' and ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("CompletionDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (rangedate == "radSubmittedDate")
                        {
                            query += "'" + property.Value.ToString() + "' ) ";
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("Limit"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        limit = Int32.Parse(property.Value.ToString());
                    }
                    else
                    {
                        limit = 0;
                    }
                }

                if (property.Name.Equals("Offset"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        ofset = Int32.Parse(property.Value.ToString());
                    }
                    else
                    {
                        ofset = 0;
                    }
                }


                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        UserRole = property.Value.ToString();

                    }
                }
                if (property.Name.Equals("SiteID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        SiteId = property.Value.ToString();
                    }

                }
                if (property.Name.Equals("OrgID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        OrgId = property.Value.ToString();
                    }
                }

            }

            if (UserRole.Contains("ORGANIZATION"))
            {
                query += " AND s.OrgId IN ('" + OrgId.Replace(",", "','") + "') ";
            }

            query += "GROUP BY er.StudentID , er.CourseId ORDER BY StudentEvaluationID DESC ";

            if (limit != 0 || ofset != 0)
            {
                query += " LIMIT " + limit + " OFFSET " + ofset + " ";
            }
            List<dynamic> dynamicModel = new List<dynamic>();

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);


                if (ds.Tables[0].Rows.Count != 0)
                {
                    var evalQuestionCode = "";
                    //added by Soe 28/11/2018
                    DataTable dt1 = ds.Tables[0];
                    foreach (DataRow row in dt1.Rows)
                    {
                        evalQuestionCode = row["EvaluationQuestionCode"].ToString();
                        if (!string.IsNullOrEmpty(evalQuestionCode))
                        {
                            break;
                        }
                    }
                    string query1 = "SELECT EvaluationQuestionJson FROM EvaluationQuestion where EvaluationQuestionCode='" + evalQuestionCode + "' and LanguageiD =35;";
                    MySqlCommand sqlCommand1 = new MySqlCommand(query1);
                    DataSet ds1 = oDb.getDataSetFromSP(sqlCommand1);

                    pType = JsonConvert.DeserializeObject<string>(pType);
                    string TypeName = "";
                    if (pType == "1")
                    {
                        TypeName = "ATC";
                    }
                    else
                    {

                        TypeName = "AAP";

                    }
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    //dynamicModel = dataServices.ToDynamic(ds.Tables[0]);
                    //dynamicModel = dataServices.EvalanswerReformater(ds.Tables[0], TypeName, ds1.Tables[0]);
                    dynamicModel = EvalanswerReformaterExcel(ds.Tables[0], TypeName, ds1.Tables[0]);
                    //DataTable dt = new DataTable();


                    //dynamicModel = await exportService.DataTableToExcel(dynamicModel, TypeName);
                    //}
                    //else if (pType == "58") {
                    // dt = await exportService.DataTableToExcel_AAP(ds.Tables[0], "Test");
                    // }

                    //dynamicModel = dataServices.ToDynamic(dt);

                }
            }
            catch (Exception e)
            {
                result = "[]";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }
            return Ok(result);
        }
        //[HttpPost("DownloadExcelEval")]
        //  public async Task<IActionResult> DownloadExcelEval([FromBody] dynamic data)
        //public async Task<IActionResult> DownloadExcelEval([FromBody] dynamic data)
        [HttpPost("DownloadExcelEval/{partnerdata}")]
        public async Task<IActionResult> DownloadExcelEval(string partnerdata, [FromBody] dynamic data)
        {
            if (!string.IsNullOrEmpty(partnerdata) && data != null)
            {
                var evalReportFilter = new EvaluationReportFilter
                {
                    FilterString = data.ToString(),
                    LastUpdated=DateTime.Now,
                    PartnerType=int.Parse(partnerdata),
                    ReportName= "Download Site Evaluation",
                    Status="New",
                    Submiter=HttpContext.User.GetUserId(),
                    SumitedDate=DateTime.Now
                };

                await db.EvaluationReportFilter.AddAsync(evalReportFilter);
                if (db.SaveChanges()>0)
                {
                    DataSet ds1 = new DataSet();
                    DataTable tblTemp = new DataTable();
                    DataTable dtQuestion = new DataTable();
                    string TypeName = "";
                    bool status = false;
                    string startDate = "";
                    string completionDate = "";
                    string query = "";
                    //ExcelExportService exportService = new ExcelExportService();
                    /* OPTIMASI 2 (From nakarin) */
                    string aapcert = "";
                    string rangedate = "";
                    string UserRole = "";
                    string SiteId = "";
                    string OrgId = "";

                    int i = 0;
                    JObject json = JObject.FromObject(data);

                    foreach (JProperty property in json.Properties())
                    {
                        if (property.Name.Equals("DateRange"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                rangedate = property.Value.ToString();
                            }
                        }

                        if (property.Name.Equals("AAP_Course"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                // aapcert = " Course";
                                aapcert = "'AAP Course'";
                            }
                        }

                        if (property.Name.Equals("AAP_Project"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                //aapcert = " Project";
                                if (!string.IsNullOrEmpty(aapcert))
                                {
                                    aapcert += ",'AAP Project'";
                                }
                                else
                                {
                                    aapcert += "'AAP Project'";
                                }

                            }
                        }
                        if (property.Name.Equals("AAP_Event"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                //aapcert = " Project";
                                if (!string.IsNullOrEmpty(aapcert))
                                {
                                    aapcert += ",'AAP Event'";
                                }
                                else
                                {
                                    aapcert += "'AAP Event'";
                                }

                            }
                        }
                    }

                    query += "Select ea.StudentEvaluationID AS EvalID, CONCAT(st.Firstname, ' ', st.Lastname) AS StudentName, ca.InstructorId, ca.ContactName AS IntructorName, s.SiteId, s.SiteName, c.CourseId,c.`Name` AS CourseTitle, p.productName, DATE_FORMAT(c.StartDate, '%d-%M-%Y') AS CourseStartDate," +
                        "DATE_FORMAT(c.CompletionDate, '%d-%M-%Y') AS CourseEndDate, DATE_FORMAT(er.DateSubmitted, '%d-%M-%Y') AS DateSubmitted, st.Company,IFNULL(st.Phone,'- private -') AS Phone,st.Email,IFNULL(st.Address,'- private -') AS Address," +
                        "IFNULL(st.Address2,'- private -') AS Address2,st.City,sc.countries_name AS Country,IFNULL(StateName,'- private -') AS StateName,IFNULL(st.PostalCode,'- private -') AS PostalCode,EvaluationAnswerJson,ea.EvaluationQuestionCode," +
                        "SUBSTRING(CONCAT(CASE WHEN `Update` = 1 THEN 'Update, ' ELSE '' END, CASE WHEN Essentials = 1 THEN 'Level 1 : Essentials, ' ELSE '' END, CASE WHEN Intermediate = 1 THEN 'Level 2 : Intermediate, ' ELSE '' END, " +
                        "CASE WHEN Advanced = 1 THEN 'Level 3 : Advanced, ' ELSE '' END, CASE WHEN Customized = 1 THEN 'Customized, ' ELSE '' END, CASE WHEN ctl.Other = 1 THEN CONCAT('Other : ', ctl.`Comment`, ', ') ELSE '' END), 1, " +
                        "CHAR_LENGTH(CONCAT(CASE WHEN `Update` = 1 THEN 'Update, ' ELSE '' END, CASE WHEN Essentials = 1 THEN 'Level 1 : Essentials, ' ELSE '' END, CASE WHEN Intermediate = 1 THEN 'Level 2 : Intermediate, ' ELSE '' END, " +
                        "CASE WHEN Advanced = 1 THEN 'Level 3 : Advanced, ' ELSE '' END, CASE WHEN Customized = 1 THEN 'Customized, ' ELSE '' END, CASE WHEN ctl.Other = 1 THEN CONCAT('Other : ', ctl.`Comment`, ', ') ELSE '' END)) - 2) AS QP1, " +
                        "CONCAT(p.productName, ' ', pv.Version) AS QP2, HoursTraining AS QP3, " +
                        "SUBSTRING(CONCAT(CASE WHEN InstructorLed = 1 THEN 'Instructor-led in the classroom, ' ELSE '' END, CASE WHEN `Online` = 1 THEN 'Online or e-learning, ' ELSE '' END), 1, " +
                        "CHAR_LENGTH(CONCAT(CASE WHEN InstructorLed = 1 THEN 'Instructor-led in the classroom, ' ELSE '' END, CASE WHEN `Online` = 1 THEN 'Online or e-learning, ' ELSE '' END)) - 2) AS QP4, " +
                        "SUBSTRING(CONCAT(CASE WHEN Autodesk = 1 THEN 'Autodesk Official Courseware (any Autodesk branded material), ' ELSE '' END, CASE WHEN AAP = 1 THEN 'Autodesk Authors and Publishers (AAP) Program Courseware, ' ELSE '' END, " +
                        "CASE WHEN ATC = 1 THEN 'Course material developed by the ATC, ' ELSE '' END, CASE WHEN Independent = 1 THEN 'Course material developed by an independent vendor or instructor, ' ELSE '' END, " +
                        "CASE WHEN IndependentOnline = 1 THEN 'Online e-learning course material developed by an independent vendor or instructor, ' ELSE '' END, CASE WHEN ATCOnline = 1 THEN 'Online e-learning course material developed by the ATC, ' ELSE '' END, " +
                        "CASE WHEN ctm.Other = 1 THEN CONCAT('Other : ', ctm.`Comment`, ', ') ELSE '' END), 1, CHAR_LENGTH(CONCAT(CASE WHEN Autodesk = 1 THEN 'Autodesk Official Courseware (any Autodesk branded material), ' ELSE '' END, " +
                        "CASE WHEN AAP = 1 THEN 'Autodesk Authors and Publishers (AAP) Program Courseware, ' ELSE '' END, CASE WHEN ATC = 1 THEN 'Course material developed by the ATC, ' ELSE '' END, " +
                        "CASE WHEN Independent = 1 THEN 'Course material developed by an independent vendor or instructor, ' ELSE '' END, CASE WHEN IndependentOnline = 1 THEN 'Online e-learning course material developed by an independent vendor or instructor, ' ELSE '' END, " +
                        "CASE WHEN ATCOnline = 1 THEN 'Online e-learning course material developed by the ATC, ' ELSE '' END, CASE WHEN ctm.Other = 1 THEN CONCAT('Other : ', ctm.`Comment`, ', ') ELSE '' END)) - 2) AS QP5 " +
                        "from EvaluationReport er " +
                        "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                        "LEFT JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
                        "LEFT JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId " +
                        "LEFT JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId " +
                        "INNER JOIN CourseSoftware cf ON c.CourseId = cf.CourseId " +
                        "INNER JOIN Products p ON cf.productId = p.productId  " +
                        "INNER JOIN ProductVersions pv ON cf.ProductVersionsId = pv.ProductVersionsId " +
                        "LEFT JOIN  Products p2 ON cf.productsecondaryId = p2.productId " +
                        "LEFT JOIN  ProductVersions pv2 ON cf.ProductSecondaryId = pv2.ProductVersionsId     " +
                        "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                        "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                        "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                        "INNER JOIN Ref_countries sc ON er.CountryCode = sc.countries_code " +
                        "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                        "LEFT JOIN States ss ON st.StateID = ss.StateID ";


                    foreach (JProperty property in json.Properties())
                    {
                        if (property.Name.Equals("Geo"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { query += " WHERE "; }
                                if (i > 0) { query += " AND "; }
                                query += "sc.geo_code IN (" + property.Value.ToString() + ") ";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("Country"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { query += " WHERE "; }
                                if (i > 0) { query += " AND "; }
                                query += "sc.countries_code IN (" + property.Value.ToString() + ") ";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("Territory"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { query += " WHERE "; }
                                if (i > 0) { query += " AND "; }
                                query += "sc.TerritoryId IN (" + property.Value.ToString() + ") ";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("MarketType"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { query += " WHERE "; }
                                if (i > 0) { query += " AND "; }
                                query += "sc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("Organization"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { query += " WHERE "; }
                                if (i > 0) { query += " AND "; }
                                query += "s.OrgId IN (" + property.Value.ToString() + ") ";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("Sites"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { query += " WHERE "; }
                                if (i > 0) { query += " AND "; }
                                query += "s.SiteId IN (" + property.Value.ToString() + ") ";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("Year"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (rangedate != "radSubmittedDate")
                                {
                                    if (i == 0) { query += " WHERE "; }
                                    if (i > 0) { query += " AND "; }
                                    query += "c.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                                    i = i + 1;
                                }
                            }
                        }
                        if (property.Name.Equals("Primary"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {

                                query += " and p.productId = '" + property.Value.ToString() + "' ";
                                i = i + 1;
                            }
                        }
                        if (property.Name.Equals("Secondary"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {

                                query += " and p2.productId = '" + property.Value.ToString() + "' ";
                                i = i + 1;

                            }
                        }

                        if (property.Name.Equals("PartnerType"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { query += " WHERE "; }
                                if (i > 0) { query += " AND "; }
                                if (property.Value.ToString() == "\"1\"")
                                {
                                    query += "c.PartnerType = 'ATC' ";
                                }
                                else if (property.Value.ToString() == "\"58\"")
                                {
                                    //query += "c.PartnerType = 'AAP " + aapcert + "' ";
                                    if (!string.IsNullOrEmpty(aapcert))
                                    {
                                        query += "c.PartnerType in (" + aapcert + ") ";
                                    }
                                    else
                                    {
                                        query += "c.PartnerType in ('AAP Event') ";
                                    }

                                }
                                else
                                {
                                    query += "(c.PartnerType = 'ATC' OR c.PartnerType in (" + aapcert + ") ";
                                }
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("StartDate"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (rangedate == "radCourseEndDate")
                                {
                                    if (i == 0) { query += " WHERE "; }
                                    if (i > 0) { query += " AND "; }
                                    query += "( c.CompletionDate between '" + property.Value.ToString() + "' and ";
                                    i = i + 1;
                                }
                            }
                        }

                        if (property.Name.Equals("CompletionDate"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (rangedate == "radCourseEndDate")
                                {
                                    query += "'" + property.Value.ToString() + "' ) ";
                                    i = i + 1;
                                }
                            }
                        }

                        if (property.Name.Equals("StartDate"))
                        {
                            startDate = property.Value.ToString();
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (rangedate == "radSubmittedDate")
                                {
                                    if (i == 0) { query += " WHERE "; }
                                    if (i > 0) { query += " AND "; }
                                    query += "( er.DateSubmitted between '" + property.Value.ToString() + "' and ";
                                    i = i + 1;
                                }
                            }
                        }

                        if (property.Name.Equals("CompletionDate"))
                        {
                            completionDate = property.Value.ToString();
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (rangedate == "radSubmittedDate")
                                {
                                    query += "'" + property.Value.ToString() + "' ) ";
                                    i = i + 1;
                                }
                            }
                        }

                        if (property.Name.Equals("Role"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {

                                UserRole = property.Value.ToString();

                            }
                        }
                        if (property.Name.Equals("SiteID"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {

                                SiteId = property.Value.ToString();
                            }

                        }
                        if (property.Name.Equals("OrgID"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                OrgId = property.Value.ToString();
                            }
                        }

                    }

                    if (UserRole.Contains("ORGANIZATION"))
                    {
                        query += " AND s.OrgId IN ('" + OrgId.Replace(",", "','") + "') ";
                    }

                    query += "GROUP BY er.StudentID , er.CourseId ORDER BY StudentEvaluationID DESC ";

                    List<dynamic> dynamicModel = new List<dynamic>();

                    try
                    {
                        var sb = new StringBuilder();
                        sb.AppendFormat(query);
                        var sqlCommand = new MySqlCommand(sb.ToString());
                        var ds = oDb.getDataSetFromSP(sqlCommand);

                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            var evalQuestionCode = "";
                            //added by Soe 28/11/2018
                            tblTemp = ds.Tables[0];
                            foreach (DataRow row in tblTemp.Rows)
                            {
                                evalQuestionCode = row["EvaluationQuestionCode"].ToString();
                                if (!string.IsNullOrEmpty(evalQuestionCode))
                                {
                                    break;
                                }
                            }
                            string query1 = "SELECT EvaluationQuestionJson FROM EvaluationQuestion where EvaluationQuestionCode='" + evalQuestionCode + "' and LanguageiD =35;";
                            MySqlCommand sqlCommand1 = new MySqlCommand(query1);
                            ds1 = oDb.getDataSetFromSP(sqlCommand1);

                            if (partnerdata == "1")
                            {
                                TypeName = "ATC";
                            }
                            else
                            {
                                TypeName = "AAP";
                            }
                            //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                            //ds = MainOrganizationController.EscapeSpecialChar(ds);
                            //dynamicModel = dataServices.ToDynamic(ds.Tables[0]);
                            //dynamicModel = dataServices.EvalanswerReformater(ds.Tables[0], TypeName, ds1.Tables[0]);
                            dtQuestion = GetQuestionList(tblTemp, TypeName, ds1.Tables[0]);
                            dynamicModel = EvalanswerReformaterExcel(ds.Tables[0], TypeName, ds1.Tables[0]);

                            //dynamicModel = await exportService.DataTableToExcel(dynamicModel, TypeName);

                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                var ws = wb.Worksheets.Add("Site Evaluations");
                                ws.Column(1).Width = -1;
                                ws.Cell(2, 4).Value = "Download Site Evaluations";
                                ws.Cell(2, 4).Style.Font.Bold = true;
                                ws.Cell(2, 4).Style.Font.FontSize = 14;
                                ws.Range(2, 4, 2, 12).Merge();
                                ws.Cell(4, 2).Value = "Date Submitted: " + startDate + " - " + completionDate;
                                ws.Cell(4, 2).Style.Font.Bold = true;
                                ws.Cell(4, 2).Style.Font.FontSize = 10;
                                string fileName = @"Assets\Images\autodesk.png";
                                string imagePath = Path.Combine(Directory.GetCurrentDirectory(), fileName);

                                FileStream strm = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
                                ws.AddPicture(strm).MoveTo(ws.Cell(2, 2)).Scale(1);
                                //ws.Cell(13, 3).Style.Font.FontSize = 8;
                                DataTable dt = dataServices.DynamicToDataTable(dynamicModel);
                                ws.Cell(5, 2).InsertTable(dt);
                                var cols = ws.Columns();
                                cols.Style.Alignment.WrapText = true;
                                cols.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                                cols.AdjustToContents(1, 5, 40);

                                var ws2 = wb.Worksheets.Add("Questions");

                                ws2.Column(1).Width = -1;
                                ws2.Cell(2, 3).Value = "Download Site Evaluations";
                                ws2.Cell(2, 3).Style.Font.Bold = true;
                                ws2.Cell(2, 3).Style.Font.FontSize = 14;
                                //ws2.Range(2, 3, 2, 5).Merge();
                                //FileStream strm2 = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
                                //ws2.AddPicture(strm2).MoveTo(ws.Cell(2, 3)).Scale(1);
                                //ws.Cell(8, 3).Style.Font.FontSize = 8;
                                ws2.Cell(4, 2).InsertTable(dtQuestion);
                                var cols2 = ws2.Columns();
                                cols2.Style.Alignment.WrapText = true;
                                cols2.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                                cols2.AdjustToContents(1, 5, 80);
                               
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    string name = "filedownload.xlsx";
                                    var contentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                                    return new FileContentResult(stream.ToArray(), contentType) //"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                    {
                                        FileDownloadName = name
                                    };


                                }
                            }
                        }


                    }

                    catch (Exception ex)
                    {
                        _auditLogServices.LogHistory(new History { Admin = "System", Date = DateTime.Now, Description = ex.Message, Id = "1"});
                        //send mail

                      await  _emailSender.SendMailAsync(AppConfig.Config["EmailSender:SupervisorEmail"], "Error when generate Download Site Evaluation report ", string.Format("Hi, {0}<br/>An error occur when generate the Download Site Evaluation report<br/>Error message:{1}", AppConfig.Config["EmailSender:SupervisorEmail"],ex.Message), null, null);
                    }

                    return Ok();
                }
            }
            return BadRequest();
        }

        public List<dynamic> EvalanswerReformaterExcel(DataTable EvalList, string TypeName, DataTable EvalQuestionList)
        {
            var listObject = dataServices.ToDynamic(EvalList);
            string key = "";
            bool isAAP = false;
            List<dynamic> formattedList = new List<dynamic>();

            var jasonValues = listObject.Select(x => new { x.EvaluationAnswerJson, x.EvalID }).ToList();

            dynamic objQuestion = JsonConvert.DeserializeObject(EvalQuestionList.Rows[0]["EvaluationQuestionJson"].ToString());
            IEnumerable<JToken> questionChoices = objQuestion.SelectTokens("$..elements[0].elements[0].choices");
            var qChoiceValue = questionChoices.AsJEnumerable().Select(x => x).Distinct().ToList();

            Dictionary<string, string> choiceDictionary = new Dictionary<string, string>();

            foreach (var item in qChoiceValue)
            {
                foreach (var i in item)
                {
                    string tempValue = "";
                    string tempText = "";
                    if (i is JObject)
                    {
                        JObject json = JObject.FromObject(i);
                        foreach (JProperty property in json.Properties())
                        {
                            if (property.Name.Equals("value"))
                            {
                                if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                                {
                                    tempValue = property.Value.ToString();
                                }
                            }
                            if (property.Name.Equals("text"))
                            {
                                if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                                {
                                    tempText = property.Value.ToString();
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(tempValue.ToString()) && !choiceDictionary.ContainsKey(tempValue.ToString()))
                        {
                            choiceDictionary.Add(tempValue.ToString(), tempText.ToString());
                        }
                    }
                }
            }

            EvalList.Columns.Remove("EvaluationAnswerJson");
            EvalList.Columns.Remove("EvaluationQuestionCode");

            formattedList = dataServices.ToDynamic(EvalList);

            ICommonServices commonServices = new CommonServices();
            if (TypeName.Equals("ATC"))
            {
                key = "Q";
            }
            else
            {
                key = "QUESTION";
                isAAP = true;
            }
            string tmpName = ""; int o = 1;
            try
            {
                foreach (var evalJason in jasonValues)
                {
                    if (commonServices.IsJasonValid(evalJason.EvaluationAnswerJson) && evalJason.EvaluationAnswerJson != null)
                    {
                        var obj = JsonConvert.DeserializeObject<dynamic>(evalJason.EvaluationAnswerJson);
                        if (obj != null)
                        {
                            int k = 0;
                            System.Dynamic.ExpandoObject objExpandoObject = formattedList.AsEnumerable().FirstOrDefault(x => x.EvalID.Equals(evalJason.EvalID));

                            foreach (JProperty jObj in obj)
                            {
                                Type t = jObj.Value.GetType();
                                string objName = jObj.Name.ToUpper();

                                if (TypeName.Equals("ATC") && objName.Equals("QUESTION1"))
                                {
                                    objName = "Q4";
                                }

                                if (t != typeof(JObject) && t != typeof(JArray))
                                {
                                    if (objName.StartsWith("C") && objName.Length >= 5)
                                    {
                                        objName = "COMMENT";
                                        objExpandoObject.TryAdd(objName, jObj.Value.Value<string>());
                                    }
                                    else
                                    {
                                        objExpandoObject.TryAdd(objName, choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Value.Value<string>())).Value);
                                    }
                                }
                                else if (t == typeof(JObject))
                                {
                                    string concatObj = null;
                                    int i = 0;
                                    foreach (var jObjValue in JObject.Parse(jObj.Value.ToString()))
                                    {
                                        if (!String.IsNullOrEmpty(jObjValue.Value.Value<string>()))
                                        {
                                            if (isAAP == false)
                                            {
                                                if (jObj.Name.ToUpper().Equals(key + "5") || jObj.Name.ToUpper().Equals(key + "6") || jObj.Name.ToUpper().Equals(key + "7") || jObj.Name.ToUpper().Equals(key + "12"))
                                                {
                                                    if (tmpName == "") o = 1;
                                                    else if (tmpName == jObj.Name.ToUpper().ToString()) o++;
                                                    else if (tmpName != jObj.Name.ToUpper().ToString()) o = 1;
                                                    string aliasQ = jObjValue.Key.ToUpper().ToString();
                                                    string temp = aliasQ;

                                                    if (jObj.Name.ToUpper().Equals(key + "5"))
                                                    {
                                                        switch (aliasQ)
                                                        {
                                                            case "CCM":
                                                                temp = o +" Courseware";
                                                                break;
                                                            case "FR_1":
                                                                temp = o + " Computer equipment";
                                                                break;
                                                            case "FR_2":
                                                                temp = o + " Training facility";
                                                                break;
                                                            case "IQ":
                                                                temp = o + " Instructor";
                                                                break;
                                                            case "OE":
                                                                temp = o + " Overall experience";
                                                                break;
                                                            default:
                                                                temp = aliasQ;
                                                                break;
                                                        }
                                                        string name = jObj.Name.ToUpper() +"."+ temp;
                                                        switch (jObjValue.Value.Value<string>())
                                                        {
                                                            case "100":
                                                                objExpandoObject.TryAdd(name, "Very Satisfied");
                                                                break;
                                                            case "75":
                                                                objExpandoObject.TryAdd(name, "Satisfied");
                                                                break;
                                                            case "50":
                                                                objExpandoObject.TryAdd(name, "Neutral");
                                                                break;
                                                            case "25":
                                                                objExpandoObject.TryAdd(name, "Dissatisfied");
                                                                break;
                                                            case "NULL":
                                                                objExpandoObject.TryAdd(name, "Very dissatisfied");
                                                                break;
                                                            default:
                                                                objExpandoObject.TryAdd(name, "Very dissatisfied");
                                                                break;
                                                        }
                                                    }
                                                    else if (jObj.Name.ToUpper().Equals(key + "6"))
                                                    {
                                                        switch (aliasQ)
                                                        {
                                                            case "CCM":
                                                                temp = o + " This course";
                                                                break;
                                                            case "IQ":
                                                                temp = o + " This instructor";
                                                                break;
                                                            case "FR":
                                                                temp = o + " The training facility";
                                                                break;
                                                            default:
                                                                temp = aliasQ;
                                                                break;
                                                        }
                                                        string name = jObj.Name.ToUpper() +"."+ temp;
                                                        switch (jObjValue.Value.Value<string>())
                                                        {
                                                            case "100":
                                                                objExpandoObject.TryAdd(name, "Extremely likely");
                                                                break;
                                                            case "75":
                                                                objExpandoObject.TryAdd(name, "Very likely");
                                                                break;
                                                            case "50":
                                                                objExpandoObject.TryAdd(name, "Likely");
                                                                break;
                                                            case "25":
                                                                objExpandoObject.TryAdd(name, "Somewhat likely");
                                                                break;
                                                            case "NULL":
                                                                objExpandoObject.TryAdd(name, "Not at all likely");
                                                                break;
                                                            default:
                                                                objExpandoObject.TryAdd(name, "Not at all likely");
                                                                break;
                                                        }
                                                    }
                                                    else if (jObj.Name.ToUpper().Equals(key + "7"))
                                                    {
                                                        string name = jObj.Name.ToUpper() + "." + temp;
                                                        switch (jObjValue.Value.Value<string>())
                                                        {
                                                            case "100":
                                                                objExpandoObject.TryAdd(name, "Extremely likely");
                                                                break;
                                                            case "75":
                                                                objExpandoObject.TryAdd(name, "Very likely");
                                                                break;
                                                            case "50":
                                                                objExpandoObject.TryAdd(name, "Somewhat likely");
                                                                break;
                                                            case "25":
                                                                objExpandoObject.TryAdd(name, "A little likely");
                                                                break;
                                                            case "NULL":
                                                                objExpandoObject.TryAdd(name, "Not at all likely");
                                                                break;
                                                            default:
                                                                objExpandoObject.TryAdd(name, "Not at all likely");
                                                                break;
                                                        }
                                                    }
                                                    else if (jObj.Name.ToUpper().Equals(key + "12"))
                                                    {
                                                        switch (aliasQ)
                                                        {
                                                            case "R1":
                                                                temp = o + " I learned new knowledge and skills";
                                                                break;
                                                            case "R2":
                                                                temp = o + " I will be able to apply the new skills I learned";
                                                                break;
                                                            case "R3":
                                                                temp = o + " The new skills I learned will improve my performance";
                                                                break;
                                                            case "R4":
                                                                temp = o + " I'm more likely to recommend Autodesk products as a result of this course";
                                                                break;
                                                            default:
                                                                temp = aliasQ;
                                                                break;
                                                        }
                                                        string name = jObj.Name.ToUpper()+"." + temp;
                                                        switch (jObjValue.Value.Value<string>())
                                                        {
                                                            case "100":
                                                                objExpandoObject.TryAdd(name, "Strongly agree");
                                                                break;
                                                            case "75":
                                                                objExpandoObject.TryAdd(name, "Agree");
                                                                break;
                                                            case "50":
                                                                objExpandoObject.TryAdd(name, "Neutral");
                                                                break;
                                                            case "25":
                                                                objExpandoObject.TryAdd(name, "Disagree");
                                                                break;
                                                            case "NULL":
                                                                objExpandoObject.TryAdd(name, "Strongly disagree");
                                                                break;
                                                            default:
                                                                objExpandoObject.TryAdd(name, "Strongly disagree");
                                                                break;
                                                        }
                                                    }
                                                    tmpName = jObj.Name.ToUpper();
                                                }
                                                else
                                                {
                                                    if (i == 0)
                                                    {
                                                        concatObj = choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObjValue.Value.Value<string>())).Value;
                                                    }
                                                    else
                                                    {
                                                        concatObj = concatObj + ", " + choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObjValue.Value.Value<string>())).Value;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (jObj.Name.ToUpper().Equals(key + "5") || jObj.Name.ToUpper().Equals(key + "6") || jObj.Name.ToUpper().Equals(key + "7") || jObj.Name.ToUpper().Equals(key + "11"))
                                                {
                                                    if (tmpName == "") o = 1;
                                                    else if (tmpName == jObj.Name.ToUpper().ToString()) o++;
                                                    else if (tmpName != jObj.Name.ToUpper().ToString()) o = 1;
                                                    string aliasQ = jObjValue.Key.ToUpper().ToString();
                                                    string temp = aliasQ;

                                                    if (jObj.Name.ToUpper().Equals(key + "5"))
                                                    {
                                                        switch (aliasQ)
                                                        {
                                                            case "OE":
                                                                temp = o + " Overall experience";
                                                                break;
                                                            case "FR":
                                                                temp = o + " Online Training Facility";
                                                                break;
                                                            case "IQ":
                                                                temp = o + " Instructor";
                                                                break;
                                                            case "CCM":
                                                                temp = o + " Courseware";
                                                                break;
                                                            default:
                                                                temp = aliasQ;
                                                                break;
                                                        }
                                                        
                                                        string name = jObj.Name.ToUpper() +"_"+ temp;
                                                        switch (jObjValue.Value.Value<string>())
                                                        {
                                                            case "100":
                                                                objExpandoObject.TryAdd(name, "Strongly agree");
                                                                break;
                                                            case "75":
                                                                objExpandoObject.TryAdd(name, "Agree");
                                                                break;
                                                            case "50":
                                                                objExpandoObject.TryAdd(name, "Neutral");
                                                                break;
                                                            case "25":
                                                                objExpandoObject.TryAdd(name, "Disagree");
                                                                break;
                                                            case "NULL":
                                                                objExpandoObject.TryAdd(name, "Strongly disagree");
                                                                break;
                                                            default:
                                                                objExpandoObject.TryAdd(name, "Strongly disagree");
                                                                break;
                                                        }
                                                    }
                                                    else if (jObj.Name.ToUpper().Equals(key + "6"))
                                                    {
                                                        switch (aliasQ)
                                                        {
                                                            case "CCM":
                                                                temp = o + " This course";
                                                                break;
                                                            case "IQ":
                                                                temp = o + " This Instructor";
                                                                break;
                                                            case "FR":
                                                                temp = o + " The Training Facility";
                                                                break;
                                                            default:
                                                                temp = aliasQ;
                                                                break;
                                                        }
                                                        string name = jObj.Name.ToUpper() +"_"+ temp;
                                                        switch (jObjValue.Value.Value<string>())
                                                        {
                                                            case "100":
                                                                objExpandoObject.TryAdd(name, "Extremely likely");
                                                                break;
                                                            case "75":
                                                                objExpandoObject.TryAdd(name, "Very likely");
                                                                break;
                                                            case "50":
                                                                objExpandoObject.TryAdd(name, "Likely");
                                                                break;
                                                            case "25":
                                                                objExpandoObject.TryAdd(name, "Somewhat likely");
                                                                break;
                                                            case "NULL":
                                                                objExpandoObject.TryAdd(name, "Not at all likely");
                                                                break;
                                                            default:
                                                                objExpandoObject.TryAdd(name, "Not at all likely");
                                                                break;
                                                        }
                                                    }
                                                    else if (jObj.Name.ToUpper().Equals(key + "7"))
                                                    {
                                                        string name = jObj.Name.ToUpper() + "_" + temp;
                                                        switch (jObjValue.Value.Value<string>())
                                                        {
                                                            case "100":
                                                                objExpandoObject.TryAdd(name, "Extremely likely");
                                                                break;
                                                            case "75":
                                                                objExpandoObject.TryAdd(name, "Very likely");
                                                                break;
                                                            case "50":
                                                                objExpandoObject.TryAdd(name, "Likely");
                                                                break;
                                                            case "25":
                                                                objExpandoObject.TryAdd(name, "Somewhat likely");
                                                                break;
                                                            case "NULL":
                                                                objExpandoObject.TryAdd(name, "Not at all likely");
                                                                break;
                                                            default:
                                                                objExpandoObject.TryAdd(name, "Not at all likely");
                                                                break;
                                                        }
                                                    }
                                                    else if (jObj.Name.ToUpper().Equals(key + "11"))
                                                    {
                                                        switch (aliasQ)
                                                        {
                                                            case "R1":
                                                                temp = o + " I learned new knowledge and skills";
                                                                break;
                                                            case "R2":
                                                                temp = o + " I will be able to apply the new skills I learned";
                                                                break;
                                                            case "R3":
                                                                temp = o + " The new skills I learned will improve my performance";
                                                                break;
                                                            case "R4":
                                                                temp = o + " I'm more likely to recommend Autodesk products as a result of this course";
                                                                break;
                                                            default:
                                                                temp = aliasQ;
                                                                break;
                                                        }
                                                        string name = jObj.Name.ToUpper() +"_"+ temp;
                                                        switch (jObjValue.Value.Value<string>())
                                                        {
                                                            case "100":
                                                                objExpandoObject.TryAdd(name, "Strongly agree");
                                                                break;
                                                            case "75":
                                                                objExpandoObject.TryAdd(name, "Agree");
                                                                break;
                                                            case "50":
                                                                objExpandoObject.TryAdd(name, "Neutral");
                                                                break;
                                                            case "25":
                                                                objExpandoObject.TryAdd(name, "Disagree");
                                                                break;
                                                            case "NULL":
                                                                objExpandoObject.TryAdd(name, "Strongly disagree");
                                                                break;
                                                            default:
                                                                objExpandoObject.TryAdd(name, "Strongly disagree");
                                                                break;
                                                        }
                                                    }
                                                    tmpName = jObj.Name.ToUpper();
                                                }
                                                else
                                                {
                                                    if (i == 0)
                                                    {
                                                        concatObj = choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObjValue.Value.Value<string>())).Value;
                                                    }
                                                    else
                                                    {
                                                        concatObj = concatObj + ", " + choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObjValue.Value.Value<string>())).Value;
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                    }

                                    if (!String.IsNullOrEmpty(concatObj))
                                    {
                                        objExpandoObject.TryAdd(objName, concatObj);
                                    }

                                }
                                else if (t == typeof(JArray))
                                {
                                    string concatObj = null;
                                    int i = 0;
                                    foreach (var jObjValue in jObj.Value)
                                    {
                                        if (!String.IsNullOrEmpty(jObjValue.Value<string>()))
                                        {
                                            if (i == 0)
                                            {
                                                concatObj = choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObjValue.Value<string>())).Value;
                                            }
                                            else
                                            {
                                                concatObj = concatObj + ", " + choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObjValue.Value<string>())).Value;
                                            }
                                            i++;
                                        }
                                    }
                                    objExpandoObject.TryAdd(objName, concatObj);

                                }
                                else
                                {
                                    objExpandoObject.TryAdd(jObj.Name.ToUpper(), choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Value)).Value);
                                }
                            }
                            formattedList.Remove(objExpandoObject);
                            formattedList.Add(objExpandoObject);
                        }
                    }
                    else
                    {
                        System.Dynamic.ExpandoObject tempExpandoObject = formattedList.AsEnumerable().FirstOrDefault(x => x.EvalID.Equals(evalJason.EvalID));
                        formattedList.Remove(tempExpandoObject);
                        continue;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return formattedList;
        }

        public DataTable GetQuestionList(DataTable EvalList, string TypeName, DataTable EvalQuestionList)
        {
            var listObject = dataServices.ToDynamic(EvalList);
            string key = "";
            bool isAAP = false;
            Dictionary<string, string> qTxtDictionary = new Dictionary<string, string>();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("QuestionNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("Question", typeof(string)));

            var qp1 = dt.NewRow();
            qp1["QuestionNumber"] = "QP1";
            qp1["Question"] = "What level are you going to teach?";
            dt.Rows.Add(qp1);
            var qp2 = dt.NewRow();
            qp2["QuestionNumber"] = "QP2";
            qp2["Question"] = "What is going to be the primary software used?";
            dt.Rows.Add(qp2);
            var qp3 = dt.NewRow();
            qp3["QuestionNumber"] = "QP3";
            qp3["Question"] = "How many hours of training will be delivered?";
            dt.Rows.Add(qp3);
            var qp4 = dt.NewRow();
            qp4["QuestionNumber"] = "QP4";
            qp4["Question"] = "What will be the training format (Check all that apply)?";
            dt.Rows.Add(qp4);
            var qp5 = dt.NewRow();
            qp5["QuestionNumber"] = "QP5";
            qp5["Question"] = "What training materials will you use in this class (Check all that apply)?";
            dt.Rows.Add(qp5);

            var evalJason = listObject.Select(x => new { x.EvaluationAnswerJson, x.EvalID }).FirstOrDefault();
            dynamic objQuestion = JsonConvert.DeserializeObject(EvalQuestionList.Rows[0]["EvaluationQuestionJson"].ToString());
            IEnumerable<JToken> questionName = objQuestion.SelectTokens("$..elements[0].elements[0].name");
            IEnumerable<JToken> questionTitle = objQuestion.SelectTokens("$..elements[0].elements[0].title");
            var qName = questionName.AsJEnumerable().Select(x => x).ToList();
            var qTitle = questionTitle.AsJEnumerable().Select(x => x).ToList();

            qTxtDictionary = questionName.Zip(qTitle, (k, v) => new { Key = k, Value = v })
                .ToDictionary(x => x.Key.ToString(), x => x.Value.ToString());

            ICommonServices commonServices = new CommonServices();
            if (TypeName.Equals("ATC"))
            {
                key = "Q";
            }
            else
            {
                key = "QUESTION";
                isAAP = true;
            }
            string tmpName = ""; int o = 1;
            try
            {
                if (commonServices.IsJasonValid(evalJason.EvaluationAnswerJson) && evalJason.EvaluationAnswerJson != null)
                {
                    var obj = JsonConvert.DeserializeObject<dynamic>(evalJason.EvaluationAnswerJson);
                    if (obj != null)
                    {
                        foreach (JProperty jObj in obj)
                        {
                            var data = dt.NewRow();

                            Type t = jObj.Value.GetType();

                            string objName = jObj.Name.ToUpper();
                            string objValue = "";

                            if (TypeName.Equals("ATC") && objName.Equals("QUESTION1"))
                            {
                                objName = "Q4";
                                objValue = qTxtDictionary.FirstOrDefault(x => x.Key.Equals("question1")).Value;
                            }
                            else
                            {
                                objValue = qTxtDictionary.FirstOrDefault(x => x.Key.Equals(objName.ToLower())).Value;
                            }

                            if (t != typeof(JObject) && t != typeof(JArray))
                            {
                                if (objName.StartsWith("C") && objName.Length >= 5)
                                {
                                    objName = "COMMENT";
                                }
                                if (objValue == "")
                                    objValue = jObj.Value.Value<string>();
                                data["QuestionNumber"] = objName;
                                data["Question"] = objValue;
                                dt.Rows.Add(data);
                            }
                            else if (t == typeof(JObject))
                            {
                                string concatObj = null;
                                int i = 0;
                               
                                foreach (var jObjValue in JObject.Parse(jObj.Value.ToString()))
                                {
                                    var data1 = dt.NewRow();
                                    if (!String.IsNullOrEmpty(jObjValue.Value.Value<string>()))
                                    {
                                        
                                        if (isAAP == false)
                                        {
                                            if (jObj.Name.ToUpper().Equals(key + "5") || jObj.Name.ToUpper().Equals(key + "6") || jObj.Name.ToUpper().Equals(key + "7") || jObj.Name.ToUpper().Equals(key + "12"))
                                            {
                                                if (tmpName == "") o = 1;
                                                else if (tmpName == jObj.Name.ToUpper().ToString()) o++;
                                                else if (tmpName != jObj.Name.ToUpper().ToString()) o = 1;
                                                string aliasQ = jObjValue.Key.ToUpper().ToString();
                                                string temp = aliasQ;

                                                if (jObj.Name.ToUpper().Equals(key + "5"))
                                                {
                                                    switch (aliasQ)
                                                    {
                                                        case "OE":
                                                            temp = o +" Overall experience";
                                                            break;
                                                        case "FR_1":
                                                            temp = o + " Computer equipment";
                                                            break;
                                                        case "FR_2":
                                                            temp = o + " Training facility";
                                                            break;
                                                        case "IQ":
                                                            temp = o + " Instructor";
                                                            break;
                                                        case "CCM":
                                                            temp = o + " Courseware";
                                                            break;                                                        
                                                        default:
                                                            temp = aliasQ;
                                                            break;
                                                    }
                                                }
                                                else if (jObj.Name.ToUpper().Equals(key + "6"))
                                                {
                                                    switch (aliasQ)
                                                    {
                                                        case "CCM":
                                                            temp = o + " This course";
                                                            break;
                                                        case "IQ":
                                                            temp = o + " This instructor";
                                                            break;
                                                        case "FR":
                                                            temp = o + " The training facility";
                                                            break;
                                                        default:
                                                            temp = aliasQ;
                                                            break;
                                                    }
                                                }
                                                else if (jObj.Name.ToUpper().Equals(key + "12"))
                                                {
                                                    switch (aliasQ)
                                                    {
                                                        case "R1":
                                                            temp = o + " I learned new knowledge and skills";
                                                            break;
                                                        case "R2":
                                                            temp = o + " I will be able to apply the new skills I learned";
                                                            break;
                                                        case "R3":
                                                            temp = o + " The new skills I learned will improve my performance";
                                                            break;
                                                        case "R4":
                                                            temp = o + " I'm more likely to recommend Autodesk products as a result of this course";
                                                            break;
                                                        default:
                                                            temp = aliasQ;
                                                            break;
                                                    }
                                                }
                                                tmpName = jObj.Name.ToUpper();
                                                string name = jObj.Name.ToUpper()+"." + temp;
                                                string value = qTxtDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Name.ToLower())).Value;
                                                data1["QuestionNumber"] = name;
                                                data1["Question"] = value;
                                                dt.Rows.Add(data1);
                                            }
                                            else
                                            {
                                                if (i == 0)
                                                {
                                                    concatObj = jObjValue.Value.Value<string>();
                                                }
                                                else
                                                {
                                                    concatObj = concatObj + ", " + jObjValue.Value.Value<string>();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (jObj.Name.ToUpper().Equals(key + "5") || jObj.Name.ToUpper().Equals(key + "6") || jObj.Name.ToUpper().Equals(key + "7") || jObj.Name.ToUpper().Equals(key + "11"))
                                            {
                                                if (tmpName == "") o = 1;
                                                else if (tmpName == jObj.Name.ToUpper().ToString()) o++;
                                                else if (tmpName != jObj.Name.ToUpper().ToString()) o = 1;
                                                string aliasQ = jObjValue.Key.ToUpper().ToString();
                                                string temp = aliasQ;

                                                if (jObj.Name.ToUpper().Equals(key + "5"))
                                                {
                                                    switch (aliasQ)
                                                    {
                                                        case "OE":
                                                            temp = o + " Overall experience";
                                                            break;
                                                        case "FR":
                                                            temp = o + " Online Training Facility";
                                                            break;
                                                        case "IQ":
                                                            temp = o + " Instructor";
                                                            break;
                                                        case "CCM":
                                                            temp = o + " Courseware";
                                                            break;
                                                        default:
                                                            temp = aliasQ;
                                                            break;
                                                    }
                                                }
                                                else if (jObj.Name.ToUpper().Equals(key + "6"))
                                                {
                                                    switch (aliasQ)
                                                    {
                                                        case "CCM":
                                                            temp = o + " This course";
                                                            break;
                                                        case "IQ":
                                                            temp = o + " This Instructor";
                                                            break;
                                                        case "FR":
                                                            temp = o + " The Training Facility";
                                                            break;
                                                        default:
                                                            temp = aliasQ;
                                                            break;
                                                    }
                                                }
                                                else if (jObj.Name.ToUpper().Equals(key + "11"))
                                                {
                                                    switch (aliasQ)
                                                    {
                                                        case "R1":
                                                            temp = o + " I learned new knowledge and skills";
                                                            break;
                                                        case "R2":
                                                            temp = o + " I will be able to apply the new skills I learned";
                                                            break;
                                                        case "R3":
                                                            temp = o + " The new skills I learned will improve my performance";
                                                            break;
                                                        case "R4":
                                                            temp = o + " I'm more likely to recommend Autodesk products as a result of this course";
                                                            break;
                                                        default:
                                                            temp = aliasQ;
                                                            break;
                                                    }
                                                }
                                                tmpName = jObj.Name.ToUpper();
                                                string name = jObj.Name.ToUpper() +"_"+ temp;
                                                string value = qTxtDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Name.ToLower())).Value;

                                                data1["QuestionNumber"] = name;
                                                data1["Question"] = value;
                                                dt.Rows.Add(data1);
                                            }
                                            else
                                            {
                                                if (i == 0)
                                                {
                                                    concatObj = jObjValue.Value.Value<string>();
                                                }
                                                else
                                                {
                                                    concatObj = concatObj + ", " + jObjValue.Value.Value<string>();
                                                }
                                            }
                                        }
                                        i++;
                                    }
                                }

                                if (!String.IsNullOrEmpty(concatObj))
                                {
                                    data["QuestionNumber"] = objName;
                                    data["Question"] = concatObj;
                                    dt.Rows.Add(data);
                                }
                            }
                            else if (t == typeof(JArray))
                            {
                                string concatObj = null;
                                int i = 0;
                                foreach (var jObjValue in jObj.Value)
                                {
                                    if (!String.IsNullOrEmpty(jObjValue.Value<string>()))
                                    {
                                        if (i == 0)
                                        {
                                            concatObj = jObjValue.Value<string>();
                                        }
                                        else
                                        {
                                            concatObj = concatObj + ", " + jObjValue.Value<string>();
                                        }
                                        i++;
                                    }
                                }
                                string value = qTxtDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Name.ToLower())).Value;
                                data["QuestionNumber"] = objName;
                                data["Question"] = value;
                                dt.Rows.Add(data);
                            }
                            else
                            {
                                data["QuestionNumber"] = jObj.Name.ToUpper();
                                data["Question"] = jObj.Value;
                                dt.Rows.Add(data);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return dt;
        }

        [HttpPost("AuditReport/{market_type}/{territory}")]
        public IActionResult Audit(string market_type, string territory, [FromBody] dynamic data)
        {
            string query = "";

            // query += "SELECT DISTINCT s.SiteId,r.RoleCode,t.Territory_Name,m.MarketType,s.SiteName,cc.ContactName AS InstructorName,DATE_FORMAT( ea.CreatedDate, '%d-%m-%Y' ) AS DateSubmitted,ts.FirstName,ts.Lastname,ts.Phone," +
            // "ts.Company,ts.Email,ts.Address,dc.KeyValue AS `Language`,rc.countries_name,c.CourseCode,c.`Name`,DATE_FORMAT( c.StartDate, '%d-%m-%Y' ) AS CourseStartDate,DATE_FORMAT( c.CompletionDate, '%d-%m-%Y' ) AS CourseCompletionDate,er.ScoreOP " +
            // "FROM EvaluationAnswer ea " +
            // "INNER JOIN CourseStudent cs ON ea.StudentId = cs.StudentID " +
            // "INNER JOIN EvaluationReport er ON cs.StudentID = er.StudentID " +
            // "INNER JOIN Courses c ON cs.CourseId = cs.CourseId " +
            // "INNER JOIN tblStudents ts ON cs.StudentID = ts.StudentID " +
            // "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
            // "INNER JOIN Contacts_All cc ON c.ContactId = cc.ContactId " +
            // "INNER JOIN Ref_countries rc ON ts.CountryID = rc.countries_code " +
            // "LEFT JOIN Ref_territory t ON rc.TerritoryId = t.TerritoryId " +
            // "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId " +
            // "INNER JOIN Roles r ON sr.RoleId = r.RoleId " +
            // "LEFT JOIN MarketType m ON rc.MarketTypeId = m.MarketTypeId " +
            // "LEFT JOIN Dictionary dc ON ts.PrimaryLanguage = dc.`Key` " +
            // "WHERE r.RoleId = '1' ";

            //query += 
            //    "SELECT s.SiteId, s.SiteName, cc.ContactName AS InstructorName, ts.FirstName, ts.Lastname, ts.Phone, ts.Company, ts.Email, ts.Address, c.CourseCode, c.`Name`, DATE_FORMAT( c.StartDate, '%d-%m-%Y' ) AS CourseStartDate, DATE_FORMAT( c.CompletionDate, '%d-%m-%Y' ) AS CourseCompletionDate, r.RoleCode, t.Territory_Name, m.MarketType, DATE_FORMAT( ea.CreatedDate, '%d-%m-%Y' ) AS DateSubmitted, dc.KeyValue AS `Language`, rc.countries_name, er.ScoreOP "+
            //    "FROM (SELECT StudentID, CourseID, CreatedDate from EvaluationAnswer where CreatedDate > NOW() - INTERVAL 7 DAY AND CreatedDate <= NOW() ORDER BY CreatedDate DESC) AS ea "+
            //    "INNER JOIN EvaluationReport er ON ea.StudentID = er.StudentID AND ea.CourseID = er.CourseID  INNER JOIN Courses c ON ea.CourseId = c.CourseId INNER JOIN tblStudents ts ON ea.StudentID = ts.StudentID INNER JOIN Main_site s ON c.SiteId = s.SiteId INNER JOIN Contacts_All cc ON c.ContactId = cc.ContactId INNER JOIN Ref_countries rc ON ts.CountryID = rc.countries_code LEFT JOIN Ref_territory t ON rc.TerritoryId = t.TerritoryId INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId INNER JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN MarketType m ON rc.MarketTypeId = m.MarketTypeId LEFT JOIN Dictionary dc ON ts.PrimaryLanguage = dc.`Key` ";

            query += "SELECT InstructorName,SiteName,SiteId,SiteCountryCode,CourseId,CourseTitle,ScoreOP,ContactId,CourseStartDate,CourseCompletionDate,RoleCode,Territory_Name,countries_name,MarketType,Convert(COUNT(Distinct (StudentID)), signed) AS Evals FROM " +
                "(SELECT RoleCode,Firstname,Lastname,Phone,Company,Email,Address,InstructorName,SiteName,ts.SiteId,SiteCountryCode,CourseId,CourseTitle,StudentID,ScoreOP,ContactId,CourseStartDate,CourseCompletionDate FROM " +
                "(SELECT Firstname,Lastname,Phone,Company,Email,Address,InstructorName,SiteName,SiteId,SiteCountryCode,CourseId,CourseTitle,ca.StudentID,ScoreOP,ContactId,CourseStartDate,CourseCompletionDate FROM " +
                "(SELECT ContactName AS InstructorName,SiteName,SiteId,SiteCountryCode,CourseId,CourseTitle,StudentID,ScoreOP,s.ContactId,CourseStartDate,CourseCompletionDate FROM " +
                "(SELECT SiteName,c.SiteId,SiteCountryCode,CourseId,CourseTitle,StudentID,ScoreOP,ContactId,CourseStartDate,CourseCompletionDate FROM " +
                "(SELECT er.SiteId,StudentID,er.CourseId,ScoreOP,`Name` AS CourseTitle,ContactId,DATE_FORMAT( c.StartDate, '%d-%M-%Y' ) AS CourseStartDate,DATE_FORMAT( c.CompletionDate, '%d-%M-%Y' ) AS CourseCompletionDate FROM " +
                "( SELECT * FROM EvaluationReport ";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if(i==0 || i>0){query += " AND ";}
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                //if (property.Name.Equals("Region"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        // if(i==0 || i>0){query += " AND ";}
                //        if(i==0){query += " WHERE ";}
                //        if(i>0){query += " AND ";}
                //        query += "rc.region_code IN (SELECT region_code FROM Ref_region WHERE region_id IN("+property.Value.ToString()+")) ";
                //        i = i + 1;
                //    }
                //}


                //if (property.Name.Equals("Territory"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        // if(i==0 || i>0){query += " AND ";}
                //        if(i==0){query += " WHERE ";}
                //        if(i>0){query += " AND ";}
                //        query += "rc.TerritoryId IN (" + property.Value.ToString() + ") ";
                //        i = i + 1;
                //    }
                //}

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if(i==0 || i>0){query += " AND ";}
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Date"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if(i==0 || i>0){query += " AND ";}
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                //if (property.Name.Equals("DateEnd"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        // if(i==0 || i>0){query += " AND ";}
                //        if(i==0){query += " WHERE ";}
                //        if(i>0){query += " AND ";}
                //        query += "MONTH(c.CompletionDate) = " + property.Value.ToString() + " ";
                //        i = i + 1;
                //    }
                //}

                //if (property.Name.Equals("DateSubmitted"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        // if(i==0 || i>0){query += " AND ";}
                //        if(i==0){query += " WHERE ";}
                //        if(i>0){query += " AND ";}
                //        query += "MONTH(ea.CreatedDate) = " + property.Value.ToString() + " ";
                //        i = i + 1;
                //    }
                //}

                //if (property.Name.Equals("MarketType"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        // if(i==0 || i>0){query += " AND ";}
                //        if(i==0){query += " WHERE ";}
                //        if(i>0){query += " AND ";}
                //        query += "rc.MarketTypeId IN (" + property.Value.ToString() + ") ";
                //        i = i + 1;
                //    }
                //}
            }

            // query += " GROUP BY er.EvaluationReportId";
            query += " LIMIT 1000 ) AS er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId) AS c " +
                "INNER JOIN Main_site s ON c.SiteId = s.SiteId) AS s " +
                "INNER JOIN Contacts_All ca ON s.ContactId = ca.ContactId) AS ca " +
                "INNER JOIN tblStudents ts ON ca.StudentID = ts.StudentID) AS ts " +
                "INNER JOIN SiteRoles sr ON ts.SiteId = sr.SiteId " +
                "INNER JOIN Roles r ON sr.RoleId = r.RoleId) AS r " +
                "INNER JOIN Ref_countries rc ON r.SiteCountryCode = rc.countries_code " +
                "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                "INNER JOIN MarketType m ON rc.MarketTypeId = m.MarketTypeId ";

            if (market_type != "null" && territory != "null")
            {
                query += "WHERE rc.MarketTypeId IN (" + market_type + ") AND rc.TerritoryId IN (" + territory + ")";
            }
            else if (market_type != "null" && territory == "null")
            {
                query += "WHERE rc.MarketTypeId IN (" + market_type + ")";
            }
            else if (market_type == "null" && territory != "null")
            {
                query += "WHERE rc.TerritoryId IN (" + territory + ")";
            }

            query += " GROUP BY CourseId ORDER BY CourseId";
            List<dynamic> dynamicModel = new List<dynamic>();


            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                    DataTable dt = ds.Tables[0];
                    dynamicModel = dataServices.ToDynamic(dt);
                }
            }
            catch
            {
                result = "[]";
            }
            //finally
            //{
            //    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            //}
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }
            return Ok(result);
            // return Ok(result);
        }

        [HttpGet("ComparisonRpt/{obj}")]
        public IActionResult ComparisonReport(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "SELECT DISTINCT o.ATCOrgId, s.ATCSiteId, s.SiteName, eq.EvaluationQuestionCode, ts.Firstname, ts.Lastname FROM tblStudents ts, Main_site s, Main_organization o, EvaluationQuestion eq, EvaluationAnswer ea, Courses c, Roles r, SiteRoles sr " +
            "WHERE ts.StudentID = ea.StudentId AND ea.EvaluationQuestionCode = eq.EvaluationQuestionCode AND eq.CourseId = c.CourseCode AND c.SiteId = s.SiteId AND s.OrgId = o.OrgId AND sr.SiteId = s.SiteId AND sr.RoleId = r.RoleId ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "s.SiteCountryCode IN (SELECT countries_code FROM Ref_countries WHERE countries_id IN (" + property.Value.ToString() + ")) ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "YEAR(c.StartDate) = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Date"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "MONTH(c.StartDate) = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "sr.RoleId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }

            query += " ORDER BY eq.EvaluationQuestionCode";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> Insert([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            JObject o4;
            JObject evaQuestion;
            JObject student;
            JObject sites;
            string res = "";
            string StudentID = null;
            sb = new StringBuilder();
            sb.Append("SELECT StudentID FROM CourseStudent WHERE CourseId = '" + data.CourseId + "' and StudentID ='" + data.StudentID + "' limit 1;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            if (ds.Tables[0].Rows.Count != 0)
            {
                StudentID = ds.Tables[0].Rows[0]["StudentID"].ToString();
            }
                
            if (string.IsNullOrEmpty(StudentID))
            {
                res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"System found out you haven't enrolled this course, Please check your enroll list\"" +
                "}";
            }
            else
            {
                res =
                               "{" +
                               "\"code\":\"0\"," +
                               "\"message\":\"Insert Data Failed\"" +
                               "}";

                sb = new StringBuilder();
                sb.AppendFormat("select count(*) as count from EvaluationAnswer;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o1 = JObject.Parse(result);
                count1 = Int32.Parse(o1["count"].ToString());
                //Console.WriteLine(count1); 


                try
                {
                    DateTime now = DateTime.Now;

                    string query = "SELECT DISTINCT EvaluationQuestionCode, CountryCode FROM EvaluationQuestion WHERE CertificateTypeID = " + data.CertificateType + " AND Year = '" + data.FyId + "' AND LanguageID = " + data.LanguageId + " GROUP BY `Year`";
                    sqlCommand = new MySqlCommand(query);
                    // Console.WriteLine(query);
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    evaQuestion = JObject.Parse(result);

                    //Get Last Student Evaluation ID from EvaluationAnswer
                    string sql_xxx = "SELECT MAX(CAST(SUBSTR(StudentEvaluationID,1,10) AS UNSIGNED)) + 1 AS last_eval_id FROM EvaluationAnswer";
                    sqlCommand = new MySqlCommand(sql_xxx);
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    o4 = JObject.Parse(result);

                    string student_eval_id = "000" + o4["last_eval_id"].ToString() + "FY" + data.Indicator; //generate student evaluation id
                                                                                                            // Console.WriteLine(student_eval_id);
                                                                                                            //var settings = new JsonSerializerSettings
                                                                                                            //{
                                                                                                            //    StringEscapeHandling = StringEscapeHandling.EscapeHtml
                                                                                                            //};
                    var jsonString = data.EvaluationAnswerJson.ToString();


                    var evalJson = JObject.Parse(jsonString);
                    //var json = JsonConvert.DeserializeAnonymousType(evalJson, settings);
                    JObject evalJsonObject = JObject.FromObject(evalJson);
                    if (evalJsonObject.ContainsKey("comment"))
                    {
                        if (evalJsonObject["comment"].ToString().Contains("'"))
                        {
                            evalJsonObject["comment"] = evalJsonObject["comment"].ToString().Replace("'", "''");
                        }

                        if (evalJsonObject["comment"].ToString().Contains("\""))
                        {
                            evalJsonObject["comment"] = evalJsonObject["comment"].ToString().Replace("\"", " ");
                        }

                    }
                    if (evalJsonObject.ContainsKey("Comments"))
                    {
                        if (evalJsonObject["Comments"].ToString().Contains("'"))
                        {
                            evalJsonObject["Comments"] = evalJsonObject["Comments"].ToString().Replace("'", "''");
                        }
                        if (evalJsonObject["Comments"].ToString().Contains("\""))
                        {
                            evalJsonObject["Comments"] = evalJsonObject["Comments"].ToString().Replace("\"", " ");
                        }

                    }

                    string checkDuplicateQuery = "SELECT Count(*) as isExist FROM EvaluationAnswer WHERE StudentID= '" + data.StudentID + "' AND CourseId= '" + data.CourseId + "' ";
                    sqlCommand = new MySqlCommand(checkDuplicateQuery);
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    int isExit = 0;
                    if (ds.Tables.Count > 0)
                    {
                        isExit = Convert.ToInt32(ds.Tables[0].Rows[0]["isExist"].ToString());
                    }

                    if (isExit == 0)
                    {

                        //SELECT CONCAT('000', CAST((MAX(CAST(SUBSTR(StudentEvaluationID, 1, 10) AS UNSIGNED)) + 1)  AS nchar), 'FY', '19') FROM EvaluationAnswer
                        string sql = "INSERT INTO EvaluationAnswer " +
                                "( EvaluationQuestionCode,StudentEvaluationID, CourseId, CountryCode, StudentId, EvaluationAnswerJson, Status, CreatedBy, CreatedDate, Quarter) " +
                                "VALUES ( '" + evaQuestion["EvaluationQuestionCode"] + "','" + student_eval_id + "','" + data.CourseId + "','" + evaQuestion["CountryCode"] + "','" + data.StudentID + "','" + evalJsonObject +
                                "','" + data.Status + "','" + data.CreatedBy + "','" + now.ToString("yyyy-MM-dd hh:mm:ss") + "'," + data.Quarter + ");";
                        sqlCommand = new MySqlCommand(sql);
                        // Console.WriteLine(sql);
                        ds = oDb.getDataSetFromSP(sqlCommand);


                        //ini untuk masukin data tambahan di tabel eva report
                        //MONTH(StartDate) As Month
                        string sql_sites = "SELECT DISTINCT SiteCountryCode,geo_code,CASE CertificateType WHEN 0 THEN 1 ELSE 58 END AS RoleId,c.SiteId,CertificateType,Month FROM " +
                    "( SELECT SiteId,MONTH(StartDate) As Month,CASE PartnerType WHEN 'AAP Course' THEN 1 " +
                    "WHEN 'AAP Project' THEN 2 WHEN 'AAP Event' THEN 3 ELSE 0 END AS CertificateType FROM Courses WHERE CourseId = '" + data.CourseId + "' ) AS c " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code ";
                        sqlCommand = new MySqlCommand(sql_sites);
                        // Console.WriteLine(sql_sites);
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                        sites = JObject.Parse(result);
                        int NbAnswerOP = Convert.ToInt32(data.NbAnswerCCM.ToString()) + Convert.ToInt32(data.NbAnswerFR.ToString()) + Convert.ToInt32(data.NbAnswerIQ.ToString());
                        int ScoreOP = Convert.ToInt32(data.ScoreCCM.ToString()) + Convert.ToInt32(data.ScoreFR.ToString()) + Convert.ToInt32(data.ScoreIQ.ToString());
                        string sql_report = "INSERT INTO EvaluationReport " +
                            "( CourseId, StudentID, NbAnswerCCM, ScoreCCM, NbAnswerFR, ScoreFR, NbAnswerIQ, ScoreIQ, NbAnswerOE, ScoreOE, NbAnswerOP, ScoreOP, Status, FYIndicatorKey, SiteId, CountryCode, GeoCode, RoleId, CertificateType, Month,DateSubmitted,Quarter,ActualDateSubmitted) " +
                            "VALUES ( '" + data.CourseId + "','" + data.StudentID + "'," + data.NbAnswerCCM + "," + data.ScoreCCM + "," + data.NbAnswerFR + "," + data.ScoreFR + "," + data.NbAnswerIQ +
                            "," + data.ScoreIQ + "," + data.NbAnswerOE + "," + data.ScoreOE + "," + NbAnswerOP + "," + ScoreOP + ",'" + data.Status + "','" + data.FyId + "','" + sites["SiteId"].ToString() + "','" + sites["SiteCountryCode"].ToString() + "','" + sites["geo_code"].ToString() + "','" + sites["RoleId"].ToString() + "','" + sites["CertificateType"].ToString() + "','" + now.Month.ToString() + "','" + now.ToString("yyyy-MM-dd hh:mm:ss") + "'," + data.Quarter + ",'" + now.ToString("yyyy-MM-dd hh:mm:ss") + "');";
                        sqlCommand = new MySqlCommand(sql_report);
                        // Console.WriteLine(sql_report);
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        string sql_check = "SELECT EvaluationReportId FROM EvaluationReport WHERE CourseId = '" + data.CourseId + "' AND StudentID = '" + data.StudentID + "'";
                        sqlCommand = new MySqlCommand(sql_check);
                        // Console.WriteLine(sql_check);
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        #region Old Code

                        // sb = new StringBuilder();
                        // sb.AppendFormat("SELECT CourseStudentId FROM CourseStudent WHERE CourseId = '" + data.CourseId + "' AND StudentID = '" + data.StudentID + "';");
                        // // Console.WriteLine(sb.ToString());
                        // sqlCommand = new MySqlCommand(sb.ToString());
                        // ds = oDb.getDataSetFromSP(sqlCommand);
                        // result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                        // o3 = JObject.Parse(result);

                        // string sql_update_studentcourse =
                        //     "UPDATE CourseStudent SET " +
                        //     "SurveyTaken='1', " +
                        //     "CourseTaken='1' " +
                        //     "WHERE CourseStudentId='" + o3["CourseStudentId"].ToString() + "'";
                        // // Console.WriteLine(sql_update_studentcourse);
                        // sqlCommand = new MySqlCommand(sql_update_studentcourse);
                        // ds = oDb.getDataSetFromSP(sqlCommand);

                        // Console.WriteLine(ds.Tables.Count);

                        #endregion

                        if (ds.Tables.Count > 0)
                        {
                            string sql_update_studentcourse =
                                "UPDATE CourseStudent SET " +
                                "SurveyTaken='1', " +
                                "CourseTaken='1' " +
                                "WHERE CourseId = '" + data.CourseId + "' AND StudentID = '" + data.StudentID + "'";
                            // Console.WriteLine(sql_update_studentcourse);
                            sqlCommand = new MySqlCommand(sql_update_studentcourse);
                            ds = oDb.getDataSetFromSP(sqlCommand);
                            DataSet ds2 = new DataSet();
                            sb = new StringBuilder();
                            sb.AppendFormat("select s.Email, s.Email2, Concat(s.Firstname, ' ', s.Lastname) as StudentName, eb.`subject`, eb.body_email from tblStudents s LEFT JOIN EmailBody eb ON CASE WHEN (select count(*) from EmailBody where  EmailBody.Key = s.LanguageID and body_type = '3') > 0 THEN s.LanguageID ELSE 35 END = eb.`Key` AND eb.body_type = '9' where StudentID = " + data.StudentID + "");
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds2 = oDb.getDataSetFromSP(sqlCommand);
                            result = MySqlDb.GetJSONObjectString(ds2.Tables[0]);
                            student = JObject.Parse(result);

                            string mailto = student["Email"].ToString();
                            string subject = student["subject"].ToString();
                            string message = "";

                            message += student["body_email"].ToString();

                            message = message.Replace("[StudentName]", student["StudentName"].ToString());

                            Boolean resmail = await _emailSender.SendMailAsync(mailto, subject, message);

                            if (resmail == true)
                            {
                                // return Ok("{\"code\":\"1\",\"msg\":\"Message sent\"}");   
                                //Console.WriteLine("{\"code\":\"1\",\"msg\":\"Message sent\"}");
                            }
                            else
                            {
                                // return Ok("{\"code\":\"0\",\"msg\":\"Message not sent\"}");  
                                // Console.WriteLine("{\"code\":\"0\",\"msg\":\"Message not sent\"}");
                            }
                            sb = new StringBuilder();
                            sb.AppendFormat("SELECT COUNT(*) AS count FROM EvaluationAnswer;");
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                            o2 = JObject.Parse(result);
                            count2 = Int32.Parse(o2["count"].ToString());
                            //Console.WriteLine(count2);

                            if (count2 > count1)
                            {
                                res =
                                    "{" +
                                    "\"code\":\"1\"," +
                                    "\"message\":\"Insert Data Success\"" +
                                    "}";
                            }

                        }

                    }
                    else
                    {
                        res =
                            "{" +
                            "\"code\":\"0\"," +
                            "\"message\":\"System found out you already submitted for this Survey, Please Contact System Admin :\"" +
                            "}";
                    }
                }
                catch (Exception e)
                {
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Insert Data Failed :\"" + e +
                        "}";
                }
                finally
                {
                    #region Old Code
                    //sb = new StringBuilder();
                    //sb.AppendFormat("select s.Email, s.Email2, Concat(s.Firstname, ' ', s.Lastname) as StudentName, eb.`subject`, eb.body_email from tblStudents s LEFT JOIN EmailBody eb ON CASE WHEN s.LanguageID NOT IN ('35') THEN '35' ELSE s.LanguageID END = eb.`Key` AND eb.body_type = '9' where StudentID = " + data.StudentID + "");
                    //sqlCommand = new MySqlCommand(sb.ToString());
                    //ds = oDb.getDataSetFromSP(sqlCommand);
                    //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    //student = JObject.Parse(result);

                    //string mailto = student["Email"].ToString();
                    //string subject = student["subject"].ToString();
                    //string message = "";

                    //message += student["body_email"].ToString();

                    //message = message.Replace("[Name]", student["StudentName"].ToString());

                    //Boolean resmail = await _emailSender.SendEmailAsync(mailto, subject, message);

                    //if (resmail == true)
                    //{
                    //    // return Ok("{\"code\":\"1\",\"msg\":\"Message sent\"}");   
                    //    Console.WriteLine("{\"code\":\"1\",\"msg\":\"Message sent\"}");
                    //}
                    //else
                    //{
                    //    // return Ok("{\"code\":\"0\",\"msg\":\"Message not sent\"}");  
                    //    Console.WriteLine("{\"code\":\"0\",\"msg\":\"Message not sent\"}");
                    //}



                    #endregion

                }

            }


            return Ok(res);
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            try
            {
                string sql =
                "update EvaluationAnswer set " +
                    "EvaluationQuestionCode='" + data.EvaluationQuestionCode + "', " +
                    "CourseId='" + data.CourseId + "', " +
                    "CountryCode='" + data.CountryCode + "', " +
                    "StudentId='" + data.StudentId + "', " +
                    "EvaluationAnswerJson='" + data.EvaluationAnswerJson + "', " +
                    "Status='" + data.Status + "', " +
                    "CreatedBy='" + data.CreatedBy + "', " +
                    "CreatedDate='" + data.CreatedDate + "' " +
                    "where EvaluationAnswerId='" + id + "'";
                sqlCommand = new MySqlCommand(sql);
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from EvaluationAnswer where EvaluationAnswerId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);

            if (o3["EvaluationQuestionCode"].ToString() == data.EvaluationQuestionCode.ToString())
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }

            return Ok(res);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            try
            {
                //sb = new StringBuilder();
                //sb.AppendFormat("delete from EvaluationAnswer where EvaluationAnswerId = '" + id + "'");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);

                var evalAnswer = db.EvaluationAnswer.FirstOrDefault(x => x.EvaluationAnswerId == id);
                if (evalAnswer != null)
                {
                    db.EvaluationAnswer.Remove(evalAnswer);
                    var deleted = db.SaveChanges();
                    if (deleted > 0)
                    {
                        var admin = HttpContext.User.GetUserId();
                        _auditLogServices.LogHistory(new History { Admin = admin, Date = DateTime.Now, Description = "Delete EvaluationAnswer <br/> EvaluationAnswerId : " + id, Id = id.ToString() });
                        res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                    }
                }
            }
            catch
            {
            }
            //finally
            //{
            //    sb = new StringBuilder();
            //    sb.AppendFormat("select * from EvaluationAnswer where EvaluationAnswerId  =" + id + ";");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);

            //    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //    o3 = JObject.Parse(result);
            //    Console.WriteLine(o3.Count);
            //    if (o3.Count < 1)
            //    {
            //        res =
            //            "{" +
            //            "\"code\":\"1\"," +
            //            "\"message\":\"Delete Data Success\"" +
            //            "}";
            //    }
            //}

            return Ok(res);
        }




    }
}