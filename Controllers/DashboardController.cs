using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.ViewModel;
// using autodesk.Models;
namespace autodesk.Controllers
{
    [Route("api/DashboardPartner")]
    public class DashboardPartner : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardPartner(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select t.Territory_Name, Count(*) as TotalPartnerThisMonth from Main_organization as m inner join Ref_countries as tc on tc.countries_code = m.RegisteredCountryCode inner join Ref_territory as t on t.TerritoryId = tc.TerritoryId where month(m.DateAdded) = month(current_date()) and year(m.DateAdded) = year(current_date()) and m.Status = 'A' group by t.Territory_Name;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("{time}/where/{obj}")]
        public IActionResult where(string time, string obj)
        {
            JArray listterritory;
            JArray listcountpartner;
            string roleid = "";
            var o1 = JObject.Parse(obj);
            JObject json = JObject.FromObject(o1);
            try
            {
                string queryterritory = "";
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            queryterritory = "select Territory_Name from Ref_territory where TerritoryId in (select rc.TerritoryId from Main_organization o INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code where o.OrgId in (" + property.Value.ToString() + ")) ORDER BY Territory_Name asc";
                        }
                        else
                        {
                            queryterritory = "select Territory_Name from Ref_territory ORDER BY Territory_Name asc ";
                        }
                    }
                }
                sb = new StringBuilder();
                sb.Append(queryterritory);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                listterritory = JArray.Parse(MySqlDb.GetJSONArrayString(ds.Tables[0]));
                // string month = "";
                // if (time == "Month")
                // {
                //     month = "MONTH ( m.DateAdded ) = MONTH ( CURRENT_DATE ( ) )  AND YEAR ( m.DateAdded ) = YEAR ( CURRENT_DATE ( ) ) AND";
                // }
                // string query = "SELECT r.RoleCode, t.Territory_Name, Count( * ) AS TotalPartnerThisMonth, CASE WHEN sr.RoleId = '60' THEN '#1AB244' WHEN sr.RoleId = '58' THEN '#DEBB27' ELSE '#AD1D28' END AS BackgroundColor FROM Main_organization AS m INNER JOIN Main_site s ON m.OrgId = s.OrgId INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId and sr.`Status` = 'A' INNER JOIN Ref_countries AS tc ON tc.countries_code = m.RegisteredCountryCode INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId INNER JOIN Roles r ON sr.RoleId = r.RoleId WHERE " + month + " m.`Status` = 'A' ";
                // string adminQuery =
                //     "SELECT r.RoleCode, t.Territory_Name, Count( * ) AS TotalPartnerThisMonth, CASE WHEN sr.RoleId = '60' THEN '#1AB244' WHEN sr.RoleId = '58' THEN '#DEBB27' ELSE '#AD1D28' END AS BackgroundColor FROM Main_organization AS m INNER JOIN Main_site s ON m.OrgId = s.OrgId INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId and sr.`Status` = 'A' INNER JOIN Ref_countries AS tc ON tc.countries_code = m.RegisteredCountryCode INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId INNER JOIN Roles r ON sr.RoleId = r.RoleId WHERE MONTH ( m.DateAdded ) = MONTH ( CURRENT_DATE ( ) )  AND YEAR ( m.DateAdded ) = YEAR ( CURRENT_DATE ( ) ) ";
                /* issue 15102018 - TOTAL ACTIVE PARTNERS: Dashboard(Admin) query */
                string month = "";
                if (time == "Month")
                {
                    month = "( m.DateAdded BETWEEN DATE_FORMAT( NOW( ), '%Y-%m-01' ) AND CURDATE( ) ) AND";
                }
                string query = "SELECT r.RoleCode, t.Territory_Name, Count( * ) AS TotalPartnerThisMonth, CASE WHEN sr.RoleId = '60' THEN '#1AB244' WHEN sr.RoleId = '58' THEN '#DEBB27' ELSE '#AD1D28' END AS BackgroundColor FROM Main_organization AS m INNER JOIN Main_site s ON m.OrgId = s.OrgId and s.Status ='A' INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId and sr.`Status` in( 'A','P') INNER JOIN Ref_countries AS tc ON tc.countries_code = m.RegisteredCountryCode INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId INNER JOIN Roles r ON sr.RoleId = r.RoleId WHERE " + month + " m.`Status` = 'A' ";
            string adminQuery ="SELECT r.RoleCode, t.Territory_Name, Count( * ) AS TotalPartnerThisMonth, CASE WHEN sr.RoleId = '60' THEN '#1AB244' WHEN sr.RoleId = '58' THEN '#DEBB27' ELSE '#AD1D28' END AS BackgroundColor FROM Main_organization AS m INNER JOIN Main_site s ON m.OrgId = s.OrgId and s.Status ='A' INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId and sr.`Status` in( 'A','P') INNER JOIN Ref_countries AS tc ON tc.countries_code = m.RegisteredCountryCode INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId INNER JOIN Roles r ON sr.RoleId = r.RoleId WHERE " + month + " m.`Status` = 'A' ";
                /* end line issue 15102018 - TOTAL ACTIVE PARTNERS: Dashboard(Admin) query */
                var isOrgNull = json.SelectToken("OrgId");
                if (String.IsNullOrEmpty(isOrgNull.ToString()))
                {
                    query = adminQuery;
                    /* issue 15102018 - TOTAL ACTIVE PARTNERS: Dashboard(Admin) query */
                    foreach (JProperty property in json.Properties())
                    {
                        if (property.Name.Equals("RoleId"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                query += "AND sr.RoleId = '" + property.Value.ToString() + "' ";
                                roleid = property.Value.ToString();
                            }
                        }
                    }
                    /* end line issue 15102018 - TOTAL ACTIVE PARTNERS: Dashboard(Admin) query */
                }
                else
                {
                    foreach (JProperty property in json.Properties())
                    {
                        if (property.Name.Equals("OrgId"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                query += "AND m.OrgId in (" + property.Value.ToString() + ") ";
                            }
                        }
                        if (property.Name.Equals("RoleId"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                query += "AND sr.RoleId = '" + property.Value.ToString() + "' ";
                                roleid = property.Value.ToString();
                            }
                        }
                    }
                }

                query += " GROUP BY t.Territory_Name asc";
                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
                listcountpartner = JArray.Parse(MySqlDb.GetJSONArrayString(ds1.Tables[0]));
                int index = 0;
                if (listcountpartner.Count > 0)
                {
                    foreach (var itemlistterritory in listterritory)
                    {
                        itemlistterritory["CountPartners"] = "0";
                        itemlistterritory["RoleCode"] = listcountpartner[0]["RoleCode"];
                        itemlistterritory["BackgroundColor"] = listcountpartner[0]["BackgroundColor"];
                        foreach (var itemlistcountpartner in listcountpartner)
                        {
                            if (itemlistterritory["Territory_Name"].ToString() == itemlistcountpartner["Territory_Name"].ToString())
                            {
                                itemlistterritory["CountPartners"] = itemlistcountpartner["TotalPartnerThisMonth"];
                            }
                        }
                    }
                }
                else
                {
                    foreach (var itemlistterritory in listterritory)
                    {
                        itemlistterritory["CountPartners"] = "0";
                        if (roleid == "60")
                        {
                            itemlistterritory["RoleCode"] = "MTP";
                            itemlistterritory["BackgroundColor"] = "#669911";
                        }
                        else if (roleid == "58")
                        {
                            itemlistterritory["RoleCode"] = "AAP";
                            itemlistterritory["BackgroundColor"] = "#5FBEAA";
                        }
                        else
                        {
                            itemlistterritory["RoleCode"] = "ATC";
                            itemlistterritory["BackgroundColor"] = "#1b66a2";
                        }
                    }
                }
            }
            catch
            {
                listterritory = null;
            }
            finally
            {
            }
            return Ok(listterritory);
        }

        [HttpGet("whereNew/{obj}")]
        public IActionResult whereNew( string obj)
        {
           
            string result = "";
            DataTable dtATC = new DataTable();
            DataTable dtAAP = new DataTable();
            DataTable dtCTC = new DataTable();
            DataTable dtTmp = new DataTable();
            var o1 = JObject.Parse(obj);
            JObject json = JObject.FromObject(o1);
            try
            {
                string tmpFY = "SELECT SUBSTRING(KeyValue, 1, 4) as FYkey FROM Dictionary  where Parent = 'FYIndicator' and Dictionary.Key COLLATE utf8_unicode_ci = Convert(Year(Now()), CHAR)";
                sb1.Clear();
                sb1.AppendFormat(tmpFY);
                sqlCommand = new MySqlCommand(sb1.ToString());
                DataSet dsFY = oDb.getDataSetFromSP(sqlCommand);
                tmpFY = dsFY.Tables[0].Rows[0]["FYkey"].ToString();
                string query = "SELECT  t.Territory_Name,COUNT(*) AS TotalPartnerThisMonth " +
                                 "FROM Main_organization AS m " +
                                 "INNER JOIN Main_site s ON m.OrgId = s.OrgId " +
                                 "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId AND sr.`Status` IN('A', 'P') " +
                                 "INNER JOIN Journals j on j.ParentId = m.OrgId INNER JOIN JournalActivities ja on j.ActivityId = ja.ActivityId " +
                                 "INNER JOIN Ref_countries AS tc ON tc.countries_code = m.RegisteredCountryCode " +
                                 "INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId " +
                                 "INNER JOIN Roles r ON sr.RoleId = r.RoleId " +
                                 "WHERE m.`Status` = 'A' AND ";
                                 
                string adminQuery = "And YEAR(j.DateAdded) = YEAR(NOW()) AND ja.ActivityName like '%"+ tmpFY+" Renewed%Paid%'  GROUP BY t.Territory_Name ASC; ";
                string tmpQuery = query + "sr.RoleId = '1' " + adminQuery;
                tmpQuery = tmpQuery.Replace("TotalPartnerThisMonth", "ATC");
                sb1.Clear();
                sb1.AppendFormat(tmpQuery);
                sqlCommand = new MySqlCommand(sb1.ToString());
                DataSet dsATC = oDb.getDataSetFromSP(sqlCommand);
                if (dsATC.Tables[0].Rows.Count != 0)
                {
                    dtATC = dsATC.Tables[0];
                    dtATC.PrimaryKey = new DataColumn[] { dtATC.Columns["Territory_Name"] };
                }
                tmpQuery = query + "sr.RoleId = '61' " + adminQuery;
                tmpQuery = tmpQuery.Replace("TotalPartnerThisMonth", "CTC");
                sb1.Clear();
                sb1.AppendFormat(tmpQuery);
                sqlCommand = new MySqlCommand(sb1.ToString());
                DataSet dsCTC = oDb.getDataSetFromSP(sqlCommand);
                if (dsCTC.Tables[0].Rows.Count != 0)
                {
                    dtCTC = dsCTC.Tables[0];
                    dtCTC.PrimaryKey = new DataColumn[] { dtCTC.Columns["Territory_Name"] };
                }
                tmpQuery = query + "(sr.RoleId = '58' or sr.RoleId = '60') " + adminQuery;
                tmpQuery = tmpQuery.Replace("TotalPartnerThisMonth", "AAPMTP");
                sb1.Clear();
                sb1.AppendFormat(tmpQuery);
                sqlCommand = new MySqlCommand(sb1.ToString());
                DataSet dsAAP = oDb.getDataSetFromSP(sqlCommand);
                if (dsAAP.Tables[0].Rows.Count != 0)
                {
                    dtAAP = dsAAP.Tables[0];
                    dtAAP.PrimaryKey = new DataColumn[] { dtAAP.Columns["Territory_Name"] };
                }
                dtATC.Merge(dtCTC);
                dtATC.Merge(dtAAP);
                dtTmp = dtATC.Copy();
                DataColumnCollection tmpcolumns = dtTmp.Columns;
                if (!tmpcolumns.Contains("CTC"))
                {
                    dtTmp.Columns.Add("CTC", typeof(System.Int32));
                    foreach (DataRow row in dtTmp.Rows)
                    {
                        row["CTC"] = 0;
                    }
                }
                if (!tmpcolumns.Contains("ATC"))
                {
                    dtTmp.Columns.Add("ATC", typeof(System.Int32));
                    foreach (DataRow row in dtTmp.Rows)
                    {
                        row["ATC"] = 0;
                    }
                }
                if (!tmpcolumns.Contains("AAPMTP"))
                {
                    dtTmp.Columns.Add("AAPMTP", typeof(System.Int32));
                    foreach (DataRow row in dtTmp.Rows)
                    {
                        row["AAPMTP"] = 0;
                    }
                }
                foreach (DataRow row in dtTmp.Rows)
                {
                    if (string.IsNullOrEmpty(row["ATC"].ToString())) row["ATC"] = 0; ;
                    if (string.IsNullOrEmpty(row["AAPMTP"].ToString())) row["AAPMTP"] = 0; ;
                    if (string.IsNullOrEmpty(row["CTC"].ToString())) row["CTC"] = 0;
                }
               
            }
            catch(Exception ex)
            {
                result = null;
            }
            finally
            {
                result= MySqlDb.GetJSONObjectString(dtTmp);
                result = "[" + result + "]";
            }
            return Ok(result);
        }
    }


    [Route("api/DashboardCoursesByMonth")]
    public class DashboardCoursesByMonth : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardCoursesByMonth(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select t.Territory_Name, Count(*) as TotalCourseDeliveredByPartners from Main_organization as m inner join Main_site as s on m.OrgId = s.OrgId inner join Ref_countries as tc on tc.countries_code = m.RegisteredCountryCode inner join Ref_territory as t on t.TerritoryId = tc.TerritoryId inner join Courses as c on s.SiteId = c.SiteId where MONTH ( c.StartDate ) = MONTH ( CURRENT_DATE ( ) ) AND YEAR ( c.StartDate ) = YEAR ( CURRENT_DATE ( ) ) AND m.Status = 'A' group by t.Territory_Name");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("where/{obj}")]
        public IActionResult where(string obj)
        {
            try
            {
                sb = new StringBuilder();
                // string query = "SELECT s.SiteId,SiteName, CASE RoleId WHEN 1 THEN 'ATC' WHEN 58 THEN 'AAP' ELSE 'MTP' END AS RoleCode ,TotalCourseMonthByPartnerType FROM (SELECT s.SiteId,SiteName,TotalCourseMonthByPartnerType FROM (SELECT COUNT(CourseId) AS TotalCourseMonthByPartnerType,SiteId FROM Courses WHERE MONTH ( CompletionDate ) = MONTH ( CURRENT_DATE ( ) ) AND YEAR ( CompletionDate ) = YEAR ( CURRENT_DATE ( ) ) GROUP BY SiteId ORDER BY CourseId) AS c INNER JOIN Main_site s ON c.SiteId = s.SiteId WHERE s.`Status` = 'A') AS s LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId WHERE sr.RoleId IN ('1','58','60') ";
                string query = "SELECT TotalCourseMonthByPartnerType, PartnerType FROM (SELECT TotalCourseMonthByPartnerType, PartnerType, OrgId FROM (SELECT COUNT(CourseId) AS TotalCourseMonthByPartnerType, SiteId, PartnerType FROM Courses WHERE MONTH ( StartDate ) = MONTH ( CURRENT_DATE ( ) ) AND YEAR ( StartDate ) = YEAR ( CURRENT_DATE ( ) ) GROUP BY PartnerType) AS c INNER JOIN Main_site s ON c.SiteId = s.SiteId WHERE s.`Status` = 'A') AS s ";
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);
                var isOrgIdNull = json.SelectToken("OrgId");

                //For Admin Role
                if (String.IsNullOrEmpty(isOrgIdNull.ToString()))
                {
                    query = "SELECT TotalCourseMonthByPartnerType, PartnerType FROM (SELECT COUNT(CourseId) AS TotalCourseMonthByPartnerType, PartnerType FROM Courses WHERE MONTH(StartDate) = MONTH(CURRENT_DATE()) AND YEAR(StartDate) = YEAR(CURRENT_DATE()) GROUP BY PartnerType) AS c";
                }
                //for others roles
                else
                {
                    foreach (JProperty property in json.Properties())
                    {
                        if (property.Name.Equals("OrgId"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                query += "WHERE s.OrgId in (" + property.Value.ToString() + ") ";
                            }
                        }
                    }
                }

                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                //Console.WriteLine(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("totalCourseByPartner/where/{obj}")]
        public IActionResult TotalJek(string obj)
        {
            try
            {
                string status = "";
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        status = property.Value.ToString();
                        if (string.IsNullOrEmpty(status))
                        {
                            status = "WHERE s.`Status` = 'A' ";
                        }
                        else
                        {
                            status = "WHERE s.`Status` = 'A' AND (c.StartDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE()) ";
                            
                        }
                    }
                }
                sb = new StringBuilder();
                //string query = "SELECT TotalCourseByPartnerType, PartnerType FROM (SELECT TotalCourseByPartnerType, PartnerType, OrgId FROM (SELECT COUNT(CourseId) AS TotalCourseByPartnerType, SiteId, PartnerType FROM Courses GROUP BY PartnerType) AS c INNER JOIN Main_site s ON c.SiteId = s.SiteId WHERE s.`Status` = 'A') AS s ";
                //Diubah tgl 11/08/2018
                string query =
                "SELECT COUNT(CourseId) AS TotalCourseByPartnerType,PartnerType FROM Courses c " +
                "INNER JOIN Main_site s ON c.SiteId = s.SiteId " + status;

                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND s.OrgId IN (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                query += " GROUP BY PartnerType";
                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DCPAAC")]
    public class DCPAAC : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        String result2;
        String result3;
        public DCPAAC(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select t.Territory_Name, p.productName as KeyValue, Count(*) as TotalCourseMonthByPartnerType from Main_organization as m inner join Main_site as s on m.OrgId = s.OrgId inner join Ref_countries as tc on tc.countries_code = m.RegisteredCountryCode inner join Ref_territory as t on t.TerritoryId = tc.TerritoryId inner join SiteRoles as sr on sr.SiteId = s.SiteId inner join Roles as r on r.RoleId = sr.RoleId inner join Courses as c on s.SiteId = c.SiteId inner join CourseSoftware as cs on cs.CourseId = c.CourseId inner join Products as p on p.productId = cs.productId where m.Status = 'A' Group by t.Territory_Name, p.productName");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("{role}/where/{obj}")]
        public IActionResult whereAAP(string role, string obj)
        {
            List<JObject> myList = new List<JObject>();
            try
            {
                JArray arr1;
                JObject obj1;
                JObject obj2;
                string org = "";
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            org = "AND s.OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                // string query = 
                //     "select replace( replace( replace( `Name`, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as `Name`, TotalCourseEvaCoreProduct from (select c.CourseId, `Name`, COUNT(productId) as TotalCourseEvaCoreProduct from (select c.CourseId, `Name`, productId from (select CourseId, p.productId from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId AND productName like '%AutoCAD%') as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+") as c INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId GROUP BY CourseId) as ea GROUP BY CourseId UNION "+
                //     "select 'Not Core Product' `Name`, (select COUNT(productId) as TotalCourseEvaAllProduct from (select c.CourseId, `Name`, productId from (select CourseId, p.productId from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId) as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+") as c INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId) - COUNT(productId) as TotalCourseEvaCoreProduct "+
                //     "from (select c.CourseId, `Name`, productId from (select CourseId, p.productId from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId AND productName like '%AutoCAD%') as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+") as c INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId";
                // if(role == "ATC"){
                //     query = "select replace( replace( replace( `Name`, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as `Name`, TotalCourseEvaCoreProduct from (select c.CourseId, `Name`, COUNT(productId) as TotalCourseEvaCoreProduct from (select c.CourseId, `Name`, productId from (select CourseId, p.productId from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId AND productName like '%AutoCAD%') as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId "+
                //     "AND c.CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533') AND YEAR(c.StartDate) = YEAR(NOW()) "+
                //     "INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+") as c INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId GROUP BY CourseId) as ea GROUP BY CourseId UNION "+
                //     "select 'Not Core Product' `Name`, (select COUNT(productId) as TotalCourseEvaAllProduct from (select c.CourseId, `Name`, productId from (select CourseId, p.productId from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId) as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId AND c.CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533') AND YEAR(c.StartDate) = YEAR(NOW()) INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+") as c INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId) - COUNT(productId) as TotalCourseEvaCoreProduct "+
                //     "from (select c.CourseId, `Name`, productId from (select CourseId, p.productId from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId AND productName like '%AutoCAD%') as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId AND c.CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533') AND YEAR(c.StartDate) = YEAR(NOW()) INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+") as c INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId";
                // }
                // string query = 
                //     "select productName, TotalCourseEvaCoreProduct from "+
                //     "( select replace( replace( replace( productName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as productName, TotalCourseEvaCoreProduct "+
                //     "from (select productName, COUNT(productId) as TotalCourseEvaCoreProduct, productId "+
                //     "from (select c.CourseId, productName, productId "+
                //     "from (select CourseId, p.productId, productName from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId WHERE p.productId in (3,28,39,152,136,1,33)) as cs "+
                //     "INNER JOIN Courses c ON cs.CourseId = c.CourseId AND c.CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533') "+
                //     "INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+" ) as c "+
                //     "INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId GROUP BY productId) as ea GROUP BY productId "+
                //     "UNION "+
                //     "select productName, '0' TotalCourseEvaCoreProduct from Products WHERE productId in (3,28,39,152,136,1,33) ) as CoreProduct GROUP BY productName "+
                //     "UNION "+
                //     "select 'Not Core Product' productName, "+
                //     "(select COUNT(productId) as TotalCourseEvaAllProduct "+
                //     "from (select c.CourseId, productId from (select CourseId, p.productId "+
                //     "from CourseSoftware cs "+
                //     "INNER JOIN Products p ON cs.productId = p.productId) as cs "+
                //     "INNER JOIN Courses c ON cs.CourseId = c.CourseId AND c.CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533') "+
                //     "INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+" ) as c "+
                //     "INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId) - "+
                //     "COUNT(productId) as TotalCourseEvaCoreProduct "+
                //     "from (select c.CourseId, productId "+
                //     "from (select CourseId, p.productId "+
                //     "from CourseSoftware cs "+
                //     "INNER JOIN Products p ON cs.productId = p.productId WHERE p.productId in (3,28,39,152,136,1,33)) as cs "+
                //     "INNER JOIN Courses c ON cs.CourseId = c.CourseId AND c.CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533') "+
                //     "INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+" ) as c "+
                //     "INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId";
                // string query = 
                //     "select productName, TotalCourseEvaCoreProduct from "+
                //     "(select replace( replace( replace( productName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as productName, TotalCourseEvaCoreProduct "+
                //     "from (select productName, COUNT(productId) as TotalCourseEvaCoreProduct, productId "+
                //     "from (select c.CourseId, productName, productId "+
                //     "from (select CourseId, p.productId, productName from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId WHERE p.productId in (3,28,39,152,136,1,33)) as cs "+
                //     "INNER JOIN Courses c ON cs.CourseId = c.CourseId AND c.CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533') "+
                //     "INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+" ) as c "+
                //     "INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId GROUP BY productId) as ea GROUP BY productId "+
                //     "UNION "+
                //     "select productName, '0' TotalCourseEvaCoreProduct from Products WHERE productId in (3,28,39,152,136,1,33) ) as CoreProduct GROUP BY productName ";
                // string query2 = 
                //     "select COUNT(productId) as TotalCourseEvaAllProduct "+
                //     "from (select c.CourseId, productId from (select CourseId, p.productId "+
                //     "from CourseSoftware cs "+
                //     "INNER JOIN Products p ON cs.productId = p.productId) as cs "+
                //     "INNER JOIN Courses c ON cs.CourseId = c.CourseId AND c.CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533') "+
                //     "INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+" ) as c "+
                //     "INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId";
                // string query3 =  
                //     "select COUNT(productId) as TotalCourseEvaCoreProduct "+
                //     "from (select c.CourseId, productId "+
                //     "from (select CourseId, p.productId "+
                //     "from CourseSoftware cs "+
                //     "INNER JOIN Products p ON cs.productId = p.productId WHERE p.productId in (3,28,39,152,136,1,33)) as cs "+
                //     "INNER JOIN Courses c ON cs.CourseId = c.CourseId AND c.CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533') "+
                //     "INNER JOIN Main_site s ON c.SiteId = s.SiteId where PartnerType like '%"+role+"%' and s.`Status` != 'X' "+org+" ) as c "+
                //     "INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId";
                string query =
                    "select productName, TotalCourseEvaCoreProduct from (select productName, COUNT(productId) as TotalCourseEvaCoreProduct from (select CourseId, p.productId, productName from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId WHERE (p.productName LIKE '%Revit%' OR p.productName LIKE '%BIM%' OR p.productId IN ( 3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33 ) ) AND CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533')) as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId  AND PartnerType like '%" + role + "%' INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.`Status` != 'X' " + org + " GROUP BY productName UNION SELECT productName, 0 TotalCourseEvaCoreProduct FROM Products WHERE productId IN (3,28,39,152,136,1,33) GROUP BY productName) AS CoreProduct GROUP BY productName";
                string query2 =
                    "select COUNT(productId) as TotalCourseEvaAllProduct from (select CourseId, p.productId, productName from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId WHERE CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533')) as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId  AND PartnerType like '%" + role + "%' INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.`Status` != 'X' " + org + "";
                string query3 =
                    "select COUNT(productId) as TotalCourseEvaCoreProduct from (select CourseId, p.productId, productName from CourseSoftware cs INNER JOIN Products p ON cs.productId = p.productId WHERE (p.productName LIKE '%Revit%' OR p.productName LIKE '%BIM%' OR p.productId IN ( 3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33 ) ) AND CourseId not in ('EM0394452145','EM3035394159','EM0394452145','EM0483399096','EM3035378533')) as cs INNER JOIN Courses c ON cs.CourseId = c.CourseId  AND PartnerType like '%" + role + "%' INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.`Status` != 'X' " + org + "";
                if (role == "ATC")
                {
                    // Console.WriteLine(query);
                }
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr1 = JArray.Parse(result);
                for (int i = 0; i < arr1.Count; i++)
                {
                    myList.Add(
                        JObject.Parse(
                            "{" +
                            "\"productName\":\"" + arr1[i]["productName"].ToString() + "\"," +
                            "\"TotalCourseEvaCoreProduct\":\"" + arr1[i]["TotalCourseEvaCoreProduct"].ToString() + "\"" +
                            "}"
                        )
                    );
                }
                sb = new StringBuilder();
                sb.AppendFormat(query2);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result2 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                obj1 = JObject.Parse(result2);
                int TotalCourseEvaAllProduct = Int32.Parse(obj1["TotalCourseEvaAllProduct"].ToString());
                sb = new StringBuilder();
                sb.AppendFormat(query3);
                //Console.WriteLine(query3);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result3 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                obj2 = JObject.Parse(result3);
                int TotalCourseEvaCoreProduct = Int32.Parse(obj2["TotalCourseEvaCoreProduct"].ToString());
                int notcoreproduct = TotalCourseEvaAllProduct - TotalCourseEvaCoreProduct;
                myList.Add(
                    JObject.Parse(
                        "{" +
                        "\"productName\":\"Not Core Product\"," +
                        "\"TotalCourseEvaCoreProduct\":\"" + notcoreproduct.ToString() + "\"" +
                        "}"
                    )
                );
            }
            catch
            {
                myList = null;
            }
            finally
            {
                // result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
            return Ok(myList);
        }
    }
    [Route("api/DashboardActivePartner")]
    public class DashboardActivePartner : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardActivePartner(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select t.Territory_Name, Count(*) as TotalPartnerThisMonth from Main_organization as m inner join Ref_countries as tc on tc.countries_code = m.RegisteredCountryCode inner join Ref_territory as t on t.TerritoryId = tc.TerritoryId where m.Status = 'A' group by t.Territory_Name;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("where/{obj}")]
        public IActionResult where(string obj)
        {
            try
            {
                sb = new StringBuilder();
                string query = "SELECT r.RoleCode, rc.countries_name, Count( * ) AS TotalPartnerThisMonth FROM Main_organization AS m INNER JOIN Ref_countries AS rc ON rc.countries_code = m.RegisteredCountryCode INNER JOIN Main_site AS s ON m.OrgId = s.OrgId INNER JOIN SiteRoles AS sr ON s.SiteId = sr.SiteID INNER JOIN Roles AS r ON sr.RoleId = r.RoleId WHERE m.STATUS = 'A' AND r.RoleCode IN ('ATC','AAP','MTP') ";
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND m.OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }
                    if (property.Name.Equals("DistributorOrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND TerritoryId in (select TerritoryId from Main_organization o INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code where o.OrgId IN (" + property.Value.ToString() + ")) ";
                        }
                    }
                }
                query += " GROUP BY rc.countries_code asc";
                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardActiveInstructors")]
    public class DashboardActiveInstructors : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;// = new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardActiveInstructors(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                // sb.Append("SELECT o.OrgId, count( * ) AS TotalActiveInstructor FROM Contacts_All c JOIN Main_site s ON c.PrimarySiteId = s.SiteId JOIN Main_organization o ON s.OrgId = o.OrgId WHERE c.`Status` = 'A' AND c.ATCRole = 'Y' AND s.`Status` = 'A'  AND o.`Status` = 'A' GROUP BY o.OrgId HAVING count( * ) > 10");
                sb.Append("SELECT rt.Territory_Name, count( * ) AS TotalActiveInstructor FROM Contacts_All c JOIN Main_site s ON c.PrimarySiteId = s.SiteId JOIN Main_organization o ON s.OrgId = o.OrgId  JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId WHERE c.`Status` = 'A' AND c.ATCRole = 'Y' AND s.`Status` = 'A' AND o.`Status` = 'A' GROUP BY rt.TerritoryId");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("{time}/where/{obj}")]
        public IActionResult where(string time, string obj)
        {
            JArray listterritory;
            JArray listcountpartner;
            string roleid = "";
            var o1 = JObject.Parse(obj);
            JObject json = JObject.FromObject(o1);
            Boolean adminkan = true;
            string query1 = "";
            string result = "";
            try
            {
                // string queryterritory = "";
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            
                            // queryterritory += "select countries_name from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId INNER JOIN Ref_countries rc ON sd.CountryCode = rc.countries_code WHERE o.OrgId IN (" + property.Value.ToString() + ") ORDER BY countries_name ASC";
                            adminkan = false;
                            //query1 +=
                            // "SELECT r.RoleCode, t.Territory_Name , Count( * ) AS TotalActiveInstructor, CASE WHEN sr.RoleId = '60' THEN '#1AB244' WHEN sr.RoleId = '58' THEN '#DEBB27' WHEN sr.RoleId = '1' THEN '#AD1D28' END AS BackgroundColor " +
                            // "FROM  Main_site s " +
                            // "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId and sr.`Status` = 'A' " +
                            // "INNER JOIN " +
                            // "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                            // "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                            // "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                            // "WHERE o.OrgId IN (" + property.Value.ToString() + ")) ds " +
                            // "ON s.SiteCountryCode = ds.CountryCode collate utf8_general_ci " +
                            // "INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId AND sc.`Status` = 'A' " +
                            // "INNER JOIN Contacts_All ca ON sc.ContactId = ca.ContactId AND ca.`Status` = 'A' " +
                            // "INNER JOIN Ref_countries AS tc ON tc.countries_code = ds.CountryCode collate utf8_general_ci " +
                            // "INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId " +
                            // "INNER JOIN Roles r ON sr.RoleId = r.RoleId " +
                            // "GROUP BY t.Territory_Name, RoleCode asc";
                            query1 = " SELECT CASE WHEN RoleName LIKE '60' THEN(select RoleCode from Roles where RoleId = 60) WHEN RoleName = '58' THEN(select RoleCode from Roles where RoleId = 58)       WHEN RoleName LIKE '%1%'  THEN(select RoleCode from Roles where RoleId = 1) END AS RoleCode, t.Territory_Name, COUNT(result.ContactId) AS TotalActiveInstructor, CASE WHEN RoleName LIKE '60' THEN '#1AB244' WHEN RoleName = '58' THEN '#DEBB27' WHEN RoleName LIKE '%1%'  THEN '#AD1D28' END AS BackgroundColor " +
                             "FROM (SELECT ss.ContactId,GROUP_CONCAT(DISTINCT rr.RoleId) AS RoleName, ss.SiteId FROM SiteContactLinks ss  INNER JOIN SiteRoles rr on rr.SiteId = ss.SiteId and rr.Status='A' Inner join Contacts_All c ON ss.ContactId = c.ContactId AND c.`Status` = 'A' AND PrimaryRoleId = '23' where ss.Status = 'A'  group by ss.ContactId) as result " +
                             "INNER JOIN Main_site s ON  result.SiteId = s.SiteId and s.Status = 'A' " +
                             "INNER JOIN(SELECT o.OrgId, s.SiteID, sd.CountryCode FROM Main_organization o " +
                             "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                             "INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId " +
                             "WHERE o.OrgId IN (" + property.Value.ToString() + ")) ds ON s.SiteCountryCode = ds.CountryCode " +
                             "INNER JOIN Ref_countries AS tc ON tc.countries_code = ds.CountryCode " +
                             "INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId " +
                             "GROUP BY t.Territory_Name ,RoleCode  ORDER BY RoleCode ASC ";
                        }
                        // else
                        // {
                        //     queryterritory = "select Territory_Name from Ref_territory ORDER BY Territory_Name asc";
                        //     adminkan = true;
                        // }
                    }
                }
                // string month = "";
                // if (time == "Month")
                // {
                //     month = "MONTH ( m.DateAdded ) = MONTH ( CURRENT_DATE ( ) )  AND YEAR ( m.DateAdded ) = YEAR ( CURRENT_DATE ( ) ) AND";
                // }
                // string query = "SELECT r.RoleCode, ";
                string query = "";
                if (adminkan)
                {
                    query = "SELECT RoleCode,Territory_Name,SUM(TotalPartnerThisMonth) AS TotalActiveInstructor,BackgroundColor FROM tmpDashboardTotalActiveInstructors GROUP BY Territory_Name,RoleId ASC";
                }
                else
                {
                    query = query1;
                }
                // query += "Count( * ) AS TotalActiveInstructor, CASE WHEN sr.RoleId = '60' THEN '#1AB244' WHEN sr.RoleId = '58' THEN '#DEBB27' ELSE '#AD1D28' END AS BackgroundColor FROM Main_organization AS m INNER JOIN Main_site s ON m.OrgId = s.OrgId INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId and sr.`Status` = 'A' INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId AND sc.`Status` = 'A' INNER JOIN Contacts_All ca ON sc.ContactId = ca.ContactId AND ca.`Status` = 'A' INNER JOIN Ref_countries AS tc ON tc.countries_code = m.RegisteredCountryCode INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId INNER JOIN Roles r ON sr.RoleId = r.RoleId WHERE "+month+" m.`Status` = 'A' ";
                // foreach (JProperty property in json.Properties())
                // { 
                //    if(property.Name.Equals("OrgId")){   
                //        if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                //            query += "AND m.OrgId in ("+property.Value.ToString()+") ";  
                //        }
                //    }
                //    if(property.Name.Equals("RoleId")){   
                //        if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                //            query += "AND sr.RoleId = '"+property.Value.ToString()+"' "; 
                //            roleid = property.Value.ToString();
                //        }
                //    }
                // } 
                // if(adminkan){
                //     query += " GROUP BY t.Territory_Name asc"; 
                // }else{
                //     query += " GROUP BY tc.countries_name asc"; 
                // }
                //Console.WriteLine(query);
                // sb1.AppendFormat(query);
                // sqlCommand = new MySqlCommand(sb1.ToString());
                // //Console.WriteLine(sb1.ToString());
                // ds1 = oDb.getDataSetFromSP(sqlCommand);
                // listcountpartner = JArray.Parse(MySqlDb.GetJSONArrayString(ds1.Tables[0]));
                Console.WriteLine(query);
                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // listterritory = JArray.Parse(MySqlDb.GetJSONArrayString(ds.Tables[0]));
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                // int index = 0;
                // if (listcountpartner.Count > 0)
                // {
                //     foreach (var itemlistterritory in listterritory)
                //     {
                //         itemlistterritory["CountPartners"] = "0";
                //         foreach (var itemlistcountpartner in listcountpartner)
                //         {
                //             itemlistterritory["RoleCode"] = itemlistcountpartner["RoleCode"];
                //             itemlistterritory["BackgroundColor"] = itemlistcountpartner["BackgroundColor"];
                //             //Jika admin
                //             if (adminkan)
                //             {
                //                 if (itemlistterritory["Territory_Name"].ToString() == itemlistcountpartner["Territory_Name"].ToString())
                //                 {
                //                     itemlistterritory["CountPartners"] = itemlistcountpartner["TotalActiveInstructor"];
                //                 }
                //             }
                //             //Jika bukan admin
                //             else
                //             {
                //                 if (itemlistterritory["countries_name"].ToString() == itemlistcountpartner["countries_name"].ToString())
                //                 {
                //                     itemlistterritory["CountPartners"] = itemlistcountpartner["TotalActiveInstructor"];
                //                 }
                //             }
                //         }
                //     }
                // }
                // else
                // {
                //     foreach (var itemlistterritory in listterritory)
                //     {
                //         itemlistterritory["CountPartners"] = "0";
                //         for (int i = 0; i < listcountpartner.Count; i++)
                //         {
                //             if (listcountpartner[i]["RoleCode"].ToString() == "MTP")
                //             {
                //                 itemlistterritory["RoleCode"] = "MTP";
                //                 itemlistterritory["BackgroundColor"] = "#669911";
                //             }
                //             else if (listcountpartner[i]["RoleCode"].ToString() == "AAP")
                //             {
                //                 itemlistterritory["RoleCode"] = "AAP";
                //                 itemlistterritory["BackgroundColor"] = "#5FBEAA";
                //             }
                //             else
                //             {
                //                 itemlistterritory["RoleCode"] = "ATC";
                //                 itemlistterritory["BackgroundColor"] = "#1b66a2";
                //             }
                //         }
                //     }
                // }
            }
            catch
            {
                // listterritory = null;
                result = "";
            }
            finally
            {
            }
            // return Ok(listterritory);
            return Ok(result);
        }
        [HttpGet("where/{obj}")]
        public IActionResult where(string obj)
        {
            try
            {
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);
                string org = "";
                string query = "";
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            org = "AND m.OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                if (org != "")
                {
                    query =
                        "SELECT RoleCode, Label, TotalActiveInstructor from " +
                        "(SELECT r.RoleCode, rc.countries_name as Label, count( * ) AS TotalActiveInstructor " +
                        "FROM Main_organization AS m INNER JOIN Main_site AS ms ON m.orgid = ms.OrgId INNER JOIN SiteRoles AS sr ON ms.SiteId = sr.SiteID " +
                        "INNER JOIN Roles AS r ON sr.RoleId = r.RoleId INNER JOIN SiteContactLinks AS scl ON ms.SiteId = scl.SiteId " +
                        "INNER JOIN Contacts_All AS c ON c.ContactId = scl.ContactId INNER JOIN Ref_countries rc ON m.RegisteredCountryCode = rc.countries_code " +
                        "WHERE m.STATUS = 'A' AND ms.STATUS = 'A' AND c.STATUS = 'A' AND r.RoleCode IN ( 'ATC', 'AAP', 'MTP' ) " + org + " " +
                        "GROUP BY rc.countries_name ASC UNION " +
                        "select '-' RoleCode, countries_name as Label, 0 TotalActiveInstructor " +
                        "from Ref_countries " +
                        "where TerritoryId in (select rc.TerritoryId from Main_organization m INNER JOIN Ref_countries rc ON m.RegisteredCountryCode = rc.countries_code " + org + ") ORDER BY countries_name) as ActiveInstructor " +
                        "GROUP BY Label ASC";
                }
                else
                {
                    query =
                        "SELECT RoleCode, Label, TotalActiveInstructor FROM " +
                        "(SELECT r.RoleCode, rt.Territory_Name as Label, count( * ) AS TotalActiveInstructor " +
                        "FROM Main_organization AS m INNER JOIN Main_site AS ms ON m.orgid = ms.OrgId INNER JOIN SiteRoles AS sr ON ms.SiteId = sr.SiteID " +
                        "INNER JOIN Roles AS r ON sr.RoleId = r.RoleId INNER JOIN SiteContactLinks AS scl ON ms.SiteId = scl.SiteId " +
                        "INNER JOIN Contacts_All AS c ON c.ContactId = scl.ContactId INNER JOIN Ref_countries rc ON m.RegisteredCountryCode = rc.countries_code " +
                        "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                        "WHERE m.STATUS = 'A' AND ms.STATUS = 'A' AND c.STATUS = 'A' AND r.RoleCode IN ( 'ATC', 'AAP', 'MTP' ) " +
                        "GROUP BY rt.Territory_Name ASC UNION " +
                        "SELECT '-' RoleCode, Territory_Name as Label, 0 TotalActiveInstructor FROM " +
                        "Ref_countries rc INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                        "GROUP BY rt.Territory_Name ASC	) AS ActiveInstructor " +
                        "GROUP BY Label ASC ";
                }
                //Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("Top5Info/{Id}")]
        public IActionResult Top5Info(string Id)
        {
            InstructorDisboardCardViewModel viewModel = new InstructorDisboardCardViewModel();
            DataSet evalEndDs = new DataSet();
            try
            {
                //var cmdText = "SELECT CourseId,Name,CompletionDate,StartDate FROM Courses WHERE ContactId= '" + Id + "'";
                var courseEnd = "SELECT c.CourseId, c.name, c.StartDate, c.CompletionDate FROM Courses c INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.`Status` != 'X' INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId AND ca.`Status` != 'X' WHERE c.CompletionDate BETWEEN (Date(Now())) and  (Date(Now()+INTERVAL (Select DataValue from LookUp Where ControlName= 'ThisWeek') DAY)) AND Date(c.CompletionDate) <=Date(Now()+INTERVAL (Select DataValue from LookUp Where ControlName= 'ThisWeek') DAY) and c.ContactId = '" + Id + "'";
                sqlCommand = new MySqlCommand(courseEnd);
                ds = oDb.getDataSetFromSP(sqlCommand);
                var evalEndCmd = "SELECT c.CourseId, c.name, c.StartDate, c.EnrollmentExpireDate FROM Courses c INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.`Status` != 'X' INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId AND ca.`Status` != 'X' WHERE c.EnrollmentExpireDate BETWEEN (Date(Now())) and  (Date(Now()+INTERVAL (Select DataValue from LookUp Where ControlName= 'EnrollmentExpireDate') DAY)) AND Date(c.EnrollmentExpireDate) <=Date(Now()+INTERVAL (Select DataValue from LookUp Where ControlName= 'EnrollmentExpireDate') DAY) and c.ContactId = '" + Id + "'";
                sqlCommand = new MySqlCommand(evalEndCmd);
                evalEndDs = oDb.getDataSetFromSP(sqlCommand);
            }
            catch (Exception e)
            {
                return Ok(e.Message);
            }
            finally
            {
                if (ds.Tables.Count > 0)
                {
                    var course = ds.Tables[0].Select().AsEnumerable().Select(row => new CourseEnd { Name = row["Name"].ToString(), Id = row["Courseid"].ToString(), EDate = Convert.ToDateTime(row["CompletionDate"].ToString()).ToShortDateString(), SDate = Convert.ToDateTime(row["StartDate"].ToString()).ToShortDateString() }).ToList();
                    viewModel.CourseEndThisWeekViewModels = new CourseEndThisWeekViewModel
                    {
                        Count = course.Count(),
                        DataList = course.OrderBy(x => x.EDate).Take(5).ToList()
                    };
                    ds.Dispose();
                }
                else
                {
                    viewModel.CourseEndThisWeekViewModels = new CourseEndThisWeekViewModel
                    {
                        Count = 0,
                        DataList = new List<CourseEnd>()
                    };
                }
                if (evalEndDs.Tables.Count > 0)
                {
                    var eval = evalEndDs.Tables[0].Select().AsEnumerable().Select(row => new EvaleEnd { Name = row["Name"].ToString(), Id = row["Courseid"].ToString(), EDate = Convert.ToDateTime(row["EnrollmentExpireDate"].ToString()).ToShortDateString(), SDate = Convert.ToDateTime(row["StartDate"].ToString()).ToShortDateString() }).ToList();
                    viewModel.EvalEndThisWeekViewModels = new EvalEndThisWeekViewModel
                    {
                        Count = eval.Count(),
                        DataList = eval.OrderBy(x => x.EDate).Take(5).ToList()
                    };
                    evalEndDs.Dispose();
                }
                else
                {
                    viewModel.EvalEndThisWeekViewModels = new EvalEndThisWeekViewModel
                    {
                        Count = 0,
                        DataList = new List<EvaleEnd>()
                    };
                }

                viewModel.StudentEnrolThisWeekViewModels = new StudentEnrolThisWeekViewModel();

                result = Newtonsoft.Json.JsonConvert.SerializeObject(viewModel);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardMonthEvaluationSurvey")]
    public class DashboardMonthEvaluationSurvey : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardMonthEvaluationSurvey(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select r.RoleCode, day(a.CreatedDate) as dayCreated, count(*) as TotalMOnthlySurveyTaken from EvaluationQuestion as q inner join EvaluationAnswer as a on q.EvaluationQuestionCode = a.EvaluationQuestionCode inner join Roles as r on r.RoleId = q.PartnerType where month(a.CreatedDate) = month(current_date()) and year(a.CreatedDate) = year(current_date()) group by r.RoleCode, day(a.CreatedDate);");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        //[HttpGet("whereorg/{obj}")]
        [HttpPost("whereorg")]
        public IActionResult whereorg([FromBody] dynamic data)
        {
            try
            {
                JObject json = JObject.FromObject(data);
                sb = new StringBuilder();
                 string query = "";
                query +=
                "SELECT COUNT(EvaluationReportId) AS TotalSurveyTaken,s.SiteId FROM EvaluationReport ea " +
                "INNER JOIN Courses c ON ea.CourseId = c.CourseId " +
                "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                 "WHERE(ea.DateSubmitted BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND CURDATE()) AND s.`Status` = 'A' ";
                //var o1 = JObject.Parse(obj);
                //JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND s.OrgId in (" + property.Value.ToString() + ") ";
                           // query += "AND s.OrgId in ('ORG2134','ORG369','OA100061','OA100163','OP701858','OP701835','OP702111','OP701863','OP701906','ORG867','OP7257','OP701863','OP701551','ORG2708','OP700706','ORG2747','OP701398','OP702027','OP701965')";
                        }
                    }
                    if (property.Name.Equals("siteid"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += " AND s.siteid in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                query += " GROUP BY s.SiteId";
                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("where/{obj}")]
        public IActionResult where(string obj)
        {
            try 
            {
                sb = new StringBuilder();
                // string query = "SELECT r.RoleCode, s.SiteName, DAY ( a.CreatedDate ) AS dayCreated, count( * ) AS TotalMOnthlySurveyTaken FROM EvaluationQuestion AS q INNER JOIN EvaluationAnswer AS a ON q.EvaluationQuestionCode = a.EvaluationQuestionCode INNER JOIN Courses AS c ON a.CourseId = c.CourseId INNER JOIN Main_site AS s ON s.SiteId = c.SiteId INNER JOIN Main_organization AS o ON s.OrgId = o.OrgId INNER JOIN SiteRoles AS sr ON s.SiteId = sr.SiteId INNER JOIN Roles AS r ON sr.RoleId = r.RoleId WHERE MONTH ( a.CreatedDate ) = MONTH ( CURRENT_DATE ( ) ) AND YEAR ( a.CreatedDate ) = YEAR ( CURRENT_DATE ( ) ) AND r.RoleCode IN ('ATC','AAP','MTP') ";

                //Sebelumnya pakai yg ini
                // string query = "SELECT TotalSurveyTaken, PartnerType FROM (SELECT TotalSurveyTaken, PartnerType, CourseId, OrgId FROM (SELECT COUNT(CourseId) AS TotalSurveyTaken, SiteId, PartnerType, CourseId FROM Courses WHERE MONTH ( CompletionDate ) = MONTH ( CURRENT_DATE ( ) ) AND YEAR ( CompletionDate ) = YEAR ( CURRENT_DATE ( ) ) GROUP BY PartnerType) AS c INNER JOIN Main_site s ON c.SiteId = s.SiteId WHERE s.`Status` = 'A') AS s INNER JOIN EvaluationAnswer ea ON s.CourseId = ea.CourseId ";
                //Diganti tgl 11/08/2018
                string query = "";
                query +=
               // "SELECT COUNT(EvaluationAnswerId) AS TotalSurveyTaken,PartnerType FROM EvaluationAnswer ea " +
               "SELECT COUNT(EvaluationReportId) AS TotalSurveyTaken,PartnerType FROM EvaluationReport ea " +
                "INNER JOIN Courses c ON ea.CourseId = c.CourseId " +
                "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                //"WHERE (ea.DateSubmitted BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01') AND CURDATE()) AND s.`Status` = 'A' ";
                "WHERE(ea.DateSubmitted BETWEEN DATE_FORMAT(NOW(), '%Y-%m-01') AND CURDATE()) AND s.`Status` = 'A' ";
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            // query += "AND m.OrgId in ("+property.Value.ToString()+") ";  
                            query += "AND s.OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }
                    if (property.Name.Equals("siteid"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            // query += "AND m.OrgId in ("+property.Value.ToString()+") ";  
                            query += " AND s.siteid in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                // query += " GROUP BY c.PartnerType, DAY ( a.CreatedDate ) asc"; 
                query += " GROUP BY PartnerType";
                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                // Console.WriteLine(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("total/where")]
     //   [HttpGet("total/where/{obj}")]
        public IActionResult whereTotal([FromBody] dynamic data)
        {
            try
            {
                JObject json = JObject.FromObject(data);
                sb = new StringBuilder();
                // string query = "SELECT r.RoleCode, s.SiteName, DAY ( a.CreatedDate ) AS dayCreated, count( * ) AS TotalSurveyTaken FROM EvaluationQuestion AS q INNER JOIN EvaluationAnswer AS a ON q.EvaluationQuestionCode = a.EvaluationQuestionCode INNER JOIN Courses AS c ON a.CourseId = c.CourseId INNER JOIN Main_site AS s ON s.SiteId = c.SiteId INNER JOIN Main_organization AS o ON s.OrgId = o.OrgId INNER JOIN SiteRoles AS sr ON s.SiteId = sr.SiteId INNER JOIN Roles AS r ON sr.RoleId = r.RoleId WHERE r.RoleCode IN ( 'ATC', 'AAP', 'MTP' ) ";

                //Sebelumnya yg ini
                // string query = "SELECT TotalSurveyTaken, PartnerType FROM (SELECT TotalSurveyTaken, PartnerType, CourseId, OrgId FROM (SELECT COUNT(CourseId) AS TotalSurveyTaken, SiteId, PartnerType, CourseId FROM Courses GROUP BY PartnerType) AS c INNER JOIN Main_site s ON c.SiteId = s.SiteId WHERE s.`Status` = 'A') AS s INNER JOIN EvaluationAnswer ea ON s.CourseId = ea.CourseId ";
                //Diganti tanggal 11/08/2018
                string query =
                //"SELECT COUNT(EvaluationAnswerId) AS TotalSurveyTaken,PartnerType FROM EvaluationAnswer ea " +
                "SELECT COUNT(EvaluationReportId) AS TotalSurveyTaken,s.SiteId FROM EvaluationReport ea " +
                "INNER JOIN Courses c ON ea.CourseId = c.CourseId " +
                "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                //"WHERE (ea.CreatedDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE()) AND s.`Status` = 'A' ";
                "WHERE (ea.DateSubmitted BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE()) AND s.`Status` = 'A' ";
                // "SELECT COUNT(EvaluationAnswerId) AS TotalSurveyTaken,PartnerType FROM EvaluationAnswer ea " +
               

                //var o1 = JObject.Parse(obj);
                //JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            // query += "AND m.OrgId in ("+property.Value.ToString()+") ";  
                            query += "AND s.OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }

                    if (property.Name.Equals("siteid"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND s.SiteId in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                // query += " GROUP BY c.PartnerType asc"; 
                query += " GROUP BY s.SiteId";
                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardCoursesDelivered")]
    public class DashboardCoursesDelivered : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardCoursesDelivered(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select t.Territory_Name, Count(*) as TotalCourseDeliveredByPartners from Main_organization as m inner join Main_site as s on m.OrgId = s.OrgId inner join Ref_countries as tc on tc.countries_code = m.RegisteredCountryCode inner join Ref_territory as t on t.TerritoryId = tc.TerritoryId inner join Courses as c on s.SiteId = c.SiteId where MONTH ( c.StartDate ) = MONTH ( CURRENT_DATE ( ) ) AND YEAR ( c.StartDate ) = YEAR ( CURRENT_DATE ( ) ) AND m.Status = 'A' group by t.Territory_Name");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardCoursesDeliveredBySite")]
    public class DashboardCoursesDeliveredBySite : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardCoursesDeliveredBySite(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select s.SiteName, Count(*) as TotalCourseDeliveredByPartners from Main_organization as m inner join Main_site as s on m.OrgId = s.OrgId inner join Ref_countries as tc on tc.countries_code = m.RegisteredCountryCode inner join Ref_territory as t on t.TerritoryId = tc.TerritoryId inner join Courses as c on s.SiteId = c.SiteId where s.Status = 'A' group by s.SiteName");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardMonthEvaluationSurveyByGeo")]
    public class DashboardMonthEvaluationSurveyByGeo : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardMonthEvaluationSurveyByGeo(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select rc.geo_name, count(*) as TotalMOnthlySurveyTaken from EvaluationQuestion as q inner join EvaluationAnswer as a on q.EvaluationQuestionCode = a.EvaluationQuestionCode inner join Ref_countries as rc on rc.countries_code = q.CountryCode where month(a.CreatedDate) = month(current_date()) and year(a.CreatedDate) = year(current_date()) group by rc.geo_name");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardTotalYearEvaluationSurvey")]
    public class DashboardTotalYearEvaluationSurvey : Controller
    {
        StringBuilder sb = new StringBuilder();
        MySqlDb oDb = new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardTotalYearEvaluationSurvey(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select r.RoleCode, count(*) as TotalYearSurveyTaken from EvaluationQuestion as q inner join EvaluationAnswer as a on q.EvaluationQuestionCode = a.EvaluationQuestionCode inner join Roles as r on r.RoleId = q.PartnerType where year(a.CreatedDate) = year(current_date()) group by r.RoleCode");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("AAP/where/{obj}")]
        public IActionResult whereAAP(string obj)
        {
            JArray listterritory;
            JArray listcountpartner;
            string roleid = "";
            try
            {
                sb = new StringBuilder();
                string query = "SELECT p.productName AS KeyValue, Count( * ) AS TotalCourseMonthByCoreProduct FROM Main_organization AS m INNER JOIN Main_site AS s ON m.OrgId = s.OrgId INNER JOIN TerritoryCountry AS tc ON tc.CountryCode = m.RegisteredCountryCode INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId INNER JOIN SiteRoles AS sr ON sr.SiteId = s.SiteId INNER JOIN Roles AS r ON r.RoleId = sr.RoleId INNER JOIN Courses AS c ON s.SiteId = c.SiteId INNER JOIN CourseSoftware AS cs ON cs.CourseId = c.CourseId INNER JOIN Products AS p ON p.productId = cs.productId WHERE m.STATUS = 'A' AND c.PartnerType IN ('AAP Course','AAP Project','AAP Event')";
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND m.OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                query += " GROUP BY t.Territory_Name, p.productName;";
                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("ATC/where/{obj}")]
        public IActionResult whereATC(string obj)
        {
            JArray listterritory;
            JArray listcountpartner;
            string roleid = "";
            try
            {
                sb = new StringBuilder();
                string query = "SELECT p.productName AS KeyValue, Count( * ) AS TotalCourseMonthByCoreProduct FROM Main_organization AS m INNER JOIN Main_site AS s ON m.OrgId = s.OrgId INNER JOIN TerritoryCountry AS tc ON tc.CountryCode = m.RegisteredCountryCode INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId INNER JOIN SiteRoles AS sr ON sr.SiteId = s.SiteId INNER JOIN Roles AS r ON r.RoleId = sr.RoleId INNER JOIN Courses AS c ON s.SiteId = c.SiteId INNER JOIN CourseSoftware AS cs ON cs.CourseId = c.CourseId INNER JOIN Products AS p ON p.productId = cs.productId WHERE m.STATUS = 'A' AND c.PartnerType LIKE '%ATC%'";
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND m.OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                query += " GROUP BY t.Territory_Name, p.productName;";
                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardCoursesDeliveredByPartners")]
    public class DashboardCoursesDeliveredByPartners : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardCoursesDeliveredByPartners(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select r.RoleCode as KeyValue, Count(*) as TotalCourseMonthByPartnerType from Main_organization as m inner join Main_site as s on m.OrgId = s.OrgId inner join SiteRoles as sr on sr.SiteId = s.SiteId inner join Roles as r on r.RoleId = sr.RoleId inner join Courses as c on s.SiteId = c.SiteId where m.Status = 'A' and month(c.CompletionDate) = month(current_date()) and year(c.CompletionDate) = year(current_date()) Group by r.RoleCode union select p.productName as KeyValue, Count(*) as TotalCourseMonthByPartnerType from Main_organization as m inner join Main_site as s on m.OrgId = s.OrgId inner join SiteRoles as sr on sr.SiteId = s.SiteId inner join Roles as r on r.RoleId = sr.RoleId inner join Courses as c on s.SiteId = c.SiteId inner join CourseSoftware as cs on cs.CourseId = c.CourseId inner join Products as p on p.productId = cs.productId where m.Status = 'A' and month(c.CompletionDate) = month(current_date()) and year(c.CompletionDate) = year(current_date()) Group by p.productName;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardEvaluationByCoreProducts")]
    public class DashboardEvaluationByCoreProducts : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardEvaluationByCoreProducts(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select p.productName, count(*) as TotalSurveyTaken from EvaluationQuestion as q inner join EvaluationAnswer as a on q.EvaluationQuestionCode = a.EvaluationQuestionCode inner join Courses as c on q.CourseId = c.CourseCode inner join CourseSoftware as cs on cs.CourseId = c.CourseId inner join Products as p on p.productId = cs.productId group by p.productName");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardPerformanceSite")]
    public class DashboardPerformanceSite : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardPerformanceSite(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet("{orgid}")]
        public IActionResult Select(string orgid)
        {
            try
            {
                string query = "select geo_name, TotalPerformanceSite from ( select rg.geo_name, Count(*) as TotalPerformanceSite from Main_organization as m inner join Main_site as s on m.OrgId = s.OrgId inner join Ref_countries as rc on rc.countries_code = m.RegisteredCountryCode inner join Ref_geo as rg on rg.geo_code = rc.geo_code inner join Courses as c on s.SiteId = c.SiteId where s.Status = 'A' and YEAR(s.DateAdded) = YEAR(NOW()) and MONTH(s.DateAdded) = MONTH(NOW()) ";
                if (orgid != "null")
                {
                    query += " and m.OrgId in (" + orgid + ") ";
                }
                query += "group by rg.geo_code union select geo_name, 0 TotalPerformanceSite from Ref_geo) as sitemonthperform GROUP BY geo_name";
                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardTop10PerformanceInstructor")]
    public class DashboardTop10PerformanceInstructor : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        public DashboardTop10PerformanceInstructor(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet("where/{obj}")]
        public IActionResult where(string obj)
        {
            try
            {
                string query = "select ca.ContactIdInt, ca.ContactId, ca.ContactName, Count(*) as TotalCourseDeliveredByIntructor from Main_organization as m inner join Main_site as s on m.OrgId = s.OrgId inner join Ref_countries as tc on tc.countries_code = m.RegisteredCountryCode inner join Ref_territory as t on t.TerritoryId = tc.TerritoryId inner join Courses as c on s.SiteId = c.SiteId inner join SiteContactLinks as sc on s.SiteId = sc.SiteId inner join Contacts_All as ca on ca.ContactId = sc.ContactId where s.Status = 'A' ";
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND s.OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                query += "group by ca.ContactId ORDER BY TotalCourseDeliveredByIntructor DESC LIMIT 10 ";
                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardPartnerSurvey")]
    public class DashboardPartnerSurvey : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        public DashboardPartnerSurvey(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT r.RoleName, Count( * ) AS TotalPartnersOnSurvey FROM EvaluationAnswer AS ea INNER JOIN Courses AS c ON ea.CourseId = c.CourseId INNER JOIN Roles AS r ON CASE WHEN (c.ATCFacility <> '0' OR c.ATCComputer <> '0') THEN 'ATC' ELSE 'AAP' END = r.RoleCode GROUP BY r.RoleName");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardSitesInEvaSurvey")]
    public class DashboardSitesInEvaSurvey : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        String result2;
        public DashboardSitesInEvaSurvey(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet("{role}/where/{obj}")]
        public IActionResult whereAAP(string role, string obj)
        {
            JObject resObj;
            JObject resObj2;
            try
            {
                string roleid = "";
                if (role == "ATC")
                {
                    roleid = "(PartnerType = '1' or PartnerType = '%1,58%') ";
                }
                else
                {
                    roleid = "PartnerType = '58' ";
                }
               
                // string query = "SELECT PartnerType, COUNT(*) as TotalSitesEval FROM (SELECT SiteId, c.CourseId, SUBSTRING(PartnerType, 1, 3) AS PartnerType FROM Courses c INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId WHERE SUBSTRING(PartnerType, 1, 3) = '"+role+"' GROUP BY SiteId) AS c INNER JOIN Main_site s ON c.SiteId = s.SiteId WHERE s.`Status` = 'A' ";
                //string query =
                //"SELECT (CASE WHEN RoleId = '58' THEN 'AAP' ELSE 'ATC' END) AS PartnerType, COUNT(e.SiteId) AS TotalSitesEval FROM " +
                //"(SELECT DISTINCT SiteId,RoleId FROM EvaluationReport WHERE RoleId = '" + roleid + "' and Status ='A') AS e " +
                //"INNER JOIN Main_site s ON e.SiteId = s.SiteId " +
                //"WHERE s.`Status` = 'A' ";
                string query = "SELECT Territory_Name,COUNT(ss.SiteId) AS TotalSitesEval FROM " +
                    " (SELECT DISTINCT er.SiteId, group_concat(DISTINCT er.RoleId ORDER BY er.RoleId) as 'PartnerType', s.SiteCountryCode, s.OrgId " +
                    "FROM EvaluationReport er INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
                    "where  YEAR(er.DateSubmitted) = YEAR(NOW()) and er.Status = 'A' and s.`Status` = 'A' group by er.SiteId) ss " +
                    "INNER JOIN Ref_countries rc ON ss.SiteCountryCode = rc.countries_code " +
                    "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                    "WHERE " + roleid;
              //string query2 = "SELECT COUNT(*) as TotalSite FROM Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId WHERE RoleId = '" + roleid + "' and s.`Status` = 'A' and sr.Status ='A' ";
                string query2 = "SELECT Territory_Name, COUNT(ss.SiteId) AS TotalSite FROM " +
                    "(select s.OrgId, s.SiteId, group_concat(DISTINCT sr.RoleId ORDER BY sr.RoleId) as 'PartnerType', sr.Status, s.SiteCountryCode " +
                    "from Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId " +
                    "WHERE s.`Status` = 'A' AND sr.Status = 'A' group by s.SiteId) ss " +
                    "INNER JOIN Ref_countries rc ON ss.SiteCountryCode = rc.countries_code " +
                    "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                    "WHERE " + roleid + " AND ss.`Status` = 'A' ";
                var o1 = JObject.Parse(obj);
                JObject json = JObject.FromObject(o1);

                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND s.OrgId in (" + property.Value.ToString() + ") ";
                            query2 += "AND s.OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                // query += "GROUP BY SUBSTRING(PartnerType, 1, 3)"; 
                query += "GROUP BY rt.TerritoryId order by rt.Territory_Name; ";
                query2 += "GROUP BY rt.TerritoryId order by rt.Territory_Name; ";
                // Console.WriteLine(query);
                DataTable dt1 = new DataTable();
                DataTable dt = new DataTable();
                sb = new StringBuilder();
                sb.AppendFormat(query);
                //Console.WriteLine(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if(ds.Tables[0].Rows.Count > 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dt = ds.Tables[0];
                    dt.PrimaryKey = new DataColumn[] { dt.Columns["Territory_Name"] };
                }
                

                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                //resObj = JObject.Parse(result);
                sb = new StringBuilder();
                sb.AppendFormat(query2);
                //Console.WriteLine(query2);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count > 0)
                {
                  //  ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dt1 = ds.Tables[0];
                    dt1.PrimaryKey = new DataColumn[] { dt1.Columns["Territory_Name"] };
                }

                dt1.Merge(dt);
                DataTable dtTmp = dt1.Copy();
                dtTmp.Columns.Add("NonCount", typeof(System.Int32));
                DataColumnCollection tmpcolumns = dtTmp.Columns;
                foreach (DataRow row in dtTmp.Rows)
                {
                    if (!tmpcolumns.Contains("TotalSite"))
                    {
                        dtTmp.Columns.Add("TotalSite", typeof(System.Int32));
                        row["TotalSite"] = 0;
                    }
                    if (!tmpcolumns.Contains("TotalSitesEval"))
                    {
                        dtTmp.Columns.Add("TotalSitesEval", typeof(System.Int32));
                        row["TotalSitesEval"] = 0;
                    }
                    int siteCount = 0;
                    int EvaluationCount = 0;
                    if (!string.IsNullOrEmpty(row["TotalSite"].ToString()))
                    {
                        siteCount = Convert.ToInt32(row["TotalSite"].ToString());
                    }
                    else
                    {
                        row["TotalSite"] = 0;
                    }
                    if (!string.IsNullOrEmpty(row["TotalSitesEval"].ToString()))
                    {
                        EvaluationCount = Convert.ToInt32(row["TotalSitesEval"].ToString());
                    }
                    else
                    {
                        row["TotalSitesEval"] = 0;
                    }
                    if (siteCount > EvaluationCount)
                    {
                        int NnonCount = siteCount - EvaluationCount;
                        row["NonCount"] = NnonCount;
                    }
                    else
                    {
                        row["NonCount"] = 0;

                    }

                }
                for (int j = 0; j < dtTmp.Rows.Count; j++)
                { if (dtTmp.Rows[j]["TotalSitesEval"].ToString() == "0")
                {
                        dtTmp.Rows[j].Delete();
                        dtTmp.AcceptChanges();
                }
               
                }

                result = MySqlDb.GetJSONObjectString(dtTmp);
                result = "[" + result + "]";
               //resObj2 = JObject.Parse(result);
                //int TotalSitesEval = Int32.Parse(resObj["TotalSitesEval"].ToString());
                //int TotalSite = Int32.Parse(resObj2["TotalSite"].ToString());
                //int TotalSitesUnEval = TotalSite - TotalSitesEval;
                //resObj["TotalSitesUnEval"] = TotalSitesUnEval.ToString();
            }
            catch(Exception ex)
            {
                result = null;
            }
            finally
            {
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardSitesMeetAnnualReq")]
    public class DashboardSitesMeetAnnualReq : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        public DashboardSitesMeetAnnualReq(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }

        [HttpGet("where/{obj}")]
        public IActionResult where(string obj)
        {
            JArray listterritory;
            JArray listcountpartner;
            string mom = "";
            string org = "";
            string org_jek = "";
            var o1 = JObject.Parse(obj);
            DataTable dtTotal = new DataTable();
            DataTable dtMeet = new DataTable();
            DataTable dtNotMeet = new DataTable();
            DataTable dtTmp = new DataTable();
            JObject json = JObject.FromObject(o1);
            try
            {
                string queryterritory = "";
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            queryterritory = "select Territory_Name from Ref_territory where TerritoryId in (select rc.TerritoryId from Main_organization o INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code where o.OrgId in (" + property.Value.ToString() + ")) ORDER BY Territory_Name asc";
                        }
                        else
                        {
                            queryterritory = "select Territory_Name from Ref_territory ORDER BY Territory_Name asc ";
                        }
                    }
                }
                sb = new StringBuilder();
                sb.Append(queryterritory);
                sqlCommand = new MySqlCommand(sb.ToString());
                //Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                listterritory = JArray.Parse(MySqlDb.GetJSONArrayString(ds.Tables[0]));
                string[] tmp = null;
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            org += "AND OrgId in (" + property.Value.ToString() + ") ";
                            org_jek = property.Value.ToString();
                        }
                    }
                    if (property.Name.Equals("MeetNotMeet"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            mom = property.Value.ToString();
                            tmp = mom.Split(',');

                        }
                    }
                }
                string label = "";
                string color = "";
                string tmpCOE = "";
                string query = "";
                foreach (string w in tmp)
                {
                    if (w == ">=") tmpCOE = "MeetCount";
                    else if (w == "<") tmpCOE = "NotMeetCount";
                    else if (w == "0") tmpCOE = "TotalCount";

                    query = "";
                    // query = "select '" + label + "' Label, Territory_Name, COUNT(COE) as MEETORNOTMEET, '" + color + "' BackgroundColor from (select SiteCountryCode, CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END COE from (select CourseId, SiteCountryCode from (select SiteId, SiteCountryCode from Main_site where `Status` = 'A' " + org + ") as s " +
                    query = "select Territory_Name, COUNT(COE) as " + tmpCOE + " from (select SiteCountryCode, CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END COE, c.PartnerType from (select CourseId, SiteCountryCode,s.PartnerType from (select Main_site.SiteId, Main_site.SiteCountryCode , group_concat(DISTINCT SiteRoles.RoleId ORDER BY SiteRoles.RoleId ) as 'PartnerType' from  Main_site inner join SiteRoles on Main_site.SiteId = SiteRoles.SiteId where SiteRoles.`Status` = 'A' " + org + " group by SiteRoles.SiteId) as s " +
                     "INNER JOIN Courses c ON s.SiteId = c.SiteId AND YEAR(c.StartDate) = YEAR(NOW())) as c INNER JOIN EvaluationReport er ON c.CourseId = er.CourseId GROUP BY c.CourseId) as er INNER JOIN Ref_countries rc ON er.SiteCountryCode = rc.countries_code INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId ";
                    //"INNER JOIN Courses c ON s.SiteId = c.SiteId AND YEAR(c.StartDate) = YEAR(NOW())) as c INNER JOIN EvaluationReport er ON c.CourseId = er.CourseId GROUP BY c.CourseId) as er INNER JOIN Ref_countries rc ON er.SiteCountryCode = rc.countries_code INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId ";
                    // "WHERE COE " + mom + " 85 and PartnerType = '1' or PartnerType like '%1,58%' GROUP BY rt.TerritoryId";

                    // query = "SELECT '" + label + "' Label, Territory_Name,MEETORNOTMEET,'" + color + "' BackgroundColor FROM tmpDashboardMeetAnnual";
                    if (w == ">=")
                    {
                        query += "WHERE COE >= 85 and (PartnerType = '1' or PartnerType like '%1,58%') GROUP BY rt.TerritoryId order by rt.Territory_Name";
                        label = "Meet";
                        color = "#669911";
                        sb1.Clear();
                        sb1.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb1.ToString());
                        // Console.WriteLine(sb1.ToString());
                        DataSet dsMeet = oDb.getDataSetFromSP(sqlCommand);
                        if (dsMeet.Tables[0].Rows.Count != 0)
                        {
                            dtMeet = dsMeet.Tables[0];
                            dtMeet.PrimaryKey = new DataColumn[] { dtMeet.Columns["Territory_Name"] };
                        }

                    }
                    else if (w == "<")
                    {
                        query += "WHERE COE < 85 and (PartnerType = '1' or PartnerType like '%1,58%') GROUP BY rt.TerritoryId order by rt.Territory_Name";
                        tmpCOE = "COE < 85 and";
                        label = "Not Meet";
                        color = "#5FBEAA";
                        sb1.Clear();
                        sb1.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb1.ToString());
                        DataSet dsNotMeet = oDb.getDataSetFromSP(sqlCommand);
                        if (dsNotMeet.Tables[0].Rows.Count != 0)
                        {
                            dtNotMeet = dsNotMeet.Tables[0];
                            dtNotMeet.PrimaryKey = new DataColumn[] { dtNotMeet.Columns["Territory_Name"] };
                        }
                    }
                    else if (w == "0")
                    {
                        query += "WHERE COE <= 100 and COE >=0 and (PartnerType = '1' or PartnerType like '%1,58%') GROUP BY rt.TerritoryId order by rt.Territory_Name";
                        tmpCOE = "";
                        sb1.Clear();
                        sb1.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb1.ToString());
                        DataSet dsTotal = oDb.getDataSetFromSP(sqlCommand);
                        if (dsTotal.Tables[0].Rows.Count != 0)
                        {
                            dtTotal = dsTotal.Tables[0];
                            dtTotal.PrimaryKey = new DataColumn[] { dtTotal.Columns["Territory_Name"] };
                        }
                    }
                }
                dtMeet.Merge(dtNotMeet);
                dtMeet.Merge(dtTotal);
                dtTmp = dtMeet.Copy();
                //dtTmp = MergeTablesByIndex(dtTotal, dtMeet);// dtTotal.Copy();
                //dtTmp = MergeTablesByIndex(dtTmp, dtNotMeet);

                dtTmp.Columns.Add("perSiteMeet", typeof(System.Int32));
                dtTmp.Columns.Add("perSiteNotMeet", typeof(System.Int32));
                DataColumnCollection tmpcolumns = dtTmp.Columns;
                foreach (DataRow row in dtTmp.Rows)
                {
                    if (!tmpcolumns.Contains("TotalCount"))
                    {
                        dtTmp.Columns.Add("TotalCount", typeof(System.Int32));
                        row["TotalCount"] = 0;
                    }
                    if(!tmpcolumns.Contains("MeetCount"))
                    {
                        dtTmp.Columns.Add("MeetCount", typeof(System.Int32));
                        row["MeetCount"] = 0;
                    }
                    if(!tmpcolumns.Contains("NotMeetCount"))
                    {
                        dtTmp.Columns.Add("NotMeetCount", typeof(System.Int32));
                        row["NotMeetCount"] = 0;
                    }
                    int MeetCount = 0;
                    int NotMeetCount = 0;
                    int total = 0;
                    if (!string.IsNullOrEmpty(row["TotalCount"].ToString()))
                    {
                        total = Convert.ToInt32(row["TotalCount"].ToString());
                    }
                    else { row["TotalCount"] = 0; }
                    if (!string.IsNullOrEmpty(row["MeetCount"].ToString()))
                    {
                        MeetCount = Convert.ToInt32(row["MeetCount"].ToString());
                    }
                    else { row["MeetCount"] = 0; }
                    if (!string.IsNullOrEmpty(row["NotMeetCount"].ToString()))
                    {
                        NotMeetCount = Convert.ToInt32(row["NotMeetCount"].ToString());
                    }
                    else { row["NotMeetCount"] = 0; }
                    if (total != 0)
                    {
                        int tmpMeet = (int)Math.Round((double)(100 * MeetCount) / total); //(MeetCount / total) * 100;
                        int tmpNotMeet = (int)Math.Round((double)(100 * NotMeetCount) / total); //(NotMeetCount / total) * 100;
                        row["perSiteMeet"] = tmpMeet;
                        row["perSiteNotMeet"] = tmpNotMeet;
                    }
                    else
                    {
                        row["perSiteMeet"] = 0;
                        row["perSiteNotMeet"] = 0;
                    }
                   
                }

               
                //if (org != "")
                //{
                //    query +=
                //        "SELECT s.SiteId, '" + label + "' Label, Territory_Name, COUNT(COE) as MEETORNOTMEET, '" + color + "' BackgroundColor FROM " +
                //        "(SELECT s.SiteId, SiteCountryCode," +
                //        "(SELECT " +
                //        "CAST(" +
                //        "CASE WHEN COUNT(StudentID) = 0 then 0 else " +
                //        "(" +
                //            "CASE WHEN COUNT(DISTINCT StudentID) > 99 THEN (SUM(CASE WHEN ScoreOE IN ('75','100') THEN 1 ELSE 0 END) / CAST(COUNT(DISTINCT StudentID) as DECIMAL(18,6))) * 100 ELSE 0 END " +
                //            "- (SQRT(CAST((" +
                //            "CASE WHEN COUNT(DISTINCT StudentID) > 99 THEN (SUM(CASE WHEN ScoreOE IN ('75','100') THEN 1 ELSE 0 END) / CAST(COUNT(DISTINCT StudentID) as DECIMAL(18,6))) * 100 ELSE 0 END " +
                //            "* (100 -" +
                //            "CASE WHEN COUNT(DISTINCT StudentID) > 99 THEN (SUM(CASE WHEN ScoreOE IN ('75','100') THEN 1 ELSE 0 END) / CAST(COUNT(DISTINCT StudentID) as DECIMAL(18,6))) * 100 ELSE 0 END " +
                //            ")) / COUNT(DISTINCT StudentID) as decimal(18,10)))  * 2.56)) " +
                //        "end AS DECIMAL ( 7, 2 )) FROM EvaluationReport er WHERE er.SiteId = s.SiteId) COE " +
                //        "FROM Main_site s " +
                //        "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId and sr.`Status` = 'A' " +
                //        "INNER JOIN " +
                //        "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                //        "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                //        "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                //        "WHERE o.OrgId IN (" + org_jek + "))  ds " +
                //        "ON s.SiteCountryCode = ds.CountryCode) AS s " +
                //        "INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " +
                //        "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                //        "WHERE s.COE " + mom + " 85 GROUP BY Territory_Name ASC ";
                //}
                //else
                //{
                //    // query = "select '" + label + "' Label, Territory_Name, COUNT(COE) as MEETORNOTMEET, '" + color + "' BackgroundColor from (select SiteCountryCode, CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END COE from (select CourseId, SiteCountryCode from (select SiteId, SiteCountryCode from Main_site where `Status` = 'A' " + org + ") as s " +
                //    query = "select  Territory_Name, COUNT(COE) as MEETORNOTMEET from (select SiteCountryCode, CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END COE, c.PartnerType from (select CourseId, SiteCountryCode,s.PartnerType from (select SiteId, SiteCountryCode Main_site.SiteId, Main_site.SiteCountryCode , group_concat(DISTINCT SiteRoles.RoleId ORDER BY SiteRoles.RoleId ) as 'PartnerType' from  Main_site inner join SiteRoles on Main_site.SiteId = SiteRoles.SiteId where `Status` = 'A' " + org + " group by SiteRoles.SiteId) as s " +
                //     "INNER JOIN Courses c ON s.SiteId = c.SiteId AND YEAR(c.StartDate) = YEAR(NOW())) as c INNER JOIN EvaluationReport er ON c.CourseId = er.CourseId GROUP BY c.CourseId) as er INNER JOIN Ref_countries rc ON er.SiteCountryCode = rc.countries_code INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                //     "INNER JOIN Courses c ON s.SiteId = c.SiteId AND YEAR(c.StartDate) = YEAR(NOW())) as c INNER JOIN EvaluationReport er ON c.CourseId = er.CourseId GROUP BY c.CourseId) as er INNER JOIN Ref_countries rc ON er.SiteCountryCode = rc.countries_code INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                //   // "WHERE COE " + mom + " 85 and PartnerType = '1' or PartnerType like '%1,58%' GROUP BY rt.TerritoryId";
                //   "WHERE " + tmpCOE + " and PartnerType = '1' or PartnerType like '%1,58%' GROUP BY rt.TerritoryId";
                //    // query = "SELECT '" + label + "' Label, Territory_Name,MEETORNOTMEET,'" + color + "' BackgroundColor FROM tmpDashboardMeetAnnual";
                //}
                //sb1.AppendFormat(query);
                //sqlCommand = new MySqlCommand(sb1.ToString());
                //// Console.WriteLine(sb1.ToString());
                //ds1 = oDb.getDataSetFromSP(sqlCommand);
                listcountpartner = JArray.Parse(MySqlDb.GetJSONArrayString(dtTmp));
                //int index = 0;
                //if (listcountpartner.Count > 0)
                //{
                //    foreach (var itemlistterritory in listterritory)
                //    {
                //        itemlistterritory["Count"] = "0";
                //        itemlistterritory["Label"] = listcountpartner[0]["Label"];
                //        itemlistterritory["BackgroundColor"] = listcountpartner[0]["BackgroundColor"];
                //        foreach (var itemlistcountpartner in listcountpartner)
                //        {
                //            if (itemlistterritory["Territory_Name"].ToString() == itemlistcountpartner["Territory_Name"].ToString())
                //            {
                //                itemlistterritory["Count"] = itemlistcountpartner["MEETORNOTMEET"];
                //            }
                //        }
                //    }
                //}
                //else
                //{
                //    foreach (var itemlistterritory in listterritory)
                //    {
                //        itemlistterritory["Count"] = "0";
                //        if (mom == ">=")
                //        {
                //            itemlistterritory["Label"] = "Meet";
                //            itemlistterritory["BackgroundColor"] = "#669911";
                //        }
                //        else
                //        {
                //            itemlistterritory["Label"] = "Not Meet";
                //            itemlistterritory["BackgroundColor"] = "#5FBEAA";
                //        }
                //    }
                //}
            }
            catch(Exception ex)
            {
                result = null;
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(dtTmp);
            }
            return Ok(result);
        }

        [HttpGet("whereAAP/{obj}")]
        public IActionResult whereAAP(string obj)
        {
            JArray listterritory;
            JArray listcountpartner;
            string mom = "";
            string org = "";
            string org_jek = "";
            var o1 = JObject.Parse(obj);
            DataTable dtTotal = new DataTable();
            DataTable dtMeet = new DataTable();
            DataTable dtNotMeet = new DataTable();
            DataTable dtTmp = new DataTable();
            JObject json = JObject.FromObject(o1);
            try
            {
                string queryterritory = "";
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            queryterritory = "select Territory_Name from Ref_territory where TerritoryId in (select rc.TerritoryId from Main_organization o INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code where o.OrgId in (" + property.Value.ToString() + ")) ORDER BY Territory_Name asc";
                        }
                        else
                        {
                            queryterritory = "select Territory_Name from Ref_territory ORDER BY Territory_Name asc ";
                        }
                    }
                }
                sb = new StringBuilder();
                sb.Append(queryterritory);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                listterritory = JArray.Parse(MySqlDb.GetJSONArrayString(ds.Tables[0]));
                string[] tmp = null;
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            org += "AND OrgId in (" + property.Value.ToString() + ") ";
                            org_jek = property.Value.ToString();
                        }
                    }
                    if (property.Name.Equals("MeetNotMeet"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            mom = property.Value.ToString();
                            tmp = mom.Split(',');

                        }
                    }
                }
                string tmpCOE = "";
                string query = "";
                
                foreach (string w in tmp)
                {
                    if (w == ">=") tmpCOE = "MeetCount";
                    else if (w == "<") tmpCOE = "NotMeetCount";
                    else if (w == "0") tmpCOE = "TotalCount";

                    query = "";
                    query = "select Territory_Name, COUNT(COE) as " + tmpCOE + " from (select SiteCountryCode, CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END COE, c.PartnerType from (select CourseId, SiteCountryCode,s.PartnerType from (select Main_site.SiteId, Main_site.SiteCountryCode , group_concat(DISTINCT SiteRoles.RoleId ORDER BY SiteRoles.RoleId ) as 'PartnerType' from  Main_site inner join SiteRoles on Main_site.SiteId = SiteRoles.SiteId where SiteRoles.`Status` = 'A' " + org + " group by SiteRoles.SiteId) as s " +
                     "INNER JOIN Courses c ON s.SiteId = c.SiteId AND YEAR(c.StartDate) = YEAR(NOW())) as c INNER JOIN EvaluationReport er ON c.CourseId = er.CourseId GROUP BY c.CourseId) as er INNER JOIN Ref_countries rc ON er.SiteCountryCode = rc.countries_code INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId ";                    //"INNER JOIN Courses c ON s.SiteId = c.SiteId AND YEAR(c.StartDate) = YEAR(NOW())) as c INNER JOIN EvaluationReport er ON c.CourseId = er.CourseId GROUP BY c.CourseId) as er INNER JOIN Ref_countries rc ON er.SiteCountryCode = rc.countries_code INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId ";
                    if (w == ">=")
                    {
                        query += "WHERE COE >= 85 and PartnerType = '58' GROUP BY rt.TerritoryId order by rt.Territory_Name";
                        sb1.Clear();
                        sb1.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb1.ToString());
                        DataSet dsMeet = oDb.getDataSetFromSP(sqlCommand);
                        if (dsMeet.Tables[0].Rows.Count != 0)
                        {
                            
                            dtMeet = dsMeet.Tables[0];
                            dtMeet.PrimaryKey = new DataColumn[] { dtMeet.Columns["Territory_Name"] };
                            //var prime1 = dtMeet.Columns[0].ColumnName.ToString(); //dtMeet.Columns("Territory_Name", typeof(string));
                            //dtMeet.PrimaryKey = new DataColumn[] { prime1 };
                        }

                    }
                    else if (w == "<")
                    {
                        query += "WHERE COE < 85 and PartnerType = '58'  GROUP BY rt.TerritoryId order by rt.Territory_Name";
                        sb1.Clear();
                        sb1.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb1.ToString());
                        DataSet dsNotMeet = oDb.getDataSetFromSP(sqlCommand);
                        if (dsNotMeet.Tables[0].Rows.Count != 0)
                        { 
                            dtNotMeet = dsNotMeet.Tables[0];
                            dtNotMeet.PrimaryKey = new DataColumn[] { dtNotMeet.Columns["Territory_Name"] };
                        }
                    }
                    else if (w == "0")
                    {
                        query += "WHERE COE <= 100 and COE >=0 and PartnerType = '58'  GROUP BY rt.TerritoryId order by rt.Territory_Name";
                        sb1.Clear();
                        sb1.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb1.ToString());
                        DataSet dsTotal = oDb.getDataSetFromSP(sqlCommand);
                        if (dsTotal.Tables[0].Rows.Count != 0)
                        {
                            dtTotal = dsTotal.Tables[0];
                            dtTotal.PrimaryKey = new DataColumn[] { dtTotal.Columns["Territory_Name"] };
                        }
                    }
                }

                dtMeet.Merge(dtNotMeet);
                dtMeet.Merge(dtTotal);
                dtTmp = dtMeet.Copy();
                dtTmp.Columns.Add("perSiteMeet", typeof(System.Int32));
                dtTmp.Columns.Add("perSiteNotMeet", typeof(System.Int32));
                DataColumnCollection tmpcolumns = dtTmp.Columns;
                foreach (DataRow row in dtTmp.Rows)
                {
                    if (!tmpcolumns.Contains("TotalCount"))
                    {
                        dtTmp.Columns.Add("TotalCount", typeof(System.Int32));
                        row["TotalCount"] = 0;
                    }
                    if (!tmpcolumns.Contains("MeetCount"))
                    {
                        dtTmp.Columns.Add("MeetCount", typeof(System.Int32));
                        row["MeetCount"] = 0;
                    }
                    if (!tmpcolumns.Contains("NotMeetCount"))
                    {
                        dtTmp.Columns.Add("NotMeetCount", typeof(System.Int32));
                        row["NotMeetCount"] = 0;
                    }
                    int MeetCount = 0;
                    int NotMeetCount = 0;
                    int total =0;
                    if (!string.IsNullOrEmpty(row["TotalCount"].ToString()))
                    {
                        total = Convert.ToInt32(row["TotalCount"].ToString());
                    }
                    else { row["TotalCount"] = 0; }
                    if (!string.IsNullOrEmpty(row["MeetCount"].ToString())) {
                        MeetCount = Convert.ToInt32(row["MeetCount"].ToString());
                    }
                    else { row["MeetCount"] = 0; }
                    if (!string.IsNullOrEmpty(row["NotMeetCount"].ToString()))
                    {
                        NotMeetCount = Convert.ToInt32(row["NotMeetCount"].ToString());
                    }
                    else { row["NotMeetCount"] = 0; }
                    if (total != 0)
                    {
                        int tmpMeet = (int)Math.Round((double)(100 * MeetCount) / total); 
                        int tmpNotMeet = (int)Math.Round((double)(100 * NotMeetCount) / total); 
                        row["perSiteMeet"] = tmpMeet;
                        row["perSiteNotMeet"] = tmpNotMeet;
                    }
                    else
                    {
                        row["perSiteMeet"] = 0;
                        row["perSiteNotMeet"] = 0;
                    }

                }

               
                listcountpartner = JArray.Parse(MySqlDb.GetJSONArrayString(dtTmp));
                
            }
            catch (Exception ex)
            {
                result = null;
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(dtTmp);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardTmpCoreProductATC")]
    public class DashboardTmpCoreProductATC : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        public DashboardTmpCoreProductATC(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult get()
        {
            string result = "";
            string result2 = "";
            string result3 = "";
            JArray arr1;
            JObject obj1;
            JObject obj2;
            List<JObject> myList = new List<JObject>();
            try
            {
                sb.AppendFormat("select * from tmpDashboardATC1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr1 = JArray.Parse(result);
                for (int i = 0; i < arr1.Count; i++)
                {
                    myList.Add(
                        JObject.Parse(
                            "{" +
                            "\"productName\":\"" + arr1[i]["productName"].ToString() + "\"," +
                            "\"TotalCourseEvaCoreProduct\":\"" + arr1[i]["TotalCourseEvaCoreProduct"].ToString() + "\"" +
                            "}"
                        )
                    );
                }
                sb = new StringBuilder();
                sb.AppendFormat("select * from tmpDashboardATC3");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result2 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                obj1 = JObject.Parse(result2);
                int TotalCourseEvaAllProduct = Int32.Parse(obj1["TotalCourseEvaAllProduct"].ToString());
                sb = new StringBuilder();
                sb.AppendFormat("select * from tmpDashboardATC2");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result3 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                obj2 = JObject.Parse(result3);
                int TotalCourseEvaCoreProduct = Int32.Parse(obj2["TotalCourseEvaCoreProduct"].ToString());
                int notcoreproduct = TotalCourseEvaAllProduct - TotalCourseEvaCoreProduct;
                myList.Add(
                    JObject.Parse(
                        "{" +
                        "\"productName\":\"Not Core Product\"," +
                        "\"TotalCourseEvaCoreProduct\":\"" + notcoreproduct.ToString() + "\"" +
                        "}"
                    )
                );
            }
            catch
            {
                myList = null;
            }
            finally
            {
            }
            return Ok(myList);
        }
    }
    [Route("api/DashboardTmpCoreProductAAP")]
    public class DashboardTmpCoreProductAAP : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        public DashboardTmpCoreProductAAP(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult get()
        {
            string result = "";
            string result2 = "";
            string result3 = "";
            JArray arr1;
            JObject obj1;
            JObject obj2;
            List<JObject> myList = new List<JObject>();
            try
            {
                sb.AppendFormat("select * from tmpDashboardAAP1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr1 = JArray.Parse(result);
                for (int i = 0; i < arr1.Count; i++)
                {
                    myList.Add(
                        JObject.Parse(
                            "{" +
                            "\"productName\":\"" + arr1[i]["productName"].ToString() + "\"," +
                            "\"TotalCourseEvaCoreProduct\":\"" + arr1[i]["TotalCourseEvaCoreProduct"].ToString() + "\"" +
                            "}"
                        )
                    );
                }
                sb = new StringBuilder();
                sb.AppendFormat("select * from tmpDashboardAAP3");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result2 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                obj1 = JObject.Parse(result2);
                int TotalCourseEvaAllProduct = Int32.Parse(obj1["TotalCourseEvaCoreProduct"].ToString());
                sb = new StringBuilder();
                sb.AppendFormat("select * from tmpDashboardAAP2");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result3 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                obj2 = JObject.Parse(result3);
                int TotalCourseEvaCoreProduct = Int32.Parse(obj2["TotalCourseEvaCoreProduct"].ToString());
                int notcoreproduct = TotalCourseEvaAllProduct - TotalCourseEvaCoreProduct;
                myList.Add(
                    JObject.Parse(
                        "{" +
                        "\"productName\":\"Not Core Product\"," +
                        "\"TotalCourseEvaCoreProduct\":\"" + notcoreproduct.ToString() + "\"" +
                        "}"
                    )
                );
            }
            catch
            {
                myList = null;
            }
            finally
            {
            }
            return Ok(myList);
        }
    }
    [Route("api/DashboardDistributor")]
    public class DashboardDistributor : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        public DashboardDistributor(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpPost("CoreProductATC")]
        public IActionResult CoreProductATC([FromBody] dynamic data)
        {
            string result = "";
            try
            {
                string query =
                    "select p.productName, COUNT(p.productId) TotalCourseEvaCoreProduct from CourseSoftware cs " +
                    "INNER JOIN Products p " +
                    "ON cs.productId = p.productId " +
                    "INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "INNER JOIN " +
                    "(select o.OrgId, s.SiteId, sd.CountryCode from Main_organization o " +
                    "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                    "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                    "WHERE o.OrgId in (" + data.OrgId + ") /*and s.Status = 'A'*/) ds " +
                    "ON s.SiteCountryCode = ds.CountryCode " +
                    "where c.PartnerType like '%ATC%' and s.`Status` != 'X' and (p.productName LIKE '%Revit%' OR p.productName LIKE '%BIM%' OR p.productId IN ( 3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33 ) ) " +
                    "group by p.productname " +
                    "union " +
                    // "select 'All Product', COUNT(*) TotalCourseEvaCoreProduct from CourseSoftware cs " +
                    // "INNER JOIN Products p " +
                    // "ON cs.productId = p.productId " +
                    // "INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    // "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    // "INNER JOIN " +
                    // "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                    // "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                    // "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                    // "WHERE o.OrgId in (" + data.OrgId + ") /*and s.Status = 'A'*/) ds " +
                    // "ON s.SiteCountryCode = ds.CountryCode " +
                    // "where c.PartnerType like '%ATC%' and s.`Status` != 'X' " +
                    // "union " +
                    "select 'All non core Product',COUNT(p.productId) TotalCourseEvaCoreProduct from CourseSoftware cs " +
                    "INNER JOIN Products p " +
                    "ON cs.productId = p.productId " +
                    "INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "INNER JOIN " +
                    "(select o.OrgId, s.SiteId, sd.CountryCode from Main_organization o " +
                    "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                    "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                    "WHERE o.OrgId in (" + data.OrgId + ") /*and s.Status = 'A'*/) ds " +
                    "ON s.SiteCountryCode = ds.CountryCode " +
                    "where c.PartnerType like '%ATC%' and s.`Status` != 'X' and p.productName NOT LIKE '%Revit%' and p.productName NOT LIKE '%BIM%' and p.productId not in (3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33)  ";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("CoreProductAAP")]
        public IActionResult CoreProductAAP([FromBody] dynamic data)
        {
            string result = "";
            try
            {
                string query =
                    "select p.productName, COUNT(p.productId) TotalCourseEvaCoreProduct from CourseSoftware cs " +
                    "INNER JOIN Products p " +
                    "ON cs.productId = p.productId " +
                    "INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "INNER JOIN " +
                    "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                    "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                    "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                    "WHERE o.OrgId in (" + data.OrgId + ") /*and s.Status = 'A'*/) ds " +
                    "ON s.SiteCountryCode = ds.CountryCode " +
                    "where c.PartnerType like '%AAP%' and s.`Status` != 'X' and (p.productName LIKE '%Revit%' OR p.productName LIKE '%BIM%' OR p.productId IN ( 3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33 ) ) " +
                    "group by p.productname " +
                    "union " +
                    // "select 'All Product', COUNT(*) TotalCourseEvaCoreProduct from CourseSoftware cs " +
                    // "INNER JOIN Products p " +
                    // "ON cs.productId = p.productId " +
                    // "INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    // "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    // "INNER JOIN " +
                    // "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                    // "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                    // "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                    // "WHERE o.OrgId in (" + data.OrgId + ") /*and s.Status = 'A'*/) ds " +
                    // "ON s.SiteCountryCode = ds.CountryCode " +
                    // "where c.PartnerType like '%AAP%' and s.`Status` != 'X' " +
                    // "union " +
                    "select 'All non core Product',COUNT(p.productId) TotalCourseEvaCoreProduct from CourseSoftware cs " +
                    "INNER JOIN Products p " +
                    "ON cs.productId = p.productId " +
                    "INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "INNER JOIN " +
                    "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                    "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                    "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                    "WHERE o.OrgId in (" + data.OrgId + ") /*and s.Status = 'A'*/) ds " +
                    "ON s.SiteCountryCode = ds.CountryCode " +
                    "where c.PartnerType like '%AAP%' and s.`Status` != 'X' and p.productName NOT LIKE '%Revit%' and p.productName NOT LIKE '%BIM%' and p.productId not in (3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33) ";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        //Dibuat tanggal 11/08/2018
        //Untuk dashboard trainer Evaluation Survey Taken By Core Product (Sori dimasukin ke routenya dashboard distributor biar cepet)
        [HttpPost("EvaTakenByCoreProduct")]
        public IActionResult EvaluationSurveyTaken([FromBody] dynamic data)
        {
            string result = "";
            try
            {
                string query =

                    // "SELECT COUNT(EvaluationAnswerId) AS TotalSurveyTaken, p.productName FROM EvaluationAnswer ea " +
                    // "INNER JOIN Courses c ON ea.CourseId = c.CourseId " +
                    // "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId " +
                    // "INNER JOIN Products p ON cs.productId = p.productId " +
                    // "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    // "WHERE s.`Status` != 'X' AND (ea.CreatedDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE()) AND p.productId IN (3,28,39,152,136,1,33) AND s.OrgId IN (" + data.OrgId + ") group by p.productname " +
                    // "UNION " +
                    // "SELECT COUNT(EvaluationAnswerId) AS TotalSurveyTaken, 'All non core Product' FROM EvaluationAnswer ea " +
                    // "INNER JOIN Courses c ON ea.CourseId = c.CourseId " +
                    // "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId " +
                    // "INNER JOIN Products p ON cs.productId = p.productId " +
                    // "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    // "WHERE s.`Status` != 'X' AND (ea.CreatedDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE()) AND p.productId NOT IN (3,28,39,152,136,1,33) AND s.OrgId IN (" + data.OrgId + ")";
                    /* issue 01112018 - product revit not showing on dashboard eval taken by core product */

                    "SELECT COUNT(EvaluationAnswerId) AS TotalSurveyTaken, p.productName FROM EvaluationAnswer ea " +
                    "INNER JOIN Courses c ON ea.CourseId = c.CourseId " +
                    "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId " +
                    "INNER JOIN Products p ON cs.productId = p.productId OR cs.productsecondaryId = p.ProductId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "WHERE s.`Status` != 'X' AND (ea.CreatedDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE() + INTERVAL 1 DAY ) AND (p.productName LIKE '%Revit%' OR p.productName LIKE '%BIM%' OR p.productId IN ( 3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33 ) ) AND s.OrgId IN (" + data.OrgId + ") group by p.productname " +
                    "UNION " +
                    "SELECT COUNT(EvaluationAnswerId) AS TotalSurveyTaken, 'All non core Product' FROM EvaluationAnswer ea " +
                    "INNER JOIN Courses c ON ea.CourseId = c.CourseId " +
                    "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId " +
                    "INNER JOIN Products p ON cs.productId = p.productId OR cs.productsecondaryId = p.ProductId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "WHERE s.`Status` != 'X' AND (ea.CreatedDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE() + INTERVAL 1 DAY ) AND p.productName NOT LIKE '%Revit%' and p.productName NOT LIKE '%BIM%' and p.productId not in (3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33) AND s.OrgId IN (" + data.OrgId + ")";
                /* end line issue 01112018 - product revit not showing on dashboard eval taken by core product */
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("SitesParticipatedATC")]
        public IActionResult SitesParticipatedATC([FromBody] dynamic data)
        {
            string result = "";
            try
            {

                // string query =
                //"SELECT 'Participate' AS Site, COUNT( e.SiteId ) AS TotalSitesEval " +
                //"FROM ( SELECT DISTINCT SiteId, RoleId FROM EvaluationReport WHERE RoleId = '1' ) AS e " +
                //"INNER JOIN Main_site s ON e.SiteId = s.SiteId " +
                //"WHERE s.`Status` = 'A' " +
                //"AND s.SiteCountryCode collate utf8_general_ci IN ( SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId collate utf8_general_ci = s.SiteId WHERE o.OrgId in (" + data.OrgId + ") ) " +
                //"union " +
                //"select 'NonParticipate' AS Site, " +
                //"(SELECT COUNT(*) as TotalSite FROM Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId WHERE RoleId = '1' and s.`Status` = 'A' AND s.SiteCountryCode collate utf8_general_ci IN ( SELECT sd.CountryCode FROM Main_organization o    INNER JOIN Main_site s ON o.OrgID = s.OrgID    INNER JOIN SiteCountryDistributor sd ON sd.SiteId collate utf8_general_ci= s.SiteId     WHERE    o.OrgId in (" + data.OrgId + ") )) - " +
                //"(SELECT COUNT( e.SiteId ) AS TotalSitesEval " +
                //"FROM ( SELECT DISTINCT SiteId, RoleId FROM EvaluationReport WHERE RoleId = '1' ) AS e " +
                //"INNER JOIN Main_site s ON e.SiteId = s.SiteId " +
                //"WHERE s.`Status` = 'A' " +
                //"AND s.SiteCountryCode collate utf8_general_ci IN ( SELECT sd.CountryCode FROM Main_organization o    INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId collate utf8_general_ci= s.SiteId     WHERE    o.OrgId in (" + data.OrgId + ") )  ) as SiteNotParticipate from Main_site GROUP BY Site";

                string query = "SELECT Territory_Name,COUNT(ss.SiteId) AS TotalSitesEval FROM " +
                    "(SELECT DISTINCT e.SiteId, group_concat(DISTINCT e.RoleId ORDER BY e.RoleId) as 'PartnerType', s.SiteCountryCode " +
                    "FROM EvaluationReport e INNER JOIN Main_site s ON e.SiteId = s.SiteId " +
                    "WHERE s.`Status` = 'A' AND s.SiteCountryCode COLLATE utf8_general_ci IN(SELECT sd.CountryCode FROM Main_organization o INNER JOIN  Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId COLLATE utf8_general_ci = s.SiteId " +
                    "WHERE o.OrgId IN (" + data.OrgId + ")) group by s.SiteId) ss " +
                    "INNER JOIN Ref_countries rc ON ss.SiteCountryCode = rc.countries_code " +
                    "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                    "WHERE (PartnerType = '1' or PartnerType = '1,58') "+
                    "GROUP BY rt.TerritoryId order by rt.Territory_Name; ";
               
                string query2= "SELECT Territory_Name, COUNT(ss.SiteId) AS TotalSite FROM "+
                    "(select s.SiteId, group_concat(DISTINCT sr.RoleId ORDER BY sr.RoleId) as 'PartnerType', sr.Status, s.SiteCountryCode from "+
                    "Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId "+
                    "WHERE s.`Status` = 'A' AND s.SiteCountryCode COLLATE utf8_general_ci IN(SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId COLLATE utf8_general_ci = s.SiteId "+
                    "WHERE  o.OrgId IN (" + data.OrgId + ")) group by s.SiteId) ss " +
                    "INNER JOIN Ref_countries rc ON ss.SiteCountryCode = rc.countries_code " +
                    "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                    "WHERE (PartnerType = '1' or PartnerType = '1,58') " +
                    "GROUP BY rt.TerritoryId order by rt.Territory_Name; ";
               
                DataTable dt1 = new DataTable();
                DataTable dt = new DataTable();
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    dt.PrimaryKey = new DataColumn[] { dt.Columns["Territory_Name"] };
                }

                sb = new StringBuilder();
                sb.AppendFormat(query2);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dt1 = ds.Tables[0];
                    dt1.PrimaryKey = new DataColumn[] { dt1.Columns["Territory_Name"] };
                }
                dt1.Merge(dt);
                DataTable dtTmp = dt1.Copy();
                dtTmp.Columns.Add("NonCount", typeof(System.Int32));
                DataColumnCollection tmpcolumns = dtTmp.Columns;
                foreach (DataRow row in dtTmp.Rows)
                {
                    if (!tmpcolumns.Contains("TotalSite"))
                    {
                        dtTmp.Columns.Add("TotalSite", typeof(System.Int32));
                        row["TotalSite"] = 0;
                    }
                    if (!tmpcolumns.Contains("TotalSitesEval"))
                    {
                        dtTmp.Columns.Add("TotalSitesEval", typeof(System.Int32));
                        row["TotalSitesEval"] = 0;
                    }
                    int siteCount = 0;
                    int EvaluationCount = 0;
                    if (!string.IsNullOrEmpty(row["TotalSite"].ToString()))
                    {
                        siteCount = Convert.ToInt32(row["TotalSite"].ToString());
                    }
                    else
                    {
                        row["TotalSite"] = 0;
                    }
                    if (!string.IsNullOrEmpty(row["TotalSitesEval"].ToString()))
                    {
                        EvaluationCount = Convert.ToInt32(row["TotalSitesEval"].ToString());
                    }
                    else
                    {
                        row["TotalSitesEval"] = 0;
                    }
                    if (siteCount > EvaluationCount)
                    {
                        int NnonCount = siteCount - EvaluationCount;
                        row["NonCount"] = NnonCount;
                    }
                    else
                    {
                        row["NonCount"] = 0;

                    }

                }
                for (int j = 0; j < dtTmp.Rows.Count; j++)
                {
                    if (dtTmp.Rows[j]["TotalSitesEval"].ToString() == "0")
                    {
                        dtTmp.Rows[j].Delete();
                        dtTmp.AcceptChanges();
                    }
                }
                result = MySqlDb.GetJSONArrayString(dtTmp);
            }
            catch(Exception ex)
            {
                result = null;
            }
            finally
            {
                
            }
            return Ok(result);
        }
        [HttpPost("SitesParticipatedAAP")]
        public IActionResult SitesParticipatedAAP([FromBody] dynamic data)
        {

            string result = "";
            try
            {

                //string query =
                //    "SELECT 'Participate' AS Site, COUNT( e.SiteId ) AS TotalSitesEval " +
                //    "FROM ( SELECT DISTINCT SiteId, RoleId FROM EvaluationReport WHERE RoleId = '58' ) AS e " +
                //    "INNER JOIN Main_site s ON e.SiteId = s.SiteId " +
                //    "WHERE s.`Status` = 'A' " +
                //    "AND s.SiteCountryCode collate utf8_general_ci IN ( SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId collate utf8_general_ci= s.SiteId WHERE o.OrgId in (" + data.OrgId + ") ) " +
                //    "union " +
                //    "select 'NonParticipate' AS Site, " +
                //    "(SELECT COUNT(*) as TotalSite FROM Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId WHERE RoleId = '58' and s.`Status` = 'A' AND s.SiteCountryCode collate utf8_general_ci IN ( SELECT sd.CountryCode FROM Main_organization o    INNER JOIN Main_site s ON o.OrgID = s.OrgID    INNER JOIN SiteCountryDistributor sd ON sd.SiteId collate utf8_general_ci= s.SiteId     WHERE    o.OrgId in (" + data.OrgId + ") )) - " +
                //    "(SELECT COUNT( e.SiteId ) AS TotalSitesEval " +
                //    "FROM ( SELECT DISTINCT SiteId, RoleId FROM EvaluationReport WHERE RoleId = '58' ) AS e " +
                //    "INNER JOIN Main_site s ON e.SiteId = s.SiteId " +
                //    "WHERE s.`Status` = 'A' " +
                //    "AND s.SiteCountryCode collate utf8_general_ci IN ( SELECT sd.CountryCode FROM Main_organization o    INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId collate utf8_general_ci= s.SiteId     WHERE    o.OrgId in (" + data.OrgId + ") )  ) as SiteNotParticipate from Main_site GROUP BY Site";

                string query = "SELECT Territory_Name,COUNT(ss.SiteId) AS TotalSitesEval FROM " +
                    "(SELECT DISTINCT e.SiteId, group_concat(DISTINCT e.RoleId ORDER BY e.RoleId) as 'PartnerType', s.SiteCountryCode " +
                    "FROM EvaluationReport e INNER JOIN Main_site s ON e.SiteId = s.SiteId " +
                    "WHERE s.`Status` = 'A' AND s.SiteCountryCode COLLATE utf8_general_ci IN(SELECT sd.CountryCode FROM Main_organization o INNER JOIN  Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId COLLATE utf8_general_ci = s.SiteId " +
                    "WHERE o.OrgId IN (" + data.OrgId + ")) group by s.SiteId) ss " +
                    "INNER JOIN Ref_countries rc ON ss.SiteCountryCode = rc.countries_code " +
                    "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                    "WHERE (PartnerType = '58') " +
                    "GROUP BY rt.TerritoryId order by rt.Territory_Name; ";

                string query2 = "SELECT Territory_Name, COUNT(ss.SiteId) AS TotalSite FROM " +
                    "(select s.SiteId, group_concat(DISTINCT sr.RoleId ORDER BY sr.RoleId) as 'PartnerType', sr.Status, s.SiteCountryCode from " +
                    "Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId " +
                    "WHERE s.`Status` = 'A' AND s.SiteCountryCode COLLATE utf8_general_ci IN(SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId COLLATE utf8_general_ci = s.SiteId " +
                    "WHERE  o.OrgId IN (" + data.OrgId + ")) group by s.SiteId) ss " +
                    "INNER JOIN Ref_countries rc ON ss.SiteCountryCode = rc.countries_code " +
                    "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                    "WHERE (PartnerType = '58') " +
                    "GROUP BY rt.TerritoryId order by rt.Territory_Name; ";

                DataTable dt1 = new DataTable();
                DataTable dt = new DataTable();
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dt = ds.Tables[0];
                    dt.PrimaryKey = new DataColumn[] { dt.Columns["Territory_Name"] };
                }

                sb = new StringBuilder();
                sb.AppendFormat(query2);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dt1 = ds.Tables[0];
                    dt1.PrimaryKey = new DataColumn[] { dt1.Columns["Territory_Name"] };
                }
                dt1.Merge(dt);
                DataTable dtTmp = dt1.Copy();
                dtTmp.Columns.Add("NonCount", typeof(System.Int32));
                DataColumnCollection tmpcolumns = dtTmp.Columns;
                foreach (DataRow row in dtTmp.Rows)
                {
                    if (!tmpcolumns.Contains("TotalSite"))
                    {
                        dtTmp.Columns.Add("TotalSite", typeof(System.Int32));
                        row["TotalSite"] = 0;
                    }
                    if (!tmpcolumns.Contains("TotalSitesEval"))
                    {
                        dtTmp.Columns.Add("TotalSitesEval", typeof(System.Int32));
                        row["TotalSitesEval"] = 0;
                    }
                    int siteCount = 0;
                    int EvaluationCount = 0;
                    if (!string.IsNullOrEmpty(row["TotalSite"].ToString()))
                    {
                        siteCount = Convert.ToInt32(row["TotalSite"].ToString());
                    }
                    else
                    {
                        row["TotalSite"] = 0;
                    }
                    if (!string.IsNullOrEmpty(row["TotalSitesEval"].ToString()))
                    {
                        EvaluationCount = Convert.ToInt32(row["TotalSitesEval"].ToString());
                    }
                    else
                    {
                        row["TotalSitesEval"] = 0;
                    }
                    if (siteCount > EvaluationCount)
                    {
                        int NnonCount = siteCount - EvaluationCount;
                        row["NonCount"] = NnonCount;
                    }
                    else
                    {
                        row["NonCount"] = 0;

                    }

                }
                for (int j = 0; j < dtTmp.Rows.Count; j++)
                {
                    if (dtTmp.Rows[j]["TotalSitesEval"].ToString() == "0")
                    {
                        dtTmp.Rows[j].Delete();
                        dtTmp.AcceptChanges();
                    }
                }
                result = MySqlDb.GetJSONArrayString(dtTmp);
            }
            
            catch
            {
                result = null;
            }
            finally
            {
                
            }
            return Ok(result);
        }
        [HttpPost("CourseDeliveredByPartnerMonth")]
        public IActionResult CourseDeliveredByPartnerMonth([FromBody] dynamic data)
        {
            string result = "";
            try
            {
                string query =
                    "Select COUNT(CourseId) as course, PartnerType from Courses c " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "INNER JOIN " +
                    "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                    "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                    "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                    "WHERE s.SiteCountryCode IN ( SELECT sd.CountryCode FROM Main_organization o    INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId in (" + data.OrgId + ") ) ) ds " +
                    "ON s.SiteCountryCode = ds.CountryCode  " +
                    "where  MONTH ( c.CompletionDate ) = MONTH ( CURRENT_DATE () ) AND YEAR ( c.CompletionDate ) = YEAR ( CURRENT_DATE()) " +
                    "group by PartnerType ";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("CourseDeliveredByPartner")]
        public IActionResult CourseDeliveredByPartner([FromBody] dynamic data)
        {
            string result = "";
            try
            {
                string query =
                    "Select COUNT(CourseId) as course, PartnerType from Courses c " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                    "INNER JOIN " +
                    "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                    "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                    "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                    "WHERE s.SiteCountryCode IN ( SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId in (" + data.OrgId + ") ) ) ds " +
                    "ON s.SiteCountryCode = ds.CountryCode " +
                    "WHERE (c.StartDate >= CONCAT(YEAR(CURDATE()),'-02-01') AND c.StartDate <= CURDATE()) AND s.`Status` = 'A' " +
                    "group by PartnerType ";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("TotalEvaSurvey")]
        public IActionResult TotalEvaSurvey([FromBody] dynamic data)
        {
            string result = "";
            try
            {
                // string query =
                //     "Select COUNT(ea.EvaluationAnswerId) as TotalSurveyTaken, PartnerType from Courses c " +
                //     "INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId " +
                //     "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
                //     "INNER JOIN (select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                //     "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                //     "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId ";
                // if (data.OrgId != "")
                // {
                //     query += "WHERE o.OrgId in (" + data.OrgId + ")";
                // }
                // query +=
                //     ") ds " +
                //     "ON s.SiteCountryCode = ds.CountryCode " +
                //     "group by PartnerType";
                string query =
                    // "SELECT SUM(Total) AS TotalSurveyTaken,PartnerType FROM Main_site s "+
                    // "INNER JOIN (select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId ";
                    "SELECT COUNT(EvaluationAnswerId) AS TotalSurveyTaken, c.PartnerType FROM Main_site s " +
                    "INNER JOIN (select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd on sd.SiteId collate utf8_general_ci= s.SiteId ";

                if (data.OrgId != "")
                {
                    query += "WHERE o.OrgId in (" + data.OrgId + ") ";
                }
                query +=
                    ") ds " +
                    "ON s.SiteCountryCode = ds.CountryCode collate utf8_general_ci " +
                    "INNER JOIN Courses c ON s.SiteId = c.SiteId " +
                    "INNER JOIN EvaluationAnswer ea ON c.CourseId = ea.CourseId " +
                    "WHERE (c.StartDate >= CONCAT(YEAR(CURDATE()),'-02-01') AND c.CompletionDate <= CURDATE()) AND s.`Status` = 'A' GROUP BY c.PartnerType";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpGet("ActivePartner/{obj}")]
        public IActionResult ActivePartner(string obj)
        {
            JArray listterritory;
            JArray listcountpartner;
            string roleid = "";
            string result = "";
            var o1 = JObject.Parse(obj);
            JObject json = JObject.FromObject(o1);
            try
            {
                string orgid = "";
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            orgid = property.Value.ToString();
                        }
                    }
                    if (property.Name.Equals("RoleId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            roleid = property.Value.ToString();
                        }
                    }
                }
                string query =
                    // "SELECT r.RoleCode, t.Territory_Name, Count( * ) AS CountPartners, CASE WHEN sr.RoleId = '60' THEN '#1AB244' WHEN sr.RoleId = '58' THEN '#DEBB27' ELSE '#AD1D28' END AS BackgroundColor " +
                    // "FROM Main_site s " +
                    // "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId " +
                    // "INNER JOIN " +
                    // "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                    // "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                    // "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                    // "WHERE o.OrgId in (" + orgid + ") )  ds " +
                    // "ON s.SiteCountryCode = ds.CountryCode " +
                    // "INNER JOIN Ref_countries AS tc ON tc.countries_code = ds.CountryCode " +
                    // "INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId " +
                    // "INNER JOIN Roles r ON sr.RoleId = r.RoleId AND r.RoleId = '" + roleid + "' " +
                    // "GROUP BY t.Territory_Name asc ";
                    //Ini sy tutup karena dari email selina issu hari kamis sore, dia bilang kalau taiwan atc tahun ini totalnya sekitar 112 dari org OP701166
                    //email sy forward ke aek
                    // "SELECT r.RoleCode, t.Territory_Name, Count( sr.SiteId ) AS CountPartners," +
                    // "CASE WHEN sr.RoleId = 60 THEN '#1AB244' WHEN sr.RoleId = 58 THEN '#DEBB27' ELSE '#AD1D28' END AS BackgroundColor FROM Main_site s " +
                    // "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId " +
                    // "INNER JOIN (select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId WHERE o.OrgId in (" + orgid + ") )  ds " +
                    // "ON s.SiteCountryCode = ds.CountryCode " +
                    // "INNER JOIN Ref_countries AS tc ON tc.countries_code = ds.CountryCode " +
                    // "INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId " +
                    // "INNER JOIN Roles r ON sr.RoleId = r.RoleId " +
                    // "WHERE sr.RoleId = " + roleid + " AND s.DateAdded BETWEEN CONCAT(YEAR(CURDATE()),'-02-01') AND CURDATE() AND s.`Status` = 'A' " +
                    // "GROUP BY t.Territory_Name asc";
                    // 

                    //query dari pak nakarin 04-08-2018
                    "SELECT r.RoleCode, t.Territory_Name, Count( * ) AS CountPartners," +
                    "CASE WHEN sr.RoleId = '60' THEN '#1AB244' WHEN sr.RoleId = '58' THEN '#DEBB27' ELSE '#AD1D28' END AS BackgroundColor FROM Main_site s " +
                    "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId " +
                    "INNER JOIN (" +
                    "select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd on " +
                    "sd.SiteId = s.SiteId WHERE o.OrgId in (" + orgid + ") )  ds " +
                    "ON s.SiteCountryCode = ds.CountryCode " +
                    "INNER JOIN Ref_countries AS tc ON tc.countries_code = ds.CountryCode " +
                    "INNER JOIN Ref_territory AS t ON t.TerritoryId = tc.TerritoryId " +
                    "INNER JOIN Roles r ON sr.RoleId = r.RoleId " +
                    "WHERE sr.RoleId = '" + roleid + "'  AND s.`Status` = 'A' and sr.Status ='A' " +
                    "GROUP BY t.Territory_Name asc";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                result = "";
            }
            finally
            {
            }
            return Ok(result);
        }
        [HttpPost("AnualReq")]
        public IActionResult AnualReq([FromBody] dynamic data)
        {
            JArray listterritory;
            JArray listcountpartner;
            string mom = "";
            string org_jek = "";
            string result = "";
            string[] tmp = null;
            DataTable dtTotal = new DataTable();
            DataTable dtMeet = new DataTable();
            DataTable dtNotMeet = new DataTable();
            DataTable dtTmp = new DataTable();
            JObject json = JObject.FromObject(data);
            sb = new StringBuilder();
            try
            {
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            org_jek = property.Value.ToString();
                        }
                    }
                    if (property.Name.Equals("MeetNotMeet"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            mom = property.Value.ToString();
                            tmp = mom.Split(',');
                        }
                    }
                }
                string tmpCOE = "";
                string query = "";
                //string label = "";
                //string color = "";
                //if (mom == ">=")
                //{
                //    label = "Meet";
                //    color = "#669911";
                //}
                //else
                //{
                //    label = "Not Meet";
                //    color = "#5FBEAA";
                //}
                if (!string.IsNullOrEmpty(org_jek))
                {
                    org_jek = "WHERE o.OrgId IN(" + org_jek + ")";
                }
                
                foreach (string w in tmp)
                {
                    if (w == ">=") tmpCOE = "MeetCount";
                    else if (w == "<") tmpCOE = "NotMeetCount";
                    else if (w == "0") tmpCOE = "TotalCount";
                    query = " SELECT Territory_Name, COUNT(COE) AS " + tmpCOE + " FROM " +
                           "(SELECT coe.SiteId, CountryCode, COE, PartnerType FROM " +
                           "(SELECT s.SiteId, CountryCode, group_concat(DISTINCT sr.RoleId ORDER BY sr.RoleId) as 'PartnerType' FROM " +
                           "(SELECT sd.CountryCode FROM  Main_organization o " +
                           "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                           "INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId " + org_jek + ") AS sd " +
                           "INNER JOIN Main_site s ON sd.CountryCode = s.SiteCountryCode " +
                           "INNER JOIN SiteRoles sr on s.SiteId = sr.SiteId where s.Status = 'A' " +
                           "GROUP BY s.SiteId) AS s " +
                           "INNER JOIN tmpSitesCOE coe ON s.SiteId = coe.SiteId AND ";
                    string tmpquery= "INNER JOIN Ref_countries rc ON coe.CountryCode = rc.countries_code " +
                            "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                            "where (PartnerType ='1' or PartnerType = '1,58') " +
                            "GROUP BY rt.TerritoryId order by rt.Territory_Name "; 
                           //" COE >= 85) AS coe " +
                           if (w == ">=")
                    {
                        query += " COE >= 85) AS coe " + tmpquery;

                        sb.Clear();
                        sb.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        DataSet dsMeet = oDb.getDataSetFromSP(sqlCommand);
                        if (dsMeet.Tables[0].Rows.Count != 0)
                        {
                            dtMeet = dsMeet.Tables[0];
                            dtMeet.PrimaryKey = new DataColumn[] { dtMeet.Columns["Territory_Name"] };
                        }

                    }
                    else if (w == "<")
                    {
                        query += "COE < 85) AS coe " + tmpquery;
                        sb.Clear();
                        sb.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        DataSet dsNotMeet = oDb.getDataSetFromSP(sqlCommand);
                        if (dsNotMeet.Tables[0].Rows.Count != 0)
                        {
                            dtNotMeet = dsNotMeet.Tables[0];
                            dtNotMeet.PrimaryKey = new DataColumn[] { dtNotMeet.Columns["Territory_Name"] };
                        }
                    }
                    else if (w == "0")
                    {
                        query += "COE <= 100 and COE >=0) AS coe " + tmpquery;
                        sb.Clear();
                        sb.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        DataSet dsTotal = oDb.getDataSetFromSP(sqlCommand);
                        if (dsTotal.Tables[0].Rows.Count != 0)
                        {
                            dtTotal = dsTotal.Tables[0];
                            dtTotal.PrimaryKey = new DataColumn[] { dtTotal.Columns["Territory_Name"] };
                        }
                    }
                }

                dtMeet.Merge(dtNotMeet);
                dtMeet.Merge(dtTotal);
                dtTmp = dtMeet.Copy();
                dtTmp.Columns.Add("perSiteMeet", typeof(System.Int32));
                dtTmp.Columns.Add("perSiteNotMeet", typeof(System.Int32));
                DataColumnCollection tmpcolumns = dtTmp.Columns;
                foreach (DataRow row in dtTmp.Rows)
                {
                    if (!tmpcolumns.Contains("TotalCount"))
                    {
                        dtTmp.Columns.Add("TotalCount", typeof(System.Int32));
                        row["TotalCount"] = 0;
                    }
                    if (!tmpcolumns.Contains("MeetCount"))
                    {
                        dtTmp.Columns.Add("MeetCount", typeof(System.Int32));
                        row["MeetCount"] = 0;
                    }
                    if (!tmpcolumns.Contains("NotMeetCount"))
                    {
                        dtTmp.Columns.Add("NotMeetCount", typeof(System.Int32));
                        row["NotMeetCount"] = 0;
                    }
                    int MeetCount = 0;
                    int NotMeetCount = 0;
                    int total = Convert.ToInt32(row["TotalCount"].ToString());
                    if (!string.IsNullOrEmpty(row["MeetCount"].ToString()))
                    {
                        MeetCount = Convert.ToInt32(row["MeetCount"].ToString());
                    }
                    if (!string.IsNullOrEmpty(row["NotMeetCount"].ToString()))
                    {
                        NotMeetCount = Convert.ToInt32(row["NotMeetCount"].ToString());
                    }

                    if (total != 0)
                    {
                        int tmpMeet = (int)Math.Round((double)(100 * MeetCount) / total);
                        int tmpNotMeet = (int)Math.Round((double)(100 * NotMeetCount) / total);
                        row["perSiteMeet"] = tmpMeet;
                        row["perSiteNotMeet"] = tmpNotMeet;
                    }
                    else
                    {
                        row["perSiteMeet"] = 0;
                        row["perSiteNotMeet"] = 0;
                    }

                }
                // string query = "";
                // if (org_jek != "")
                // {
                //     query =
                //         "SELECT s.SiteId, '" + label + "' Label, Territory_Name, COUNT(COE) as Count, '" + color + "' BackgroundColor FROM " +
                //         "(SELECT s.SiteId, SiteCountryCode," +
                //         "(SELECT " +
                //         "CAST(" +
                //         "CASE WHEN COUNT(StudentID) = 0 then 0 else " +
                //         "(" +
                //             "CASE WHEN COUNT(DISTINCT StudentID) > 99 THEN (SUM(CASE WHEN ScoreOE IN ('75','100') THEN 1 ELSE 0 END) / CAST(COUNT(DISTINCT StudentID) as DECIMAL(18,6))) * 100 ELSE 0 END " +
                //             "- (SQRT(CAST((" +
                //             "CASE WHEN COUNT(DISTINCT StudentID) > 99 THEN (SUM(CASE WHEN ScoreOE IN ('75','100') THEN 1 ELSE 0 END) / CAST(COUNT(DISTINCT StudentID) as DECIMAL(18,6))) * 100 ELSE 0 END " +
                //             "* (100 -" +
                //             "CASE WHEN COUNT(DISTINCT StudentID) > 99 THEN (SUM(CASE WHEN ScoreOE IN ('75','100') THEN 1 ELSE 0 END) / CAST(COUNT(DISTINCT StudentID) as DECIMAL(18,6))) * 100 ELSE 0 END " +
                //             ")) / COUNT(DISTINCT StudentID) as decimal(18,10)))  * 2.56)) " +
                //         "end AS DECIMAL ( 7, 2 )) FROM EvaluationReport er WHERE er.SiteId = s.SiteId) COE " +
                //         "FROM Main_site s " +
                //         "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId and sr.`Status` = 'A' " +
                //         "INNER JOIN " +
                //         "(select o.OrgId, s.SiteID, sd.CountryCode from Main_organization o " +
                //         "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                //         "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                //         "WHERE o.OrgId IN (" + org_jek + "))  ds " +
                //         "ON s.SiteCountryCode = ds.CountryCode) AS s " +
                //         "INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " +
                //         "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                //         "WHERE s.COE " + mom + " 85 GROUP BY Territory_Name ASC ";
                // }
                // else
                // {
                //     query = "select '" + label + "' Label, Territory_Name, COUNT(COE) as Count, '" + color + "' BackgroundColor from (select SiteCountryCode, CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END COE from (select CourseId, SiteCountryCode from (select SiteId, SiteCountryCode from Main_site where `Status` = 'A') as s " +
                //     "INNER JOIN Courses c ON s.SiteId = c.SiteId AND YEAR(c.StartDate) = YEAR(NOW())) as c INNER JOIN EvaluationReport er ON c.CourseId = er.CourseId GROUP BY c.CourseId) as er INNER JOIN Ref_countries rc ON er.SiteCountryCode = rc.countries_code INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                //     "WHERE COE " + mom + " 85 GROUP BY rt.TerritoryId";
                // }
                //tmp anual req
                //untuk ubah ke % ROUND(((COUNT(COE) / (SELECT COUNT(*) FROM tmpSitesCOE)) * 100),2) as Count
                //    if (org_jek != "")
                //{

                //query =
                //    "SELECT '" + label + "' Label, Territory_Name, COUNT(COE) as Count, '" + color + "' BackgroundColor from " +
                //    "(select coe.SiteId, CountryCode, COE from " +
                //    "(select SiteId, CountryCode from " +
                //    "(select sd.CountryCode from Main_organization o " +
                //    "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                //    "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                //    "WHERE o.OrgId IN (" + org_jek + ")) as sd " +
                //    "INNER JOIN Main_site s ON sd.CountryCode = s.SiteCountryCode) as s " +
                //    "INNER JOIN tmpSitesCOE coe ON s.SiteId = coe.SiteId AND COE " + mom + " 85) as coe " +
                //    "INNER JOIN Ref_countries rc ON coe.CountryCode = rc.countries_code " +
                //    "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId GROUP BY Territory_Name";


                //}
                //else
                //{
                //    query =
                //        "SELECT '" + label + "' Label, Territory_Name, COUNT(COE) as Count, '" + color + "' BackgroundColor from " +
                //        "(select coe.SiteId, CountryCode, COE from " +
                //        "(select SiteId, CountryCode from " +
                //        "(select sd.CountryCode from Main_organization o " +
                //        "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                //        "INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId " +
                //        ") as sd " +
                //        "INNER JOIN Main_site s ON sd.CountryCode = s.SiteCountryCode) as s " +
                //        "INNER JOIN tmpSitesCOE coe ON s.SiteId = coe.SiteId AND COE " + mom + " 85) as coe " +
                //        "INNER JOIN Ref_countries rc ON coe.CountryCode = rc.countries_code " +
                //        "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId GROUP BY Territory_Name";
                //}

                //sb.AppendFormat(query);
                //sqlCommand = new MySqlCommand(sb.ToString());
                //// Console.WriteLine(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);
                //result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            catch(Exception ex)
            {
                result = "";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(dtTmp);
            }
            return Ok(result);
        }

        [HttpPost("AnualReqAAP")]
        public IActionResult AnualReqAAP([FromBody] dynamic data)
        {

            string mom = "";
            string org_jek = "";
            string result = "";
            string[] tmp = null;
            DataTable dtTotal = new DataTable();
            DataTable dtMeet = new DataTable();
            DataTable dtNotMeet = new DataTable();
            DataTable dtTmp = new DataTable();
            JObject json = JObject.FromObject(data);
            sb = new StringBuilder();
            try
            {
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            org_jek = property.Value.ToString();
                        }
                    }
                    if (property.Name.Equals("MeetNotMeet"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            mom = property.Value.ToString();
                            tmp = mom.Split(',');
                        }
                    }
                }
                string tmpCOE = "";
                string query = "";

                if (!string.IsNullOrEmpty(org_jek))
                {
                    org_jek = "WHERE o.OrgId IN(" + org_jek + ")";
                }

                foreach (string w in tmp)
                {
                    if (w == ">=") tmpCOE = "MeetCount";
                    else if (w == "<") tmpCOE = "NotMeetCount";
                    else if (w == "0") tmpCOE = "TotalCount";
                    query = " SELECT Territory_Name, COUNT(COE) AS " + tmpCOE + " FROM " +
                           "(SELECT coe.SiteId, CountryCode, COE, PartnerType FROM " +
                           "(SELECT s.SiteId, CountryCode, group_concat(DISTINCT sr.RoleId ORDER BY sr.RoleId) as 'PartnerType' FROM " +
                           "(SELECT sd.CountryCode FROM  Main_organization o " +
                           "INNER JOIN Main_site s ON o.OrgID = s.OrgID " +
                           "INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId " + org_jek + ") AS sd " +
                           "INNER JOIN Main_site s ON sd.CountryCode = s.SiteCountryCode " +
                           "INNER JOIN SiteRoles sr on s.SiteId = sr.SiteId where s.Status = 'A' " +
                           "GROUP BY s.SiteId) AS s " +
                           "INNER JOIN tmpSitesCOE coe ON s.SiteId = coe.SiteId AND ";
                    string tmpquery = "INNER JOIN Ref_countries rc ON coe.CountryCode = rc.countries_code " +
                            "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                            "where (PartnerType ='58') " +
                            "GROUP BY rt.TerritoryId order by rt.Territory_Name ";
                    //" COE >= 85) AS coe " +
                    if (w == ">=")
                    {
                        query += " COE >= 85) AS coe " + tmpquery;

                        sb.Clear();
                        sb.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        DataSet dsMeet = oDb.getDataSetFromSP(sqlCommand);
                        if (dsMeet.Tables[0].Rows.Count != 0)
                        {
                            dtMeet = dsMeet.Tables[0];
                            dtMeet.PrimaryKey = new DataColumn[] { dtMeet.Columns["Territory_Name"] };
                        }

                    }
                    else if (w == "<")
                    {
                        query += "COE < 85) AS coe " + tmpquery;
                        sb.Clear();
                        sb.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        DataSet dsNotMeet = oDb.getDataSetFromSP(sqlCommand);
                        if (dsNotMeet.Tables[0].Rows.Count != 0)
                        {
                            dtNotMeet = dsNotMeet.Tables[0];
                            dtNotMeet.PrimaryKey = new DataColumn[] { dtNotMeet.Columns["Territory_Name"] };
                        }
                    }
                    else if (w == "0")
                    {
                        query += "COE <= 100 and COE >=0) AS coe " + tmpquery;
                        sb.Clear();
                        sb.AppendFormat(query);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        DataSet dsTotal = oDb.getDataSetFromSP(sqlCommand);
                        if (dsTotal.Tables[0].Rows.Count != 0)
                        {
                            dtTotal = dsTotal.Tables[0];
                            dtTotal.PrimaryKey = new DataColumn[] { dtTotal.Columns["Territory_Name"] };
                        }
                    }
                }

                dtMeet.Merge(dtNotMeet);
                dtMeet.Merge(dtTotal);
                dtTmp = dtMeet.Copy();
                dtTmp.Columns.Add("perSiteMeet", typeof(System.Int32));
                dtTmp.Columns.Add("perSiteNotMeet", typeof(System.Int32));
                DataColumnCollection tmpcolumns = dtTmp.Columns;
                foreach (DataRow row in dtTmp.Rows)
                {
                    if (!tmpcolumns.Contains("TotalCount"))
                    {
                        dtTmp.Columns.Add("TotalCount", typeof(System.Int32));
                        row["TotalCount"] = 0;
                    }
                    if (!tmpcolumns.Contains("MeetCount"))
                    {
                        dtTmp.Columns.Add("MeetCount", typeof(System.Int32));
                        row["MeetCount"] = 0;
                    }
                    if (!tmpcolumns.Contains("NotMeetCount"))
                    {
                        dtTmp.Columns.Add("NotMeetCount", typeof(System.Int32));
                        row["NotMeetCount"] = 0;
                    }
                    int MeetCount = 0;
                    int NotMeetCount = 0;
                    int total = 0;
                    if (!string.IsNullOrEmpty(row["TotalCount"].ToString()))
                    {
                        MeetCount = Convert.ToInt32(row["TotalCount"].ToString());
                    }
                    else { row["TotalCount"] = 0; }
                    if (!string.IsNullOrEmpty(row["MeetCount"].ToString()))
                    {
                        MeetCount = Convert.ToInt32(row["MeetCount"].ToString());
                    }
                    else { row["MeetCount"] = 0; }

                    if (!string.IsNullOrEmpty(row["NotMeetCount"].ToString()))
                    {
                        NotMeetCount = Convert.ToInt32(row["NotMeetCount"].ToString());
                    }
                    else { row["NotMeetCount"] = 0; }
                    if (total != 0)
                    {
                        int tmpMeet = (int)Math.Round((double)(100 * MeetCount) / total);
                        int tmpNotMeet = (int)Math.Round((double)(100 * NotMeetCount) / total);
                        row["perSiteMeet"] = tmpMeet;
                        row["perSiteNotMeet"] = tmpNotMeet;
                    }
                    else
                    {
                        row["perSiteMeet"] = 0;
                        row["perSiteNotMeet"] = 0;
                    }

                }
 
            }
            catch (Exception ex)
            {
                result = "";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(dtTmp);
            }
            return Ok(result);
        }
    }
    [Route("api/DashboardSiteAdmin")]
    public class DashboardSiteAdmin : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result = "";
        public DashboardSiteAdmin(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpPost("top10instructor")]
        public IActionResult top10instructor([FromBody] dynamic data)
        {
            try
            {
                string query = "select ca.ContactId,ca.ContactName, COUNT(c.ContactId) as TotalCourseDeliveredByIntructor  from Contacts_All ca " +
                    "INNER JOIN SiteContactLinks sc ON ca.ContactId = sc.ContactId   and sc.Status = 'A' " +
                    " Inner join Main_site s on s.SiteId = sc.SiteId and s.Status = 'A' " +
                    " INNER JOIN Courses c ON ca.ContactId = c.ContactId AND sc.SiteId = c.SiteId " +
                    "where ca.Status = 'A' ";
                    //"select ca.ContactId,ContactName,TotalCourseDeliveredByIntructor from " +
                    //"(select c.ContactId, COUNT(c.ContactId) as TotalCourseDeliveredByIntructor from " +
                    //"(select ContactId, s.SiteId from " +
                    //"(select SiteId from Main_site where `Status`='A' ";
                JObject json = JObject.FromObject(data);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND OrgId in (" + property.Value.ToString() + ") ";
                        }
                    }
                    if (property.Name.Equals("SiteId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND s.SiteId in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                query += " group by  ca.ContactId,ca.ContactName" +
                    " ORDER BY TotalCourseDeliveredByIntructor DESC LIMIT 10;";
                    //") as s " +
                    //"INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId and sc.`Status` = 'A') as sc " +
                    //"INNER JOIN Courses c ON sc.ContactId = c.ContactId AND sc.SiteId = c.SiteId GROUP BY c.ContactId) as c " +
                    //"INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId and ca.`Status` = 'A' ORDER BY TotalCourseDeliveredByIntructor DESC LIMIT 10";
                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("SitePerformance")]
        public IActionResult SitePerformance([FromBody] dynamic data)
        {
            try
            {
                string siteID = data.siteid.ToString();
                if (!string.IsNullOrEmpty(siteID))
                {
                    siteID = "and s.SiteId in (" + data.siteid + ") ";
                }
                //Sebelumnya pakai yg ini
                // string query = 
                //     "select geo_name, TotalPerformanceSite from ( select rg.geo_name, Count(*) as TotalPerformanceSite from Main_organization as m inner join Main_site as s on m.OrgId = s.OrgId inner join Ref_countries as rc on rc.countries_code = m.RegisteredCountryCode inner join Ref_geo as rg on rg.geo_code = rc.geo_code inner join Courses as c on s.SiteId = c.SiteId where s.Status = 'A' and YEAR(s.DateAdded) = YEAR(NOW()) and MONTH(s.DateAdded) = MONTH(NOW()) "+
                //     "and m.OrgId in (" + data.OrgId + ") "+
                //     "group by rg.geo_code union select geo_name, 0 TotalPerformanceSite from Ref_geo) as sitemonthperform GROUP BY geo_name";
                //Diubah tgl 11/08/2018
                string query =
                "select geo_name, TotalPerformanceSite from " +
                "( select rg.geo_name, Count(*) as TotalPerformanceSite from Main_organization as m " +
                "inner join Main_site as s on m.OrgId = s.OrgId " +
                "inner join Ref_countries as rc on m.RegisteredCountryCode = rc.countries_code " +
                "inner join Ref_geo as rg on rc.geo_code = rg.geo_code " +
                "inner join Courses as c on s.SiteId = c.SiteId " +
                "where s.`Status` = 'A' and (s.DateAdded BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01') AND CURDATE()) and m.OrgId in (" + data.OrgId + ") " + siteID +
                //"group by rg.geo_code union select geo_name, 0 TotalPerformanceSite from Ref_geo) as sitemonthperform GROUP BY geo_name";
                "group by rg.geo_code) as sitemonthperform GROUP BY geo_name";

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("SitePerformanceor")]
        public IActionResult SitePerformanceor([FromBody] dynamic data)
        {
            try
            {
                string siteID = data.siteid.ToString();
                if (!string.IsNullOrEmpty(siteID))
                {
                    siteID = "and s.SiteId in (" + data.siteid + ") ";
                }
               
                string query =
                "select geo_name, TotalPerformanceSite from " +
                "( select rg.geo_name, Count(*) as TotalPerformanceSite from Main_organization as m " +
                "inner join Main_site as s on m.OrgId = s.OrgId " +
                "inner join Ref_countries as rc on m.RegisteredCountryCode = rc.countries_code " +
                "inner join Ref_geo as rg on rc.geo_code = rg.geo_code " +
                "inner join Courses as c on s.SiteId = c.SiteId " +
                "where s.`Status` = 'A' and (s.DateAdded BETWEEN DATE_FORMAT(NOW() ,'%Y-%m-01') AND CURDATE()) and m.OrgId in (" + data.OrgId + ") " + siteID +
                "group by rg.geo_code union select geo_name, 0 TotalPerformanceSite from Ref_geo) as sitemonthperform GROUP BY geo_name";

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("CoursesByMonth")]
        public IActionResult CoursesByMonth([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                string query = "Select COUNT(CourseId) AS TotalCourseMonthByPartnerType, s.SiteId from Main_site s inner join Courses c on s.SiteId = c.SiteId where MONTH(StartDate) = MONTH( CURRENT_DATE ( ) ) AND YEAR(StartDate ) = YEAR( CURRENT_DATE ( ) ) and s.Status = 'A' ";
                //string query = "SELECT TotalCourseMonthByPartnerType, PartnerType FROM (SELECT TotalCourseMonthByPartnerType, PartnerType, OrgId FROM (SELECT COUNT(CourseId) AS TotalCourseMonthByPartnerType, SiteId, PartnerType FROM Courses WHERE MONTH ( CompletionDate ) = MONTH ( CURRENT_DATE ( ) ) AND YEAR ( CompletionDate ) = YEAR ( CURRENT_DATE ( ) ) GROUP BY PartnerType) AS c INNER JOIN Main_site s ON c.SiteId = s.SiteId WHERE s.`Status` = 'A') AS s ";
                JObject json = JObject.FromObject(data);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("OrgId"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "and s.OrgId in (" + property.Value.ToString() + ") ";
                         }
                    }
                    if (property.Name.Equals("siteid"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "and s.SiteId in (" + property.Value.ToString() + ") ";
                        }
                    }
                }
                query += " GROUP BY s.SiteId;";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("CoreProductATC")]
        public IActionResult CoreProductATC([FromBody] dynamic data)
        {
            string result = "";
            try
            {
                string siteID = data.siteid.ToString();
                if (!string.IsNullOrEmpty(siteID))
                {
                    siteID = "and s.SiteId in (" + data.siteid + ") ";
                }
                //Diubah tanggal 11/08/2018
                string query =
                    "select p.productName, COUNT(c.CourseId) TotalCourseEvaCoreProduct " +
                    "from CourseSoftware cs " +
                    "INNER JOIN Products p ON cs.productId = p.productId INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.OrgId in (" + data.OrgId + ") " + siteID +
                    "where c.PartnerType like '%ATC%' and s.`Status` != 'X' and (c.StartDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE()) and (p.productName LIKE '%Revit%' OR p.productName LIKE '%BIM%' OR p.productId IN ( 3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33 ) ) group by p.productname " +
                    "union " +
                    "select 'All non core Product' as 'productName',COUNT(c.CourseId) TotalCourseEvaCoreProduct from CourseSoftware cs " +
                    "INNER JOIN Products p ON cs.productId = p.productId INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.OrgId in (" + data.OrgId + ") " + siteID +
                    "where c.PartnerType like '%ATC%' and s.`Status` != 'X' and (c.StartDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE()) and p.productName NOT LIKE '%Revit%' and p.productName NOT LIKE '%BIM%' and p.productId not in (3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33) ";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
               
            }
            catch
            {
            }
            finally
            {
                if (ds.Tables[0].Rows.Count != 0)
                {
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                }
                else
                {
                    result = "Not found";
                }
                    
            }
            return Ok(result);
        }
        [HttpPost("CoreProductAAP")]
        public IActionResult CoreProductAAP([FromBody] dynamic data)
        {
            string result = "";
            try
            {
                string siteID = data.siteid.ToString();
                if (!string.IsNullOrEmpty(siteID))
                {
                    siteID = "and s.SiteId in (" + data.siteid + ") ";
                }
                //Diubah tanggal 11/08/2018
                string query =
                    "select p.productName, COUNT(c.CourseId) TotalCourseEvaCoreProduct " +
                    "from CourseSoftware cs " +
                    "INNER JOIN Products p ON cs.productId = p.productId INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.OrgId in (" + data.OrgId + ") " + siteID +
                    "where c.PartnerType like '%AAP%' and s.`Status` != 'X' and (c.StartDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE()) and (p.productName LIKE '%Revit%' OR p.productName LIKE '%BIM%' OR p.productId IN ( 3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33 ) ) group by p.productname " +
                    "union " +
                    "select 'All non core Product',COUNT(c.CourseId) TotalCourseEvaCoreProduct from CourseSoftware cs " +
                    "INNER JOIN Products p ON cs.productId = p.productId INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
                    "INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.OrgId in (" + data.OrgId + ") " + siteID +
                    "where c.PartnerType like '%AAP%' and s.`Status` != 'X' and (c.StartDate BETWEEN DATE_FORMAT(NOW() ,'%Y-02-01') AND CURDATE()) and p.productName NOT LIKE '%Revit%' and p.productName NOT LIKE '%BIM%' and p.productId not in (3, 28, 39,40.42,302,303,304,305,306,307,308,338,388,395, 152,135, 136,147,148,836, 1, 33)";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
              
            }
            catch
            {
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

    }
}