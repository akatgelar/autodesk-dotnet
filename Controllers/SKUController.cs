using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.Models;
using autodesk.Code.AuditLogServices;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/SKU")]
    public class SKUController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        private readonly MySQLContext _mySQLContext;
        String result;

        public Audit _audit;//= new Audit();
        private readonly IAuditLogServices _auditLogServices;

        public SKUController(MySqlDb sqlDb, MySQLContext mySQLContext, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _mySQLContext = mySQLContext;
            _audit = new Audit(oDb);
            _auditLogServices = auditLogServices;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append(
                    "SELECT " +
                    "(SELECT SiteName FROM Main_site s WHERE s.SiteId = sku.SiteID ) AS Distributor, " +
                    "(SELECT Territory_Name FROM Ref_territory WHERE TerritoryId IN (SELECT TerritoryId FROM Ref_countries WHERE countries_code IN (SUBSTRING_INDEX(sku.CountryCode,',',1)))) AS Territory_Name, " +
                    "SKUDescription, Price, License, d.KeyValue AS Currency, SiteCountryDistributorSKUId " +
                    "FROM SiteCountryDistributorSKU sku INNER JOIN Dictionary d ON sku.Currency = d.`Key` AND d.Parent = 'Currencies' AND d.`Status` = 'A' " +
                    "GROUP BY SiteCountryDistributorSKUId " +
                    "ORDER BY SKUDescription ASC"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CountryCode/{code}")]
        public IActionResult Select(string code)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append(
                    "select * from SiteCountryDistributorSKU join Dictionary on SiteCountryDistributorSKU.Currency = Dictionary.Key " +
                    "where Dictionary.Parent = 'Currencies' and CountryCode = '" + code + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("TerritoryId/{code}")]
        public IActionResult TerritoryId(string code)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append(
                    "select * from SiteCountryDistributorSKU sku join Dictionary d on sku.Currency = d.Key " +
                    "where d.Parent = 'Currencies' and SUBSTRING_INDEX(sku.CountryCode,',',1) in (select countries_code from Ref_countries where TerritoryId in (" + code + "))"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from SiteCountryDistributorSKU where SiteCountryDistributorSKUId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("EditInvoice/{sku}/{invoice}")]
        public IActionResult WhereId(int sku, string invoice)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from SiteCountryDistributorSKU sku LEFT JOIN InvoiceSKU isku ON sku.SiteCountryDistributorSKUId = isku.SiteCountryDistributorSKUId where sku.SiteCountryDistributorSKUId = '" + sku + "' and isku.InvoiceId = '" + invoice + "'");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select * from SiteCountryDistributorSKU ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("SiteID"))
                {
                    query += "SiteID = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("CountryCode"))
                {
                    query += "CountryCode = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("EDistributorType"))
                {
                    query += "EDistributorType = '" + property.Value.ToString() + "' ";
                }

                i = i + 1;
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("Currency")]
        public IActionResult SelectCurrency()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Dictionary where Parent = 'Currencies';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            int lastID = 0;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from SiteCountryDistributorSKU;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1);

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into SiteCountryDistributorSKU " +
                    "(SiteID, CountryCode, EDistributorType, SKUDescription, Price, License, Currency, Status) " +
                    "values ('" + data.SiteID + "','" + data.CountryCode + "','" + data.EdistributorType + "','" + data.SKUDescription + "','" + data.Price + "','" + data.License + "','" + data.Currency + "','" + data.Status + "'); " +
                    "select LAST_INSERT_ID() as last_insert_id; "
                );

                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                lastID = Int32.Parse(o3["last_insert_id"].ToString());
            }
            catch
            {
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from SiteCountryDistributorSKU;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            if (count2 > count1)
            {

                //string description = "Insert data SKU" + "<br/>" +
                //    "Description = "+ data.SKUDescription + "<br/>" +
                //    "Price = "+ data.Price + "<br/>" +
                //    "License = "+ data.License + "<br/>" +
                //    "Currency = "+ data.Currency;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                sb = new StringBuilder();
                sb.AppendFormat("select * from SiteCountryDistributorSKU where SiteCountryDistributorSKUId = " + lastID + ";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.SiteCountryDistributorSKU, data.UserId.ToString(), data.cuid.ToString(), "where SiteCountryDistributorSKUId =" + lastID + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }
            }

            return Ok(res);

        }

        [HttpGet("getDistributor/{obj}")]
        public IActionResult WhereObjDistributor(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select s.Orgid,s.SiteId,s.SiteName,EDistributorType,scd.CountryCode,scd.PrimaryD,(SELECT GROUP_CONCAT(DISTINCT RoleName ORDER BY RoleName SEPARATOR ', ') " +
            "From SiteRoles scl, Roles r WHERE scl.SiteId = s.SiteId AND Status <> 'X' AND scl.RoleId = r.RoleId ) As Roles from Main_site s " +
            "inner join SiteCountryDistributor scd on s.SiteId = scd.SiteId INNER JOIN Ref_countries rc ON scd.CountryCode = rc.countries_code ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("countries_id"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "scd.CountryCode in (select countries_code from Ref_countries WHERE countries_id in (" + property.Value.ToString() + ")) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("EDistributorType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "EDistributorType = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("TerritoryId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.TerritoryId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }

            if (i == 0) { query += " where "; }
            if (i > 0) { query += " and "; }
            query += " s.Status <> 'X' group by s.SiteId";

            try
            {
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            //JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from SiteCountryDistributorSKU where SiteCountryDistributorSKUId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update SiteCountryDistributorSKU set " +
                    "SiteID='" + data.SiteID + "'," +
                    "CountryCode='" + data.CountryCode + "'," +
                    "EDistributorType='" + data.EdistributorType + "'," +
                    "SKUDescription='" + data.SKUDescription + "'," +
                    "Price='" + data.Price + "'," +
                    "License='" + data.License + "'," +
                    "Status='" + data.Status + "'," +
                    "Currency='" + data.Currency + "'" +
                    "where SiteCountryDistributorSKUId='" + id + "'"
                );
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from SiteCountryDistributorSKU where SiteCountryDistributorSKUId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //o3 = JObject.Parse(result);  

            /* autodesk plan 10 oct - complete all history log */

            //string description = "Update data SKU" + "<br/>" +
            //    "SKU Id = "+ id +"<br/>"+
            //    "Distributor = "+ data.SiteID +"<br/>"+
            //    "Description = "+ data.SKUDescription +"<br/>"+
            //    "Price = "+ data.Price +"<br/>"+
            //    "License = "+ data.License +"<br/>"+
            //    "Currency = "+ data.Currency +"<br/>";

            //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.SiteCountryDistributorSKU, data.UserId.ToString(), data.cuid.ToString(), "where SiteCountryDistributorSKUId =" + id + ""))
            {
                res =
                 "{" +
                 "\"code\":\"1\"," +
                 "\"message\":\"Update Data Success\"" +
                 "}";
            }
            /* end line autodesk plan 10 oct - complete all history log */

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 

            //} 

            return Ok(res);
        }

        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        {
            //JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            var masterSKU = _mySQLContext.SiteCountryDistributorSKU.FirstOrDefault(x => x.SiteCountryDistributorSKUId == id);
            if (masterSKU != null)
            {
                var pricingSKU = _mySQLContext.InvoicePricingSKU.Where(x => x.SiteCountryDistributorSKUId == masterSKU.SiteCountryDistributorSKUId).ToList();
                if (pricingSKU?.Count()>0)
                {
                    pricingSKU.ForEach(x=>x.Status = AutodeskConst.StatusConts.X);

                }
                masterSKU.Status = AutodeskConst.StatusConts.X;
                _mySQLContext.SaveChanges();
            }
            else
            {

                var pricingSKU = _mySQLContext.InvoicePricingSKU.FirstOrDefault(x => x.Id == id);
                if (pricingSKU != null)
                {
                    pricingSKU.Status = AutodeskConst.StatusConts.X;
                    _mySQLContext.SaveChanges();
                }
            }






            //sb = new StringBuilder();
            //sb.AppendFormat("select * from SiteCountryDistributorSKU where SiteCountryDistributorSKUId  =" + id + ";");
            //sqlCommand = new MySqlCommand(sb.ToString());
            //ds = oDb.getDataSetFromSP(sqlCommand);
            //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            //try
            //{
            //    sb = new StringBuilder();
            //    sb.AppendFormat("delete from SiteCountryDistributorSKU where SiteCountryDistributorSKUId = '" + id + "'");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);
            //}
            //catch
            //{
            //}
            //finally
            //{
            //    sb = new StringBuilder();
            //    sb.AppendFormat("select * from SiteCountryDistributorSKU where SiteCountryDistributorSKUId  =" + id + ";");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);

            //    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //    o3 = JObject.Parse(result);

            //    if (o3.Count < 1)
            //    {

            //string description = "Delete data SKU" + "<br/>" +
            //"SKU ID = " + id;

            //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());
            //JObject NewData = new JObject();
            //if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.SiteCountryDistributorSKU, UserId.ToString(), cuid.ToString(), "where SiteCountryDistributorSKUId =" + id + ""))
            //{
            _auditLogServices.LogHistory(new History
            {
                Admin = HttpContext.User.GetUserId(),
                Date = DateTime.Now,
                Description = "Delete SKU <br/>",
                Id = id.ToString()
            });
            res =
             "{" +
             "\"code\":\"1\"," +
             "\"message\":\"Delete Data Success\"" +
             "}";
            //}

            //}
            return Ok(res);
        }


    }
}
