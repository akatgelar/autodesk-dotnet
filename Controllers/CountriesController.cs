using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.Models;
using autodesk.Code.AuditLogServices;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/Countries")]
    public class CountriesController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit;// = new Audit();
        private readonly MySQLContext _mySQLContext;
        private readonly IAuditLogServices _auditLogServices;
        public CountriesController(MySqlDb sqlDb, MySQLContext mySQLContext, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _audit = new Audit(oDb);
            _mySQLContext = mySQLContext;
            _auditLogServices = auditLogServices;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                var user = _mySQLContext.ContactsAll.FirstOrDefault(x => x.ContactId == HttpContext.User.GetUserId());

                if (user != null && user.UserLevelId != AutodeskConst.UserLevelIdEnum.SUPERADMIN && user.UserLevelId != AutodeskConst.UserLevelIdEnum.STUDENT)
                {
                    var query = (from o in _mySQLContext.MainOrganization 
                                  join s in _mySQLContext.MainSite on o.OrgId equals s.OrgId
                                  join scl in _mySQLContext.SiteContactLinks on s.SiteId equals scl.SiteId
                                  join c in _mySQLContext.ContactsAll on scl.ContactId equals c.ContactId
                                  where c.EmailAddress == user.EmailAddress
                                  select new { o.OrgId, c.ContactId }
                                  );
                    var joinQuery = (from s in _mySQLContext.MainSite 
                                      join rc in _mySQLContext.RefCountries on s.SiteCountryCode equals rc.CountriesCode
                                      join q in query on s.OrgId equals q.OrgId
                                      select new { rc.TerritoryId }
                                      );
                    var resultQuery = (from jq in joinQuery 
                                       join country in _mySQLContext.RefCountries on jq.TerritoryId equals country.TerritoryId
                                       select country
                                       ).Distinct().OrderBy(x=>x.CountriesName);

                    using (var command = _mySQLContext.Database.GetDbConnection().CreateCommand())
                    {
                        command.CommandText = resultQuery.ToSql();

                        _mySQLContext.Database.OpenConnection();
                        using (var reader = command.ExecuteReader())
                        {
                            var res = reader.ToJsonString();
                            return Ok(res);
                        }

                    }


                }
                else
                {
                    sb = new StringBuilder();
                    sb.Append("select * from Ref_countries where geo_code <> '' ORDER BY countries_name ASC ");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                    }
                    else
                    {
                        return Ok(JsonConvert.DeserializeObject("[]"));
                    }

                    return Ok(result);
                }
            }
            catch (Exception e)
            {
                return Ok(JsonConvert.DeserializeObject("[]"));
            }

        }
        [HttpPost("SelectAdmin")]
        public IActionResult SelectAdmin([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Ref_countries where geo_code <> '' ORDER BY countries_name ASC ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }
        [HttpGet("{isDistributor}/{org}")]
        public IActionResult SelectCountryByTerritory(Boolean isDistributor, string org)
        {
            //Fix isu no.263 untuk user Distributor
            string query = "";
            if (isDistributor == true)
            {
                query =
                "SELECT countries_code,countries_name FROM Ref_countries rc " +
                "WHERE TerritoryId IN " +
                "(SELECT TerritoryId FROM Main_organization o INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code WHERE OrgId IN (" + org + "))";
            }
            else
            {
                query = "select * from Ref_countries where geo_code <> '' ORDER BY countries_name ASC ";
            }
            try
            {
                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("NotOnTerritory")]
        public IActionResult SelectNotOnTerritory()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select countries_id, countries_name from Ref_countries where TerritoryId not in (select TerritoryId from Ref_territory)");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("NotOnTerritoryWithId/{id}")]
        public IActionResult SelectNotOnTerritory(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select countries_id, countries_name from Ref_countries where TerritoryId not in (select TerritoryId from Ref_territory where TerritoryId not in ('" + id + "'))");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CountryCode/{obj}")]
        public IActionResult SelectCountryCode(string obj)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select countries_code from Ref_countries where countries_id in (" + obj + ")");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CountryId/{obj}")]
        public IActionResult SelectCountryId(string obj)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select countries_id,countries_name from Ref_countries where countries_code in (" + obj + ")");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetTerritorySKU/{obj}")]
        public IActionResult GetTerritorySKU(string obj)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select DISTINCT rt.TerritoryId, rt.Territory_Name from Ref_countries rc INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId where rc.countries_code in (" + obj + ")");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("order_countries_tel")]
        public IActionResult SelectPhoneCode()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select DISTINCT(countries_tel) from Ref_countries where geo_code <> '' ORDER BY countries_tel asc ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CountryFilter/{id}")]
        public IActionResult WhereIdSubregion(int id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select countries_id,countries_code,countries_name,MarketTypeId from Ref_countries where subregion_code in "
                    + "(select subregion_code from Ref_subregion where subregion_id = '" + id + "') and countries_code != ''");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select geo_id, Ref_countries.geo_code, Ref_countries.geo_name, region_id, Ref_countries.region_code, Ref_countries.region_name, subregion_id, Ref_countries.subregion_code, Ref_countries.subregion_name, countries_code, countries_name, countries_tel, Embargoed, MarketTypeId from Ref_countries " +
                    "join Ref_subregion on Ref_countries.subregion_code = Ref_subregion.subregion_code " +
                    "join Ref_region on Ref_countries.region_code = Ref_region.region_code " +
                    "join Ref_geo on Ref_countries.geo_code = Ref_geo.geo_code where countries_id = '" + id + "' group by countries_code");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("filter/{id}")]
        public IActionResult WhereIdGeo(string id)
        {
            try
            {
                if (id != "")
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Ref_countries where geo_code in ( " + id + " );");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                else
                {
                    result = "";
                }
                // Console.WriteLine(sb.ToString());

            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }


        //[HttpGet("filterByGeoByTerritory/{idGeo}/{idTerritory}")]
        [HttpPost("filterByGeoByTerritory")]
        public IActionResult filterByGeoByTerritory([FromBody] dynamic data)
        {
            try
            {
                if (data.CtmpGeo.ToString() != "" || data.CtmpTerritory.ToString() != "")
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Ref_countries where geo_code in ( " + data.CtmpGeo + " ) or TerritoryId in ( " + data.CtmpTerritory + " ) ");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                else
                {
                    result = "";
                }
                // Console.WriteLine(sb.ToString());


            }
            catch
            {
                result = "[]";
            }
            finally
            {
                if (ds != null && ds.Tables != null && ds.Tables.Count > 0)
                {
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                }

            }

            return Ok(result);
        }

        [HttpGet("GetGeo/{countries_id}")]
        public IActionResult GetGeoId(string countries_id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select geo_code from Ref_countries where countries_code = '" + countries_id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("filterregion/{id}")]
        public IActionResult WhereIdRegion(string id)
        {
            try
            {
                if (id != "")
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Ref_countries where region_code in ( " + id + " );");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                else
                {
                    result = "";
                }

            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("filtersubregion/{id}")]
        public IActionResult WhereIdSubRegion(string id)
        {
            try
            {
                if (id != "")
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Ref_countries where subregion_code in ( " + id + " );");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                else
                {
                    result = "";
                }

            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select * from Ref_countries join Ref_geo on Ref_countries.geo_code = Ref_geo.geo_code join Ref_region on Ref_countries.region_code = Ref_region.region_code join Ref_subregion on Ref_countries.subregion_code = Ref_subregion.subregion_code ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("subregion_code"))
                {
                    query += "subregion_code = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("countries_code"))
                {
                    query += "countries_code = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("TerritoryId"))
                {
                    query += "TerritoryId in (" + property.Value.ToString() + ") ";
                }

                i = i + 1;
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_countries;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1);
            sb = new StringBuilder();
            sb.AppendFormat("SELECT TerritoryId FROM Ref_territory where Territory_Name='" + data.region_name + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);
            data.TerritoryId = o3["TerritoryId"].ToString();
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Ref_countries " +
                    "(countries_code,countries_name,geo_code,region_code,subregion_code,TerritoryId,geo_name,region_name,subregion_name,countries_tel,MarketTypeId,Embargoed,cuid,cdate) " +
                    "values ('" + data.countries_code + "','" + data.countries_name + "','" + data.geo_code + "','" + data.region_code + "','" + data.subregion_code + "','" + data.TerritoryId + "','" + data.geo_name + "','" + data.region_name + "','" + data.subregion_name + "','" + data.countries_tel + "','" + data.MarketTypeId + "','" + data.Embargoed + "','" + data.cuid + "',NOW());"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_countries;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            if (count2 > count1)
            {
                //string description = "Insert data Country" + "<br/>" +                     
                //    "Country Code = "+ data.countries_code + "<br/>" +
                //    "Country Name = "+ data.countries_name + "<br/>" +
                //    "Country Phone Code = "+ data.countries_tel + "<br/>" +
                //    "Geo Name = "+ data.geo_name + "<br/>" +
                //    "Region Nmae = "+ data.region_name + "<br/>" +
                //    "Sub Region Name = "+ data.subregion_name;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                JObject OldData = new JObject();
                sb = new StringBuilder();
                sb.AppendFormat("select * from Ref_countries where countries_code ='" + data.countries_code + "' and countries_name='" + data.countries_name + "'; ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_countries, data.UserId.ToString(), data.cuid.ToString(), "where Country Code =" + data.countries_code + ""))
                {
                    res =
                       "{" +
                       "\"code\":\"1\"," +
                       "\"message\":\"Insert Data Success\"" +
                       "}";
                }

            }

            return Ok(res);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            //JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_countries where countries_id =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Ref_countries set " +
                    "countries_code='" + data.countries_code + "'," +
                    "countries_name='" + data.countries_name + "'," +
                    "geo_code='" + data.geo_code + "'," +
                    "region_code='" + data.region_code + "'," +
                    "subregion_code='" + data.subregion_code + "'," +
                    "geo_name='" + data.geo_name + "'," +
                    "region_name='" + data.region_name + "'," +
                    "subregion_name='" + data.subregion_name + "'," +
                    "countries_tel='" + data.countries_tel + "'," +
                    "muid='" + data.muid + "'," +
                    "mdate=NOW() " +
                    "where countries_id='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_countries where countries_id =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 
            //string descriptionU = "Update data Country" + "<br/>" +                     
            //        "Country Code = "+ data.countries_code + "<br/>" +
            //        "Country Name = "+ data.countries_name + "<br/>" +
            //        "Country Phone Code = "+ data.countries_tel + "<br/>" +
            //        "Geo Name = "+ data.geo_name + "<br/>" +
            //        "Region Nmae = "+ data.region_name + "<br/>" +
            //        "Sub Region Name = "+ data.subregion_name;

            //    _audit.ActionPost(data.UserId.ToString(), descriptionU, data.cuid.ToString());
            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_countries, data.UserId.ToString(), data.cuid.ToString(), "where Country Code =" + data.countries_code + ""))
            {
                res =
                   "{" +
                   "\"code\":\"1\"," +
                   "\"message\":\"Update Data Success\"" +
                   "}";
            }

            //} 

            return Ok(res);
        }



        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            try
            {
                //sb = new StringBuilder();
                //sb.AppendFormat("delete from Ref_countries where countries_id = '"+id+"'");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);

                var country = _mySQLContext.RefCountries.FirstOrDefault(x => x.CountriesId == id);
                if (country != null)
                {
                    _mySQLContext.RefCountries.Remove(country);
                    var deleted = _mySQLContext.SaveChanges();
                    if (deleted > 0)
                    {
                        var admin = HttpContext.User.GetUserId();
                        _auditLogServices.LogHistory(new History { Admin = admin, Date = DateTime.Now, Description = "Delete RefCountries <br/> CountriesId : " + id, Id = id.ToString() });
                        res =
                       "{" +
                       "\"code\":\"1\"," +
                       "\"message\":\"Delete Data Success\"" +
                       "}";
                    }
                }
            }
            catch
            {
            }
            //finally
            //{ 
            //    sb = new StringBuilder();
            //    sb.AppendFormat("select * from Ref_countries where countries_id  ="+id+";");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);

            //    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //    o3 = JObject.Parse(result);  
            //    Console.WriteLine(o3.Count);
            //    if(o3.Count < 1){ 

            //        string description = "Delete data Country" + "<br/>" +
            //        "Country ID = "+ id;

            //        _audit.ActionPost(UserId.ToString(), description, cuid.ToString());

            //        res =
            //            "{"+
            //            "\"code\":\"1\","+
            //            "\"message\":\"Delete Data Success\""+
            //            "}"; 
            //    }
            //}

            return Ok(res);
        }

        //[HttpGet("DistTerr/{territoryarr}/{orgarr}")]
        [HttpPost("DistTerr")]
        //public IActionResult DistTerr(string territoryarr,string orgarr, [FromBody] dynamic data)
        public IActionResult DistTerr([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select countries_code, countries_name from Ref_countries where TerritoryId IN (" + data.CtmpTerritory + ") AND countries_code IN (select sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + data.CDistTerr + " )) ORDER BY countries_name");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        //[HttpGet("OrgSiteTerr/{territoryarr}/{orgarr}")]
        [HttpPost("OrgSiteTerr")]
        // public IActionResult OrgSiteTerr(string territoryarr,string orgarr, [FromBody] dynamic data)
        public IActionResult OrgSiteTerr([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select countries_code, countries_name from Ref_countries where TerritoryId IN (" + data.CtmpTerritory + ") AND countries_code IN (select o.RegisteredCountryCode from Main_organization o WHERE o.OrgId IN ( " + data.COrgSiteTerr + " )) ORDER BY countries_name");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        //[HttpGet("DistGeo/{geoarr}/{orgarr}")]
        [HttpPost("DistGeo")]
        public IActionResult DistGeo([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select countries_code, countries_name from Ref_countries where geo_code IN (" + data.CtmpGeo + ") AND countries_code IN (select sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + data.CDistGeo + " )) ORDER BY countries_name");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        //[HttpGet("OrgSite/{geoarr}/{orgarr}")]
        [HttpPost("OrgSite")]
        public IActionResult OrgSite([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select countries_code, countries_name from Ref_countries where geo_code IN (" + data.CtmpGeo + ") AND countries_code IN (select o.RegisteredCountryCode from Main_organization o WHERE o.OrgId IN ( " + data.COrgSite + " )) ORDER BY countries_name");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        /* fixed issue 24092018 - populate country by role site admin */
        // [HttpPost("SiteAdmin/{geoarr}/{sitearr}")]
        [HttpPost("SiteAdmin")]
        //public IActionResult SiteAdmin(string geoarr,string sitearr)
        public IActionResult SiteAdmin([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select countries_code, countries_name from Ref_countries where geo_code IN (" + data.CtmpGeo + ") AND countries_code IN (select s.SiteCountryCode from Main_site s WHERE s.SiteId IN ( " + data.SiteAdmin + " )) ORDER BY countries_name");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        //[HttpGet("SiteAdminTerr/{territoryarr}/{sitearr}")]
        [HttpPost("SiteAdminTerr")]
        //public IActionResult SiteAdminTerr(string territoryarr,string sitearr)
        public IActionResult SiteAdminTerr(string territoryarr, string sitearr, [FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select countries_code, countries_name from Ref_countries where TerritoryId IN (" + data.CtmpTerritory + ") AND countries_code IN (select s.SiteCountryCode from Main_site s WHERE s.SiteId IN ( " + data.SiteAdminTerr + " )) ORDER BY countries_name");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }
        /* end line fixed issue 24092018 - populate country by role site admin */


        [HttpPost("whererpt")]
        public IActionResult whererpt([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select distinct RegisteredCity from Main_organization where RegisteredCountryCode IN (" + data.Countrycode + ") and RegisteredCity IS NOT NULL and  RegisteredCity <> ' '  ORDER BY RegisteredCountryCode");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }


    }
}
