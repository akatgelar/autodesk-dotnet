﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/UserLevel")]
    public class UserLevelController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public UserLevelController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select UserLevelId, Name from UserLevel;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ListAccess")]
        public IActionResult SelectColumns()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from UserLevel;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        /* new request nambah user role -__ */

        // [HttpGet("WithoutStudent")]
        // public IActionResult WithoutStudent()
        // {
        //     try
        //     {
        //         sb = new StringBuilder();
        //         sb.Append("select UserLevelId, `Name` from UserLevel where UserLevelId not in ('STUDENT','SITE')");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not Found";
        //     }
        //     finally
        //     {
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // }

        // [HttpGet("WithoutStudent/{code}")]
        // public IActionResult WithoutStudentWithCodeAcc(string code)
        // {
        //     string query = "";
        //     try
        //     {
        //         if(code == "Distributor"){
        //             query="select UserLevelId, `Name` from UserLevel WHERE UserLevelId IN ('ADMIN','ORGANIZATION','TRAINER','DISTRIBUTOR')";
        //         }
        //         else if(code == "Org"){
        //             query="select UserLevelId, `Name` from UserLevel WHERE UserLevelId IN ('TRAINER','ORGANIZATION')";
        //         }
        //         // else if(code == "Site"){
        //         //     query="select UserLevelId, `Name` from UserLevel WHERE UserLevelId IN ('TRAINER')";
        //         // }
        //         else if(code == "Admin"){
        //             query="select UserLevelId, `Name` from UserLevel WHERE UserLevelId IN ('ORGANIZATION','DISTRIBUTOR','TRAINER','ADMIN')";
        //         }

        //         sb = new StringBuilder();
        //         sb.Append(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not Found";
        //     }
        //     finally
        //     {
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // }
        
        [HttpGet("WithoutStudent/{code}")]
        public IActionResult WithoutStudentWithCodeAcc(string code)
        {
            string query = "select UserLevelId, `Name` from UserLevel WHERE UserLevelId ";
            try
            {
                if(code == "Distributor"){
                    query += "IN ('DISTRIBUTOR','ORGANIZATION','TRAINER','SITE')";
                }
                else if(code == "Org"){
                    query += "IN ('ORGANIZATION','SITE','TRAINER')";
                }
                else if(code == "Site"){
                    query += "IN ('SITE','TRAINER')";
                }else{
                    query += "NOT IN ('STUDENT')";
                }

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        /* new request nambah user role -__ */

        [HttpGet("{id}")]
        public IActionResult WhereId(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from UserLevel where UserLevelId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";

            try
            {  
                sb = new StringBuilder();
                // sb.AppendFormat("update UserLevel set '" + data.Kolom + "' = '" + data.HakAkses + "' where UserLevelId = '" + id + "'");
                sb.AppendFormat("update UserLevel set " + data.Kolom + " where UserLevelId = '" + id + "' ");  
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from UserLevel where UserLevelId ='"+id+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            res =
                "{"+
                "\"code\":\"1\","+
                "\"message\":\"Update Data Success\""+
                "}"; 
 
            return Ok(res);  
        } 

    }
}