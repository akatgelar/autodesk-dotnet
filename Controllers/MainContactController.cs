using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;   
using autodesk.Code.CommonServices;
using autodesk.Code.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace autodesk.Controllers
{
    [Route("api/MainContact")]
    public class MainContactController : Controller
    {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        DataSet ds1 = new DataSet();
        DataSet ds2 = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        private IDataServices dataService;
        private ICommonServices commonServices;
        String result;
        String result2;
        private Audit _audit;
        private MySQLContext db;
        public MainContactController(MySqlDb sqlDb, MySQLContext _db)
        {
            oDb = sqlDb;
            db = _db;
            dataService=new DataServices(oDb,db);
            _audit= new Audit(oDb);
            commonServices = new CommonServices();
        }


        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Contacts_All ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("FindContact/{value}")]
        public IActionResult FindContact(string value)
        {
            JObject resultfinal;
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT ContactId, replace( replace( replace( ContactName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as ContactName, CONCAT(ContactId,' | ',replace( replace( replace( ContactName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' )) AS Contact "+
                "FROM Contacts_All WHERE ContactId like '%"+value+"%'");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =  
                    "{"+
                    "\"results\":"+MySqlDb.GetJSONArrayString(ds.Tables[0])+""+
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                resultfinal = JObject.Parse(result);
            }
 
            return Ok(resultfinal);                  
        }

        [HttpGet("DetailContact/{id}/{siteid}")]
        public IActionResult DetailContact(string id, string siteid)
        {
            List<dynamic> dynamicModel = new List<dynamic>();
           
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "SELECT c.ContactIdInt, c.ContactId, replace( replace( replace( c.FirstName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as FirstName, replace( replace( replace( c.LastName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as LastName, replace( replace( replace( c.ContactName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as ContactName, c.Telephone1, c.Telephone2, c.Mobile1, c.Mobile2, tz.compName as Timezone, Timezone as valueTimeZone, c.Comments,c.Designation, DATE_FORMAT(c.DateLastAdmin, '%m/%d/%Y') AS DateLastAdmin, c.ContactId, c.InstructorId, c.Department, c.EmailAddress, c.EmailAddress2, c.PrimaryLanguage, DATE_FORMAT(c.LastLogin, '%m/%d/%Y') AS LastLogin, c.LastAdminBy, c.DoNotEmail, c.UserLevelId, c.PrimaryRoleId, rl.RoleName, c.PrimaryIndustryId, c.PrimarySiteId, c.SecondaryLanguage, tz.compName as Timezone, c.Salutation, o.OrganizationId, o.OrgName, d1.KeyValue as PrimaryLanguageUI, d2.KeyValue as SecondaryLanguageUI, c.Status as Status, c.TerminationReason as TerminationReason " +
                    "FROM Contacts_All c LEFT JOIN SiteContactLinks sc ON c.ContactId = sc.ContactId LEFT JOIN Main_site s ON sc.SiteId = s.SiteId LEFT JOIN Main_organization o ON s.OrgId = o.OrgId LEFT JOIN Dictionary d1 ON c.PrimaryLanguage = d1.`Key` AND d1.Parent = 'Languages' AND d1.`Status` = 'A' LEFT JOIN Dictionary d2 ON c.SecondaryLanguage = d2.`Key` AND d2.Parent = 'Languages' AND d2.`Status` = 'A' LEFT JOIN Roles rl ON rl.RoleId = c.PrimaryRoleId " +
                    "LEFT JOIN SabaComponentIds as tz on tz.compId = c.Timezone " +
                    "WHERE c.ContactIdInt = '" + id + "' and s.SiteId = '" + siteid + "' " +
                    "GROUP BY c.ContactIdInt"
                );
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        string notes = row[column].ToString();
                        notes = commonServices.HtmlEncoder(notes);
                        row[column] = notes;
                    }
                    
                }
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                if (dynamicModel.Count > 0)
                {
                    result = JsonConvert.SerializeObject(dynamicModel.FirstOrDefault(), Formatting.Indented,
                        settings);
                }
                else
                {
                    result = "Not Found";
                }
                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }


        [HttpGet("ContactQualifications/{siteid}")]
        public IActionResult ContactQualifications(string siteid)
        {
            JArray arrQualification;
            // JArray arrContact;
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    // "SELECT GROUP_CONCAT(c.ContactName) AS ContactName, c.ContactIdInt, q.QualificationId, CONCAT( p.productName, ' ', IFNULL( pv.Version, '' ) ) AS ProductName, p.`Year`, pf.familyName FROM Qualifications q LEFT JOIN Contacts_All c ON q.ContactId = c.ContactId LEFT JOIN SiteContactLinks scl ON c.ContactId = scl.ContactId LEFT JOIN Products p ON q.AutodeskProduct = p.ProductId LEFT JOIN ProductVersions pv ON q.AutodeskProductVersion = pv.ProductVersionsId LEFT JOIN ProductFamilies pf ON p.familyId = pf.familyId  WHERE q.STATUS <> 'X' AND scl.STATUS <> 'X' AND c.STATUS <> 'X' AND c.STATUS <> 'D' AND scl.SiteId = '" + siteid + "' GROUP BY CONCAT( p.productName, '-', IFNULL( pv.Version, '' ) )"
                    // "SELECT GROUP_CONCAT(DISTINCT(c.ContactName)) AS ContactName,c.ContactIdInt,q.QualificationId,CONCAT( p.productName, ' ', IFNULL( pv.Version, '' ) ) AS ProductName,p.`Year`,GROUP_CONCAT(pf.familyId) AS familyId " +
                    // "FROM Qualifications q " +
                    // "INNER JOIN Products p ON q.AutodeskProduct = p.productId " +
                    // "LEFT JOIN ProductFamilies pf ON p.productId = pf.productId " +
                    // "INNER JOIN Family f ON pf.familyId = f.familyId " +
                    // "LEFT JOIN ProductVersions pv ON q.AutodeskProductVersion = pv.ProductVersionsId " +
                    // "INNER JOIN Contacts_All c ON q.ContactId = c.ContactId " +
                    // "LEFT JOIN SiteContactLinks scl ON c.ContactId = scl.ContactId " +
                    // "INNER JOIN Main_site s ON scl.SiteId = s.SiteId " +
                    // "WHERE q.STATUS <> 'X' AND scl.STATUS <> 'X' AND c.STATUS <> 'X' AND c.STATUS <> 'D' AND s.SiteIdInt = '" + siteid + "' GROUP BY CONCAT( p.productName, '-', IFNULL( pv.Version, '' ) )"
                    "SELECT GROUP_CONCAT(DISTINCT(c.ContactName)) AS ContactName,c.ContactIdInt,q.QualificationId,CONCAT( p.productName, ' ', IFNULL( pv.Version, '' ) ) AS ProductName,p.`Year` "+
                    "FROM Qualifications q "+
                    "INNER JOIN Products p ON q.AutodeskProduct = p.ProductId "+
                    "LEFT JOIN ProductVersions pv ON q.AutodeskProductVersion = pv.ProductVersionsId "+
                    "INNER JOIN Contacts_All c ON q.ContactId = c.ContactId "+
                    "LEFT JOIN SiteContactLinks scl ON c.ContactId = scl.ContactId "+
                    "INNER JOIN Main_site s ON scl.SiteId = s.SiteId "+
                    "WHERE q.STATUS <> 'X' AND scl.STATUS <> 'X' AND c.STATUS <> 'X' AND c.STATUS <> 'D' AND s.SiteIdInt = '" + siteid + "' "+
                    "GROUP BY CONCAT( p.productName, '-', IFNULL( pv.Version, '' ) );"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arrQualification = JArray.Parse(result);
                // for(int i=0;i<arrQualification.Count;i++){
                //    JArray Contacts= new JArray();
                //     sb = new StringBuilder();
                //     sb.AppendFormat(
                //         "SELECT c.ContactName, c.ContactIdInt FROM Qualifications q, Contacts_All c, SiteContactLinks scl  WHERE"+
                //         " q.ContactId = c.ContactId  AND q.Status <> 'X'  AND scl.Status <> 'X'  AND c.Status <> 'X'"+
                //         " AND c.Status <> 'D'  AND c.ContactId = scl.ContactId  AND scl.SiteId = '"+siteid+"' and q.AutodeskProduct = '"+arrQualification[i]["AutodeskProduct"]+"'"
                //     );
                //     sqlCommand = new MySqlCommand(sb.ToString());
                //     ds = oDb.getDataSetFromSP(sqlCommand);
                //     result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                //     arrContact = JArray.Parse(result);
                //     if(arrContact.Count > 0){
                //         string[] contactarr = new string[arrContact.Count];
                //         string[] contactidarr = new string[arrContact.Count];
                //         for(int j=0;j<arrContact.Count;j++){
                //             Contacts.Add(JObject.Parse(
                //                 @"{""ContactIdInt"":""" + arrContact[j]["ContactIdInt"].ToString() + "\", " +
                //                 @"""ContactName"":""" + arrContact[j]["ContactName"].ToString() + 
                //                 @"""}"));
                //         }
                //         arrQualification[i] = new JObject(
                //             new JProperty("QualificationId", arrQualification[i]["QualificationId"]),
                //             new JProperty("productName", arrQualification[i]["productName"]),
                //             new JProperty("Version", arrQualification[i]["Version"]),
                //             new JProperty("Contacts", Contacts));
                //     }
                //     else{
                //         arrQualification[i] = new JObject(
                //             new JProperty("QualificationId", arrQualification[i]["QualificationId"]),
                //             new JProperty("productName", arrQualification[i]["productName"]),
                //             new JProperty("Version", arrQualification[i]["Version"]),
                //             new JProperty("Contacts", ""));
                //     }
                // }
            }
            catch
            {
                arrQualification = null;
            }
            finally
            {
            }

            return Ok(arrQualification);
        }

        [HttpGet("{email}")]
        public IActionResult WhereEmail(string email)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Contacts_All where EmailAddress = '" + email + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("FirstLogin/{id}")]
        public IActionResult FirstLogin(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select EmailAddress from Contacts_All where ContactId = '" + id + "';update Contacts_All set LoginCount = LoginCount + 1 where ContactId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("FirstLoginStudent/{id}")]
        public IActionResult FirstLoginStudent(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select Email AS EmailAddress from tblStudents where StudentID = '" + id + "';update tblStudents set LoginCount = LoginCount + 1 where StudentID = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ContactAll/{obj}")]
        public IActionResult WhereObjContact(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select ContactId,ContactName,EmailAddress from Contacts_All ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("CountryCode"))
                {
                    query += "CountryCode = '" + property.Value.ToString() + "' ";
                }

                if (property.Name.Equals("ContactId"))
                {
                    query += "ContactId = '" + property.Value.ToString() + "' ";
                }

                i = i + 1;
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("getcontactwithcountsites/{id}")]
        public IActionResult WhereContactCountSites(string id)
        {
            JArray oContact;

            try
            {
                // sb = new StringBuilder();
                // sb.AppendFormat(
                //     "select count(*) as CountSites," +
                //     " c.ContactIdInt, c.ContactId, replace( replace( replace( c.FirstName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as FirstName, replace( replace( replace( c.LastName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as LastName, replace( replace( replace( c.ContactName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as ContactName, c.Telephone1, c.Telephone2, c.Mobile1, c.Mobile2, tz.compName as Timezone, c.Comments, DATE_FORMAT(c.DateLastAdmin, '%m/%d/%Y') AS DateLastAdmin, c.ContactId, c.InstructorId, c.Department, c.EmailAddress, c.EmailAddress2, c.PrimaryLanguage, DATE_FORMAT(c.LastLogin, '%m/%d/%Y') AS LastLogin, c.LastAdminBy, c.DoNotEmail, c.UserLevelId, c.PrimaryRoleId, c.PrimaryIndustryId, c.SecondaryLanguage, tz.compName as Timezone, c.Salutation, " +
                //     " Main_site.SiteId from Contacts_All c left join" +
                //     " SiteContactLinks on c.ContactId = SiteContactLinks.ContactId left join" +
                //     " Main_site on SiteContactLinks.SiteId = Main_site.SiteId left join" +
                //     " Main_organization on Main_site.OrgId = Main_organization.OrgId" +
                //     " LEFT JOIN SabaComponentIds as tz on tz.compId = c.Timezone" + 
                //     " where Main_organization.OrgId = '" + id + "' GROUP BY c.ContactId"
                // );
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);
                // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);

                // oContact = JArray.Parse(result);
                // List<String> arrContactId = new List<String>();

                // foreach (JObject o in oContact.Children<JObject>())
                // {
                //     foreach (JProperty p in o.Properties())
                //     {
                //         if (p.Name.Equals("ContactId"))
                //         {
                //             arrContactId.Add(p.Value.ToString());
                //         }
                //     }
                // }

                // JObject o1;
                // List<String> countContact = new List<String>();

                // for (var i = 0; i < arrContactId.Count; i++)
                // {
                //     sb1 = new StringBuilder();
                //     sb1.AppendFormat("select count(*) as count from SiteContactLinks where ContactId = '" + arrContactId[i] + "' and `Status` = 'A' and SiteId in (select SiteId from Main_site where OrgId = '" + id + "')");
                //     sqlCommand = new MySqlCommand(sb1.ToString());
                //     ds1 = oDb.getDataSetFromSP(sqlCommand);
                //     result2 = MySqlDb.GetJSONObjectString(ds1.Tables[0]);
                //     o1 = JObject.Parse(result2);
                //     countContact.Add(o1["count"].ToString());
                // }

                // int index = 0;
                // foreach (var item in oContact)
                // {
                //     item["CountSites"] = countContact[index];
                //     index++;
                // }

                sb = new StringBuilder();
                sb.AppendFormat(
                    "select c.ContactIdInt, c.ContactId, c.FirstName, c.LastName, c.ContactName, c.Telephone1, c.Telephone2, c.Mobile1, c.Mobile2, tz.compName as Timezone, c.Comments, DATE_FORMAT(c.DateLastAdmin, '%m/%d/%Y') AS DateLastAdmin, c.ContactId, c.InstructorId, c.Department, c.EmailAddress, c.EmailAddress2, c.PrimaryLanguage, DATE_FORMAT(c.LastLogin, '%m/%d/%Y') AS LastLogin, c.LastAdminBy, c.DoNotEmail, c.UserLevelId, c.PrimaryRoleId, c.PrimaryIndustryId, c.SecondaryLanguage, tz.compName as Timezone, c.Salutation, CountSites, SiteId from " +
                    "(select COUNT(SiteId) as CountSites,ContactId,SiteId from SiteContactLinks where SiteId in (select s.SiteId from Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId where s.OrgId = '" + id + "' AND s.Status in ('A','I') AND sr.`Status` NOT IN ('X' , 'D', 'T')) AND `Status`='A' GROUP BY ContactId ) as sc " +
                    "INNER JOIN Contacts_All c on sc.ContactId = c.ContactId AND c.`Status` = 'A' " +
                    "LEFT JOIN SabaComponentIds as tz on tz.compId = c.Timezone order by Firstname"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
                
            }
            catch
            {
                // oContact = null;
                result = "";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            // return Ok(oContact);
            return Ok(result);
        }

        [HttpGet("getcontactwithcountsitesforsites/{id}")]
        public IActionResult WhereContactCountSitesforsites(string id)
        {
            JArray oContact;
            List<dynamic> dynamicList = new List<dynamic>();
            try
            {
                // sb = new StringBuilder();
                // sb.AppendFormat("select ContactIdInt, ContactId, InstructorId, replace( replace( replace( ContactName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as ContactName, EmailAddress from Contacts_All where ContactId in (select ContactId from SiteContactLinks where SiteId = '" + id + "')");
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);
                // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                // oContact = JArray.Parse(result);

                // List<String> arrContactId = new List<String>();

                // foreach (JObject o in oContact.Children<JObject>())
                // {
                //     foreach (JProperty p in o.Properties())
                //     {
                //         if (p.Name.Equals("ContactId"))
                //         {
                //             arrContactId.Add(p.Value.ToString());
                //         }
                //     }
                // }

                // JObject o1;
                // List<String> countContact = new List<String>();
                // JObject o2;
                // List<String> thissite = new List<String>();

                // for (var i = 0; i < arrContactId.Count; i++)
                // {
                //     sb1 = new StringBuilder();
                //     sb1.AppendFormat("select count(*) as count from SiteContactLinks where ContactId = '" + arrContactId[i] + "' and `Status` = 'A'");
                //     sqlCommand = new MySqlCommand(sb1.ToString());
                //     ds1 = oDb.getDataSetFromSP(sqlCommand);
                //     result2 = MySqlDb.GetJSONObjectString(ds1.Tables[0]);
                //     o1 = JObject.Parse(result2);
                //     countContact.Add(o1["count"].ToString());

                //     sb = new StringBuilder();
                //     sb.AppendFormat("select count(*) as count1 from SiteContactLinks where ContactId = '" + arrContactId[i] + "' and SiteId = '" + id + "' and `Status` = 'A'");
                //     sqlCommand = new MySqlCommand(sb.ToString());
                //     ds = oDb.getDataSetFromSP(sqlCommand);
                //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                //     o2 = JObject.Parse(result);
                //     thissite.Add(o2["count1"].ToString());
                // }

                // int index = 0;
                // foreach (var item in oContact)
                // {
                //     item["CountSites"] = countContact[index];
                //     item["ThisSite"] = thissite[index];
                //     index++;
                // }

                sb = new StringBuilder();
                
                // sb.AppendFormat(
                //     "select ContactIdInt, c.ContactId, InstructorId,ContactName, EmailAddress, CountSites from "+
                //     "(select sc2.ContactId, COUNT(sc2.SiteId) as CountSites from "+
                //     "(select ContactId,SiteId from SiteContactLinks where SiteId = '"+id+"' AND `Status` = 'A') as sc "+
                //     "INNER JOIN SiteContactLinks sc2 ON sc.ContactId = sc2.ContactId  AND `Status` = 'A' GROUP BY ContactId) as sc2 "+
                //     "INNER JOIN Contacts_All c ON sc2.ContactId = c.ContactId AND c.`Status` = 'A'"
                // );

                /* fix count sites issue 07092018  */

                sb.AppendFormat(
                    "Select * from (select ContactIdInt, c.ContactId, InstructorId,ContactName, EmailAddress " +
                    ", (SELECT COUNT(DISTINCT sc2.SiteId) FROM SiteContactLinks sc2 LEFT JOIN Main_site s ON sc2.SiteId = s.SiteId LEFT JOIN SiteRoles sr ON sc2.SiteId = sr.SiteId WHERE sc2.ContactId = sc.ContactId AND sc2.`Status` = 'A' AND sr.`Status` NOT IN ('T','X') AND s.`Status` NOT IN ('X','D','Z')) AS CountSites "+
                    "from "+
                    "(select ContactId,SiteId from SiteContactLinks where SiteId = '"+id+"' AND `Status` = 'A') as sc "+
                    "INNER JOIN Contacts_All c ON sc.ContactId = c.ContactId AND c.`Status` = 'A') as tb where CountSites>0 "
                );

                /* fix count sites issue 07092018  */

                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                // oContact = null;
                result = "";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
                // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            // return Ok(oContact);
            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            var o2 = JObject.Parse(obj);
            var o3 = JObject.Parse(obj);
            string query_select = "";
            string query = "";
            string territory = "";

            query_select += "SELECT c.ContactIdInt, c.ContactId, c.ContactName, c.EmailAddress, o.OrganizationId, s.SiteId, rc.countries_name, ( SELECT count( * ) FROM SiteContactLinks sc WHERE c.ContactId = sc.ContactId AND sc.`Status` = 'A' ) AS CountSites ";
            query += "FROM Main_organization o LEFT JOIN Main_site s ON s.OrgId = o.OrgId LEFT JOIN Contacts_All c ON c.PrimarySiteId = s.SiteId LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON c.CountryCode = rc.countries_code ";

            int i = 0;
            JObject json = JObject.FromObject(o1);

            foreach (JProperty property in json.Properties())
            {
                if(property.Name.Equals("UserRoleTerritory")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        territory = property.Value.ToString();
                    }
                }
            }

            foreach (JProperty property in json.Properties())
            {

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.ContactId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("FirstName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.FirstName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("LastName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.LastName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("FirstName2"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.FirstName = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("LastName2"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.LastName = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("EmailAddress"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.EmailAddress = '" + property.Value.ToString() + "' or Username like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("StateProvince"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.StateProvince like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PrimarySiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.PrimarySiteId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "r.RoleName = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.`Status` in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("JustThisOrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.OrgId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query_select += ", (select count(*) from SiteContactLinks sc where c.ContactId = sc.ContactId and s.OrgId = '" + property.Value.ToString() + "') as OnThisSite ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ContryDistributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query_select += ", (select count(*) from ContactCountry cc where c.ContactId = cc.ContactId and cc.CountryCode = '" + property.Value.ToString() + "') as OnThisDistributor ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Region"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.region_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Subregion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.subregion_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.countries_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "rc.TerritoryId in ( "+property.Value.ToString()+" ) "; 
                        i=i+1;
                    }else{
                        if(territory != ""){
                            if(i==0){query += " where ";}
                            if(i>0){query += " and ";} 
                            query += "rc.TerritoryId in ( "+territory+" ) "; 
                        }
                        i = i + 1; 
                    }
                }

            }

            if (i == 0)
            {
                // query += " WHERE (c.ContactId <> null OR c.ContactId <> '') AND c.ShowInSearch = 1";
                query += " WHERE (c.ContactId <> null OR c.ContactId <> '')";
            }
            else
            {
                // query += " AND (c.ContactId <> null OR c.ContactId <> '') AND c.ShowInSearch = 1";
                query += " AND (c.ContactId <> null OR c.ContactId <> '')";
            }

            query += " GROUP BY c.ContactId";

            // Console.WriteLine(query_select + " " + query);

            try
            {
                sb.AppendFormat(query_select + " " + query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("FilterContactNew")]
        public IActionResult FilterContactNew([FromBody] dynamic data)
        {
            string query_select = "";
            string query = "";
            string territory = "";
            string tmpFirstName = "";
            string tmpLastName = "";
            var selectedcoulmns = "";
            int i = 0;
            JObject json = JObject.FromObject(data);

            /* buat kondisi user isi filter contact nya atau enggak */
            Boolean isi = false;
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        isi = true;
                    }
                }

                if (property.Name.Equals("FirstName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        isi = true;
                    }
                }
                
                if (property.Name.Equals("LastName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        isi = true;
                    }
                }

                if (property.Name.Equals("EmailAddress"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        isi = true;
                    }
                }

                if(property.Name.Equals("Distributor")){   
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        isi = true;
                    }
                }
            }
            /* buat kondisi user isi filter contact nya atau enggak */

            // foreach (JProperty property in json.Properties())
            // {
            //     if(property.Name.Equals("UserRoleTerritory")){   
            //         if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
            //             territory = property.Value.ToString();
            //         }
            //     }
            // }

            /* buat add contact affiliate (role distributor juga) */
            var siteid="";
            var orgdistributor="";
            foreach (JProperty property in json.Properties())
            {
                if(property.Name.Equals("SiteId")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        siteid = property.Value.ToString();
                    }
                }
            }

            // foreach (JProperty property in json.Properties())
            // {
            //     if(property.Name.Equals("DistributorNyari")){   
            //         if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
            //             orgdistributor = property.Value.ToString();
            //         }
            //     }
            // }

            /* buat add contact affiliate (role distributor juga) */
            if (json["ContactSearch"] != null)
            {
                query = " select  c.ContactIdInt,c.ContactId,InstructorId,ContactName,EmailAddress,OrganizationId,s.SiteId,countries_name FROM Contacts_All c INNER join SiteContactLinks sc on c.ContactId = sc.ContactId   and (c.UserLevelId <>'' and c.UserLevelId is not null) INNER join Main_site s on sc.SiteId = s.SiteId INNER join Main_organization o on s.OrgId = o.OrgId " +
                                     " INNER join Ref_countries rc on c.CountryCode = rc.countries_code   inner join (select  SiteId, RoleId, max(SiteRoleId) maxid from SiteRoles group by SiteId, RoleId) MaxRole on s.SiteId = MaxRole.SiteId  " +
                                        " INNER JOIN SiteRoles sr ON MaxRole.maxid = sr.SiteRoleId INNER JOIN Roles r ON r.RoleId = sr.RoleId  INNER join MarketType m on rc.MarketTypeId = m.MarketTypeId INNER join Ref_territory t on rc.TerritoryId = t.TerritoryId   Inner join RoleParams rp on rp.RoleParamId =sr.RoleParamId AND sr.`Status` in ( 'A' )  ";

            }
            else
            {
                query =
                    "SELECT ContactIdInt,ContactId,InstructorId,ContactName,EmailAddress,OrganizationId,SiteId,countries_name,CountSites,CountEmail,OnThisSite from ( " + /* group by email (contactid yg tertinggi) & buat add contact affiliate (role distributor juga)*/
                    "SELECT c.ContactIdInt, c.ContactId, CASE WHEN c.InstructorId IN (null,'') THEN 'None' ELSE c.InstructorId END AS InstructorId, c.ContactName, CASE WHEN c.EmailAddress IN (null,'') THEN 'None' ELSE c.EmailAddress END AS EmailAddress, OrganizationId, SiteId, IFNULL(rc.countries_name,'None') AS countries_name, ( SELECT count( * ) FROM SiteContactLinks sc WHERE c.ContactId = sc.ContactId AND sc.`Status` = 'A' ) AS CountSites" +
                    ", ( SELECT COUNT(*) FROM Contacts_All ca WHERE c.EmailAddress = ca.EmailAddress ) AS CountEmail " + /* group by email (contactid yg tertinggi) */
                    ", ( SELECT COUNT(*) from SiteContactLinks scl where sc.ContactId = scl.ContactId and scl.SiteId = '" + siteid + "' and scl.Status <>'X') AS OnThisSite "; /* buat add contact affiliate */

                /* buat add contact (role distributor) */

                // if(orgdistributor != ""){
                //     query +=    
                //         ", ( SELECT count(*) from Main_site s INNER JOIN SiteContactLinks scl ON s.SiteID = scl.SiteId WHERE sc.ContactId = scl.ContactId AND SiteCountryCode IN (SELECT CountryCode FROM SiteCountryDistributor where SiteID IN (SELECT SiteId from Main_site where OrgId IN ("+orgdistributor+")))) AS OnThisSiteDistributor ";
                // }else{
                //     query +=  
                //         ", '' AS OnThisSiteDistributor ";
                // }
                /* buat add contact (role distributor) */

                query +=
                    "from " +
                    "(select ContactId, OrganizationId, s.SiteId, SiteCountryCode, RegisteredCountryCode from " +

                    // "(select s.SiteId,o.OrganizationId, SiteCountryCode, RegisteredCountryCode from Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId AND sr.`Status` IN ('A','B','F','O','P','R','S','T','U','V','X') "; //Pas create new contact kaga muncul di pencarian

                    "(select DISTINCT s.SiteId,o.OrganizationId, SiteCountryCode, RegisteredCountryCode from Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId   Inner join Roles r on sr.RoleId =r.RoleId Inner join RoleParams rp on rp.RoleParamId =sr.RoleParamId ";
            }
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND r.RoleCode = '" + property.Value.ToString() + "' ";
                    }
                }

                if (property.Name.Equals("SubPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                      
                        // query += "o.`Status` in ( "+property.Value.ToString()+" ) "; /* change filter partner type status to status site */
                     //   query += "And (rp.`RoleParamId` in ( " + property.Value.ToString().Replace("\\","") + " )  or  rp.`RoleParamId`= null or  rp.`RoleParamId` ='') ";
                        query += "AND rp.`RoleParamId` in ( " + property.Value.ToString().Replace("\\", "") + " ) ";
                    }
                }

                if (property.Name.Equals("JustThisOrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.OrgId in (" + property.Value.ToString() + ") ";
                    }
                }

                /*ubah filter status ke status partner type */
                if (property.Name.Equals("PartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                         query += "AND sr.`Status` in ( " + property.Value.ToString() + " ) ";
                    }
                }
            }
            if (json["ContactSearch"] == null)
            {
                query +=
                "INNER JOIN Main_organization o ON s.OrgId = o.OrgId ";

                foreach (JProperty property in json.Properties())
                {
                    // if (property.Name.Equals("PartnerTypeStatus"))
                    // {
                    //     if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    //     {
                    //          query += "where s.`Status` in ( " + property.Value.ToString() + " ) ";
                    //     }
                    // }

                    if (property.Name.Equals("SiteAdmin"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            query += "AND s.OrgId IN  ( " + property.Value.ToString() + " ) ";
                        }
                    }
                }

                // query +=
                //     "GROUP BY SiteId) as s "+

                //     // "INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId AND sc.`Status` = 'A') as sc "+ 
                //     "INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId) as sc "+ /* contact yg gak punya affiliate juga minta di tampilin di result search */

                //     // "INNER JOIN Contacts_All c ON sc.ContactId = c.ContactId AND c.`Status` = 'A' AND (c.ContactId <> null OR c.ContactId <> '') ";
                //     "INNER JOIN Contacts_All c ON sc.ContactId = c.ContactId AND (c.ContactId <> null OR c.ContactId <> '') "; /* contact yg gak active juga minta di tampilin di result search */

                /* kalo filter contact gak di isi , cari yg affiliate (mun teu di kieu keun query na lila mun filter teu di isi)*/
                // if(isi){
                //     query +=
                //     "GROUP BY SiteId) as s "+
                //     "INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId) as sc "+
                //     "INNER JOIN Contacts_All c ON sc.ContactId = c.ContactId AND (c.ContactId <> null OR c.ContactId <> '') ";
                // }else{
                //     query +=
                //     "GROUP BY SiteId) as s "+
                //     "INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId AND sc.`Status` = 'A') as sc "+
                //     "INNER JOIN Contacts_All c ON sc.ContactId = c.ContactId AND (c.ContactId <> null OR c.ContactId <> '') ";
                // }
                /* kalo filter contact gak di isi , cari yg affiliate (mun teu di kieu keun query na lila mun filter teu di isi)*/

                /* manggih nu rada gancang, tinggal di group by padahal, lila neang na */
                query +=
                    ") as s " +
                    "INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId GROUP BY sc.ContactId) as sc " + /* contact yg gak punya affiliate juga minta di tampilin di result search */
                    "INNER JOIN Contacts_All c ON sc.ContactId = c.ContactId AND (c.ContactId <> null OR c.ContactId <> '') "; /* contact yg gak active juga minta di tampilin di result search */
                                                                                                                               /* manggih nu rada gancang, tinggal di group by padahal, lila neang na */
            }
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        //query += "AND c.Status <> 'D' AND (c.ContactId like '%" + property.Value.ToString().Trim() + "%') ";
                        query += "AND c.Status <> 'D' AND (c.ContactId like '%" + property.Value.ToString().Trim() + "%' OR c.InstructorId like '%" + property.Value.ToString().Trim() + "%') ";

                    }
                }

                if (property.Name.Equals("FirstName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (string.IsNullOrEmpty(tmpFirstName))
                        {
                            tmpFirstName = "(c.FirstName like '%" + property.Value.ToString().Trim() + "%') ";
                        }
                        //query += "AND c.FirstName like '%" + property.Value.ToString().Trim() + "%' ";
                    }
                }
                if (property.Name.Equals("FirstNameSpecial"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (!string.IsNullOrEmpty(tmpFirstName))
                        {
                            query += "AND (" + tmpFirstName + " or (c.FirstName like '%" + property.Value.ToString().Trim() + "%')) ";
                        }

                    }
                }
                if (property.Name.Equals("LastName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        //query += "AND c.LastName like '%" + property.Value.ToString().Trim() + "%' ";
                        if (string.IsNullOrEmpty(tmpLastName))
                        {
                            tmpLastName = "(c.LastName like '%" + property.Value.ToString().Trim() + "%') ";
                        }
                    }
                }
                if (property.Name.Equals("LastNameSpecial"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (!string.IsNullOrEmpty(tmpLastName))
                        {
                            query += "AND (" + tmpLastName + " or (c.LastName like '%" + property.Value.ToString().Trim() + "%')) ";
                        }

                    }
                }
                if (property.Name.Equals("EmailAddress"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND (c.EmailAddress like '%" + property.Value.ToString().Trim() + "%' or Username like '%" + property.Value.ToString().Trim() + "%') ";
                    }
                }

                if(property.Name.Equals("Distributor")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += 
                            "AND CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" ) GROUP BY o.OrgId) > 0 "+
                            "THEN s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" )) "+ 
                            "ELSE s.SiteCountryCode IN (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( "+property.Value.ToString()+" )) END ";
                    }
                }
            }

            if (json["ContactSearch"] == null)
            {

                query +=
                // "INNER JOIN Ref_countries rc ON c.CountryCode = rc.countries_code  OR sc.SiteCountryCode = rc.countries_code OR sc.RegisteredCountryCode = rc.countries_code ";
                // "INNER JOIN Ref_countries rc ON ( SELECT SiteCountryCode FROM Main_site WHERE SiteId = c.PrimarySiteId ) = rc.countries_code ";
                "and c.Status='A' " +
                "INNER JOIN Ref_countries rc ON ( SELECT SiteCountryCode FROM Main_site WHERE SiteId = CASE WHEN c.PrimarySiteId = '' THEN sc.SiteId ELSE c.PrimarySiteId END ) = rc.countries_code "; /* ada beberapa contact yg gak ada primary site, jadi we hem */
            }
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.countries_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "rc.TerritoryId in ( "+property.Value.ToString()+" ) "; 
                        i=i+1;
                    }
                    // else{
                    //     if(territory != ""){
                    //         if(i==0){query += " where ";}
                    //         if(i>0){query += " and ";} 
                    //         query += "rc.TerritoryId in ( "+territory+" ) "; 
                    //         i = i + 1; 
                    //     }
                    // }
                }
            }
            query += " AND c.`Status` <>'X' ";
            if (json["ContactSearch"] == null)
            {
                query += "GROUP BY c.ContactId " +

               /* group by email (contactid yg tertinggi) */

               "ORDER BY CAST(c.ContactId AS UNSIGNED) DESC " +
               ") AS Contact GROUP BY EmailAddress ORDER BY CAST(ContactId AS UNSIGNED) ASC";

            }
            else
                query += "GROUP BY c.ContactId ";



            /* group by email (contactid yg tertinggi) */

            List <dynamic> dynamicList = new List<dynamic>();

            try
            {
             
               // sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(query);
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    // ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
                // result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("FilterContact")]
        public IActionResult FilterContact([FromBody] dynamic data)
        {
            string query_select = "";
            string query = "";
            string territory = "";

            query_select += "SELECT c.ContactIdInt, c.ContactId, c.InstructorId, c.ContactName, c.EmailAddress, o.OrganizationId, s.SiteId, rc.countries_name, ( SELECT count( * ) FROM SiteContactLinks sc WHERE c.ContactId = sc.ContactId AND sc.`Status` = 'A' ) AS CountSites ";
            // query += "FROM Main_organization o LEFT JOIN Main_site s ON s.OrgId = o.OrgId LEFT JOIN Contacts_All c ON c.PrimarySiteId = s.SiteId LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON c.CountryCode = rc.countries_code ";
            // query += "FROM Main_organization o LEFT JOIN Main_site s ON s.OrgId = o.OrgId LEFT JOIN Contacts_All c ON c.PrimarySiteId = s.SiteId LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code ";
            query += "FROM Main_organization o LEFT JOIN Main_site s ON s.OrgId = o.OrgId LEFT JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId AND sc.`Status` = 'A' LEFT JOIN Contacts_All c ON sc.ContactId = c.ContactId LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code ";

            int i = 0;
            JObject json = JObject.FromObject(data);

            foreach (JProperty property in json.Properties())
            {
                if(property.Name.Equals("UserRoleTerritory")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        territory = property.Value.ToString();
                    }
                }
            }

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("UserId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.ContactId != '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "(c.ContactId like '%" + property.Value.ToString().Trim() + "%' OR c.InstructorId like '%" + property.Value.ToString().Trim() + "%') ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("FirstName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.FirstName like '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("LastName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.LastName like '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("FirstName2"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.FirstName = '" + property.Value.ToString().Trim() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("LastName2"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.LastName = '" + property.Value.ToString().Trim() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("EmailAddress"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "(c.EmailAddress like '%" + property.Value.ToString().Trim() + "%' or Username like '%" + property.Value.ToString().Trim() + "%')";
                        // query += "c.EmailAddress = '" + property.Value.ToString() + "' or Username like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("StateProvince"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.StateProvince like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PrimarySiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.PrimarySiteId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "r.RoleName = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "sr.`Status` in ( " + property.Value.ToString() + " ) ";
                        query += "s.`Status` in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("JustThisOrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.OrgId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query_select += ", (select count(*) from SiteContactLinks sc where c.ContactId = sc.ContactId and s.OrgId = '" + property.Value.ToString() + "') as OnThisSite ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ContryDistributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query_select += ", (select count(*) from ContactCountry cc where c.ContactId = cc.ContactId and cc.CountryCode = '" + property.Value.ToString() + "') as OnThisDistributor ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Region"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.region_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Subregion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.subregion_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.countries_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "rc.TerritoryId in ( "+property.Value.ToString()+" ) "; 
                        i=i+1;
                    }else{
                        if(territory != ""){
                            if(i==0){query += " where ";}
                            if(i>0){query += " and ";} 
                            query += "rc.TerritoryId in ( "+territory+" ) "; 
                            i = i + 1; 
                        }
                    }
                }
            }

            if (i == 0)
            {
                // query += " WHERE (c.ContactId <> null OR c.ContactId <> '') AND c.ShowInSearch = 1";
                query += " WHERE (c.ContactId <> null OR c.ContactId <> '')";
                // query += " WHERE c.ContactId != '5102861' AND (c.ContactId <> null OR c.ContactId <> '')";
            }
            else
            {
                // query += " AND (c.ContactId <> null OR c.ContactId <> '') AND c.ShowInSearch = 1";
                query += " AND (c.ContactId <> null OR c.ContactId <> '')";
                // query += " AND c.ContactId != '5102861' AND (c.ContactId <> null OR c.ContactId <> '')";
            }

            query += " GROUP BY c.ContactId";

            // Console.WriteLine(query_select + " " + query);

            try
            {
                sb.AppendFormat(query_select + " " + query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                result = MySqlDb.SerializeDataTable(ds.Tables[0]);

                //Encoding enc = Encoding.GetEncoding("us-ascii", new CustomMapper(), new DecoderExceptionFallback());

                //string str1 = result;
                //Console.WriteLine(str1);
                //for (int ctr = 0; ctr <= str1.Length - 1; ctr++)
                //{
                //    Console.Write("{0} ", Convert.ToUInt16(str1[ctr]).ToString("X4"));
                //    if (ctr == str1.Length - 1)
                //        Console.WriteLine();
                //}
                //Console.WriteLine();

                //// Encode the original string using the ASCII encoder.
                //byte[] bytes = enc.GetBytes(str1);
                //Console.Write("Encoded bytes: ");
                //foreach (var byt in bytes)
                //    Console.Write("{0:X2} ", byt);

                //Console.WriteLine("\n");

                //// Decode the ASCII bytes.
                //string str2 = enc.GetString(bytes);
                //Console.WriteLine("Round-trip: {0}", str1.Equals(str2));
                //if (!str1.Equals(str2))
                //{
                //    Console.WriteLine(str2);
                //    foreach (var ch in str2)
                //        Console.Write("{0} ", Convert.ToUInt16(ch).ToString("X4"));

                //    Console.WriteLine();
                //}
            }
            return Ok(result);
        }

        [HttpGet("whereGeneral/{obj}")]
        public IActionResult whereGeneral(string obj)
        {
            var o1 = JObject.Parse(obj);
            var o2 = JObject.Parse(obj);
            var o3 = JObject.Parse(obj);
            string query_select = "";
            string query = "";

            query_select += "SELECT c.ContactIdInt, c.ContactId, c.ContactName, c.EmailAddress, o.OrganizationId, s.SiteId, rc.countries_name, ( SELECT count( * ) FROM SiteContactLinks sc WHERE c.ContactId = sc.ContactId AND sc.`Status` = 'A' ) AS CountSites ";
            query += "FROM Main_organization o LEFT JOIN Main_site s ON s.OrgId = o.OrgId LEFT JOIN Contacts_All c ON c.PrimarySiteId = s.SiteId LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON c.CountryCode = rc.countries_code ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {

                if (property.Name.Equals("id_contact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "c.ContactId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("name_contact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "c.ContactName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("email_contact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "c.EmailAddress like '%" + property.Value.ToString() + "%' or Username like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("notes_contact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "c.Bio like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
            }

            if (i == 0)
            {
                // query += " WHERE (c.ContactId <> null OR c.ContactId <> '') AND c.ShowInSearch = 1";
                query += " WHERE (c.ContactId <> null OR c.ContactId <> '')";
            }
            else
            {
                // query += " AND (c.ContactId <> null OR c.ContactId <> '') AND c.ShowInSearch = 1";
                query += " AND (c.ContactId <> null OR c.ContactId <> '')";
            }

            query += " GROUP BY c.ContactName";

            try
            {
                sb.AppendFormat(query_select + " " + query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("GetDistContact/where/{obj}")]
        public IActionResult WhereObjDistributorContact(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            // query += "select ContactIdInt,Contacts_All.ContactId,InstructorId,RoleName,ContactName,FirstName,LastName,LastLogin,EmailAddress,PrimaryD, count(ContactCountry.ContactId) as Affiliate "
            // + "from Contacts_All join ContactCountry on ContactCountry.ContactId = Contacts_All.ContactId left join Roles on Contacts_All.PrimaryRoleId = Roles.RoleId ";

            query += 
                "SELECT c.ContactIdInt, c.ContactId, c.InstructorId, r.RoleName, c.ContactName, LastLogin, EmailAddress, cc.PrimaryD, "+
                "(SELECT Count(scl.SiteId) From SiteContactLinks scl, Main_site s WHERE scl.ContactId = c.ContactId AND scl.SiteId = s.SiteId AND scl.Status <> 'X' AND s.Status <> 'X' AND s.Status <> 'D') As Affiliate, "+ 
                "(SELECT CAST(GROUP_CONCAT(DISTINCT iscl.SiteId ORDER BY iscl.SiteId SEPARATOR ', ') As CHAR(5000)) FROM SiteContactLinks iscl, Main_site ss WHERE iscl.SiteId = ss.SiteId AND ss.OrgId = s.OrgId AND iscl.ContactId = c.ContactId AND iscl.Status <> 'X' AND ss.Status = 'A') As RelatedSites "+
                "FROM SiteContactLinks scl, Main_site s, Contacts_All c, ContactCountry cc, Roles r "+
                "WHERE c.ContactId = scl.ContactId and c.ContactId = cc.ContactId and c.PrimaryRoleId = r.RoleId AND scl.SiteId = s.SiteId AND scl.Status = 'A' "+
                "AND c.Status <> 'X' AND c.Status <> 'D' AND s.Status <> 'D' AND s.Status <> 'X' ";

            int i = 0;
            JObject json = JObject.FromObject(o1);

            foreach (JProperty property in json.Properties())
            {
                // if (i == 0)
                // {
                //     query += " where ";
                // }
                // if (i > 0)
                // {
                //     query += " and ";
                // }

                if (property.Name.Equals("CountryCode"))
                {
                    // query += "ContactCountry.CountryCode = '" + property.Value.ToString() + "' ";
                    query += "AND cc.CountryCode = '" + property.Value.ToString() + "' ";
                }

                i = i + 1;
            }

            // query += "GROUP BY Contacts_All.ContactId ORDER BY Contacts_All.ContactName";
            query += 
                "GROUP BY c.ContactId ORDER BY c.ContactName";

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("GetContactCountry/where/{obj}")]
        public IActionResult WhereContactCountry(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select * from ContactCountry";

            int i = 0;
            JObject json = JObject.FromObject(o1);

            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("CountryCode"))
                {
                    query += "ContactCountry.CountryCode = '" + property.Value.ToString() + "' ";
                }

                i = i + 1;
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            JObject o1;
            JObject o2;
            string res;
            JArray sites;

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Contacts_All");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            count3 = count1 + 1;

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Contacts_All (ContactId, FirstName, LastName, ContactName, EmailAddress, UserLevelId) " +
                    "values ('" + count3 + "','" + data.FirstName + "','" + data.LastName + "','" + data.ContactName + "','" + data.EmailAddress + "','" + data.UserLevelId + "'); "
                );
                //sb.AppendFormat(
                //    "insert into Contacts_All (ContactId, FirstName, LastName, ContactName, EmailAddress, UserLevelId) " +
                //    "values ('" + count3 + "','" + data.FirstName + "','" + data.LastName + "','" + data.ContactName + "','" + data.EmailAddress + "','" + data.UserLevelId + "'); "
                //);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("select * from Main_site where OrgId in (select OrgId from Main_site where SiteId = '" + data.SiteId + "')");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                sites = JArray.Parse(result);

                for (int i = 0; i < sites.Count; i++)
                {
                    if (sites[i]["SiteId"] == data.SiteId)
                    {
                        sb1 = new StringBuilder();
                        sb1.AppendFormat(
                            "insert into SiteContactLinks " +
                            "(Status, DateAdded, AddedBy, SiteId, ContactId) " +
                            "values ( 'A','" + data.DateAdded + "','" + data.AddedBy + "','" + sites[i]["SiteId"] + "','" + count3 + "');"
                        );
                        sqlCommand = new MySqlCommand(sb1.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }
                    else
                    {
                        sb1 = new StringBuilder();
                        sb1.AppendFormat(
                            "insert into SiteContactLinks " +
                            "(Status, DateAdded, AddedBy, SiteId, ContactId) " +
                            "values ( 'X','" + data.DateAdded + "','" + data.AddedBy + "','" + sites[i]["SiteId"] + "','" + count3 + "');"
                        );
                        sqlCommand = new MySqlCommand(sb1.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }
                }

            }
            catch { }
            finally { }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Contacts_All");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());

            sb = new StringBuilder();
            sb.AppendFormat("select * from Contacts_All where ContactId =" + count3);
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            if (count2 > count1)
            {
                JObject OldData = new JObject();
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, data.UserId.ToString(), data.cuid.ToString(), " where ContactId = " + count3 + "");

                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
            }
            else
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpPost("ContactDistributor")]
        public IActionResult InsertContact([FromBody] dynamic data)
        {
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            JObject o1;
            JObject o2;
            string res;

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from ContactCountry");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            count3 = count1 + 1;

            try
            {
                sb1 = new StringBuilder();
                sb1.AppendFormat(
                    "insert into ContactCountry (CountryCode,ContactId,PrimaryD) " +
                    "values ('" + data.CountryCode + "','" + data.ContactId + "', 0);"
                );
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
            }
            catch { }
            finally { }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from ContactCountry");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());

            if (count2 > count1)
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
            }
            else
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpPut("ContactCountry/where/{obj}")]
        public IActionResult UpdateContactCountry(string obj, [FromBody] dynamic data)
        {

            JObject o3;
            var o1 = JObject.Parse(obj);
            string query = "";

            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("ContactId"))
                {
                    query += "ContactId ='" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("CountryCode"))
                {
                    query += "CountryCode = '" + property.Value.ToString() + "' ";
                }

                i = i + 1;
            }

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update ContactCountry set " +
                    "ContactId='" + data.ContactId + "'," +
                    "CountryCode='" + data.CountryCode + "'," +
                    "PrimaryD='" + data.PrimaryD + "' " +
                    query
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from ContactCountry " + query);
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);

            if (o3["ContactId"].ToString() == data.ContactId.ToString())
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpPost("{id}")]
        public IActionResult Update(string id, [FromBody] dynamic data)
        {

            #region olddata_beforeupdate
            sb = new StringBuilder();
            sb.AppendFormat("select * from Contacts_All where ContactIdInt =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion

            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            JObject o1;
            string instructor_id;

            if(data.IsInstructor == true && data.InstructorId == ""){
                sb = new StringBuilder();
                
                sb.AppendFormat("SELECT MAX(CAST(InstructorId AS UNSIGNED)) + 1 AS LastInstructor FROM Contacts_All");
                // sb.AppendFormat("SELECT LPAD(CAST(InstructorId AS UNSIGNED) + 1,6,'0') AS LastInstructor FROM Contacts_All WHERE ContactIdInt = ( SELECT MAX( ContactIdInt ) FROM Contacts_All WHERE InstructorId <> '' OR InstructorId <> NULL ) ");
                
                /* generate instructorid based updated contact to be instructor */
                // sb.AppendFormat("SELECT DISTINCT LPAD(CAST(InstructorId AS UNSIGNED) + 1,6,'0') AS LastInstructor FROM Contacts_All WHERE InstructorId = ( SELECT MAX( CAST(InstructorId AS UNSIGNED) ) FROM (select InstructorId FROM Contacts_All where CHAR_LENGTH(TRIM(InstructorId)) > 5) as Instructor )");

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o1 = JObject.Parse(result);
                instructor_id = o1["LastInstructor"].ToString();
            } else{
                instructor_id = data.InstructorId;
            }

            try
            {
                string comments = data.Comments.ToString();
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Contacts_All set "
                    + "Salutation='" + data.Honorific + "', "
                    + "InstructorId='" + instructor_id + "', "
                    + "FirstName='" + data.FirstName + "', "
                    + "LastName='" + data.LastName + "', "
                    + "ContactName='" + data.FirstName + " " + data.LastName + "', "
                    + "EmailAddress='" + data.EmailAddress + "', "
                    + "EmailAddress2='" + data.EmailAddress2 + "', "
                    + "UserLevelId='" + data.UserLevelId + "', "
                    + "PrimaryRoleId='" + data.PrimaryRoleId + "', "
                    + "PrimaryIndustryId='" + data.PrimaryIndustryId + "', "
                    + "Timezone='" + data.Timezone + "', "
                    + "Mobile1='" + data.Mobile1 + "', "
                    + "Mobile2='" + data.Mobile2 + "', "
                    + "Telephone1='" + data.Telephone1 + "', "
                    + "Telephone2='" + data.Telephone2 + "', "
                    + "PrimaryLanguage='" + data.PrimaryLanguage + "', "
                    + "SecondaryLanguage='" + data.SecondaryLanguage + "', "
                    + "LastAdminBy='" + data.LastAdminBy + "', "
                    + "DoNotEmail='" + data.DoNotEmail + "', "
                    + "DateLastAdmin=NOW(), "
                    + "Designation='" + data.Designation + "', "
                    + "Comments='" + comments + "' "
                    + "Where ContactIdInt=" + Convert.ToInt32(id)
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Update Data Failed\"" +
                    "}";
            }
            finally
            {

                #region olddata_afterupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from Contacts_All where ContactIdInt =" + id + ";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                #endregion

                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, data.UserId.ToString(), data.cuid.ToString()," where ContactIdInt =" + id + "");

                //string description = "Update data Contact <br/>" +
                //        "ContactId = "+ data.ContactId +" <br/>" +
                //        "Email Address = "+ data.EmailAddress +" <br/>" +
                //        "Contact Name = "+ data.ContactName;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }
            return Ok(res);
        }

        [HttpPut("ProfileEIM/{id}")]
        public IActionResult UpdateProfile(string id, [FromBody] dynamic data)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Contacts_All set "
                    + "FirstName='" + data.FirstName + "', "
                    + "LastName='" + data.LastName + "', "
                    + "Gender='" + data.Gender + "', "
                    + "PrimaryIndustryId='" + data.PrimaryIndustryId + "', "
                    + "Address1='" + data.Address1 + "', "
                    + "EmailAddress='" + data.EmailAddress + "', "
                    + "Address2='" + data.Address2 + "', "
                    + "Address3='" + data.Address3 + "', "
                    + "Bio='" + data.Bio + "', "
                    + "City='" + data.City + "', "
                    + "Mobile1='" + data.Mobile1 + "', "
                    + "PostalCode='" + data.PostalCode + "', "
                    + "PrimaryLanguage='" + data.PrimaryLanguage + "', "
                    + "SecondaryLanguage='" + data.SecondaryLanguage + "', "
                    + "Salutation='" + data.Salutation + "', "
                    + "StateProvince='" + data.StateProvince + "', "
                    + "Telephone1='" + data.Telephone1 + "', "
                    + "WebsiteUrl='" + data.WebsiteUrl + "' "
                    + "Where ContactIdInt='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch { }
            finally { }

            sb = new StringBuilder();
            sb.AppendFormat("select * from Contacts_All where ContactIdInt =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);
            if (o3["FirstName"].ToString() == data.FirstName.ToString())
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpPost("Delete")]
        public IActionResult Delete([FromBody] dynamic data)
        {
            JObject o3;

            #region olddata_beforeupdate
            sb = new StringBuilder();
            sb.AppendFormat("select * from Contacts_All where ContactId = " + data.id);
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion


            string res = "";
            try
            {
                sb = new StringBuilder();
                //sb.AppendFormat("delete from Contacts_All where ContactIdInt = '" + id + "'");
                sb.AppendFormat("update Contacts_All set Status='X' where ContactId = " + data.id);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            {
                sb = new StringBuilder();
                //sb.AppendFormat("select * from Contacts_All where ContactIdInt  =" + id + ";");
                sb.AppendFormat("select * from Contacts_All where ContactId  =" + data.id + " and Status<>'X';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);



                #region newdata_afterupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from Contacts_All where ContactId  =" + data.id + "");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                #endregion


                if (o3.Count < 1)
                {
                    string description = "Deleted data Contact <br/>" +
                        "ContactId = " + data.id;

                    _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, data.UserId.ToString(), data.cuid.ToString(), " where ContactId  =" + data.id + "");
                   // _audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                }
                else
                {
                     res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Delete Data Failed\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        [HttpDelete("ContactCountry/{id}")]
        public IActionResult DeleteContactCountry(string id)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("delete from ContactCountry where ContactId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from ContactCountry where ContactId  =" + id + ";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                if (o3.Count < 1)
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        [HttpPost("FilterGeneral")]
        public IActionResult FilterGeneral([FromBody] dynamic data)
        {
            string query = "";

            int i = 0;
            JObject json = JObject.FromObject(data);

            query = 
                "SELECT c.ContactIdInt, c.ContactId, c.InstructorId, c.ContactName, c.EmailAddress, OrganizationId, SiteId, rc.countries_name, ( SELECT count( * ) FROM SiteContactLinks sc WHERE c.ContactId = sc.ContactId AND sc.`Status` = 'A' ) AS CountSites from "+
                "(select ContactId, OrganizationId, s.SiteId, SiteCountryCode, RegisteredCountryCode from "+
                "(select s.SiteId,o.OrganizationId, SiteCountryCode, RegisteredCountryCode from Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId AND sr.`Status`  in ( 'A','V','X','I','O','P','R','B','S','T' ) " +
                "INNER JOIN Main_organization o ON s.OrgId = o.OrgId "+
                "GROUP BY SiteId) as s "+
                "INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId AND sc.`Status` = 'A') as sc "+
                "INNER JOIN Contacts_All c ON sc.ContactId = c.ContactId AND c.`Status` = 'A' AND (c.ContactId <> null OR c.ContactId <> '') ";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("id_contact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " AND ( "; }
                        if (i > 0) { query += " or "; }
                        query += "c.ContactId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("name_contact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " AND ( "; }
                        if (i > 0) { query += " or "; }
                        query += "c.ContactName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("name_contact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " AND ( "; }
                        if (i > 0) { query += " or "; }
                        query += "c.ContactName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                
                if (property.Name.Equals("email_contact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " AND ( "; }
                        if (i > 0) { query += " or "; }
                        query += "c.EmailAddress like '%" + property.Value.ToString() + "%' or Username like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("notes_contact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " AND ( "; }
                        if (i > 0) { query += " or "; }
                        query += "c.Bio like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
            }

            query += " ) ";

            foreach (JProperty property in json.Properties())
            {
                if(property.Name.Equals("Distributor")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND c.CountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN  ( "+property.Value.ToString()+" ) ) "; 
                    }
                }
            }

            query +=
                "INNER JOIN Ref_countries rc ON c.CountryCode = rc.countries_code  OR sc.SiteCountryCode = rc.countries_code OR sc.RegisteredCountryCode = rc.countries_code "+
                "GROUP BY c.EmailAddress";

            // Console.WriteLine(query);

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

    }
}