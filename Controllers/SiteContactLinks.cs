using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using Newtonsoft.Json;
// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/SiteContactLinks")]
    public class SiteContactLinks : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        String result1;
        StringBuilder sb2 = new StringBuilder();
        DataSet ds2 = new DataSet();
        String result2;

        public Audit _audit;//= new Audit();


        public SiteContactLinks(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            _audit=new Audit(oDb);
        }
        // [HttpGet]
        // public IActionResult Select()
        // {
        //     try
        //     {
        //         sb = new StringBuilder();
        //         sb.Append("select * from SiteContactLinks ");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "{}";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
        //     }

        //     return Ok(result);                  
        // }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from SiteContactLinks where SiteContactLinksId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 
        
        

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from SiteContactLinks ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("SiteId")){   
                    query += "SiteId = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("ContactId")){   
                    query += "ContactId = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPost("InsertSiteContact")]
        public IActionResult InsertSiteContact([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from SiteContactLinks;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into SiteContactLinks "+
                    "(Status, DateAdded, AddedBy, SiteId, ContactId) "+
                    "values ('A','"+data.DateAdded+"','ADMIN','"+data.SiteId+"','"+data.ContactId+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from SiteContactLinks;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
            
        }

            JObject contact;
        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {

           

            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";   
            
            try
            {  
                string query = "";

                if(data.ContactIdInt.ToString() == ""){
                    query = "select ContactId, ContactIdInt from Contacts_All where ContactIdInt in (SELECT MAX(ContactIdInt) from Contacts_All)";
                }else{
                    query = "select ContactId, ContactIdInt from Contacts_All where ContactIdInt = '"+data.ContactIdInt+"'";
                }

                sb1 = new StringBuilder();
                sb1.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
                result1 = MySqlDb.GetJSONObjectString(ds1.Tables[0]);
                contact = JObject.Parse(result1);

                for(int j=0; j<data.SiteIdArr.Count;j++){ 
                    if(data.SiteIdArr[j][2] == "InsertNew"){

                    

                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "insert into SiteContactLinks "+
                            "(Status, DateAdded, AddedBy, SiteId, ContactId) "+
                            "values ( '"+data.Status[j]+"',NOW(),'"+data.AddedBy+"','"+data.SiteIdArr[j][0]+"','"+contact["ContactId"]+"');"
                        );
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        //string description = "Update data Site Affiliate Contact <br/>" +
                        //"SiteId = " + data.SiteIdArr[j][0] + " <br/>" +
                        //"Status = " + data.Status[j] + " <br/>" +
                        //"ContactId = " + contact["ContactId"];
                        //if (data.UserId != null && data.cuid != null)
                        //{
                        //    _audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                        //}

                    }
                    else{

                        #region olddata_beforeupdate
                        sb = new StringBuilder();
                        sb.AppendFormat("select * from SiteContactLinks where SiteContactLinkId ='" + data.SiteIdArr[j][2] + "';");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                        JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                        #endregion



                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "update SiteContactLinks set "+  
                            "`Status`='"+data.Status[j]+"', "+ 
                            "AddedBy='"+data.AddedBy+"', "+ 
                            "SiteId='"+data.SiteIdArr[j][0]+"', "+ 
                            "ContactId='"+contact["ContactId"]+"' "+ 
                            "where SiteContactLinkId='"+data.SiteIdArr[j][2]+"'"
                        );
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        #region newdata_afterupdate
                        sb = new StringBuilder();
                        sb.AppendFormat("select * from SiteContactLinks where SiteContactLinkId ='" + data.SiteIdArr[j][2] + "';");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                        JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                        #endregion

                        


                        if (data.UserId != null && data.cuid != null)
                        {
                            _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.SiteContactLinks, data.UserId.ToString(), data.cuid.ToString(), " where SiteContactLinkId = " + data.SiteIdArr[j][2] + "");
                            //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                        }
                    }
                }

                if(data.PrimarySiteId != "" && data.PrimarySiteId != null){

                    #region olddata_beforeupdate
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Contacts_All where ContactIdInt ='" + data.ContactIdInt + "';");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                    #endregion




                    sb2 = new StringBuilder();
                    sb2.AppendFormat(
                        "update Contacts_All set "+  
                        "PrimarySiteId='"+data.PrimarySiteId+"' "+ 
                        "where ContactIdInt='"+data.ContactIdInt+"'"
                    ); 
                    sqlCommand = new MySqlCommand(sb2.ToString());
                    ds2 = oDb.getDataSetFromSP(sqlCommand);
                    result2 = MySqlDb.GetJSONObjectString(ds2.Tables[0]);


                    #region olddata_afterupdate
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Contacts_All where ContactIdInt ='" + data.ContactIdInt + "';");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                    #endregion

     

                    //string description = "Update data Primary Site Contact <br/>" +
                    //    "Primary SiteId = "+ data.PrimarySiteId +" <br/>" +
                    //    "ContactId = "+ contact["ContactId"];

                    if (data.UserId != null && data.cuid != null)
                    {
                        _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Main_site, data.UserId.ToString(), data.cuid.ToString(), " where ContactIdInt =" + data.ContactIdInt + "");
                        //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                    }
                }
                
            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            { 

                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"ContactIdInt\":\""+contact["ContactIdInt"].ToString()+"\","+
                    "\"message\":\"Update Data Success\""+
                    "}";  
            }
            
            return Ok(res);  
        }

        [HttpPost("Affiliate")]
        public IActionResult InsertAffiliate([FromBody] dynamic data)
        {    
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            #region olddata_beforeupdate
            sb = new StringBuilder();
            sb.AppendFormat("select * from SiteContactLinks where ContactId = '" + data.ContactAffiliate + "' and SiteId = '" + data.SiteId + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion

            try
            {  

                /* reaffiliate issue 11092018 */

                sb = new StringBuilder();
                sb.AppendFormat(
                    "select count(*) as c from SiteContactLinks where ContactId = '"+data.ContactAffiliate+"' and SiteId = '"+data.SiteId+"'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                string resulttmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject o1 = JObject.Parse(resulttmp);
                int count = Int32.Parse(o1["c"].ToString());


               


                if (count > 0){
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "update SiteContactLinks set `Status` = 'A' where ContactId = '"+data.ContactAffiliate+"' and SiteId = '"+data.SiteId+"'"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }else{
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "insert into SiteContactLinks "+
                        "(Status, DateAdded, AddedBy, SiteId, ContactId) "+
                        "values ( 'A',NOW(),'"+data.AddedBy+"','"+data.SiteId+"','"+data.ContactAffiliate+"');"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }

                /* reaffiliate issue 11092018 */

                // sb = new StringBuilder();
                // sb.AppendFormat(
                //     "insert into SiteContactLinks "+
                //     "(Status, DateAdded, AddedBy, SiteId, ContactId) "+
                //     "values ( 'A',NOW(),'"+data.AddedBy+"','"+data.SiteId+"','"+data.ContactAffiliate+"');"
                // );
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);
                
            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            {

                #region newdata_afterupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from SiteContactLinks where ContactId = '" + data.ContactAffiliate + "' and SiteId = '" + data.SiteId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                #endregion

                //string description = "Update data Affiliate Site Contact <br/>" +
                //        "SiteId = "+ data.SiteId +" <br/>" +
                //        "ContactId = "+ data.ContactAffiliate;

                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, data.UserId.ToString(), data.cuid.ToString(), " where ContactId = " + data.ContactAffiliate + " and SiteId = " + data.SiteId + "");
                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}";  
            }
            
            return Ok(res);  
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] dynamic data)
        {  
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
            try
            {  
                for(int i=0; i<data.ContactAffiliate.Count;i++){
                    if(data.ContactAffiliate[i][1] == true){
                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "update SiteContactLinks set "+  
                            "Status='A', "+  
                            "LastAdminBy='"+data.AddedBy+"', "+ 
                            "DateLastAdmin=NOW() "+ 
                            "where SiteContactLinkId='"+data.ContactAffiliate[i][2]+"'"
                        ); 
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        /* pas checked aja masuk nya ya */

                        if(data.ContactAffiliate[i][3] == "Yes"){
                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "insert into SiteContactLinks "+
                            "(Status, DateAdded, AddedBy, SiteId, ContactId) "+
                            "values ( 'A',NOW(),'"+data.AddedBy+"','"+data.SiteId+"','"+data.ContactAffiliate[i][0]+"');"
                        );
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        /* pas checked aja masuk nya ya */
                    }
                    }
                    else{
                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "update SiteContactLinks set "+  
                            "Status='X', "+  
                            "LastAdminBy='"+data.AddedBy+"', "+ 
                            "DateLastAdmin=NOW(), "+ 
                            "DeletedBy='"+data.AddedBy+"', "+ 
                            "DateDeleted=NOW() "+ 
                            "where SiteContactLinkId='"+data.ContactAffiliate[i][2]+"'"
                        );                     
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }

                    // if(data.ContactAffiliate[i][3] == "Yes"){
                    //     sb = new StringBuilder();
                    //     sb.AppendFormat(
                    //         "insert into SiteContactLinks "+
                    //         "(Status, DateAdded, AddedBy, SiteId, ContactId) "+
                    //         "values ( 'A',NOW(),'"+data.AddedBy+"','"+data.SiteId+"','"+data.ContactAffiliate[i][0]+"');"
                    //     );
                    //     sqlCommand = new MySqlCommand(sb.ToString());
                    //     ds = oDb.getDataSetFromSP(sqlCommand);
                    // }
                }

                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            }
            catch
            { 
            }
            finally
            {  
            }
 
            return Ok(res);  
        } 

        
        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // { 
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("delete from SiteContactLinks where SiteContactLinksId = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //     }
        //     finally
        //     { 
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from SiteContactLinks where SiteContactLinksId  ="+id+";");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
            
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //         o3 = JObject.Parse(result);  
        //         Console.WriteLine(o3.Count);
        //         if(o3.Count < 1){ 
        //             res =
        //                 "{"+
        //                 "\"code\":\"1\","+
        //                 "\"message\":\"Delete Data Success\""+
        //                 "}"; 
        //         }
        //     }

        //     return Ok(res);
        // } 

         


    }
}