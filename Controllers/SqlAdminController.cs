﻿using autodesk.Code;
using autodesk.Code.SqlAdminServices;
using autodesk.Code.SqlAdminServices.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;

namespace autodesk.Controllers
{
    [Produces("application/json")]
    [Route("api/SqlAdmin")]
    public class SqlAdminController : ControllerBase
    {
        private readonly ISqlAdminServices _sqlAdminServices;

        public SqlAdminController(ISqlAdminServices sqlAdminServices)
        {
            _sqlAdminServices = sqlAdminServices;
        }

        [HttpGet("GetTables")]
        public async Task<ResponseModel<List<TableDto>>> GetTables()
        {
            var emailClaim = HttpContext.User.HasClaim(x => x.Type == ClaimTypes.Email);
            if (emailClaim)
            {
                var email = HttpContext.User.FindFirst(x => x.Type == ClaimTypes.Email).Value;
                var result = await Task.Run(() => _sqlAdminServices.GetTables(email));

                return new ResponseModel<List<TableDto>> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = result };
            }
            return new ResponseModel<List<TableDto>> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.NotFound, Data = null };
        }
        [HttpPost("GenerateOTPToken")]
        public async Task<ResponseModel> GenerateOTPToken()
        {
            var emailClaim = HttpContext.User.HasClaim(x => x.Type == ClaimTypes.Email);
            if (emailClaim)
            {
                var email = HttpContext.User.FindFirst(x => x.Type == ClaimTypes.Email).Value;
                try
                {
                    var result = await Task.Run(() => _sqlAdminServices.GenerateOTP(email));
                    if (result)
                    {
                        return new ResponseModel { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success };

                    }
                    return new ResponseModel { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Fail };

                }
                catch (System.Exception ex)
                {
                    return new ResponseModel { Code = (int)HttpStatusCode.InternalServerError, Message = AutodeskConst.ResponseMessage.Error };
                }
            }
            return new ResponseModel { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.NotFound };
        }
        [HttpPost("ExecuteQuery")]
        public async Task<ResponseModel<object>> ExecuteQuery([FromBody] QueryDto queryDto)
        {
            try
            {
                var result = await Task.Run(() => _sqlAdminServices.ExecuteQuery(queryDto));
                if (result != null)
                {
                    if (result.Status == AutodeskConst.ResponseMessage.OTPInvalid)
                    {
                        return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = "Invalid sercurity code", Data = null };
                    }
                    if (result.Status == AutodeskConst.ResponseMessage.SAUnauthorized)
                    {
                        return new ResponseModel<object> { Code = (int)HttpStatusCode.Unauthorized, Message = "Unauthorized access", Data = null };
                    }

                    return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = result };
                }
                return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.NotFound, Data = null };

            }
            catch (System.Exception ex)
            {

                return new ResponseModel<object> { Code = (int)HttpStatusCode.InternalServerError, Message = AutodeskConst.ResponseMessage.Error, Data = null };
            }

        }
    }
}