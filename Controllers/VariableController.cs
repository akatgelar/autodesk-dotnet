using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/Variable")]
    public class VariableController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit;//= new Audit();

        public VariableController(MySqlDb sqlDb)
        {
            
            oDb = sqlDb;
            _audit = new Audit(oDb);
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Variable ORDER BY variable_value ASC");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        

        [HttpGet("{id}/{parent}")]
        public IActionResult WhereId(string id, string parent)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Dictionary where `Key` = '"+id+"' and Parent = '"+parent+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from Dictionary ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("Parent")){   
                    query += "Parent = '"+property.Value.ToString()+"' "; 
                }

                if(property.Name.Equals("Status")){   
                    query += "`Status` = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            query += " ORDER BY KeyValue ASC "; 

            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Dictionary;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Dictionary "+
                    "(Parent, `Key`, KeyValue, `Status`) "+
                    "values ('"+data.variable_category+"','"+data.variable_key+"','"+data.variable_value+"','A');"
                );
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Dictionary;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            sb = new StringBuilder();
            sb.AppendFormat("select * from Dictionary where `Key`='"+ data.variable_key + " and Parent='" + data.variable_category + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            if (count2 > count1)
            {
                //string description = "Insert data Variable" + "<br/>" +
                //    "Parent = "+ data.variable_category + "<br/>" +
                //    "Key = "+ data.variable_key + "<br/>" +
                //    "Key Value = "+ data.variable_value;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Dictionary, data.UserId.ToString(), data.cuid.ToString(), "where `Key` =" + data.variable_key + "' and Parent = '" + data.variable_category + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }
 
            } 
 
            return Ok(res);  
            
        }


        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] dynamic data)
        {  
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from Dictionary where `Key` =" + id + " and Parent = '"+data.variable_category+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Dictionary set "+
                    "`Key`='"+data.variable_key+"',"+
                    "KeyValue='"+data.variable_value+"',"+
                    "`Status`='A' "+
                    "where `Key`='"+id+"' and Parent = '"+data.variable_category+"'"
                );
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from Dictionary where `Key` = '"+data.variable_key+"' and Parent = '"+data.variable_category+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 
            //string descriptionU = "Update data Variable" + "<br/>" +
            //        "Parent = "+ data.variable_category + "<br/>" +
            //        "Key = "+ data.variable_key + "<br/>" +
            //        "Key Value = "+ data.variable_value;

            //    _audit.ActionPost(data.UserId.ToString(), descriptionU, data.cuid.ToString());
            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Dictionary, data.UserId.ToString(), data.cuid.ToString(), "where `Key` ="+data.variable_key+"' and Parent = '"+data.variable_category+ ""))
            {
                res =
                 "{" +
                 "\"code\":\"1\"," +
                 "\"message\":\"Update Data Success\"" +
                 "}";
            }
            //} 
 
            return Ok(res);  
        } 



        // [HttpDelete("{parent}/{id}")]
        // public IActionResult Delete(string parent,string id)
        [HttpGet("SoftDelete/{parent}/{id}/{cuid}/{UserId}")]
        public IActionResult SoftDelete(string parent, string id, string cuid, string UserId)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from Dictionary where `Key`  ='" + id + "' and Parent = '" + parent + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                // sb = new StringBuilder();
                // sb.AppendFormat("delete from Dictionary where `Key` = '"+id+"' and Parent = '"+parent+"'");
                // Console.WriteLine(sb.ToString());
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("update Dictionary set `Status`='X' where `Key` = '"+id+"' and Parent = '"+parent+"'");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from Dictionary where `Key`  ='"+id+"' and Parent = '"+parent+"';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                
                if(o3["Status"].ToString() == "X"){
                    //string description = "Delete data Variable" + "<br/>" +
                    //"Variable ID = "+ id + "<br/>" +
                    //"Parent = "+ parent;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());
                    JObject NewData = new JObject();
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Dictionary, UserId.ToString(), cuid.ToString(), "where `Key` =" + id + "' and Parent = '" + parent + ""))
                    {
                        res =
                         "{" +
                         "\"code\":\"1\"," +
                         "\"message\":\"Delete Data Success\"" +
                         "}";
                    }
                }
            }

            return Ok(res);
        } 


    }
}
