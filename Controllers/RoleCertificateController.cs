using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.Models;
using autodesk.Code.AuditLogServices;
// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/RoleCertificate")]
    public class RoleCertificate : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        private readonly MySQLContext _mySQLContext;
        private readonly IAuditLogServices _auditLogServices;
        public RoleCertificate(MySqlDb sqlDb, MySQLContext mySQLContext, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _mySQLContext = mySQLContext;
            _auditLogServices = auditLogServices;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from RoleCertificate ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

         

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from RoleCertificate where RoleCertificateId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 
        
        

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            // query += "select * from RoleCertificate join Dictionary on CertificateType = KeyValue";
            query += "SELECT * FROM Dictionary ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("RoleCode")){   
                    query += "RoleCode = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("Parent")){   
                    query += "Parent = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("Status")){   
                    // query += "Dictionary.`Status` = '"+property.Value.ToString()+"' ";
                    query += "`Status` = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 

            // query+= " group by RoleCertificateId";
            query+= " GROUP BY `Key`";
            Console.WriteLine(query);
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }
        
        

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from RoleCertificate;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into RoleCertificate "+
                    "( RoleCertificateId, RoleCode, CertificateName, CertificateType, Status) "+
                    "values ( '"+data.RoleCertificateId+"','"+data.RoleCode+"','"+data.CertificateName+"','"+data.CertificateType+"','"+data.Status+"');"
                );

            Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from RoleCertificate;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
            
        }


        

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update RoleCertificate set "+  
                    "RoleCode='"+data.RoleCode+"', "+
                    "CertificateName='"+data.CertificateName+"', "+
                    "CertificateType ='"+data.CertificateType+"', "+
                    "Status='"+data.Status+"' "+ 
                    "where RoleCertificateId='"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from RoleCertificate where RoleCertificateId ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            if(o3["RoleCode"].ToString() == data.RoleCode.ToString())
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            } 
 
            return Ok(res);  
        } 

        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";  
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from RoleCertificate where RoleCertificateId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                var roleCert = _mySQLContext.RoleCertificate.FirstOrDefault(x => x.RoleCertificateId == id);
                if (roleCert!=null)
                {
                    _mySQLContext.RoleCertificate.Remove(roleCert);
                    var deleted = _mySQLContext.SaveChanges();
                    if (deleted>0)
                    {
                        var admin = HttpContext.User.GetUserId();
                        _auditLogServices.LogHistory(new History { Admin = admin, Date = DateTime.Now, Description = "Delete RoleCertificate <br/> RoleCertificateId : " + id, Id = id.ToString() });
                        res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                    }
                }
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from RoleCertificate where RoleCertificateId  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){ 
                    res =
                        "{"+
                        "\"code\":\"1\","+
                        "\"message\":\"Delete Data Success\""+
                        "}"; 
                }
            }

            return Ok(res);
        } 

         


    }
}