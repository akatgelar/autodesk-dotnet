using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;
 

namespace autodesk.Controllers
{
    [Route("api/AcademicTargetProgram")]
    public class AcademicTargetProgramController : Controller
    {
        StringBuilder sb = new StringBuilder();

        private MySqlDb oDb;
                    //= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result="";

        public AcademicTargetProgramController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from AcademicTargetProgram");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("AcademicProject/{id}")]
        public IActionResult SelectAcademicProject(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT ap.ProjectId, ap.SiteId, d.KeyValue, DATE_FORMAT(ap.CompletionDate,'%m/%d/%Y') AS CompletionDate, ap.TargetEducator, ap.TargetStudent, ap.productId, ap.Institution, ap.CountryCode, ap.`Comment`, ap.`Status`, ap.DateAdded, ap.AddedBy, ap.DateLastAdmin, ap.LastAdminBy, p.productName "+
                    "FROM AcademicProject ap LEFT JOIN Dictionary d ON ap.`Usage` = d.`Key` LEFT JOIN Products p ON ap.productId = p.productId  "+
                    "WHERE d.Parent = 'ProjectType' and ap.SiteId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }
         

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from AcademicTargetProgram where AcdemicId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("Project/{id}")]
        public IActionResult WhereProjectId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from AcademicProject where ProjectId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }        

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "SELECT atp.AcdemicId, atp.AcademicType, atp.SiteId, d.KeyValue as `Usage`, atp.FYIndicator, atp.TargetEducator, atp.TargetStudent, atp.TargetInstitution, atp.`Comment`, atp.`Status`, atp.DateAdded, atp.AddedBy, atp.DateLastAdmin, atp.LastAdminBy FROM AcademicTargetProgram atp JOIN Dictionary d ON atp.`Usage` = d.`Key` ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("AcademicType")){   
                    query += "AcademicType = '"+property.Value.ToString()+"' AND d.Parent = '"+property.Value.ToString()+"' AND d.`Status` = 'A'"; 
                }
                if(property.Name.Equals("SiteId")){   
                    query += "SiteId = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }
        
        [HttpPost("Project")]
        public IActionResult InsertAcademicProject([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from AcademicProject;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into AcademicProject "+
                    "(SiteId, `Usage`, CompletionDate, TargetEducator, TargetStudent, productId, productVersion, Institution, CountryCode, Comment, `Status`, DateAdded, AddedBy) "+
                    "values ('"+data.SiteId+"','"+data.Usage+"','"+data.CompletionDate+"',"+data.TargetEducator+","+data.TargetStudent+",'"+data.productId+"','"+data.productVersion+"','"+data.Institution+"','"+data.CountryCode+"','"+data.Comment+"','A','"+data.DateAdded+"','"+data.AddedBy+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Insert Data Failed\""+
                    "}"; 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from AcademicProject;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}"; 
            } 
 
            return Ok(res);     
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from AcademicTargetProgram;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into AcademicTargetProgram "+
                    "( AcademicType, `Usage`, SiteId, FYIndicator, TargetEducator, TargetStudent, TargetInstitution, `Comment`, DateAdded, AddedBy, `Status`) "+
                    "values ( '"+data.AcademicType+"', '"+data.ProgramType+"','"+data.SiteId+"','"+data.FYIndicator+"','"+data.TargetEducator+"','"+data.TargetStudent+"','"+data.TargetInstitution+"','"+data.Comment+"',NOW(),'"+data.AddedBy+"','A');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Insert Data Failed\""+
                    "}";  
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from AcademicTargetProgram;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
            
        }
        
        [HttpPut("Project/{id}")]
        public IActionResult UpdateAcademicProject(string id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update AcademicProject set "+
                    "`Usage`='"+data.Usage+"', "+
                    "CompletionDate='"+data.CompletionDate+"', "+
                    "TargetEducator="+data.TargetEducator+", "+
                    "TargetStudent="+data.TargetStudent+", "+
                    "productId='"+data.productId+"', "+
                    "productVersion='"+data.productVersion+"', "+
                    "Institution='"+data.Institution+"', "+
                    "CountryCode='"+data.CountryCode+"', "+
                    "Comment ='"+data.Comment+"', "+
                    "DateLastAdmin='"+data.DateLastAdmin+"', "+
                    "LastAdminBy='"+data.LastAdminBy+"' "+
                    "where ProjectId='"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {  
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from AcademicProject where ProjectId = '"+id+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            if(o3["ProjectId"].ToString() == id.ToString())
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            } 
 
            return Ok(res);  
        } 

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update AcademicTargetProgram set "+
                    "`Usage`='"+data.ProgramType+"', "+
                    "TargetEducator="+data.TargetEducator+", "+
                    "TargetStudent="+data.TargetStudent+", "+
                    "TargetInstitution='"+data.TargetInstitution+"', "+
                    "Comment ='"+data.Comment+"', "+
                    "DateLastAdmin=NOW(), "+
                    "LastAdminBy='"+data.LastAdminBy+"' "+
                    "where AcdemicId='"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {  
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from AcademicTargetProgram where AcdemicId = '"+id+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            if(o3["AcdemicId"].ToString() == id.ToString())
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            } 
 
            return Ok(res);  
        } 

        // [HttpPut("status/{id}")]
        // public IActionResult UpdateSatus(int id)
        // {  
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
                
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update AcademicTargetProgram set "+ 
        //             "status='Deleted' "+
        //             "where AcademicTargetProgram_id='"+id+"'"
        //         ); 
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Delete Data Failed\""+
        //             "}";  
        //     }
        //     finally
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Delete Data Success\""+
        //             "}";  
        //     }

        //     return Ok(res);  
        // } 

        
        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // { 
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("delete from AcademicTargetProgram where AcademicTargetProgram_id = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Delete Data Failed\""+
        //             "}"; 
        //     }
        //     finally
        //     { 
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from AcademicTargetProgram where AcademicTargetProgram_id  ="+id+";");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
            
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //         o3 = JObject.Parse(result);  
        //         Console.WriteLine(o3.Count);
        //         if(o3.Count < 1){ 
        //             res =
        //                 "{"+
        //                 "\"code\":\"1\","+
        //                 "\"message\":\"Delete Data Success\""+
        //                 "}"; 
        //         }
        //     }

        //     return Ok(res);
        // }

        // [HttpGet("CekUnik/{obj}")]
        // public IActionResult WhereObjUnik(string obj)
        // {     
        //     var o1 = JObject.Parse(obj);
        //     string query = ""; 
        //     Console.WriteLine("Tes");
        //     query += "select AcademicTargetProgram_name, count(*) as count from AcademicTargetProgram ";
        //     int i = 0;
        //     JObject json = JObject.FromObject(o1);
        //     foreach (JProperty property in json.Properties())
        //     { 
        //         if(i==0){
        //             query += " where ";
        //         }
        //         if(i>0){
        //             query += " and ";
        //         }

        //         if(property.Name.Equals("AcademicTargetProgramName")){   
        //             query += "AcademicTargetProgram_name = '"+property.Value.ToString()+"' "; 
        //         }

        //         i=i+1;
        //     } 
        //     query += "group by AcademicTargetProgram_name having count(*) > 0";
        //     Console.WriteLine(query);

        //     try
        //     {  
        //         sb.AppendFormat(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not Found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);    
        //     }
        //     return Ok(result);  
        // } 

    }
}