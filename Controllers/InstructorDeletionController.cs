﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Threading.Tasks;
using autodesk.Code;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using autodesk.Code.ViewModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace autodesk.Controllers
{
    [Route("api/InstructorDeletion")]
    public class InstructorDeletionController : Controller
    {
        private MySQLContext dbContext;

        private MySqlDb sqlDb;

        private IDataServices dataServices;

        private IEmailService _emailSender;

        private string Message;
        public InstructorDeletionController(MySQLContext _dbContext,MySqlDb _sqlDb, IEmailService emailSender)
        {
            dbContext = _dbContext;
            sqlDb = _sqlDb;
            dataServices= new DataServices(sqlDb,dbContext);
            Message = string.Empty;
            _emailSender = emailSender;
        }


        [HttpGet("GetInstructor/{ContactID}")]
        public IActionResult GetInstructorList(string ContactID)
        {
            if (!string.IsNullOrEmpty(ContactID))
            {

                //Get Affiliated Org ID of Current User
                var orgList = dbContext.SiteContactLinks.Join(dbContext.MainSite,
                        c => c.SiteId, m => m.SiteId, (c, m) => new {C = c, M = m})
                    .Where(x => x.C.ContactId.Equals(ContactID) && x.C.SiteId == x.M.SiteId)
                    .Select(x => x.M.OrgId).ToList();

                //if There have Affiliated Org it will execute next step otherwise, it will return with "no  content message"
                if (orgList.Count <= 0) return Ok(new {Code =false, Model="", Message =" No Contents found for current login user!"});
                {
                    //Get Site List from Org
                    var siteList = dbContext.MainSite.Where(x => orgList.Contains(x.OrgId)).Select(x => x.SiteId)
                        .ToList();

                    //if there have site it will execute next step
                    if (siteList.Count > 0)                       
                    {
                        var instructorList = dbContext.ContactsAll.Join(dbContext.SiteContactLinks, c => c.ContactId,
                                s => s.ContactId, (c, s) => new {Contact = c, Site = s})
                            .Where(x => x.Contact.ContactId == x.Site.ContactId && siteList.Contains(x.Site.SiteId) && x.Contact.UserLevelId.Equals("TRAINER"))                        
                            .Select(x => new
                            {
                                ContactID = x.Contact.ContactId,
                                Name = x.Contact.ContactName,
                                InstructorID = x.Contact.InstructorId

                                //  ReasonType = dbContext.Dictionary.Where(r=>r.Key.Equals("RespondType") && r.Status.Equals("1")).Select(r=>new DictionaryViewModel{Key = r.Key,Name = r.KeyValue}).ToList()
                            }).Distinct().ToList();

                        //initialized to view model
                        var instructorReportViewModelList = instructorList.Select(x => new InstructorReportViewModel
                        {
                            ContactID = x.ContactID,
                            Name = x.Name,
                            InstructorID = x.InstructorID,
                            Status = dbContext.InstructorRequest.Where(f=>f.ContactId.Equals(x.ContactID)).Select(f=>f.Status).FirstOrDefault(),
                            SiteId = dbContext.SiteContactLinks
                                .Join(dbContext.MainSite, st => st.SiteId, ms => ms.SiteId,
                                    (st, ms) => new {C = st, St = ms}).Where(f => f.C.ContactId.Equals(ContactID))
                                .Select(
                                    f => new SiteViewModel
                                    {
                                        Id = f.St.SiteId,
                                        Name = f.St.SiteName
                                    }).ToList()
                        }).OrderByDescending(x => x.Name).ToList();

                        return Ok(new {Code = true, Model = instructorReportViewModelList, Message = "Succeeded"});
                    }
                    else
                    {
                        return Ok(new
                        {
                            Code = false, Model = " ",
                            Message = "No Site is Affiliated to " +
                                      string.Join(",", orgList.Select(i => i.ToString()).ToArray())
                        });
                    }
                }
            }
            else
            {
                return NoContent();

            }

        }

        [HttpGet("GetRequestedList/{ContactID}")]
        public IActionResult GetRequestedList(string ContactID)
        {
         var model = new List<RequesterViewModel>();
         var isSucceed = false;

         if (!string.IsNullOrEmpty(ContactID))
         {
          var isDistributor = false;
          isDistributor = dbContext.ContactsAll.Where(x => x.ContactId.Equals(ContactID)).Select(x => x.UserLevelId)
           .FirstOrDefault().Contains("DISTRIBUTOR");

          if (isDistributor)
           try
           {
            var instructorRequestedList = dbContext.InstructorRequest.Join(dbContext.ContactsAll, i => i.RequesterId,
             c => c.ContactId, (i, c) => new
             {
              request = i,
              contact = c
             }).Where(x => x.request.DistributorId.Equals(ContactID) && x.request.Status.Equals(AutodeskConst.InstructorDeletionStatus.Pending)).Select(x =>
             new RequesterViewModel
             {
              ContactId = x.request.RequesterId,
              Name = x.contact.ContactName,
              SiteName = dbContext.MainSite.SingleOrDefault(s => s.SiteId.Equals(x.contact.PrimarySiteId)).SiteName,
              UserLevel = x.contact.UserLevelId,
              RequestedInstructorLists = dbContext.ContactsAll
               .Where(f => f.ContactId.Equals(x.request.ContactId)).Select(f =>
                new RequestedInstructorList
                {
                 RequestID = x.request.Id,
                 ContactId = f.ContactId,
                 ContactName = f.ContactName,
                 ReasonType = x.request.ReasonType,
                 Reason = x.request.Reason,
                 RequestedSiteId = x.request.RequestedSiteId
                }).ToList()
             }).GroupBy(x => x.ContactId).Distinct().ToList();

            if (instructorRequestedList.Count > 0)
            {
             foreach (var listGroup in instructorRequestedList)
             {
              var fetchdata = listGroup.Select(x => new RequesterViewModel
              {
               ContactId = x.ContactId,
               Name = x.Name,
               SiteName = x.SiteName,
               UserLevel = x.UserLevel,
               RequestedInstructorLists = listGroup.SelectMany(f => f.RequestedInstructorLists).ToList()
              }).FirstOrDefault();
              model.Add(fetchdata);
             }

             isSucceed = true;
            }
           }
           catch (Exception e)
           {
            return Ok(new
            {
             Code = isSucceed,
             Model = "",
             Message = "Oops Something went wrong while processing your request!"
            });
           }
         }


         return Ok(new {Code = isSucceed, Model = model, Message = "Succeed"});
        }

        [HttpPost("RequestDeletion")]
        public async Task<IActionResult> RequestDeletion([FromBody]InstructorReportViewModel instructorReportViewModel)
        {
            InstructorRequest instructorModel=new InstructorRequest();
            string message = string.Empty;
            bool code = false;
            if (ModelState.IsValid)
            {
                try
                {
                    var siteCountry = dbContext.SiteContactLinks.Join(dbContext.MainSite, c => c.SiteId, s => s.SiteId,
                            (c, s) => new
                            {
                                Contact = c,
                                Site = s
                            }).Where(x => x.Contact.ContactId.Equals(instructorReportViewModel.CurrentUserId))
                        .Select(x => x.Site.SiteCountryCode).FirstOrDefault();
                 
                    if (!string.IsNullOrEmpty(siteCountry))
                    {

                     var distributorModel = this.dataServices.GetDistributor(siteCountry);
                     

                     if (distributorModel != null)
                     {
                      instructorModel.ContactId = instructorReportViewModel.ContactID;
                      instructorModel.DistributorId = distributorModel.ContactId;
                    
                      //string.Join(",",siteCountry.Select(i=>i.ToString()).ToArray());
                      //instructorModel.SiteId=string.Join(",", instructorReportViewModel.SiteId.Select(i => i.Id.ToString()).ToArray());
                      instructorModel.RequestedSiteId = instructorReportViewModel.RequestedSiteId;
                      instructorModel.RequesterId = instructorReportViewModel.CurrentUserId;
                      instructorModel.ReasonType = instructorReportViewModel.SelectedRespondType;
                      instructorModel.Reason = instructorReportViewModel.Reason;
                      instructorModel.Status = AutodeskConst.InstructorDeletionStatus.Pending;
                      dbContext.InstructorRequest.Add(instructorModel);
                      dbContext.SaveChanges();
                      code = true;
                      await this.SendEmail(distributorModel.EmailAddress, instructorReportViewModel.CurrentUserId, instructorReportViewModel.Reason);
                     }                    
                    }                   
                }
                catch (Exception e)
                {              
                 code = false;
                }
            }
            

            message = code ? "Successfully Requested!" : "Oops! something went wrong while processing your request!";

            return Ok(new {Code= code,Message =message});
        }

        [HttpGet("CancelRequest/{ContactId}/{RequesterId}")]
        public IActionResult CancelRequest(string ContactId,string RequesterId)
        {
            if (!(string.IsNullOrEmpty(ContactId) && string.IsNullOrEmpty(RequesterId)))
            {
                try
                {
                    var deleteModel =dbContext.InstructorRequest.FirstOrDefault(x => x.ContactId.Equals(ContactId)  && x.RequesterId.Equals(RequesterId));
                    if (deleteModel != null)
                    {
                        dbContext.InstructorRequest.Remove(deleteModel);
                        dbContext.SaveChanges();
                    }
                    else
                    {
                        return Ok(new
                        {
                            Code = false, Message = "No Content is found for current request, please refresh and try again!"
                        });
                    }


                }
                catch (Exception e)
                {
                    return Ok(new {Code = false, Message = "Oops! something went wrong while processing your request!"});
                }
               
            }
            return Ok(new {Code= true,Message ="Request Canceled!"});
        }

        [HttpGet("ActionForRequest/{RequestID}/{ContactID}")]
        public IActionResult ActionForRequest(string RequestID, string ContactID)
        {
         bool isSucceed = false;
         string message = "Oops! something went wrong while processing your request!";
         if (!string.IsNullOrEmpty(RequestID) && !string.IsNullOrEmpty(ContactID))
         {
          bool isDistributor = false;
          isDistributor = dbContext.ContactsAll.Where(x => x.ContactId.Equals(ContactID)).Select(x => x.UserLevelId)
           .FirstOrDefault().Contains("DISTRIBUTOR");

          if (isDistributor)
          {
           var requestedAction = dbContext.InstructorRequest.FirstOrDefault(x => x.Id == Convert.ToInt32(RequestID));
       
           List<string> siteIds = new List<string>();
           if (requestedAction != null)
           {

            if (requestedAction.ReasonType.Contains("Invalid"))
            {
             isSucceed = this.dataServices.RemoveContactRole(requestedAction.ContactId);
           
            }
            else if (requestedAction.ReasonType.Contains("Resign"))
            {
             isSucceed = this.dataServices.ContactDeletion(requestedAction.ContactId);
            }
            else if (requestedAction.ReasonType.Contains("Add Affiliation"))
            {
             siteIds = requestedAction.RequestedSiteId.Split(",").ToList();
             isSucceed = this.dataServices.AddAffiliationToSites(requestedAction.ContactId, siteIds, ContactID);
            }
            else
            {
             siteIds = requestedAction.RequestedSiteId.Split(",").ToList();
             if (siteIds.Count > 0)
             {
              isSucceed = this.dataServices.UnAffiliatedFromSite(requestedAction.ContactId, siteIds);
             }

            }
            if (isSucceed)
            {
             requestedAction.Status = AutodeskConst.InstructorDeletionStatus.Approved;
             dbContext.Update(requestedAction);
             dbContext.SaveChanges();
            }
           }
          }

         }

         if (isSucceed)
         {
          message = "Successfully approved for the request!";
         }
            return Ok(new {Code = isSucceed, Message = message});
        }

        [HttpGet("GetRespondType/{Type}")]
        public IActionResult GetRespondType(string Type)
        {
            bool code = false;
            List<DictionaryViewModel> respondTypeModel= new List<DictionaryViewModel>();
            if (!string.IsNullOrEmpty(Type))
            {
                respondTypeModel = dataServices.GetListFromDictionaryByParent(Type);
                if (respondTypeModel.Count > 0)
                {
                    Message = "Succeeded";
                    code = true;
                }
                else
                {
                    Message = "No Data Found for Key " + Type;
                }
                
            }

            return Ok(new {Code = code, Model = respondTypeModel, Message=Message});
        }

        [HttpGet("GetRequester/{ContactID}")]
        public IActionResult GetRequester(string ContactID)
        {
            bool code = false;
            List<ColumnViewModel> requesterViewMode = new List<ColumnViewModel>();
            if(!string.IsNullOrEmpty(ContactID))
            {
            requesterViewMode = dbContext.InstructorRequest
            .Join(dbContext.ContactsAll, i => i.RequesterId, ca => ca.ContactId, (i, ca)=> new {requester =i, contactAll=ca})
            .Where(x=>x.requester.DistributorId.Equals(ContactID))
            .GroupBy(x=>x.contactAll).Select(x=> new ColumnViewModel{
                Field = x.Key.ContactId,
                FieName = x.Key.ContactName
            }).Distinct().ToList();
            
            }



            return Ok(new {Code = code, Model = requesterViewMode});
        }
		
		private async Task SendEmail(string distributorEmail, string requesterEmail, string reason)
        {

            //send mail to distributor
            var list = dbContext.EmailBodyType.ToList();
            var emailBodyType = dbContext.EmailBodyType.Where(t => t.EmailBodyTypeName.Contains("Instructor Deletion"));
            if (emailBodyType != null && emailBodyType.FirstOrDefault() != null)
            {
                var entity = emailBodyType.FirstOrDefault();
                var emailBody = dbContext.EmailBody.FirstOrDefault(b => b.BodyType == entity.EmailBodyTypeId.ToString());
                if (emailBody != null)
                {
                    
                    var bodyContent = emailBody.BodyEmail;
                    bodyContent = bodyContent.Replace("[ReceiverName]", distributorEmail);
                    bodyContent = bodyContent.Replace("[Requester]", requesterEmail);
					bodyContent = bodyContent.Replace("[Reason]", reason);
                    bodyContent = bodyContent.Replace("[AppUrl]", "<a href='" + AppConfig.Config["App:AppUrl"] + "' target='_blank' >here</a>");
                    var emailRes = await _emailSender.SendMailAsync(distributorEmail, emailBody.Subject, bodyContent);
                }

            }
        }
		
		
    }
}
