using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;
using autodesk.Code.CommonServices;
using autodesk.Code.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using autodesk.Code.AuditLogServices;

namespace autodesk.Controllers
{
    [Route("api/OrganizationJournalEntries")]
    public class OrganizationJournalEntriesController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        private IDataServices dataService;
        private ICommonServices commonServices;
        String result;
        DataSet dsTemp = new DataSet();
        String hasil;
        private readonly MySQLContext db;
        public Audit _audit;//= new Audit();
        private readonly IAuditLogServices _auditLogServices;
        public OrganizationJournalEntriesController(MySqlDb sqlDb, MySQLContext _db, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _audit = new Audit(oDb);
            db = _db;
            dataService = new DataServices(oDb, db);
            commonServices = new CommonServices();
            _auditLogServices = auditLogServices;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Journals ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ActivityJournal/{id}/{activity_type}")]
        public IActionResult SelectActJournal(string id, string activity_type)
        {
            try
            {
                sb = new StringBuilder();
                // sb.Append("SELECT j.JournalId, j.ParentId, DATE_FORMAT(j.DateAdded,'%m/%d/%Y') AS DateAdded, j.AddedBy, j.ActivityId, DATE_FORMAT(j.ActivityDate,'%m/%d/%Y') AS ActivityDate, j.Notes, DATE_FORMAT(j.DateLastAdmin,'%m/%d/%Y') AS DateLastAdmin, j.LastAdminBy, j.`Status`, j.GUID, ja.ActivityName FROM Journals AS j INNER JOIN JournalActivities AS ja ON j.ActivityId = ja.ActivityId WHERE j.ParentId = '"+id+"' AND j.STATUS <> 'X' AND ja.ActivityType = '"+activity_type+"' ORDER BY DATE(j.ActivityDate) ASC");
                sb.Append("SELECT j.JournalId, j.ParentId, DATE_FORMAT(j.DateAdded,'%m/%d/%Y') AS DateAdded, j.AddedBy, j.ActivityId, DATE_FORMAT(j.ActivityDate,'%m/%d/%Y') AS ActivityDate, j.Notes, DATE_FORMAT(j.DateLastAdmin,'%m/%d/%Y') AS DateLastAdmin, j.LastAdminBy, j.`Status`, j.GUID, ja.ActivityName FROM Journals AS j INNER JOIN JournalActivities AS ja ON j.ActivityId = ja.ActivityId WHERE j.ParentId IN ('" + id + "') AND j.STATUS <> 'X' AND ja.ActivityType = '" + activity_type + "' ORDER BY DATE(j.ActivityDate) ASC"); /* should be use contactid not contactidint - 01102018*/
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt1 = ds.Tables[0];
                foreach (DataRow row in dt1.Rows)
                {
                    string notes = row["Notes"].ToString();
                    notes = commonServices.HtmlEncoder(notes);
                    row["Notes"] = notes;
                }
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Journals where JournalId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);

            }
            var finalResult = JObject.Parse(result);
            return Ok(finalResult);
        }

        [HttpGet("ByJournalId/{id}")]
        public IActionResult WhereJournalId(int id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select ActivityId, DATE_FORMAT(ActivityDate,'%d/%m/%Y') AS ActivityDate, Notes, ParentId from Journals where JournalId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt1 = ds.Tables[0];
                foreach (DataRow row in dt1.Rows)
                {
                    string notes = row["Notes"].ToString();
                    notes = commonServices.HtmlEncoder(notes);
                    row["Notes"] = notes;
                }

            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";
            JArray resultarr;

            query += "SELECT j.JournalId, j.ParentId, DATE_FORMAT( j.DateAdded, '%m/%d/%Y' ) AS DateAdded, j.AddedBy, j.ActivityId, DATE_FORMAT( j.ActivityDate, '%m/%d/%Y' ) AS ActivityDate, j.Notes, DATE_FORMAT( j.DateLastAdmin, '%m/%d/%Y' ) AS DateLastAdmin, j.LastAdminBy, j.`Status`, j.GUID, ja.ActivityName, GROUP_CONCAT(DISTINCT ist.SiteId ) AS Sites FROM Journals j LEFT JOIN JournalActivities ja ON j.ActivityId = ja.ActivityId LEFT JOIN Invoices i ON j.ParentId = i.OrgId AND DATE_FORMAT(j.DateAdded, '%m/%d/%Y' ) = DATE_FORMAT(i.DateAdded, '%m/%d/%Y' ) LEFT JOIN InvoicesSites ist ON i.InvoiceId = ist.InvoiceId AND ist.`Status` = 'A'";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("ParentId"))
                {
                    query += "j.ParentId = '" + property.Value.ToString() + "' ";
                }

                i = i + 1;
            }

            query += " GROUP BY j.JournalId order by j.DateAdded desc";

            // Console.WriteLine(query);

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt1 = ds.Tables[0];
                foreach (DataRow row in dt1.Rows)
                {
                    string notes = row["Notes"].ToString();
                    notes = commonServices.HtmlEncoder(notes);
                    row["Notes"] = notes;
                }

            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultarr = JArray.Parse(MySqlDb.GetJSONArrayString(ds.Tables[0]));
                if (resultarr.Count > 0)
                {
                    if (resultarr[0]["JournalId"] == null || resultarr[0]["JournalId"].ToString() == "")
                    {
                        result = "Not Found";
                    }
                    else
                    {
                        if (ds.Tables.Count > 0)
                        {
                            List<dynamic> dynamicModel = new List<dynamic>();

                            dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                            var settings = new JsonSerializerSettings
                            {

                            };
                            settings.Converters.Add(new DataSetConverter());
                            settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                            result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                                settings);
                            // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);  
                        }

                    }
                }
                else
                {
                    result = "Not Found";
                }
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Journals;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1);


            #region olddata_beforeupdate
            //sb = new StringBuilder();
            //sb.AppendFormat("select * from Journals where ParentId='" + data.ParentId + "' and ActivityId = '" + data.ActivityId + "' ");
            //sqlCommand = new MySqlCommand(sb.ToString());
            //ds = oDb.getDataSetFromSP(sqlCommand);
            //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion


            try
            {
                string tmpLastAdmin = "";
                sb = new StringBuilder();
                string notes = data.Notes.ToString();
                sb.AppendFormat(
                    "insert into Journals " +
                    "(ParentId, DateAdded, AddedBy, ActivityId, ActivityDate, Notes) " +
                    "values ('" + data.ParentId + "',NOW(),'" + data.AddedBy + "','" + data.ActivityId + "','" + data.ActivityDate + "','" + data.Notes + "');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (data.History.ToString().Contains("Site"))
                {
                    tmpLastAdmin = "update Main_site set LastAdminBy='" + data.AddedBy.ToString() + "', DateLastAdmin = Now() where SiteId='" + data.ParentId + "';";
                }
                else if (data.History.ToString().Contains("Organization"))
                {
                    tmpLastAdmin = "update Main_organization set LastAdminBy='" + data.AddedBy.ToString() + "', DateLastAdmin = Now() where OrgId='" + data.ParentId + "';";
                }
                else if (data.History.ToString().Contains("Contact"))
                {
                    tmpLastAdmin = "update Contacts_All set LastAdminBy='" + data.AddedBy.ToString() + "', DateLastAdmin = Now() where ContactId='" + data.ParentId + "';";
                }
                if (!string.IsNullOrEmpty(tmpLastAdmin))
                {
                    sb = new StringBuilder();
                    sb.AppendFormat(tmpLastAdmin);
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                }
            }
            catch(Exception ex)
            {
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Journals;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            if (count2 > count1)
            {

                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM JournalActivities WHERE ActivityId = '" + data.ActivityId + "' LIMIT 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                string encodedata = dt.Rows[0]["Description"].ToString();
                encodedata = commonServices.HtmlEncoder(encodedata);
                dt.Rows[0]["Description"] = encodedata;
                string restmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject resobj = JObject.Parse(restmp);


                #region newdata_afterupdate
                StringBuilder sb2 = new StringBuilder();
                sb2.AppendFormat("select * from Journals where ParentId='" + data.ParentId + "' and ActivityId = '" + data.ActivityId + "' ");
                sqlCommand = new MySqlCommand(sb2.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                //JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                #endregion



                //string description = "Insert data " + data.History + "<br/>" +
                //    "ParentId = " + data.ParentId + "<br/>" +
                //    "Activity Name = " + resobj["ActivityName"] + "<br/>" +
                //    "Activity Date = " + data.ActivityDate + "<br/>" +
                //    "Notes = " + data.Notes;

                //_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Journals, data.UserId.ToString(), data.cuid.ToString(), "where ParentId=" + data.ParentId + " and ActivityId = " + data.ActivityId + " ");
                _auditLogServices.LogHistory(new History {
                    Admin = HttpContext.User.GetUserId(),
                    Date = DateTime.Now,
                    Description = "Add Journal Entry.Data: " + result,
                    Id= data.ActivityId.ToString(),

                });
                //       _audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
            }

            return Ok(res);

        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            #region olddata_beforeupdate
            sb = new StringBuilder();
            sb.AppendFormat("select * from Journals where JournalId='" + id + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion


            try
            {
                string tmpLastAdmin = "";
                string notes = data.Notes.ToString();
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Journals set " +
                    "ActivityId='" + data.ActivityId + "'," +
                    "ActivityDate='" + data.ActivityDate + "'," +
                    "Notes='" + notes + "'," +
                    "DateLastAdmin=NOW()," +
                    "LastAdminBy='" + data.LastAdminBy + "'" +
                    "where JournalId='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (data.History.ToString().Contains("Site"))
                {
                    tmpLastAdmin = "update Main_site set LastAdminBy='" + data.LastAdminBy.ToString() + "', DateLastAdmin = Now() where SiteId='" + data.ParentId + "';";
                }
                else if (data.History.ToString().Contains("Organizaation"))
                {
                    tmpLastAdmin = "update Main_organization set LastAdminBy='" + data.LastAdminBy.ToString() + "', DateLastAdmin = Now() where OrgId='" + data.ParentId + "';";
                }
                else if (data.History.ToString().Contains("Contact"))
                {
                    tmpLastAdmin = "update Contacts_All set LastAdminBy='" + data.LastAdminBy.ToString() + "', DateLastAdmin = Now() where ContactId='" + data.ParentId + "';";
                }
                if (!string.IsNullOrEmpty(tmpLastAdmin))
                {
                    sb = new StringBuilder();
                    sb.AppendFormat(tmpLastAdmin);
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                }

            }
            catch(Exception ex)
            {
                res =
                     "{" +
                     "\"code\":\"0\"," +
                     "\"message\":\"Update Data Failed\"" +
                     "}";
            }
            finally
            {

                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM JournalActivities WHERE ActivityId = '" + data.ActivityId + "' LIMIT 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                string encodedata = dt.Rows[0]["Description"].ToString();
                encodedata = commonServices.HtmlEncoder(encodedata);
                dt.Rows[0]["Description"] = encodedata;
                string restmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject resobj = JObject.Parse(restmp);


                #region newdata_afterupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from Journals where JournalId='" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                #endregion

                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Journals, data.UserId.ToString(), data.cuid.ToString(), " where JournalId=" + id + "");

                //string description = "Update data " + data.History + "<br/>" +
                //    "ParentId = " + data.ParentId + "<br/>" +
                //    "ActivityId = " + resobj["ActivityName"] + "<br/>" +
                //    "ActivityDate = " + data.ActivityDate + "<br/>" +
                //    "Notes = " + data.Notes;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, int UserId)
        {
            string tmpLastAdmin = "";
            string tmpActivityType = "";
            string tmpParentId = "";
            JObject o3 = new JObject();
            int isDeleted = 0;
            sb = new StringBuilder();
            sb.AppendFormat("select * from Journals where JournalId  =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);

            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select ja.ActivityType, j.ParentId from Journals j inner join JournalActivities ja " +
                                 "on j.ActivityId = ja.ActivityId where j.JournalId =" + id);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if(ds.Tables[0].Rows.Count > 0)
                {
                    tmpActivityType = ds.Tables[0].Rows[0]["ActivityType"].ToString();
                    tmpParentId = ds.Tables[0].Rows[0]["ParentId"].ToString();
                }
                

                sb = new StringBuilder();
                sb.AppendFormat("delete from Journals where JournalId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                isDeleted = oDb.temporaryUpdateFunction(sqlCommand);

                if (tmpActivityType.Contains("Site"))
                {
                    tmpLastAdmin = "update Main_site set LastAdminBy='" + cuid.ToString() + "', DateLastAdmin = Now() where SiteId='" + tmpParentId + "';";
                }
                else if (tmpActivityType.Contains("Organization"))
                {
                    tmpLastAdmin = "update Main_organization set LastAdminBy='" + cuid.ToString() + "', DateLastAdmin = Now() where OrgId='" + tmpParentId + "';";
                }
                else if (tmpActivityType.Contains("Contact"))
                {
                    tmpLastAdmin = "update Contacts_All set LastAdminBy='" + cuid.ToString() + "', DateLastAdmin = Now() where ContactId='" + tmpParentId + "';";
                }
                if (!string.IsNullOrEmpty(tmpLastAdmin))
                {
                    sb = new StringBuilder();
                    sb.AppendFormat(tmpLastAdmin);
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                }
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Opp!, something went wrong while processing your request, please try again later!\"" +
                    "}";
            }
            finally
            {


                if (isDeleted == 1)
                {

                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Journals where JournalId  =" + id + ";");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                    
                    _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Journals, UserId.ToString(), cuid.ToString(), " where JournalId  =" + id + "");
                    //string description = "Delete data " + o3["Notes"].ToString() + "<br/>" +
                    //"ParentId = " + o3["ParentId"] + "<br/>" +
                    //"JournalId = " + id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());

                    res = "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                }
                else
                {
                    res = "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Opp!, something went wrong while processing your request, please try again later!\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        [HttpPost("report/{code}")]
        public IActionResult findBy(string code, [FromBody] dynamic data)
        {
            JArray columnOrg;
            JArray columnSite;
            string query = "";
            string selectedColumns = "";
            string kolomOrg = "";
            string kolomSite = "";
            string anotherfiled = "";
            Boolean Status = false;
            Boolean tmpSiteStatus = false;
            selectedColumns += "select ja.ActivityName as 'Activity Name' ";

            /*fix issue excel no 263.Show data based role*/

            string role = "";
            string orgid = "";

            /*end fix issue excel no 263.Show data based role*/

            int n = 1;
            JObject column = JObject.FromObject(data);
            foreach (JProperty property in column.Properties())
            {
                if (property.Name.Equals("fieldOrg"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomOrg = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomOrg = "," + property.Value.ToString();
                        }

                        kolomOrg = kolomOrg.Replace("o.DateAdded", "DATE_FORMAT(o.DateAdded, '%d-%m-%Y') AS DateAdded");
                        kolomOrg = kolomOrg.Replace("o.DateLastAdmin", "DATE_FORMAT(o.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        if (kolomOrg.Contains("Status"))
                        {
                            kolomOrg = kolomOrg.Replace("o.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= o.Status)  'Status'");
                        }
                        if (kolomOrg.Contains("Notes"))
                        {
                            kolomOrg = kolomOrg.Replace("j.Notes", "j.Notes as 'Journal Entry Note'");
                        }
                        if (kolomOrg.Contains("RegisteredCountryCode"))
                        {
                            kolomOrg = kolomOrg.Replace("o.RegisteredCountryCode", "rc.countries_name as 'Org Country'");
                        }
                        if (kolomOrg.Contains("InvoicingCountryCode"))
                        {
                            kolomOrg = kolomOrg.Replace("o.InvoicingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = o.InvoicingCountryCode) as 'Invoicing Country'");
                        }
                        if (kolomOrg.Contains("ContractCountryCode"))
                        {
                            kolomOrg = kolomOrg.Replace("o.ContractCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = o.ContractCountryCode) as 'Contract Country'");
                        }

                        selectedColumns += kolomOrg;

                        n = n + 1;
                    }
                    else
                    {

                        //selectedColumns = "select ja.ActivityName as 'Activity Name'";
                        //sb = new StringBuilder();
                        //sb.AppendFormat("DESCRIBE Main_organization");
                        //sqlCommand = new MySqlCommand(sb.ToString());
                        //dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        //hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        //columnOrg = JArray.Parse(hasil);

                        //string[] columnarrtmp = new string[columnOrg.Count];
                        //for (int index = 0; index < columnOrg.Count; index++)
                        //{
                        //    columnarrtmp[index] = "o." + columnOrg[index]["Field"].ToString();
                        //}

                        //if (n == 0)
                        //{
                        //    kolomOrg = string.Join(",", columnarrtmp);
                        //}
                        //else if (n > 0)
                        //{
                        //    kolomOrg = "," + string.Join(",", columnarrtmp);
                        //}

                        //kolomOrg = kolomOrg.Replace("o.DateAdded", "DATE_FORMAT(o.DateAdded, '%d-%m-%Y') AS DateAdded");
                        //kolomOrg = kolomOrg.Replace("o.DateLastAdmin", "DATE_FORMAT(o.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        //if (kolomOrg.Contains("Status"))
                        //{
                        //    kolomOrg = kolomOrg.Replace("o.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= o.Status)  'Status'");
                        //}
                        //if (kolomOrg.Contains("Notes"))
                        //{
                        //    kolomOrg = kolomOrg.Replace("j.Notes", "j.Notes as 'Journal Entry Note'");
                        //}
                        //if (kolomOrg.Contains("RegisteredCountryCode"))
                        //{
                        //    kolomOrg = kolomOrg.Replace("o.RegisteredCountryCode", "rc.countries_name as 'Org Country'");
                        //}
                        //selectedColumns += kolomOrg;

                        //n = n + 1;
                    }
                }



                if (property.Name.Equals("fieldSite"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomSite = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomSite = "," + property.Value.ToString();
                        }

                        //kolomSite = kolomSite.Replace("s.SiteName", "replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as SiteName");
                        //kolomSite = kolomSite.Replace("s.SiteWebAddress", "replace( replace( replace( s.SiteWebAddress, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as SiteWebAddress");
                        kolomSite = kolomSite.Replace("s.DateAdded", "DATE_FORMAT(s.DateAdded, '%d-%m-%Y') AS DateAdded");
                        kolomSite = kolomSite.Replace("s.DateLastAdmin", "DATE_FORMAT(s.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        if (kolomSite.Contains("Status"))
                        {
                            kolomSite = kolomSite.Replace("s.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= s.Status)  'Site Status'");
                        }
                        if (kolomSite.Contains("Notes"))
                        {
                            kolomSite = kolomSite.Replace("j.Notes", "j.Notes as 'Journal Entry Note'");
                        }
                        if (kolomSite.Contains("SiteCountryName"))
                        {
                            kolomSite = kolomSite.Replace("s.SiteCountryName", "rc.countries_name as 'Site Country'");
                        }
                        if (kolomSite.Contains("MailingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.MailingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.MailingCountryCode) as 'Mailing Country'");
                        }
                        if (kolomSite.Contains("ShippingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.ShippingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.ShippingCountryCode) as 'Shipping Country'");
                        }
                        

                        selectedColumns += kolomSite;

                        n = n + 1;
                    }
                    else
                    {
                        //sb = new StringBuilder();
                        //sb.AppendFormat("DESCRIBE Main_site");
                        //sqlCommand = new MySqlCommand(sb.ToString());
                        //dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        //hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        //columnSite = JArray.Parse(hasil);

                        //string[] columnarrtmp = new string[columnSite.Count];
                        //for (int index = 0; index < columnSite.Count; index++)
                        //{
                        //    columnarrtmp[index] = "s." + columnSite[index]["Field"].ToString();
                        //}

                        //if (n == 0)
                        //{
                        //    kolomSite = string.Join(",", columnarrtmp);
                        //}
                        //else if (n > 0)
                        //{
                        //    kolomSite = "," + string.Join(",", columnarrtmp);
                        //}

                        ////kolomSite = kolomSite.Replace("s.SiteName", "replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as SiteName");
                        ////kolomSite = kolomSite.Replace("s.SiteWebAddress", "replace( replace( replace( s.SiteWebAddress, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as SiteWebAddress");
                        //kolomSite = kolomSite.Replace("s.DateAdded", "DATE_FORMAT(s.DateAdded, '%d-%m-%Y') AS DateAdded");
                        //kolomSite = kolomSite.Replace("s.DateLastAdmin", "DATE_FORMAT(s.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        //if (kolomSite.Contains("Status"))
                        //{
                        //    kolomSite = kolomSite.Replace("s.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= s.Status)  'Site Status'");
                        //}
                        //if (kolomSite.Contains("Notes"))
                        //{
                        //    kolomSite = kolomSite.Replace("j.Notes", "j.Notes as 'Journal Entry Note'");
                        //}
                        //if (kolomSite.Contains("SiteCountryCode"))
                        //{
                        //    kolomSite = kolomSite.Replace("s.SiteCountryCode", "rc.countries_name as 'Site Country'");
                        //}
                        //selectedColumns += kolomSite;

                        //n = n + 1;
                    }
                }

                if (property.Name.Equals("anotherField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            anotherfiled = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            anotherfiled += "," + property.Value.ToString();
                        }
                        anotherfiled = anotherfiled.Replace("m.MarketType", "m.MarketType as 'Market Type'");
                        anotherfiled = anotherfiled.Replace("r.RoleName", "r.RoleName as 'Partner Type'");
                        anotherfiled = anotherfiled.Replace("sr.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= sr.Status) as 'Partner Type Status'");
                        anotherfiled = anotherfiled.Replace("sr.CSN", "sr.CSN as 'Partner Type CSN'");
                        anotherfiled = anotherfiled.Replace("sr.ITS_ID", "sr.ITS_ID as 'ITS Site ID'");
                        anotherfiled = anotherfiled.Replace("sr.UParent_CSN", "sr.UParent_CSN as 'Ultimate Parent CSN'");
                        anotherfiled = anotherfiled.Replace("rg.geo_name", "rg.geo_name as 'Geo'");
                        anotherfiled = anotherfiled.Replace("rr.region_name", "rr.region_name as 'Region'");
                        anotherfiled = anotherfiled.Replace("rsr.subregion_name", "rsr.subregion_name as 'SubRegion'");
                        anotherfiled = anotherfiled.Replace("t.Territory_Name", "t.Territory_Name as 'Territory'");
                        selectedColumns += anotherfiled;
                        n = n + 1;
                    }
                }
                /*fix issue excel no 263.Show data based role*/

                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        role = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        orgid = property.Value.ToString();
                    }
                }

                /*end fix issue excel no 263.Show data based role*/

                if (property.Name.Equals("onlyOrgJE"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        Status = Convert.ToBoolean(property.Value.ToString());
                    }
                }
                if (property.Name.Equals("onlySiteJE"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        tmpSiteStatus = Convert.ToBoolean(property.Value.ToString());
                    }
                }
            }

            if (code == "Org")
            {
                if (Status == true)
                {
                    query = query.Replace("ja.Note", "j.Note");
                    query += selectedColumns + " FROM Main_organization o " +
                        " join Journals j on o.OrgId = j.ParentId " +
                        " join JournalActivities ja on j.ActivityId = ja.ActivityId " +
                        " join Main_site s on o.OrgId = s.OrgId" +
                        " join Ref_countries rc on o.RegisteredCountryCode = rc.countries_code" +
                        " join MarketType m on rc.MarketTypeId = m.MarketTypeId " +
                        " join SiteRoles sr on s.SiteId = sr.SiteId " +
                        " join Ref_geo rg on rc.geo_code = rg.geo_code " +
                        " join Ref_region rr on rc.region_code = rr.region_code " +
                        " join Ref_subregion rsr on rc.subregion_code = rsr.subregion_code " +
                        " join Ref_territory t on rc.TerritoryId = t.TerritoryId " +
                        " join Roles r on sr.RoleId = r.RoleId ";
                }
                else
                {
                    query += selectedColumns + " FROM   Main_organization o  " +
                "Left join (select j.ParentId,ja.ActivityType,j.ActivityDate, ja.ActivityId,ja.ActivityName, ss.SiteId,ss.SiteCountryCode, " +
                " m.MarketType,r.RoleName,sr.Status,sr.CSN,sr.ITS_ID,sr.UParent_CSN,c.geo_name,c.region_name,  c.subregion_name,tc.Territory_Name, " +
                " c.countries_name,ss.orgId,ss.SiteStatus_retired , ss.SiteName,ss.EnglishSiteName,ss.CommercialSiteName,ss.Workstations,ss.SiteTelephone," +
                " ss.SiteFax,ss.SiteEmailAddress,ss.SiteWebAddress,ss.SiteDepartment,ss.SiteAddress1,ss.SiteAddress2,ss.SiteAddress3,ss.SiteCity," +
                " ss.SiteStateProvince,ss.SitePostalCode,ss.MailingDepartment,ss.MailingAddress1,ss.MailingAddress2,ss.MailingAddress3,ss.MailingCity," +
                " ss.MailingStateProvince,ss.MailingCountryCode,ss.MailingPostalCode,ss.ShippingDepartment,ss.ShippingAddress1,ss.ShippingAddress2," +
                " ss.ShippingAddress3,  ss.ShippingCity,ss.ShippingStateProvince,ss.ShippingCountryCode,ss.ShippingPostalCode,ss.SiebelDepartment," +
                " ss.SiteAdminFirstName,ss.SiteAdminLastName,ss.SiteAdminEmailAddress,ss.SiteAdminTelephone,ss.SiteAdminFax,ss.SiteManagerFirstName," +
                " ss.SiteManagerLastName,ss.SiteManagerEmailAddress,ss.SiteManagerTelephone,ss.SiteManagerFax, ss.AdminNotes,ss.DateAdded,ss.AddedBy," +
                " ss.DateLastAdmin,ss.LastAdminBy FROM  Journals j left join Main_organization o   on j.ParentId = o.OrgId  inner join Main_site ss on o.OrgId = ss.OrgId " +
                " inner join JournalActivities ja on ja.ActivityId = j.ActivityId inner join Ref_countries c  on c.countries_code = ss.SiteCountryCode inner join SiteRoles sr " +
                " on sr.SiteId = ss.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId  " +
                "  inner join MarketType m on c.MarketTypeId = m.MarketTypeId  where sr.Status <> 'X'  AND ss.Status <> 'X' AND ss.Status <> 'D' " +
                " and ja.ActivityId in (" + data["filterByActivity"].ToString() + "))ja" +
                " on o.OrgId = ja.ParentId " +
                " left join Main_site s on o.OrgId = s.OrgId" +
                " join Ref_countries rc on o.RegisteredCountryCode = rc.countries_code" +
                " join MarketType m on rc.MarketTypeId = m.MarketTypeId " +
                " join SiteRoles sr on s.SiteId = sr.SiteId " +
                " join Ref_geo rg on rc.geo_code = rg.geo_code " +
                " join Ref_region rr on rc.region_code = rr.region_code " +
                " join Ref_subregion rsr on rc.subregion_code = rsr.subregion_code " +
                " join Ref_territory t on rc.TerritoryId = t.TerritoryId " +
                " join Roles r on sr.RoleId = r.RoleId ";
                }
            }
            else
            {
                if (Status == true)
                {
                    query += selectedColumns + " FROM Main_site s " +
                    " join Journals j on s.SiteId = j.ParentId " +
                    " join JournalActivities ja on j.ActivityId = ja.ActivityId " +
                    " inner join Main_organization o on s.OrgId = o.OrgId" +
                    " inner join Ref_countries rc on s.SiteCountryCode = rc.countries_code" +
                    " inner join MarketType m on rc.MarketTypeId = m.MarketTypeId " +
                    " inner join SiteRoles sr on s.SiteId = sr.SiteId " +
                    " inner join Ref_geo rg on rc.geo_code = rg.geo_code " +
                    " join Ref_region rr on rc.region_code = rr.region_code " +
                    " join Ref_subregion rsr on rc.subregion_code = rsr.subregion_code " +
                    " inner join Ref_territory t on rc.TerritoryId = t.TerritoryId " +
                    " join Roles r on sr.RoleId = r.RoleId ";
                }
                else
                {
                    query += selectedColumns + " FROM   Main_organization o  " +
                "  inner join Main_site s on o.OrgId = s.OrgId  Left join  (select j.ParentId,ja.ActivityType,j.ActivityDate, ja.ActivityId,ja.ActivityName, ss.SiteId,ss.SiteCountryCode, " +
                " m.MarketType,r.RoleName,sr.Status,sr.CSN,sr.ITS_ID,sr.UParent_CSN,c.geo_name,c.region_name,  c.subregion_name,tc.Territory_Name, " +
                " c.countries_name,ss.orgId,ss.SiteStatus_retired , ss.SiteName,ss.EnglishSiteName,ss.CommercialSiteName,ss.Workstations,ss.SiteTelephone," +
                " ss.SiteFax,ss.SiteEmailAddress,ss.SiteWebAddress,ss.SiteDepartment,ss.SiteAddress1,ss.SiteAddress2,ss.SiteAddress3,ss.SiteCity," +
                " ss.SiteStateProvince,ss.SitePostalCode,ss.MailingDepartment,ss.MailingAddress1,ss.MailingAddress2,ss.MailingAddress3,ss.MailingCity," +
                " ss.MailingStateProvince,ss.MailingCountryCode,ss.MailingPostalCode,ss.ShippingDepartment,ss.ShippingAddress1,ss.ShippingAddress2," +
                " ss.ShippingAddress3,  ss.ShippingCity,ss.ShippingStateProvince,ss.ShippingCountryCode,ss.ShippingPostalCode,ss.SiebelDepartment," +
                " ss.SiteAdminFirstName,ss.SiteAdminLastName,ss.SiteAdminEmailAddress,ss.SiteAdminTelephone,ss.SiteAdminFax,ss.SiteManagerFirstName," +
                " ss.SiteManagerLastName,ss.SiteManagerEmailAddress,ss.SiteManagerTelephone,ss.SiteManagerFax, ss.AdminNotes,ss.DateAdded,ss.AddedBy," +
                " ss.DateLastAdmin,ss.LastAdminBy, j.Notes FROM  Journals j left join Main_site ss   on j.ParentId = ss.SiteId   " +
                " inner join JournalActivities ja on ja.ActivityId = j.ActivityId inner join Ref_countries c  on c.countries_code = ss.SiteCountryCode inner join SiteRoles sr " +
                " on sr.SiteId = ss.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId  " +
                "  inner join MarketType m on c.MarketTypeId = m.MarketTypeId  where sr.Status <> 'X'  AND ss.Status <> 'X' AND ss.Status <> 'D' " +
                " and ja.ActivityId in (" + data["filterByActivity"].ToString() + "))ja" +
                " on s.SiteId= ja.ParentId " +
                " join Ref_countries rc on o.RegisteredCountryCode = rc.countries_code" +
                " join MarketType m on rc.MarketTypeId = m.MarketTypeId " +
                " join SiteRoles sr on s.SiteId = sr.SiteId " +
                " join Ref_geo rg on rc.geo_code = rg.geo_code " +
                " join Ref_region rr on rc.region_code = rr.region_code " +
                " join Ref_subregion rsr on rc.subregion_code = rsr.subregion_code " +
                " join Ref_territory t on rc.TerritoryId = t.TerritoryId " +
                " join Roles r on sr.RoleId = r.RoleId ";
                }
                }


            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {

                if (property.Name.Equals("FinancialYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }

                        var fyYearCorrect = Convert.ToInt32(property.Value.ToString().Substring(2)) + 1;
                        //fyYearCorrect.ToString().Replace("20","FY")
                        if (code == "Org" )
                        {
                            if (Status == true)
                            {
                                query += "ja.ActivityName like '%" + "FY" + fyYearCorrect +
                                         "%' and ja.ActivityType = 'Organization Journal Entry'";
                            }
                        }
                        else if (code == "Site")
                        {
                            if (tmpSiteStatus == true)
                            {
                                query += "ja.ActivityName like '%" + "FY" + fyYearCorrect +
                                    "%' and ja.ActivityType = 'Site Journal Entry'";
                            }
                           
                        } else
                            {
                            //query += "ja.ActivityName like '%" + "FY" + fyYearCorrect +
                            //         "%' and ActivityType = 'Site Journal Entry'";
                            query += "ja.ActivityType = 'Site Journal Entry'";
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByActivity"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if(Status == true)
                        {
                            if (i == 0) { query += " where "; }
                            if (i > 0) { query += " and "; }
                            query += "j.ActivityId in (" + property.Value.ToString() + ") ";
                            i = i + 1;
                        }
                      
                    }
                }

                if (property.Name.Equals("filterByActivityDate"))
                {
                    if ((property.Value.ToString() != ",") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        string[] startendfilter = property.Value.ToString().Split(',');
                        if (Status == false)
                        {
                            query += "ja.ActivityDate BETWEEN '" + startendfilter[0] + "' AND '" + startendfilter[1] + "' ";
                        }else
                            query += "j.ActivityDate BETWEEN '" + startendfilter[0] + "' AND '" + startendfilter[1] + "' ";

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.RoleId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.Status in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByMarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.MarketTypeId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByRegion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.region_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySubRegion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.subregion_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByTerritory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "rc.TerritoryId in (" + property.Value.ToString() + ") ";

                        /*fix issue excel no 263.Show data based role*/

                        if (role == "")
                        {
                            query += "rc.TerritoryId in (" + property.Value.ToString() + ") ";
                        }
                        else if (role == "DISTRIBUTOR")
                        {
                            query += "rc.TerritoryId in (select rc.TerritoryId from Ref_countries rc INNER JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + orgid + ") GROUP BY rc.TerritoryId) ";
                        }
                        else
                        {
                            query += "rc.TerritoryId in (" + property.Value.ToString() + ") AND o.OrgId in (" + orgid + ") ";
                        }

                        /*end fix issue excel no 263.Show data based role*/

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountries"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.countries_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                
            }

            if (code == "Org")
            {
                if (Status == true)
                {
                   query += " AND  ja.ActivityType = 'Organization Journal Entry' group by  o.OrgId,ja.ActivityName  desc"; 
                }
                else
                { query += " 1=1  group by  o.OrgId,ja.ActivityName  desc"; }
            }
            else
            {
                if (tmpSiteStatus == true)
                {
                     query += "   and ja.ActivityType = 'Site Journal Entry' group by  s.SiteId,ja.ActivityName  desc"; 
                }
                else
                {
                    query += " 1=1  group by  s.SiteId,ja.ActivityName  desc";
                }
                //waquery += " group by s.SiteId desc";
            }
            List<dynamic> dynamicList = new List<dynamic>();

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());

                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        string encodedata = row[col].ToString();
                        string type = col.DataType.ToString();
                        if (type != "System.DateTime")
                        {
                            encodedata = commonServices.HtmlEncoder(encodedata);
                            row[col] = encodedata;
                        }

                    }

                }
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
                // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);
        }

    }
}
