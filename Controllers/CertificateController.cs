using autodesk.Code;
using autodesk.Code.Models;
using autodesk.Code.Models.Dto.CertificateDto;
using autodesk.Code.PdfServices;
using autodesk.DTO;
using HtmlAgilityPack;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using autodesk.Code.CommonServices;
using autodesk.Code.AuditLogServices;
using Newtonsoft.Json;


// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/Certificate")]
    public class CertifiedController : Controller
    {
        BaseFont ArtifaktBlack;
        BaseFont ArtifaktBook;
        BaseFont ArtifaktRegular;
        BaseFont ArtifaktBold;
        BaseFont ArtifaktElRegular;
        BaseFont Heisei;
        BaseFont Hygothic;
        BaseFont Stsong;
        BaseFont Angsana;
        BaseFont Greek;
        BaseColor BaseGreen = new BaseColor(132, 192, 67);
        List<float> temptitlex = new List<float>();
        List<float> temptitley = new List<float>();
        List<float> tempx = new List<float>();
        List<float> tempy = new List<float>();
        float firsttextx = 0f;
        private ICommonServices commonServices;


        List<ElementNodeDTO> elements = new List<ElementNodeDTO>();
        reportype reportype = reportype.type1;
        List<CellNodeDTO> initcells = new List<CellNodeDTO>();

        float adjusttextx = 0;
        float adjusttexty = 0;
        float adjusttitlex = 0;
        float adjusttitley = 0;

        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        JObject cert;

        private Audit _audit;// = new Audit(oDb);
        private IPdfServices _pdfServices;
        private readonly IHostingEnvironment _hostingEnvironment;

        private MySQLContext _mySQLContext;
        private List<Tuple<string, byte[]>> _backgroundImages;
        private readonly IEmailService _emailService;
        private readonly IAuditLogServices _auditLogServices;
        public CertifiedController(IHostingEnvironment hostingEnvironment, MySqlDb sqlDb, MySQLContext mySQLContext, IEmailService emailService, IAuditLogServices auditLogServices)
        {
            _mySQLContext = mySQLContext;
            _pdfServices = new PdfServices();
            _hostingEnvironment = hostingEnvironment;
            oDb = sqlDb;
            _audit = new Audit(oDb);
            _emailService = emailService;
            commonServices = new CommonServices();
            _auditLogServices = auditLogServices;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT * FROM Certificate LEFT JOIN Dictionary ON Dictionary.Key=Certificate.LanguageCertificate AND Dictionary.Parent = 'Languages' AND Dictionary.Status = 'A'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }



        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Certificate where CertificateId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }



        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select CertificateId from Certificate ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CertificateTypeId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "CertificateTypeId like '%" + property.Value.ToString() + "%' ";
                        query += "CertificateTypeId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "Year like '%" + property.Value.ToString() + "%' ";
                        query += "`Year` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("LanguageCertificate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "LanguageCertificate like '%" + property.Value.ToString() + "%' ";
                        query += "LanguageCertificate = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                    else
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "LanguageCertificate like '%35%' ";
                        query += "LanguageCertificate = '35' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "PartnerTypeId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
            }

            try
            {
                sb.AppendFormat(query);

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                /* issue 08102018 - student cannot download certificate (old courses) */

                if (ds.Tables[0].Rows.Count == 0)
                {
                    StringBuilder sb1 = new StringBuilder();
                    sb1.AppendFormat("select DISTINCT `Year` from Certificate ORDER BY `Year` DESC LIMIT 1");
                    sqlCommand = new MySqlCommand(sb1.ToString());
                    DataSet ds1 = new DataSet();
                    ds1 = oDb.getDataSetFromSP(sqlCommand);
                    string resulttmp = MySqlDb.GetJSONObjectString(ds1.Tables[0]);
                    JObject objCertf = JObject.Parse(resulttmp);

                    string newquery = "select CertificateId from Certificate ";
                    int j = 0;
                    foreach (JProperty property in json.Properties())
                    {
                        if (property.Name.Equals("CertificateTypeId"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (j == 0) { newquery += " where "; }
                                if (j > 0) { newquery += " and "; }
                                newquery += "CertificateTypeId = '" + property.Value.ToString() + "' ";
                                j = j + 1;
                            }
                        }
                        if (property.Name.Equals("Year"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (j == 0) { newquery += " where "; }
                                if (j > 0) { newquery += " and "; }
                                newquery += "`Year` = '" + objCertf["Year"].ToString() + "' ";
                                j = j + 1;
                            }
                        }
                        if (property.Name.Equals("LanguageCertificate"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (j == 0) { newquery += " where "; }
                                if (j > 0) { newquery += " and "; }
                                newquery += "LanguageCertificate = '" + property.Value.ToString() + "' ";
                                j = j + 1;
                            }
                            else
                            {
                                if (j == 0) { newquery += " where "; }
                                if (j > 0) { newquery += " and "; }
                                newquery += "LanguageCertificate = '35' ";
                                j = j + 1;
                            }
                        }
                        if (property.Name.Equals("PartnerType"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (j == 0) { newquery += " where "; }
                                if (j > 0) { newquery += " and "; }
                                newquery += "PartnerTypeId = '" + property.Value.ToString() + "' ";
                                j = j + 1;
                            }
                        }
                    }
                    StringBuilder sb2 = new StringBuilder();
                    sb2.AppendFormat(newquery);
                    sqlCommand = new MySqlCommand(sb2.ToString());
                    DataSet ds2 = new DataSet();
                    ds2 = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONArrayString(ds2.Tables[0]);
                }
                else
                {
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                }

                /* end line issue 08102018 - student cannot download certificate (old courses) */
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                //  result = MySqlDb.GetJSONArrayString(ds.Tables[0]);

                var courseid = "";
                var userid = "";
                if (json["UserId"] != null && json["courseid"] != null)
                {
                    courseid = json["courseid"].ToString();
                    userid = json["UserId"].ToString();
                }

                //string description = "Download Certificate <br/>" +
                //    "Course Id = " + courseid;

                //_audit.ActionPost(userid, description, "-");
                //JObject NewData = new JObject();
                //JObject OldData = new JObject();
                //_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Certificate, userid.ToString(), userid.ToString(), "where CertificateId =" + id + "");

            }
            return Ok(result);
        }

        [HttpGet("Courses/where/{obj}")]
        public IActionResult WhereObjCourse(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            // query += "select CourseStudent.StudentID, CourseStudent.SurveyTaken, CourseStudent.CourseTaken,Courses.CourseId, Courses.HoursTraining, DATE_FORMAT(Courses.CompletionDate,'%d/%m/%Y') AS CompletionDate, DATE_FORMAT(Courses.StartDate,'%d/%m/%Y') AS StartDate, Courses.`Name`, Contacts_All.ContactId, Contacts_All.ContactName,replace( replace( replace( Main_site.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as SiteName, Main_site.SiteAddress1, Courses.FYIndicatorKey, Courses.TrainingTypeKey,Courses.ProjectTypeKey,Courses.EventTypeKey, Products.productName, LocationName,Courses.`Name`,tblStudents.Firstname, tblStudents.Lastname from CourseStudent left join Courses on CourseStudent.CourseId = Courses.CourseId left join Contacts_All on Courses.ContactId = Contacts_All.ContactId left join Main_site on Contacts_All.PrimarySiteId = Main_site.SiteId left join CourseSoftware on CourseStudent.CourseId = CourseSoftware.CourseId left join Products on CourseSoftware.productId = Products.productId left join tblStudents on CourseStudent.StudentID = tblStudents.StudentID ";
            query +=
            "SELECT cs.StudentID, cs.SurveyTaken, cs.CourseTaken, c.CourseId, c.HoursTraining, DATE_FORMAT(c.CompletionDate,'%d/%m/%Y') AS CompletionDate, DATE_FORMAT(c.StartDate,'%d/%m/%Y') AS StartDate, c.`Name`," +
            "ca.ContactId, ca.ContactName,replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as SiteName, s.SiteAddress1, c.FYIndicatorKey, c.TrainingTypeKey," +
            "c.ProjectTypeKey,c.EventTypeKey, CONCAT(IFNULL(p.productName,''),' ',IFNULL(pv.Version,'')) AS productName, LocationName,ts.Firstname, ts.Lastname, CONCAT(c.CourseId,ts.StudentID) AS CertNumber,Institution,InstitutionLogo FROM CourseStudent cs " +
            "INNER JOIN Courses c ON cs.CourseId = c.CourseId " +
            "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
            "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
            "INNER JOIN CourseSoftware cf ON cs.CourseId = cf.CourseId " +
            "LEFT JOIN ProductVersions pv ON cf.ProductVersionsId = pv.ProductVersionsId " +
            "LEFT JOIN Products p ON cf.productId = p.productId " +
            "INNER JOIN tblStudents ts on cs.StudentID = ts.StudentID ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("StudentID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "cs.StudentID = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CourseId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "cs.CourseId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

            }

            query += " GROUP BY cs.StudentID";
            // Console.WriteLine(query);
            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                cert = JObject.Parse(result);

                //get available institution logo from database (only for project certificate type)
                if (ds.Tables[0].Rows[0]["InstitutionLogo"] != null)
                {
                    byte[] bytes = (byte[])ds.Tables[0].Rows[0]["InstitutionLogo"];
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    string imageUrl = "data:image/png;base64," + base64String;

                    cert["InstitutionLogo"] = imageUrl;
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                result = cert.ToString();
            }
            return Ok(result);
        }



        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Certificate;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());


            try
            {
                var FixTitleTextFooter = data.TitleTextFooter.ToString().Replace("'", "");
                var FixTitleHeaderDescription = data.TitleHeaderDescription.ToString().Replace("'", "");

                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Certificate " +
                    "( CertificateBackgroundId, PartnerTypeId,CertificateTypeId, Code, Name, Year, HTMLDesign, Status, LanguageCertificate, TitleTextHeader, TitleHeaderDescription, TitleTextFooter, TitleCertificateNo, TitleStudentName, TitleEvent, TitleCourseTitle, TitleProduct, TitleInstructor, TitleDate, TitleDuration, TitlePartner, TitleInstitution, TitleLocation) " +
                    "values ( '" + data.CertificateBackgroundId + "'," + data.PartnerTypeId + "," + data.CertificateTypeId + ",'" + data.Code + "','" + data.Name + "','" + data.Year + "','" + data.HTMLDesign + "','" + data.Status + "','" + data.LanguageCertificate + "','" + data.TitleTextHeader + "','" + FixTitleHeaderDescription + "','" + FixTitleTextFooter + "','" + data.TitleCertificateNo + "','" + data.TitleStudentName + "','" + data.TitleEvent + "','" + data.TitleCourseTitle + "','" + data.TitleProduct + "','" + data.TitleInstructor + "','" + data.TitleDate + "','" + data.TitleDuration + "','" + data.TitlePartner + "','" + data.TitleInstitution + "','" + data.TitleLocation + "');"
                );


                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Certificate;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());

            sb = new StringBuilder();
            sb.AppendFormat("select * from Certificate where Code = '" + data.Code+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            if (count2 > count1)
            {
                //string description = "Insert data Certificate" + "<br/>" +
                //   "Code = " + data.Code + "<br/>" +
                //   "Name = " + data.Name;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                JObject OldData = new JObject();
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Certificate, data.UserId.ToString(), data.cuid.ToString(), "where Code =" + data.Code + "");

                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
            }

            return Ok(res);

        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Certificate where CertificateId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {
                var FixTitleTextFooter = data.TitleTextFooter.ToString().Replace("'", "");
                var FixTitleHeaderDescription = data.TitleHeaderDescription.ToString().Replace("'", "");

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Certificate set " +
                    "Code='" + data.Code + "', " +
                    "Name='" + data.Name + "', " +
                    "HTMLDesign='" + data.HTMLDesign + "', " +
                    "TitleTextHeader='" + data.TitleTextHeader + "', " +
                    "TitleHeaderDescription='" + FixTitleHeaderDescription + "', " +
                    "TitleTextFooter='" + FixTitleTextFooter + "', " +
                    "TitleCertificateNo='" + data.TitleCertificateNo + "', " +
                    "TitleStudentName='" + data.TitleStudentName + "', " +
                    "TitleEvent='" + data.TitleEvent + "', " +
                    "TitleCourseTitle='" + data.TitleCourseTitle + "', " +
                    "TitleProduct='" + data.TitleProduct + "', " +
                    "TitleInstructor='" + data.TitleInstructor + "', " +
                    "TitleDate='" + data.TitleDate + "', " +
                    "TitleDuration='" + data.TitleDuration + "', " +
                    "TitlePartner='" + data.TitlePartner + "', " +
                    "TitleInstitution='" + data.TitleInstitution + "', " +
                    "TitleLocation='" + data.TitleLocation + "', " +
                    "LanguageCertificate='" + data.LanguageCertificate + "', " +
                    "PartnerTypeId='" + data.PartnerTypeId + "', " +
                    "CertificateTypeId='" + data.CertificateTypeId + "' " +
                    "where CertificateId='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("Success");

            }
            catch
            {
                Console.WriteLine("failed");
                Console.WriteLine(sb.ToString());

            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from Certificate where CertificateId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            o3 = JObject.Parse(result);

            if (o3["Name"].ToString() == data.Name.ToString())
            {

                //string description = "Update data Certificate" + "<br/>" +
                //    "Code = " + data.Code + "<br/>" +
                //    "Name = " + data.Name;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Certificate, data.UserId.ToString(), data.cuid.ToString(), "where CertificateId =" + id + "");

                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }

            return Ok(res);
        }


        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            try
            {
                //sb = new StringBuilder();
                //sb.AppendFormat("delete from Certificate where CertificateId = '" + id + "'");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);

                var certificate = _mySQLContext.Certificate.FirstOrDefault(x => x.CertificateId == id);
                if (certificate != null)
                {
                    _mySQLContext.Certificate.Remove(certificate);
                    var deleted = _mySQLContext.SaveChanges();
                    if (deleted > 0)
                    {
                        var admin = HttpContext.User.GetUserId();
                        _auditLogServices.LogHistory(new History {Admin=admin,Date=DateTime.Now,Description="Delete Certificate <br/> CertificateId : "+id,Id=id.ToString() });
                        res =
                       "{" +
                       "\"code\":\"1\"," +
                       "\"message\":\"Delete Data Success\"" +
                       "}";
                    }
                }
            }
            catch
            {
            }
            //finally
            //{
            //    sb = new StringBuilder();
            //    sb.AppendFormat("select * from Certificate where CertificateId  =" + id + ";");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);

            //    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //    o3 = JObject.Parse(result);

            //    if (o3.Count < 1)
            //    {

            //        string description = "Delete data Certificate" + "<br/>" +
            //        "CertificateId = " + id;

            //        _audit.ActionPost(UserId.ToString(), description, cuid.ToString());


            //        res =
            //            "{" +
            //            "\"code\":\"1\"," +
            //            "\"message\":\"Delete Data Success\"" +
            //            "}";
            //    }
            //}

            return Ok(res);
        }

        public async Task<string> RenderViewAsync<TModel>(string viewName, TModel model, bool partial = false)
        {
            if (string.IsNullOrEmpty(viewName))
            {
                viewName = this.ControllerContext.ActionDescriptor.ActionName;
            }

            this.ViewData.Model = model;

            using (var writer = new StringWriter())
            {
                IViewEngine viewEngine = this.HttpContext.RequestServices.GetService(typeof(ICompositeViewEngine)) as ICompositeViewEngine;
                ViewEngineResult viewResult = viewEngine.FindView(this.ControllerContext, viewName, !partial);

                if (viewResult.Success == false)
                {
                    return $"A view with the name {viewName} could not be found";
                }

                ViewContext viewContext = new ViewContext(
                    this.ControllerContext,
                    viewResult.View,
                    this.ViewData,
                    this.TempData,
                    writer,
                    new HtmlHelperOptions()
                );

                await viewResult.View.RenderAsync(viewContext);

                return writer.GetStringBuilder().ToString();
            }
        }

        private CertifiedDTO GetCertified(int cID)
        {
            CertifiedDTO certificate = null;

            var sb = new StringBuilder();
            sb.AppendFormat("select * from Certificate where CertificateId = '" + cID + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    certificate = new CertifiedDTO();
                    var encodedString = ds.Tables[0].Rows[0]["HTMLDesign"].ToString();
                    certificate.TitleTextHeader = ds.Tables[0].Rows[0]["TitleTextHeader"].ToString();
                    certificate.TitleHeaderDescription = ds.Tables[0].Rows[0]["TitleHeaderDescription"].ToString();
                    certificate.TitleTextFooter = ds.Tables[0].Rows[0]["TitleTextFooter"].ToString();
                    certificate.TitleCertificateNo = ds.Tables[0].Rows[0]["TitleCertificateNo"].ToString();
                    certificate.Code = ds.Tables[0].Rows[0]["Code"].ToString();
                    certificate.Name = ds.Tables[0].Rows[0]["Code"].ToString();
                    certificate.Year = ds.Tables[0].Rows[0]["Year"].ToString();
                    certificate.LanguageCertificate = ds.Tables[0].Rows[0]["LanguageCertificate"].ToString();
                    certificate.CertificateBackgroundId = ds.Tables[0].Rows[0]["CertificateBackgroundId"].ToString();
                    certificate.PartnerTypeId = ds.Tables[0].Rows[0]["PartnerTypeId"].ToString();
                    certificate.CertificateTypeId = ds.Tables[0].Rows[0]["CertificateTypeId"].ToString();
                    certificate.TitleEvent = ds.Tables[0].Rows[0]["TitleEvent"].ToString();
                    certificate.TitleStudentName = ds.Tables[0].Rows[0]["TitleStudentName"].ToString();
                    certificate.TitleCourseTitle = ds.Tables[0].Rows[0]["TitleCourseTitle"].ToString();
                    certificate.TitleProduct = ds.Tables[0].Rows[0]["TitleProduct"].ToString();
                    certificate.TitleInstructor = ds.Tables[0].Rows[0]["TitleInstructor"].ToString();
                    certificate.TitleDate = ds.Tables[0].Rows[0]["TitleDate"].ToString();
                    certificate.TitleDuration = ds.Tables[0].Rows[0]["TitleDuration"].ToString();
                    certificate.TitlePartner = ds.Tables[0].Rows[0]["TitlePartner"].ToString();
                    certificate.TitleInstitution = ds.Tables[0].Rows[0]["TitleInstitution"].ToString();
                    certificate.TitleLocation = ds.Tables[0].Rows[0]["TitleLocation"].ToString();
                    if (!String.IsNullOrEmpty(encodedString))
                    {
                        certificate.HTMLDesign = _pdfServices.DecodeBase64BitString(encodedString);
                    }
                }


            }
            return certificate;
        }

        private CertifiedDTO GetCertificate(int partnerTypeId, int certificateTypeId, int year, int languageCertificate)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("select * from Certificate where PartnerTypeId = '" + partnerTypeId + "' and CertificateTypeId = '" + certificateTypeId + "' and year = " + year + " and languageCertificate = " + languageCertificate);
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Certificate where PartnerTypeId = '" + partnerTypeId + "' and CertificateTypeId = '" + certificateTypeId + "' and languageCertificate = " + languageCertificate + " order by year desc");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }

            if (ds.Tables.Count == 0 || ds.Tables[0].Rows.Count == 0)
            {
                return null;
            }

            var certificate = new CertifiedDTO
            {
                TitleTextHeader = ds.Tables[0].Rows[0]["TitleTextHeader"].ToString(),
                TitleHeaderDescription = ds.Tables[0].Rows[0]["TitleHeaderDescription"].ToString(),
                TitleTextFooter = ds.Tables[0].Rows[0]["TitleTextFooter"].ToString(),
                TitleCertificateNo = ds.Tables[0].Rows[0]["TitleCertificateNo"].ToString(),
                Code = ds.Tables[0].Rows[0]["Code"].ToString(),
                Name = ds.Tables[0].Rows[0]["Code"].ToString(),
                Year = ds.Tables[0].Rows[0]["Year"].ToString(),
                LanguageCertificate = ds.Tables[0].Rows[0]["LanguageCertificate"].ToString(),
                CertificateBackgroundId = ds.Tables[0].Rows[0]["CertificateBackgroundId"].ToString(),
                PartnerTypeId = ds.Tables[0].Rows[0]["PartnerTypeId"].ToString(),
                CertificateTypeId = ds.Tables[0].Rows[0]["CertificateTypeId"].ToString(),
                TitleEvent = ds.Tables[0].Rows[0]["TitleEvent"].ToString(),
                TitleStudentName = ds.Tables[0].Rows[0]["TitleStudentName"].ToString(),
                TitleCourseTitle = ds.Tables[0].Rows[0]["TitleCourseTitle"].ToString(),
                TitleProduct = ds.Tables[0].Rows[0]["TitleProduct"].ToString(),
                TitleInstructor = ds.Tables[0].Rows[0]["TitleInstructor"].ToString(),
                TitleDate = ds.Tables[0].Rows[0]["TitleDate"].ToString(),
                TitleDuration = ds.Tables[0].Rows[0]["TitleDuration"].ToString(),
                TitlePartner = ds.Tables[0].Rows[0]["TitlePartner"].ToString(),
                TitleInstitution = ds.Tables[0].Rows[0]["TitleInstitution"].ToString(),
                TitleLocation = ds.Tables[0].Rows[0]["TitleLocation"].ToString()
            };

            var encodedString = ds.Tables[0].Rows[0]["HTMLDesign"].ToString();
            if (!String.IsNullOrEmpty(encodedString))
            {
                certificate.HTMLDesign = _pdfServices.DecodeBase64BitString(encodedString);
            }

            return certificate;
        }

        private CourseStudentDTO GetCourseStudent(int sId, string cOId)
        {
            CourseStudentDTO coursestudent = null;
            if (sId == 0 && cOId == "0")
            //if (true)
            {
                coursestudent = new CourseStudentDTO();
                coursestudent.Firstname = "Data:Student Name";
                coursestudent.Lastname = "";
                coursestudent.Name = "Data: Course Title";
                coursestudent.LocationName = "Data: Event Location";
                coursestudent.ContactName = "Data: Instructor Name";
                coursestudent.productName = "Data: Product Name";
                coursestudent.CompletionDate = "Data: Date";
                coursestudent.HoursTraining = "Data: Duration";
                coursestudent.SiteName = "Data: AUTODESK AUTHORIZED ACADEMIC PARTNER";
                coursestudent.CertNumber = "###";
                coursestudent.Institution = "Data: Institution Name";
            }
            else
            {
                var sb = new StringBuilder();
                sb.AppendFormat("SELECT cs.StudentID, cs.SurveyTaken, cs.CourseTaken, c.CourseId, c.HoursTraining, DATE_FORMAT(c.CompletionDate,'%d-%M-%Y') AS CompletionDate, DATE_FORMAT(c.StartDate,'%d/%m/%Y') AS StartDate, c.`Name`,");
                sb.AppendFormat("ca.ContactId, ca.ContactName,replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as SiteName, s.SiteAddress1, c.FYIndicatorKey, c.TrainingTypeKey,");
                sb.AppendFormat("c.ProjectTypeKey,c.EventTypeKey, CONCAT(IFNULL(p.productName,''),' ',IFNULL(pv.Version,'')) AS productName, LocationName,ts.Firstname, ts.Lastname, CONCAT(c.CourseId,ts.StudentID) AS CertNumber,Institution FROM CourseStudent cs ");
                sb.AppendFormat("INNER JOIN Courses c ON cs.CourseId = c.CourseId ");
                sb.AppendFormat("INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId ");
                sb.AppendFormat("INNER JOIN Main_site s ON c.SiteId = s.SiteId ");
                sb.AppendFormat("INNER JOIN CourseSoftware cf ON cs.CourseId = cf.CourseId ");
                sb.AppendFormat("LEFT JOIN ProductVersions pv ON cf.ProductVersionsId = pv.ProductVersionsId ");
                sb.AppendFormat("LEFT JOIN Products p ON cf.productId = p.productId ");
                sb.AppendFormat("INNER JOIN tblStudents ts on cs.StudentID = ts.StudentID ");
                sb.AppendFormat(" where cs.StudentID = '" + sId + "' ");
                sb.AppendFormat(" and cs.CourseId = '" + cOId + "'");
                sb.AppendFormat(" GROUP BY cs.StudentID");

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string encodeFirstName = ds.Tables[0].Rows[0]["Firstname"].ToString();
                        string encodeLastName = ds.Tables[0].Rows[0]["Lastname"].ToString();
                        string encodeContactName = ds.Tables[0].Rows[0]["ContactName"].ToString();
                        string encodeName = ds.Tables[0].Rows[0]["Name"].ToString();
                        encodeFirstName = commonServices.HtmlDecoder(encodeFirstName);
                        encodeLastName = commonServices.HtmlDecoder(encodeLastName);
                        encodeContactName = commonServices.HtmlDecoder(encodeContactName);
                        encodeName = commonServices.HtmlDecoder(encodeName);
                        coursestudent = new CourseStudentDTO();
                        coursestudent.Firstname = encodeFirstName;
                        coursestudent.Lastname = encodeLastName;
                        coursestudent.Name = encodeName;
                        //coursestudent.Firstname = ds.Tables[0].Rows[0]["Firstname"].ToString();
                        // coursestudent.Lastname = ds.Tables[0].Rows[0]["Lastname"].ToString();
                        //coursestudent.Name = ds.Tables[0].Rows[0]["Name"].ToString();
                        coursestudent.LocationName = ds.Tables[0].Rows[0]["LocationName"].ToString();
                        //coursestudent.ContactName = ds.Tables[0].Rows[0]["ContactName"].ToString();
                        coursestudent.ContactName = encodeContactName;
                        coursestudent.productName = ds.Tables[0].Rows[0]["productName"].ToString();
                        // coursestudent.StartDate = ds.Tables[0].Rows[0]["StartDate"].ToString();
                        coursestudent.CompletionDate = ds.Tables[0].Rows[0]["CompletionDate"].ToString();
                        coursestudent.HoursTraining = ds.Tables[0].Rows[0]["HoursTraining"].ToString();
                        coursestudent.SiteName = ds.Tables[0].Rows[0]["SiteName"].ToString();
                        coursestudent.CertNumber = ds.Tables[0].Rows[0]["CertNumber"].ToString();
                        coursestudent.Institution = ds.Tables[0].Rows[0]["Institution"].ToString();
                    }


                }
            }

            return coursestudent;
        }
        private byte[] GetCourseLogo(string cOId)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("select InstitutionLogo from Courses where CourseID ='" + cOId + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var logo = ds.Tables[0].Rows[0]["InstitutionLogo"];
                    if (logo != null && !string.IsNullOrEmpty(logo.ToString()))
                    {
                        return (byte[])logo;
                    }

                }


            }
            return null;
        }

        private byte[] GetOrgLogo(string cOId)
        {
            var sb = new StringBuilder();
            sb.AppendFormat("SELECT t.Name,t.Type,t.Content FROM OrgSiteAttachment t inner join Main_site s on  t.OrgID = s.OrgId inner join Courses c on c.SiteId = s.SiteId where c.CourseId = '" + cOId + "' and t.DocType = 'logo';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var logo = ds.Tables[0].Rows[0]["Content"];
                    if (logo != null && !string.IsNullOrEmpty(logo.ToString()))
                    {
                        return (byte[])logo;
                    }

                }


            }
            return null;
        }

        [HttpGet("CertificateDownload/where/{partnerTypeId}/{certificateTypeId}/{year}/{languageCertificate}/{sId}/{cOId}")]
        public void NewDownloadCertificate(int partnerTypeId, int certificateTypeId, int year, int languageCertificate, int sId, string cOId)
        {
            if (partnerTypeId <= 0 || sId <= 0 || string.IsNullOrEmpty(cOId))
            {
                return;
            }

            this.HttpContext.Response.ContentType = "application/pdf";
            var log = Code.Utilities.SetHistoryData(cOId, "System", AutodeskConst.LogDescription.Insert, "History", "Log befor generate pdf");
            _auditLogServices.LogHistory(log);
            GetCertificatePdf(partnerTypeId, certificateTypeId, year, languageCertificate, sId, cOId);
        }

        [HttpGet("CertificateDownload/where/{partnerType}/{year}/{languageCertificate}/{sId}/{cOId}")]
        public void StudentDownloadCertificate(string partnerType, int year, int languageCertificate, int sId, string cOId)
        {
            if (string.IsNullOrEmpty(partnerType) || sId <= 0 || string.IsNullOrEmpty(cOId))
            {
                return;
            }

            var partnerTypeId = 1;
            var certificateTypeId = 0;
            switch (partnerType)
            {
                case "AAP Course":
                    partnerTypeId = 58;
                    certificateTypeId = 1;
                    break;
                case "AAP Project":
                    partnerTypeId = 58;
                    certificateTypeId = 2;
                    break;
                case "AAP Event":
                    partnerTypeId = 58;
                    certificateTypeId = 3;
                    break;

                default:
                    partnerTypeId = 1;
                    certificateTypeId = 0;
                    break;
            }

            this.HttpContext.Response.ContentType = "application/pdf";

            GetCertificatePdf(partnerTypeId, certificateTypeId, year, languageCertificate, sId, cOId);
        }

        [HttpGet("EmailCertificate/{partnerType}/{year}/{languageCertificate}/{sId}/{cOId}")]
        public async Task<IActionResult> EmailCertificate(string partnerType, int year, int languageCertificate, int sId, string cOId)
        {
            if (string.IsNullOrEmpty(partnerType) || sId <= 0 || string.IsNullOrEmpty(cOId))
            {
                return NoContent();
            }

            var partnerTypeId = 1;
            var certificateTypeId = 0;
            switch (partnerType)
            {
                case "AAP Course":
                    partnerTypeId = 58;
                    certificateTypeId = 1;
                    break;
                case "AAP Project":
                    partnerTypeId = 58;
                    certificateTypeId = 2;
                    break;
                case "AAP Event":
                    partnerTypeId = 58;
                    certificateTypeId = 3;
                    break;

                default:
                    partnerTypeId = 1;
                    certificateTypeId = 0;
                    break;
            }

            var certificate = GetCertificatePdfByteArray(partnerTypeId, certificateTypeId, year, languageCertificate, sId, cOId);

            var student = _mySQLContext.TblStudents.FirstOrDefault(x => x.StudentId == sId);
            if (student != null)
            {
                string fileName = partnerType + "_" + student.Firstname + "_" + student.Lastname + "_" + student.Email + ".pdf";
                await SendCertificateToUser(fileName, student.Email, certificate);
            }

            return Ok();
        }

        private async Task<bool> SendCertificateToUser(string fileName, string email, byte[] data)
        {
            try
            {

                if (data != null)
                {
                    var listAttachment = new List<Attachment>();
                    var attachment = new Attachment
                    {
                        Content = Convert.ToBase64String(data),
                        Filename = fileName,
                        Type = Code.Utilities.GetMimeType(".pdf"),
                        Disposition = "attachment"

                    };
                    listAttachment.Add(attachment);

                    var emailBodyType = _mySQLContext.EmailBodyType.FirstOrDefault(x => x.EmailBodyTypeName == "Email Certificate");
                    if (emailBodyType != null)
                    {
                        var emailBody = _mySQLContext.EmailBody.FirstOrDefault(x => x.BodyType == emailBodyType.EmailBodyTypeId.ToString());
                        if (emailBody != null)
                        {
                            string subject = emailBody.Subject;
                            string body = emailBody.BodyEmail;
                            var studentFullName = _mySQLContext.TblStudents.FirstOrDefault(x => x.Email == email);
                            if (studentFullName != null)
                            {
                                body = body.Replace("[FullName]", studentFullName.Firstname + " " + studentFullName.Lastname);
                            }
                            else
                            {
                                body = body.Replace("[FullName]", email);
                            }

                            await _emailService.SendMailAsync(email, subject, body, null, listAttachment);
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {

                return false;
            }

        }
        private void GetCertificatePdf(int partnerTypeId, int certificateTypeId, int year, int languageCertificate, int sId, string cOId)
        {
            try
            {
                var webRootPath = _hostingEnvironment.WebRootPath;

                var certificate = GetCertificate(partnerTypeId, certificateTypeId, year, languageCertificate);
                var log = Code.Utilities.SetHistoryData(cOId, "System", AutodeskConst.LogDescription.Insert, "History", $"GetCertificate result:{JsonConvert.SerializeObject(certificate)}");
                _auditLogServices.LogHistory(log);

                var coursestudent = GetCourseStudent(sId, cOId);
                log = Code.Utilities.SetHistoryData(cOId, "System", AutodeskConst.LogDescription.Insert, "History", $"GetCourseStudent result:{JsonConvert.SerializeObject(coursestudent)}");
                _auditLogServices.LogHistory(log);
                if (certificate == null)
                {
                    return;
                }

                if (coursestudent == null)
                {
                    return;
                }

                if (string.IsNullOrEmpty(certificate.HTMLDesign))
                {
                    return;
                }
                log = Code.Utilities.SetHistoryData(cOId, "System", AutodeskConst.LogDescription.Insert, "History", "Log before generate PDF");

                _auditLogServices.LogHistory(log);
                #region Generate Pdf

                var pdfDoc = new iTextSharp.text.Document(PageSize.A4.Rotate(), 0f, 0f, 0f, 0f);
                var htmlparser = new HTMLWorker(pdfDoc);
                var writer = PdfWriter.GetInstance(pdfDoc, this.HttpContext.Response.Body);
                writer.SetEncryption(
                // null user password => users can open document __without__ pasword
                null,
                // owner password => required to __modify__ document/permissions        
                System.Text.Encoding.UTF8.GetBytes("e6N[%cxSd+6t%!CE"),
                /*
                 * bitwise or => see iText API for permission parameter:
                 * http://api.itextpdf.com/itext/com/itextpdf/text/pdf/PdfWriter.html
                 */
                PdfWriter.ALLOW_PRINTING
                ,
                // encryption level also in documentation referenced above        
                PdfWriter.ENCRYPTION_AES_128
              );
                PdfPageEvent pageevent = new PdfPageEvent();
                pageevent.PrintTime = DateTime.Now;
                pageevent.webRootPath = webRootPath;

                var doc = new HtmlDocument();
                doc.LoadHtml(certificate.HTMLDesign);
                var element = doc.GetElementbyId("content_background");
                if (element != null)
                {
                    var src = element.Attributes["src"];
                    pageevent.bgimg = src.Value.Replace("assets/certificate/", "");
                    pageevent.bgimg = pageevent.bgimg.Replace("/", "");
                    pageevent.bgImgInByte = GetBackgroundImage(pageevent.bgimg);
                }
                element = doc.GetElementbyId("content_logo");
                if (element != null)
                {
                    var logo = GetCourseLogo(cOId);
                    if (logo != null)
                    {
                        pageevent.logoimg = logo;

                        var x = ParseFloat(element.Attributes["data-x"]);
                        var y = ParseFloat(element.Attributes["data-y"]);
                        pageevent.logox = x;
                        pageevent.logoy = y;
                    }



                }
                //element = doc.GetElementbyId("orglogo");
                //if (element != null)
                //{
                //    var logo = GetOrgLogo(cOId);
                //    if (logo != null)
                //    {
                //        pageevent.orglogoimg = logo;

                //        var x = ParseFloat(element.Attributes["data-x"]);
                //        var y = ParseFloat(element.Attributes["data-y"]);
                //        pageevent.logox = x;
                //        pageevent.logoy = y;
                //    }



                //}
                adjusttitlex = -11f;
                adjusttextx = -11f;

                ArtifaktBlack = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Legend-Black.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                ArtifaktBook = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Legend-Book.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                ArtifaktRegular = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Legend-Regular.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                ArtifaktBold = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Legend-Bold.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                ArtifaktElRegular = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Element-Regular.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                Stsong = BaseFont.CreateFont(webRootPath + @"\fonts\stsong\chinese-stsong.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);// Chinese Simplified
                Heisei = BaseFont.CreateFont(webRootPath + @"\fonts\heisei\Heisei Mincho Std W3.otf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);// Japan
                Hygothic = BaseFont.CreateFont(webRootPath + @"\fonts\hygothic-medium\korean-h2gtrm.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);// Korean
                Angsana = BaseFont.CreateFont(webRootPath + @"\fonts\th\angsana.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);// Chinese Simplified

                var textheaderdescription = "";
                List<Font> fonts = new List<Font>();
                FontSelector fontSelector = new FontSelector();
                fonts.Add(new Font(ArtifaktElRegular));
                fonts.Add(new Font(ArtifaktBlack));
                fonts.Add(new Font(ArtifaktBook));
                fonts.Add(new Font(ArtifaktRegular));
                fonts.Add(new Font(ArtifaktElRegular));
                fonts.Add(new Font(Stsong));
                fonts.Add(new Font(Heisei));
                fonts.Add(new Font(Hygothic));
                fonts.Add(new Font(Angsana));
                //fonts.Add(new Font(BaseFont.CreateFont("MSung-Light", "UniCNS-UCS2-H", BaseFont.NOT_EMBEDDED), 10)); // Chinese Traditional
                foreach (Font f in fonts)
                {
                    fontSelector.AddFont(f);
                }


                writer.PageEvent = pageevent;
                var pageSize = pdfDoc.PageSize;

                pdfDoc.Open();

                var cb = writer.DirectContent;
                cb.BeginText();


                element = doc.GetElementbyId("textheader");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleTextHeader, elementtype = elementtype.herder });
                }

                element = doc.GetElementbyId("textheaderdescription");
                if (element != null)
                {
                    textheaderdescription = certificate.TitleHeaderDescription;
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleHeaderDescription, elementtype = elementtype.description });
                }

                element = doc.GetElementbyId("titlecertstudent");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleStudentName, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certstudent");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.Firstname + " " + coursestudent.Lastname, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("certstudenttag");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.LocationName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecerteventlocation");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleLocation, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certeventlocation");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.LocationName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertno");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleCertificateNo, elementtype = elementtype.cartnotitle });
                }

                element = doc.GetElementbyId("certno");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.CertNumber, elementtype = elementtype.cartno });
                }

                element = doc.GetElementbyId("titlecertevent");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleEvent, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certevent");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.Name, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertdate");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleDate, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certdate");
                if (element != null)
                {
                    // elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.StartDate, elementtype = elementtype.text });
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.CompletionDate, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertpartner");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitlePartner, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certpartner");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.SiteName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertproduct");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleProduct, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certproduct");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.productName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertinstructor");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleInstructor, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certinstructor");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.ContactName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecerttitle");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleCourseTitle, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certtitle");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.Name, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertinstitution");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleInstitution, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certinstitution");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.Institution, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertduration");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleDuration, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certduration");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.HoursTraining, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("yeartitle");
                if (element != null)
                {
                    //elements.Add(new ElementNodeDTO() { element = element, text = certificate.Year, elementtype = elementtype.year });
                    if (Convert.ToInt32(certificate.Year) < Convert.ToInt32(DateTime.Now.ToString("yyyy")))
                    {
                        elements.Add(new ElementNodeDTO() { element = element, text = DateTime.Now.ToString("yyyy"), elementtype = elementtype.year });
                    }
                    else
                    {
                        elements.Add(new ElementNodeDTO() { element = element, text = certificate.Year, elementtype = elementtype.year });
                    }
                    var x = ParseFloat(element.Attributes["data-x"]);
                    var y = ParseFloat(element.Attributes["data-y"]);

                    if (x < 50 & y < 500)
                    {
                        reportype = reportype.type1; //172
                    }
                    else if (x < 60 & y < 500)
                    {
                        reportype = reportype.type5;
                    }
                    else if (x < 300 & y < 400)
                    {
                        reportype = reportype.type3;//173
                    }
                    else if (x < 400 & y < 500)
                    {
                        reportype = reportype.type2;//185
                    }
                    else if (x < 700 & y < 500)
                    {
                        reportype = reportype.type4;
                    }
                }

                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type1, x = 43, y = 522 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type2, x = 329, y = 500 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type3, x = 248, y = 331 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type4, x = 652, y = 450 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type5, x = 55, y = 516 });
                var yearx = 0f;
                var yeary = 0f;
                var initcell = initcells.Where(w => w.elementtype == elementtype.year & w.reportype == reportype).FirstOrDefault();
                if (initcell != null)
                {
                    if (initcell.x.HasValue)
                    {
                        yearx = initcell.x.Value - 7f;
                    }

                    if (initcell.y.HasValue)
                    {
                        yeary = initcell.y.Value - 7f;
                    }

                    temptitlex.Add(yearx);
                    tempx.Add(yearx);
                }



                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type1, x = yearx, y = 255 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type2, x = yearx, y = 115 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type3, x = yearx, y = 125 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type4, x = yearx, y = 255 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type5, x = yearx, y = 255 });

                var basefont = getBasefont(fontSelector, textheaderdescription);
                if (basefont == Hygothic)
                {
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type1, x = yearx, y = 277, maxlenght = 28 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type2, x = yearx, y = 140, maxlenght = 52 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type3, x = yearx, y = 145, maxlenght = 55 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type4, x = yearx, y = 277, maxlenght = 115 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type5, x = yearx, y = 277, maxlenght = 35 });
                }
                else if (basefont == Stsong)
                {
                    fonts = new List<Font>();
                    fontSelector = new FontSelector();
                    fonts.Add(new Font(Stsong));
                    fonts.Add(new Font(ArtifaktElRegular));
                    fonts.Add(new Font(ArtifaktBlack));
                    fonts.Add(new Font(ArtifaktBook));
                    fonts.Add(new Font(ArtifaktRegular));
                    fonts.Add(new Font(ArtifaktElRegular));
                    foreach (Font f in fonts)
                    {
                        fontSelector.AddFont(f);
                    }

                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type1, x = yearx, y = 277, maxlenght = 62 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type2, x = yearx, y = 140, maxlenght = 80 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type3, x = yearx, y = 145, maxlenght = 110 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type4, x = yearx, y = 277, maxlenght = 115 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type5, x = yearx, y = 277, maxlenght = 35 });
                }
                else
                {
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type1, x = yearx, y = 277, maxlenght = 49 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type2, x = yearx, y = 140, maxlenght = 82 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type3, x = yearx, y = 145, maxlenght = 92 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type4, x = yearx, y = 277, maxlenght = 125 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type5, x = yearx, y = 277, maxlenght = 48 });
                }
                element = doc.GetElementbyId("titlecertno");
                if (element != null)
                {
                    var x = ParseFloat(element.Attributes["data-x"]);
                    var y = ParseFloat(element.Attributes["data-y"]);
                    var newcerx = yearx;
                    var newcery = y;
                    var basefont2 = getBasefont(fontSelector, certificate.TitleCertificateNo);
                    if (x - yearx > 90)
                    {
                        newcerx = x;
                        newcery = y;
                    }
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type1, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type2, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type3, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type4, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type5, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                }

                foreach (var e in elements.OrderBy(s => ParseFloat(s.element.Attributes["data-y"])))
                {
                    drawelement(e.element, pageSize, cb, fontSelector, e.text, e.elementtype);
                }

                foreach (var y in temptitley)
                {
                    cb.EndText();

                    cb.SetLineWidth(0.1);
                    cb.SetColorStroke(BaseColor.LIGHT_GRAY);
                    cb.Rectangle(firsttextx, pageSize.GetTop(y - 13), 460, 0.1f);
                    cb.Stroke();
                    cb.BeginText();

                }
                cb.EndText();
                #endregion
                log = Code.Utilities.SetHistoryData(cOId, "System", AutodeskConst.LogDescription.Insert, "History", "Generate Pdf successfully");

                _auditLogServices.LogHistory(log);

                pdfDoc.Close();


            }
            catch (Exception ex)
            {
                var log = Code.Utilities.SetHistoryData(cOId, "System", AutodeskConst.LogDescription.Insert, "History", $"Exception when generate :{ex.Message}");

                _auditLogServices.LogHistory(log);

            }
        }

        private byte[] GetCertificatePdfByteArray(int partnerTypeId, int certificateTypeId, int year, int languageCertificate, int sId, string cOId)
        {
            using (var myMemoryStream = new MemoryStream())
            {
                var webRootPath = _hostingEnvironment.WebRootPath;
                var certificate = GetCertificate(partnerTypeId, certificateTypeId, year, languageCertificate);
                var coursestudent = GetCourseStudent(sId, cOId);

                if (certificate == null || coursestudent == null || string.IsNullOrEmpty(certificate.HTMLDesign))
                {
                    return null;
                }

                var pdfDoc = new iTextSharp.text.Document(PageSize.A4.Rotate(), 0f, 0f, 0f, 0f);
                var htmlparser = new HTMLWorker(pdfDoc);
                var writer = PdfWriter.GetInstance(pdfDoc, myMemoryStream);
                writer.SetEncryption(
                // null user password => users can open document __without__ pasword
                null,
                // owner password => required to __modify__ document/permissions        
                System.Text.Encoding.UTF8.GetBytes("e6N[%cxSd+6t%!CE"),
                /*
                 * bitwise or => see iText API for permission parameter:
                 * http://api.itextpdf.com/itext/com/itextpdf/text/pdf/PdfWriter.html
                 */
                PdfWriter.ALLOW_PRINTING
                ,
                // encryption level also in documentation referenced above        
                PdfWriter.ENCRYPTION_AES_128
              );
                PdfPageEvent pageevent = new PdfPageEvent();
                pageevent.PrintTime = DateTime.Now;
                pageevent.webRootPath = webRootPath;

                var doc = new HtmlDocument();
                doc.LoadHtml(certificate.HTMLDesign);
                var element = doc.GetElementbyId("content_background");
                if (element != null)
                {
                    var src = element.Attributes["src"];
                    pageevent.bgimg = src.Value.Replace("assets/certificate/", "");
                    pageevent.bgimg = pageevent.bgimg.Replace("/", "");
                    pageevent.bgImgInByte = GetBackgroundImage(pageevent.bgimg);
                }
                element = doc.GetElementbyId("content_logo");
                if (element != null)
                {
                    var logo = GetCourseLogo(cOId);
                    if (logo != null)
                    {
                        pageevent.logoimg = logo;

                        var x = ParseFloat(element.Attributes["data-x"]);
                        var y = ParseFloat(element.Attributes["data-y"]);
                        pageevent.logox = x;
                        pageevent.logoy = y;
                    }
                }
                adjusttitlex = -11f;
                adjusttextx = -11f;

                ArtifaktBlack = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Legend-Black.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                ArtifaktBook = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Legend-Book.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                ArtifaktRegular = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Legend-Regular.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                ArtifaktBold = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Legend-Bold.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                ArtifaktElRegular = BaseFont.CreateFont(webRootPath + @"\fonts\artifakt\Artifakt-Element-Regular.otf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
                Stsong = BaseFont.CreateFont(webRootPath + @"\fonts\stsong\chinese-stsong.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);// Chinese Simplified
                Heisei = BaseFont.CreateFont(webRootPath + @"\fonts\heisei\Heisei Mincho Std W3.otf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);// Japan
                Hygothic = BaseFont.CreateFont(webRootPath + @"\fonts\hygothic-medium\korean-h2gtrm.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);// Korean
                Angsana = BaseFont.CreateFont(webRootPath + @"\fonts\th\angsana.ttf", BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED);// Chinese Simplified

                var textheaderdescription = "";
                List<Font> fonts = new List<Font>();
                FontSelector fontSelector = new FontSelector();
                fonts.Add(new Font(ArtifaktElRegular));
                fonts.Add(new Font(ArtifaktBlack));
                fonts.Add(new Font(ArtifaktBook));
                fonts.Add(new Font(ArtifaktRegular));
                fonts.Add(new Font(ArtifaktElRegular));
                fonts.Add(new Font(Stsong));
                fonts.Add(new Font(Heisei));
                fonts.Add(new Font(Hygothic));
                fonts.Add(new Font(Angsana));
                foreach (Font f in fonts)
                {
                    fontSelector.AddFont(f);
                }


                writer.PageEvent = pageevent;
                var pageSize = pdfDoc.PageSize;

                pdfDoc.Open();

                var cb = writer.DirectContent;
                cb.BeginText();

                elements = new List<ElementNodeDTO>();

                element = doc.GetElementbyId("textheader");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleTextHeader, elementtype = elementtype.herder });
                }

                element = doc.GetElementbyId("textheaderdescription");
                if (element != null)
                {
                    textheaderdescription = certificate.TitleHeaderDescription;
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleHeaderDescription, elementtype = elementtype.description });
                }

                element = doc.GetElementbyId("titlecertstudent");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleStudentName, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certstudent");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.Firstname + " " + coursestudent.Lastname, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("certstudenttag");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.LocationName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecerteventlocation");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleLocation, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certeventlocation");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.LocationName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertno");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleCertificateNo, elementtype = elementtype.cartnotitle });
                }

                element = doc.GetElementbyId("certno");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.CertNumber, elementtype = elementtype.cartno });
                }

                element = doc.GetElementbyId("titlecertevent");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleEvent, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certevent");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.Name, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertdate");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleDate, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certdate");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.CompletionDate, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertpartner");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitlePartner, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certpartner");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.SiteName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertproduct");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleProduct, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certproduct");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.productName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertinstructor");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleInstructor, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certinstructor");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.ContactName, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecerttitle");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleCourseTitle, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certtitle");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.Name, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertinstitution");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleInstitution, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certinstitution");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.Institution, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("titlecertduration");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = certificate.TitleDuration, elementtype = elementtype.texttitle });
                }

                element = doc.GetElementbyId("certduration");
                if (element != null)
                {
                    elements.Add(new ElementNodeDTO() { element = element, text = coursestudent.HoursTraining, elementtype = elementtype.text });
                }

                element = doc.GetElementbyId("yeartitle");
                if (element != null)
                {
                    //elements.Add(new ElementNodeDTO() { element = element, text = certificate.Year, elementtype = elementtype.year });
                    if (Convert.ToInt32(certificate.Year) < Convert.ToInt32(DateTime.Now.ToString("yyyy")))
                    {
                        elements.Add(new ElementNodeDTO() { element = element, text = DateTime.Now.ToString("yyyy"), elementtype = elementtype.year });
                    }
                    else
                    {
                        elements.Add(new ElementNodeDTO() { element = element, text = certificate.Year, elementtype = elementtype.year });
                    }
                    var x = ParseFloat(element.Attributes["data-x"]);
                    var y = ParseFloat(element.Attributes["data-y"]);

                    if (x < 50 & y < 500)
                    {
                        reportype = reportype.type1; //172
                    }
                    else if (x < 60 & y < 500)
                    {
                        reportype = reportype.type5;
                    }
                    else if (x < 300 & y < 400)
                    {
                        reportype = reportype.type3;//173
                    }
                    else if (x < 400 & y < 500)
                    {
                        reportype = reportype.type2;//185
                    }
                    else if (x < 700 & y < 500)
                    {
                        reportype = reportype.type4;
                    }
                }

                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type1, x = 43, y = 522 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type2, x = 329, y = 500 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type3, x = 248, y = 331 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type4, x = 652, y = 450 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.year, reportype = reportype.type5, x = 55, y = 516 });
                var yearx = 0f;
                var yeary = 0f;
                var initcell = initcells.Where(w => w.elementtype == elementtype.year & w.reportype == reportype).FirstOrDefault();
                if (initcell != null)
                {
                    if (initcell.x.HasValue)
                    {
                        yearx = initcell.x.Value - 7f;
                    }

                    if (initcell.y.HasValue)
                    {
                        yeary = initcell.y.Value - 7f;
                    }

                    temptitlex.Add(yearx);
                    tempx.Add(yearx);
                }



                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type1, x = yearx, y = 255 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type2, x = yearx, y = 115 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type3, x = yearx, y = 125 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type4, x = yearx, y = 255 });
                initcells.Add(new CellNodeDTO() { elementtype = elementtype.herder, reportype = reportype.type5, x = yearx, y = 255 });

                var basefont = getBasefont(fontSelector, textheaderdescription);
                if (basefont == Hygothic)
                {
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type1, x = yearx, y = 277, maxlenght = 28 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type2, x = yearx, y = 140, maxlenght = 52 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type3, x = yearx, y = 145, maxlenght = 55 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type4, x = yearx, y = 277, maxlenght = 115 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type5, x = yearx, y = 277, maxlenght = 35 });
                }
                else if (basefont == Stsong)
                {
                    fonts = new List<Font>();
                    fontSelector = new FontSelector();
                    fonts.Add(new Font(Stsong));
                    fonts.Add(new Font(ArtifaktElRegular));
                    fonts.Add(new Font(ArtifaktBlack));
                    fonts.Add(new Font(ArtifaktBook));
                    fonts.Add(new Font(ArtifaktRegular));
                    fonts.Add(new Font(ArtifaktElRegular));
                    foreach (Font f in fonts)
                    {
                        fontSelector.AddFont(f);
                    }

                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type1, x = yearx, y = 277, maxlenght = 62 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type2, x = yearx, y = 140, maxlenght = 80 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type3, x = yearx, y = 145, maxlenght = 110 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type4, x = yearx, y = 277, maxlenght = 115 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type5, x = yearx, y = 277, maxlenght = 35 });
                }
                else
                {
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type1, x = yearx, y = 277, maxlenght = 49 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type2, x = yearx, y = 140, maxlenght = 82 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type3, x = yearx, y = 145, maxlenght = 92 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type4, x = yearx, y = 277, maxlenght = 125 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.description, reportype = reportype.type5, x = yearx, y = 277, maxlenght = 48 });
                }
                element = doc.GetElementbyId("titlecertno");
                if (element != null)
                {
                    var x = ParseFloat(element.Attributes["data-x"]);
                    var y = ParseFloat(element.Attributes["data-y"]);
                    var newcerx = yearx;
                    var newcery = y;
                    var basefont2 = getBasefont(fontSelector, certificate.TitleCertificateNo);
                    if (x - yearx > 90)
                    {
                        newcerx = x;
                        newcery = y;
                    }
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type1, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type2, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type3, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type4, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                    initcells.Add(new CellNodeDTO() { elementtype = elementtype.cartnotitle, reportype = reportype.type5, x = newcerx, y = newcery, text = certificate.TitleCertificateNo, basefont = basefont2 });
                }

                foreach (var e in elements.OrderBy(s => ParseFloat(s.element.Attributes["data-y"])))
                {
                    drawelement(e.element, pageSize, cb, fontSelector, e.text, e.elementtype);
                }

                foreach (var y in temptitley)
                {
                    cb.EndText();

                    cb.SetLineWidth(0.1);
                    cb.SetColorStroke(BaseColor.LIGHT_GRAY);
                    cb.Rectangle(firsttextx, pageSize.GetTop(y - 13), 460, 0.1f);
                    cb.Stroke();
                    cb.BeginText();

                }
                cb.EndText();

                pdfDoc.Close();

                return myMemoryStream.ToArray();
            }
        }

        //[HttpGet("BatchDownload/{partnerTypeId}/{certificateTypeId}/{year}/{languageCertificate}/{sId}/{cOId}")]
        [HttpPost("BatchDownload")]
        public IActionResult GetZip([FromBody] BatchDownloadDto data)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                MySQLContext context = new MySQLContext();
                using (var memoryStream = new MemoryStream())
                {
                    using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        foreach (var sId in data.StudentIds)
                        {
                            var studentId = Convert.ToInt32(sId);
                            //var fileName = _mySQLContext.TblStudents.Where(s => s.StudentId == studentId).Select(s => s.Firstname + "_" + s.Lastname + "_" + s.Email + ".pdf").FirstOrDefault();
                            var student = _mySQLContext.TblStudents.Where(s => s.StudentId == studentId).FirstOrDefault();
                            if (student != null)
                            {
                                var fileName = student.Firstname + "_" + student.Lastname + "_" + student.Email + ".pdf";
                                var byteValue = GetCertificatePdfByteArray(data.PartnerTypeId, data.CertificateTypeId, Convert.ToInt32(data.Year), Convert.ToInt32(data.LanguageCertificate), studentId, data.CourseId);

                                var zipArchiveEntry = archive.CreateEntry(fileName, CompressionLevel.Fastest);
                                using (var zipStream = zipArchiveEntry.Open())
                                {
                                    zipStream.Write(byteValue, 0, byteValue.Length);
                                }
                            }
                        }
                    }
                    memoryStream.Position = 0;
                    memoryStream.CopyTo(ms);
                }

                return File(ms.ToArray(), "application/zip");
            }
            catch (Exception)
            {
                throw new Exception("Can't zip files");
            }
        }

        private byte[] GetBackgroundImage(string bgimg)
        {
            var defaultPath = AppConfig.Config["CertificateBackground:CertificateDirectory"];
            var certPath = Path.Combine(Directory.GetCurrentDirectory(), defaultPath, bgimg);
            var fi = new FileInfo(certPath);
            byte[] content = null;
            if (fi.Exists)
            {
                using (var fStr = new FileStream(certPath, FileMode.Open, FileAccess.Read))
                {
                    using (var memStr = new MemoryStream())
                    {
                        fStr.CopyTo(memStr);
                        content = memStr.ToArray();
                    }
                }
            }
            //if (_backgroundImages == null)
            //{
            //    _backgroundImages = new List<Tuple<string, byte[]>>();
            //}

            //if (_backgroundImages.Any(b => b.Item1 == bgimg))
            //{
            //    return _backgroundImages.Where(b => b.Item1 == bgimg).Select(b => b.Item2).First();
            //}

            //sqlCommand = new MySqlCommand("select BackgroundImageInBlob from CertificateBackground where `code` = '" + bgimg + "'");
            //ds = oDb.getDataSetFromSP(sqlCommand);
            //if (ds == null || ds.Tables == null || ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0 || ds.Tables[0].Rows[0][0] == DBNull.Value)
            //{
            //    return null;
            //}
            //else
            //{
            //    var value = (Byte[])(ds.Tables[0].Rows[0][0]);
            //    _backgroundImages.Add(new Tuple<string, byte[]>(bgimg, value));
            //    return value;
            //}

            return content;
        }

        List<string> WrapText(string text, int maxWidth)
        {
            List<string> wrappedLines = new List<string>();
            StringBuilder actualLine = new StringBuilder();
            var splite = text.Split(" ");
            foreach (var t in splite)
            {
                var text2 = actualLine.ToString() + " " + t;
                if (text2.Length > maxWidth)
                {
                    wrappedLines.Add(actualLine.ToString());
                    actualLine = new StringBuilder();
                    actualLine.Append(t);
                    actualLine.Append(" ");
                }
                else
                {
                    actualLine.Append(t);
                    actualLine.Append(" ");
                }
            }
            if (actualLine.ToString().Length > 0)
            {
                wrappedLines.Add(actualLine.ToString());
            }

            return wrappedLines;
        }
        public static float ParseFloat(HtmlAttribute obj)
        {

            float d = 0;
            if (obj != null)
            {
                try
                {
                    var s = obj.Value.ToString();
                    if (!string.IsNullOrEmpty(s))
                    {
                        return float.Parse(s.Trim().Replace(",", ""));
                    }

                }
                catch
                {
                    return d;
                }

            }

            return d;
        }
        private string getelement(string s1, string sstr, string estr)
        {
            var element = "";
            var i = s1.IndexOf(sstr);
            while (1 == 1)
            {
                var ci = s1.Substring(i, 1);
                element += ci;

                if (ci == estr)
                {
                    break;
                }

                i++;
            }
            return element;
        }
        private void drawelement(HtmlNode element, Rectangle pageSize, PdfContentByte cb, FontSelector fontSelector, string text, elementtype etype)
        {

            var style = element.Attributes["style"];

            if (style == null || !style.Value.Contains("hidden"))
            {
                var x = ParseFloat(element.Attributes["data-x"]);
                var y = ParseFloat(element.Attributes["data-y"]);
                var orgx = x;
                text = text.Replace("\\", "");
                text = text.Replace("&nbsp;", " ");

                //text += " 5858585858585858";
                if (etype == elementtype.herder)
                {

                    var initcell = initcells.Where(w => w.elementtype == elementtype.herder & w.reportype == reportype).FirstOrDefault();
                    if (initcell != null)
                    {
                        if (initcell.x.HasValue)
                        {
                            x = initcell.x.Value;
                        }

                        if (initcell.y.HasValue)
                        {
                            y = initcell.y.Value;
                        }
                    }
                    x = adjustpositionx(temptitlex, x);
                    drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, text, etype);
                }
                else if (etype == elementtype.description)
                {
                    var maxlenght = 40;
                    var initcell = initcells.Where(w => w.elementtype == elementtype.description & w.reportype == reportype).FirstOrDefault();
                    if (initcell != null)
                    {
                        if (initcell.x.HasValue)
                        {
                            x = initcell.x.Value;
                        }

                        if (initcell.y.HasValue)
                        {
                            y = initcell.y.Value;
                        }

                        if (initcell.maxlenght.HasValue)
                        {
                            maxlenght = initcell.maxlenght.Value;
                        }
                    }

                    x = adjustpositionx(temptitlex, x);
                    orgx = x;

                    List<string> wrappedLines = WrapText(text, maxlenght);
                    if (wrappedLines.Count > 1)
                    {
                        foreach (var line in wrappedLines)
                        {
                            drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, line, etype);
                            x = orgx;
                            y += 17;
                        }
                    }
                    else
                    {
                        drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, text, etype);
                    }
                }
                else if (etype == elementtype.year)
                {
                    var initcell = initcells.Where(w => w.elementtype == elementtype.year & w.reportype == reportype).FirstOrDefault();
                    if (initcell != null)
                    {
                        if (initcell.x.HasValue)
                        {
                            x = initcell.x.Value;
                        }

                        if (initcell.y.HasValue)
                        {
                            y = initcell.y.Value;
                        }
                    }
                    drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, text, etype);
                }
                else if (etype == elementtype.cartnotitle)
                {
                    var initcell = initcells.Where(w => w.elementtype == elementtype.cartnotitle & w.reportype == reportype).FirstOrDefault();
                    if (initcell != null)
                    {
                        if (initcell.x.HasValue)
                        {
                            x = initcell.x.Value;
                        }

                        if (initcell.y.HasValue)
                        {
                            y = initcell.y.Value;
                        }
                    }
                    x = adjustpositionx(temptitlex, x);
                    drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, text, etype);
                }
                else if (etype == elementtype.cartno)
                {
                    var initcell = initcells.Where(w => w.elementtype == elementtype.cartnotitle & w.reportype == reportype).FirstOrDefault();
                    if (initcell != null)
                    {
                        if (initcell.y.HasValue)
                        {
                            var dif1 = y - initcell.y.Value;
                            var dif2 = initcell.y.Value - y;
                            if (dif1 < 0)
                            {
                                dif1 = dif1 * -1;
                            }

                            if (dif2 < 0)
                            {
                                dif2 = dif2 * -1;
                            }

                            if (dif1 < 20 | dif2 < 20)
                            {
                                if (initcell.x.HasValue && !string.IsNullOrEmpty(initcell.text))
                                {
                                    if (initcell.basefont == Hygothic)
                                    {
                                        x = initcell.x.Value + (initcell.text.Length * 15f);
                                    }
                                    else
                                    {
                                        x = initcell.x.Value + (initcell.text.Length * 8f);
                                    }

                                    if (initcell.y.HasValue)
                                    {
                                        y = initcell.y.Value;
                                    }
                                }

                            }
                            else
                            {
                                if (initcell.x.HasValue)
                                {
                                    x = initcell.x.Value;
                                }
                            }
                        }

                    }
                    x = adjustpositionx(temptitlex, x);
                    drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, text, etype);
                }
                else if (etype == elementtype.texttitle)
                {
                    x += adjusttitlex;
                    y += adjusttitley;

                    x = adjustpositionx(tempx, x);
                    y = adjusttitlepositiony(temptitley, y);

                    drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, text, etype);
                }
                else if (etype == elementtype.text)
                {

                    x += adjusttextx;
                    y += adjusttexty;

                    x = adjustpositionx(tempx, x);
                    y = adjustpositiony(tempy, y);

                    if (firsttextx == 0)
                    {
                        firsttextx = x;
                    }

                    orgx = x;
                    text = text.Trim();
                    var maxlenght = 35;
                    if (element.Id == "certtitle")
                    {
                        maxlenght = 40;
                    }
                    else if (element.Id == "certevent")
                    {
                        maxlenght = 70;
                    }
                    else if (element.Id == "certpartner")
                    {
                        maxlenght = 70;
                    }

                    List<string> wrappedLines = WrapText(text, maxlenght);
                    if (wrappedLines.Count > 1)
                    {
                        y -= 8;
                        foreach (var line in wrappedLines)
                        {
                            drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, line.Trim(), etype);
                            x = orgx;
                            y += 12;
                        }
                    }
                    else
                    {
                        drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, text, etype);
                    }
                }

                else
                {
                    drawelement2(element, pageSize, cb, ref x, ref y, fontSelector, text, etype);
                }
            }

        }
        private void drawelement2(HtmlNode element, Rectangle pageSize, PdfContentByte cb, ref float x, ref float y, FontSelector fontSelector, string text, elementtype etype)
        {
            var xArtifakt = 5f;
            var xHeisei = 10.5f;
            var xHYGothic = 9f;
            var xSTSong = 8;
            var xAngsana = 5f;
            Phrase ph = fontSelector.Process(text);
            foreach (Chunk c in ph.Chunks)
            {
                cb.SetTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL);
                if (etype == elementtype.herder)
                {
                    drawelement3(c, cb, 18f, ArtifaktBlack, true);
                    cb.SetColorStroke(BaseGreen);
                    cb.SetColorFill(BaseGreen);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, c.Content, pageSize.GetLeft(x), pageSize.GetTop(y), 0);
                    if (c.Font.Familyname.Contains("Artifakt"))
                    {
                        x += (c.Content.Length * xArtifakt);
                    }
                    else if (c.Font.Familyname.Contains("Heisei"))
                    {
                        x += (c.Content.Length * xHeisei);
                    }
                    else if (c.Font.Familyname.Contains("HYGothic"))
                    {
                        x += (c.Content.Length * (xHYGothic + 10));
                    }
                    else if (c.Font.Familyname.Contains("STSong"))
                    {
                        x += (c.Content.Length * xSTSong);
                    }
                    else if (c.Font.Familyname.Contains("Angsana"))
                    {
                        x += (c.Content.Length * xAngsana);
                    }
                }
                else if (etype == elementtype.description)
                {
                    drawelement3(c, cb, 11f, ArtifaktBook);
                    cb.SetColorStroke(BaseColor.BLACK);
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, c.Content, pageSize.GetLeft(x), pageSize.GetTop(y), 0);
                    if (c.Font.Familyname.Contains("Artifakt"))
                    {
                        x += (c.Content.Length * (xArtifakt + 1.1f));
                    }
                    else if (c.Font.Familyname.Contains("Heisei"))
                    {
                        x += (c.Content.Length * xHeisei);
                    }
                    else if (c.Font.Familyname.Contains("HYGothic"))
                    {
                        x += (c.Content.Length * (xHYGothic + 1.3f));
                    }
                    else if (c.Font.Familyname.Contains("STSong"))
                    {
                        x += (c.Content.Length * xSTSong);
                    }
                    else if (c.Font.Familyname.Contains("Angsana"))
                    {
                        x += (c.Content.Length * xAngsana);
                    }
                }
                else if (etype == elementtype.year)
                {
                    cb.SetLineWidth(1);
                    cb.SetTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE);
                    cb.SetFontAndSize(ArtifaktElRegular, 34f);
                    cb.SetColorStroke(BaseColor.WHITE);
                    cb.SetColorFill(BaseColor.WHITE);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, c.Content, pageSize.GetLeft(x), pageSize.GetTop(y), 0);
                }
                else if (etype == elementtype.cartnotitle)
                {
                    drawelement3(c, cb, 12f, ArtifaktBold, true);
                    cb.SetColorStroke(BaseColor.BLACK);
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, c.Content, pageSize.GetLeft(x), pageSize.GetTop(y), 0);
                    if (c.Font.Familyname.Contains("Artifakt"))
                    {
                        x += (c.Content.Length * xArtifakt);
                    }
                    else if (c.Font.Familyname.Contains("Heisei"))
                    {
                        x += (c.Content.Length * xHeisei);
                    }
                    else if (c.Font.Familyname.Contains("HYGothic"))
                    {
                        x += (c.Content.Length * (xHYGothic + 3));
                    }
                    else if (c.Font.Familyname.Contains("STSong"))
                    {
                        x += (c.Content.Length * xSTSong);
                    }
                    else if (c.Font.Familyname.Contains("Angsana"))
                    {
                        x += (c.Content.Length * xAngsana);
                    }
                }
                else if (etype == elementtype.cartno)
                {
                    drawelement3(c, cb, 12f, ArtifaktBold, true);
                    cb.SetColorStroke(BaseColor.BLACK);
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, c.Content, pageSize.GetLeft(x), pageSize.GetTop(y), 0);
                    if (c.Font.Familyname.Contains("Artifakt"))
                    {
                        x += (c.Content.Length * xArtifakt);
                    }
                    else if (c.Font.Familyname.Contains("Heisei"))
                    {
                        x += (c.Content.Length * xHeisei);
                    }
                    else if (c.Font.Familyname.Contains("HYGothic"))
                    {
                        x += (c.Content.Length * xHYGothic);
                    }
                    else if (c.Font.Familyname.Contains("STSong"))
                    {
                        x += (c.Content.Length * xSTSong);
                    }
                    else if (c.Font.Familyname.Contains("Angsana"))
                    {
                        x += (c.Content.Length * xAngsana);
                    }
                }
                else if (etype == elementtype.texttitle)
                {
                    drawelement3(c, cb, 8f, ArtifaktBlack, true);
                    cb.SetColorStroke(BaseGreen);
                    cb.SetColorFill(BaseGreen); //#84c043
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, c.Content, pageSize.GetLeft(x), pageSize.GetTop(y), 0);

                    if (c.Font.Familyname.Contains("Artifakt"))
                    {
                        x += (c.Content.Length * xArtifakt);
                    }
                    else if (c.Font.Familyname.Contains("Heisei"))
                    {
                        x += (c.Content.Length * xHeisei);
                    }
                    else if (c.Font.Familyname.Contains("HYGothic"))
                    {
                        x += (c.Content.Length * xHYGothic);
                    }
                    else if (c.Font.Familyname.Contains("STSong"))
                    {
                        x += (c.Content.Length * xSTSong);
                    }
                    else if (c.Font.Familyname.Contains("Angsana"))
                    {
                        x += (c.Content.Length * xAngsana);
                    }
                }
                else if (etype == elementtype.text)
                {
                    drawelement3(c, cb, 8f, ArtifaktBlack);
                    cb.SetColorStroke(BaseColor.BLACK);
                    cb.SetColorFill(BaseColor.BLACK);
                    cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, c.Content.ToUpper(), pageSize.GetLeft(x), pageSize.GetTop(y), 0);
                    if (c.Font.Familyname.Contains("Artifakt"))
                    {
                        x += (c.Content.Length * xArtifakt);
                    }
                    else if (c.Font.Familyname.Contains("Heisei"))
                    {
                        x += (c.Content.Length * xHeisei);
                    }
                    else if (c.Font.Familyname.Contains("HYGothic"))
                    {
                        x += (c.Content.Length * xHYGothic);
                    }
                    else if (c.Font.Familyname.Contains("STSong"))
                    {
                        x += (c.Content.Length * xSTSong);
                    }
                    else if (c.Font.Familyname.Contains("Angsana"))
                    {
                        x += (c.Content.Length * xAngsana);
                    }
                }
            }
        }

        private void drawelement3(Chunk c, PdfContentByte cb, float size, BaseFont artifakt, bool bold = false)
        {

            if (c.Font.Familyname.Contains("Artifakt"))
            {
                cb.SetFontAndSize(artifakt, size);
            }
            else if (c.Font.Familyname.Contains("Heisei"))
            {
                cb.SetFontAndSize(Heisei, size);
            }
            else if (c.Font.Familyname.Contains("HYGothic"))
            {
                if (bold)
                {
                    cb.SetLineWidth(0.5);
                    cb.SetTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE);
                }
                cb.SetFontAndSize(Hygothic, size);
            }

            else if (c.Font.Familyname.Contains("STSong"))
            {
                if (bold)
                {
                    cb.SetLineWidth(0.5);
                    cb.SetTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE);
                }
                else
                {
                    //cb.SetLineWidth(0.001);
                    //cb.SetTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE);
                }
                cb.SetFontAndSize(Stsong, size);
            }
            else if (c.Font.Familyname.Contains("Angsana"))
            {
                cb.SetFontAndSize(Angsana, size * 1.6f);
            }
        }


        private BaseFont getBasefont(FontSelector fontSelector, string text)
        {
            Phrase ph = fontSelector.Process(text);
            foreach (Chunk c in ph.Chunks)
            {
                if (c.Font.Familyname.Contains("Heisei"))
                {
                    return Heisei;
                }
                else if (c.Font.Familyname.Contains("HYGothic"))
                {
                    return Hygothic;
                }
                else if (c.Font.Familyname.Contains("STSong"))
                {
                    return Stsong;
                }
                else if (c.Font.Familyname.Contains("Angsana"))
                {
                    return Angsana;
                }
            }
            return ArtifaktBlack;
        }
        private float adjustpositionx(List<float> list, float original)
        {
            var adjust = (int)original;

            var isnew = true;
            for (var i = adjust - 7; i < adjust + 7; i++)
            {
                if (list.Contains(i))
                {
                    adjust = i;
                    isnew = false;
                    break;
                }
            }
            if (isnew)
            {
                list.Add(adjust);
            }

            return adjust;
        }

        private float adjustpositiony(List<float> list, float original)
        {
            var adjust = (int)original;
            var isnew = true;
            for (var i = adjust - 7; i < adjust + 7; i++)
            {
                if (list.Contains(i))
                {
                    adjust = i;
                    isnew = false;
                    break;
                }
            }
            if (isnew)
            {
                var line80 = 80;
                var line40 = 40;
                if (list.Count == 0)
                {
                    if (reportype == reportype.type1)
                    {
                        adjust = 285;
                    }
                    else if (reportype == reportype.type2)
                    {
                        adjust = 276;
                    }
                    else if (reportype == reportype.type3)
                    {
                        adjust = 293;
                    }
                    else if (reportype == reportype.type4)
                    {
                        adjust = 381;
                    }
                    else if (reportype == reportype.type5)
                    {
                        adjust = 280;
                    }
                }
                else if (list.Count > 0)
                {
                    var linehight = adjust - list[list.Count - 1];
                    if (linehight >= 70 && linehight <= 90)
                    {
                        adjust = (int)list[list.Count - 1] + line80;
                    }
                    else if (linehight >= 30 && linehight <= 50)
                    {
                        adjust = (int)list[list.Count - 1] + line40;
                    }
                }
                list.Add(adjust);
            }


            return adjust;
        }
        private float adjusttitlepositiony(List<float> list, float original)
        {
            var adjust = (int)original;
            var isnew = true;
            for (var i = adjust - 7; i < adjust + 7; i++)
            {
                if (list.Contains(i))
                {
                    adjust = i;
                    isnew = false;
                    break;
                }
            }
            if (isnew)
            {
                var line80 = 80;
                var line40 = 40;
                if (list.Count == 0)
                {
                    if (reportype == reportype.type1)
                    {
                        adjust = 305;
                    }
                    else if (reportype == reportype.type2)
                    {
                        adjust = 296;
                    }
                    else if (reportype == reportype.type3)
                    {
                        adjust = 313;
                    }
                    else if (reportype == reportype.type4)
                    {
                        adjust = 400;
                    }
                    else if (reportype == reportype.type5)
                    {
                        adjust = 298;
                    }
                }
                else if (list.Count > 0)
                {
                    var linehight = adjust - list[list.Count - 1];
                    if (linehight > 70 && linehight <= 90)
                    {
                        adjust = (int)list[list.Count - 1] + line80;
                    }
                    else if (linehight > 30 && linehight <= 50)
                    {
                        adjust = (int)list[list.Count - 1] + line40;
                    }
                }
                list.Add(adjust);
            }


            return adjust;
        }
    }
}

namespace autodesk.DTO
{
    public class ElementNodeDTO
    {
        public HtmlNode element { get; set; }
        public elementtype elementtype { get; set; }
        public string text { get; set; }
    }
    public class CellNodeDTO
    {
        public BaseFont basefont { get; set; }
        public reportype reportype { get; set; }
        public elementtype elementtype { get; set; }
        public float? x { get; set; }
        public float? y { get; set; }
        public int? maxlenght { get; set; }
        public string text { get; set; }
    }
    public enum reportype
    {
        type1,
        type2,
        type3,
        type4,
        type5,

    }
    public enum elementtype
    {
        text,
        texttitle,
        description,
        herder,
        year,
        cartnotitle,
        cartno,


    }
    public class CourseStudentDTO
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Name { get; set; }
        public string LocationName { get; set; }
        public string ContactName { get; set; }
        public string productName { get; set; }
        // public string StartDate { get; set; }
        public string CompletionDate { get; set; }
        public string HoursTraining { get; set; }
        public string SiteName { get; set; }
        public string CertNumber { get; set; }
        public string Institution { get; set; }



    }
    public class CertifiedDTO
    {
        public string HTMLDesign { get; set; }
        public string TitleTextHeader { get; set; }
        public string TitleHeaderDescription { get; set; }
        public string TitleTextFooter { get; set; }
        public string TitleCertificateNo { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string LanguageCertificate { get; set; }
        public string CertificateBackgroundId { get; set; }
        public string PartnerTypeId { get; set; }
        public string CertificateTypeId { get; set; }
        public string TitleEvent { get; set; }
        public string TitleStudentName { get; set; }
        public string TitleCourseTitle { get; set; }
        public string TitleProduct { get; set; }
        public string TitleInstructor { get; set; }
        public string TitleDate { get; set; }
        public string TitleDuration { get; set; }
        public string TitlePartner { get; set; }
        public string TitleInstitution { get; set; }
        public string TitleLocation { get; set; }
        public string OrgLogo { get; set; }
    }
}