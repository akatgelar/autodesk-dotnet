using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using DocumentFormat.OpenXml.Office2013.PowerPoint.Roaming;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using autodesk.Code.AuditLogServices;

// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/ContactAll")]
    public class ContactAllController : Controller
    {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        String result1;
        DataSet dsTemp = new DataSet();
        private IDataServices dataService;
        String hasil;
        public Audit _audit;//= new Audit();
        private MySQLContext db;
        private readonly IAuditLogServices _auditLogServices;
        public ContactAllController(MySqlDb sqlDb, MySQLContext _db, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            db = _db;
            dataService = new DataServices(oDb, db);
            _audit = new Audit(oDb);
            _auditLogServices = auditLogServices;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Contacts_All ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ACI")]
        public IActionResult SelectACI()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select productName, Applications.ContactId, FirstName, LastName, SubmissionDate, ApprovedDate, RejectedDate, Applications.`Status` " +
                    "FROM Applications LEFT JOIN ApplicationsProducts ON Applications.ApplicationId = ApplicationsProducts.ApplicationId " +
                    "LEFT JOIN Products ON ApplicationsProducts.ProductId = Products.productId " +
                    "LEFT JOIN Contacts_All ON Applications.ContactId = Contacts_All.ContactId " +
                    "LEFT JOIN Main_site ON Contacts_All.PrimarySiteId = Main_site.SiteId " +
                    "LEFT JOIN Countries ON Contacts_All.CountryCode = Countries.CountryCode WHERE productName <> '' ORDER BY productName ASC ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ACI/where/{obj}")]
        public IActionResult WhereObjACI(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select productName, Applications.ContactId, FirstName, LastName, SubmissionDate, ApprovedDate, RejectedDate, Applications.`Status` " +
                    "FROM Applications LEFT JOIN ApplicationsProducts ON Applications.ApplicationId = ApplicationsProducts.ApplicationId " +
                    "LEFT JOIN Products ON ApplicationsProducts.ProductId = Products.productId " +
                    "LEFT JOIN Contacts_All ON Applications.ContactId = Contacts_All.ContactId " +
                    "LEFT JOIN Main_site ON Contacts_All.PrimarySiteId = Main_site.SiteId " +
                    "LEFT JOIN Countries ON Contacts_All.CountryCode = Countries.CountryCode WHERE productName <> '' ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("ProductId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "Products.productId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "Contacts_All.PrimarySiteId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SubmissionDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "SubmissionDate = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CountryId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "Countries.CountryId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ApprovedBy"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "ApprovedBy = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("RejectedBy"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "RejectedBy = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("RejectedDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "RejectedDate = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ModifiedDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "ModifiedDate = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }


                i = i + 1;
            }

            query += " ORDER BY productName ASC";

            try
            {
                sb.AppendFormat(query);
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("{email}")]
        public IActionResult WhereEmail(string email)
        {
            try
            {
                result = "";
                sb = new StringBuilder();
                sb.AppendFormat("select * from Contacts_All where EmailAddress = "+"\""+ email +"\""+" AND `Status` = 'A'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch (Exception ex)
            {
                _auditLogServices.LogHistory(new History
                {
                    Admin = "System",
                    Date = DateTime.Now,
                    Description = "Error when get contact_all .Details:</br>" + ex.Message,
                    Id = "-1"
                });
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("Profile/{id}")]
        public IActionResult Profile(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Contacts_All where ContactId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select * from Contacts_All ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("ContactId"))
                {
                    query += "ContactId = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("Password"))
                {
                    query += "password = MD5('" + property.Value.ToString() + "') ";
                }
                if (property.Name.Equals("EmailAddress"))
                {
                    // query += "EmailAddress = '" + property.Value.ToString() + "' ";
                    query += "EmailAddress = '" + property.Value.ToString().Replace(@"'", @"\'") + "' "; /* issue 27092018 - handling crud with char (') inside */
                }

                i = i + 1;
            }

            Console.WriteLine(query);
            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }


        [HttpPut("ChangePassword/{id}")]
        public IActionResult Update(string id, [FromBody] dynamic data)
        {
            JObject contactpassword;
            JObject contactpasswordnew;
            Boolean success = false;
            JObject OldData = new JObject();
            string res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Change Password Failed, Current Password is Wrong\"" +
                    "}";

            try
            {
                sb = new StringBuilder();
                // sb.AppendFormat("select `password`, password_plain from Contacts_All where ContactId = '" + id + "'"); /* remove password_plain */
                sb.AppendFormat("select `password` from Contacts_All where ContactId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                contactpassword = JObject.Parse(result);
                OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());


                sb = new StringBuilder();

                // sb.AppendFormat(
                //     "update Contacts_All set " +
                //     "`password` = CASE WHEN (md5('" + data.currentpassword + "') = '" + contactpassword["password"].ToString() + "') THEN md5('" + data.password + "') ELSE '" + contactpassword["password"].ToString() + "' END ," +
                //     "`password_plain` = CASE WHEN ('" + data.currentpassword + "' = '" + contactpassword["password_plain"].ToString() + "') THEN '" + data.password + "' ELSE '" + contactpassword["password_plain"].ToString() + "' END " +
                //     "where ContactId = '" + id + "'"
                // ); /* remove password_plain */

                sb.AppendFormat(
                    "update Contacts_All set " +
                    "`password` = CASE WHEN (md5('" + data.currentpassword + "') = '" + contactpassword["password"].ToString() + "' OR '" + data.currentpassword + "' = '" + contactpassword["password"].ToString() + "') THEN md5('" + data.password + "') ELSE '" + contactpassword["password"].ToString() + "' END " +
                    "where ContactId = '" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("select `password` from Contacts_All where ContactId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result1 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                contactpasswordnew = JObject.Parse(result1);

                if (contactpassword["password"].ToString() != contactpasswordnew["password"].ToString())
                {
                    success = true;
                }
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Change Password Failed, Current Password is Wrong\"" +
                    "}";
            }
            finally
            {
                if (success)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select `password` from Contacts_All where  ContactId = '" + id + "'");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, id, OldData["ContactName"].ToString(), " where ContactId = " + id + ""))
                    {
                        res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Change Password Success\"" +
                        "}";
                    }
                }
            }

            return Ok(res);
        }

        [HttpPut("ProfileEIM/{id}")]
        public IActionResult UpdateProfile(string id, [FromBody] dynamic data)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";


            sb = new StringBuilder();
            sb.AppendFormat("select * from Contacts_All where ContactId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            JObject NewData = new JObject();


            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Contacts_All set "
                    + "FirstName='" + data.FirstName + "', "
                    + "LastName='" + data.LastName + "', "
                    + "ContactName='" + data.FirstName + " " + data.LastName + "', "
                    + "Gender='" + data.Gender + "', "
                    + "PrimaryIndustryId='" + data.PrimaryIndustryId + "', "
                    + "Address1='" + data.Address1 + "', "
                    + "EmailAddress='" + data.EmailAddress + "', "
                    + "Address2='" + data.Address2 + "', "
                    + "Address3='" + data.Address3 + "', "
                    + "Bio='" + data.Bio + "', "
                    + "City='" + data.City + "', "
                    + "Mobile1='" + data.Mobile1 + "', "
                    + "PostalCode='" + data.PostalCode + "', "
                    + "PrimaryLanguage='" + data.PrimaryLanguage + "', "
                    + "SecondaryLanguage='" + data.SecondaryLanguage + "', "
                    + "Salutation='" + data.Salutation + "', "
                    + "StateProvince='" + data.StateProvince + "', "
                    + "Telephone1='" + data.Telephone1 + "', "
                    + "CountryCode='" + data.CountryCode + "', "
                    + "ShowInSearch='" + data.ShowInSearch + "', "
                    + "DoNotEmail='" + data.DoNotEmail + "', "
                    + "ShareEmail='" + data.ShareEmail + "', "
                    + "ShareTelephone='" + data.ShareTelephone + "', "
                    + "ShareMobile='" + data.ShareMobile + "', "
                    + "MobileCode='" + data.MobileCode + "', "
                    + "TelephoneCode='" + data.TelephoneCode + "', "
                    + "WebsiteUrl='" + data.WebsiteUrl + "' "
                    + "Where ContactId='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);






            }
            catch
            {

                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Update Data Failed\"" +
                    "}";
            }
            finally
            {

                StringBuilder sb2 = new StringBuilder();
                sb2.AppendFormat("select * from Contacts_All where ContactId =" + id + ";");
                sqlCommand = new MySqlCommand(sb2.ToString());
                DataSet ds2 = oDb.getDataSetFromSP(sqlCommand);
                string result2 = MySqlDb.GetJSONObjectString(ds2.Tables[0]);
                NewData = JsonConvert.DeserializeObject<JObject>(result2.ToString());

                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, id, OldData["ContactName"].ToString(), " where ContactId =" + id + ""))
                {
                    res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
                }


                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }
            return Ok(res);
        }

        [HttpPut("ChangeUserStatus/{id}")]
        public IActionResult ChangeUserStatus(string id, [FromBody] dynamic data)
        {
            JToken value;
            bool status = false;
            string resMsg = string.Empty;

            if (!string.IsNullOrEmpty(id) && data != null)
            {


                var existingContact = this.db.ContactsAll.SingleOrDefault(x => x.ContactId.Equals(id));
                if (existingContact != null)
                {
                    existingContact.Status = data.Status == "False" ? "I" : "A";
                    existingContact.TerminationReason = data.Reason;
                    try
                    {
                        this.db.Update(existingContact);
                        this.db.SaveChanges();
                        status = true;
                        resMsg = "Successfully Updated!";
                    }
                    catch (Exception e)
                    {
                        status = false;
                        resMsg = "Oop, something went wrong while processing your request";
                    }
                }
            }
            return Ok(new { code = status, message = resMsg });
        }

        [HttpPut("ChangeEmail/{id}")]
        public IActionResult UpdateEmail(string id, [FromBody] dynamic data)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("select EmailAddress from Contacts_All where ContactId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            DataSet ds = oDb.getDataSetFromSP(sqlCommand);
            string result2 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result2.ToString());


            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Contacts_All set "
                    + "EmailAddress='" + data.Email + "' "
                    + "Where ContactId='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Update Data Failed\"" +
                    "}";
            }
            finally
            {

                StringBuilder sb2 = new StringBuilder();
                sb2.AppendFormat("select EmailAddress from Contacts_All where ContactId =" + id + ";");
                sqlCommand = new MySqlCommand(sb2.ToString());
                DataSet ds2 = oDb.getDataSetFromSP(sqlCommand);
                string result = MySqlDb.GetJSONObjectString(ds2.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, id, OldData["ContactName"].ToString(), " where ContactId =" + id + ""))
                {
                    res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
                }
            }
            return Ok(res);
        }

        [HttpPost("reportContact")]
        public IActionResult findBy([FromBody] dynamic data)
        {
            JArray columnContact;
            JArray columnSite;
            string query = "";
            string selectedColumns = "";
            string kolomSite = "";
            string kolomContact = "";
            string anotherfiled = "";
            /*fix issue excel no 263.Show data based role*/

            string role = "";
            string orgid = "";

            /*end fix issue excel no 263.Show data based role*/

            selectedColumns += "select ";

            int n = 0;
            JObject column = JObject.FromObject(data);
            foreach (JProperty property in column.Properties())
            {
                if (property.Name.Equals("fieldContact"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomContact = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomContact = "," + property.Value.ToString();
                        }

                        kolomContact = kolomContact.Replace("c.DateAdded", "DATE_FORMAT(c.DateAdded, '%d-%m-%Y') AS DateAdded");
                        kolomContact = kolomContact.Replace("c.DateLastAdmin", "DATE_FORMAT(c.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        kolomContact = kolomContact.Replace("c.DateBirth", "DATE_FORMAT(c.DateBirth, '%d-%m-%Y') AS DateBirth");
                        if (kolomContact.Contains("Status") && !kolomContact.Contains("s.StatusEmailSend"))
                        {
                            kolomContact = kolomContact.Replace("c.Status,", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= c.Status)  'Contact Status',");
                        }
                        if (kolomContact.Contains("CountryCode"))
                        {
                            kolomContact = kolomContact.Replace("c.CountryCode", "rc.countries_name as 'Contact Country'");
                        }
                        selectedColumns += kolomContact;

                        n = n + 1;
                    }
                    else
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat("DESCRIBE Contacts_All");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        columnContact = JArray.Parse(hasil);

                        string[] columnarrtmp = new string[columnContact.Count - 1];
                        int j = 0;
                        for (int index = 0; index < columnContact.Count; index++)
                        {
                            if (columnContact[index]["Field"].ToString() != "password")
                            {
                                columnarrtmp[j] = "c." + columnContact[index]["Field"].ToString();
                                j++;
                            }
                        }

                        if (n == 0)
                        {
                            kolomContact = string.Join(",", property.Value.ToString());
                        }
                        else if (n > 0)
                        {
                            kolomContact = "," + string.Join(",", property.Value.ToString());
                        }

                        kolomContact = kolomContact.Replace("c.DateAdded", "DATE_FORMAT(c.DateAdded, '%d-%m-%Y') AS DateAdded");
                        kolomContact = kolomContact.Replace("c.DateLastAdmin", "DATE_FORMAT(c.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        kolomContact = kolomContact.Replace("c.DateBirth", "DATE_FORMAT(c.DateBirth, '%d-%m-%Y') AS DateBirth");
                        if (kolomContact.Contains("Status") && !kolomContact.Contains("s.StatusEmailSend"))
                        {
                            kolomContact = kolomContact.Replace("c.Status,", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= c.Status)  'Contact Status',");
                        }
                        if (kolomContact.Contains("CountryCode"))
                        {
                            kolomContact = kolomContact.Replace("c.CountryCode", "rc.countries_name as 'Contact Country'");
                        }
                        selectedColumns += kolomContact;

                        n = n + 1;
                    }
                }

                if (property.Name.Equals("fieldSite"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomSite = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomSite = "," + property.Value.ToString();
                        }

                        kolomSite = kolomSite.Replace("ss.DateAdded", "DATE_FORMAT(ss.DateAdded, '%d-%m-%Y') AS DateAdded");
                        kolomSite = kolomSite.Replace("ss.DateLastAdmin", "DATE_FORMAT(ss.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        if (kolomSite.Contains("Status"))
                        {
                            kolomSite = kolomSite.Replace("s.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= s.Status)  'Site Status'");
                        }
                        if (kolomSite.Contains("SiteCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.SiteCountryCode", "(select countries_name from Ref_countries where countries_code = s.SiteCountryCode  group by countries_code ) as 'Site Country'");
                        }
                        if (kolomSite.Contains("MailingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.MailingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.MailingCountryCode) as 'Mailing Country'");
                        }
                        if (kolomSite.Contains("ShippingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.ShippingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.ShippingCountryCode) as 'Shipping Country'");
                        }
                        selectedColumns += kolomSite;

                        n = n + 1;
                    }
                    else
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat("DESCRIBE Main_site");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        columnSite = JArray.Parse(hasil);

                        string[] columnarrtmp = new string[columnSite.Count];
                        for (int index = 0; index < columnSite.Count; index++)
                        {
                            columnarrtmp[index] = "ss." + columnSite[index]["Field"].ToString();
                        }

                        if (n == 0)
                        {
                            kolomSite = string.Join(",", columnarrtmp);
                        }
                        else if (n > 0)
                        {
                            kolomSite = "," + string.Join(",", columnarrtmp);
                        }

                        kolomSite = kolomSite.Replace("ss.DateAdded", "DATE_FORMAT(ss.DateAdded, '%d-%m-%Y') AS DateAdded");
                        kolomSite = kolomSite.Replace("ss.DateLastAdmin", "DATE_FORMAT(ss.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        if (kolomSite.Contains("Status"))
                        {
                            kolomSite = kolomSite.Replace("s.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= s.Status)  'Site Status'");
                        }
                        if (kolomSite.Contains("SiteCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.SiteCountryCode", "rc.countries_name as 'Site Country'");
                        }
                        if (kolomSite.Contains("MailingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.MailingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.MailingCountryCode) as 'Mailing Country'");
                        }
                        if (kolomSite.Contains("ShippingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.ShippingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.ShippingCountryCode) as 'Shipping Country'");
                        }
                        selectedColumns += kolomSite;

                        n = n + 1;
                    }
                }

                if (property.Name.Equals("fieldAnother"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            anotherfiled = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            anotherfiled += "," + property.Value.ToString();
                        }
                        anotherfiled = anotherfiled.Replace("m.MarketType", "m.MarketType as 'Market Type'");
                        anotherfiled = anotherfiled.Replace("r.RoleName", "r.RoleName as 'Partner Type'");
                        anotherfiled = anotherfiled.Replace("sr.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= sr.Status) as 'Partner Type Status'");
                        anotherfiled = anotherfiled.Replace("sr.CSN", "sr.CSN as 'Partner Type CSN'");
                        anotherfiled = anotherfiled.Replace("sr.ITS_ID", "sr.ITS_ID as 'ITS Site ID'");
                        anotherfiled = anotherfiled.Replace("sr.UParent_CSN", "sr.UParent_CSN as 'Ultimate Parent CSN'");
                        anotherfiled = anotherfiled.Replace("tc.geo_name", "tc.geo_name as 'Geo'");
                        anotherfiled = anotherfiled.Replace("tc.region_name", "tc.region_name as 'Region'");
                        anotherfiled = anotherfiled.Replace("tc.subregion_name", "tc.subregion_name as 'SubRegion'");
                        anotherfiled = anotherfiled.Replace("t.Territory_Name", "t.Territory_Name as 'Territory'");
                        selectedColumns += anotherfiled;
                        n = n + 1;
                    }
                }
                /*fix issue excel no 263.Show data based role*/

                if ((property.Name.Equals("filterByRollup")))
                {

                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() == "Y"))
                    {

                        anotherfiled = selectedColumns.Replace("r.RoleName as 'Partner Type'", "group_concat(DISTINCT RoleName ORDER BY RoleName ) as 'Partner Type'");
                        selectedColumns = anotherfiled;
                    }


                }



                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        role = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        orgid = property.Value.ToString();
                    }
                }

                /*end fix issue excel no 263.Show data based role*/
            }

            query += selectedColumns + " FROM Contacts_All c" +
                " INNER join SiteContactLinks sc on c.ContactId = sc.ContactId" +
                " INNER join Main_site s on sc.SiteId = s.SiteId" +
                " INNER join Main_organization o on s.OrgId = o.OrgId" +
                " INNER join Ref_countries rc on c.CountryCode = rc.countries_code" +
                "  inner join (select  SiteId, RoleId, max(SiteRoleId) maxid from SiteRoles group by SiteId, RoleId) MaxRole on s.SiteId = MaxRole.SiteId " +
                " INNER JOIN SiteRoles sr ON MaxRole.maxid = sr.SiteRoleId INNER JOIN Roles r ON r.RoleId = sr.RoleId " +
                " INNER join MarketType m on rc.MarketTypeId = m.MarketTypeId " +
                // " join Ref_geo rg on rc.geo_code = rg.geo_code " +
                // " join Ref_region rr on rc.region_code = rr.region_code " +
                // " join Ref_subregion rsr on rc.subregion_code = rsr.subregion_code " +
                " INNER join Ref_territory t on rc.TerritoryId = t.TerritoryId " +
                "  Inner join RoleParams rp on rp.RoleParamId =sr.RoleParamId AND sr.`Status` in ( 'A' ) and (c.UserLevelId <>'' and c.UserLevelId is not null) ";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.ContactName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByEmail"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.EmailAddress like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySiteName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.RoleId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.Status in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByMarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.MarketTypeId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByContactRole"))
                {
                    //if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    //{
                    //    if (i == 0) { query += " where "; }
                    //    if (i > 0) { query += " and "; }
                    //    query += "c.UserLevelId in (" + property.Value.ToString() + ") ";
                    //    i = i + 1;
                    //}

                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        property.Value = property.Value.ToString().Replace("\'", "");
                        string[] userlevel = property.Value.ToString().Split(',');
                        if (userlevel.Length > 1)
                        {
                            for (int j = 0; j < userlevel.Length; j++)
                            {
                                if (j == 0)
                                {
                                    query += "((c.UserLevelId like '%" + userlevel[0] + "%')";
                                }else  if (j == userlevel.Length - 1)
                                    {
                                        query += ")";
                                    }
                                    else
                                {
                                    query += " or (c.UserLevelId like '%" + userlevel[j] + "%')";
                                }
                                
                            }
                        }
                        else
                        {
                            query += "(c.UserLevelId like '%" + userlevel[0] + "%') ";
                        }
                        i = i + 1;
                    }

                }

                if (property.Name.Equals("filterByATCRole"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.ATCRole = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByRegion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.region_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySubRegion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.subregion_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByTerritory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "rc.TerritoryId in (" + property.Value.ToString() + ") ";

                        /*fix issue excel no 263.Show data based role*/

                        if (role == "")
                        {
                            query += "rc.TerritoryId in (" + property.Value.ToString() + ") ";
                        }
                        else if (role == "DISTRIBUTOR")
                        {
                            query += "rc.TerritoryId in (select rc.TerritoryId from Ref_countries rc INNER JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + orgid + ") GROUP BY rc.TerritoryId) ";
                        }
                        else
                        {
                            query += "rc.TerritoryId in (" + property.Value.ToString() + ") AND o.OrgId in (" + orgid + ") ";
                        }

                        /*end fix issue excel no 263.Show data based role*/

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountries"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.countries_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByIndustry"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.PrimaryIndustryId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPrimarySite"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (property.Value.ToString() == "Y")
                        {
                            if (i == 0) { query += " where "; }
                            if (i > 0) { query += " and "; }
                            query += "c.PrimarySiteId <> '' ";
                        }
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByLastLogin"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "datediff(c.LastLogin,'2009-03-01')>=" + property.Value.ToString() + " ";
                        i = i + 1;
                    }
                }



            }

            query += "group by c.ContactId desc";

            // Console.WriteLine(query);
            List<dynamic> dynamicList = new List<dynamic>();
            try
            {

                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
                // result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("EmailGenerator/{obj}")]
        public IActionResult generateEmail(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "SELECT DISTINCT s.ContactId, s.ContactName, EmailAddress, EmailAddress2  FROM Contacts_All s, SiteContactLinks scl, Main_site ss, Ref_countries c, SiteRoles sr WHERE s.ContactId = scl.ContactId  AND scl.SiteId = ss.SiteId  AND scl.Status <> 'X' " +
            "AND ss.Status <> 'X' AND ss.SiteCountryCode = c.countries_code AND ss.SiteId = sr.SiteId AND sr.Status <> 'X' AND s.Status <> 'X' AND s.Status <> 'D' AND (s.EmailAddress <> '' OR s.EmailAddress2 <> '') ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("SiteName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "SiteName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "ContactName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("LogAfterMarch"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() == "true"))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "DATEDIFF( s.LastLogin, '2009-03-01' ) >= 0";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("LastLogin"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "DATEDIFF(Now(),s.LastLogin) <= " + property.Value.ToString() + " ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "sr.RoleId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "sr.Status in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactRole"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "s.PrimaryRoleId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Industry"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "s.PrimaryIndustryId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Language"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "PrimaryLanguage = " + property.Value.ToString() + " ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "SiteCountryCode in (select countries_code from Ref_countries where countries_id in (" + property.Value.ToString() + ")) ";
                        i = i + 1;
                    }
                }



                i = i + 1;
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("ContactTable")]
        public IActionResult ContactTable()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("DESCRIBE Contacts_All");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ContactTableAttribute")]
        public IActionResult ContactTableAttribute()
        {
            DataTable dt1 = new DataTable();
            try
            {
                sb = new StringBuilder();
                sb.Append("DESCRIBE Contacts_All; SHOW COLUMNS FROM Journals LIKE '%Notes';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                dt1 = ds.Tables[0];
                DataTable dt2 = ds.Tables[1];
                dt1.Merge(dt2, false, MissingSchemaAction.Add);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(dt1);

            }

            return Ok(result);
        }

        [HttpGet("SiteTable")]
        public IActionResult SiteTable()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("DESCRIBE Main_site");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("SecurityRole")]
        public IActionResult ShowSecurityRole()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT c.ContactName, c.ContactId, c.ContactIdInt, c.EmailAddress, j.Status As RoleStatus, j.LastAdminBy, j.DateLastAdmin, ja.ActivityName, s.SiteId, s.SiteName, c.UserLevelId  FROM Contacts_All c, Journals j, JournalActivities ja, Main_site s " +
                    "WHERE j.ActivityId = ja.ActivityId AND c.ContactId = j.ParentId  AND c.PrimarySiteId = s.siteId AND j.Status = 'A' AND c.UserLevelId in ('ADMIN','SUPERADMIN') " +
                    "ORDER By ActivityName");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost("ContactAttrReport")]
        public IActionResult findContactAttribute([FromBody] dynamic data)
        {
            JArray columnSite;
            JArray columnContact;
            string query = "";
            string selectedColumns = "";
            string kolomSite = "";
            string kolomContact = "";
            string anotherfiled = "";
            /*fix issue excel no 263.Show data based role*/
          //  Boolean Status = false;
            string role = "";
            string orgid = "";

            /*end fix issue excel no 263.Show data based role*/


            

            int n = 1;
            JObject column = JObject.FromObject(data);
            if (column["ContactField"] != null)
            {
                selectedColumns += "select DISTINCT ss.ActivityName ";
            }else
                selectedColumns += "select DISTINCT ja.ActivityName ";
            foreach (JProperty property in column.Properties())
            {
                if (property.Name.Equals("SiteField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomSite = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomSite = "," + property.Value.ToString();
                        }

                        kolomSite = kolomSite.Replace("ss.DateAdded", "DATE_FORMAT(ss.DateAdded, '%d/%m/%Y') AS DateAdded");
                        kolomSite = kolomSite.Replace("ss.DateLastAdmin", "DATE_FORMAT(ss.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        //  kolomSite = kolomSite.Replace("ss.Status","GROUP_CONCAT(DISTINCT sr.Status) AS Status"); /* tambah parameter partner status issue 05091028 */
                        if (kolomSite.Contains("Status"))
                        {
                            kolomSite = kolomSite.Replace("ss.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= ss.Status)  'Site Status'");
                        }


                        if (kolomSite.Contains("SiteCountryCode"))
                        {
                            if (column["ContactField"] != null)
                            {
                                kolomSite = kolomSite.Replace("ss.SiteCountryCode", "ss.countries_name as 'Site Country'");
                            }
                            else
                                kolomSite = kolomSite.Replace("ss.SiteCountryCode", "c.countries_name as 'Site Country'");
                        }

                        if (kolomSite.Contains("MailingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("ss.MailingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = ss.MailingCountryCode) as 'Mailing Country'");
                        }
                        if (kolomSite.Contains("ShippingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("ss.ShippingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = ss.ShippingCountryCode) as 'Shipping Country'");
                        }


                        if (kolomSite.Contains("SiteId"))
                        {
                            if (column["ContactField"] == null)
                            {
                                kolomSite = kolomSite.Replace("ss.SiteId", "s.SiteId");


                            }

                        }

                        selectedColumns += kolomSite;
                        n = n + 1;
                    }
                    else
                    {


                        //  sb = new StringBuilder();
                        //  sb.AppendFormat("DESCRIBE Main_site");
                        //  sqlCommand = new MySqlCommand(sb.ToString());
                        //  dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        //  hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        //  columnSite = JArray.Parse(hasil);

                        //  string[] columnarrtmp = new string[columnSite.Count];
                        //  for (int index = 0; index < columnSite.Count; index++)
                        //  {
                        //      columnarrtmp[index] = "ss." + columnSite[index]["Field"].ToString();
                        //  }

                        //  if(n==0){
                        //      kolomSite = string.Join(",", columnarrtmp);
                        //  } else if(n>0){
                        //      kolomSite = ","+string.Join(",", columnarrtmp);
                        //  }

                        //  kolomSite = kolomSite.Replace("ss.DateAdded","DATE_FORMAT(ss.DateAdded, '%d/%m/%Y') AS DateAdded");
                        //  kolomSite = kolomSite.Replace("ss.DateLastAdmin","DATE_FORMAT(ss.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        ////  kolomSite = kolomSite.Replace("ss.Status","GROUP_CONCAT(DISTINCT sr.Status) AS Status"); /* tambah parameter partner status issue 05091028 */
                        //  if (kolomSite.Contains("Status"))
                        //  {
                        //      kolomSite = kolomSite.Replace("ss.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= ss.Status)  'Site Status'");
                        //  }

                        //  if (kolomSite.Contains("SiteCountryCode"))
                        //  {
                        //      kolomSite = kolomSite.Replace("ss.SiteCountryCode", "ss.countries_name as 'Site Country'");
                        //  }
                        //  selectedColumns += kolomSite;

                        //  n = n + 1;
                    }
                }

                if (property.Name.Equals("ContactField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomContact = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomContact = "," + property.Value.ToString();
                        }

                        kolomContact = kolomContact.Replace("s.DateAdded", "DATE_FORMAT(s.DateAdded, '%d/%m/%Y') AS DateAdded");
                        kolomContact = kolomContact.Replace("s.DateLastAdmin", "DATE_FORMAT(s.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        kolomContact = kolomContact.Replace("s.DateBirth", "DATE_FORMAT(s.DateBirth, '%d/%m/%Y') AS DateBirth");
                        if (kolomContact.Contains("s.Status") && !kolomContact.Contains("s.StatusEmailSend"))
                        {
                            kolomContact = kolomContact.Replace("s.Status,", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= s.Status)  'Contact Status',");
                        }
                        if (kolomContact.Contains("s.CountryCode"))
                        {
                            kolomContact = kolomContact.Replace("s.CountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.CountryCode) as 'Contact Country' ");
                        }
                        selectedColumns += kolomContact;
                        n = n + 1;
                    }
                    else
                    {
                        //sb = new StringBuilder();
                        //sb.AppendFormat("DESCRIBE Contacts_All");
                        //sqlCommand = new MySqlCommand(sb.ToString());
                        //dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        //hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        //columnContact = JArray.Parse(hasil);

                        //string[] columnarrtmp = new string[columnContact.Count - 1];
                        //int j = 0;
                        //for (int index = 0; index < columnContact.Count; index++)
                        //{
                        //    if (columnContact[index]["Field"].ToString() != "password")
                        //    {
                        //        columnarrtmp[j] = "s." + columnContact[index]["Field"].ToString();
                        //        j++;
                        //    }
                        //}
                        //if(n==0){
                        //    kolomContact=  string.Join(",",columnarrtmp);
                        //}else if(n>0){
                        //    kolomContact=  ","+string.Join(",",columnarrtmp);
                        //}

                        //kolomContact = kolomContact.Replace("s.DateAdded","DATE_FORMAT(s.DateAdded, '%d/%m/%Y') AS DateAdded");
                        //kolomContact = kolomContact.Replace("s.DateLastAdmin","DATE_FORMAT(s.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        //kolomContact = kolomContact.Replace("s.DateBirth","DATE_FORMAT(s.DateBirth, '%d/%m/%Y') AS DateBirth");
                        //if (kolomContact.Contains("s.Status"))
                        //{
                        //    kolomContact = kolomContact.Replace("s.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= s.Status)  'Contact Status'");
                        //}
                        //selectedColumns += kolomContact;
                        //n = n + 1;
                    }
                }

                if (property.Name.Equals("AnotherField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            anotherfiled = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            anotherfiled += "," + property.Value.ToString();
                        }
                        anotherfiled = anotherfiled.Replace("m.MarketType", "m.MarketType as 'Market Type'");
                        anotherfiled = anotherfiled.Replace("r.RoleName", "r.RoleName as 'Partner Type'");
                        anotherfiled = anotherfiled.Replace("sr.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= sr.Status) as 'Partner Type Status'");
                        anotherfiled = anotherfiled.Replace("sr.CSN", "sr.CSN as 'Partner Type CSN'");
                        anotherfiled = anotherfiled.Replace("sr.ITS_ID", "sr.ITS_ID as 'ITS Site ID'");
                        anotherfiled = anotherfiled.Replace("sr.UParent_CSN", "sr.UParent_CSN as 'Ultimate Parent CSN'");
                        anotherfiled = anotherfiled.Replace("c.geo_name", "c.geo_name as 'Geo'");
                        anotherfiled = anotherfiled.Replace("c.region_name", "c.region_name as 'Region'");
                        anotherfiled = anotherfiled.Replace("c.subregion_name", "c.subregion_name as 'SubRegion'");
                        anotherfiled = anotherfiled.Replace("tc.Territory_Name", "tc.Territory_Name as 'Territory'");

                        selectedColumns += anotherfiled;
                        n = n + 1;
                    }
                }
            }




            if (column["ContactField"] != null)
            {


                //query += selectedColumns + " FROM Contacts_All s inner join  Journals j on j.ParentId = s.ContactId inner join JournalActivities ja on j.ActivityId = ja.ActivityId Left join (select scl.ContactId, ss.SiteId,ss.SiteCountryCode,m.MarketType,r.RoleName,sr.Status,sr.CSN,sr.ITS_ID,sr.UParent_CSN,c.geo_name,c.region_name, " +
                //              " c.subregion_name,tc.Territory_Name,c.countries_name,ss.orgId,ss.SiteStatus_retired , ss.SiteName,ss.EnglishSiteName,ss.CommercialSiteName,ss.Workstations,ss.SiteTelephone,ss.SiteFax,ss.SiteEmailAddress,ss.SiteWebAddress,ss.SiteDepartment,ss.SiteAddress1,ss.SiteAddress2,ss.SiteAddress3,ss.SiteCity,ss.SiteStateProvince,ss.SitePostalCode,ss.MailingDepartment,ss.MailingAddress1,ss.MailingAddress2,ss.MailingAddress3,ss.MailingCity,ss.MailingStateProvince,ss.MailingCountryCode,ss.MailingPostalCode,ss.ShippingDepartment,ss.ShippingAddress1,ss.ShippingAddress2,ss.ShippingAddress3, " +
                //                  " ss.ShippingCity,ss.ShippingStateProvince,ss.ShippingCountryCode,ss.ShippingPostalCode,ss.SiebelDepartment,ss.SiteAdminFirstName,ss.SiteAdminLastName,ss.SiteAdminEmailAddress,ss.SiteAdminTelephone,ss.SiteAdminFax,ss.SiteManagerFirstName,ss.SiteManagerLastName,ss.SiteManagerEmailAddress,ss.SiteManagerTelephone,ss.SiteManagerFax," +
                //                  " ss.AdminNotes,ss.DateAdded,ss.AddedBy,ss.DateLastAdmin,ss.LastAdminBy from Main_organization o inner join Main_site ss on o.OrgId =ss.OrgId  inner join SiteContactLinks scl on scl.SiteId = ss.SiteId inner join Contacts_All ca on ca.ContactId = scl.ContactId and scl.Status ='A' inner join Ref_countries c  on c.countries_code = ss.SiteCountryCode inner join SiteRoles sr on sr.SiteId = ss.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId " +
                //             " inner join MarketType m on c.MarketTypeId = m.MarketTypeId  where sr.Status <> 'X'  AND ss.Status <> 'X' AND ss.Status <> 'D'  ";




                query += selectedColumns + " FROM Contacts_All s  Left join (select ja.ActivityName,ja.ActivityId,j.ActivityDate, scl.ContactId, ss.SiteId,ss.SiteCountryCode,m.MarketType,r.RoleName,sr.Status,sr.CSN,sr.ITS_ID,sr.UParent_CSN,c.geo_name,c.region_name,  c.subregion_name,tc.Territory_Name,c.countries_name,ss.orgId,ss.SiteStatus_retired , ss.SiteName,ss.EnglishSiteName,ss.CommercialSiteName,  ss.Workstations,ss.SiteTelephone,ss.SiteFax,ss.SiteEmailAddress,ss.SiteWebAddress,ss.SiteDepartment,ss.SiteAddress1,ss.SiteAddress2,ss.SiteAddress3,"+
                                             " ss.SiteCity,ss.SiteStateProvince,ss.SitePostalCode,ss.MailingDepartment,ss.MailingAddress1,ss.MailingAddress2,ss.MailingAddress3,ss.MailingCity, ss.MailingStateProvince,ss.MailingCountryCode,ss.MailingPostalCode,ss.ShippingDepartment,ss.ShippingAddress1,ss.ShippingAddress2,ss.ShippingAddress3,   ss.ShippingCity,ss.ShippingStateProvince,ss.ShippingCountryCode,ss.ShippingPostalCode,ss.SiebelDepartment,ss.SiteAdminFirstName,ss.SiteAdminLastName,ss.SiteAdminEmailAddress,ss.SiteAdminTelephone,ss.SiteAdminFax,ss.SiteManagerFirstName,ss.SiteManagerLastName,ss.SiteManagerEmailAddress,"+
                                             "  ss.SiteManagerTelephone,ss.SiteManagerFax, ss.AdminNotes,ss.DateAdded,ss.AddedBy,ss.DateLastAdmin,ss.LastAdminBy from  Journals j inner join Contacts_All ca on ca.ContactId = j.ParentId inner join JournalActivities ja on j.ActivityId = ja.ActivityId " +
                                             "   inner join SiteContactLinks scl on ca.ContactId = scl.ContactId and scl.Status = 'A' inner join Main_site ss on scl.SiteId = ss.SiteId inner join Main_organization o  on o.OrgId = ss.OrgId inner join Ref_countries c  on c.countries_code = ss.SiteCountryCode " +
                                             "   inner join SiteRoles sr on sr.SiteId = ss.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId inner join MarketType m on c.MarketTypeId = m.MarketTypeId  where sr.Status <> 'X'  AND ss.Status <> 'X' AND ss.Status <> 'D' and ja.ActivityId in ('" + column["Activity"] + "')  ";


            }
            else
            {

                if (column["PresentSetting"] !=null && column["PresentSetting"].ToString() == "free")
                {
                    query += selectedColumns + " FROM Main_site s " +
                                        " left join(select j.ParentId, ja.ActivityType, ja.ActivityId, ja.ActivityName, ss.SiteId,ss.SiteCountryCode, m.MarketType, r.RoleName, sr.Status, sr.CSN, sr.ITS_ID, sr.UParent_CSN, c.geo_name, c.region_name,c.subregion_name, tc.Territory_Name, c.countries_name, ss.orgId, ss.SiteStatus_retired, ss.SiteName, ss.EnglishSiteName,ss.CommercialSiteName, ss.Workstations, ss.SiteTelephone, ss.SiteFax, ss.SiteEmailAddress, ss.SiteWebAddress, ss.SiteDepartment, " +
                                          " ss.SiteAddress1, ss.SiteAddress2, ss.SiteAddress3, ss.SiteCity, ss.SiteStateProvince, ss.SitePostalCode, ss.MailingDepartment, " +
                                          " ss.MailingAddress1, ss.MailingAddress2, ss.MailingAddress3, ss.MailingCity, ss.MailingStateProvince, ss.MailingCountryCode,  ss.MailingPostalCode, ss.ShippingDepartment, ss.ShippingAddress1, ss.ShippingAddress2, ss.ShippingAddress3, ss.ShippingCity, " +
                                            " ss.ShippingStateProvince, ss.ShippingCountryCode, ss.ShippingPostalCode, ss.SiebelDepartment, ss.SiteAdminFirstName,ss.SiteAdminLastName, ss.SiteAdminEmailAddress, ss.SiteAdminTelephone, ss.SiteAdminFax, ss.SiteManagerFirstName, " +
                                           "   ss.SiteManagerLastName, ss.SiteManagerEmailAddress, ss.SiteManagerTelephone, ss.SiteManagerFax, ss.AdminNotes, ss.DateAdded,ss.AddedBy, ss.DateLastAdmin, ss.LastAdminBy FROM  Journals j  inner join Main_site ss on j.ParentId = ss.SiteId left join Main_organization o   on ss.OrgId = o.OrgId " +
                                                " left join JournalActivities ja on ja.ActivityId = j.ActivityId inner join Ref_countries c  on c.countries_code = ss.SiteCountryCode inner join SiteRoles sr  on sr.SiteId = ss.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId inner join MarketType m on c.MarketTypeId = m.MarketTypeId  where sr.Status <> 'X'  AND ss.Status <> 'X' " + 
                                               " AND ss.Status <> 'D' and ja.Status <> 'X' and ja.ActivityId in ('"+ column["Activity"] +"'))ja on s.SiteId = ja.ParentId left join Main_organization o on o.OrgId =s.OrgId " +
                                             "inner join Ref_countries c  on c.countries_code = s.SiteCountryCode " +
                                              " inner join SiteRoles sr on sr.SiteId = s.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId " +
                                              " inner join MarketType m on c.MarketTypeId = m.MarketTypeId where " +
                                              " sr.Status <> 'X'  AND s.Status <> 'X' AND s.Status <> 'D'  AND o.Status <> 'X' AND o.Status <> 'D' ";




                }
                else
                {
                    query += selectedColumns + " FROM Main_organization o inner join Main_site s on o.OrgId =s.OrgId " +
                             "inner join Ref_countries c  on c.countries_code = s.SiteCountryCode " +
                               " inner join SiteRoles sr on sr.SiteId = s.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId " +
                             " inner join MarketType m on c.MarketTypeId = m.MarketTypeId inner join Journals j on j.ParentId = s.SiteId inner join JournalActivities ja on j.ActivityId = ja.ActivityId where " +
                              " sr.Status <> 'X'  AND s.Status <> 'X' AND s.Status <> 'D'  AND o.Status <> 'X' AND o.Status <> 'D' ";

                }

            }
            // AND j.ActivityId = ja.ActivityId

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                /*fix issue excel no 263.Show data based role*/

                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        role = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        orgid = property.Value.ToString();
                    }
                }



                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() == null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "m.MarketTypeId in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }



                if (property.Name.Equals("LogAfterMarch"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != "True"))
                    { /* saya ubah true -> True , biar masuk kondisi , boom issue 05092018 */
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "DATEDIFF(s.LastLogin,'2009-03-01') >= 0";
                        i = i + 1;
                    }
                }



                if (property.Name.Equals("SiteCountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                     
                        if (column["ContactField"] == null)
                        {
                            query += "s.SiteCountryCode in (" + property.Value.ToString() + ")";
                        }else
                            query += "ss.SiteCountryCode in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                /* tambah parameter partner status issue 05091028 */

                if (property.Name.Equals("PartnerStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "sr.`Status` in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                /* tambah parameter partner status issue 05091028 */

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }

                        /*fix issue excel no 263.Show data based role*/

                        if (role == "")
                        {
                            query += "tc.TerritoryId in (" + property.Value.ToString() + ") ";
                        }
                        else if (role == "DISTRIBUTOR")
                        {
                            query += "tc.TerritoryId in (select rc.TerritoryId from Ref_countries rc INNER JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + orgid + ") GROUP BY rc.TerritoryId) ";
                        }
                        else
                        {
                            query += "tc.TerritoryId in (" + property.Value.ToString() + ") AND o.OrgId in (" + orgid + ") ";
                        }

                        /*end fix issue excel no 263.Show data based role*/

                        i = i + 1;
                    }
                }

                /*end fix issue excel no 263.Show data based role*/


            }
            int js = 0;
            // query += " order by s.ContactId";
            if (column["ContactField"] != null)
            {
                if (column["PresentSetting"] != null && column["PresentSetting"].ToString() != "free")
                {
                    query += " ) ss on ss.ContactId = s.ContactId  where "; /* tambah parameter partner status issue 05091028 */
                }
                else
                {
                    query += " ) ss on ss.ContactId = s.ContactId and ";
                }

            }

            else
            {
                //query += " and ";
                js = 1;
            }

            
            foreach (JProperty property in json.Properties())
            {
                if (column["PresentSetting"] !=null && column["PresentSetting"].ToString() != "free")
                {
                    if (property.Name.Equals("Activity"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            if (js > 0) { query += " and "; }
                            if (column["ContactField"] != null)
                            {
                                query += "ss.ActivityId in (" + property.Value.ToString() + ") ";
                            }else
                                query += "j.ActivityId in (" + property.Value.ToString() + ") ";
                            js = js + 1;
                        }
                    }
                }

                    if (property.Name.Equals("PresentSetting"))
                {
                    if ((property.Value.ToString() == "free") && (property.Value.ToString() != null))
                    {
                        //  property["ActivityDate"].Values("");
                        json["ActivityDate"] = "";
                      //  Status = false;
                    }
                }

                if (property.Name.Equals("ActivityDate"))
                {
                    if ((property.Value.ToString() != ",") && (property.Value.ToString() != null) && (property.Value.ToString() != ""))
                    {

                        if (js > 0) { query += " and "; }
                        string[] startendfilter = property.Value.ToString().Split(',');

                        if (column["ContactField"] != null)
                        {
                            query += "ss.ActivityDate BETWEEN '" + startendfilter[0] + "' AND '" + startendfilter[1].Replace("[", "").Replace("]", "") + "' ";
                        }else
                            query += "j.ActivityDate BETWEEN '" + startendfilter[0] + "' AND '" + startendfilter[1].Replace("[", "").Replace("]", "") + "' ";
                        js = js + 1;
                    }
                }

                if (property.Name.Equals("PrimarySite"))
                {
                    
                    if ((property.Value.ToString() != "") && (property.Value.ToString() == "True"))
                    { /* saya ubah true -> True , biar masuk kondisi , boom issue 05092018 */
                        if (js > 0) { query += " and "; }
                        if (column["ContactField"] == null)
                        {
                            query += "PrimarySiteId = s.SiteId ";
                        }
                        else
                        {
                            query += "s.PrimarySiteId = ss.SiteId ";
                        }
                        js = js + 1;
                    }
                }

                if (property.Name.Equals("LastLogin"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (js > 0) { query += " and "; }
                        query += "DATEDIFF(Now(),s.LastLogin) <= " + property.Value.ToString();
                        js = js + 1;
                    }
                }

            }

            if (js != 0)
            {
                query += " and s.Status <> 'X' AND s.Status <> 'D'  ";
            }
            if (column["ContactField"] != null)
            {
                query += "  GROUP BY s.ContactId";
            }
            else
            {
                // query += "  s.Status <> 'X' AND s.Status <> 'D'  ";
            }
            List<dynamic> dynamicList = new List<dynamic>();

            try
            {


                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataService.ToDynamic(ds.Tables[0]);

                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);    
            }
            return Ok(result);
        }

        [HttpPost("ContactAttributeReport")]
        public IActionResult ContactAttributeReport([FromBody] dynamic data)
        {
            JArray columnSite;
            JArray columnContact;
            string query = "";
            string selectedColumns = "";
            string kolomSite = "";
            string kolomContact = "";
            string anotherfiled = "";
            /*fix issue excel no 263.Show data based role*/
            //  Boolean Status = false;
            string role = "";
            string orgid = "";

            /*end fix issue excel no 263.Show data based role*/




            int n = 1;
            JObject column = JObject.FromObject(data);
            if (column["ContactField"] != null)
            {
                selectedColumns += "select DISTINCT ss.ActivityName ";
            }
            else
                selectedColumns += "select DISTINCT ja.ActivityName ";
            foreach (JProperty property in column.Properties())
            {
                if (property.Name.Equals("SiteField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomSite = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomSite = "," + property.Value.ToString();
                        }

                        kolomSite = kolomSite.Replace("ss.DateAdded", "DATE_FORMAT(ss.DateAdded, '%d/%m/%Y') AS DateAdded");
                        kolomSite = kolomSite.Replace("ss.DateLastAdmin", "DATE_FORMAT(ss.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        //  kolomSite = kolomSite.Replace("ss.Status","GROUP_CONCAT(DISTINCT sr.Status) AS Status"); /* tambah parameter partner status issue 05091028 */
                        if (kolomSite.Contains("Status"))
                        {
                            kolomSite = kolomSite.Replace("ss.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= ss.Status)  'Site Status'");
                        }


                        if (kolomSite.Contains("SiteCountryCode"))
                        {
                            if (column["ContactField"] != null)
                            {
                                kolomSite = kolomSite.Replace("ss.SiteCountryCode", "ss.countries_name as 'Site Country'");
                            }
                            else
                                kolomSite = kolomSite.Replace("ss.SiteCountryCode", "c.countries_name as 'Site Country'");
                        }

                        if (kolomSite.Contains("MailingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("ss.MailingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = ss.MailingCountryCode) as 'Mailing Country'");
                        }
                        if (kolomSite.Contains("ShippingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("ss.ShippingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = ss.ShippingCountryCode) as 'Shipping Country'");
                        }


                        if (kolomSite.Contains("SiteId"))
                        {
                            if (column["ContactField"] == null)
                            {
                                kolomSite = kolomSite.Replace("ss.SiteId", "s.SiteId");


                            }

                        }

                        selectedColumns += kolomSite;
                        n = n + 1;
                    }
                    else
                    {


                        //  sb = new StringBuilder();
                        //  sb.AppendFormat("DESCRIBE Main_site");
                        //  sqlCommand = new MySqlCommand(sb.ToString());
                        //  dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        //  hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        //  columnSite = JArray.Parse(hasil);

                        //  string[] columnarrtmp = new string[columnSite.Count];
                        //  for (int index = 0; index < columnSite.Count; index++)
                        //  {
                        //      columnarrtmp[index] = "ss." + columnSite[index]["Field"].ToString();
                        //  }

                        //  if(n==0){
                        //      kolomSite = string.Join(",", columnarrtmp);
                        //  } else if(n>0){
                        //      kolomSite = ","+string.Join(",", columnarrtmp);
                        //  }

                        //  kolomSite = kolomSite.Replace("ss.DateAdded","DATE_FORMAT(ss.DateAdded, '%d/%m/%Y') AS DateAdded");
                        //  kolomSite = kolomSite.Replace("ss.DateLastAdmin","DATE_FORMAT(ss.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        ////  kolomSite = kolomSite.Replace("ss.Status","GROUP_CONCAT(DISTINCT sr.Status) AS Status"); /* tambah parameter partner status issue 05091028 */
                        //  if (kolomSite.Contains("Status"))
                        //  {
                        //      kolomSite = kolomSite.Replace("ss.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= ss.Status)  'Site Status'");
                        //  }

                        //  if (kolomSite.Contains("SiteCountryCode"))
                        //  {
                        //      kolomSite = kolomSite.Replace("ss.SiteCountryCode", "ss.countries_name as 'Site Country'");
                        //  }
                        //  selectedColumns += kolomSite;

                        //  n = n + 1;
                    }
                }

                if (property.Name.Equals("ContactField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomContact = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomContact = "," + property.Value.ToString();
                        }

                        kolomContact = kolomContact.Replace("s.DateAdded", "DATE_FORMAT(s.DateAdded, '%d/%m/%Y') AS DateAdded");
                        kolomContact = kolomContact.Replace("s.DateLastAdmin", "DATE_FORMAT(s.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        kolomContact = kolomContact.Replace("s.DateBirth", "DATE_FORMAT(s.DateBirth, '%d/%m/%Y') AS DateBirth");
                        if (kolomContact.Contains("s.Status") && !kolomContact.Contains("s.StatusEmailSend"))
                        {
                            kolomContact = kolomContact.Replace("s.Status,", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= s.Status)  'Contact Status',");
                        }
                        if (kolomContact.Contains("s.CountryCode"))
                        {
                            kolomContact = kolomContact.Replace("s.CountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.CountryCode) as 'Contact Country' ");
                        }
                        selectedColumns += kolomContact;
                        n = n + 1;
                    }
                    else
                    {
                        //sb = new StringBuilder();
                        //sb.AppendFormat("DESCRIBE Contacts_All");
                        //sqlCommand = new MySqlCommand(sb.ToString());
                        //dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        //hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        //columnContact = JArray.Parse(hasil);

                        //string[] columnarrtmp = new string[columnContact.Count - 1];
                        //int j = 0;
                        //for (int index = 0; index < columnContact.Count; index++)
                        //{
                        //    if (columnContact[index]["Field"].ToString() != "password")
                        //    {
                        //        columnarrtmp[j] = "s." + columnContact[index]["Field"].ToString();
                        //        j++;
                        //    }
                        //}
                        //if(n==0){
                        //    kolomContact=  string.Join(",",columnarrtmp);
                        //}else if(n>0){
                        //    kolomContact=  ","+string.Join(",",columnarrtmp);
                        //}

                        //kolomContact = kolomContact.Replace("s.DateAdded","DATE_FORMAT(s.DateAdded, '%d/%m/%Y') AS DateAdded");
                        //kolomContact = kolomContact.Replace("s.DateLastAdmin","DATE_FORMAT(s.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        //kolomContact = kolomContact.Replace("s.DateBirth","DATE_FORMAT(s.DateBirth, '%d/%m/%Y') AS DateBirth");
                        //if (kolomContact.Contains("s.Status"))
                        //{
                        //    kolomContact = kolomContact.Replace("s.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= s.Status)  'Contact Status'");
                        //}
                        //selectedColumns += kolomContact;
                        //n = n + 1;
                    }
                }

                if (property.Name.Equals("AnotherField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            anotherfiled = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            anotherfiled += "," + property.Value.ToString();
                        }
                        anotherfiled = anotherfiled.Replace("m.MarketType", "m.MarketType as 'Market Type'");
                        anotherfiled = anotherfiled.Replace("r.RoleName", "r.RoleName as 'Partner Type'");
                        anotherfiled = anotherfiled.Replace("sr.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= sr.Status) as 'Partner Type Status'");
                        anotherfiled = anotherfiled.Replace("sr.CSN", "sr.CSN as 'Partner Type CSN'");
                        anotherfiled = anotherfiled.Replace("sr.ITS_ID", "sr.ITS_ID as 'ITS Site ID'");
                        anotherfiled = anotherfiled.Replace("sr.UParent_CSN", "sr.UParent_CSN as 'Ultimate Parent CSN'");
                        anotherfiled = anotherfiled.Replace("c.geo_name", "c.geo_name as 'Geo'");
                        anotherfiled = anotherfiled.Replace("c.region_name", "c.region_name as 'Region'");
                        anotherfiled = anotherfiled.Replace("c.subregion_name", "c.subregion_name as 'SubRegion'");
                        anotherfiled = anotherfiled.Replace("tc.Territory_Name", "tc.Territory_Name as 'Territory'");

                        selectedColumns += anotherfiled;
                        n = n + 1;
                    }
                }
            }




            if (column["ContactField"] != null)
            {


                //query += selectedColumns + " FROM Contacts_All s inner join  Journals j on j.ParentId = s.ContactId inner join JournalActivities ja on j.ActivityId = ja.ActivityId Left join (select scl.ContactId, ss.SiteId,ss.SiteCountryCode,m.MarketType,r.RoleName,sr.Status,sr.CSN,sr.ITS_ID,sr.UParent_CSN,c.geo_name,c.region_name, " +
                //              " c.subregion_name,tc.Territory_Name,c.countries_name,ss.orgId,ss.SiteStatus_retired , ss.SiteName,ss.EnglishSiteName,ss.CommercialSiteName,ss.Workstations,ss.SiteTelephone,ss.SiteFax,ss.SiteEmailAddress,ss.SiteWebAddress,ss.SiteDepartment,ss.SiteAddress1,ss.SiteAddress2,ss.SiteAddress3,ss.SiteCity,ss.SiteStateProvince,ss.SitePostalCode,ss.MailingDepartment,ss.MailingAddress1,ss.MailingAddress2,ss.MailingAddress3,ss.MailingCity,ss.MailingStateProvince,ss.MailingCountryCode,ss.MailingPostalCode,ss.ShippingDepartment,ss.ShippingAddress1,ss.ShippingAddress2,ss.ShippingAddress3, " +
                //                  " ss.ShippingCity,ss.ShippingStateProvince,ss.ShippingCountryCode,ss.ShippingPostalCode,ss.SiebelDepartment,ss.SiteAdminFirstName,ss.SiteAdminLastName,ss.SiteAdminEmailAddress,ss.SiteAdminTelephone,ss.SiteAdminFax,ss.SiteManagerFirstName,ss.SiteManagerLastName,ss.SiteManagerEmailAddress,ss.SiteManagerTelephone,ss.SiteManagerFax," +
                //                  " ss.AdminNotes,ss.DateAdded,ss.AddedBy,ss.DateLastAdmin,ss.LastAdminBy from Main_organization o inner join Main_site ss on o.OrgId =ss.OrgId  inner join SiteContactLinks scl on scl.SiteId = ss.SiteId inner join Contacts_All ca on ca.ContactId = scl.ContactId and scl.Status ='A' inner join Ref_countries c  on c.countries_code = ss.SiteCountryCode inner join SiteRoles sr on sr.SiteId = ss.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId " +
                //             " inner join MarketType m on c.MarketTypeId = m.MarketTypeId  where sr.Status <> 'X'  AND ss.Status <> 'X' AND ss.Status <> 'D'  ";




                query += selectedColumns + " FROM Contacts_All s  Left join (select ja.ActivityName,ja.ActivityId,j.ActivityDate, scl.ContactId, ss.SiteId,ss.SiteCountryCode,m.MarketType,r.RoleName,sr.Status,sr.CSN,sr.ITS_ID,sr.UParent_CSN,c.geo_name,c.region_name,  c.subregion_name,tc.Territory_Name,c.countries_name,ss.orgId,ss.SiteStatus_retired , ss.SiteName,ss.EnglishSiteName,ss.CommercialSiteName,  ss.Workstations,ss.SiteTelephone,ss.SiteFax,ss.SiteEmailAddress,ss.SiteWebAddress,ss.SiteDepartment,ss.SiteAddress1,ss.SiteAddress2,ss.SiteAddress3," +
                                             " ss.SiteCity,ss.SiteStateProvince,ss.SitePostalCode,ss.MailingDepartment,ss.MailingAddress1,ss.MailingAddress2,ss.MailingAddress3,ss.MailingCity, ss.MailingStateProvince,ss.MailingCountryCode,ss.MailingPostalCode,ss.ShippingDepartment,ss.ShippingAddress1,ss.ShippingAddress2,ss.ShippingAddress3,   ss.ShippingCity,ss.ShippingStateProvince,ss.ShippingCountryCode,ss.ShippingPostalCode,ss.SiebelDepartment,ss.SiteAdminFirstName,ss.SiteAdminLastName,ss.SiteAdminEmailAddress,ss.SiteAdminTelephone,ss.SiteAdminFax,ss.SiteManagerFirstName,ss.SiteManagerLastName,ss.SiteManagerEmailAddress," +
                                             "  ss.SiteManagerTelephone,ss.SiteManagerFax, ss.AdminNotes,ss.DateAdded,ss.AddedBy,ss.DateLastAdmin,ss.LastAdminBy,j.Notes from  Journals j inner join Contacts_All ca on ca.ContactId = j.ParentId inner join JournalActivities ja on j.ActivityId = ja.ActivityId " +
                                             "   inner join SiteContactLinks scl on ca.ContactId = scl.ContactId and scl.Status = 'A' inner join Main_site ss on scl.SiteId = ss.SiteId inner join Main_organization o  on o.OrgId = ss.OrgId inner join Ref_countries c  on c.countries_code = ss.SiteCountryCode " +
                                             "   inner join SiteRoles sr on sr.SiteId = ss.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId inner join MarketType m on c.MarketTypeId = m.MarketTypeId  where sr.Status <> 'X'  AND ss.Status <> 'X' AND ss.Status <> 'D' and scl.Status <>'X' and j.Status <> 'X' and ja.ActivityId in ('" + column["Activity"] + "')  ";


            }
            else
            {

                if (column["PresentSetting"] != null && column["PresentSetting"].ToString() == "free")
                {
                    query += selectedColumns + " FROM Main_site s " +
                                        " left join(select j.ParentId, ja.ActivityType, ja.ActivityId, ja.ActivityName, ss.SiteId,ss.SiteCountryCode, m.MarketType, r.RoleName, sr.Status, sr.CSN, sr.ITS_ID, sr.UParent_CSN, c.geo_name, c.region_name,c.subregion_name, tc.Territory_Name, c.countries_name, ss.orgId, ss.SiteStatus_retired, ss.SiteName, ss.EnglishSiteName,ss.CommercialSiteName, ss.Workstations, ss.SiteTelephone, ss.SiteFax, ss.SiteEmailAddress, ss.SiteWebAddress, ss.SiteDepartment, " +
                                          " ss.SiteAddress1, ss.SiteAddress2, ss.SiteAddress3, ss.SiteCity, ss.SiteStateProvince, ss.SitePostalCode, ss.MailingDepartment, " +
                                          " ss.MailingAddress1, ss.MailingAddress2, ss.MailingAddress3, ss.MailingCity, ss.MailingStateProvince, ss.MailingCountryCode,  ss.MailingPostalCode, ss.ShippingDepartment, ss.ShippingAddress1, ss.ShippingAddress2, ss.ShippingAddress3, ss.ShippingCity, " +
                                            " ss.ShippingStateProvince, ss.ShippingCountryCode, ss.ShippingPostalCode, ss.SiebelDepartment, ss.SiteAdminFirstName,ss.SiteAdminLastName, ss.SiteAdminEmailAddress, ss.SiteAdminTelephone, ss.SiteAdminFax, ss.SiteManagerFirstName, " +
                                           "   ss.SiteManagerLastName, ss.SiteManagerEmailAddress, ss.SiteManagerTelephone, ss.SiteManagerFax, ss.AdminNotes, ss.DateAdded,ss.AddedBy, ss.DateLastAdmin, ss.LastAdminBy FROM  Journals j  inner join Main_site ss on j.ParentId = ss.SiteId left join Main_organization o   on ss.OrgId = o.OrgId " +
                                                " left join JournalActivities ja on ja.ActivityId = j.ActivityId inner join Ref_countries c  on c.countries_code = ss.SiteCountryCode inner join SiteRoles sr  on sr.SiteId = ss.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId inner join MarketType m on c.MarketTypeId = m.MarketTypeId  where sr.Status <> 'X'  AND ss.Status <> 'X' " +
                                               " AND ss.Status <> 'D' and ja.Status <> 'X' and ja.ActivityId in ('" + column["Activity"] + "'))ja on s.SiteId = ja.ParentId left join Main_organization o on o.OrgId =s.OrgId " +
                                             "inner join Ref_countries c  on c.countries_code = s.SiteCountryCode " +
                                              " inner join SiteRoles sr on sr.SiteId = s.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId " +
                                              " inner join MarketType m on c.MarketTypeId = m.MarketTypeId where " +
                                              "  sr.Status <> 'X'  AND s.Status <> 'X' AND s.Status <> 'D'  AND o.Status <> 'X' AND o.Status <> 'D' ";




                }
                else
                {
                    query += selectedColumns + " FROM Main_organization o inner join Main_site s on o.OrgId =s.OrgId " +
                             "inner join Ref_countries c  on c.countries_code = s.SiteCountryCode " +
                               " inner join SiteRoles sr on sr.SiteId = s.SiteId inner join Roles r on r.RoleId = sr.RoleId inner join Ref_territory tc on tc.TerritoryId = c.TerritoryId " +
                             " inner join MarketType m on c.MarketTypeId = m.MarketTypeId inner join Journals j on j.ParentId = s.SiteId inner join JournalActivities ja on j.ActivityId = ja.ActivityId where " +
                              " sr.Status <> 'X'  AND s.Status <> 'X' AND s.Status <> 'D'  AND o.Status <> 'X' AND o.Status <> 'D' ";

                }

            }
            // AND j.ActivityId = ja.ActivityId

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                /*fix issue excel no 263.Show data based role*/

                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        role = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        orgid = property.Value.ToString();
                    }
                }



                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() == null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "m.MarketTypeId in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }



                if (property.Name.Equals("LogAfterMarch"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != "True"))
                    { /* saya ubah true -> True , biar masuk kondisi , boom issue 05092018 */
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "DATEDIFF(s.LastLogin,'2009-03-01') >= 0";
                        i = i + 1;
                    }
                }



                if (property.Name.Equals("SiteCountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }

                        if (column["ContactField"] == null)
                        {
                            query += "s.SiteCountryCode in (" + property.Value.ToString() + ")";
                        }
                        else
                            query += "ss.SiteCountryCode in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                /* tambah parameter partner status issue 05091028 */

                if (property.Name.Equals("PartnerStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "sr.`Status` in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                /* tambah parameter partner status issue 05091028 */

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }

                        /*fix issue excel no 263.Show data based role*/

                        if (role == "")
                        {
                            query += "tc.TerritoryId in (" + property.Value.ToString() + ") ";
                        }
                        else if (role == "DISTRIBUTOR")
                        {
                            query += "tc.TerritoryId in (select rc.TerritoryId from Ref_countries rc INNER JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + orgid + ") GROUP BY rc.TerritoryId) ";
                        }
                        else
                        {
                            query += "tc.TerritoryId in (" + property.Value.ToString() + ") AND o.OrgId in (" + orgid + ") ";
                        }

                        /*end fix issue excel no 263.Show data based role*/

                        i = i + 1;
                    }
                }

                /*end fix issue excel no 263.Show data based role*/


            }
            int js = 0;
            // query += " order by s.ContactId";
            if (column["ContactField"] != null)
            {
                if (column["PresentSetting"] != null && column["PresentSetting"].ToString() != "free")
                {
                    query += " ) ss on ss.ContactId = s.ContactId  where "; /* tambah parameter partner status issue 05091028 */
                }
                else
                {
                    query += " ) ss on ss.ContactId = s.ContactId  ";
                }

            }

            else
            {
                //query += " and ";
                js = 1;
            }


            foreach (JProperty property in json.Properties())
            {
                if (column["PresentSetting"] != null && column["PresentSetting"].ToString() != "free")
                {
                    if (property.Name.Equals("Activity"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            if (js > 0) { query += " and "; }
                            if (column["ContactField"] != null)
                            {
                                query += "ss.ActivityId in (" + property.Value.ToString() + ") ";
                            }
                            else
                                query += "j.ActivityId in (" + property.Value.ToString() + ") ";
                            js = js + 1;
                        }
                    }
                }

                if (property.Name.Equals("PresentSetting"))
                {
                    if ((property.Value.ToString() == "free") && (property.Value.ToString() != null))
                    {
                        //  property["ActivityDate"].Values("");
                        json["ActivityDate"] = "";
                        //  Status = false;
                    }
                }

                if (property.Name.Equals("ActivityDate"))
                {
                    if ((property.Value.ToString() != ",") && (property.Value.ToString() != null) && (property.Value.ToString() != ""))
                    {

                        if (js > 0) { query += " and "; }
                        string[] startendfilter = property.Value.ToString().Split(',');

                        if (column["ContactField"] != null)
                        {
                            query += "ss.ActivityDate BETWEEN '" + startendfilter[0] + "' AND '" + startendfilter[1].Replace("[", "").Replace("]", "") + "' ";
                        }
                        else
                            query += "j.ActivityDate BETWEEN '" + startendfilter[0] + "' AND '" + startendfilter[1].Replace("[", "").Replace("]", "") + "' ";
                        js = js + 1;
                    }
                }

                if (property.Name.Equals("PrimarySite"))
                {

                    if ((property.Value.ToString() != "") && (property.Value.ToString() == "True"))
                    { /* saya ubah true -> True , biar masuk kondisi , boom issue 05092018 */
                        if (js > 0) { query += " and "; }
                        if (column["PresentSetting"] != null && column["PresentSetting"].ToString() != "free")
                        {
                            query += "s.PrimarySiteId = ss.SiteId ";
                        }
                        else if (js == 0)
                        {
                            query += " and s.PrimarySiteId = ss.SiteId ";
                        }
                        js = js + 1;
                    }
                }

                if (property.Name.Equals("LastLogin"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (js > 0) { query += " and "; }
                        query += "DATEDIFF(Now(),s.LastLogin) <= " + property.Value.ToString();
                        js = js + 1;
                    }
                }

            }

            if (js != 0)
            {
                //query += " and s.Status <> 'X' AND s.Status <> 'D'  ";
            }
            if (column["ContactField"] != null)
            {
                query += "  GROUP BY s.ContactId";
            }
            else
            {
                // query += "  s.Status <> 'X' AND s.Status <> 'D'  ";
            }
            List<dynamic> dynamicList = new List<dynamic>();

            try
            {


                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataService.ToDynamic(ds.Tables[0]);

                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);    
            }
            return Ok(result);
        }



        [HttpPost("QualificationByContact")]
        public IActionResult QualificationByContactReport([FromBody] dynamic data)
        {
            JArray columnSite;
            JArray columnContact;
            string query = "";
            string selectedColumns = "";
            string kolomSite = "";
            string kolomContact = "";
            string anotherfiled = "";
            selectedColumns += "SELECT DISTINCT ";

            /*fix issue excel no 263.Show data based role*/

            string role = "";
            string orgid = "";

            /*end fix issue excel no 263.Show data based role*/

            int n = 0;
            JObject column = JObject.FromObject(data);
            foreach (JProperty property in column.Properties())
            {
                if (property.Name.Equals("SiteField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomSite = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomSite = "," + property.Value.ToString();
                        }
                        if (kolomSite.Contains("SiteCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.SiteCountryCode", "rc.countries_name as 'Site Country'");
                        }
                        if (kolomSite.Contains("MailingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.MailingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.MailingCountryCode) as 'Mailing Country'");
                        }
                        if (kolomSite.Contains("ShippingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.ShippingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.ShippingCountryCode) as 'Shipping Country'");
                        }
                        if (kolomSite.Contains("SiebelCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.SiebelCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.SiebelCountryCode) as 'Siebel Country'");
                        }
                        selectedColumns += kolomSite;

                        n = n + 1;
                    }
                    else
                    {
                        n = 0;
                        sb = new StringBuilder();
                        sb.AppendFormat("DESCRIBE Main_site");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        columnSite = JArray.Parse(hasil);

                        string[] columnarrtmp = new string[columnSite.Count];
                        for (int index = 0; index < columnSite.Count; index++)
                        {
                            if (columnSite[index]["Field"].ToString() != "SiteIdInt" && columnSite[index]["Field"].ToString() != "Status" && columnSite[index]["Field"].ToString() != "DateAdded" && columnSite[index]["Field"].ToString() != "AddedBy" && columnSite[index]["Field"].ToString() != "DateLastAdmin" && columnSite[index]["Field"].ToString() != "LastAdminBy" && columnSite[index]["Field"].ToString() != "CSOUpdatedOn" && columnSite[index]["Field"].ToString() != "CSOVersion" && columnSite[index]["Field"].ToString() != "CSOpartner_type")
                            {
                                columnarrtmp[index] = columnSite[index]["Field"].ToString();
                            }
                        }

                        if (n == 0)
                        {
                            kolomSite = string.Join(",", property.Value.ToString()); //string.Join(",", columnarrtmp);
                        }
                        else if (n > 0)
                        {
                            kolomSite = "," + string.Join(",", property.Value.ToString());//string.Join(",", columnarrtmp);
                        }
                        if (kolomSite.Contains("SiteCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.SiteCountryCode", "rc.countries_name as 'Site Country'");
                        }
                        if (kolomSite.Contains("MailingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.MailingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.MailingCountryCode) as 'Mailing Country'");
                        }
                        if (kolomSite.Contains("ShippingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.ShippingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.ShippingCountryCode) as 'Shipping Country'");
                        }
                        if (kolomSite.Contains("SiebelCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("s.SiebelCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = s.SiebelCountryCode) as 'Siebel Country'");
                        }
                        selectedColumns += kolomSite;

                        n = n + 1;
                    }
                }

                if (property.Name.Equals("ContactField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (string.IsNullOrEmpty(kolomSite))
                        {
                            n = 0;
                        }
                        if (n == 0)
                        {
                            kolomContact = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomContact = "," + property.Value.ToString();
                        }

                        kolomContact = kolomContact.Replace("c.DateAdded", "DATE_FORMAT(c.DateAdded, '%d/%m/%Y') AS DateAdded");
                        kolomContact = kolomContact.Replace("c.DateLastAdmin", "DATE_FORMAT(c.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        kolomContact = kolomContact.Replace("c.DateBirth", "DATE_FORMAT(c.DateBirth, '%d/%m/%Y') AS DateBirth");
                        if (kolomContact.Contains("Status") && !kolomContact.Contains("s.StatusEmailSend"))
                        {
                            kolomContact = kolomContact.Replace("c.Status,", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= c.Status)  'Contact Status',");
                        }
                        if (kolomContact.Contains("CountryCode"))
                        {
                            kolomContact = kolomContact.Replace("c.CountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = c.CountryCode) as 'Contact Country' ");
                        }
                        selectedColumns += kolomContact;

                        n = n + 1;
                    }
                    else
                    {
                        n = 0;
                        sb = new StringBuilder();
                        sb.AppendFormat("DESCRIBE Contacts_All");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        columnContact = JArray.Parse(hasil);

                        string[] columnarrtmp = new string[columnContact.Count - 1];
                        int j = 0;
                        for (int index = 0; index < columnContact.Count; index++)
                        {
                            if (columnContact[index]["Field"].ToString() != "password")
                            {
                                columnarrtmp[j] = columnContact[index]["Field"].ToString();
                                j++;
                            }
                        }

                        if (n == 0)
                        {
                            kolomContact = string.Join(",", property.Value.ToString());
                        }
                        else if (n > 0)
                        {
                            kolomContact = "," + string.Join(",", property.Value.ToString());
                        }

                        kolomContact = kolomContact.Replace("c.DateAdded", "DATE_FORMAT(c.DateAdded, '%d/%m/%Y') AS DateAdded");
                        kolomContact = kolomContact.Replace("c.DateLastAdmin", "DATE_FORMAT(c.DateLastAdmin, '%d/%m/%Y') AS DateLastAdmin");
                        kolomContact = kolomContact.Replace("c.DateBirth", "DATE_FORMAT(c.DateBirth, '%d/%m/%Y') AS DateBirth");
                        if (kolomContact.Contains("Status") && !kolomContact.Contains("s.StatusEmailSend"))
                        {
                            kolomContact = kolomContact.Replace("c.Status,", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= c.Status)  'Contact Status',");
                        }
                        if (kolomContact.Contains("CountryCode"))
                        {
                            kolomContact = kolomContact.Replace("c.CountryCode", " (select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = c.CountryCode) as 'Contact Country' ");
                        }
                        selectedColumns += kolomContact;

                        n = n + 1;
                    }
                }

                if (property.Name.Equals("AnotherField"))
                {
                    if (string.IsNullOrEmpty(kolomContact) && string.IsNullOrEmpty(kolomSite))
                    {
                        n = 0;
                    }
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            anotherfiled = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            anotherfiled += "," + property.Value.ToString();
                        }
                        anotherfiled = anotherfiled.Replace("m.MarketType", "m.MarketType as 'Market Type'");
                        anotherfiled = anotherfiled.Replace("r.RoleName", "r.RoleName as 'Partner Type'");
                        anotherfiled = anotherfiled.Replace("p.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= p.Status) as 'Partner Type Status'");
                        anotherfiled = anotherfiled.Replace("sr.CSN", "sr.CSN as 'Partner Type CSN'");
                        anotherfiled = anotherfiled.Replace("sr.ITS_ID", "sr.ITS_ID as 'ITS Site ID'");
                        anotherfiled = anotherfiled.Replace("sr.UParent_CSN", "sr.UParent_CSN as 'Ultimate Parent CSN'");
                        anotherfiled = anotherfiled.Replace("rc.geo_name", "rc.geo_name as 'Geo'");
                        anotherfiled = anotherfiled.Replace("rc.region_name", "rc.region_name as 'Region'");
                        anotherfiled = anotherfiled.Replace("rc.subregion_name", "rc.subregion_name as 'SubRegion'");
                        anotherfiled = anotherfiled.Replace("t.Territory_Name", "t.Territory_Name as 'Territory'");
                        selectedColumns += anotherfiled;
                        n = n + 1;
                    }
                }

                /*fix issue excel no 263.Show data based role*/

                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        role = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        orgid = property.Value.ToString();
                    }
                }

                /*end fix issue excel no 263.Show data based role*/
            }

            // query += selectedColumns + " FROM Contacts_All c JOIN Journals j ON j.ParentId = c.ContactId " +
            // " JOIN SiteContactLinks scl ON scl.ContactId = c.ContactId " +
            // " JOIN Main_site ss ON scl.SiteId = ss.SiteId " +
            // " JOIN Main_organization o ON ss.OrgId = o.OrgId " +
            // " JOIN Ref_countries rc ON ss.SiteCountryCode = rc.countries_code " +
            // " JOIN SiteRoles sr ON ss.SiteId = sr.SiteId " +
            // " JOIN Roles r ON sr.RoleId = r.RoleId " +
            // " JOIN Applications app ON c.ContactId = app.ContactId " +
            // " JOIN ApplicationsProducts ap ON app.ApplicationId = ap.ApplicationId " +
            // " JOIN Products p ON ap.ProductId = p.productId " +
            // " JOIN JournalActivities ja ON j.ActivityId = ja.ActivityId " +
            // " LEFT JOIN Ref_territory t ON t.TerritoryId = rc.TerritoryId " +
            // " INNER JOIN MarketType m ON rc.MarketTypeId = m.MarketTypeId " +
            // " WHERE sr.STATUS <> 'X' AND scl.STATUS <> 'X' AND ss.STATUS <> 'X' AND ss.STATUS <> 'D' AND o.STATUS <> 'X' AND o.STATUS <> 'D' AND j.STATUS <> 'X' AND ja.STATUS <> 'X' AND c.STATUS <> 'X' AND c.STATUS <> 'D' ";

            //query += selectedColumns + " FROM " +
            //"(SELECT rc.*, r.RoleName AS PartnerType,sr.`Status` AS PartnerTypeStatus,sr.CSN AS PartnerTypeCSN,sr.ITS_ID AS ITS_SiteID,sr.UParent_CSN AS UltimateParentCSN,sr.RoleId FROM " +
            //"(SELECT s.*,m.MarketType,m.MarketTypeId,geo_name,region_name,subregion_name,Territory_Name,rc.TerritoryId FROM " +
            //"(SELECT c.*,SiteIdInt,ATCSiteId,OrgId,SiteStatus_retired,SiteName,SiebelSiteName,EnglishSiteName,CommercialSiteName,Workstations,MagellanId," +
            //"SAPNumber_retired,SAPShipTo_retired,SiteTelephone,SiteFax,SiteEmailAddress,SiteWebAddress,SiteDepartment,SiteAddress1,SiteAddress2,SiteAddress3,SiteCity," +
            //"SiteStateProvince,SiteCountryCode,SitePostalCode,MailingDepartment,MailingAddress1,MailingAddress2,MailingAddress3,MailingCity,MailingStateProvince," +
            //"MailingCountryCode,MailingPostalCode,ShippingDepartment,ShippingAddress1,ShippingAddress2,ShippingAddress3,ShippingCity,ShippingStateProvince," +
            //"ShippingCountryCode,ShippingPostalCode,SiebelDepartment,SiebelAddress1,SiebelAddress2,SiebelAddress3,SiebelCity,SiebelStateProvince,SiebelCountryCode," +
            //"SiebelPostalCode,SiteAdminFirstName,SiteAdminLastName,SiteAdminEmailAddress,SiteAdminTelephone,SiteAdminFax,SiteManagerFirstName,SiteManagerLastName," +
            //"SiteManagerEmailAddress,SiteManagerTelephone,SiteManagerFax,AdminNotes,CSOLocationUUID,CSOLocationName,CSOLocationNumber,CSOAuthorizationCodes FROM " +
            //"(SELECT c.*, scl.SiteId,ProductId FROM Contacts_All c " +
            //"INNER JOIN Applications app ON c.ContactId = app.ContactId " +
            //"INNER JOIN ApplicationsProducts ap ON app.ApplicationId = ap.ApplicationId " +
            //"INNER JOIN SiteContactLinks scl ON app.ContactId = scl.ContactId " +
            //"GROUP BY c.ContactId LIMIT 200) AS c " +
            //"INNER JOIN Main_site ss ON c.SiteId = ss.SiteId) AS s " +
            //"INNER JOIN Main_organization o ON s.OrgId = o.OrgId " +
            //"INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " +
            //"INNER JOIN Ref_territory t ON rc.TerritoryId = t.TerritoryId " +
            //"INNER JOIN MarketType m ON rc.MarketTypeId = m.MarketTypeId) AS rc " +
            //"INNER JOIN SiteRoles sr ON rc.SiteId = sr.SiteId " +
            //"INNER JOIN Roles r ON sr.RoleId = r.RoleId) AS r " +
            //"LEFT JOIN Products p ON r.ProductId = p.productId";

            query += selectedColumns + " FROM " +
                " Main_organization o inner join Main_site s on o.OrgId = s.OrgId" +
                " INNER JOIN SiteContactLinks scl  on s.SiteId = scl.SiteId" +
                " INNER JOIN Contacts_All c on scl.ContactId = c.ContactId" +
                " INNER JOIN Qualifications q on c.ContactId = q.ContactId" +
                " INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code" +
                " INNER JOIN Ref_territory t ON rc.TerritoryId = t.TerritoryId" +
                " INNER JOIN MarketType m ON rc.MarketTypeId = m.MarketTypeId" +
                " INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId INNER JOIN Roles r ON sr.RoleId = r.RoleId" +
                " LEFT JOIN Products p on q.AutodeskProduct = p.ProductId";
            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Product"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "p.ProductId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "r.RoleId in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "p.Status in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "m.MarketTypeId in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteCountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteCountryCode in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "TerritoryId in ("+property.Value.ToString()+") "; 

                        /*fix issue excel no 263.Show data based role*/

                        if (role == "")
                        {
                            query += "t.TerritoryId in (" + property.Value.ToString() + ") ";
                        }
                        else if (role == "DISTRIBUTOR")
                        {
                            query += "t.TerritoryId in (select rc.TerritoryId from Ref_countries rc INNER JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + orgid + ") GROUP BY rc.TerritoryId) ";
                        }
                        else
                        {
                            query += "t.TerritoryId in (" + property.Value.ToString() + ") AND o.OrgId in (" + orgid + ") ";
                        }

                        /*end fix issue excel no 263.Show data based role*/

                        i = i + 1;
                    }
                }

            }
            List<dynamic> dynamicList = new List<dynamic>();
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {

                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    // ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                //result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
            }
            return Ok(result);
        }

        /* get by contactidint - 01102018  */
        [HttpGet("ContactIdInt/{id}")]
        public IActionResult ContactIdInt(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select ContactName,ContactIdInt,ContactId,Telephone1,Mobile1,Timezone,Comments,DateLastAdmin,PrimaryIndustryId,InstructorId,Department,LastLogin,UserLevelId from Contacts_All where ContactIdInt = '" + id + "'");

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];

            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }
        /* get by contactidint - 01102018  */

    }
}