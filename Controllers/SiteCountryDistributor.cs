using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.Models;
using autodesk.Code.AuditLogServices;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/SiteCountryDistributor")]
    public class SiteCountryDistributor : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        private readonly MySQLContext _mySQLContext;
        private readonly IAuditLogServices _auditLogServices;
        public SiteCountryDistributor(MySqlDb sqlDb, MySQLContext mySQLContext, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _mySQLContext = mySQLContext;
            _auditLogServices = auditLogServices;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from SiteCountryDistributor ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("bySiteId/{id}")]
        public IActionResult WhereId(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from SiteCountryDistributor where SiteID = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from SiteCountryDistributor ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("SiteID")){   
                    query += "SiteID like '%"+property.Value.ToString()+"%' "; 
                }
                if(property.Name.Equals("CountryCode")){   
                    query += "CountryCode like '%"+property.Value.ToString()+"%' "; 
                }
                if(property.Name.Equals("EDistributorType")){   
                    query += "EDistributorType like '%"+property.Value.ToString()+"%' "; 
                }
                if(property.Name.Equals("PrimaryD")){   
                    query += "PrimaryD like '%"+property.Value.ToString()+"%' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpGet("getDistributor/{obj}")]
        public IActionResult WhereObjDistributor(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select s.Orgid,s.SiteId,s.SiteName,EDistributorType,scd.CountryCode,scd.PrimaryD,(SELECT GROUP_CONCAT(DISTINCT RoleName ORDER BY RoleName SEPARATOR ', ') "+
            "From SiteRoles scl, Roles r WHERE scl.SiteId = s.SiteId AND Status <> 'X' AND scl.RoleId = r.RoleId ) As Roles from Main_site s "+
            "inner join SiteCountryDistributor scd on s.SiteId = scd.SiteId ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(property.Name.Equals("CountryCode")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "scd.CountryCode = '"+property.Value.ToString()+"' "; 
                        i=i+1;
                    }
                }
                if(property.Name.Equals("EDistributorType")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "EDistributorType = '"+property.Value.ToString()+"' "; 
                        i=i+1;
                    }
                }
                
                if(property.Name.Equals("SiteId")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "s.SiteId = '"+property.Value.ToString()+"' "; 
                        i=i+1;
                    }
                }

            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpGet("getAnotherDistributor/{CountryCode}/{DistributorType}")]
        public IActionResult AnotherDistributor(string CountryCode, string DistributorType)
        {     
            string query = ""; 

            query += "SELECT DISTINCT s.SiteId,s.OrgId,s.SiteName,s.SiteCity,s.SiteStateProvince, c.geo_code As Geo, c.region_code As Region, c.countries_name As Country, "+
            "(SELECT GROUP_CONCAT(DISTINCT RoleName ORDER BY RoleName SEPARATOR ', ') From SiteRoles scl, Roles r WHERE scl.SiteId = s.SiteId AND Status <> 'X' AND scl.RoleId = r.RoleId ) As Roles ,"+
            "(SELECT GROUP_CONCAT( DISTINCT Status ORDER BY Status) FROM SiteRoles WHERE Status <> 'X' AND SiteId = s.SiteId) As Statuses "+
            "FROM Main_site s, Ref_countries c, Main_organization o,SiteRoles sr  WHERE s.OrgId = o.OrgId AND s.Status <> 'X'  AND SiteCountryCode = c.countries_code "+
            "AND s.SiteId = sr.SiteId AND sr.RoleId = (select RoleId from Roles where RoleCode = '"+DistributorType+"' )  AND s.Status <> 'D' "+
            "AND SiteCountryCode IN ('"+CountryCode+"') AND s.SiteId not in (select SiteId from SiteCountryDistributor where CountryCode IN ('"+CountryCode+"') and EDistributorType = '"+DistributorType+"')ORDER BY s.SiteName ASC";

            try
            {  
                sb.AppendFormat(query);
                //Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }


        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from SiteCountryDistributor;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into SiteCountryDistributor "+
                    "(SiteID, CountryCode, EDistributorType, PrimaryD) "+
                    "values ('"+data.SiteID+"','"+data.CountryCode+"','"+data.EDistributorType+"','"+data.PrimaryD+"');"
                );
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from SiteCountryDistributor;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
            
        }


        [HttpPut("where/{obj}")]
        public IActionResult Update(string obj, [FromBody] dynamic data)
        {  
            var o1 = JObject.Parse(obj);
            string query = ""; 

            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("SiteID")){   
                    query += "SiteID ='"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("CountryCode")){   
                    query += "CountryCode = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("EDistributorType")){   
                    query += "EDistributorType = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("PrimaryD")){   
                    query += "PrimaryD = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update SiteCountryDistributor set "+
                    "SiteID='"+data.SiteID+"',"+
                    "CountryCode='"+data.CountryCode+"',"+
                    "EDistributorType='"+data.EDistributorType+"',"+
                    "PrimaryD='"+data.PrimaryD+"' "+ 
                    query
                ); 
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            // sb = new StringBuilder();
            // sb.AppendFormat("select * from SiteCountryDistributor " + query);
            // Console.WriteLine(sb.ToString());
            // sqlCommand = new MySqlCommand(sb.ToString());
            // ds = oDb.getDataSetFromSP(sqlCommand);
        
            // result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            // o3 = JObject.Parse(result);  

            // if(o3["SiteID"].ToString() == data.SiteID.ToString())
            // { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            // } 
 
            return Ok(res);  
        }

        [HttpDelete("where/{obj}")]
        public IActionResult Delete(string obj)
        { 
            var o1 = JObject.Parse(obj);
            string query = "";
            string des = "Delete SiteCountryDistributor <br/> ";
            string id = string.Empty;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";   
            
            query += "delete from SiteCountryDistributor ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }
                id = property.Value.ToString();
                if (property.Name.Equals("SiteID")){
                  
                    query += "SiteID ='"+id+"' ";
                    des += "SiteID : "+id;
                }
                if(property.Name.Equals("CountryCode")){  
                  
                    query += "CountryCode = '"+id+"' ";
                    des += "CountryCode : " + id;
                }
                if(property.Name.Equals("EDistributorType")){   
                    query += "EDistributorType = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("PrimaryD")){   
                    query += "PrimaryD = '"+id+"' ";
                    des += "PrimaryD : " + id;
                }

                i=i+1;
            } 

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                var admin = HttpContext.User.GetUserId();
                _auditLogServices.LogHistory(new History { Admin = admin, Date = DateTime.Now, Description = des , Id = id.ToString() });
            }
            catch
            { 
            }
            finally
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Delete Data Success\""+
                    "}"; 
            }

            return Ok(res);
        } 
    }
}
