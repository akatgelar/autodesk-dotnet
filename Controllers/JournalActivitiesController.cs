using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/JournalActivities")]
    public class JournalActivitiesController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        private IDataServices dataService;
        private MySQLContext db;
        public JournalActivitiesController(MySqlDb sqlDb, MySQLContext _db)
        {
            oDb = sqlDb;
            db = _db;
            dataService=new DataServices(oDb,db);

        }
 
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from JournalActivities ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from JournalActivities where ActivityId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select ActivityId,ActivityName from JournalActivities ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("ActivityType")){   
                    query += "ActivityType = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("StatusNot")){   
                    query += "Status <> '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("Status")){   
                    query += "Status = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            }

            query += " and Status <> 'X' order by ActivityName asc";
            List<dynamic> dynamicModel = new List<dynamic>();
      
            try
            {
               
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables.Count > 0)
                {
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }
                
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
                //result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        // [HttpPost]
        // public IActionResult Insert([FromBody] dynamic data)
        // {    
            
        //     int count1 = 0;
        //     int count2 = 0;
        //     JObject o1;
        //     JObject o2;
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Insert Data Failed\""+
        //         "}";  
             
        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from Invoices;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);
         
        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o1 = JObject.Parse(result); 
        //     count1 = Int32.Parse(o1["count"].ToString());
        //     Console.WriteLine(count1); 
            
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "insert into Invoices "+
        //             "(OrgId, InvoiceNumber, ContractID, InvoiceType, ContractStatus, InvoiceDescription, InvoiceDate, POReceivedDate, POID, ContractStartDate, ContractEndDate, OldContractId, InvoicedCurrency, L1_Summary) "+
        //             "values ('"+data.OrgId+"','"+data.InvoiceNumber+"','"+data.ContractID+"','"+data.InvoiceType+"','"+data.ContractStatus+"','"+data.InvoiceDescription+"','"+data.InvoiceDate+"','"+data.POReceivedDate+"','"+data.POID+"','"+data.ContractStartDate+"','"+data.ContractEndDate+"','"+data.OldContractId+"','"+data.InvoicedCurrency+"','"+data.L1_Summary+"');"
        //         );
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //     }
        //     finally
        //     { 
        //     }

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from Invoices;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);   

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o2 = JObject.Parse(result); 
        //     count2 = Int32.Parse(o2["count"].ToString());
        //     Console.WriteLine(count2);
            
        //     if(count2 > count1)
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Insert Data Success\""+
        //             "}";  
        //     } 
 
        //     return Ok(res);  
            
        // }


        // [HttpPut("{id}")]
        // public IActionResult Update(int id, [FromBody] dynamic data)
        // {  
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Update Data Failed\""+
        //         "}";  
                
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update Invoices set "+
        //             "InvoiceNumber='"+data.InvoiceNumber+"',"+
        //             "ContractID='"+data.ContractID+"',"+
        //             "InvoiceType='"+data.InvoiceType+"',"+
        //             "ContractStatus='"+data.ContractStatus+"',"+
        //             "InvoiceDescription='"+data.InvoiceDescription+"',"+
        //             "InvoiceDate='"+data.InvoiceDate+"',"+
        //             "POReceivedDate='"+data.POReceivedDate+"',"+
        //             "POID='"+data.POID+"',"+
        //             "ContractStartDate='"+data.ContractStartDate+"',"+
        //             "ContractEndDate='"+data.ContractEndDate+"',"+
        //             "OldContractId='"+data.OldContractId+"',"+
        //             "InvoicedCurrency='"+data.InvoicedCurrency+"',"+
        //             "L1_Summary='"+data.L1_Summary+"'"+
        //             "where InvoiceId='"+id+"'"
        //         ); 
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //         Console.WriteLine("success");
        //     }
        //     catch
        //     {
        //         Console.WriteLine("failed");  
        //     }
        //     finally
        //     {  
        //     }
 
        //     sb = new StringBuilder();
        //     sb.AppendFormat("select * from Invoices where InvoiceId ="+id+";");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);
        
        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o3 = JObject.Parse(result);  

        //     // if(o3["OrgId"].ToString() == data.OrgId.ToString())
        //     // { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Update Data Success\""+
        //             "}"; 
        //     //} 
 
        //     return Ok(res);  
        // } 



        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // { 
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("delete from Invoices where InvoiceId = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //     }
        //     finally
        //     { 
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from Invoices where InvoiceId  ="+id+";");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
            
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //         o3 = JObject.Parse(result);  
        //         Console.WriteLine(o3.Count);
        //         if(o3.Count < 1){ 
        //             res =
        //                 "{"+
        //                 "\"code\":\"1\","+
        //                 "\"message\":\"Delete Data Success\""+
        //                 "}"; 
        //         }
        //     }

        //     return Ok(res);
        // } 


    }
}
