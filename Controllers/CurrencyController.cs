using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;

namespace autodesk.Controllers
{
	[Route("api/Currency")]
    public class CurrencyController : Controller
    {
    	StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;// = new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit ;

        public CurrencyController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            _audit=new Audit(oDb);
        }

        [HttpGet("SelectByYear/{year}")]
        public IActionResult SelectByYear(string year)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Dictionary where Parent = 'Currencies' and `Status` = 'A' and `Key` not in (select Currency from CurrencyConversion where `Year` = '"+year+"')");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("SelectByYearCurr/{year}/{currency}")]
        public IActionResult SelectByYearCurr(string year, string currency)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Dictionary where Parent = 'Currencies' and `Status` = 'A' and `Key` not in (select Currency from CurrencyConversion where `Year` = '"+year+"' and `Key` != '"+currency+"')");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("{key}")]
        public IActionResult WhereId(string key)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Dictionary where `Key` = '"+key+"' and Parent = 'Currencies' order by `Key` asc");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 

    	[HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Dictionary where Parent = 'Currencies';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Dictionary "+
                    "(`Key`, Parent, KeyValue, Status) "+
                    "values ('"+data.Key+"','"+data.Parent+"','"+data.KeyValue+"','"+data.Status+"');"
                );

                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Dictionary where Parent = 'Currencies';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            {
                //string description = "Insert data Currency" + "<br/>" +
                //    "Parent = "+ data.Parent + "<br/>" +
                //    "Currency = "+ data.Key + "<br/>" +
                //    "Description = "+ data.KeyValue;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                sb = new StringBuilder();
                sb.AppendFormat("select * from Dictionary where Parent = 'Currencies' and `Key`='"+ data.Key+ "' and KeyValue ='"+ data.KeyValue+ "'; ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                JObject OldData = new JObject();
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Dictionary, data.UserId.ToString(), data.cuid.ToString(), " where 'Currencies' and `Key`=" + data.Key + " and KeyValue =" + data.KeyValue + "");

                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from Dictionary ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(property.Name.Equals("Key")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "`Key` like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                } 
                if(property.Name.Equals("Parent")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "Parent like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                }
                if(property.Name.Equals("Status")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "`Status` like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                }
                 
            } 
 
            try
            {
                query += "order by Dictionary.Key";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPost("CurrencyConversion")]
        public IActionResult InsertCC([FromBody] dynamic data)
        {    
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CurrencyConversion;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into CurrencyConversion "+
                    "(Year, Currency, ValuePerUSD, Status) "+
                    "values ('"+data.Year+"','"+data.Currency+"','"+data.ValuePerUSD+"','"+data.Status+"');"
                );

            Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CurrencyConversion;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            {
                //string descriptionCC = "Insert data Currency Conversion" + "<br/>" +
                //    "Year = "+ data.Year + "<br/>" +
                //    "Currency = "+ data.Currency + "<br/>" +
                //    "Value per USD = "+ data.ValuePerUSD;

                //_audit.ActionPost(data.UserId.ToString(), descriptionCC, data.cuid.ToString());
                
                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM CurrencyConversion ORDER BY CurrencyConversionId DESC LIMIT 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject resobj = JObject.Parse(result);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.CurrencyConversion, data.UserId.ToString(), data.cuid.ToString(), " where CurrencyConversionId =" + resobj["CurrencyConversionId"].ToString() + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }
                 
            } 
 
            return Ok(res);  
        }

        [HttpGet("CurrencyConversion")]
        public IActionResult SelectCC()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from CurrencyConversion join Dictionary on CurrencyConversion.`Year` = Dictionary.`Key` where Dictionary.Parent = 'FYIndicator'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("CurrencyConversion/{id}")]
        public IActionResult WhereIdCC(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from CurrencyConversion where CurrencyConversionId = "+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CurrencyConversion/where/{obj}")]
        public IActionResult WhereObjConvert(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from CurrencyConversion ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("Currency")){   
                    query += "Currency = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("Status")){   
                    query += "`Status` = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("Year")){   
                    query += "`Year` = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPut("{key}")]
        public IActionResult Update(string key, [FromBody] dynamic data)
        {  
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Dictionary where `Key` ='" + key + "' and Parent = 'Currencies';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Dictionary set "+
                    "`Key`='"+data.Key+"',"+
                    "KeyValue='"+data.KeyValue+"'"+
                    "where `Key`='"+key+"' and Parent = 'Currencies'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from Dictionary where `Key` ='"+key+"' and Parent = 'Currencies';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            //o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // {        

            /* autodesk plan 10 oct - complete all history log */

            //string description = "Update data Currency" + "<br/>" +
            //    "Currency Code = "+ key +"<br/>"+
            //    "Currency = "+ data.KeyValue;

            //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

            /* end line autodesk plan 10 oct - complete all history log */
            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Dictionary, data.UserId.ToString(), data.cuid.ToString(), " where `Key` =" + key + " and Parent = 'Currencies';"))
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }
           
            //} 
 
            return Ok(res);  
        }

        [HttpPut("CurrencyConversion/{id}")]
        public IActionResult UpdateCC(int id, [FromBody] dynamic data)
        {  
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from CurrencyConversion where CurrencyConversionId ='" + id + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update CurrencyConversion set "+
                    "`Year`='"+data.Year+"',"+
                    "Currency='"+data.Currency+"',"+
                    "ValuePerUSD="+data.ValuePerUSD+","+
                    "`Status`='"+data.Status+"' "+
                    "where CurrencyConversionId = '"+id+"'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from CurrencyConversion where CurrencyConversionId ='"+id+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 
            //string descriptionCC = "Update data Currency Conversion" + "<br/>" +
            //    "Year = "+ data.Year + "<br/>" +
            //    "Currency = "+ data.Currency + "<br/>" +
            //    "Value per USD = "+ data.ValuePerUSD;

            //_audit.ActionPost(data.UserId.ToString(), descriptionCC, data.cuid.ToString());
            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.CurrencyConversion, data.UserId.ToString(), data.cuid.ToString(), " where CurrencyConversionId =" + id + ""))
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }
           
            //} 
 
            return Ok(res);  
        }

        [HttpDelete("CurrencyConversion/{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from CurrencyConversion where CurrencyConversionId  =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from CurrencyConversion where CurrencyConversionId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from CurrencyConversion where CurrencyConversionId  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                JObject NewData = new JObject();
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){ 

                    //string description = "Delete data Currency Conversion" + "<br/>" +
                    //"Currency Conversion ID = "+ id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());

                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.CurrencyConversion, UserId.ToString(), cuid.ToString(), " where CurrencyConversionId =" + id + ""))
                    {
                        res =
                         "{" +
                         "\"code\":\"1\"," +
                         "\"message\":\"Delete Data Success\"" +
                         "}";
                    }
                    
                }
            }

            return Ok(res);
        }

    }
}