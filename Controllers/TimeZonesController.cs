using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;
 

namespace autodesk.Controllers
{
    [Route("api/TimeZones")]
    public class TimeZonesController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public TimeZonesController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT * FROM SabaComponentIds WHERE compType = 'timezone_id' ORDER BY compName");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }
         

        // [HttpGet("{id}")]
        // public IActionResult WhereId(int id)
        // { 
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from TimeZones where TimeZones_id = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // } 
        
        

        // [HttpGet("where/{obj}")]
        // public IActionResult WhereObj(string obj)
        // {     
        //     var o1 = JObject.Parse(obj);
        //     string query = ""; 

        //     query += "select * from TimeZones ";
        //     int i = 0;
        //     JObject json = JObject.FromObject(o1);
        //     foreach (JProperty property in json.Properties())
        //     { 
        //         if(i==0){
        //             query += " where ";
        //         }
        //         if(i>0){
        //             query += " and ";
        //         }

        //         if(property.Name.Equals("TimeZones_category")){   
        //             query += "TimeZones_category = '"+property.Value.ToString()+"' "; 
        //         }

        //         i=i+1;
        //     } 

        //     query += " ORDER BY TimeZones_name ASC "; 
 
        //     try
        //     {  
        //         sb.AppendFormat(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
        //     }
        //     return Ok(result);  
        // }
        
        

        // [HttpPost]
        // public IActionResult Insert([FromBody] dynamic data)
        // {    
            
        //     int count1 = 0;
        //     int count2 = 0;
        //     JObject o1;
        //     JObject o2;
        //     // JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Insert Data Failed\""+
        //         "}";  
             
        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from TimeZones;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);
         
        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o1 = JObject.Parse(result); 
        //     count1 = Int32.Parse(o1["count"].ToString());
        //     Console.WriteLine(count1); 
            
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "insert into TimeZones "+
        //             "( TimeZones_category, TimeZones_name, description, `unique`, cuid, cdate, muid, mdate, status) "+
        //             "values ( '"+data.TimeZones_category+"','"+data.TimeZones_name+"','"+data.description+"','"+data.unique+"','"+data.cuid+"','"+data.cdate+"','"+data.muid+"','"+data.mdate+"','"+data.status+"');"
        //         );

        //     Console.WriteLine(sb.ToString());
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Insert Data Failed\""+
        //             "}";  
        //     }
        //     finally
        //     { 
        //     }

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from TimeZones;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);   

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o2 = JObject.Parse(result); 
        //     count2 = Int32.Parse(o2["count"].ToString());
        //     Console.WriteLine(count2);
            
        //     if(count2 > count1)
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Insert Data Success\""+
        //             "}";  
        //     } 
 
        //     return Ok(res);  
            
        // }

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, [FromBody] dynamic data)
        // {  
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Update Data Failed\""+
        //         "}";  
                
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update TimeZones set "+ 
        //             "TimeZones_category='"+data.TimeZones_category+"', "+
        //             "TimeZones_name='"+data.TimeZones_name+"', "+
        //             "description='"+data.description+"', "+
        //             "`unique`='"+data.unique+"', "+
        //             "cuid='"+data.cuid+"', "+
        //             "cdate='"+data.cdate+"', "+
        //             "muid='"+data.muid+"', "+
        //             "mdate='"+data.mdate+"', "+ 
        //             "status='"+data.status+"' "+
        //             "where TimeZones_id='"+id+"'"
        //         ); 
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Update Data Failed\""+
        //             "}";  
        //     }
        //     finally
        //     {  
        //     }
 
        //     sb = new StringBuilder();
        //     sb.AppendFormat("select * from TimeZones where TimeZones_id ="+id+";");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);
        
        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o3 = JObject.Parse(result);  

        //     if(o3["TimeZones_name"].ToString() == data.TimeZones_name.ToString())
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Update Data Success\""+
        //             "}"; 
        //     } 
 
        //     return Ok(res);  
        // } 

        // [HttpPut("status/{id}")]
        // public IActionResult UpdateSatus(int id)
        // {  
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
                
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update TimeZones set "+ 
        //             "status='Deleted' "+
        //             "where TimeZones_id='"+id+"'"
        //         ); 
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Delete Data Failed\""+
        //             "}";  
        //     }
        //     finally
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Delete Data Success\""+
        //             "}";  
        //     }

        //     return Ok(res);  
        // } 

        
        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // { 
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("delete from TimeZones where TimeZones_id = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Delete Data Failed\""+
        //             "}"; 
        //     }
        //     finally
        //     { 
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from TimeZones where TimeZones_id  ="+id+";");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
            
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //         o3 = JObject.Parse(result);  
        //         Console.WriteLine(o3.Count);
        //         if(o3.Count < 1){ 
        //             res =
        //                 "{"+
        //                 "\"code\":\"1\","+
        //                 "\"message\":\"Delete Data Success\""+
        //                 "}"; 
        //         }
        //     }

        //     return Ok(res);
        // }

        // [HttpGet("CekUnik/{obj}")]
        // public IActionResult WhereObjUnik(string obj)
        // {     
        //     var o1 = JObject.Parse(obj);
        //     string query = ""; 
        //     Console.WriteLine("Tes");
        //     query += "select TimeZones_name, count(*) as count from TimeZones ";
        //     int i = 0;
        //     JObject json = JObject.FromObject(o1);
        //     foreach (JProperty property in json.Properties())
        //     { 
        //         if(i==0){
        //             query += " where ";
        //         }
        //         if(i>0){
        //             query += " and ";
        //         }

        //         if(property.Name.Equals("TimeZonesName")){   
        //             query += "TimeZones_name = '"+property.Value.ToString()+"' "; 
        //         }

        //         i=i+1;
        //     } 
        //     query += "group by TimeZones_name having count(*) > 0";
        //     Console.WriteLine(query);

        //     try
        //     {  
        //         sb.AppendFormat(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not Found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);    
        //     }
        //     return Ok(result);  
        // } 

    }
}