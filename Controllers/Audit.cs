using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using System.Reflection;
using Newtonsoft.Json;

namespace autodesk.Controllers
{   
    public class Audit
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        public Audit(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }

        public bool AuditLog(JObject OldData, JObject NewData,string tableName, string UserId, string Admin,string condition)
        {
            string[] description = { "0", "1" };
           // string newdescription = "";
            try
            {

                string sourceJsonString = OldData.ToString();
                string targetJsonString = NewData.ToString();
                JObject sourceJObject = JsonConvert.DeserializeObject<JObject>(sourceJsonString);
                JObject targetJObject = JsonConvert.DeserializeObject<JObject>(targetJsonString);
                if (targetJsonString != "{}")
                {
                    if (!JToken.DeepEquals(sourceJObject, targetJObject))
                    {
                        int i = 0;
                        foreach (KeyValuePair<string, JToken> sourceProperty in sourceJObject)
                        {
                            JProperty targetProp = targetJObject.Property(sourceProperty.Key);

                            if (!JToken.DeepEquals(sourceProperty.Value, targetProp.Value))
                            {
                                if (i == 0)
                                { description[0] = "update data " + tableName + " "; description[1] = "update data " + tableName + " "; i = 1; }
                                Console.WriteLine(string.Format("{0} property value is changed", sourceProperty.Key));
                                description[0] += sourceProperty.Key + "= " + sourceProperty.Value + ",";
                                description[1] += sourceProperty.Key + "= " + targetProp.Value + ",";
                            }
                            else
                            {
                               // Console.WriteLine(string.Format("{0} property value didn't change", sourceProperty.Key));
                            }
                        }
                    }
                }

                    if (sourceJsonString.ToString() == "{}")
                    {
                        int i = 0;
                        description[0] = "insert Data to " + tableName;
                        foreach (KeyValuePair<string, JToken> sourceProperty in targetJObject)
                        {
                            if (i == 0)
                            {  description[1] = "Insert data " + tableName + " "; i = 1; }
                            description[1] += sourceProperty.Key + "= " + sourceProperty.Value + ",";
                        }
                    }
                    else if (targetJsonString.ToString() == "{}")
                    {
                        int i = 0;
                        description[0] = "Delete Data from " + tableName;
                        foreach (KeyValuePair<string, JToken> sourceProperty in sourceJObject)
                        {
                            if (i == 0)
                            { description[0] = "Delete Data " + tableName + " ";  i = 1; }
                            description[0] += sourceProperty.Key + "= " + sourceProperty.Value + ",";
                        }
                    }
                ActionPostNew(UserId, description[0] + condition , description[1] + condition, Admin);
                
                
               
            }
            catch
            {
                return false;
            }
            return true;
        }
        public bool ActionPost(string UserId, string Desc, string Admin)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into History "+
                    "(Date, Id, Description, Admin ) "+
                    "values (NOW(), '"+UserId+"', '"+Desc+"', '"+Admin+"')"
                );
                Console.WriteLine("history log");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool ActionPostNew(string UserId, string Desc,string newDesc, string Admin)
        {
            try
            {
                
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into History " +
                    "(Date, Id, Description,NewDescription, Admin ) " +
                    "values (NOW(), '" + UserId + "', '" + Desc + "','" + newDesc + "',  '" + Admin + "')"
                );
                Console.WriteLine("history log");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                return false;
            }
            return true;
        }



    }
    
}