using autodesk.Code;
using autodesk.Code.AuditLogServices;
using autodesk.Code.CommonServices;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
//using autodesk.Code.TokenGenerator;

namespace autodesk.Controllers
{
    [Route("api/Auth")]
    public class UserController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;// = new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        private IDataServices dataServices;
        private static bool isSucceed = false;
        private MySQLContext db;
        String result;
        String result1;
        JArray o2;
        string mainURL = AppConfig.Config["Auth:mainURL"];
        string jwtKey = AppConfig.Config["Jwt:Key"];

        private readonly IEmailService _emailSender;
        private Audit _audit;
        private readonly IAuditLogServices _auditLogServices;
        public UserController(IEmailService emailSender, MySqlDb sqlDb, MySQLContext _db, IAuditLogServices auditLogServices)
        {
            _emailSender = emailSender;
            oDb = sqlDb;
            db = _db;
            dataServices = new DataServices(oDb, db);
            _audit = new Audit(oDb);
            _auditLogServices = auditLogServices;

        }



        [HttpPost("CreatePassword")]
        public IActionResult CreatePassword([FromBody] dynamic data)
        {
            string res;

            string password = data.Password;
            string strdate = data.Date;
            DateTime myDate = DateTime.ParseExact(strdate, "yyyy-MM-dd HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);

            string salt = myDate.ToString("yyyyMMddHHMMss", CultureInfo.InvariantCulture);
            string saltedPassword = string.Format("{0}|{1}", salt, password.Trim());

            Byte[] byteInput = Encoding.UTF8.GetBytes(saltedPassword);
            HashAlgorithm hash = new SHA512Managed();
            res = Convert.ToBase64String(hash.ComputeHash(byteInput));

            return Ok(res);

        }

        [HttpPut("GeneratePass")]
        // public IActionResult GeneratePass([FromBody] dynamic data)
        public async Task<IActionResult> GeneratePass([FromBody] dynamic data)
        {
            JObject contactpassword;
            JObject contactpasswordnew;
            Boolean success = false;
            string randompassword = "";

            JObject NewData = new JObject();
            string test = "";
            sb = new StringBuilder();
            sb.AppendFormat("select password,contactId,contactName from Contacts_All  where ContactIdInt = '" + data.ContactId + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            string res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Generate Password Failed\"" +
                    "}";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select ca.`password`, ca.EmailAddress, ca.EmailAddress2, ca.ContactName, eb.`subject`, eb.body_email from Contacts_All ca LEFT JOIN EmailBody eb ON CASE WHEN ca.PrimaryLanguage NOT IN ('35') THEN '35' ELSE ca.PrimaryLanguage END = eb.`Key` AND eb.body_type = '6' where ContactIdInt = '" + data.ContactId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                contactpassword = JObject.Parse(result);
                OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                randompassword = CreatePassword(8);
                Boolean ondatabases = true;

                while (ondatabases)
                {
                    int count = 0;
                    string resulttmp = "";

                    sb = new StringBuilder();
                    sb.AppendFormat("select count(*) as count from Contacts_All where `password` = md5('" + randompassword + "')");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    resulttmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    count = Int32.Parse(JObject.Parse(resulttmp)["count"].ToString());

                    if (count > 0)
                    {
                        randompassword = CreatePassword(8);
                    }
                    else
                    {
                        ondatabases = false;
                    }
                }

                string emailinst = contactpassword["EmailAddress"].ToString();
                if (contactpassword["EmailAddress2"].ToString() != "" && contactpassword["EmailAddress2"] != null)
                {
                    emailinst += "," + contactpassword["EmailAddress2"].ToString();
                }

                string mailto = contactpassword["EmailAddress"].ToString();
                string subject = contactpassword["subject"].ToString();
                string message = "";

                message += contactpassword["body_email"].ToString();

                message = message.Replace("[Name]", contactpassword["ContactName"].ToString());
                message = message.Replace("[Password]", randompassword);

                test = mailto + " | " + subject + " | " + message;

                Boolean resmail = await _emailSender.SendMailAsync(mailto, subject, message);

                if (resmail)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "update Contacts_All set " +
                        "`password` = md5('" + randompassword + "') " +
                        "where ContactIdInt = '" + data.ContactId + "'"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }

                sb = new StringBuilder();
                sb.AppendFormat("select `password` from Contacts_All where ContactIdInt = '" + data.ContactId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result1 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                NewData = JsonConvert.DeserializeObject<JObject>(result1.ToString());
                contactpasswordnew = JObject.Parse(result1);

                if (contactpassword["password"].ToString() != contactpasswordnew["password"].ToString())
                {
                    success = true;
                }
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Generate Password Failed\"" +
                    "}";
            }
            finally
            {
                if (success)
                {
                    //string description = "Reset Password Contact <br/>" +
                    //    "ContactId = " + data.ContactId;

                    //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                    //if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, data.Userid.ToString(), data.cuid.ToString(), " where ContactIdInt = " + data.ContactId + ""))
                    //{
                    res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Password has been reset.<br/>Email success\"" +
                    "}";
                    //}

                    _auditLogServices.LogHistory(new History
                    {
                        Admin = HttpContext.User.GetUserId(),
                        Date = DateTime.Now,
                        Description = "{" + "\"code\":\"1\"," + "\"message\":\"Password has been reset.<br/>Email success\"" + "}",
                        Id = data.ContactId

                    });
                }
            }

            return Ok(res);
        }

        [HttpPut("GeneratePassPopUp")]
        public IActionResult GeneratePassPopUp([FromBody] dynamic data)
        {
            JObject contactpassword;
            JObject contactpasswordnew;
            Boolean success = false;
            string randompassword = "";

            string test = "";

            sb = new StringBuilder();

            JObject NewData = new JObject();
            sb = new StringBuilder();
            sb.AppendFormat("select ca.`password`,ca.ContactId,ca.ContactName from Contacts_All ca where ContactIdInt = '" + data.ContactId + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());


            string res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Generate Password Failed\"" +
                    "}";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select ca.`password` from Contacts_All ca where ContactIdInt = '" + data.ContactId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                contactpassword = JObject.Parse(result);



                randompassword = CreatePassword(8);
                Boolean ondatabases = true;

                while (ondatabases)
                {
                    int count = 0;
                    string resulttmp = "";

                    sb = new StringBuilder();
                    sb.AppendFormat("select count(*) as count from Contacts_All where `password` = md5('" + randompassword + "')");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    resulttmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    count = Int32.Parse(JObject.Parse(resulttmp)["count"].ToString());

                    if (count > 0)
                    {
                        randompassword = CreatePassword(8);
                    }
                    else
                    {
                        ondatabases = false;
                    }
                }

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Contacts_All set " +
                    "`password` = md5('" + randompassword + "') " +
                    "where ContactIdInt = '" + data.ContactId + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("select ca.`password`,ca.ContactId,ca.ContactName from Contacts_All ca  where ContactIdInt = '" + data.ContactId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result1 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                contactpasswordnew = JObject.Parse(result1.ToString());
                NewData = JsonConvert.DeserializeObject<JObject>(result1.ToString());
                if (contactpassword["password"].ToString() != contactpasswordnew["password"].ToString())
                {
                    success = true;
                }
            }
            catch (Exception ex)
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Generate Password Failed\"" +
                    "}";
            }
            finally
            {
                if (success)
                {
                    //string description = "Reset Password Contact <br/>" +
                    //    "ContactId = " + data.ContactId;
                    //_audit.ActionPost(string.Empty, description, string.Empty);
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, data.Userid.ToString(), data.cuid.ToString(), " where ContactIdInt = " + data.ContactId + "; Notes: Temporary Password for user"))
                    {
                        res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Temporary Password<br/>" + randompassword + "\"" +
                        "}";
                    }
                }
            }

            return Ok(res);
        }

        [HttpPut("GeneratePassStudent")]
        // public IActionResult GeneratePassStudent([FromBody] dynamic data)
        public async Task<IActionResult> GeneratePassStudent([FromBody] dynamic data)
        {
            JObject studentpassword;
            JObject studentpasswordnew;
            JObject OldData = new JObject();
            JObject NewData = new JObject();
            Boolean success = false;
            string randompassword = "";

            string test = "";

            string res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Generate Password Failed\"" +
                    "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from tblStudents where StudentID = '" + data.StudentID + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result1 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            NewData = JsonConvert.DeserializeObject<JObject>(result1.ToString());
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select s.`password`, s.Email, s.Email2, Concat(s.Firstname, ' ', s.Lastname) as StudentName, eb.`subject`, eb.body_email from tblStudents s LEFT JOIN EmailBody eb ON CASE WHEN s.LanguageID NOT IN ('35') THEN '35' ELSE s.LanguageID END = eb.`Key` AND eb.body_type = '6' where StudentID = '" + data.StudentID + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                studentpassword = JObject.Parse(result);
                OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                randompassword = CreatePassword(8);
                Boolean ondatabases = true;

                while (ondatabases)
                {
                    int count = 0;
                    string resulttmp = "";

                    sb = new StringBuilder();
                    sb.AppendFormat("select count(*) as count from tblStudents where `password` = md5('" + randompassword + "')");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    resulttmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    count = Int32.Parse(JObject.Parse(resulttmp)["count"].ToString());

                    if (count > 0)
                    {
                        randompassword = CreatePassword(8);
                    }
                    else
                    {
                        ondatabases = false;
                    }
                }

                string emailinst = studentpassword["Email"].ToString();
                if (studentpassword["Email2"].ToString() != "" && studentpassword["Email2"] != null)
                {
                    emailinst += "," + studentpassword["Email2"].ToString();
                }

                string mailto = studentpassword["Email"].ToString();
                string subject = studentpassword["subject"].ToString();
                string message = "";

                message += studentpassword["body_email"].ToString();

                message = message.Replace("[Name]", studentpassword["StudentName"].ToString());
                message = message.Replace("[Password]", randompassword);

                test = mailto + " | " + subject + " | " + message;

                Boolean resmail = await _emailSender.SendMailAsync(mailto, subject, message);

                if (resmail)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "update tblStudents set " +
                        "`password` = md5('" + randompassword + "') " +
                        "where StudentID = '" + data.StudentID + "'"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }

                sb = new StringBuilder();
                sb.AppendFormat("select `password` from tblStudents where StudentID = '" + data.StudentID + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result1 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                studentpasswordnew = JObject.Parse(result1);

                if (studentpassword["password"].ToString() != studentpasswordnew["password"].ToString())
                {
                    success = true;
                }
            }
            catch (Exception e)
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Generate Password Failed\"" +
                    "}";
            }
            finally
            {
                if (success)
                {

                    //string description = "Reset Password Student <br/>" +
                    //    "Student Id = " + data.StudentID;

                    //_audit.ActionPost(data.StudentID.ToString(), description, HttpContext.User.GetUserId());
                    _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.tblStudents, data.StudentID.ToString(), HttpContext.User.GetUserId(), " where StudentID = " + data.StudentID.ToString() + "; Notes: Reset Password for Student");

                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Your password has been reset.<br/>Please check your email\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        private class OrgSiteList
        {
            public string OrgId { get; set; }
            public string SiteId { get; set; }
        }
        [HttpPost("Login")]
        public IActionResult Login([FromBody] dynamic data)
        {
            CommonServices cms = new CommonServices();
            bool isActive = false;
            bool isWrongEmailOrPw = false;
            JArray countryArray = new JArray();
            JObject temp = new JObject();
            var res = "";
            var source = "admin";
            string md5Password = string.Empty;
            var version = dataServices.GetVersion();
            _auditLogServices.LogHistory(new History
            {
                Admin = "System",
                Date = DateTime.Now,
                Description = "Login function.Get Get Version result:<br/>" + version.Result,
                Id = "-1"
            });
            string userlvl = "";
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = MySqlDb.GetMd5Hash(md5Hash, source);
            }
            using (MD5 pass = MD5.Create())
            {
                string cPassword = data.Password;
                md5Password = MySqlDb.GetMd5Hash(pass, cPassword);
            }
            //PWDChangeRequired
            try
            {
                sb = new StringBuilder();
                string query1 = "SELECT " +
                "c.ContactId AS UserId, " +
                "c.FirstName, " +
                "c.LastName, " +
                "c.ContactName, " +
                "c.EmailAddress AS Email, " +
                "'default.png' AS ProfilePicture, " +
                "c.PrimarySiteId, " +
                "c.PrimaryRoleId, " +
                "c.PrimaryIndustryId, " +
                "c.CountryCode, " +
                //"Ref_countries.countries_id as CountryId, " +
                "c.StateProvince, " +
                "c.PostalCode, " +
                "c.LoginCount, " +
                "c.InstructorId, " +
                "c.UserLevelId " +
                //"Ref_countries.TerritoryId, " +
                "from Contacts_All c " +
                // "INNER JOIN Ref_countries ON c.CountryCode = Ref_countries.countries_code " +              
                "where EmailAddress = '" + data.Email + "' and c.`Status` = 'A'";

                if (data.Password != "B@l$u@e!c%u#b&e2121")
                {
                    query1 += " and password = md5('" + data.Password + "')";
                }

                string email = data.Email;
                string password = data.Password;

                //var loginUser = this.db.ContactsAll.Where(x => x.EmailAddress.Equals(email) && x.Password.Equals(md5Password)).Select(
                // x => new
                // {
                //  UserId = x.ContactId,
                //  FirstName = x.FirstName,
                //  LastName = x.LastName,
                //  ContactName = x.ContactName,
                //  Email = x.EmailAddress,
                //  ProfilePicture = x.ProfilePicture,
                //  PrimarySiteId = x.PrimarySiteId,
                //  PrimaryRoleId = x.PrimaryRoleId,
                //  PrimaryIndustryId = x.PrimaryIndustryId,
                //  CountryCode = x.CountryCode,
                //  StateProvince = x.StateProvince,
                //  PostalCode = x.PostalCode,
                //  LoginCount = x.LoginCount,
                //  InstructorId = x.InstructorId,
                //  UserLevelId = x.UserLevelId
                // }).SingleOrDefault();

                sb.Append(query1);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);


                if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                {
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                    o2 = JArray.Parse(result);



                    _auditLogServices.LogHistory(new History
                    {
                        Admin = "System",
                        Date = DateTime.Now,
                        Description = "Login function.Get Contact result:<br/>" + result,
                        Id = "-1"
                    });
                    isActive = true;

                    string CountryCode = o2[0]["CountryCode"] == null ? string.Empty : o2[0]["CountryCode"].ToString();

                    if (!String.IsNullOrEmpty(CountryCode))
                    {
                        sb = new StringBuilder();
                        string countryQuery = "SELECT countries_id as CountryId,TerritoryId FROM Ref_countries " +
                                              "WHERE countries_code='" + CountryCode + "'";
                        sb.Append(countryQuery);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        if (ds.Tables.Count > 0)
                        {

                            result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                            _auditLogServices.LogHistory(new History
                            {
                                Admin = "System",
                                Date = DateTime.Now,
                                Description = "Login function.Get country result:<br/>" + result,
                                Id = "-1"
                            });
                            countryArray = JArray.Parse(result);
                            var countryId = countryArray[0]["CountryId"] == null ? "" : countryArray[0]["CountryId"].ToString();
                            var territoryId = countryArray[0]["TerritoryId"] == null
                             ? ""
                             : countryArray[0]["TerritoryId"].ToString();
                            o2[0]["CountryId"] = string.Join(",", countryId);
                            o2[0]["TerritoryId"] = string.Join(",", territoryId);
                        }

                    }

                    sb = new StringBuilder();

                    string query2 =
                     "select OrgId, GROUP_CONCAT(s.SiteId) AS SiteId from Contacts_All c " +
                     "LEFT JOIN SiteContactLinks sc ON c.ContactId = sc.ContactId " +
                     "LEFT JOIN Main_site s ON sc.SiteId = s.SiteId  " +
                     "where EmailAddress = '" + data.Email + "' and sc.`Status` = 'A' and c.`Status` = 'A'";

                    if (data.Password != "B@l$u@e!c%u#b&e2121")
                    {
                        query2 += " and password = md5('" + data.Password + "')";
                    }

                    query2 += " GROUP BY OrgId";

                    if (data.Email == AppConfig.Config["EmailSender:SupervisorEmail"])
                    {
                        query2 = "select null as OrgId,null as SiteId from Contacts_All c " +
                 "where EmailAddress = '" + data.Email + "' and c.`Status` = 'A'";
                        if (data.Password != "B@l$u@e!c%u#b&e2121")
                        {
                            query2 += " and password = md5('" + data.Password + "')";
                        }
                    }



                    sb.Append(query2);
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);


                    if (ds != null && ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
                    {
                        result = MySqlDb.GetJSONArrayString(ds.Tables[0]);

                        _auditLogServices.LogHistory(new History
                        {
                            Admin = "System",
                            Date = DateTime.Now,
                            Description = "Login function.Get org and site result:<br/>" + result,
                            Id = "-1"
                        });
                        var o3 = JArray.Parse(result);


                        string[] orgarr = new string[o3.Count];
                        string[] sitearr = new string[o3.Count];
                        if (o3.Count > 0)
                        {
                            for (int k = 0; k < o3.Count; k++)
                            {
                                orgarr[k] = o3[k]["OrgId"].ToString();
                                sitearr[k] = o3[k]["SiteId"].ToString();
                            }

                            o2[0]["OrgId"] = string.Join(",", orgarr);
                            o2[0]["SiteId"] = string.Join(",", sitearr);

                            isActive = true;
                        }
                        else
                        {
                            o2[0]["OrgId"] = "";
                            o2[0]["SiteId"] = "";
                        }
                        if (!CheckActiveUser(data.Email.ToString()))
                        {
                            isActive = false;
                        }
                    }
                    else
                    {
                        isActive = false;

                    }

                }
                else
                {
                    isWrongEmailOrPw = true;

                }
            }
            catch (Exception ex)
            {
                _auditLogServices.LogHistory(new History
                {
                    Admin = "System",
                    Date = DateTime.Now,
                    Description = "Login function.Fail to login:<br/>" + ex.Message,
                    Id = "-1"
                });
                res = "{\"code\":\"0\",\"msg\":\"Email or Password is incorrect\"}";
            }
            finally
            {
                if (isWrongEmailOrPw)
                {
                    res = "{\"code\":\"0\",\"msg\":\"Email or Password is incorrect\"}";
                }
                else if (isActive)
                {
                    if (o2.LongCount() > 0)
                    {
                        if (o2[0]["LoginCount"].ToString() != "0" && o2[0]["LoginCount"].ToString() != "")
                        {

                            var o3 = JObject.Parse(o2[0].ToString());
                            userlvl = o3["UserLevelId"].ToString();
                            string arruserlevel1 = o3["UserLevelId"].ToString();
                            //decimal Version = Convert.ToDecimal(o3["Version"].ToString());
                            string[] arruserlevel2 = arruserlevel1.Split(",");
                            List<string> arruserlevel3 = new List<string> { };

                            try
                            {
                                for (int z = 0; z < arruserlevel2.Count(); z++)
                                {
                                    sb = new StringBuilder();
                                    string query2 = "select * from UserLevel where UserLevelId = '" + arruserlevel2[z] + "'";
                                    sb.Append(query2);
                                    sqlCommand = new MySqlCommand(sb.ToString());
                                    ds = oDb.getDataSetFromSP(sqlCommand);
                                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                                    arruserlevel3.Add(result);
                                }

                                var tempres9 = JObject.Parse(arruserlevel3[0]);

                                foreach (var kv in JObject.Parse(arruserlevel3[0]))
                                {
                                    if ((kv.Key == "ID") || (kv.Key == "UserLevelId") || (kv.Key == "Name"))
                                    {

                                    }
                                    else
                                    {
                                        var val = "00000";
                                        if (arruserlevel2.Count() == 2)
                                        {
                                            if ((JObject.Parse(arruserlevel3[0])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[1])[kv.Key].ToString() == "11111"))
                                            {
                                                val = "11111";
                                                tempres9[kv.Key] = val;
                                            }
                                        }
                                        else if (arruserlevel2.Count() == 3)
                                        {
                                            if ((JObject.Parse(arruserlevel3[0])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[1])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[2])[kv.Key].ToString() == "11111"))
                                            {
                                                val = "11111";
                                                tempres9[kv.Key] = val;
                                            }
                                        }
                                        else if (arruserlevel2.Count() == 4)
                                        {
                                            if ((JObject.Parse(arruserlevel3[0])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[1])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[2])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[3])[kv.Key].ToString() == "11111"))
                                            {
                                                val = "11111";
                                                tempres9[kv.Key] = val;
                                            }
                                        }
                                        else if (arruserlevel2.Count() == 5)
                                        {
                                            if ((JObject.Parse(arruserlevel3[0])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[1])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[2])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[3])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[4])[kv.Key].ToString() == "11111"))
                                            {
                                                val = "11111";
                                                tempres9[kv.Key] = val;
                                            }
                                        }
                                        else if (arruserlevel2.Count() == 6)
                                        {
                                            if ((JObject.Parse(arruserlevel3[0])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[1])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[2])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[3])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[4])[kv.Key].ToString() == "11111") || (JObject.Parse(arruserlevel3[5])[kv.Key].ToString() == "11111"))
                                            {
                                                val = "11111";
                                                tempres9[kv.Key] = val;
                                            }
                                        }

                                    }
                                }

                                temp = tempres9;
                                _auditLogServices.LogHistory(new History
                                {
                                    Admin = "System",
                                    Date = DateTime.Now,
                                    Description = "Login function.User is active ,get access result :<br/>" + temp,
                                    Id = "-1"
                                });
                            }
                            catch
                            {
                                res = "{\"code\":\"0\",\"msg\":\"Login failed\"}";
                            }
                            finally
                            {
                                DateTime tokenDateTime = DateTime.Now;
                                //TokenGenerator tokenGenerator = new TokenGenerator();
                                //int UserId = Convert.ToInt32(o3["UserId"]);
                                //var tk=tokenGenerator.GetEncryptedToken(arruserlevel1, Version, UserId);
                                //  var detk = tokenGenerator.GetDecryptedTokenValue(tk);
                                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                                var o4 = JObject.Parse(result);
                                o3["Access"] = temp;
                                o3["DateCreated"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                                string token = "";
                                string user = o3.ToString();
                                var date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                                var secretKey = new byte[] { 164, 60, 194, 0, 161, 189, 41, 38, 130, 89, 141, 164, 45, 170, 159, 209, 69, 137, 243, 216, 191, 131, 47, 250, 32, 107, 231, 117, 37, 158, 225, 234 };
                                // token = Jose.JWT.Encode(o3.ToString(), secretKey, Jose.JwsAlgorithm.HS256);

                                // res = "{\"jwt\":\"" + token + "\",\"user\":" + user + ",\"code\":\"1\",\"msg\":\"Login successful\"}";
                                var userId = o3["UserId"].ToObject<string>();
                                var newToken = cms.CreateToken(new ContactsAll
                                {
                                    ContactId = userId,
                                    EmailAddress = data.Email,
                                    UserLevelId = userlvl
                                },
                                jwtKey,
                                version.Result.Version.ToString());

                                _auditLogServices.LogHistory(new History
                                {
                                    Admin = "System",
                                    Date = DateTime.Now,
                                    Description = "Login function.Create token result :<br/> ContactId:" + userId + "<br/>JwtKey:" + jwtKey + "<br/>Version:" + version.Result,
                                    Id = "-1"
                                });

                                res = "{\"jwt\":\"" + newToken + "\",\"user\":" + user + ",\"code\":\"1\",\"msg\":\"Login successful\"}";
                                try
                                {
                                    /*
                                    sb = new StringBuilder();
                                    sb.AppendFormat(
                                        "delete from UserToken where UserId ='" + o3["UserId"] + "';"
                                    );
                                    sqlCommand = new MySqlCommand(sb.ToString());
                                    ds = oDb.getDataSetFromSP(sqlCommand);


                                    sb = new StringBuilder();
                                    sb.AppendFormat(
                                        "insert into UserToken " +
                                        "( Token, UserId, Datetime) " +
                                        "values ( '" + token + "','" + o3["UserId"] + "','" + date + "');"
                                    );
                                    sqlCommand = new MySqlCommand(sb.ToString());
                                    ds = oDb.getDataSetFromSP(sqlCommand);
                                    */
                                }
                                catch
                                {
                                }
                            }

                        }
                        else
                        {
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update Contacts_All set LoginCount = 0 " +
                                "where ContactId = '" + o2[0]["UserId"].ToString() + "'"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);
                            res = "{\"code\":\"firstlogin\",\"id\":\"" + o2[0]["UserId"].ToString() + "\"}";
                        }
                    }
                    else
                    {
                        _auditLogServices.LogHistory(new History
                        {
                            Admin = "System",
                            Date = DateTime.Now,
                            Description = "Login function.Email or Password is incorrect:<br/>" + JsonConvert.SerializeObject(o2),
                            Id = "-1"
                        });

                        res = "{\"code\":\"0\",\"msg\":\"Email or Password is incorrect\"}";
                    }
                }
                else
                {

                    _auditLogServices.LogHistory(new History
                    {
                        Admin = "System",
                        Date = DateTime.Now,
                        Description = "Login function.User Account is Deactivated or not found",
                        Id = "-1"
                    });
                    res = "{\"code\":\"I\",\"msg\":\"User Account is Deactivated, Please Contact Admin.\"}";
                }

            }

            return Ok(res);

        }


        public bool CheckActiveUser(string emailAddress)
        {
            var user = db.ContactsAll.FirstOrDefault(x => x.EmailAddress == emailAddress);
            if (user != null)
            {
                if (user.UserLevelId != AutodeskConst.UserLevelIdEnum.SUPERADMIN)
                {
                    var query = (
                        from scl in db.SiteContactLinks
                        join sr in db.SiteRoles on scl.SiteId equals sr.SiteId
                        join c in db.ContactsAll on scl.ContactId equals c.ContactId
                        where c.EmailAddress == emailAddress
                        select new
                        {
                            sr.Status
                        }
                        ).ToList();
                    if (query?.Count > 0 && query.Any(x => x.Status == AutodeskConst.StatusConts.A))
                    {
                        return true;
                    }
                }
                else
                {
                    return true;
                }
            }

            return false;

        }

        [HttpPost("LoginStudent")]
        public IActionResult LoginStudent([FromBody] dynamic data)
        {
            CommonServices cms = new CommonServices();
            var res = "";
            var source = "admin";
            int loginCount = 0;
            var version = dataServices.GetVersion();
            string userlvl = "";
            using (MD5 md5Hash = MD5.Create())
            {
                string hash = MySqlDb.GetMd5Hash(md5Hash, source);
            }

            try
            {
                sb = new StringBuilder();
                string query1 = "select StudentID as UserId, CONCAT(Firstname,' ',Lastname) as StudentName, 'STUDENT' as UserLevelId, Email, 'default.png' as ProfilePicture, Status, '" + data.language + "' as LanguageID,LoginCount from tblStudents where Email = '" + data.Email + "'  and Status = 'A'";
                if (data.Password != "B@l$u@e!c%u#b&e2121")
                {
                    query1 += " and password = md5('" + data.Password + "')";
                }

                sb.Append(query1);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Email or Password is incorrect\"}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                var o2 = JArray.Parse(result);

                if (o2.LongCount() > 0)
                {
                    var o3 = JObject.Parse(o2[0].ToString());
                    userlvl = o3["UserLevelId"].ToString();
                    loginCount = o3["LoginCount"].ToString() == "" ? loginCount + 1 : Convert.ToInt32(o3["LoginCount"].ToString());
                    try
                    {
                        sb = new StringBuilder();
                        string query2 = "select * from UserLevel where UserLevelId = '" + o3["UserLevelId"] + "'";
                        sb.Append(query2);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }
                    catch
                    {
                        res = "{\"code\":\"0\",\"msg\":\"Login failed\"}";
                    }
                    finally
                    {
                        result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                        var o4 = JObject.Parse(result);

                        o3["Access"] = o4;
                        o3["DateCreated"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        string token = "";
                        string user = o3.ToString();
                        var date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        var secretKey = new byte[] { 164, 60, 194, 0, 161, 189, 41, 38, 130, 89, 141, 164, 45, 170, 159, 209, 69, 137, 243, 216, 191, 131, 47, 250, 32, 107, 231, 117, 37, 158, 225, 234 };
                        //token = Jose.JWT.Encode(o3.ToString(), secretKey, Jose.JwsAlgorithm.HS256);
                        var userId = o3["UserId"].ToObject<string>();
                        var newToken = cms.CreateToken(new ContactsAll { ContactId = userId, EmailAddress = data.Email, UserLevelId = userlvl }, jwtKey, version.Result.Version.ToString());

                        res = "{\"jwt\":\"" + newToken + "\",\"user\":" + user + ",\"code\":\"1\",\"msg\":\"Login successful\"}";


                        try
                        {
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "delete from UserToken where UserId ='" + o3["UserId"] + "';"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);


                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "insert into UserToken " +
                                "( Token, UserId, Datetime) " +
                                "values ( '" + newToken + "','" + o3["UserId"] + "','" + date + "');"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);
                        }
                        catch
                        {
                        }
                        finally
                        {
                            if (loginCount > 0)
                            {
                                res = "{\"jwt\":\"" + newToken + "\",\"user\":" + user + ",\"code\":\"1\",\"msg\":\"Login successful\"}";
                            }
                            else
                            {
                                res = "{\"jwt\":\"" + newToken + "\",\"user\":" + user + ",\"code\":\"firstlogin\",\"msg\":\"Need to Reset Password\",\"id\":" + o3["UserId"] + "}";
                            }
                        }
                    }
                }

                else
                {
                    res = "{\"code\":\"0\",\"msg\":\"Email or Password is incorrect\"}";
                }
            }

            return Ok(res);

        }


        [HttpGet("Logout")]
        public IActionResult Logout([FromBody] dynamic data)
        {

            var token = "";
            var res = "";

            try
            {
                token = Request.Headers["Authorization"];
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Token not found\"}";
            }
            finally
            {
                try
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from UserToken where Token ='" + token + "';");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                }
                catch
                {
                    res = "{\"code\":\"0\",\"msg\":\"Invalid token\"}";
                }
                finally
                {
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    var o3 = JObject.Parse(result);
                    if (o3.Count > 0)
                    {
                        try
                        {
                            sb = new StringBuilder();
                            string query1 = "delete from UserToken where Token ='" + token + "'";
                            sb.Append(query1);
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            res = "{\"code\":\"1\",\"msg\":\"You are now logout\"}";
                        }
                        catch
                        {
                            res = "{\"code\":\"0\",\"msg\":\"Logout failed\"}";
                        }
                    }
                    else
                    {
                        res = "{\"code\":\"0\",\"msg\":\"Invalid Token\"}";
                    }


                }
            }

            return Ok(res);

        }

        [HttpPost("CountLogin/{role}/{id}")]
        public IActionResult CountLogin(string role, string id)
        {
            string res = "{\"code\":\"0\"}";
            try
            {
                string query = "";

                if (role == "contact")
                {
                    query = "update Contacts_All set LoginCount = LoginCount + 1, LastLogin = NOW() where ContactId = '" + id + "'";
                }
                else if (role == "student")
                {
                    query = "update tblStudents set LoginCount = LoginCount + 1, LastLogin = NOW() where StudentID = '" + id + "'";
                }

                if (query != "")
                {
                    sb = new StringBuilder();
                    sb.AppendFormat(query);
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
            }
            catch
            {
                res = "{\"code\":\"0\"}";
            }
            finally
            {
                res = "{\"code\":\"1\"}";
            }

            return Ok(res);

        }


        [HttpGet("CheckToken")]
        public IActionResult CheckToken([FromBody] dynamic data)
        {

            var token = "";
            var res = "";

            try
            {
                token = Request.Headers["Authorization"];
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Token not found\"}";
            }
            finally
            {
                try
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from UserToken where Token ='" + token + "';");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                catch
                {
                    res = "{\"code\":\"0\",\"msg\":\"Invalid token\"}";
                }
                finally
                {
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    var o3 = JObject.Parse(result);
                    if (o3.Count > 0)
                    {
                        res = "{\"code\":\"1\",\"msg\":\"Token Valid\"}";
                    }
                    else
                    {
                        res = "{\"code\":\"0\",\"msg\":\"Invalid Token\"}";
                    }
                }
            }

            return Ok(res);

        }

        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        [HttpPost("Register")]
        public async Task<IActionResult> Register([FromBody] dynamic data)
        {
            bool resmail = false;
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            JObject o5;

            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            //count email registered
            sb = new StringBuilder();
            sb.AppendFormat("select count(Email) as count from tblStudents where Email = '" + data.Email + "' AND Status='A'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);
            count3 = Int32.Parse(o3["count"].ToString());

            if (count3 > 0)
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Registration failed.<br>Email Already Registered.\"" +
                    "}";
            }
            else
            {
                //count first
                sb = new StringBuilder();
                sb.AppendFormat("select count(StudentID) as count from tblStudents;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o1 = JObject.Parse(result);
                count1 = Int32.Parse(o1["count"].ToString());

                //create password random
                string randompassword = CreatePassword(8);

                //execute
                try
                {
                    if (data.UserLevelId == "Professional")
                    {
                        data.DateBirth = null;
                        data.EGD = null;
                    }
                    sb = new StringBuilder();
                    // sb.AppendFormat(
                    //     "insert into tblStudents " +
                    //     "( Firstname, Lastname, CountryID, Email, Email2, Password, Password_plain, DateBirth, Status, LanguageID, DateAdded, LoginCount, ExpetationGradDate, TermAndCondition ) " +
                    //     "values ( '" + data.FirstName + "','" + data.LastName + "','" + data.Country + "','" + data.Email + "','" + data.EmailAddress2 + "',md5(CONCAT(NOW(),'-','"+data.Email+"')), CONCAT(NOW(),'-','"+data.Email+"'),'" + data.DateBirth + "', 'A','" + data.LanguageID + "', NOW(), 0, '" + data.EGD + "', '" + data.TermAndCondition + "');"
                    // ); /* remove password_plain */
                    sb.AppendFormat(
                        "insert into tblStudents " +
                        "( Firstname, Lastname, CountryID, Email, Email2, Password, DateBirth, Status, LanguageID, DateAdded, LoginCount, ExpetationGradDate, TermAndCondition,StatusLevel ) " +
                        "values ( '" + data.FirstName + "','" + data.LastName + "','" + data.Country + "','" + data.Email + "','" + data.EmailAddress2 + "',md5(CONCAT(NOW(),'-','" + data.Email + "')),'" + data.DateBirth + "', 'A','" + data.LanguageID + "', NOW(), 0, '" + data.EGD + "', '" + data.TermAndCondition + "', '" + data.UserLevelId + "');"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                    sb = new StringBuilder();
                    sb.AppendFormat("SELECT distinct CONCAT( s.Firstname, ' ', s.Lastname ) AS StudentName, `password`, s.DateAdded + INTERVAL 1 DAY AS ExpiredDate, StudentID, Firstname, Lastname, `subject`, body_email FROM tblStudents s LEFT JOIN EmailBody eb ON CASE WHEN (select count(*) from EmailBody where  EmailBody.Key = s.LanguageID and body_type = '3') > 0 THEN s.LanguageID ELSE 35 END = eb.`Key` AND eb.body_type = '3' WHERE StudentID IN ( SELECT MAX( StudentID ) FROM tblStudents )");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    o4 = JObject.Parse(result);
                }
                catch (Exception ex)
                {
                }
                finally
                {
                    string mailto = data.Email;
                    string subject = o4["subject"].ToString();
                    string message = o4["body_email"].ToString();

                    message = message.Replace("[StudentName]", o4["StudentName"].ToString());
                    message = message.Replace("[LinkActivation]", mainURL + "activation/student-" + o4["password"].ToString());
                    message = message.Replace("[ExpiredTime]", o4["ExpiredDate"].ToString());

                    resmail = await _emailSender.SendMailAsync(mailto, subject, message);
                    //Boolean resmail = await _emailSender.SendEmailAsync(mailto, subject, message);
                    if (resmail != true)
                    {
                        res =
                            "{" +
                            "\"code\":\"0\"," +
                            "\"message\":\"Email failed to send\"" +
                            "}";
                    }
                    else
                    {
                        res =
                            "{" +
                            "\"code\":\"1\"," +
                            "\"message\":\"Email success to send\"" +
                            "}";
                    }
                    //using (var task = new Task<Task<bool>>(() => _emailSender.SendEmailAsync(mailto, subject, message)))
                    //{
                    //    task.Start();

                    //    if (task.Wait(10000))
                    //    {

                    //        var result = await task.Result;
                    //        if (result)
                    //        {
                    //            res =
                    //           "{" +
                    //           "\"code\":\"0\"," +
                    //           "\"message\":\"Email sent\"" +
                    //           "}";
                    //        }
                    //        else
                    //        {
                    //            res = "{" +
                    //                   "\"code\":\"0\"," +
                    //                   "\"message\":\"Email not sent\"" +
                    //                   "}";
                    //        }
                    //    }
                    //    else
                    //    {
                    //        res = "{" +
                    //             "\"code\":\"0\"," +
                    //             "\"message\":\"Email not sent in time\"" +
                    //             "}";
                    //    }

                    //}

                    //if (resmail == true)
                    //{
                    //    res =
                    //        "{" +
                    //        "\"code\":\"0\"," +
                    //        "\"message\":\"Email sent\"" +
                    //        "}";
                    //}
                    //else
                    //{
                    //    res =
                    //        "{" +
                    //        "\"code\":\"0\"," +
                    //        "\"message\":\"Email not sent\"" +
                    //        "}";
                    //}

                }

                //count after
                sb = new StringBuilder();
                sb.AppendFormat("select count(StudentID) as count from tblStudents;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o2 = JObject.Parse(result);
                count2 = Int32.Parse(o2["count"].ToString());

                if (count2 > count1)
                {

                    sb = new StringBuilder();
                    sb.AppendFormat("SELECT *, CONCAT(FirstName,' ',LastName) as StudentName FROM tblStudents WHERE StudentID IN (SELECT MAX(StudentID) FROM tblStudents)");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                    string restmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    JObject resobj = JObject.Parse(restmp);
                    JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                    //string description = "Register New Student <br/>" +
                    //    "Student Id = " + resobj["StudentID"] + " <br/>" +
                    //    "Student Name = " + resobj["StudentName"] + " <br/>" +
                    //    "Email Address = " + resobj["Email"];

                    //_audit.ActionPost(resobj["StudentID"].ToString(), description, resobj["StudentName"].ToString());
                    JObject OldData = new JObject();
                    try
                    {

                        _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.tblStudents, resobj["StudentID"].ToString(), resobj["Email"].ToString(), " where StudentID = " + resobj["StudentID"].ToString() + "");
                    }
                    catch (Exception ex)
                    { ex.ToString(); }
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Registration success.<br/>Please check your email\"" +
                        "}";
                }

            }
            return Ok(res);

        }

        [HttpPost("ForgetPassword")]
        public async Task<IActionResult> ForgetPassword([FromBody] dynamic data)
        {
            int count3 = 0;
            JObject o3;

            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";
            #region olddata_beforeupdate

            sb = new StringBuilder();
            sb.AppendFormat("select * from Contacts_All where ca.EmailAddress = '" + data.Email + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            #endregion

            //count email registered
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count, eb.`subject`, eb.body_email, ca.ContactName, ca.ContactId from Contacts_All ca LEFT JOIN EmailBody eb ON CASE WHEN PrimaryLanguage NOT IN ('35') THEN '35' ELSE PrimaryLanguage END = eb.`Key` AND eb.body_type = '14' where ca.EmailAddress = '" + data.Email + "' and ca.`Status` = 'A'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);
            count3 = Int32.Parse(o3["count"].ToString());

            if (count3 < 1)
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Email Not Registered.\"" +
                    "}";
            }
            else
            {
                DateTime localDate = DateTime.Now;
                string strdate = localDate.ToString("yyyy-MM-dd hh:mm:ss");
                string saltedPassword = string.Format("{0}|{1}", data.Email, strdate.Trim());
                Byte[] byteInput = Encoding.UTF8.GetBytes(saltedPassword);

                // HashAlgorithm hash = new SHA512Managed();
                var res2 = Convert.ToBase64String(byteInput);
                res2 = res2.Replace("=", "_");

                //execute
                try
                {

                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "update Contacts_All set ResetPasswordDate = '" + strdate + "', ResetPasswordCode = '" + res2 + "' where EmailAddress = '" + data.Email + "' and `Status` = 'A'"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                catch
                {
                }
                finally
                {
                    string mailto = data.Email;
                    string subject = o3["subject"].ToString();
                    string url = mainURL + "reset-password/" + res2;
                    string message = o3["body_email"].ToString();

                    message = message.Replace("[LinkResetPassword]", "<a href='" + url + "'>" + url + "</a>");
                    message = message.Replace("[ContactName]", o3["ContactName"].ToString()); /* add parameter name email body */

                    bool resmail = await _emailSender.SendMailAsync(mailto, subject, message);

                    if (resmail == true)
                    {
                        res = "{\"code\":\"1\",\"msg\":\"Message sent\"}";
                    }
                    else
                    {
                        res = "{\"code\":\"0\",\"msg\":\"Message not sent\"}";
                    }

                    //string description = "Forget Password Contact <br/>" +
                    //    "Contact Id = " + o3["ContactId"] + " <br/>" +
                    //    "Contact Name = " + o3["ContactName"] + " <br/>" +
                    //    "Email Address = " + data.Email;

                    //_audit.ActionPost(o3["ContactId"].ToString(), description, o3["ContactName"].ToString());
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Contacts_All where ContactId = '" + o3["ContactId"].ToString() + "'");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                    _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, o3["ContactId"].ToString(), o3["ContactName"].ToString(), " where ContactId = " + o3["ContactId"].ToString() + "; Notes: Forgot user password");

                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Please check your email to reset your password.\"" +
                        "}";

                }
            }

            return Ok(res);

        }



        [HttpGet("ResetPassword/{ResetPasswordCode}")]
        public IActionResult ResetPassword(string ResetPasswordCode)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Get Data Failed\"" +
                "}";
            int count3 = 0;
            JObject o3;

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select count(*) as count, EmailAddress from Contacts_All where ResetPasswordCode = '" + ResetPasswordCode + "';update Contacts_All set LoginCount = LoginCount + 1 where ResetPasswordCode = '" + ResetPasswordCode + "';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                count3 = Int32.Parse(o3["count"].ToString());

                if (count3 > 0)
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Reset Password Code Valid.\"," +
                        "\"Email\":\"" + o3["EmailAddress"].ToString() + "\"" +
                        "}";
                }
                else
                {
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Your reset password link is invalid, could you please try to reset password again.\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        [HttpPost("ChangePassword")]
        public async Task<IActionResult> ChangePassword([FromBody] dynamic data)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Get Data Failed\"" +
                "}";
            int count3 = 0;
            JObject o3;

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select count(*) as count from Contacts_All where ResetPasswordCode = '" + data.ResetPasswordCode + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                count3 = Int32.Parse(o3["count"].ToString());

                if (count3 > 0)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Contacts_All where ResetPasswordCode = '" + data.ResetPasswordCode + "'");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    o3 = JObject.Parse(result);

                    //generate password
                    string password = data.Password;
                    string strdate = "";
                    string salt = "";
                    try
                    {
                        strdate = o3["DateAdded"].ToString();
                        DateTime myDate = DateTime.Parse(strdate);
                        salt = myDate.ToString("yyyyMMddHHMMss", CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        strdate = "";
                        salt = "";
                    }

                    string saltedPassword = string.Format("{0}|{1}", salt, password.Trim());

                    Byte[] byteInput = Encoding.UTF8.GetBytes(saltedPassword);
                    HashAlgorithm hash = new SHA512Managed();
                    res = Convert.ToBase64String(hash.ComputeHash(byteInput));

                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "update Contacts_All set ResetPasswordCode = '', password = md5('" + data.Password + "') where ResetPasswordCode = '" + data.ResetPasswordCode + "'"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                    res =
                       "{" +
                       "\"code\":\"1\"," +
                       "\"message\":\"Reset Password Success.\"" +
                       "}";

                }
                else
                {
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Your reset password link is invalid, could you please try to reset password again.\"" +
                        "}";
                }
            }

            return Ok(res);

        }


        [HttpPost("ForgetPasswordStudent")]
        public async Task<IActionResult> ForgetPasswordStudent([FromBody] dynamic data)
        {
            int count3 = 0;
            JObject o3;

            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from tblStudents where Email = '" + data.Email + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            //count email registered
            sb = new StringBuilder();
            // sb.AppendFormat("select count(*) as count, eb.`subject`, eb.body_email, CONCAT(s.Firstname,' ',s.Lastname) as StudentName from tblStudents s LEFT JOIN EmailBody eb ON CASE WHEN LanguageID NOT IN ('35') THEN '35' ELSE LanguageID END = eb.`Key` AND eb.body_type = '14' where s.Email = '"+data.Email+"' ORDER BY StudentID DESC");
            sb.AppendFormat(
                "SELECT count(*) as count, `subject`,body_email,StudentName,StudentID FROM ( " +
                "SELECT eb.`subject`, eb.body_email, CONCAT(s.Firstname,' ',s.Lastname) AS StudentName, StudentID FROM tblStudents s LEFT JOIN EmailBody eb ON CASE WHEN LanguageID NOT IN ('35') THEN '35' ELSE LanguageID END = eb.`Key` AND eb.body_type = '14' where s.Email = '" + data.Email + "' AND s.Status='A' ORDER BY StudentID DESC " +
                ") AS Email"
                );
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);
            count3 = Int32.Parse(o3["count"].ToString());

            if (count3 < 1)
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Email Not Registered.\"" +
                    "}";
            }
            else
            {

                DateTime localDate = DateTime.Now;
                string strdate = localDate.ToString("yyyy-MM-dd hh:mm:ss");

                string saltedPassword = string.Format("{0}|{1}", data.Email, strdate.Trim());

                Byte[] byteInput = Encoding.UTF8.GetBytes(saltedPassword);
                // HashAlgorithm hash = new SHA512Managed();
                var res2 = Convert.ToBase64String(byteInput);
                res2 = res2.Replace("=", "_");

                //execute
                try
                {
                    // sb = new StringBuilder();
                    // sb.AppendFormat(
                    //     "update tblStudents set ResetPasswordDate = '" + strdate + "', ResetPasswordCode = '" + res2 + "' where Email = '" + data.Email + "'"
                    // );
                    // sqlCommand = new MySqlCommand(sb.ToString());
                    // ds = oDb.getDataSetFromSP(sqlCommand);

                    /* handling duplicate email */
                    JObject objStudentId;
                    string resTmp = "";

                    sb = new StringBuilder();

                    #region Select Student Query
                    // Ater data cleaning we will depend on Status for Duplicate Records
                    sb.AppendFormat(
                        "SELECT StudentID FROM tblStudents WHERE Email = '" + data.Email + "' AND Status='A'"
                    );
                    //old Code
                    //sb.AppendFormat(
                    //    "SELECT StudentID FROM tblStudents WHERE Email = '" + data.Email + "' ORDER BY StudentID DESC LIMIT 1"
                    //);
                    #endregion

                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    resTmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    objStudentId = JObject.Parse(resTmp);

                    sb = new StringBuilder();

                    #region Update Student Query to update ResetPAsswordCode
                    sb.AppendFormat(
                        "update tblStudents set ResetPasswordDate = '" + strdate + "', ResetPasswordCode = '" + res2 + "' where StudentID = '" + objStudentId["StudentID"] + "'"
                    );
                    #endregion

                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    /* handling duplicate email */

                }
                catch
                {
                }
                finally
                {
                    string mailto = data.Email;
                    string subject = o3["subject"].ToString();
                    string url = mainURL + "reset-password-student/" + res2;
                    string message = o3["body_email"].ToString();

                    message = message.Replace("[LinkResetPassword]", "<a href='" + url + "'>" + url + "</a>");
                    message = message.Replace("[ContactName]", o3["StudentName"].ToString()); /* add parameter name email body */

                    Boolean resmail = await _emailSender.SendMailAsync(mailto, subject, message);

                    if (resmail == true)
                    {
                        res = "{\"code\":\"1\",\"msg\":\"Message sent\"}";
                    }
                    else
                    {
                        res = "{\"code\":\"0\",\"msg\":\"Message not sent\"}";
                    }

                    //string description = "Forget Password Student <br/>" +
                    //    "Student Id = " + o3["StudentID"] + " <br/>" +
                    //    "Student Name = " + o3["StudentName"] + " <br/>" +
                    //    "Email Address = " + data.Email;

                    //_audit.ActionPost(o3["StudentID"].ToString(), description, o3["StudentName"].ToString());
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from tblStudents where Email = '" + data.Email + "'");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                    _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.tblStudents, o3["StudentID"].ToString(), o3["StudentName"].ToString(), " where StudentID = " + o3["StudentID"].ToString() + "and Email Address = " + data.Email + "; Notes: Forgot Student Password");

                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Please check your email to reset your password.\"" +
                        "}";

                }

            }

            return Ok(res);

        }

        [HttpGet("ResetPasswordStudent/{ResetPasswordCode}")]
        public IActionResult ResetPasswordStudent(string ResetPasswordCode)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Get Data Failed\"" +
                "}";
            int count3 = 0;
            JObject o3;

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select count(*) as count, Email from tblStudents where ResetPasswordCode = '" + ResetPasswordCode + "';update tblStudents set LoginCount = LoginCount + 1 where ResetPasswordCode = '" + ResetPasswordCode + "';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                count3 = Int32.Parse(o3["count"].ToString());

                if (count3 > 0)
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Reset Password Code Valid.\"," +
                        "\"Email\":\"" + o3["Email"].ToString() + "\"" +
                        "}";
                }
                else
                {
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Your reset password link is invalid, could you please try to reset password again.\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        [HttpPost("ChangePasswordStudent")]
        public async Task<IActionResult> ChangePasswordStudent([FromBody] dynamic data)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Get Data Failed\"" +
                "}";
            int count3 = 0;
            JObject o3;

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select count(*) as count from tblStudents where ResetPasswordCode = '" + data.ResetPasswordCode + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                count3 = Int32.Parse(o3["count"].ToString());

                if (count3 > 0)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from tblStudents where ResetPasswordCode = '" + data.ResetPasswordCode + "'");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    o3 = JObject.Parse(result);

                    //generate password
                    string password = data.Password;
                    string strdate = "";
                    string salt = "";
                    try
                    {
                        strdate = o3["DateAdded"].ToString();
                        DateTime myDate = DateTime.Parse(strdate);
                        salt = myDate.ToString("yyyyMMddHHMMss", CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        strdate = "";
                        salt = "";
                    }

                    string saltedPassword = string.Format("{0}|{1}", salt, password.Trim());

                    Byte[] byteInput = Encoding.UTF8.GetBytes(saltedPassword);
                    HashAlgorithm hash = new SHA512Managed();
                    res = Convert.ToBase64String(hash.ComputeHash(byteInput));

                    sb = new StringBuilder();
                    // sb.AppendFormat(
                    //     "update tblStudents set ResetPasswordCode = '', password = md5('" + data.Password + "') , password_plain = '" + data.Password + "' where ResetPasswordCode = '" + data.ResetPasswordCode + "'"
                    // );  /* remove password_plain */
                    sb.AppendFormat(
                        "update tblStudents set ResetPasswordCode = '', password = md5('" + data.Password + "') where ResetPasswordCode = '" + data.ResetPasswordCode + "'"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                    res =
                       "{" +
                       "\"code\":\"1\"," +
                       "\"message\":\"Reset Password Success.\"" +
                       "}";

                }
                else
                {
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Your reset password link is invalid, could you please try to reset password again.\"" +
                        "}";
                }
            }


            return Ok(res);

        }

        JObject o4;
        StringBuilder sb1 = new StringBuilder();
        [HttpPost("ProfileEIM")]
        public async Task<IActionResult> AddNewContact([FromBody] dynamic data)
        {
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            int count4 = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            JObject o5;
            string res;
            string instructorid = "";
            string passwordtmp = "";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Contacts_All");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());

            sb = new StringBuilder();
            sb.AppendFormat("select MAX(CAST(ContactId AS UNSIGNED)) as ContactId from Contacts_All");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);
            count3 = Int32.Parse(o3["ContactId"].ToString()) + 1;

            Boolean userlvl = false;
            string[] userlvlarr = data.UserLevelId.ToString().Split(",");
            for (int i = 0; i < userlvlarr.Length; i++)
            {
                if (userlvlarr[i] == "TRAINER")
                {
                    userlvl = true;
                }
            }

            if (data.ATCRole.ToString() == "Y" || userlvl)
            {

                sb = new StringBuilder();

                // sb.AppendFormat("SELECT InstructorId FROM Contacts_All WHERE ContactIdInt = ( SELECT MAX( ContactIdInt ) FROM Contacts_All WHERE InstructorId <> '' OR InstructorId <> NULL ) ");

                /* generate instructorid based updated contact to be instructor */
                // sb.AppendFormat("SELECT DISTINCT LPAD(CAST(InstructorId AS UNSIGNED) + 1,6,'0') AS LastInstructor FROM Contacts_All WHERE InstructorId = ( SELECT MAX( CAST(InstructorId AS UNSIGNED) ) FROM (select InstructorId FROM Contacts_All where CHAR_LENGTH(TRIM(InstructorId)) > 5) as Instructor )");

                sb.AppendFormat("SELECT MAX(CAST(InstructorId AS UNSIGNED)) + 1 AS LastInstructor FROM Contacts_All");

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o5 = JObject.Parse(result);

                // int n;
                // bool itsnumber = int.TryParse(o5["InstructorId"].ToString(), out n);

                // if(!itsnumber){
                //     count4 = Int32.Parse(o5["InstructorId"].ToString().Substring(Math.Max(0,o5["InstructorId"].ToString().Length - 6))) + 1;
                // }else{
                //     count4 = Int32.Parse(o5["InstructorId"].ToString())+1;
                // }

                // instructorid = count4.ToString("000000");

                instructorid = o5["LastInstructor"].ToString();

            }

            //create password random
            string randompassword = CreatePassword(8);

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Contacts_All (ContactId, FirstName, LastName, Salutation, Gender, PrimaryIndustryId, " +
                    "PrimaryLanguage, SecondaryLanguage, Address1, Address2, Address3, City, StateProvince, CountryCode, " +
                    "PostalCode, WebsiteUrl, Bio, EmailAddress, EmailAddress2, Telephone1, Mobile1, DoNotEmail, ShareEmail, ShareTelephone, " +
                    "ShareMobile, ShowInSearch, MobileCode, TelephoneCode, ATCRole, UserLevelId, AddedBy, ContactName, PrimaryRoleId, PrimarySiteId, Status, DateAdded, LoginCount, password, InstructorId,Designation,DateLastAdmin,LastAdminBy) " +
                    "values ('" + count3 + "','" + data.FirstName + "','" + data.LastName + "','" + data.Salutation + "','" + data.Gender + "','" + data.PrimaryIndustryId +
                    "', '" + data.PrimaryLanguage + "','" + data.SecondaryLanguage + "','" + data.Address1 + "','" + data.Address2 + "','" + data.Address3 + "','" + data.City + "','" + data.StateProvince + "','" + data.CountryCode +
                    "', '" + data.PostalCode + "','" + data.WebsiteUrl + "','" + data.Bio + "','" + data.EmailAddress + "','" + data.EmailAddress2 + "','" + data.Telephone1 + "','" + data.Mobile1 + "','" + data.DoNotEmail + "','" + data.ShareEmail + "','" + data.ShareTelephone +
                    "', '" + data.ShareMobile + "','" + data.ShowInSearch + "','" + data.MobileCode + "','" + data.TelephoneCode + "','" + data.ATCRole + "','" + data.UserLevelId + "','" + data.AddedBy + "','" + data.ContactName + "','" + data.PrimaryRoleId + "','" + data.PrimarySiteId + "','A',NOW(), 0, md5(CONCAT(NOW(),'-','" + data.EmailAddress + "')), '" + instructorid + "','" + data.Designation + "',Now(),'" + data.cuid + "');" 
                );

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (data.SiteId != "")
                {
                    sb1 = new StringBuilder();
                    sb1.AppendFormat(
                        "insert into SiteContactLinks " +
                        "(Status, DateAdded, AddedBy, SiteId, ContactId) " +
                        "values ( 'A',NOW(),'" + data.AddedBy + "','" + data.SiteId + "','" + count3 + "');"
                    );
                    sqlCommand = new MySqlCommand(sb1.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }

                sb = new StringBuilder();
                sb.AppendFormat("SELECT ca.ContactIdInt, ca.ContactId, ca.`password`, ca.DateAdded + INTERVAL 1 DAY as ExpiredDate, eb.`subject`, eb.body_email FROM Contacts_All ca LEFT JOIN EmailBody eb ON CASE WHEN ca.PrimaryLanguage NOT IN ('35') THEN '35' ELSE ca.PrimaryLanguage END = eb.`Key` AND eb.body_type = '7' where ca.ContactIdInt in (select MAX(ContactIdInt) FROM Contacts_All)");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o4 = JObject.Parse(result);
            }

            catch { }

            finally
            {
                string mailto = data.EmailAddress;
                string subject = o4["subject"].ToString();
                string message = o4["body_email"].ToString();

                message = message.Replace("[ContactName]", data.ContactName.ToString());
                message = message.Replace("[LinkActivation]", mainURL + "activation/contact-" + o4["password"].ToString());
                message = message.Replace("[ExpiredTime]", o4["ExpiredDate"].ToString());

                Boolean resmail = await _emailSender.SendMailAsync(mailto, subject, message);

                if (resmail != true)
                {
                    passwordtmp = randompassword;
                }
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Contacts_All");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());

            if (count2 > count1)
            {
                //string description = "Insert data Contact <br/>" +
                //    "ContactId = " + o4["ContactId"] + "<br/>" +
                //    "Contact Name = " + data.ContactName + "<br/>" +
                //    "Email = " + data.EmailAddress;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM Contacts_All where ContactId ='" + count3 + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                JObject OldData = new JObject();
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Contacts_All, data.UserId.ToString(), data.cuid.ToString(), " where ContactId = " + count3 + "");

                if (passwordtmp != "")
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"emailfailed\":\"1\"," +
                        "\"ContactId\":\"" + o4["ContactIdInt"].ToString() + "\"," +
                        "\"message\":\"Email failed to send \"" +
                        "}";
                }
                else
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"emailfailed\":\"0\"," +
                        "\"message\":\"Insert Data Success\"," +
                        "\"ContactId\":\"" + o4["ContactIdInt"].ToString() + "\"," +
                        "\"Id\":\"" + o4["ContactId"].ToString() + "\"," +
                        "\"Password\":\"" + passwordtmp + "\"," +
                        "\"SiteId\":\"" + data.SiteId + "\"" +
                        "}";
                }
            }
            else
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }

            return Ok(res);

        }

        [HttpPost("TerminateAccount")]
        public async Task<IActionResult> TerminateAccount([FromBody] dynamic data)
        {
            JObject o1;
            string res;
            Boolean resmail = false;
            JArray arr;
            sb = new StringBuilder();
            sb.AppendFormat("select * from tblStudents where StudentID = '" + data.StudentId + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update tblStudents set Status = 'P1', DateRequestTerminated = NOW() where StudentID = '" + data.StudentId + "'"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("select ContactName, EmailAddress, `subject`, body_email from (select ContactName, EmailAddress, PrimaryLanguage from Contacts_All where UserLevelId LIKE '%APPROVER_1%') as ca LEFT JOIN EmailBody eb ON CASE WHEN ca.PrimaryLanguage NOT IN ('35') THEN '35' ELSE ca.PrimaryLanguage END = eb.`Key` AND eb.body_type = '2'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr = JArray.Parse(result);

                for (int i = 0; i < arr.Count; i++)
                {
                    string mailto = arr[i]["EmailAddress"].ToString();
                    string subject = arr[i]["subject"].ToString();
                    string message = "";

                    message += arr[i]["body_email"].ToString();
                    message = message.Replace("[ContactName]", arr[i]["ContactName"].ToString());
                    message = message.Replace("[StudentId]", data.StudentId.ToString());
                    message = message.Replace("[StudentName]", data.StudentName.ToString());

                    resmail = await _emailSender.SendMailAsync(mailto, subject, message);
                }
            }
            catch { }
            finally
            {
                if (resmail != true)
                {
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Terminate Account Failed\"" +
                        "}";
                }
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from tblStudents where StudentID = '" + data.StudentId + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            if (o1["Status"].ToString() == "P1")
            {
                //string description = "Terminate Account Student <br/>" +
                //        "Student Id = " + data.StudentId + " <br/>" +
                //        "Student Name = " + data.StudentName;

                //_audit.ActionPost(data.StudentId.ToString(), description, data.StudentName.ToString());
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.tblStudents, data.StudentId.ToString(), data.StudentName.ToString(), " where StudentID = " + data.StudentId + "; Notes: Terminate student Account");


                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Terminate Account Will Be Process\"" +
                    "}";
            }
            else
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Terminate Account Failed\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpGet("EmailRequestSecondApprover")]
        public async Task<IActionResult> EmailRequestSecondApprover()
        {
            Boolean resmail = false;
            JArray arr;
            JArray arrApprover;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"There is no request terminate sending email.\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select DISTINCT UserLevelId, EmailAddress, ContactName from Contacts_All where UserLevelId like '%APPROVER%' and UserLevelId != 'APPROVER_1' ORDER BY UserLevelId");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            arrApprover = JArray.Parse(result);

            for (int h = 0; h < arrApprover.Count; h++)
            {
                sb = new StringBuilder();
                sb.AppendFormat("select StudentID, CONCAT(Firstname,' ',Lastname) as StudentName, Email, DATE_ADD(DateRequestTerminated, INTERVAL 1 DAY) AS RequestTerminatedSecondApprover from tblStudents where Status != 'A' and Status != 'X'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr = JArray.Parse(result);

                for (int i = 0; i < arr.Count; i++)
                {
                    if (arr[i]["RequestTerminatedSecondApprover"].ToString().Substring(0, 10) == DateTime.Today.ToString("dd/MM/yyyy"))
                    {
                        try
                        {
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update tblStudents set Status = 'P" + (h + 2) + "', DateRequestTerminated = NOW() where StudentID = '" + arr[i]["StudentID"].ToString() + "'"
                                );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            string mailto = arrApprover[h]["EmailAddress"].ToString();
                            string subject = "Autodesk Request Terminate Account";
                            string message = "";
                            message += "Hi " + arrApprover[h]["ContactName"].ToString() + ",";
                            message += "<br/><br/><br/>";
                            message += "You have request to terminate this account!";
                            message += "<br/><br/>";
                            message += "Student ID : " + arr[i]["StudentID"].ToString() + ",";
                            message += "<br/>";
                            message += "Student Name : " + arr[i]["StudentName"].ToString();
                            message += "<br/><br/>";
                            message += "Please process the request on this link " + mainURL + "studentinfo/terminate-student";
                            message += "<br/>";
                            message += "Thank you.";
                            message += "<br/>";
                            message += "Autodesk Learning Partner Team";
                            message += "<br/>";
                            message += "Please do not reply to this e-mail message. This address is not monitored.";

                            resmail = await _emailSender.SendMailAsync(mailto, subject, message);
                        }
                        catch
                        { }
                        finally
                        {
                            if (resmail == true)
                            {
                                res =
                                    "{" +
                                    "\"code\":\"1\"," +
                                    "\"message\":\"Email request terminate has been send.\"" +
                                    "}";
                            }
                            else
                            {
                                res =
                                    "{" +
                                    "\"code\":\"0\"," +
                                    "\"message\":\"There is no request terminate sending email.\"" +
                                    "}";
                            }

                        }
                    }
                }
            }
            return Ok(res);
        }

        [HttpGet("GetApprover")]
        public IActionResult GetApprover()
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select DISTINCT UserLevelId, EmailAddress, ContactName from Contacts_All where UserLevelId like '%APPROVER%' ORDER BY UserLevelId");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch { }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("ActivationCheck/{role}/{link}")]
        public IActionResult ActivationCheck(string role, string link)
        {
            try
            {
                string query = "";

                if (role == "contact")
                {
                    // query = "select ContactId, password_plain, password from Contacts_All "; /* remove password_plain */
                    query = "select ContactId, password, EmailAddress AS Email from Contacts_All ";
                }
                else
                {
                    // query = "select StudentID, password_plain, password from tblStudents "; /* remove password_plain */
                    query = "select StudentID, password, Email AS Email from tblStudents ";
                }

                // query += "where `password` = '"+link+"' and NOW() <= DateAdded + INTERVAL 1 DAY";
                // query += "where `password` = '"+link+"' AND NOW() <= SUBSTRING(password_plain, 1, 19) + INTERVAL 1 DAY"; /* remove password_plain */
                query += "where `password` = '" + link + "'";

                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpDelete("ExpiredActivation/{role}/{link}")]
        public IActionResult ExpiredActivation(string role, string link)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            string query = "";
            string query2 = "";

            try
            {
                if (role == "contact")
                {
                    query = "delete from Contacts_All ";
                    query2 = "select `password` from Contacts_All ";
                }
                else
                {
                    query = "delete from tblStudents ";
                    query2 = "select `password` from tblStudents ";
                }

                query += "where `password` = '" + link + "' and NOW() >= DateAdded + INTERVAL 1 DAY";

                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Delete Data Failed\"" +
                    "}";
            }
            finally
            {
                query2 += "where `password` = '" + link + "'";

                sb = new StringBuilder();
                sb.AppendFormat(query2);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                if (o3.Count < 1)
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                }
            }
            return Ok(res);
        }

        [HttpGet("EmailNotificationNewOrg")]
        public async Task<IActionResult> EmailNotificationNewOrg()
        {
            string res = "";
            bool resmail = false;
            JObject o1;
            JObject org;
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM Main_organization WHERE OrganizationId IN ( SELECT MAX( OrganizationId ) FROM Main_organization )");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                org = JObject.Parse(result);

                sb = new StringBuilder();
                // sb.AppendFormat("SELECT DISTINCT GROUP_CONCAT(ca.EmailAddress) AS EmailAddress, eb.`subject`, eb.body_email FROM Contacts_All ca LEFT JOIN EmailBody eb ON CASE WHEN ca.PrimaryLanguage NOT IN ('35') THEN '35' ELSE ca.PrimaryLanguage END = eb.`Key` AND eb.body_type = '8' WHERE ca.UserLevelId LIKE '%SUPERADMIN%'");
                // sb.AppendFormat("SELECT DISTINCT GROUP_CONCAT(ca.EmailAddress) AS EmailAddress, eb.`subject`, eb.body_email FROM Contacts_All ca LEFT JOIN EmailBody eb ON CASE WHEN ca.PrimaryLanguage NOT IN ('35') THEN '35' ELSE ca.PrimaryLanguage END = eb.`Key` AND eb.body_type = '8' WHERE ContactId = '940398'"); /* khusus buat selina */
                sb.AppendFormat("SELECT DISTINCT GROUP_CONCAT(ca.EmailAddress) AS EmailAddress, eb.`subject`, eb.body_email FROM Contacts_All ca LEFT JOIN EmailBody eb ON CASE WHEN ca.PrimaryLanguage NOT IN ('35') THEN '35' ELSE ca.PrimaryLanguage END = eb.`Key` AND eb.body_type = '8' WHERE ContactId IN ('940398','925453')"); /* only for selina dan rob */
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o1 = JObject.Parse(result);

                // string mailto = org["BillingContactEmailAddress"]+","+o1["EmailAddress"].ToString(); /* khusus buat selina */
                string mailto = "";

                // if(o1 != null){
                //     mailto = o1["EmailAddress"].ToString();
                // }else{
                //     mailto = "selina.shen@autodesk.com";
                // }

                /* only for selina dan rob*/

                if (o1 != null)
                {
                    mailto = o1["EmailAddress"].ToString();
                }
                else
                {
                    mailto = "robneudecker@engageglobalsolutions.com,selina.shen@autodesk.com";
                }

                /* only for selina dan rob */

                string subject = o1["subject"].ToString();
                string message = o1["body_email"].ToString();

                message = message.Replace("[DateAdded]", org["DateAdded"].ToString());
                message = message.Replace("[OrgId]", org["OrgId"].ToString());
                message = message.Replace("[OrgName]", org["OrgName"].ToString());

                resmail = await _emailSender.SendMailAsync(mailto, subject, message);

            }
            catch { }
            finally
            {
                if (resmail != true)
                {
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Email failed to send\"" +
                        "}";
                }
                else
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Email success to send\"" +
                        "}";
                }
            }
            return Ok(res);
        }

        [HttpPut("GeneratePassPopUpStudent")]
        public IActionResult GeneratePassPopUpStudent([FromBody] dynamic data)
        {
            JObject studentpassword;
            JObject studentpasswordnew;
            JObject NewData = new JObject();
            JObject OldData = new JObject();
            Boolean success = false;
            string randompassword = "";

            string res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Generate Password Failed\"" +
                    "}";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select s.`password` from tblStudents s where s.StudentID = '" + data.StudentID + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                studentpassword = JObject.Parse(result);
                OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                randompassword = CreatePassword(8);
                Boolean ondatabases = true;

                while (ondatabases)
                {
                    int count = 0;
                    string resulttmp = "";

                    sb = new StringBuilder();
                    sb.AppendFormat("select count(*) as count from tblStudents where `password` = md5('" + randompassword + "')");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    resulttmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    count = Int32.Parse(JObject.Parse(resulttmp)["count"].ToString());

                    if (count > 0)
                    {
                        randompassword = CreatePassword(8);
                    }
                    else
                    {
                        ondatabases = false;
                    }
                }

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update tblStudents set " +
                    "`password` = md5('" + randompassword + "') " +
                    ",LoginCount = 0 " +
                    "where StudentID = '" + data.StudentID + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("select `password` from tblStudents where StudentID = '" + data.StudentID + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result1 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                NewData = JsonConvert.DeserializeObject<JObject>(result1.ToString());
                studentpasswordnew = JObject.Parse(result1);

                if (studentpassword["password"].ToString() != studentpasswordnew["password"].ToString())
                {
                    success = true;
                }
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Generate Password Failed\"" +
                    "}";
            }
            finally
            {
                if (success)
                {
                    //string description = "Reset Password Student <br/>" +
                    //    "ContactId = " + data.ContactId;
                    //_audit.ActionPost(string.Empty, description, string.Empty);

                    //_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.tblStudents, data.UserId.ToString(), data.cuid.ToString(), " where StudentID = " + data.StudentID + ";Notes: Reset Password for Student");
                    _auditLogServices.LogHistory(new History
                    {
                        Admin = HttpContext.User.GetUserId(),
                        Date = DateTime.Now,
                        Description = "{" + "\"code\":\"1\"," + "\"message\":\"Temporary Password generate success\"" + "}",
                        Id = data.StudentID

                    });

                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Temporary Password<br/>" + randompassword + "\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        [HttpGet("BrowserList")]
        public async Task<IActionResult> BrowserList(string currentBrowser)
        {
            int res = 0;
            if (!String.IsNullOrEmpty(currentBrowser))
            {
                res = await dataServices.GetLookBrowserLimitation(currentBrowser);
            }
            return this.Ok(new { code = "succeed", version = res });
        }

        [HttpGet("EmailExist")]
        public async Task<IActionResult> EmailExist(string studentEmail)
        {
            bool isDuplicate = false;
            try
            {
                if (!string.IsNullOrEmpty(studentEmail))
                {
                    //should this work, seen efore with myql got problems on somewhere
                    //db.TblStudents.Any(x => x.Email.Equals(studentEmail))
                    var modelCount = db.TblStudents.Where(x => x.Email.Equals(studentEmail)).ToList();
                    if (modelCount.Count > 0)
                    {
                        isDuplicate = true;
                    }
                    isSucceed = true;
                }
            }
            catch (Exception e)
            {
                isSucceed = false;

            }

            return Ok(new
            {
                Code = isSucceed,
                IsDuplicate = isDuplicate
            });
        }

    }
}