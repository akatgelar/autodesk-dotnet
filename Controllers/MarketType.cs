using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.Models;
using autodesk.Code.AuditLogServices;
// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/MarketType")]
    public class MarketType : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit;//= new Audit();
        private readonly MySQLContext _mySQLContext;
        private readonly IAuditLogServices _auditLogServices;
        public MarketType(MySqlDb sqlDb, MySQLContext mySQLContext, IAuditLogServices auditLogServices)
        {

            oDb = sqlDb;
            _audit=new Audit(oDb);
            _mySQLContext = mySQLContext;
            _auditLogServices = auditLogServices;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from MarketType ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

         

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from MarketType where MarketTypeId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 
        
        

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from MarketType ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("MarketType")){   
                    query += "MarketType = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }
        
        

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from MarketType;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into MarketType "+
                   // "( MarketTypeId, MarketType) "+
                   "(  MarketType) " +
                    //"values ( '" +data.MarketTypeId+"','"+data.MarketType+"');"
                    "values ( '" + data.MarketType + "');"
                );

            Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from MarketType;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                //string description = "Insert data Market Type" + "<br/>" +
                //    "Market Type Name = "+ data.MarketType;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                sb = new StringBuilder();
                sb.AppendFormat("select * from MarketType ORDER BY MarketTypeId DESC limit 1; ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.MarketType, data.UserId.ToString(), data.cuid.ToString(), "where MarketType =" + data.MarketType + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }
 
            } 
 
            return Ok(res);  
            
        }


        

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from MarketType where MarketTypeId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update MarketType set "+ 
                    "MarketType='"+data.MarketType+"' "+
                    "where MarketTypeId='"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from MarketType where MarketTypeId ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            o3 = JObject.Parse(result);  

            if(o3["MarketType"].ToString() == data.MarketType.ToString())
            { 
                //string description = "Update data Market Type" + "<br/>" +
                //    "Market Type Name = "+ data.MarketType;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.MarketType, data.UserId.ToString(), data.cuid.ToString(), "where MarketTypeId =" + id + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Update Data Success\"" +
                     "}";
                }
                
            } 
 
            return Ok(res);  
        } 

        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";  
            try
            {  
                //sb = new StringBuilder();
                //sb.AppendFormat("delete from MarketType where MarketTypeId = '"+id+"'");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);

                var marketType = _mySQLContext.MarketType.FirstOrDefault(x => x.MarketTypeId == id);
                if (marketType!=null)
                {
                    _mySQLContext.MarketType.Remove(marketType);
                    var deleted = _mySQLContext.SaveChanges();
                    if (deleted>0)
                    {
                        var admin = HttpContext.User.GetUserId();
                        _auditLogServices.LogHistory(new History { Admin = admin, Date = DateTime.Now, Description = "Delete MarketType <br/> MarketTypeId : " + id, Id = id.ToString() });
                        res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                    }
                }
            }
            catch
            { 
            }
            //finally
            //{ 
            //    sb = new StringBuilder();
            //    sb.AppendFormat("select * from MarketType where MarketTypeId  ="+id+";");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);
            
            //    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //    o3 = JObject.Parse(result);  
            //    Console.WriteLine(o3.Count);
            //    if(o3.Count < 1){ 
            //        res =
            //            "{"+
            //            "\"code\":\"1\","+
            //            "\"message\":\"Delete Data Success\""+
            //            "}"; 
            //    }
            //}

            return Ok(res);
        } 

         


    }
}