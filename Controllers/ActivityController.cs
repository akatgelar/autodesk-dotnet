using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using autodesk.Code.ViewModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using autodesk.Code.CommonServices;
using autodesk.Code.AuditLogServices;

// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/Activity")]
    public class ActivityController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;// = new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        public Audit _audit;//= new Audit();
        String result;
        private IDataServices dataServices;
        private MySQLContext db;
        private ICommonServices commonServices;
        private readonly IAuditLogServices _auditLogServices;

        public ActivityController(MySqlDb sqlDb, MySQLContext _db, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _audit=new Audit(oDb);
            db = _db;
            dataServices=new DataServices(oDb,db);
            commonServices = new CommonServices();
            _auditLogServices = auditLogServices;
        }

     
 
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from JournalActivities ORDER BY ActivityName ASC");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }
         

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from JournalActivities where ActivityId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                if(dt.Rows[0]["IsUnique"].ToString() == null || dt.Rows[0]["IsUnique"].ToString() == "" || dt.Rows[0]["IsUnique"].ToString() == "null")
                {
                    dt.Rows[0]["IsUnique"] = "N";
                }
                string encodedata = dt.Rows[0]["Description"].ToString();
                encodedata = commonServices.HtmlEncoder(encodedata);
                dt.Rows[0]["Description"] = encodedata;
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }

            return Ok(result);
        } 
        
        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from JournalActivities ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("ActivityType")){   
                    query += "ActivityType = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("Status")){   
                    query += "Status = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 

            query += " ORDER BY ActivityName ASC "; 
 
            try
            {  
                sb.AppendFormat(query);
                
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                
                var settings = new JsonSerializerSettings
                {
                   
                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                settings.DefaultValueHandling = DefaultValueHandling.Include;
                var list= dataServices.DataTableToList<ActivitiesViewMode>(ds.Tables[0]);
                result= JsonConvert.SerializeObject(list, Formatting.Indented,
                    settings);
                //result = JsonConvert.SerializeObject<List<ActivitiesViewMode>>(ds.Tables[0], Formatting.Indented,
              //  settings);
               
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);    
            }
            return Ok(result);  
        }
        
        

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";

            #region olddata_beforeupdate         
            JObject OldData = JsonConvert.DeserializeObject<JObject>("".ToString());
            #endregion




            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from JournalActivities;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into JournalActivities "+
                    "( ActivityType, ActivityName, Description, IsUnique, AddedBy, DateAdded, LastAdminBy, DateLastAdmin, Status) "+
                    "values ( '"+data.activity_category+"','"+data.activity_name+"','"+data.description+"','"+data.unique+"','"+data.cuid+"','"+data.cdate+"','"+data.muid+"','"+data.mdate+"','"+data.status+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Insert Data Failed\""+
                    "}";  
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from JournalActivities;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            
            
            if(count2 > count1)
            { 
                string description = "Insert data Activity" + "<br/>" +
                    "Activity Type = "+ data.activity_category + "<br/>" +
                    "Activity Name = "+ data.activity_name + "<br/>" +
                    "Description = "+ data.description + "<br/>" +
                    "Unique = "+ data.unique + "<br/>" +
                    "Status = "+ data.status;

                #region newdata_afterupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from JournalActivities  where ActivityType  ='" + data.activity_category + "' and ActivityName ='" + data.activity_name + "' and IsUnique =' " + data.unique + "' and Status ='" + data.status + "' and Description ='"+ data.description + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                #endregion

                //_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.JournalActivities, data.UserId.ToString(), data.cuid.ToString(), " where ActivityType  =" + data.activity_category + " and ActivityName =" + data.activity_name + " and IsUnique =" + data.unique + " and Status =" + data.status + " and Description =" + data.description + "");
                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                _auditLogServices.LogHistory(new History {
                    Admin=HttpContext.User.GetUserId(),
                    Date=DateTime.Now,
                    Description="Add Activities <br/>Data:"+JsonConvert.SerializeObject(NewData),
                    Id=NewData.GetValue("ActivityId")?.ToString()
                });
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
            
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";

            #region olddata_beforeupdate
            sb = new StringBuilder();
            sb.AppendFormat("select * from JournalActivities where ActivityId='" + id + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion



            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update JournalActivities set "+ 
                    "ActivityType='"+data.activity_category+"', "+
                    "ActivityName='"+data.activity_name+"', "+
                    "Description='"+data.description+"', "+
                    "IsUnique='"+data.unique+"', "+
                    "AddedBy='"+data.cuid+"', "+
                    "DateAdded='"+data.cdate+"', "+
                    "LastAdminBy='"+data.muid+"', "+
                    "DateLastAdmin='"+data.mdate+"', "+ 
                    "Status='"+data.status+"' "+
                    "where ActivityId='"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from JournalActivities where ActivityId ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);


            #region newdata_afterupdate      
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion

            _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.JournalActivities, data.UserId.ToString(), data.cuid.ToString(), " where ActivityId =" + id + "");
            //string descriptionU = "Update data Activity" + "<br/>" +
            //    "Activity Type = "+ data.activity_category + "<br/>" +
            //    "Activity Name = "+ data.activity_name + "<br/>" +
            //    "Description = "+ data.description + "<br/>" +
            //    "Unique = "+ data.unique + "<br/>" +
            //    "Status = "+ data.status;

            //  _audit.ActionPost(data.UserId.ToString(), descriptionU, data.cuid.ToString());

            res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
 
            return Ok(res);  
        } 

        [HttpPut("status/{id}")]
        public IActionResult UpdateSatus(int id, [FromBody] dynamic data)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            var act = db.JournalActivities.FirstOrDefault(x => x.ActivityId == id);
            if (act != null)
            {
                act.Status = AutodeskConst.StatusConts.X;
                db.SaveChanges();
            }


            //#region olddata_beforeupdate
            //sb = new StringBuilder();
            //sb.AppendFormat("select * from JournalActivities where ActivityId='" + id + "'");
            //sqlCommand = new MySqlCommand(sb.ToString());
            //ds = oDb.getDataSetFromSP(sqlCommand);
            //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //#endregion

            

            //try
            //{  
            //    sb = new StringBuilder();
            //    sb.AppendFormat(
            //        "update JournalActivities set "+ 
            //        "Status='X' "+
            //        "where ActivityId='"+id+"'"
            //    ); 
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);
            //}
            //catch
            //{
            //    res =
            //        "{"+
            //        "\"code\":\"0\","+
            //        "\"message\":\"Delete Data Failed\""+
            //        "}";  
            //}
            //finally
            {

                /* autodesk plan 10 oct - complete all history log */
                //#region newdata_afterupdate
                //sb = new StringBuilder();
                //sb.AppendFormat("select * from JournalActivities where ActivityId='" + id + "'");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);
                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                //JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
               // #endregion

                //_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.JournalActivities, data.UserId.ToString(), data.cuid.ToString(), " where ActivityId=" + id + "");

                ////string description = "Delete data Activity" + "<br/>" +
                ////    "Activity Id = "+ id ;

                ////_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                /* end line autodesk plan 10 oct - complete all history log */

                _auditLogServices.LogHistory(new History
                {
                    Admin = HttpContext.User.GetUserId(),
                    Date = DateTime.Now,
                    Description = "Delete Activities ",
                    Id = id.ToString()
                });

                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Delete Data Success\""+
                    "}";  
            }

            return Ok(res);  
        } 

        
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        { 

            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";  
            try
            {  
                //sb = new StringBuilder();
                //sb.AppendFormat("delete from JournalActivities where ActivityId = '"+id+"'");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);

                var journalAct = db.JournalActivities.FirstOrDefault(x => x.ActivityId == id);
                if (journalAct!=null)
                {
                    db.JournalActivities.Remove(journalAct);
                    var deleted = db.SaveChanges();
                    if (deleted>0)
                    {
                        var admin = HttpContext.User.GetUserId();
                        _auditLogServices.LogHistory(new History { Admin = admin, Date = DateTime.Now, Description = "Delete JournalActivities <br/> ActivityId : " + id,Id=id.ToString() });
                        res =
                       "{" +
                       "\"code\":\"1\"," +
                       "\"message\":\"Delete Data Success\"" +
                       "}";
                    }
                    
                }
            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Delete Data Failed\""+
                    "}"; 
            }
            //finally
            //{ 
            //    sb = new StringBuilder();
            //    sb.AppendFormat("select * from JournalActivities where ActivityId ="+id+";");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);
            
            //    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
             
            //    o3 = JObject.Parse(result);  
            //    
            //    if(o3.Count < 1){ 
            //        res =
            //            "{"+
            //            "\"code\":\"1\","+
            //            "\"message\":\"Delete Data Success\""+
            //            "}"; 
            //    }
            //}

            return Ok(res);
        }

        [HttpGet("CekUnik/{obj}")]
        public IActionResult WhereObjUnik(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 
            
            query += "select ActivityName, count(*) as count from JournalActivities ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("ActivityName")){   
                    query += "ActivityName = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
            query += "group by ActivityName having count(*) > 0";
            

            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpGet("JournalAct/{type}")]
        public IActionResult SelectActivitiesofJournal(string type)
        {
            string paramName=string.Empty;
            if (type.Contains("Contact"))
            {
                paramName = "Contact Attribute";
            }else if (type.Contains("Site"))
            {
                paramName = "Site Attribute";
            }

            if (!String.IsNullOrEmpty(paramName))
            {

            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT ActivityId, ActivityName  FROM JournalActivities  WHERE ActivityType = '"+paramName+"' AND Status <> 'X' ORDER BY ActivityName");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }

            }
            else
            {
                result = "[]";
            }
            return Ok(result);                  
        } 

    }
}