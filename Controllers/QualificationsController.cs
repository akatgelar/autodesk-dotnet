using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.CommonServices;
using Newtonsoft.Json;
// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/Qualifications")]
    public class QualificationsController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result="";
        private Audit _audit;
        private ICommonServices commonServices;

        public QualificationsController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            _audit = new Audit(oDb);
            commonServices = new CommonServices();
        }

        [HttpGet("contact/{id}")]
        public IActionResult Select(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT p.productId, q.QualificationId, q.`Status`, DATE_FORMAT( q.DateAdded, '%m/%d/%Y' ) AS DateAdded, q.AddedBy, DATE_FORMAT( q.DateLastAdmin, '%m/%d/%Y' ) AS DateLastAdmin, q.LastAdminBy, q.ContactId, q.AutodeskProduct, DATE_FORMAT( q.QualificationDate, '%m/%d/%Y' ) AS QualificationDate, q.QualificationType, q.Comments, q.GUID, p.productName, pv.Version "+
                "FROM Qualifications q LEFT JOIN Products p ON q.AutodeskProduct = p.ProductId LEFT JOIN ProductVersions pv ON q.AutodeskProductVersion = pv.ProductVersionsId WHERE q.STATUS <> 'X'  AND q.ContactId = '"+id+ "'  AND p.Status ='A' ORDER BY q.QualificationId ");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        string encodedata = row[col].ToString();
                        string type = col.DataType.ToString();
                        if (type != "System.DateTime" && col.ColumnName != "DateAdded" && col.ColumnName != "DateLastAdmin" && col.ColumnName != "QualificationDate"  )
                        {
                            encodedata = commonServices.HtmlEncoder(encodedata);
                            row[col] = encodedata;
                        }

                    }

                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("Product/{id}")]
        public IActionResult Product(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT p.productId, q.QualificationId, q.`Status`, DATE_FORMAT( q.DateAdded, '%m/%d/%Y' ) AS DateAdded, q.AddedBy, DATE_FORMAT( q.DateLastAdmin, '%m/%d/%Y' ) AS DateLastAdmin, q.LastAdminBy, q.ContactId, q.AutodeskProduct, DATE_FORMAT( q.QualificationDate, '%m/%d/%Y' ) AS QualificationDate, q.QualificationType, q.Comments, q.GUID, p.productName, pv.Version "+
                "FROM Qualifications q JOIN Products p ON q.AutodeskProduct = p.ProductId JOIN ProductVersions pv ON q.AutodeskProductVersion = pv.ProductVersionsId WHERE q.STATUS <> 'X'  AND q.ContactId = '"+id+"' GROUP BY p.productName ");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("Version/{productid}/{id}")]
        public IActionResult Version(string productid,string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT pv.ProductVersionsId, pv.Version "+
                "FROM Qualifications q JOIN Products p ON q.AutodeskProduct = p.ProductId JOIN ProductVersions pv ON q.AutodeskProductVersion = pv.ProductVersionsId WHERE q.STATUS <> 'X'  AND q.ContactId = '"+id+"' AND p.productId = '"+productid+"' ORDER BY q.QualificationId ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        // [HttpGet("AcademicProject/{id}")]
        // public IActionResult SelectAcademicProject(string id)
        // {
        //     try
        //     {
        //         sb = new StringBuilder();
        //         sb.Append("select * from AcademicProject left join Dictionary on AcademicProject.ProjectId = Dictionary.`Key` "+
        //             "left join Products on AcademicProject.productId = Products.productId "+
        //             "where Dictionary.Parent = 'ProjectType' and AcademicProject.SiteId = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
        //     }
 
        //     return Ok(result);                  
        // }
         

        [HttpGet("{id}")]
        public IActionResult WhereId(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Qualifications where QualificationId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            var finalResult = JObject.Parse(result);
            return Ok(finalResult);
        } 
        
        

        // [HttpGet("where/{obj}")]
        // public IActionResult WhereObj(string obj)
        // {     
        //     var o1 = JObject.Parse(obj);
        //     string query = ""; 

        //     query += "select * from Qualifications ";
        //     int i = 0;
        //     JObject json = JObject.FromObject(o1);
        //     foreach (JProperty property in json.Properties())
        //     { 
        //         if(i==0){
        //             query += " where ";
        //         }
        //         if(i>0){
        //             query += " and ";
        //         }

        //         if(property.Name.Equals("AcademicType")){   
        //             query += "AcademicType = '"+property.Value.ToString()+"' "; 
        //         }
        //         if(property.Name.Equals("SiteId")){   
        //             query += "SiteId = '"+property.Value.ToString()+"' "; 
        //         }

        //         i=i+1;
        //     } 

        //     Console.WriteLine(query);
 
        //     try
        //     {  
        //         sb.AppendFormat(query);
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "Not found";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
        //     }
        //     return Ok(result);  
        // }
        
        // [HttpPost("Project")]
        // public IActionResult InsertAcademicProject([FromBody] dynamic data)
        // {    
            
        //     int count1 = 0;
        //     int count2 = 0;
        //     JObject o1;
        //     JObject o2;
        //     // JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Insert Data Failed\""+
        //         "}";  
             
        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from AcademicProject;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);
         
        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o1 = JObject.Parse(result); 
        //     count1 = Int32.Parse(o1["count"].ToString());
        //     Console.WriteLine(count1); 
            
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "insert into AcademicProject "+
        //             "(SiteId, CompletionDate, TargetEducator, TargetStudent, productId, Institution, CountryCode, Comment, DateAdded, AddedBy) "+
        //             "values ('"+data.SiteId+"','"+data.CompletionDate+"',"+data.TargetEducator+","+data.TargetStudent+",'"+data.productId+"','"+data.Institution+"','"+data.CountryCode+"','"+data.Comment+"','"+data.DateAdded+"','"+data.AddedBy+"');"
        //         );
        //         Console.WriteLine(sb.ToString());
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"0\","+
        //             "\"message\":\"Insert Data Failed\""+
        //             "}"; 
        //     }
        //     finally
        //     { 
        //     }

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from AcademicProject;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);   

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o2 = JObject.Parse(result); 
        //     count2 = Int32.Parse(o2["count"].ToString());
        //     Console.WriteLine(count2);
            
        //     if(count2 > count1)
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Insert Data Success\""+
        //             "}"; 
        //     } 
 
        //     return Ok(res);     
        // }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {


            #region olddata_beforeupdate          
            JObject OldData = JsonConvert.DeserializeObject<JObject>("".ToString());
            #endregion

            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Qualifications "+
                    "( AutodeskProduct, QualificationType, QualificationDate, Comments, AddedBy, DateAdded, ContactId, `Status`, AutodeskProductVersion,GUID),DateLastAdmin,LastAdminBy" +
                    "values ( '"+data.AutodeskProduct+"', '"+data.QualificationType+"','"+data.QualificationDate+"','"+data.Comments+"','"+data.AddedBy+
                    "',NOW(),'" + data.ContactId + "','A', '" + data.Version + "','',Now(),'"+ data.cuid + "');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Contacts_All set LastAdminBy = '" + data.cuid.ToString() + "', DateLastAdmin = Now() where ContactId = '" + data.ContactId + "'; "
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

            }
            catch
            { 
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Insert Data Failed\""+
                    "}";  
            }
            finally
            { 

                sb = new StringBuilder();
                sb.Append("SELECT p.productId, q.QualificationId, q.`Status`, DATE_FORMAT( q.DateAdded, '%m/%d/%Y' ) AS DateAdded, q.AddedBy, DATE_FORMAT( q.DateLastAdmin, '%m/%d/%Y' ) AS DateLastAdmin, q.LastAdminBy, q.ContactId, q.AutodeskProduct, DATE_FORMAT( q.QualificationDate, '%m/%d/%Y' ) AS QualificationDate, q.QualificationType, q.Comments, q.GUID, p.productName, pv.Version "+
                "FROM Qualifications q LEFT JOIN Products p ON q.AutodeskProduct = p.ProductId LEFT JOIN ProductVersions pv ON q.AutodeskProductVersion = pv.ProductVersionsId WHERE q.STATUS <> 'X' AND q.QualificationId IN (SELECT MAX(QualificationId) FROM Qualifications) ORDER BY q.QualificationId limit 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                string restmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject resobj = JObject.Parse(restmp);

                //string description = "Insert data Qualification Contact <br/>" +
                //        "ContactId = "+ data.ContactId +" <br/>" +
                //        "Product = "+ resobj["productName"] +" <br/>" +
                //        "Version = "+ resobj["Version"] +" <br/>" +
                //        "Type = "+ data.QualificationType +" <br/>" +
                //        "Date = "+ data.QualificationDate;


                #region newdata_afterupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from Qualifications where ContactId  ='" + data.id + "' and AutodeskProduct ='"+ resobj["productName"] + "' and AutodeskProductVersion ='" + resobj["Version"]  + "' and QualificationType = '" + data.QualificationType + "' and QualificationDate ='"+ data.QualificationDate  + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                #endregion

                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Qualifications, data.UserId.ToString(), data.cuid.ToString(), " where ContactId  =" + data.id + " and AutodeskProduct =" + resobj["productName"] + " and AutodeskProductVersion =" + resobj["Version"] + " and QualificationType = " + data.QualificationType + " and QualificationDate =" + data.QualificationDate + "");
               //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";
            }
            return Ok(res);  
            
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] dynamic data)
        {

            #region olddata_beforeupdate
            sb = new StringBuilder();
            sb.AppendFormat("select * from Qualifications where QualificationId='" + id + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion

            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Qualifications set "+ 
                    "AutodeskProduct='"+data.AutodeskProduct+"', "+
                    "AutodeskProductVersion='"+data.Version+"', "+
                    "QualificationType='"+data.QualificationType+"', "+
                    "QualificationDate='"+data.QualificationDate+"', "+
                    "Comments='"+data.Comments+"', "+
                    "LastAdminBy='"+data.LastAdminBy+"', "+
                    "DateLastAdmin=NOW() "+ 
                    "where QualificationId='"+id+"'"
                ); 
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Contacts_All set LastAdminBy = '" + data.LastAdminBy.ToString() + "', DateLastAdmin = Now() where ContactId = '" + data.ContactId + "'; "
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            {  

                sb = new StringBuilder();
                sb.Append("SELECT p.productId, q.QualificationId, q.`Status`, DATE_FORMAT( q.DateAdded, '%m/%d/%Y' ) AS DateAdded, q.AddedBy, DATE_FORMAT( q.DateLastAdmin, '%m/%d/%Y' ) AS DateLastAdmin, q.LastAdminBy, q.ContactId, q.AutodeskProduct, DATE_FORMAT( q.QualificationDate, '%m/%d/%Y' ) AS QualificationDate, q.QualificationType, q.Comments, q.GUID, p.productName, pv.Version "+
                "FROM Qualifications q LEFT JOIN Products p ON q.AutodeskProduct = p.ProductId LEFT JOIN ProductVersions pv ON q.AutodeskProductVersion = pv.ProductVersionsId WHERE q.STATUS <> 'X' AND q.QualificationId ='"+id+"' ORDER BY q.QualificationId limit 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                string restmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject resobj = JObject.Parse(restmp);

                string description = "Update data Qualification Contact <br/>" +
                    "ContactId = "+ data.ContactId +" <br/>" +
                    "Product = "+ resobj["productName"] +" <br/>" +
                    "Version = "+ resobj["Version"] +" <br/>" +
                    "Type = "+ data.QualificationType +" <br/>" +
                    "Date = "+ data.QualificationDate;

                #region newdata_afterupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from Qualifications where QualificationId='" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                #endregion

                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Qualifications, data.UserId.ToString(), data.cuid.ToString(), " where QualificationId=" + id + "");
                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            }
 
                
 
            return Ok(res);  
        } 

        /* issue 17102018 - add delete function */

        [HttpDelete("{id}/{cuid}/{UserId}/{ContactId}")]
        public IActionResult Delete(string id,string cuid,string UserId,string ContactId)
        {


            #region olddata_beforeupdate
            sb = new StringBuilder();
            sb.AppendFormat("select * from Qualifications where QualificationId='" + id + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            #endregion

           
            int isDeleted = 0;
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("Update Qualifications set Status = 'X' WHERE QualificationId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                isDeleted = oDb.temporaryUpdateFunction(sqlCommand);
            }
            catch
            {
                res = "Not found";
            }
            finally
            {
                //sb = new StringBuilder();
                //sb.AppendFormat("SELECT * FROM Qualifications WHERE QualificationId = '" + id + "'");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);

                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                //o3 = JObject.Parse(result);
                //added by soe
                if (isDeleted>0)
                {


                    #region newdata_afterupdate
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Qualifications where QualificationId='" + id + "'");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                    #endregion

                    _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Qualifications, UserId.ToString(), cuid.ToString(), " where QualificationId=" + id + "");


                    //string description = "Delete data Qualification Contact <br/>" +
                    //    "ContactId = " + ContactId +" <br/>" +
                    //    "QualificationId = "+ id ;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());

                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        /* end line issue 17102018 - add delete function */

    }
}