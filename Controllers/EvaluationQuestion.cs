using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.AuditLogServices;

// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/EvaluationQuestion")]
    public class EvaluationQuestion : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        StringBuilder sb1 = new StringBuilder();
		DataSet ds1 = new DataSet();
		String result1;

        public Audit _audit;//= new Audit();
        private readonly IAuditLogServices _auditLogServices;
        public EvaluationQuestion(MySqlDb sqlDb, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _audit=new Audit(oDb);
            _auditLogServices = auditLogServices;
        }


        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT EvaluationQuestionCode, CertificateTypeId, CourseId, EvaluationQuestionId, d1.KeyValue as `Year`, d2.KeyValue as `Language`,d2.`Key` AS LanguageID" +
                    " FROM EvaluationQuestion eq INNER JOIN Dictionary d1 ON eq.`Year` = d1.`Key` and d1.Parent = 'FYIndicator' INNER JOIN Dictionary d2 ON eq.LanguageID = d2.`Key` and d2.Parent = 'Languages' GROUP BY EvaluationQuestionId ORDER BY EvaluationQuestionCode");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }


        [HttpGet("Copy")]
        public IActionResult Copy()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT EvaluationQuestionCode, CertificateTypeId, CourseId, EvaluationQuestionId, d1.KeyValue as `Year`, d2.KeyValue as `Language` FROM EvaluationQuestion eq JOIN Dictionary d1 ON eq.`Year` = d1.`Key` and d1.Parent = 'FYIndicator'  JOIN Dictionary d2 ON eq.LanguageID = d2.`Key` and d2.Parent = 'Languages' WHERE LanguageID = '35' GROUP BY EvaluationQuestionCode");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select EvaluationQuestionId,CertificateTypeId, PartnerType, KeyValue as CertificateType, EvaluationQuestionCode, CourseId, Year, EvaluationQuestionTemplate, EvaluationQuestionJson "+
                    "from EvaluationQuestion LEFT JOIN Dictionary on EvaluationQuestion.CertificateTypeId = Dictionary.Key WHERE Dictionary.Parent = 'AAPCertificateType' AND EvaluationQuestionId ='"+id+"' LIMIT 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetSurvey/{eval_id}/{course_id}")]
        public IActionResult GetSurvey(string eval_id,string course_id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "SELECT ea.EvaluationQuestionCode,eq.EvaluationQuestionTemplate,eq.EvaluationQuestionJson,eq.LanguageID,ea.EvaluationAnswerJson " +
                    "FROM EvaluationAnswer ea JOIN EvaluationQuestion eq ON ea.EvaluationQuestionCode = eq.EvaluationQuestionCode WHERE ea.EvaluationAnswerId = '" + eval_id + "' AND ea.CourseId = '" + course_id + "' GROUP BY ea.EvaluationAnswerId"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("AvailableLang/{id}")]
        public IActionResult AvailableLanguage(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("SELECT d.`Key`, d.KeyValue, e.EvaluationQuestionTemplate, e.EvaluationQuestionJson FROM EvaluationQuestion e INNER JOIN Dictionary d ON e.LanguageID = d.`Key` WHERE d.Parent = 'Languages' AND e.EvaluationQuestionId = '" + id+ "' GROUP BY EvaluationQuestionId ORDER BY EvaluationQuestionCode");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("checkcode/{id}")]
        public IActionResult checkcode(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("Select EvaluationQuestionCode from EvaluationQuestion where EvaluationQuestionCode = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 
        
        [HttpGet("GetAll/{id}")]
        public IActionResult WhereIdEva(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select EvaluationQuestionId,EvaluationQuestionCode,CourseId,CountryCode,PartnerType,CertificateTypeId,Year,CreatedBy,CreatedDate,LanguageID from EvaluationQuestion where EvaluationQuestionId ='"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 
        
        [HttpGet("GetTemplate/{id}")]
        public IActionResult ByIdEva(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select EvaluationQuestionTemplate from EvaluationQuestion where EvaluationQuestionId ='"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                //if (ds.Tables[0].Rows.Count != 0)
                //{
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                //    ds = MainOrganizationController.EscapeSpecialChar(ds);
                //}
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from EvaluationQuestion ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(property.Name.Equals("EvaluationQuestionId")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "EvaluationQuestionId like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                } 
				if(property.Name.Equals("EvaluationQuestionCode")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "EvaluationQuestionCode like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                }
				if(property.Name.Equals("CourseId")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "CourseId like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                } 
				if(property.Name.Equals("CountryCode")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "CountryCode like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                }
                if(property.Name.Equals("CertificateTypeId")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "CertificateTypeId like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                }
                 if(property.Name.Equals("PartnerType")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "PartnerType like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                }
                
                if(property.Name.Equals("Year")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "Year like '%"+property.Value.ToString()+"%' "; 
                        i=i+1;
                    }
                }

                if(property.Name.Equals("LanguageID")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "LanguageID = "+property.Value.ToString()+" "; 
                        i=i+1;
                    }
                }  
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }
        
        

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from EvaluationQuestion;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            
            try
            {  
                var jsonTemplate = data.EvaluationQuestionTemplate.ToString().Replace("'","");
                var jsonQuestion = data.EvaluationQuestionJson.ToString().Replace("'","");
                string sql = 
                    "insert into EvaluationQuestion "+
                    "( EvaluationQuestionCode, CertificateTypeId, PartnerType, CourseId, CountryCode, EvaluationQuestionTemplate, EvaluationQuestionJson,LanguageID, "+
                    "CreatedBy, CreatedDate, Year) "+
                    "values ( '"+data.EvaluationQuestionCode+"','"+data.CertificateTypeId+"','"+data.PartnerType+"','"+data.CourseId+"','"+data.CountryCode+"','"+jsonTemplate+"','"+jsonQuestion+
                    "',"+data.LanguageID+",'"+data.CreatedBy+"',NOW(),'"+data.Year+"');";
                sqlCommand = new MySqlCommand(sql);
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            	res =
	                "{"+
	                "\"code\":\"0\","+
	                "\"message\":\"Insert Data Failed\""+
	                "}"; 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from EvaluationQuestion;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());

            sb1 = new StringBuilder();
            sb1.AppendFormat("select EvaluationQuestionId from EvaluationQuestion ORDER BY EvaluationQuestionId DESC limit 1;");
            sqlCommand = new MySqlCommand(sb1.ToString());
            ds1 = oDb.getDataSetFromSP(sqlCommand);   
            result1 = MySqlDb.GetJSONObjectString(ds1.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result1.ToString());
            o3 = JObject.Parse(result1); 
            string evaluationqid = o3["EvaluationQuestionId"].ToString();

            if(count2 > count1)
            { 

                sb = new StringBuilder();
                sb.Append("SELECT r.RoleName, EvaluationQuestionCode, CertificateTypeId, CourseId, EvaluationQuestionId, d1.KeyValue as `Year`, d2.KeyValue as `Language`,d2.`Key` AS LanguageID" +
                    " FROM EvaluationQuestion eq INNER JOIN Dictionary d1 ON eq.`Year` = d1.`Key` and d1.Parent = 'FYIndicator' INNER JOIN Dictionary d2 ON eq.LanguageID = d2.`Key` and d2.Parent = 'Languages' INNER JOIN Roles r ON eq.PartnerType = r.RoleId WHERE EvaluationQuestionId IN (SELECT MAX(EvaluationQuestionId) FROM EvaluationQuestion) LIMIT 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                string restmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject objres = JObject.Parse(restmp); 

                //string description = "Insert data EvaluationQuestion" + "<br/>" +
                //    "Evaluation Code = "+ data.EvaluationQuestionCode + "<br/>" +
                //    "Partner Type = "+ objres["RoleName"] + "<br/>" +
                //    "Language = "+ objres["Language"] + "<br/>" +
                //    "Year = "+ objres["Year"];

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.EvaluationQuestion, data.UserId.ToString(), data.cuid.ToString(), "where EvaluationQuestionId =" + evaluationqid + ""))
                {
                    res =
                   "{" +
                   "\"code\":\"1\"," +
                   "\"message\":\"Insert Data Success\"," +
                   "\"EvaluationQuestionId\":\"" + evaluationqid + "\"" +
                   "}";
                }
               
            } 
 
            return Ok(res);  
            
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from EvaluationQuestion where EvaluationQuestionId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {  
                string sql = 
                    "update EvaluationQuestion set "+ 
                    "EvaluationQuestionCode='"+data.EvaluationQuestionCode+"', "+
                    "CourseId='"+data.CourseId+"', "+
                    "CountryCode='"+data.CountryCode+"', "+
                    "CertificateTypeId="+data.CertificateTypeId + ", "+
                    // "CreatedBy='"+data.CreatedBy+"', "+
                    "Year='"+data.Year+"', "+
                    "PartnerType='"+data.PartnerType+"' "+
                    // "CreatedDate='"+data.CreatedDate+"' "+ 
                    "where EvaluationQuestionId='"+id+"'";
                sqlCommand = new MySqlCommand(sql);
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            	res =
	                "{"+
	                "\"code\":\"0\","+
	                "\"message\":\"Update Data Failed\""+
	                "}";  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from EvaluationQuestion where EvaluationQuestionId ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            o3 = JObject.Parse(result);  

            if(o3["EvaluationQuestionCode"].ToString() == data.EvaluationQuestionCode.ToString())
            { 
                sb = new StringBuilder();
                sb.Append("SELECT r.RoleName, EvaluationQuestionCode, CertificateTypeId, CourseId, EvaluationQuestionId, d1.KeyValue as `Year`, d2.KeyValue as `Language`,d2.`Key` AS LanguageID" +
                    " FROM EvaluationQuestion eq INNER JOIN Dictionary d1 ON eq.`Year` = d1.`Key` and d1.Parent = 'FYIndicator' INNER JOIN Dictionary d2 ON eq.LanguageID = d2.`Key` and d2.Parent = 'Languages' INNER JOIN Roles r ON eq.PartnerType = r.RoleId WHERE EvaluationQuestionId = '"+id+"' LIMIT 1");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                string restmp = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject objres = JObject.Parse(restmp); 

                //string description = "Update data EvaluationQuestion" + "<br/>" +
                //    "Evaluation Code = "+ data.EvaluationQuestionCode + "<br/>" +
                //    "Partner Type = "+ objres["RoleName"] + "<br/>" +
                //    "Language = "+ objres["Language"] + "<br/>" +
                //    "Year = "+ objres["Year"];

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.EvaluationQuestion, data.UserId.ToString(), data.cuid.ToString(), "where EvaluationQuestionId =" + id + ""))
                {
                    res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
                }
                
            } 
 
            return Ok(res);  
        }

        [HttpPut("UpdateTemplate/{id}")]
        public IActionResult UpdateTemplate(string id, [FromBody] dynamic data)
        {  
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from EvaluationQuestion where EvaluationQuestionId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {
                var jsonTemplate = data.EvaluationQuestionTemplate.ToString().Replace("'", "");
                var jsonQuestion = data.EvaluationQuestionJson.ToString().Replace("'", "");
                string sql = 
                    "update EvaluationQuestion set "+ 
                    "EvaluationQuestionTemplate='"+data.EvaluationQuestionTemplate+"', "+
                    "EvaluationQuestionJson='"+data.EvaluationQuestionJson+"' "+
                    "where EvaluationQuestionId='"+id+"' and LanguageID = "+data.LanguageID+"";
                //Console.WriteLine(sql);
                sqlCommand = new MySqlCommand(sql);
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{"+
                    "\"code\":\"0\","+
                    "\"message\":\"Update Data Failed\""+
                    "}";  
            }
            finally
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from EvaluationQuestion where EvaluationQuestionId =" + id + ";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                _auditLogServices.LogHistory(new Code.Models.History
                {
                    Admin= HttpContext.User.GetUserId(),
                    Date=DateTime.Now,
                    Description= "Update EvaluationQuestion.Data:"+data.ToString(),
                    Id= id
                });
                //if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.EvaluationQuestion, data.UserId.ToString(), data.cuid.ToString(), "where EvaluationQuestionId =" + id + ""))
                //{
                    res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
                //}

            }
            
          
 
            return Ok(res);  
        } 

        
        [HttpDelete("{id}/{cuid}/{UserId}/{code}")]
        public IActionResult Delete(int id, string cuid, string UserId, string code)
        { 
            Console.WriteLine(cuid);
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from EvaluationQuestion where EvaluationQuestionId  =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from EvaluationQuestion where EvaluationQuestionId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            	res =
	                "{"+
	                "\"code\":\"0\","+
	                "\"message\":\"Delete Data Failed\""+
	                "}"; 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from EvaluationQuestion where EvaluationQuestionId  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                if(o3.Count < 1){ 

                    //string description = "Delete data Survey Template Evaluation <br/>" +
                    //    "Evaluation Question Id = "+ id +"<br/>"+
                    //    "Evaluation Code = "+ code;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());

                    JObject NewData = new JObject();
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.EvaluationQuestion, UserId.ToString(), cuid.ToString(), "where EvaluationQuestionId =" + id + ""))
                    {
                        res =
                         "{" +
                         "\"code\":\"1\"," +
                         "\"message\":\"Delete Data Success\"" +
                         "}";
                    }
                    
                }
            }

            return Ok(res);
        } 

         


    }
}