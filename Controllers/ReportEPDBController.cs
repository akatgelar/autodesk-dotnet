using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;

namespace autodesk.Controllers
{
    [Route("api/ReportEPDB")]
    public class ReportEPDBController : Controller
    {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        DataSet dsTemp = new DataSet();

        public ReportEPDBController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }

        [HttpGet("SiteOrg")]
        public IActionResult SiteOrg()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT * FROM Main_organization  WHERE OrgId NOT IN ( SELECT DISTINCT OrgId From Main_site WHERE Status = 'A' ) AND Status = 'A'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("OrgNoAddress")]
        public IActionResult OrgNoAddress()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT * FROM Main_organization WHERE RegisteredCountryCode = '' AND Status = 'A'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpGet("OrphanedSites")]
        public IActionResult OrphanedSites()
        {
            string query = "";
            query += "SELECT * FROM Main_site  WHERE OrgId IS NULL OR OrgId = '' OR OrgId NOT IN (Select OrgId From Main_organization WHERE Status = 'A') AND Status = 'A'";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpGet("SitesNoAddress")]
        public IActionResult SitesNoAddress()
        {
            string query = "";
            query += "SELECT * FROM Main_site WHERE SiteCountryCode = '' AND Status = 'A'";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpGet("SitesNoContacts")]
        public IActionResult SitesNoContacts()
        {
            string query = "";
            query += "SELECT *  FROM Main_site  WHERE SiteID NOT IN (SELECT DISTINCT SiteId from SiteContactLinks WHERE Status <> 'X') AND Status = 'A'";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // belum fix "Sites with similar Address1"
        [HttpGet("SitesAddress1")]
        public IActionResult SitesAddress1()
        {
            string query = "";
            query += "SELECT co.Geo, co.Region, s1.OrgId, s1.SiteId, s1.SiteName, s2.SiteId As hiddenSiteId2, '' As MarkAsDupeSite, '' As DuplicateSiteByAddress, "+
            "s1.SiteAddress1, s1.SiteCity, co.Country ,  (SELECT CAST( GROUP_CONCAT( CONCAT_WS( '_', RoleName, Status ) SEPARATOR '<li>' ) As CHAR(100)) FROM SiteRoles sr, "+
            "Roles r WHERE SiteId = s1.SiteId AND Status <> 'X' AND sr.RoleId = r.RoleId ) As s1PTs ,  (SELECT CAST( GROUP_CONCAT( CONCAT_WS( '_', RoleName, Status ) "+
            "SEPARATOR '<li>' ) As CHAR(100)) FROM SiteRoles sr, Roles r WHERE SiteId = s2.SiteId AND Status <> 'X' AND sr.RoleId = r.RoleId ) As  s2PTs "+
            "FROM `Sites` s1, Sites s2, Countries co WHERE s1.SiteId <> s2.SiteId AND LEFT( REPLACE( s1.SiteAddress1, ' ', '' ) , 10 ) = LEFT( REPLACE( s2.SiteAddress1, ' ', '' ) , 10 )  "+
            "AND s1.SiteAddress1 IS NOT NULL  AND  s1.SiteAddress1 <> '' AND s1.SiteAddress1 <> '0' AND s1.SiteAddress1 <> 0 AND s1.Status = 'A' AND s2.Status = 'A' "+
            "AND s1.SiteCountryCode = co.CountryCode AND (s1.SiteId,s2.SiteId) NOT IN (SELECT OtherItemId, MasterItemId FROM IntegrityOk WHERE IntegrityCheck = 'DuplicateSiteByAddress') "+
            "ORDER BY s1.SiteAddress1";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // di existing tidak nampil "SiebelAddress <> SiteAddress"
        [HttpGet("SiebelSiteAddress")]
        public IActionResult SiebelSiteAddress()
        {
            string query = "";
            query += "";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }
 
        // di existing tidak nampil "Bad CSN Matches"
        [HttpGet("BadCSNMatches")]
        public IActionResult BadCSNMatches()
        {
            string query = "";
            query += "";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // belum fix "PDB Status <> Siebel Status"
        [HttpGet("PDBSiebelStatus")]
        public IActionResult PDBSiebelStatus()
        {
            string query = "";
            query += "SELECT DISTINCT Geo, co.Country, s.SiteId, SiteName, sr.Status, OrgId, sb.ACCOUNT_STATUS FROM Sites s, Countries co, SiteRoles sr, Roles r, SiebelContactData sb "+
            "WHERE sr.SiteId = s.SiteId AND sr.Status <>  'X' AND sr.RoleId = r.RoleId AND sr.CSN = sb.ACCOUNT_CSN AND s.Status = 'A' AND SiteCountryCode = co.CountryCode "+
            "AND ((sr.Status = 'A' AND sb.ACCOUNT_STATUS <> 'Active')  OR (sr.Status <> 'A' AND sb.ACCOUNT_STATUS = 'Active')) ORDER BY SiteName";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpGet("OrphanedContacts")]
        public IActionResult OrphanedContacts()
        {
            string query = "";
            query += "SELECT * FROM Contacts_All  WHERE ContactId NOT IN ( SELECT DISTINCT ContactId From SiteContactLinks WHERE Status <> 'X')  AND Status = 'A'";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // belum fix "Potential Duplicate Users by Name"
        [HttpGet("DuplicateName ")]
        public IActionResult DuplicateName()
        {
            string query = "";
            query += "SELECT c1.ContactName, c1.ContactId + c2.ContactId As hiddenIdSum, c1.ContactId As ContactId1, c2.ContactId As hiddenContactId2, "+
            "s1.SiteName As SiteName1, s2.SiteName As hiddenSiteName2, c1.EmailAddress As Email1, c2.EmailAddress As hiddenEmail2, '' As DuplicateUserByName, '' As MarkAsDupe, "+
            "c1.InstructorId As InstructorId1, c2.InstructorId As hiddenInstructorId2, (SELECT Geo FROM Countries co, Sites s WHERE s.SiteCountryCode = co.CountryCode "+
            "AND s.SiteID = c1.PrimarySiteId ) As Geo, (SELECT Region FROM Countries co, Sites s WHERE s.SiteCountryCode = co.CountryCode "+
            "AND s.SiteID = c1.PrimarySiteId ) As Region, c1.LastLogin As LastLogin1, c2.LastLogin As hiddenLastLogin2, (SELECT Count(1) "+
            "FROM LMSRegistrations WHERE LRN_CONTACT_ID = c1.ContactId) As CoursesRegistered, (SELECT Count(1) FROM LMSCapacity WHERE ContactId = c1.ContactId) As Certifications, "+
            "c1.DateAdded, c1.AddedBy, c1.DateLastAdmin, c1.LastAdminBy FROM Contacts c1, Contacts c2 , Sites s1, Sites s2 WHERE c1.ContactName = c2.ContactName "+
            "AND c1.PrimarySiteId = s1.SiteId  AND c2.PrimarySiteId = s2.SiteId AND c1.ContactId <> c2.ContactId  AND c1.Status = 'A'  AND c2.Status = 'A'  "+
            "AND c1.ContactName <> ''  AND c1.ContactName <> 'test test'  AND c1.ContactName <> '_ _'  AND (c1.ContactId,c2.ContactId) NOT IN (SELECT OtherItemId, MasterItemId "+
            "FROM IntegrityOk WHERE IntegrityCheck = 'DuplicateUserByName') ORDER BY c1.Contactname, hiddenIdSum, Geo, Region";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // di existing tidak nampil
        [HttpGet("DuplicateSiebelId")]
        public IActionResult DuplicateSiebelId()
        {
            string query = "";
            query += "";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // belum fix "Siebel Matches to verify"
        [HttpGet("SiebelMatches")]
        public IActionResult SiebelMatches()
        {
            string query = "";
            query += "SELECT c.ContactId, c.FirstName As PDBFName, c.LastName As PDBLName , scd.CONTACT_CSN As SiebelId, scd.First_Name As SiebelFName, scd.Last_Name As SiebelLName "+
            "FROM SiebelContactCSNLinks sccl , SiebelContactData scd ,  Contacts c WHERE c.ContactId = sccl.ContactId AND sccl.SiebelId = scd.Contact_CSN "+
            "AND c.FirstName <> scd.First_Name AND c.LastName <> scd.Last_Name AND c.LastName <> scd.First_Name AND c.FirstName <> scd.Last_Name "+
            "AND  (c.ContactId,scd.CONTACT_CSN) NOT IN (SELECT MasterItemId, OtherItemId FROM IntegrityOk WHERE IntegrityCheck = 'QuestionableSiebelIdMatches') ORDER BY PDBFName, PDBlName";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // "Active Users in more than one Organization"
        [HttpGet("ActiveUsers")]
        public IActionResult ActiveUsers()
        {
            string query = "";
            query += "SELECT c.ContactId, c.firstname, c.lastname, Count(DISTINCT s.OrgId) as orgs , GROUP_CONCAT( DISTINCT s.OrgId ) As OrgIds FROM Contacts_All c, "+
            "SiteContactLinks scl, Main_site s, Main_organization o WHERE c.ContactId = scl.ContactId AND  scl.SiteId = s.SiteId AND s.OrgId = o.OrgId AND c.Status = 'A' "+
            "AND s.Status = 'A' AND o.Status = 'A' GROUP By c.ContactId HAVING Count(DISTINCT o.OrgId) > 1 ORDER By c.ContactId";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // belum fix "Duplicate Email Addresses"
        [HttpGet("DuplicateEmail")]
        public IActionResult DuplicateEmail()
        {
            string query = "";
            query += "SELECT EmailAddress, Count( * ) AS Cnt, Max(LastLogin) As HasLoggedIn, GROUP_CONCAT( DISTINCT CONCAT(geo_name, ':', region_name) SEPARATOR ', ') "+
            "As GeoRegion, countries_name,  (SELECT CAST(GROUP_CONCAT( DISTINCT CONCAT(RoleCode,'_',Status)) As CHAR(100)) FROM SiteRoles sr, "+
            "Roles r WHERE sr.RoleId = r.RoleId AND c.PrimarySiteId = SiteId) As Statuses FROM Contacts_All c LEFT OUTER JOIN  Main_site s ON c.PrimarySiteId = s.SiteId "+
            "AND s.Status = 'A' LEFT OUTER JOIN Ref_countries co ON s.SiteCountryCode = co.countries_code WHERE c.EmailAddress <> ''  AND c.Status = 'A'  "+
            "GROUP BY c.EmailAddress   HAVING Count(DISTINCT ContactId) >1  ORDER BY Cnt DESC";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // "Illegal? Email Addresses"
        [HttpGet("IllegalEmail")]
        public IActionResult IllegalEmail()
        {
            string query = "";
            query += "SELECT EmailAddress As AddressToCheck  FROM Contacts_All  WHERE EmailAddress <> ''  AND Status = 'A'  "+
            "AND EmailAddress NOT REGEXP '[a-zA-Z]*@[a-zA-Z]*' ORDER BY DateLastAdmin";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // "Bad Primary Site Id"
        [HttpGet("BadPrimarySiteId")]
        public IActionResult BadPrimarySiteId()
        {
            string query = "";
            query += "SELECT ContactId, ContactName, (SELECT Count(1) FROM SiteContactLinks WHERE ContactId = c.ContactId AND Status = 'A') As Cnt  "+
            "FROM Contacts_All c  WHERE c.Status = 'A'  AND PrimarySiteId <> ''  AND  PrimarySiteId NOT   IN (  SELECT SiteId  FROM SiteContactLinks  "+
            "WHERE ContactId = c.ContactId  AND STATUS = 'A'  ) ORDER BY ContactName";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // "Missing Primary Site Id"
        [HttpGet("MissingPrimarySiteId")]
        public IActionResult MissingPrimarySiteId()
        {
            string query = "";
            query += "SELECT ContactId As ToFix_ContactId, ContactName, DateAdded , (SELECT UCase(GROUP_CONCAT(scl.SiteId)) "+
            "FROM SiteContactLinks scl, Main_site s WHERE s.SiteID = scl.SiteId AND scl.ContactId = c.ContactId AND scl.STATUS = 'A' "+
            "AND  s.STATUS = 'A') AS PrimarySiteId  FROM Contacts_All c  WHERE c.Status = 'A'  AND PrimarySiteId = ''  "+
            "AND ContactId  IN (  SELECT ContactId  FROM SiteContactLinks scl, Main_site s  WHERE s.SiteID = scl.SiteId "+
            "AND  scl.ContactId = c.ContactId AND scl.STATUS = 'A' AND s.STATUS = 'A'  )";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // belum fix "Missing First or Last Name"
        [HttpGet("MissingName")]
        public IActionResult MissingName()
        {
            string query = "";
            query += "SELECT ContactId, Trim(FirstName) As FirstName, Trim(LastName) As LastName, Trim(ContactName), Description  "+
            "FROM Contacts_All c, History h  WHERE c.Status = 'A'  AND c.ContactId = h.Id  AND h.Description LIKE '%MIGRATION</%'  "+
            "AND  (FirstName = ''  OR FirstName IS NULL  OR LastName = ''  OR LastName IS NULL)  ORDER BY Trim(ContactName)";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // belum fix "Duplicate (hidden) Users with LMS History"
        [HttpGet("DuplicateHistory")]
        public IActionResult DuplicateHistory()
        {
            string query = "";
            query += "SELECT c.ContactId, c.ContactName, Count(COURSE_CODE) As RegisteredCourses, co.Geo, co.Region, s.OrgId, s.SiteId, "+
            "s.SiteName, co.Country, s.SiteCity FROM Contacts_All c, Main_site s, Ref_countries co, LMSRegistrations lms WHERE  c.PrimarySiteId = s.SiteId "+
            "AND s.SiteCountryCode = co.CountryCode AND c.ContactId = lms.LRN_CONTACT_ID AND s.Status = 'A' AND c.Status = 'D' "+
            "GROUP BY LRN_CONTACT_ID ORDER BY SiteName, ContactName";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // belum fix "Duplicate (hidden) Users with LMS Certifications"
        [HttpGet("DuplicateCertifications")]
        public IActionResult DuplicateCertifications()
        {
            string query = "";
            query += "SELECT c.ContactId, c.ContactName, Count(1) As Certifications, co.geo_name, co.region_name, s.OrgId, s.SiteId, s.SiteName, co.Country, s.SiteCity "+
            "FROM Contacts_All c, Main_site s, Ref_countries co, LMSCapacity lms WHERE  c.PrimarySiteId = s.SiteId AND s.SiteCountryCode = co.CountryCode "+
            "AND c.ContactId = lms.ContactId AND s.Status = 'A' AND c.Status = 'D' GROUP BY lms.ContactId ORDER BY SiteName, ContactName";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // "Duplicate Attributes"
        [HttpGet("DuplicateAttributes")]
        public IActionResult DuplicateAttributes()
        {
            string query = "";
            query += "SELECT j.ActivityId, ja.ActivityName, ja.ActivityType, j.ParentId  , Count(j.JournalId) As Count   FROM Journals j, JournalActivities ja  WHERE j.Status <> 'X'   AND ja.Status <> 'X'   AND ja.IsUnique = 'Y'   AND  j.ActivityId = ja.ActivityId  GROUP BY j.ActivityId, j.ParentId  HAVING Count(j.JournalId) > 1";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpGet("DuplicateAttributes1")]
        public IActionResult DuplicateAttributes1()
        {
            string query = "";
            query += "SELECT CONCAT('<tr><td>',ActivityName,'</td><td>',ActivityType,'</td></tr>')  FROM JournalActivities  WHERE Status <> 'X'  "+
            "AND IsUnique = 'Y'   ORDER BY ActivityName";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpGet("DuplicateAttributes2")]
        public IActionResult DuplicateAttributes2()
        {
            string query = "";
            query += "SELECT CONCAT('<tr><td>',ActivityName,'</td><td>',ActivityType,'</td></tr>')  FROM JournalActivities  WHERE Status <> 'X'  "+
            "AND (IsUnique <> 'Y' OR IsUnique is null)  ORDER BY ActivityName";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // "Illegal State/Province/Province values"
        [HttpGet("IllegalState")]
        public IActionResult IllegalState()
        {
            string query = "";
            query += "(SELECT distinct OrgId, '' As SiteId, '' As ContactId, RegisteredStateProvince, RegisteredCountryCode As CountryCode "+
            "FROM Main_organization WHERE RegisteredCountryCode IN ('CA','US','JP') AND RegisteredStateProvince <> ''  "+
            "AND  RegisteredStateProvince NOT IN (SELECT StateCode From States) )  UNION (SELECT distinct OrgId, SiteId, '', SiteStateProvince, SiteCountryCode As CountryCode "+
            "FROM Main_site WHERE SiteCountryCode IN ('CA','US','JP') AND  SiteStateProvince <> ''  AND SiteStateProvince NOT IN (SELECT StateCode From States) ) "+
            "ORDER BY CountryCode, OrgId, SiteId";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // di existing tidak nampil "Instructors whose name changed during the ATC Migration"
        [HttpGet("InstructorsWhose")]
        public IActionResult InstructorsWhose()
        {
            string query = "";
            query += "";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        // di existing tidak nampil "ATC Instructors missing "Active for on-line eval" = "On""
        [HttpGet("ATCInstructors")]
        public IActionResult ATCInstructors()
        {
            string query = "";
            query += "";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpPost("ReportHistory")]
        public IActionResult ReportHistory([FromBody] dynamic data)

        {     
            string query = "select Date, Id, replace( replace( replace( Description, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as Description, Admin from History ";
            int i = 0;
            JObject json = JObject.FromObject(data);
            
            foreach (JProperty property in json.Properties())
            { 
                if(property.Name.Equals("OrgId") || property.Name.Equals("SiteId") || property.Name.Equals("ContactId")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";}
                        query += "Description like '%" + property.Value.ToString() + "%' ";
                        //query += "Id = '"+property.Value.ToString()+ "' AND Description like '%"+ property.Value.ToString() + "%' ";  
                        i =i+1;
                    }
                }
                if(property.Name.Equals("TextToMatch")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "Description like '%"+property.Value.ToString()+"%' ";  
                        i=i+1;
                    }
                }
                if(property.Name.Equals("Admin")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "Admin like '%"+property.Value.ToString()+"%' ";  
                        i=i+1;
                    }
                }
                if(property.Name.Equals("LastXDays")){   
                    if((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() != "0")){
                        if(i==0){query += " where ";}
                        if(i>0){query += " and ";} 
                        query += "Date >= NOW() - INTERVAL "+property.Value.ToString()+" DAY ";  
                        i=i+1;
                    }
                }
            } 

            query += "ORDER BY Date DESC ";

            if(i == 0){
                query+="LIMIT 2115";
            }
 
            try
            {  
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }

            }
            catch
            {
                result = "Not found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }
    }
}