﻿using autodesk.Code;
using autodesk.Code.MergeServices;
using autodesk.Code.MergeServices.Dto;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Threading.Tasks;

namespace autodesk.Controllers
{
    [Produces("application/json")]
    [Route("api/Merge")]
    public class MergeController : ControllerBase
    {
        private readonly IMergeServices _mergeServices;
        private readonly IEmailService _emailService;
        public MergeController(IMergeServices mergeServices, IEmailService emailService)
        {
            _mergeServices = mergeServices;
            _emailService = emailService;
        }
        [Route("GetOrg")]
        [HttpGet]
        public async Task<ResponseModel<OrgMergeDto>> GetOrg(string orgId)
        {
            var result = await Task.Run(() => _mergeServices.GetOrg(orgId));
            if (result != null)
            {
                return new ResponseModel<OrgMergeDto> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = result };
            }
            return new ResponseModel<OrgMergeDto> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = null };
        }

        [Route("MergeOrg")]
        [HttpPost]
        public async Task<ResponseModel<object>> MergeOrg([FromBody]OrgMergeDto orgMergeDto, [FromQuery]string newOrgId)
        {
            var result = await Task.Run(() => _mergeServices.MergeOrg(orgMergeDto, newOrgId));
            if (result)
            {
                return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = true };
            }
            return new ResponseModel<object> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = false };
        }
        [Route("MarkOrgDuplicate")]
        [HttpPut]
        public async Task<ResponseModel<object>> MarkOrgDuplicate([FromQuery]string orgId)
        {
            var result = await Task.Run(() => _mergeServices.MarkOrgDuplicate(orgId));
            if (result)
            {
                return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = true };
            }
            return new ResponseModel<object> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = false };
        }

        [Route("GetSite")]
        [HttpGet]
        public async Task<ResponseModel<SiteMergeDto>> GetSite(string siteId)
        {
            var result = await Task.Run(() => _mergeServices.GetSite(siteId));
            if (result != null)
            {
                return new ResponseModel<SiteMergeDto> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = result };
            }
            return new ResponseModel<SiteMergeDto> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = null };
        }
        [Route("MergeSite")]
        [HttpPost]
        public async Task<ResponseModel<object>> MergeSite([FromBody]SiteMergeDto siteMergeDto, [FromQuery]string newSiteId)
        {
            var result = await Task.Run(() => _mergeServices.MergeSite(siteMergeDto, newSiteId));
            if (result)
            {
                return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = true };
            }
            return new ResponseModel<object> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = false };
        }
        [Route("MarkSiteDuplicate")]
        [HttpPut]
        public async Task<ResponseModel<object>> MarkSiteDuplicate([FromQuery]string siteId)
        {
            var result = await Task.Run(() => _mergeServices.MarkSiteDuplicate(siteId));
            if (result)
            {
                return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = true };
            }
            return new ResponseModel<object> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = false };
        }

        [Route("GetContact")]
        [HttpGet]
        public async Task<ResponseModel<ContactMergeDto>> GetContact(string contactId)
        {
            var result = await Task.Run(() => _mergeServices.GetContact(contactId));
            if (result != null)
            {
                return new ResponseModel<ContactMergeDto> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = result };
            }
            return new ResponseModel<ContactMergeDto> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = null };
        }
        [Route("MergeContact")]
        [HttpPost]
        public async Task<ResponseModel<object>> MergeContact([FromBody]ContactMergeDto contactMergeDto, [FromQuery]string newContactId)
        {
            var result = await Task.Run(() => _mergeServices.MergeContact(contactMergeDto, newContactId));
            if (result)
            {
                return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = true };
            }
            return new ResponseModel<object> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = false };
        }
        [Route("MarkContactDuplicate")]
        [HttpPut]
        public async Task<ResponseModel<object>> MarkContactDuplicate([FromQuery]string contactId)
        {
            var result = await Task.Run(() => _mergeServices.MarkContactDuplicate(contactId));
            if (result)
            {
                return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = true };
            }
            return new ResponseModel<object> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = false };
        }

        [Route("GetStudent")]
        [HttpGet]
        public async Task<ResponseModel<StudentMergeDto>> GetStudent(int studentId)
        {
            var result = await Task.Run(() => _mergeServices.GetStudent(studentId));
            if (result != null)
            {
                return new ResponseModel<StudentMergeDto> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = result };
            }
            return new ResponseModel<StudentMergeDto> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = null };
        }
        [Route("MergeStudent")]
        [HttpPost]
        public async Task<ResponseModel<object>> MergeStudent([FromBody]StudentMergeDto studentMergeDto, [FromQuery]int newStudentId)
        {
            var result = await Task.Run(() => _mergeServices.MergeStudent(studentMergeDto, newStudentId));
            if (result)
            {
                return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = true };
            }
            return new ResponseModel<object> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = false };
        }
        [Route("MarkStudentDuplicate")]
        [HttpPut]
        public async Task<ResponseModel<object>> MarkStudentDuplicate([FromQuery]int studentId)
        {
            var result = await Task.Run(() => _mergeServices.MarkStudentDuplicate(studentId));
            if (result)
            {
                return new ResponseModel<object> { Code = (int)HttpStatusCode.OK, Message = AutodeskConst.ResponseMessage.Success, Data = true };
            }
            return new ResponseModel<object> { Code = (int)HttpStatusCode.BadRequest, Message = AutodeskConst.ResponseMessage.Fail, Data = false };
        }
    }
}