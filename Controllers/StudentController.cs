﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;

using autodesk.Code;
using autodesk.Code.CommonServices;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using autodesk.Code.AuditLogServices;
using autodesk.Code.Models;

// using autodesk.Models;


namespace autodesk.Controllers
{
    [Route("api/Student")]
    public class StudentController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        private Audit _audit;
        private ICommonServices _commonServices;
        private readonly IAuditLogServices _auditLogServices;

        public StudentController(MySqlDb sqlDb, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _commonServices = new CommonServices();
            _audit = new Audit(oDb);
            _auditLogServices = auditLogServices;
        }

        [HttpGet("Select/{value}")]
        public IActionResult Select(int value)
        {
            try
            {
                sb = new StringBuilder();
                if (value == 0)
                {
                    sb.Append("SELECT t1.StudentID, replace( replace( replace( t1.Firstname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Firstname, replace( replace( replace( t1.Lastname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Lastname, replace( replace( replace( t1.Email, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Email, replace( t1.Phone, '\\\\','\\\\\\\\' ) AS Phone FROM tblStudents t1 inner join TerritoryCountry t2 on t1.CountryID=t2.CountryCode WHERE t1.`Status` != 'X' AND t1.FirstName != '' AND t1.LastName != '' AND t1.Email != '' LIMIT 5000");

                }
                else
                {
                    sb.Append("SELECT t1.StudentID, replace( replace( replace( t1.Firstname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Firstname, replace( replace( replace( t1.Lastname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Lastname, replace( replace( replace( t1.Email, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Email, replace( t1.Phone, '\\\\','\\\\\\\\' ) AS Phone FROM tblStudents t1 inner join TerritoryCountry t2 on t1.CountryID=t2.CountryCode WHERE t2.TerritoryId=" + value + " and t1.`Status` != 'X' AND t1.FirstName != '' AND t1.LastName != '' AND t1.Email != '' LIMIT 5000");

                }
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }
        [HttpGet("SelectByID/{value}")]
        public IActionResult SelectByID(int value)
        {
            try
            {
                sb = new StringBuilder();

                //  sb.Append("SELECT t1.StudentID, replace( replace( replace( t1.Firstname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Firstname, replace( replace( replace( t1.Lastname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Lastname, replace( replace( replace( t1.Email, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Email, replace( t1.Phone, '\\\\','\\\\\\\\' ) AS Phone FROM tblStudents t1 inner join TerritoryCountry t2 on t1.CountryID=t2.CountryCode WHERE t2.TerritoryId=" + value + " and t1.`Status` != 'X' AND t1.FirstName != '' AND t1.LastName != '' AND t1.Email != '' LIMIT 5000");
                sb.Append("select StudentID, Status,(select count(*) from CourseStudent where StudentID=tblStudents.StudentID) CourseCount, Email, Email2, DATE_FORMAT(DateBirth, '%m/%d/%Y') AS DateBirth, Salutation, Firstname, Lastname, PrimaryIndustryId, CountryID, LanguageID, DATE_FORMAT(ExpetationGradDate, '%m/%d/%Y') AS ExpetationGradDate, StatusLevel ,countries_code from tblStudents left join Ref_countries on Ref_countries.countries_code = tblStudents.CountryID   where firstname = (select firstname from tblStudents where StudentID =" + value + ") and lastname =(select lastname from tblStudents where StudentID =" + value + ") and Email =(select Email from tblStudents where StudentID =" + value + ") order by Status");

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }
        [HttpGet("UpdatebyId/{value}")]
        public IActionResult UpdatebyId(int value)
        {
            //  JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(" SET SQL_SAFE_UPDATES=0; update CourseStudent set StudentID =" + value + "  where StudentID in (select studentId from tblStudents where   firstname = (select firstname from tblStudents where StudentID =" + value + ") and lastname = (select lastname from tblStudents where StudentID = " + value + ")); "
                              + " update EvaluationAnswer set StudentID = " + value + "  where StudentID in (select studentId from tblStudents where firstname = (select firstname from tblStudents where StudentID = " + value + ") and lastname = (select lastname from tblStudents where StudentID = " + value + ")) ; "
                             + " update EvaluationReport set StudentID =  " + value + "  where StudentID in (select studentId from tblStudents where firstname = (select firstname from tblStudents where StudentID =  " + value + ") and  lastname = (select lastname from tblStudents where StudentID =  " + value + "));  SET SQL_SAFE_UPDATES=1;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");
            }
            finally
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }


            return Ok(res);
        }

        [HttpGet("FindStudent/{value}")]
        public IActionResult FindStudent(string value)
        {
            JObject resultfinal;
            try
            {
                string query = "";
                query = "SELECT StudentID, replace( replace( replace( Firstname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Firstname, replace( replace( replace( Lastname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Lastname, replace( replace( replace( Email, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Email, replace( Phone, '\\\\','\\\\\\\\' ) AS Phone, " +
                "CONCAT(StudentID,' | ',replace( replace( replace( Firstname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ),' ',replace( replace( replace( Lastname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ),' | ',replace( replace( replace( Email, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' )) AS Student " +
                "FROM tblStudents WHERE (";
                Regex re = new Regex(@"\d+");
                Match m = re.Match(value);
                if (m.Success)
                {
                    query += " StudentID like " + Convert.ToInt32(value) + " OR ";
                }
                query += "FirstName like '%" + value + "%' OR LastName like '%" + value + "%' OR Email like '%" + value + "%') AND `Status` = 'A' AND FirstName != '' AND LastName != '' AND Email != ''";
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                    "{" +
                    "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }

            return Ok(resultfinal);
        }

        [HttpGet("GetStudentByCountry/{id}")]
        public IActionResult GetStudentByCountry(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT StudentID, replace( replace( replace( Firstname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Firstname, replace( replace( replace( Lastname, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS Lastname, replace( Email, '\\\\','\\\\\\\\' ) AS Email, replace( Phone, '\\\\','\\\\\\\\' ) AS Phone FROM tblStudents WHERE CountryID = '" + id + "' AND STATUS != 'X'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("PendingTerminated")]
        public IActionResult PendingTerminated()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select *,countries_name from tblStudents left join Ref_countries on Ref_countries.countries_code = tblStudents.CountryID where tblStudents.`Status` != 'X' and tblStudents.`Status` != 'A' group by StudentID");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetStudent/{id}")]
        public IActionResult WhereId(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select StudentID, Status, Email, Email2, DATE_FORMAT(DateBirth, '%m/%d/%Y') AS DateBirth, Salutation, Firstname, Lastname, PrimaryIndustryId, CountryID, LanguageID, DATE_FORMAT(ExpetationGradDate, '%m/%d/%Y') AS ExpetationGradDate, StatusLevel ,countries_code from tblStudents left join Ref_countries on Ref_countries.countries_code = tblStudents.CountryID where StudentID = '" + id + "'");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetStudentCourse_1/{evaluation_answer_id}")]
        public IActionResult StudentCourse1(string evaluation_answer_id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "SELECT CONCAT(FirstName,' ',LastName) AS StudentName, Company, Address,Phone,Email,DATE_FORMAT(c.CompletionDate,'%d-%M-%Y') AS CourseDate," +
                    "`Update`,Essentials,Intermediate,Advanced,Customized,ctl.Other AS OtherCTL, ctl.`Comment` AS CommentCTL, CONCAT(p.productName,' ',pv.Version) AS PrimaryProduct," +
                    "HoursTraining,InstructorLed,`Online`,Autodesk,AAP,ATC,Independent,IndependentOnline,ATCOnline,ctm.Other AS OtherCTM, ctm.`Comment` AS CommentCTM,EvaluationAnswerJson,EvaluationQuestionTemplate FROM EvaluationAnswer ea " +
                    "INNER JOIN Courses c ON ea.CourseId = c.CourseId " +
                    "INNER JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId " +
                    "INNER JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId " +
                    "INNER JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId " +
                    "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId " +
                    "INNER JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
                    "INNER JOIN Products p ON pv.productId = p.productId " +
                    "INNER JOIN tblStudents ts ON ea.StudentId = ts.StudentId " +
                    "INNER JOIN EvaluationQuestion eq ON ea.EvaluationQuestionCode = eq.EvaluationQuestionCode " +
                    "WHERE ea.EvaluationAnswerId = " + evaluation_answer_id + " GROUP BY EvaluationAnswerId"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetStudentCourse/{obj}")]
        public IActionResult GSC(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "SELECT CONCAT(ts.Firstname,' ',ts.Lastname) AS StudentName, ts.Company, ts.Address, ts.Phone, ts.Email, DATE(c.StartDate) AS CourseDate, c.ContactId," +
            "CASE WHEN ctl.Update = 1 THEN 'Update' END AS UpdateLvl," +
            "CASE WHEN ctl.Essentials = 1 THEN 'Level 1: Essentials' END AS Essens," +
            "CASE WHEN ctl.Intermediate = 1 THEN 'Level 2: Intermediate' END AS Intermed," +
            "CASE WHEN ctl.Advanced = 1 THEN 'Level 3: Advanced' END AS Adv," +
            "CASE WHEN ctl.Customized = 1 THEN 'Customized' END AS Cust," +
            "CASE WHEN ctl.Other = 1 THEN CONCAT('Other:',' ',ctl.`Comment`) END AS OtherCTL," +
            "CONCAT(p.productName,' ',pv.Version) AS PrimaryProduct," +
            "CONCAT(p.productName,' ',pv.Version) AS SecondaryProduct," +
            "c.HoursTraining," +
            "CASE WHEN ctf.InstructorLed = 1 THEN 'Instructor-led in the classroom' END AS InstLed," +
            "CASE WHEN ctf.Online = 1 THEN 'Online or e-learning' END AS InstOnline," +
            "CASE WHEN ctm.Autodesk = 1 THEN 'Autodesk Official Courseware (any Autodesk branded material)' END AS Autodesk," +
            "CASE WHEN ctm.AAP = 1 THEN 'Autodesk Authors and Publishers (AAP) Program Courseware' END AS AAP," +
            "CASE WHEN ctm.ATC = 1 THEN 'Course material developed by the ATC' END AS ATC," +
            "CASE WHEN ctm.Independent = 1 THEN 'Course material developed by an independent vendor or instructor' END AS Independent," +
            "CASE WHEN ctm.IndependentOnline = 1 THEN 'Online e-learning course material developed by an independent vendor or instructor' END AS IndependentOnline," +
            "CASE WHEN ctm.ATCOnline = 1 THEN 'Online e-learning course material developed by the ATC' END AS ATCOnline," +
            "CASE WHEN ctm.Other = 1 THEN CONCAT('Other:',' ',ctm.`Comment`) END AS OtherCTM," +
            "ea.EvaluationQuestionCode, ea.EvaluationAnswerJson " +
            "FROM tblStudents ts, Courses c, CourseStudent cs, CourseTeachingLevel ctl, CourseTrainingFormat ctf, CourseTrainingMaterial ctm, ProductVersions pv, Products p, CourseSoftware cp, EvaluationAnswer ea " +
            "WHERE cs.StudentID = ts.StudentID AND cs.CourseId = c.CourseId AND c.CourseId = ctl.CourseId AND c.CourseId = ctf.CourseId AND c.CourseId = ctm.CourseId AND cp.productId = p.productId AND cp.ProductVersionsId = pv.ProductVersionsId " +
            "AND pv.productId = p.productId AND cp.productsecondaryId = p.productId AND cp.ProductVersionsSecondaryId = pv.ProductVersionsId AND pv.productId = p.productId AND ea.CourseId = c.CourseId AND ea.StudentId = cs.StudentID ";

            //I'm not sure about Primary & Secondary Product

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("StudentID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "ts.StudentID = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
            }

            query += " GROUP BY ts.StudentID";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            return Ok(result);
        }

        //get by email
        [HttpGet("{email}")]
        public IActionResult WhereEmail(string email)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select StudentID, Firstname, Lastname, Email, Phone from tblStudents where Email = '" + email + "' AND Status='A'");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }
        private class StudentSearchDto
        {
            public string SL_TerritoryId { get; set; }
            public string SL_StudentName { get; set; }
            public string SL_StudentId { get; set; }
            public string SL_StudentEmail { get; set; }
        }
        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            var jsonObj = JsonConvert.DeserializeObject<StudentSearchDto>(obj);
            if (string.IsNullOrEmpty(jsonObj.SL_StudentId) && string.IsNullOrEmpty(jsonObj.SL_StudentName) && string.IsNullOrEmpty(jsonObj.SL_StudentEmail))
            {
                string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Require input at least Student Id or Student Name or Student Email. \"" +
                "}";
                return Ok(res);
            }

            string query = "";

            query += "SELECT StudentID,  Firstname, Lastname, replace( replace( Email, '\"' , '\\\\\"' ), '\\\\\"' , '\\\"' ) AS Email, replace( Phone, '\"' , '\\\\\"' ) AS Phone from tblStudents left join TerritoryCountry on tblStudents.CountryID = TerritoryCountry.CountryCode ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("SL_TerritoryId"))
                {
                    //if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    //{
                    //    if (i == 0) { query += " where "; }
                    //    if (i > 0) { query += " and "; }
                    //    query += "TerritoryCountry.TerritoryId = '" + property.Value.ToString() + "' ";
                    //    i = i + 1;
                    //}
                }
                if (property.Name.Equals("StudentID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.StudentID = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("StateID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.StateID = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Firstname"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.Firstname = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Lastname"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.Lastname = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                //if (property.Name.Equals("CountryID"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        if (i == 0) { query += " where "; }
                //        if (i > 0) { query += " and "; }
                //        query += "CountryID = '" + property.Value.ToString() + "' ";
                //        i = i + 1;
                //    }
                //}

                if (property.Name.Equals("DateBirth"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.DateBirth = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SL_StudentId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.StudentID like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SL_StudentName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "CONCAT(tblStudents.Firstname,' ',tblStudents.Lastname) like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SL_StudentEmail"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.Email like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

            }

            if (i == 0) { query += " where "; }
            if (i > 0) { query += " and "; }
            query += "tblStudents.Status != 'X' group by StudentID";
            // Console.WriteLine(query);

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("ForgotPwdStudent")]
        public IActionResult ForgotPwdStudent([FromBody] dynamic data)
        {
           
            string query = "";
            query += "SELECT StudentID,  Firstname, Lastname, replace( replace( Email, '\"' , '\\\\\"' ), '\\\\\"' , '\\\"' ) AS Email, replace( Phone, '\"' , '\\\\\"' ) AS Phone from tblStudents left join TerritoryCountry on tblStudents.CountryID = TerritoryCountry.CountryCode ";
            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {

                if (property.Name.Equals("Firstname"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.Firstname = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Lastname"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.Lastname = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CountryID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "tblStudents.CountryID = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
            }

            if (i == 0) { query += " where "; }
            if (i > 0) { query += " and "; }
            query += "tblStudents.Status = 'A' group by StudentID";
            // Console.WriteLine(query);

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }


        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from tblStudents;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1);

            try
            {
                sb = new StringBuilder();
                // sb.AppendFormat(
                //     "insert into tblStudents "+
                //     "( Firstname, Lastname, Company, Address, Address2, City, CountryID, StateID, PostalCode, Phone, Email, Password, Password_plain, LanguageID, ExternalID) "+
                //     "values ( '"+data.Firstname+"','"+data.Lastname+"','"+data.Company+"','"+data.Address+"','"+data.Address2+"','"+data.City+"','"+data.CountryID+"','"+data.StateID+"','"+data.PostalCode+"','"+data.Phone+"','"+data.Email+"','"+data.Password+"','"+data.Password_plain+"','"+data.LanguageID+"','"+data.ExternalID+"');"
                // ); /* remove password_plain */
                sb.AppendFormat(
                    "insert into tblStudents " +
                    "( Firstname, Lastname, Company, Address, Address2, City, CountryID, StateID, PostalCode, Phone, Email, Password, LanguageID, ExternalID) " +
                    "values ( '" + data.Firstname + "','" + data.Lastname + "','" + data.Company + "','" + data.Address + "','" + data.Address2 + "','" + data.City + "','" + data.CountryID + "','" + data.StateID + "','" + data.PostalCode + "','" + data.Phone + "','" + data.Email + "','" + data.Password + "','" + data.LanguageID + "','" + data.ExternalID + "');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from tblStudents;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            if (count2 > count1)
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
            }

            return Ok(res);

        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from tblStudents where StudentID = '" + id + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update tblStudents set " +
                    "Firstname='" + data.Firstname + "', " +
                    "Lastname='" + data.Lastname + "', " +
                    //  "Company='"+data.Company+"', "+
                    //  "Address='"+data.Address+"', "+
                    //  "Address2='"+data.Address2+"', "+
                    // "City='"+data.City+"', "+
                    // "CountryID='"+data.CountryID+"', "+
                    // "StateID='"+data.StateID+"', "+
                    // "PostalCode='"+data.PostalCode+"', "+
                    // "Phone='"+data.Phone+"', "+
                    "Email='" + data.Email + "'" +
                    //  "LanguageID='"+data.LanguageID+"', "+
                    //  "ExternalID='"+data.ExternalID+"' "+ 
                    " where StudentID='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from tblStudents where StudentID =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            o3 = JObject.Parse(result);

            if (o3["Firstname"].ToString() == data.Firstname.ToString())
            {

                //string description = "Update data Student <br/>" +
                //        "Student Id = " + id + " <br/>" +
                //        "Student Name = " + data.Firstname + " " + data.Lastname + " <br/>" +
                //        "Email = " + data.Email;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.tblStudents, data.UserId.ToString(), data.cuid.ToString(), " where StudentID = " + id + "");

                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpPut("TerminatedAdmin/{id}")]
        public IActionResult TerminatedAdmin(int id, [FromBody] dynamic data)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from tblStudents where StudentID =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {
                // sb = new StringBuilder();
                // sb.AppendFormat(
                //     "Delete tblStudents set "+ 
                //     "Status='X' "+ 
                //     "where StudentID='"+id+"'"
                // ); 
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("delete from tblStudents where StudentID = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("delete from CourseStudent where StudentID = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("delete from EvaluationAnswer where StudentId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Delete Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from tblStudents where StudentID =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);

            if (o3.Count < 1)
            {
                //string description = "Terminate Account Student <br/>" +
                //        "Student Id = " + id;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                string tmpTable = AutodeskConst.AuditInfoEnum.tblStudents +", "+ AutodeskConst.AuditInfoEnum.CourseStudent + ", " + AutodeskConst.AuditInfoEnum.EvaluationAnswer;
                JObject NewData = new JObject();
                _audit.AuditLog(OldData, NewData, tmpTable, data.UserId.ToString(), data.cuid.ToString(), " where StudentID = " + id + "");

                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Delete Data Success\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpPut("Delete/{id}")]
        public IActionResult Delete(int id, [FromBody] dynamic data)
        {

            JObject o3;
            int isDeleted = 0;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            try
            {
                sb = new StringBuilder();
                //sb.AppendFormat("delete from tblStudents where StudentID = '"+id+"'");
                sb.AppendFormat("update tblStudents set Status = 'X' where StudentID = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                isDeleted = oDb.temporaryUpdateFunction(sqlCommand);
            }
            catch
            {
            }
            finally
            {
                //sb = new StringBuilder();
                ////sb.AppendFormat("select * from tblStudents where StudentID  ="+id+";");
                //sb.AppendFormat("select * from tblStudents where StudentID  =" + id + " and Status<>'X'; ");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);

                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                //o3 = JObject.Parse(result);  
                //Console.WriteLine(o3.Count);
                if (isDeleted > 0)
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                    var admin = HttpContext.User.GetUserId();
                    _auditLogServices.LogHistory(new History { Admin = admin, Date = DateTime.Now, Description = "Delete Student <br/> StudentID : " + id, Id = id.ToString() });
                }
                else
                {
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Something went wrong while processing your request, please try again later.\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        [HttpPut("UpdateProfile/{id}")]
        public IActionResult UpdateProfile(int id, [FromBody] dynamic data)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update tblStudents set " +
                    "Firstname='" + data.FirstName + "', " +
                    "Lastname='" + data.LastName + "', " +
                    "Salutation='" + data.Salutation + "', " +
                    // "Gender='" + data.Gender + "', " +
                    // "Company='" + data.Company + "', " +
                    "PrimaryIndustryId='" + data.PrimaryIndustryId + "', " +
                    // "Address='" + data.Address1 + "', " +
                    // "Address2='" + data.Address2 + "', " +
                    // "City='" + data.City + "', " +
                    "CountryID='" + data.CountryCode + "', " +
                    // "StateID='" + data.StateProvince + "', " +
                    // "StateProvince='" + data.StateProvince + "', " + 
                    // "PostalCode='" + data.PostalCode + "', " +
                    // "TelephoneCode='" + data.TelephoneCode + "', " +
                    // "Phone='" + data.Telephone1 + "', " +
                    // "MobileCode='" + data.MobileCode + "', " +
                    // "Mobile='" + data.Mobile1 + "', " +
                    // "LanguageID='" + data.LanguageID + "', " +
                    // "ExternalID='" + data.ExternalID + "', " +
                    "Email2='" + data.EmailAddress2 + "', " +
                    "DateBirth='" + data.DateBirth + "', " +
                    "ExpetationGradDate='" + data.EGD + "', " +
                    "StatusLevel='" + data.StatusLevel + "', " +
                    "LanguageID=" + data.PrimaryLanguage + " " +
                    "where StudentID='" + id + "'"
                );
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from tblStudents where StudentID =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);

            if (o3["Firstname"].ToString() == data.FirstName.ToString())
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpPut("ChangePassword/{id}")]
        public IActionResult ChangePassword(int id, [FromBody] dynamic data)
        {
            JObject contactpassword;
            JObject contactpasswordnew;
            string newPasswordMd5String = "";
            string oldPasswordMD5String = "";
            int loginCount = 0;
            Boolean success = false;
            string res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Change Password Failed, Current Password is Wrong\"" +
                    "}";
            if (data != null)
            {
                try
                {

                    sb = new StringBuilder();

                    sb.AppendFormat("select `password`,LoginCount from tblStudents where StudentID = '" + id + "'");
                    // Console.WriteLine(sb.ToString());
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    contactpassword = JObject.Parse(result);
                    loginCount = Convert.ToInt32(contactpassword["LoginCount"].ToString()) + 1;
                    string currentpass = data["currentpassword"].ToString();
                    newPasswordMd5String = _commonServices.EncrypttoMd5(data["password"].ToString());

                    if (currentpass.Length > 25)
                    {
                        success = true;
                    }
                    else
                    {

                        oldPasswordMD5String = _commonServices.EncrypttoMd5(currentpass);


                        if (oldPasswordMD5String.Equals(contactpassword["password"].ToString()))
                        {

                            success = true;

                        }
                        else
                        {
                            success = false;
                            res =
                                "{" +
                                "\"code\":\"0\"," +
                                "\"message\":\"Change Password Failed, Current Password is Wrong\"" +
                                "}";
                        }
                    }


                }
                catch
                {
                    success = false;
                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Oops!, Something went wrong while processing your request, please contact system administrator.\"" +
                        "}";


                }
                finally
                {
                    if (success == true)
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "update tblStudents set " +
                            "`Password` = '" + newPasswordMd5String + "', " +

                            "LoginCount = " + loginCount + " " +
                            "where StudentID = '" + id + "'"
                        );
                        #region Commented Genius Code
                        //"`Password` = CASE WHEN (md5('" + data.currentpassword + "') = '" + contactpassword["password"].ToString() + "' OR '"+data.currentpassword+"' = '" + contactpassword["password"].ToString() + "') THEN md5('" + data.password + "') ELSE '" + contactpassword["password"].ToString() + "' END " +
                        #endregion
                        sqlCommand = new MySqlCommand(sb.ToString());
                        var isUpdate = oDb.temporaryUpdateFunction(sqlCommand);
                        //sb = new StringBuilder();
                        //sb.AppendFormat("select `password` from tblStudents where StudentID = '" + id + "'");
                        //sqlCommand = new MySqlCommand(sb.ToString());
                        //ds = oDb.getDataSetFromSP(sqlCommand);
                        //String result1 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                        //contactpasswordnew = JObject.Parse(result1);

                        if (isUpdate == 1)
                        {
                            success = true;
                            res =
                                "{" +
                                "\"code\":\"1\"," +
                                "\"message\":\"Change Password Success\"" +
                                "}";
                        }
                        else
                        {
                            success = false;
                            res =
                                "{" +
                                "\"code\":\"0\"," +
                                "\"message\":\"Oops!, Something went wrong while processing your request, please contact system administrator.\"" +
                                "}";

                        }
                    }

                }

            }
            else
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Please Fill in all fields!\"" +
                    "}";
            }


            return Ok(res);
        }

        [HttpGet("GetCountryId/{code}")]
        public IActionResult GetCountryId(string code)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select CountryId from Countries  where CountryCode = '" + code + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

    }
}