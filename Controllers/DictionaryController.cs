using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;

namespace autodesk.Controllers{

	[Route("api/Dictionaries")]
	public class DictionariesController : Controller{

        StringBuilder sb = new StringBuilder();
	    private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;


	    public DictionariesController(MySqlDb sqlDb)
	    {
	        oDb = sqlDb;

	    }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from Dictionary";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += "and ";
                }

                if(property.Name.Equals("Parent")){   
                    query += "Parent = '"+property.Value.ToString()+"' "; 
                }
                if(property.Name.Equals("Status")){   
                    query += "`Status` = '"+property.Value.ToString()+"' "; 
                }
                if (property.Name.Equals("Key"))
                {
                    query += "`Key` = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("KeyValue")){   
                    query += "`KeyValue` = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 

            query += "order by `KeyValue` ASC";
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Dictionary;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Dictionary "+
                    "(`Key`, Parent, KeyValue, `Status`) "+
                    "values ('"+data.Key+"','"+data.Parent+"','"+data.KeyValue+"','"+data.Status+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Dictionary;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);
        }

        [HttpGet("LanguageCopyTemplate/{code}")]
        public IActionResult LanguageCopyTemplate(string code)
        {     
            string query = ""; 

            query += "select * from Dictionary where Parent = 'Languages' and `Status` = 'A' and `Key` NOT IN (select LanguageID from EvaluationQuestion where EvaluationQuestionCode = '"+code+"')";
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }


    }
}