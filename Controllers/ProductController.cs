﻿using System;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using autodesk.Code.ViewModel;

// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/Product")]
    public class ProductController : Controller
    {
        // MySQLContext myContext;
        // List<Product> Product = new List<Product>();
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        private IDataServices dataServices;
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        String res;



        public Audit _audit;//= new Audit();
        private MySQLContext db;
        public ProductController(MySqlDb sqlDb, MySQLContext _db)
        {

            oDb = sqlDb;
            _audit=new Audit(oDb);
            db = _db;
            dataServices = new DataServices(oDb,db);
        }

        [HttpGet("ProductMaster")]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                // sb.Append("SELECT productName,GROUP_CONCAT(familyId SEPARATOR ',') AS familyId,`Year`,GROUP_CONCAT(QUOTE(productId) SEPARATOR ',' ) AS productId FROM Products WHERE familyId IN ('AEC', 'PDM', 'M&E') GROUP BY productName ASC");
                sb.Append("SELECT productId,productName,`Status` FROM Products WHERE `Status` = 'A' ORDER BY productName ASC");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        //Hanya mengambil dari tabel product tanpa di join dengan tabel product version
        //[HttpGet("GetSingleProduct")]
        [HttpGet]
        public IActionResult SelectSingelProduct()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT productId,productName,`Status` FROM Products WHERE `Status` = 'A' ORDER BY productName ASC");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ListProduct")]
        public IActionResult GetListOfProduct()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select p.productId,p.productName,pv.ProductVersionsId,pv.Version,pv.`Status` FROM Products p " +
                    "INNER JOIN ProductVersions pv ON p.productId = pv.productId INNER JOIN ProductFamilies pf ON p.productId = pf.productId INNER JOIN Family f ON pf.familyId = f.familyId GROUP BY pv.ProductVersionsId ORDER BY pv.DisplayOrder ASC");
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ListProductCourse")]
        public IActionResult ListProductCourse()
        {
            try
            {
                sb = new StringBuilder();
                
                // sb.Append("select p.productId, p.productName FROM Products p " + 
                //     "INNER JOIN ProductVersions pv ON p.productId = pv.productId INNER JOIN ProductFamilies pf ON p.productId = pf.productId INNER JOIN Family f ON pf.familyId = f.familyId WHERE p.`Status` = 'A' AND pv.`Status` = 'A' GROUP BY p.productId ORDER BY pv.DisplayOrder ASC");
                
                /* issue 09102018 - order by product name A~Z (add course) */

                sb.Append("select p.productId, p.productName FROM Products p " + 
                    "INNER JOIN ProductVersions pv ON p.productId = pv.productId INNER JOIN ProductFamilies pf ON p.productId = pf.productId INNER JOIN Family f ON pf.familyId = f.familyId WHERE p.`Status` = 'A' AND pv.`Status` = 'A' GROUP BY p.productId ORDER BY p.productName ASC");
                
                /* end line issue 09102018 - order by product name A~Z (add course) */

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CategoryByFamily")]
        public IActionResult CategoryByFamily()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append(
                    "SELECT pf.productId,productName,familyName FROM ProductFamilies pf " +
                    "INNER JOIN Products p ON pf.productId = p.productId " +
                    "INNER JOIN ProductVersions pv ON p.productId = pv.productId "+
                    "INNER JOIN Family f ON pf.familyId = f.familyId " +
                    "WhERE p.Status <>'X' AND pf.Status<>'X' AND pv.Status<>'X' " +
                    "GROUP BY p.productId ORDER BY p.productName ASC"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CheckAvailable/{name}")]
        public IActionResult CheckAvailableProduct(string name)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT DISTINCT COUNT(productId) AS jumlah, productId FROM Products WHERE UPPER(productName) = UPPER('" + name + "')");
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CheckAvailableFamily/{productId}")]
        public IActionResult CheckAvailableFamily(string productId)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append(
                    "SELECT p.productId,pf.familyId FROM Products p " +
                    "INNER JOIN ProductFamilies pf ON pf.productId = p.productId " +
                    "WHERE p.productId = '" + productId + "' AND pf.familyId IN (SELECT familyId FROM Family WHERE `Status` = 'A')"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                if(ds.Tables.Count == 0){
                    result = null;
                } else{
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                }
            }

            return Ok(result);
        }

        [HttpGet("CheckAvailableVersion/{productId}/{version}")]
        public IActionResult CheckAvailableFamily(string productId, string version)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append(
                    "SELECT ProductVersionsId,productId,Version,`Status` FROM ProductVersions " +
                    "WHERE productId = '" + productId + "' AND UPPER(Version) = UPPER('" + version + "')"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                if(ds.Tables.Count == 0){
                    result = null;
                } else{
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                }
            }

            return Ok(result);
        }

        [HttpGet("qualification/ProductFamilies")]
        public IActionResult qualificationSelectFamilies()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Family where `Status` = 'A' ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ProductFamilies")]
        public IActionResult SelectFamilies()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Family ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Products where productId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetProduct/{id}")]
        public IActionResult WhereIdGetProduct(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("SELECT productName,GROUP_CONCAT( familyId SEPARATOR ',' ) AS familyId,`Year`,GROUP_CONCAT(QUOTE(productId) SEPARATOR ',' ) AS productId FROM Products WHERE familyId IN ( 'AEC', 'PDM', 'M&E' ) AND productName = '" + id + "' GROUP BY productName");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ProductFamilies/{id}")]
        public IActionResult WhereFamilyId(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Family where familyId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("Version/{id}")]
        public IActionResult WhereProductId(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select ProductVersionsId,Version from ProductVersions where productId = " + id + " and `Status` = 'A' order by Version DESC");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ProductVersion/{id}")]
        public IActionResult WhereProductVersionId(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from ProductVersions where ProductVersionsId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("Family")]
        public IActionResult WhereFamily()
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("SELECT familyId,familyName FROM Family WHERE `Status` = 'A'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("qualification/where/{obj}")]
        public IActionResult QualificationWhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += 
            "select distinct p.productId, p.productName,pf.familyId FROM Products p " +
            "INNER JOIN ProductVersions pv ON p.productId = pv.productId " +
            "INNER JOIN ProductFamilies pf ON p.productId = pf.productId " +
            "INNER JOIN Family f ON pf.familyId = f.familyId ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("familyId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "pf.familyId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Year"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "p.`Year` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Status"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "p.`Status` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("VersionStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "pv.`Status` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                
                if (property.Name.Equals("ProductId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "p.productId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                
                if (property.Name.Equals("VersionId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "pv.ProductVersionsId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
            }

            // query += " GROUP BY p.productId ORDER BY pv.DisplayOrder ASC";

            query += "ORDER BY p.productName"; /* display orderly (A~Z) issue 12092018 */

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "SELECT p.productId,p.productName,p.`Year`,pv.`Status`,GROUP_CONCAT(DISTINCT pf.familyId) AS familyId,Version,ProductVersionsId FROM ProductFamilies pf " + 
                "INNER JOIN Products p ON pf.productId = p.productId " +
                "INNER JOIN ProductVersions pv ON p.productId = pv.productId " +
                "INNER JOIN Family f ON pf.familyId = f.familyId ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += "WHERE ";
                }
                if (i > 0)
                {
                    query += "AND ";
                }

                if (property.Name.Equals("familyId"))
                {
                    query += "pf.familyId = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("Year"))
                {
                    query += "p.`Year` = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("Status"))
                {
                    query += "p.`Status` = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("ProductId"))
                {
                    query += "p.productId = '" + property.Value.ToString() + "' ";
                }
                if (property.Name.Equals("VersionId"))
                {
                    query += "pv.ProductVersionsId = '" + property.Value.ToString() + "' ";
                }

                i = i + 1;
            }

            // query += " GROUP BY p.productId ASC";
            query += " GROUP BY pv.ProductVersionsId ORDER BY pv.DisplayOrder ASC";
            // Console.WriteLine(query);

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("FindProduct/where/{obj}")]
        public IActionResult WhereProductObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";

            query += "select * from Products ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " where ";
                }
                if (i > 0)
                {
                    query += " and ";
                }

                if (property.Name.Equals("familyId"))
                {
                    query += "familyId = '" + property.Value.ToString() + "' ";
                }

                i = i + 1;
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            int id = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Products;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            // Console.WriteLine(count1); 

            try
            {
                //Masukin ke tabel Products
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Products " +
                    //"(productName, `Year`, `Status`) " +
                    "(productName, `Status`) " +
                    "values ('" + data.productName + "','A'); SELECT LAST_INSERT_ID() AS productId"
                );
                string[] temp = data.familyId.ToString().Split(",");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                res = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(res);
                id = Int32.Parse(o3["productId"].ToString());

                //Masukin ke tabel ProductFamilies
                for (int i = 0; i < temp.Length; i++)
                {
                    sb = new StringBuilder();
                    //sb.AppendFormat("insert into ProductFamilies (productId,familyId,`Year`,`Status`) values ('" + id + "','" + temp[i] + "','" + data.year + "','" + data.Status + "')");
                    sb.AppendFormat("insert into ProductFamilies (productId,familyId,`Status`) values ('" + id + "','" + temp[i] + "','" + data.Status + "')");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    Console.WriteLine(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }

                //Masukin ke tabel ProductVersions
                sb = new StringBuilder();
                sb.AppendFormat("insert into ProductVersions (productId,Version,`Status`) values ('" + id + "','" + data.version + "','" + data.Status + "')");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Products;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            // Console.WriteLine(count2);

            if (count2 > count1)
            {
                // sb = new StringBuilder();
                // sb.AppendFormat("select * from Products order by productId desc limit 1 ;");
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);

                // result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                // o3 = JObject.Parse(result); 

                // if(o3["productId"].ToString() == (data.familyId+"-"+count1).ToString())
                // {
                //string description = "Insert data Product" + "<br/>" +
                //    "Product Name = "+ data.productName + "<br/>" +
                //    "Version = "+ data.version + "<br/>" +
                //    "Status = "+ data.Status;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString()); 

                sb = new StringBuilder();
                sb.AppendFormat("select * from Products where productId =" + id+ ";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Products, data.UserId.ToString(), data.cuid.ToString(), "where productId =" + id + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }
                //}   
            }

            return Ok(res);
        }

        [HttpPost("AddIntoPF")]
        public IActionResult AddingPF([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from ProductFamilies");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            
            string[] temp = data.familyId.ToString().Split(",");

            try
            {
                //Masukin ke tabel ProductFamilies
                for (int i = 0; i < temp.Length; i++)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("insert into ProductFamilies (productId,familyId,`Status`) values ('" + data.productId + "','" + temp[i] + "','" + data.Status + "')");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from ProductFamilies;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            

            if (count2 > count1)
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
            }

            return Ok(res);
        }

        [HttpPost("ProductVersions")]
        public IActionResult IntoProductVersions([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from ProductVersions");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            // Console.WriteLine(count1); 

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into ProductVersions " +
                    "(productId, Version, `Status`) " +
                    "values (" + data.productId + ",'" + data.version + "','" + data.Status + "')"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from ProductVersions;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            // Console.WriteLine(count2);

            if (count2 > count1)
            {
                // sb = new StringBuilder();
                // sb.AppendFormat("select * from Products order by productId desc limit 1 ;");
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);

                // result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                // o3 = JObject.Parse(result); 

                // if(o3["productId"].ToString() == (data.familyId+"-"+count1).ToString())
                // { 
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
                //}   
            }

            return Ok(res);
        }

        [HttpPost("ProductFamilies")]
        public IActionResult InsertFamilies([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Family;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            // Console.WriteLine(count1); 

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Family " +
                    "(familyId, familyName, `Status`) " +
                    "values ('" + data.familyId + "','" + data.familyName + "','" + data.Status + "');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Family;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            // Console.WriteLine(count2);

            if (count2 > count1)
            {
                // sb = new StringBuilder();
                // sb.AppendFormat("select * from Products order by productId desc limit 1 ;");
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);

                // result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                // o3 = JObject.Parse(result); 

                // if(o3["productId"].ToString() == (data.familyId+"-"+count1).ToString())
                // { 
                //string description = "Insert data Category Product" + "<br/>" +
                //    "Category ID = "+ data.familyId + "<br/>" +
                //    "Category Name = "+ data.familyName + "<br/>" +
                //    "Status = "+ data.Status;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                sb = new StringBuilder();
                sb.AppendFormat("select * from Family where familyId='" + data.familyId + "';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Family, data.UserId.ToString(), data.cuid.ToString(), "where familyId =" + data.familyId + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }

                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
                //}   
            }

            return Ok(res);
        }

        [HttpPost("IntoProductFamilies")]
        public IActionResult InsertIntoPF([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from ProductFamilies");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            // Console.WriteLine(count1); 

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into ProductFamilies " +
                    "(productId, familyId,`Year`,`Status`) " +
                    "values (" + data.productId + ",'" + data.familyId + "','" + data.year + "','" + data.Status + "')"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from ProductFamilies;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            // Console.WriteLine(count2);

            if (count2 > count1)
            {
                // sb = new StringBuilder();
                // sb.AppendFormat("select * from Products order by productId desc limit 1 ;");
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);

                // result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                // o3 = JObject.Parse(result); 

                // if(o3["productId"].ToString() == (data.familyId+"-"+count1).ToString())
                // { 
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
                //}   
            }

            return Ok(res);
        }

        [HttpPut("{id}")]
        public IActionResult Update(string id, [FromBody] dynamic data)
        {
            JObject o3;
            JObject o1;
            int countStatus = 0;

            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from Products where productId = '" + id + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {
                //Update tabel Products
                sb = new StringBuilder();
                sb.AppendFormat(
                    // "UPDATE Products p, ProductFamilies pf SET " +
                    "UPDATE Products p SET " +
                    "p.productName = '" + data.productName + "', " +
                    // "p.`Year` = '" + data.year + "', " +
                    "p.`Year` = '" + data.year + "' " +
                    //"pf.`Year` = '" + data.year + "', " +
                    // "p.`Status` = '" + data.Status + "', " +
                    // "pf.`Status` = '" + data.Status + "' " +
                    // "WHERE p.productId = pf.productId AND p.productId = " + id + ""
                    "WHERE p.productId = " + id + ""
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                //Update tabel ProductFamilies
                sb = new StringBuilder();
                sb.AppendFormat(
                    "UPDATE ProductFamilies SET " +
                    "Status = '" + data.Status + "' " +
                    "WHERE productId = '" + data.productId + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                //Update tabel ProductVersion
                sb = new StringBuilder();
                sb.AppendFormat(
                    "UPDATE ProductVersions SET " +
                    "Version = '" + data.version + "', " +
                    "Status = '" + data.Status + "' " +
                    "WHERE ProductVersionsId = '" + data.productVersionsId + "' AND productId = '" + data.productId + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat(
                    "select COUNT(*) as count from Products p INNER JOIN ProductVersions pv ON p.productId = pv.productId " +
                    "WHERE p.productId = " + id + " and pv.`Status` = 'A'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o1 = JObject.Parse(result);
                countStatus = Int32.Parse(o1["count"].ToString());

                //update status product
                Console.WriteLine(countStatus);
                if(countStatus < 1){
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "UPDATE Products p SET " +
                        "p.`Status` = 'X', " +
                        "WHERE p.productId = " + id + ""
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Update Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from Products where productId = '" + id + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            if (o3["productId"].ToString() == id.ToString())
            {
                //string descriptionU = "Update data Product" + "<br/>" +
                //    "Product Name = "+ data.productName + "<br/>" +
                //    "Status = "+ data.Status + "<br/>" +
                //    "Version = "+ data.version;

                //_audit.ActionPost(data.UserId.ToString(), descriptionU, data.cuid.ToString());

                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Products, data.UserId.ToString(), data.cuid.ToString(), "where productId =" + id + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Update Data Success\"" +
                     "}";
                }
            }

            return Ok(res);
        }

        [HttpPut("ProductFamilies/{id}")]
        public IActionResult UpdateFamilies(string id, [FromBody] dynamic data)
        {
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Family where familyId = '" + id + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Family set " +
                    "familyId='" + data.familyId + "', " +
                    "familyName='" + data.familyName + "', " +
                    "`Status`='" + data.Status + "' " +
                    "where familyId='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Update Data Failed\"" +
                    "}";
            }
            finally
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Family where familyId = '" + id + "';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Family, data.UserId.ToString(), data.cuid.ToString(), "where familyId =" + id + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Update Data Success\"" +
                     "}";
                }
            }

            //string descriptionU = "Update data Category Product" + "<br/>" +
            //    "Category ID = "+ data.familyId + "<br/>" +
            //    "Category Name = "+ data.familyName + "<br/>" +
            //    "Status = "+ data.Status;

            //_audit.ActionPost(data.UserId.ToString(), descriptionU, data.cuid.ToString());
           

            return Ok(res);
        }

        [HttpDelete("{id}/{versionId}/{cuid}/{UserId}")]
        public IActionResult Delete(string id,string versionId, string cuid, string UserId)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from ProductVersions where productId = '" + "' AND ProductVersionsId = '" + versionId + "'");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {
                sb = new StringBuilder();
                //harus ngecek lagi kalau di table product versions udah ga ada lagi product id yang sama, berarti harus hapus di table product dan product familiesnya juga
                sb.AppendFormat("DELETE FROM ProductVersions WHERE productId = '" + id + "' AND ProductVersionsId = '" + versionId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                //ini untuk ngecek kalau udah ga ada lagi product version kita ubah status di table product jadi 'X'
                sb = new StringBuilder();
                sb.AppendFormat("SELECT COUNT(productId) AS jumlah FROM ProductVersions WHERE productId = '" + id + "'");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);

                if(o3["jumlah"].ToString() == "0"){
                    //Kalau udah ga ada lagi data product nya di product version ubah status product jadi 'X'
                    sb = new StringBuilder();
                    sb.AppendFormat("UPDATE Products SET `Status` = 'X' WHERE productId = '" + id + "'");
                    // Console.WriteLine(sb.ToString());
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }

            }
            catch
            {
                res = "Not found";
            }
            finally
            {
                //string description = "Delete data Product" + "<br/>" +
                //    "Product ID = "+ id + "<br/>" +
                //    "Product Version ID = "+ versionId;

                //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());
                JObject NewData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.ProductVersions, UserId.ToString(),cuid.ToString(), "where productId = '" + id + "' AND ProductVersionsId = '" + versionId + "'"))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Delete Data Success\"" +
                     "}";
                }

            }

            return Ok(res);
        }

        [HttpDelete("ProductFamilies/{id}/{cuid}/{UserId}")]
        public IActionResult DeleteFamilies(string id, string cuid, string UserId)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from Family where familyId = '" + id + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("delete from Family where familyId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res = "Not found";
            }
            finally
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Family where familyId = '" + id + "';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                // Console.WriteLine(o3.Count);
                if (o3.Count < 1)
                {
                    //string description = "Delete data Category Product" + "<br/>" +
                    //"Category ID = "+ id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());

                    JObject NewData = new JObject();
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Family, UserId.ToString(), cuid.ToString(), "where familyId = '" + id + ""))
                    {
                        res =
                         "{" +
                         "\"code\":\"1\"," +
                         "\"message\":\"Delete Data Success\"" +
                         "}";
                    }
                }
            }

            return Ok(res);
        }

        [HttpDelete("DeleteFamilies/{id}/{familyId}")]
        public IActionResult OnlyDeleteFamilies(string id, string familyId)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("DELETE FROM ProductFamilies WHERE productId = " + id + " AND familyId = '" + familyId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                res = "Not found";
            }
            finally
            {
                sb = new StringBuilder();
                sb.AppendFormat("SELECT * FROM ProductFamilies WHERE productId = " + id + " AND familyId = '" + familyId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                if (o3.Count < 1)
                {
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Success\"" +
                        "}";
                }
            }

            return Ok(res);
        }

        [HttpGet("ProductCourse")]
        public IActionResult ProductCourse()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append(
                    "SELECT DISTINCT CONCAT(p.productName,' ',pv.Version) AS PrimaryProduct " +
                    "FROM CourseSoftware cs " +
                    "INNER JOIN Products p ON cs.productId = p.productId " +
                    "INNER JOIN ProductVersions pv ON p.productId = pv.productId " +
                    "GROUP BY cs.productId "
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetPrimaryProducts")]
        public IActionResult GetPrimaryProducts()
        {
           
            var dataList = dataServices.GetProductPrimaryorSecondary<PrimaryProduct>("Primary");
            result = Newtonsoft.Json.JsonConvert.SerializeObject(dataList);
            return Ok(result);
        }
        [HttpGet("GetSecondaryProducts")]
        public IActionResult GetSecondaryProducts()
        {
            
            var dataList = dataServices.GetProductPrimaryorSecondary<SecondaryProduct>("Secondary");
            result = Newtonsoft.Json.JsonConvert.SerializeObject(dataList);
            return Ok(result);
        }
        [HttpGet("GetPrimaryAndSecondaryProducts")]
        public IActionResult GetPrimaryAndSecondaryProducts()
        {
           
            var primaryProducts = dataServices.GetProductPrimaryorSecondary<PrimaryProduct>("Primary");
            var secondaryProducts = dataServices.GetProductPrimaryorSecondary<SecondaryProduct>("Secondary");
            ProductDropdownViewModel productDropdownViewModel = new ProductDropdownViewModel
            {
                primaryProducts = primaryProducts,
                SecondaryProducts = secondaryProducts
            };
            result = Newtonsoft.Json.JsonConvert.SerializeObject(productDropdownViewModel);
            return Ok(result);
        }

        
        
    }
}
