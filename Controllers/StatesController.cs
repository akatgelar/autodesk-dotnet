using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/States")]
    public class StatesController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public StatesController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from States ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }


        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from States where StateCode = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from States ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("CountryCode")){   
                    query += "CountryCode = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPost("whererpt")]
        public IActionResult whererpt([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from States where CountryCode IN (" + data.Countrycode + ") ORDER BY CountryCode");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }


        //[HttpGet("whererpt/{obj}")]
        //public IActionResult WhererptObj(string obj)
        //{
        //    var o1 = JObject.Parse(obj);
        //    string query = "";

        //    query += "select * from States ";
        //    int i = 0;
        //    JObject json = JObject.FromObject(o1);
        //    foreach (JProperty property in json.Properties())
        //    {
        //        if (i == 0)
        //        {
        //            query += " where ";
        //        }
        //        if (i > 0)
        //        {
        //            query += " and ";
        //        }

        //        if (property.Name.Equals("CountryCode"))
        //        {
        //            query += "CountryCode in ('" + property.Value.ToString() + "') ";
        //        }

        //        i = i + 1;
        //    }

        //    try
        //    {
        //        sb.AppendFormat(query);
        //        sqlCommand = new MySqlCommand(sb.ToString());
        //        ds = oDb.getDataSetFromSP(sqlCommand);
        //    }
        //    catch
        //    {
        //        result = "[]";
        //    }
        //    finally
        //    {
        //        result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
        //    }
        //    return Ok(result);
        //}
    }
}