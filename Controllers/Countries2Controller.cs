using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/Countries2")]
    public class Countries2Controller : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Countries2Controller(MySqlDb sqlDb)
        {
            oDb = sqlDb;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Ref_countries where countries_code <> '' ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Ref_countries where countries_id = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CountryLinks/{id}")]
        public IActionResult CountryLinks(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select subregion_id,subregion_name,region_id,region_name,geo_id,geo_name from Ref_countries "+
                    "left join Ref_subregion on Ref_countries.subregion_code = Ref_subregion.subregion_code "+
                    "left join Ref_region on Ref_subregion.region_code= Ref_region.region_code "+
                    "left join Ref_geo on Ref_region.geo_code = Ref_geo.geo_code "+
                    "where Ref_countries.countries_id ="+id+"");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("CountryFilter/{id}")]
        public IActionResult WhereIdSubregion(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select countries_id,countries_code,countries_name,MarketTypeId from Ref_countries where subregion_code in "
                    +"(select subregion_code from Ref_subregion where subregion_id = '"+id+"') and countries_code != ''");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }  

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from Ref_countries ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("CountryCode")){   
                    query += "countries_code = '"+property.Value.ToString()+"' "; 
                }  
                if(property.Name.Equals("SubRegion")){   
                    query += "subregion_name = '"+property.Value.ToString()+"' "; 
                }  
                if(property.Name.Equals("subregion_code")){   
                    query += "subregion_code = '"+property.Value.ToString()+"' "; 
                } 
                if(property.Name.Equals("MarketTypeId")){   
                    query += "MarketTypeId = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_countries;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Ref_countries "+
                    "(countries_code, countries_name, geo_code, region_code, subregion_code, geo_name, region_name, subregion_name, countries_tel, MarketTypeId, Embargoed) "+
                    "values ('"+data.CountryCode+"','"+data.Country+"','"+data.Geo+"','"+data.Region+"','"+data.SubRegion+"','"+data.GeoName+"','"+data.RegionName+"','"+data.SubRegionName+"','"+data.CountryTelCode+"','"+data.MarketTypeId+"','"+data.Embargoed+"');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_countries;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);
            
            if(count2 > count1)
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Insert Data Success\""+
                    "}";  
            } 
 
            return Ok(res);  
            
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";  
                
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Ref_countries set "+
                    "countries_code='"+data.CountryCode+"',"+
                    "countries_name='"+data.Country+"',"+
                    "geo_code='"+data.Geo+"',"+
                    "region_code='"+data.Region+"',"+
                    "subregion_code='"+data.SubRegion+"',"+
                    "geo_name='"+data.GeoName+"',"+
                    "region_name='"+data.RegionName+"',"+
                    "subregion_name='"+data.SubRegionName+"',"+
                    "countries_tel='"+data.CountryTelCode+"',"+
                    "MarketTypeId='"+data.MarketTypeId+"',"+
                    "Embargoed='"+data.Embargoed+"'"+
                    "where countries_id='"+id+"'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_countries where countries_id ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);  

            if(o3["countries_code"].ToString() == data.CountryCode.ToString())
            { 
                res =
                    "{"+
                    "\"code\":\"1\","+
                    "\"message\":\"Update Data Success\""+
                    "}"; 
            } 
 
            return Ok(res);  
        } 



        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";  
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from Ref_countries where countries_id = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from Ref_countries where countries_id  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){ 
                    res =
                        "{"+
                        "\"code\":\"1\","+
                        "\"message\":\"Delete Data Success\""+
                        "}"; 
                }
            }

            return Ok(res);
        } 


    }
}
