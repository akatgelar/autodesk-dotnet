using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/SubRegion")]
    public class SubRegionController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit;//= new Audit();

        public SubRegionController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            _audit=new Audit(oDb);

        }


        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Ref_subregion ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select * from Ref_subregion where subregion_id = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("SubRegionFilter/{id}")]
        public IActionResult WhereIdRegion(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select subregion_id,subregion_code,subregion_name from Ref_subregion where region_code in "
                    +"(select region_code from Ref_region where region_id = '"+id+"')");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("filter/{id}")]
        public IActionResult WhereIdGeo(string id)
        { 
            try
            {  
                if(id!=""){
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Ref_subregion where geo_code in ( "+id+" );");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                else{
                    result = "";
                }
                
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("filterregion/{id}")]
        public IActionResult WhereIdRegion(string id)
        { 
            try
            {  
                if(id!=""){
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from Ref_subregion where region_code in ( "+id+" );");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                else{
                    result = "";
                }
                
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from Ref_subregion ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("region_code")){   
                    query += "region_code = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            JObject o4;
            string result2 = "";
            string result3 = "";
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_subregion;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 

            // Geo Name
            sb = new StringBuilder();
            sb.AppendFormat("select geo_name from Ref_geo where geo_code = '"+data.geo_code+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result2 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result2); 
         
            // Region Name
            sb = new StringBuilder();
            sb.AppendFormat("select region_name from Ref_region where region_code = '"+data.region_code+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result3 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o4 = JObject.Parse(result3);
            
            
            try
            {  
                sb = new StringBuilder();
                // sb.AppendFormat(
                //     "insert into Ref_subregion "+
                //     "(geo_code, territory_code, region_code, subregion_code, subregion_name, cuid, cdate, muid, mdate) "+
                //     "values ('"+data.geo_code+"','"+data.territory_code+"','"+data.region_code+"','"+data.subregion_code+"','"+data.subregion_name+"','"+data.cuid+"','"+data.cdate+"','"+data.muid+"','"+data.mdate+"');"
                // );
                sb.AppendFormat(
                    "insert into Ref_subregion "+
                    "(geo_code, geo_name, region_code, region_name, subregion_code, subregion_name, cuid, cdate, muid, mdate) "+
                    "values ('"+data.geo_code+"','"+o3["geo_name"]+"','"+data.region_code+"','"+o4["region_name"]+"','"+data.subregion_code+"','"+data.subregion_name+"','"+data.cuid+"','"+data.cdate+"','"+data.muid+"','"+data.mdate+"');"
                );
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_subregion;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_subregion where subregion_code='" + data.subregion_code + " and region_code='" + data.region_code + "';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            if (count2 > count1)
            { 

                //string description = "Insert data Sub Region" + "<br/>" +
                //    "Sub Region Code = "+ data.subregion_code + "<br/>" +
                //    "Sub Region Name = "+ data.subregion_name + "<br/>" +
                //    "Geo Code = "+ data.geo_code + "<br/>" +
                //    "Geo Name = "+ o3["geo_name"] + "<br/>" +
                //    "Region Code = "+ data.region_code + "<br/>" +
                //    "Region Name = "+ o4["region_name"];

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_subregion, data.UserId.ToString(), data.cuid.ToString(), "where subregion_code =" + data.subregion_code + " and region_code=" + data.region_code +  ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }
            } 
 
            return Ok(res);  
            
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  


            JObject o3;
            JObject o4;
            string result2;
            string result3;

            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_subregion where subregion_id =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            // Geo Name
            sb = new StringBuilder();
            sb.AppendFormat("select geo_name from Ref_geo where geo_code = '"+data.geo_code+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result2 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result2);

            // Region Name
            sb = new StringBuilder();
            sb.AppendFormat("select region_name from Ref_region where region_code = '"+data.region_code+"';");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result3 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o4 = JObject.Parse(result3);

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Ref_subregion set "+
                    "geo_code='"+data.geo_code+"',"+
                    "geo_name='"+o3["geo_name"]+"',"+
                    "region_code='"+data.region_code+"',"+
                    "region_name='"+o4["region_name"]+"',"+
                    "subregion_code='"+data.subregion_code+"',"+
                    "subregion_name='"+data.subregion_name+"',"+
                    "cuid='"+data.cuid+"',"+
                    "cdate='"+data.cdate+"',"+
                    "muid='"+data.muid+"',"+
                    "mdate='"+data.mdate+"'"+
                    "where subregion_id='"+id+"'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");  
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_subregion where subregion_id ="+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //string description = "Update data Sub Region" + "<br/>" +
            //        "Sub Region Code = "+ data.subregion_code + "<br/>" +
            //        "Sub Region Name = "+ data.subregion_name + "<br/>" +
            //        "Geo Code = "+ data.geo_code + "<br/>" +
            //        "Geo Name = "+ o3["geo_name"] + "<br/>" +
            //        "Region Code = "+ data.region_code + "<br/>" +
            //        "Region Name = "+ o4["region_name"];

            //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_subregion, data.UserId.ToString(), data.cuid.ToString(), "where subregion_id =" + id + ""))
            {
                res =
                 "{" +
                 "\"code\":\"1\"," +
                 "\"message\":\"Update Data Success\"" +
                 "}";
            }
 
            return Ok(res);  
        } 



        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_subregion where subregion_id =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from Ref_subregion where subregion_id = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from Ref_subregion where subregion_id  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){

                    //string description = "Delete data SubRegion" + "<br/>" +
                    //"Region ID = "+ id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());
                    JObject NewData = new JObject();
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_subregion, UserId.ToString(), cuid.ToString(), "where subregion_id =" + id + ""))
                    {
                        res =
                         "{" +
                         "\"code\":\"1\"," +
                         "\"message\":\"Delete Data Success\"" +
                         "}";
                    }
                }
            }

            return Ok(res);
        } 


    }
}
