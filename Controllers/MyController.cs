﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http; 
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;

namespace autodesk.Controllers
{
    
    public class MyController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;        
        
        
        public MyController(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            // this.getHeader();
            // var payload = new Dictionary<string, object>()
            // {
            // 	{ "email", "admin@gmail.com" },
            // 	{ "password", "21232f297a57a5a743894a0e4a801fc3" }
            // };

            // string token = "";
            // var secretKey = new byte[]{164,60,194,0,161,189,41,38,130,89,141,164,45,170,159,209,69,137,243,216,191,131,47,250,32,107,231,117,37,158,225,234};
            // token = Jose.JWT.Encode(payload, secretKey, Jose.JwsAlgorithm.HS256); 
            // Console.WriteLine(token);

        }

        public int getHeader()
        {
            var token = ""; 
            int res = 0;

            try
            { 
                token = Request.Headers["Authorization"];  
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                res = 0;   
            }  
            finally
            {
                try
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select * from UserToken where Token ='"+token+"';");
                    Console.WriteLine(sb.ToString());
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                 
                }
                catch (Exception ex)
                { 
                    res = 2;   
                } 
                finally
                {
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    var o3 = JObject.Parse(result);  
                    Console.WriteLine(o3.Count);
                    if(o3.Count > 0)
                    {   
                         res = 1;
                    }
                    else
                    { 
                        res = 2;   
                    }

                    
                }
            }

            return res;
        }


        public int getHeader2()
        {
 
            var content_type = "";
            var token = "";
            var data = "";
            var data_final = new object();
            var obj = new Object{};
            var secretKey = new byte[]{};
            int res = 0;

            try{
                Console.WriteLine(""); 
                Console.WriteLine("======================================"); 
                content_type = Request.Headers["Content-Type"]; 
                token = Request.Headers["Authorization"];
                Console.WriteLine(content_type);
                Console.WriteLine(token);
                Console.WriteLine("======================================"); 
            }
            catch(Exception e){
                Console.WriteLine(e);
            } 

            
            if(token != null){ 
                try{ 
                    secretKey = new byte[]{164,60,194,0,161,189,41,38,130,89,141,164,45,170,159,209,69,137,243,216,191,131,47,250,32,107,231,117,37,158,225,234};
                    data = Jose.JWT.Decode(token, secretKey, Jose.JwsAlgorithm.HS256); 
                    var o1 = JObject.Parse(data.ToString());
                    Console.WriteLine(data);
                    Console.WriteLine("======================================"); 
                    Console.WriteLine(""); 

                    // res = data;
                    res = 1;
                    try
                    {
                        sb = new StringBuilder();
                        string query1 = "select * from User where Email = '"+o1["email"]+"' and Password ='"+o1["password"]+"'";
                        sb.Append(query1);
                        // Console.WriteLine(query);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);   
                    }
                    catch
                    {
                        result = "{}";
                    } 
                    finally
                    {
                        result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
                        var o2 = JArray.Parse(result);
                        Console.WriteLine(o2);
                        // Console.WriteLine(o2.LongCount());
                        
                        if(o2.LongCount() > 0){ 
                            var o3 = JObject.Parse(o2[0].ToString());
                            Console.WriteLine(o3["UserLevelId"]);

                            try
                            {
                                sb = new StringBuilder();
                                string query2 = "select * from UserLevel where UserLevelId = '"+o3["UserLevelId"]+"'";
                                sb.Append(query2);
                                Console.WriteLine(query2);
                                sqlCommand = new MySqlCommand(sb.ToString());
                                ds = oDb.getDataSetFromSP(sqlCommand); 
                            }
                            catch
                            {
                                result = "{}";
                            } 
                            finally
                            { 
                                result = MySqlDb.GetJSONObjectString(ds.Tables[0]); 
                                var o4 = JObject.Parse(result);
                                Console.WriteLine(o4);
                                o3["Access"] = o4;
                                Console.WriteLine(o3);
                            }
                        }
                    }
                }
                catch(Exception e){
                    Console.WriteLine(e);

                    // res = "{code:'0',msg:'Invalid token'}";
                    res = 2;
                } 
            }
            else{
                // res = "{code:'0',msg:'Token not found'}"; 
                // Console.WriteLine("{code:'0',msg:'Token not found'}");
                res = 0;
            }
           

            return res;

        }


        public string noToken()
        {
            
            return "{\"code\":\"0\",\"msg\":\"Token Not Found\"}";
        }
        
        public string wrongToken()
        {
            
            return "{\"code\":\"0\",\"msg\":\"Invalid Token\"}";
        }

        
         
    }
}
