using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/AccessRole")]
    public class AccessRole : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb; //= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        private Audit _audit;

        public AccessRole(MySqlDb sqlDb)
        {
            oDb = sqlDb;
            _audit = new Audit(oDb);
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select ID as AccessRoleId, UserLevelId as Name, Name as Description from UserLevel;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id) 
        {
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select ID as AccessRoleId, UserLevelId as Name, Name as Description from UserLevel where ID = '" + id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }        

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj) 
        {
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from AccessRole ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("AccessRole")){   
                    query += "AccessRole = '"+property.Value.ToString()+"' "; 
                }

                i=i+1;
            }

            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into UserLevel "+
                    "( UserLevelId, Name) "+
                    "values ( '"+data.Name+"','"+data.Description+"');"
                );
                sb.Append("select ID as AccessRoleId, UserLevelId as Name, Name as Description from UserLevel;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                //string description = "Insert data New Role <br/>" +
                //    "Role Name = "+ data.Name + "<br/>" +
                //    "Description = "+ data.Description;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

                JObject OldData = new JObject();
                sb = new StringBuilder();
                sb.AppendFormat("select * from UserLevel where UserLevelId =" + data.Name + ";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                string result1 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result1.ToString());
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.UserLevel, data.UserId.ToString(), data.cuid.ToString(), "where UserLevelId =" + data.Name + "");

               
            }
 
            return Ok(result);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            JObject NewData = new JObject();
            sb = new StringBuilder();
            sb.AppendFormat("select * from UserLevel where ID =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            string result1 = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result1.ToString());
            try
            {
               

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update UserLevel set "+ 
                    "Name='"+data.Description+"' "+
                    "where ID='"+id+"'; "
                );
                sb.Append("select ID as AccessRoleId, UserLevelId as Name, Name as Description from UserLevel;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                NewData = JsonConvert.DeserializeObject<JObject>(result1.ToString());
            }
            catch
            {
                result = "{}";
            }
            finally
            {

                //string description = "Update data Role <br/>" +
                //    "Role Name = "+ data.Name + "<br/>" +
                //    "Description = "+ data.Description;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.UserLevel, data.UserId.ToString(), data.cuid.ToString(), "where ID =" + id + "");

               
            }

            return Ok(result);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";  
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from AccessRole where AccessRoleId = '"+id+"'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 

            }
            finally
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from AccessRole where AccessRoleId  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                Console.WriteLine(o3.Count);
                if(o3.Count < 1){ 
                    res =
                        "{"+
                        "\"code\":\"1\","+
                        "\"message\":\"Delete Data Success\""+
                        "}"; 
                }
            }

            return Ok(res);
        }
    }
}