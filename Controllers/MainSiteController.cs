using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using autodesk.Code.CommonServices;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/MainSite")]
    public class MainSiteController : Controller
    {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        StringBuilder sb3 = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        DataSet ds1 = new DataSet();
        private IDataServices dataService;
        private ICommonServices commonServices;
        DataSet ds2 = new DataSet();
        DataSet ds3 = new DataSet();
        DataSet dsTemp = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        String result2;
        String result3;
        String result4;
        String hasil;

        public Audit _audit;//= new Audit();
        private readonly MySQLContext _mySQLContext;
        public MainSiteController(MySqlDb sqlDb, MySQLContext mySQLContext)
        {
            oDb = sqlDb;
            _audit = new Audit(oDb);
            _mySQLContext = mySQLContext;
            dataService = new DataServices(oDb, _mySQLContext);
            commonServices = new CommonServices();
        }

        [HttpGet("getSiteIdByRole/{userlvl}/{orgids}")]
        public IActionResult getSiteIdByRole(string userlvl, string orgids)
        {
            try
            {
                Boolean itsdistributor = false;
                string[] arruser = userlvl.Split(',');
                for (int i = 0; i < arruser.Length; i++)
                {
                    if (arruser[i] == "DISTRIBUTOR")
                    {
                        itsdistributor = true;
                    }
                }

                string query = "SELECT DISTINCT SiteId FROM Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId WHERE o.`Status` <> 'X' AND s.`Status` <> 'X' and o.OrgId in (" + orgids + ")";

                if (itsdistributor)
                {
                    query = "select DISTINCT SiteId from Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code where o.`Status` <> 'X' AND s.`Status` <> 'X' AND rc.TerritoryId in (select rc.TerritoryId from Main_organization o JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code where o.OrgId in (" + orgids + "))";
                }

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Main_site ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("FindSite/{value}")]
        public IActionResult FindSite(string value)
        {
            JObject resultfinal;
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT SiteId, replace( replace( replace( SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as SiteName, CONCAT(SiteId,' | ',replace( replace( replace( SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' )) AS Site " +
                "FROM Main_site WHERE SiteId like '%" + value + "%'");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                    "{" +
                    "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }

            return Ok(resultfinal);
        }

        [HttpGet("BySiteId/{id}")]
        public IActionResult WhereId(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT * FROM Main_site WHERE SiteId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("getSiteId")]
        public IActionResult selectSiteId()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select SiteId from Main_site");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ApplicantTerritory")]
        public IActionResult SelectApplTerr()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select Main_site.SiteId, OrgName, TerritoryCountry.CountryCode from Main_site left join Main_organization on Main_site.OrgId = Main_organization.OrgId " +
                    "left join TerritoryCountry on Main_organization.RegisteredCountryCode = TerritoryCountry.CountryCode WHERE TerritoryCountry.CountryCode != '' ORDER BY Main_site.SiteId ASC");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("getColumns_Attribute")]
        public IActionResult SelectColumnsAttribute()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("DESCRIBE Main_site;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("getColumns_Journal")]
        public IActionResult SelectColumnsJournal()
        {
            DataTable dt1 = new DataTable();
            try
            {
                sb = new StringBuilder();
                sb.Append("DESCRIBE Main_site; SHOW COLUMNS FROM Journals LIKE '%Notes%';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                dt1 = ds.Tables[0];
                DataTable dt2 = ds.Tables[1];
                dt1.Merge(dt2, false, MissingSchemaAction.Add);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("getColumns")]
        public IActionResult SelectColumns()
        {
            DataTable dt1 = new DataTable();
            try
            {
                //sb = new StringBuilder();
                //sb.Append("DESCRIBE Main_site ");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);
                //Soe Added on 5/12/2018
                sb = new StringBuilder();
                // sb.Append("DESCRIBE Main_site;");
                sb.Append("DESCRIBE Main_site; SHOW COLUMNS FROM RoleParams LIKE '%ParamValue%'; SHOW COLUMNS FROM SiteRoles LIKE '%Status%';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                dt1 = ds.Tables[0];
                DataTable dt2 = ds.Tables[1];
                DataTable dt3 = ds.Tables[2];
                //added by Soe
                dt3.Rows[0]["Field"] = "SubPartnerTypeStatus";
                dt1.Merge(dt2, false, MissingSchemaAction.Add);
                dt1.Merge(dt3, false, MissingSchemaAction.Add);


            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost("reportSite")]
        public IActionResult searchSite([FromBody] dynamic data)

        {
            JObject geo;
            JObject region;
            JObject subregion;
            JObject countries;
            JObject columnOrg;
            JArray columnSite;
            string query = "";
            string selectedColumns = "";
            string kolomOrg = "";
            string kolomSite = "";
            string anotherfiled = "";
            /*fix issue excel no 263.Show data based role*/

            string role = "";
            string orgid = "";

            /*end fix issue excel no 263.Show data based role*/

            selectedColumns += "select DISTINCT ";

            int n = 0;
            JObject column = JObject.FromObject(data);
            foreach (JProperty property in column.Properties())
            {
                if (property.Name.Equals("fieldOrg"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomOrg = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomOrg = "," + property.Value.ToString();
                        }

                        kolomOrg = kolomOrg.Replace("o.DateAdded", "DATE_FORMAT(o.DateAdded, '%d-%m-%Y') AS DateAdded");
                        kolomOrg = kolomOrg.Replace("o.DateLastAdmin", "DATE_FORMAT(o.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        if (kolomOrg.Contains("Status"))
                        {
                            kolomOrg = kolomOrg.Replace("o.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= o.Status)  'Status'");
                        }
                        if (kolomOrg.Contains("RegisteredCountryCode"))
                        {
                            kolomOrg = kolomOrg.Replace("o.RegisteredCountryCode", "tc.countries_name as 'Org Country'");
                        }
                        if (kolomOrg.Contains("ContractCountryCode"))
                        {
                            kolomOrg = kolomOrg.Replace("o.ContractCountryCode", "tc.countries_name as 'Contract Country'");

                        }
                        if (kolomOrg.Contains("InvoicingCountryCode"))
                        {
                            kolomOrg = kolomOrg.Replace("o.InvoicingCountryCode", "tc.countries_name as 'Invoicing Country'");

                        }
                        selectedColumns += kolomOrg;

                        n = n + 1;
                    }
                    else
                    {

                        ////sb = new StringBuilder();
                        ////sb.AppendFormat("SET group_concat_max_len = 1193;" +
                        ////    "SELECT GROUP_CONCAT('ss.',COLUMN_NAME) AS kolomOrg FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Main_site'");
                        ////sqlCommand = new MySqlCommand(sb.ToString());
                        ////dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        ////hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        ////columnOrg = JObject.Parse(hasil);
                        ////selectedColumns += columnOrg["kolomOrg"].ToString();
                        ////n = n + 1;
                        ////selectedColumns = "select DISTINCT ";

                        //sb = new StringBuilder();
                        //sb.AppendFormat("DESCRIBE Main_organization");
                        //sqlCommand = new MySqlCommand(sb.ToString());
                        //dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        //hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        //columnOrg = JArray.Parse(hasil);

                        //string[] columnarrtmp = new string[columnOrg.Count];
                        //for (int index = 0; index < columnOrg.Count; index++)
                        //{
                        //    columnarrtmp[index] = "o." + columnOrg[index]["Field"].ToString();
                        //}

                        //if (n == 0)
                        //{
                        //    kolomOrg = string.Join(",", columnarrtmp);
                        //}
                        //else if (n > 0)
                        //{
                        //    kolomOrg = "," + string.Join(",", columnarrtmp);
                        //}

                        //kolomOrg = kolomOrg.Replace("o.DateAdded", "DATE_FORMAT(o.DateAdded, '%d-%m-%Y') AS DateAdded");
                        //kolomOrg = kolomOrg.Replace("o.DateLastAdmin", "DATE_FORMAT(o.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        //if (kolomOrg.Contains("Status"))
                        //{
                        //    kolomOrg = kolomOrg.Replace("o.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= o.Status)  'Status'");
                        //}
                        //if (kolomOrg.Contains("RegisteredCountryCode"))
                        //{
                        //    kolomOrg = kolomOrg.Replace("o.RegisteredCountryCode", "tc.countries_name as 'Org Country'");
                        //}

                        //selectedColumns += kolomOrg;

                        //n = n + 1;
                    }
                }

                if (property.Name.Equals("fieldSite"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            kolomSite = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            kolomSite = "," + property.Value.ToString();
                        }
                        if (kolomSite.Contains("Status"))
                        {
                            kolomSite = kolomSite.Replace("ss.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= ss.Status)  'Site Status'");
                        }

                        if (kolomSite.Contains("SiteCountryName"))
                        {
                            kolomSite = kolomSite.Replace("ss.SiteCountryName", "tc.countries_name as 'Site Country'");
                        }
                        if (kolomSite.Contains("MailingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("ss.MailingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = ss.MailingCountryCode limit 1) as 'Mailing Country'");
                        }
                        if (kolomSite.Contains("ShippingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("ss.ShippingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = ss.ShippingCountryCode limit 1) as 'Shipping Country'");
                        }
                        if (kolomSite.Contains("ParamValue"))
                        {
                            kolomSite = kolomSite.Replace("rop.ParamValue", "rop.ParamValue as 'Sub Partner Type'");
                        }
                        if (kolomSite.Contains("SubPartnerTypeStatus"))
                        {
                            kolomSite = kolomSite.Replace("SubPartnerTypeStatus", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= sr.Status) 'Sub Partner Type Status'");
                        }

                        selectedColumns += kolomSite;

                        n = n + 1;
                    }
                    else
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat("DESCRIBE Main_site");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        columnSite = JArray.Parse(hasil);

                        string[] columnarrtmp = new string[columnSite.Count];
                        for (int index = 0; index < columnSite.Count; index++)
                        {
                            columnarrtmp[index] = "ss." + columnSite[index]["Field"].ToString();
                        }

                        if (n == 0)
                        {
                            kolomSite = string.Join(",", columnarrtmp);
                        }
                        else if (n > 0)
                        {
                            kolomSite = "," + string.Join(",", columnarrtmp);
                        }

                        kolomSite = kolomSite.Replace("ss.DateAdded", "DATE_FORMAT(ss.DateAdded, '%d-%m-%Y') AS DateAdded");
                        kolomSite = kolomSite.Replace("ss.DateLastAdmin", "DATE_FORMAT(ss.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        if (kolomSite.Contains("Status"))
                        {
                            kolomSite = kolomSite.Replace("ss.Status", "(select KeyValue from Dictionary d where d.Parent = 'SiteStatus' and d.Key = ss.Status)  'Site Status'");
                        }
                        if (kolomSite.Contains("SiteCountryName"))
                        {
                            kolomSite = kolomSite.Replace("ss.SiteCountryName", "tc.countries_name as 'Site Country'");
                        }
                        if (kolomSite.Contains("MailingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("ss.MailingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = ss.MailingCountryCode limit 1) as 'Mailing Country'");
                        }
                        if (kolomSite.Contains("ShippingCountryCode"))
                        {
                            kolomSite = kolomSite.Replace("ss.ShippingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = ss.ShippingCountryCode limit 1) as 'Shipping Country'");
                        }
                        if (kolomSite.Contains("ParamValue"))
                        {
                            kolomSite = kolomSite.Replace("rop.ParamValue", "rop.ParamValue as 'Sub Partner Type'");
                        }
                        if (kolomSite.Contains("SubPartnerTypeStatus"))
                        {
                            kolomSite = kolomSite.Replace("SubPartnerTypeStatus", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= sr.Status) 'Sub Partner Type Status'");
                        }

                        selectedColumns += kolomSite;

                        n = n + 1;
                    }
                }

                if (property.Name.Equals("anotherField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            anotherfiled = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            anotherfiled += "," + property.Value.ToString();
                        }
                        anotherfiled = anotherfiled.Replace("m.MarketType", "m.MarketType as 'Market Type'");
                        anotherfiled = anotherfiled.Replace("r.RoleName", "r.RoleName as 'Partner Type'");
                        anotherfiled = anotherfiled.Replace("sr.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= sr.Status) as 'Partner Type Status'");
                        anotherfiled = anotherfiled.Replace("sr.CSN", "sr.CSN as 'Partner Type CSN'");
                        anotherfiled = anotherfiled.Replace("sr.ITS_ID", "sr.ITS_ID as 'ITS Site ID'");
                        anotherfiled = anotherfiled.Replace("sr.UParent_CSN", "sr.UParent_CSN as 'Ultimate Parent CSN'");
                        anotherfiled = anotherfiled.Replace("tc.geo_name", "tc.geo_name as 'Geo'");
                        anotherfiled = anotherfiled.Replace("tc.region_name", "tc.region_name as 'Region'");
                        anotherfiled = anotherfiled.Replace("tc.subregion_name", "tc.subregion_name as 'SubRegion'");
                        anotherfiled = anotherfiled.Replace("t.Territory_Name", "t.Territory_Name as 'Territory'");
                        selectedColumns += anotherfiled;
                        n = n + 1;
                    }
                }
                if ((property.Name.Equals("filterByRollup")))
                {

                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() == "Y"))
                    {

                        anotherfiled = selectedColumns.Replace("r.RoleName as 'Partner Type'", "group_concat(DISTINCT RoleName ORDER BY RoleName ) as 'Partner Type'");
                        selectedColumns = anotherfiled;
                    }

                }

                /*fix issue excel no 263.Show data based role*/

                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        role = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        orgid = property.Value.ToString();
                    }
                }

                /*end fix issue excel no 263.Show data based role*/

            }
            query += selectedColumns + " FROM Main_site ss inner join Main_organization o on o.OrgId = ss.OrgId " +
                    " inner join (select  SiteId, RoleId, max(SiteRoleId) maxid from SiteRoles group by SiteId, RoleId) MaxRole on ss.SiteId = MaxRole.SiteId " +
                      " INNER JOIN SiteRoles sr ON MaxRole.maxid = sr.SiteRoleId INNER JOIN Roles r ON r.RoleId = sr.RoleId  " +
                    "left JOIN RoleParams rop ON sr.RoleParamId = rop.RoleParamId inner join Ref_countries tc on ss.SiteCountryCode = tc.countries_code " +
                    "INNER JOIN Ref_territory t ON t.TerritoryId = tc.TerritoryId INNER JOIN MarketType m ON tc.MarketTypeId = m.MarketTypeId " +
                     "where " +
                     "ss.SiteCountryCode = tc.countries_code AND o.OrgId = ss.OrgId AND ss.SiteId = sr.SiteId AND sr.RoleId = r.RoleId  ";


            //query += selectedColumns + " FROM  Main_site ss , Main_organization o, Roles r, SiteRoles sr " +
            // "INNER JOIN RoleParams rop ON sr.RoleParamId = rop.RoleParamId, " +
            // "(select SiteId,group_concat(NULLIF(ITS_ID, '')) as ITS_ID,  group_concat(NULLIF(CSN, '')) as CSN,"+
            // "group_concat(NULLIF(NewParent_CSN, '')) as NewParent_CSN,group_concat(NULLIF(Uparent_CSN, '')) "+
            // "as UParent_CSN from SiteRoles where Status NOT IN ('X','D') and RoleId in ('58','1')  group by SiteId asc) as viewsr , "+
            //"Ref_countries tc INNER join Ref_territory t on t.TerritoryId = tc.TerritoryId inner join MarketType m on tc.MarketTypeId = m.MarketTypeId " +

            // "viewsr.SiteId = ss.SiteId and "+ 

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "ss.SiteName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("partnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "sr.RoleId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("subPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        IList<string> subPartnerType = property.Value.ToString().Split(',').Reverse().ToList<string>();
                        string resQry = "";
                        int ct = 0;
                        foreach (var spt in subPartnerType)
                        {
                            ct = ct + 1;
                            resQry = resQry + "'" + spt + "'";
                            if (ct != subPartnerType.Count)
                            {
                                resQry = resQry + ",";
                            }
                        }
                        query += "(rop.ParamValue is null or rop.ParamValue in (" + resQry + ")) ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("partnerStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "sr.`Status` in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("marketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "m.MarketTypeId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // sb = new StringBuilder();
                        // sb.AppendFormat("select group_concat(quote(geo_code)) as geo_code from Ref_geo where geo_id in ("+property.Value.ToString()+")");
                        // sqlCommand = new MySqlCommand(sb.ToString());
                        // dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        // hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        // geo = JObject.Parse(hasil);

                        // if(i==0 || i>0){query += " and ";} 
                        // query += "tc.geo_code in ("+geo["geo_code"].ToString()+") ";  
                        // i=i+1;

                        if (i == 0 || i > 0) { query += " and "; }
                        query += "tc.geo_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("region"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // sb = new StringBuilder();
                        // sb.AppendFormat("select group_concat(quote(region_code)) as region_code from Ref_region where region_id in ("+property.Value.ToString()+")");
                        // sqlCommand = new MySqlCommand(sb.ToString());
                        // dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        // hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        // region = JObject.Parse(hasil);

                        // if(i==0 || i>0){query += " and ";} 
                        // query += "tc.region_code in ("+region["region_code"].ToString()+") ";  
                        // i=i+1;

                        if (i == 0 || i > 0) { query += " and "; }
                        query += "tc.region_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("subregion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // sb = new StringBuilder();
                        // sb.AppendFormat("select group_concat(quote(subregion_code)) as subregion_code from Ref_subregion where subregion_id in ("+property.Value.ToString()+")");
                        // sqlCommand = new MySqlCommand(sb.ToString());
                        // dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        // hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        // subregion = JObject.Parse(hasil);

                        // if(i==0 || i>0){query += " and ";} 
                        // query += "tc.subregion_code in ("+subregion["subregion_code"].ToString()+") ";  
                        // i=i+1;

                        if (i == 0 || i > 0) { query += " and "; }
                        query += "tc.subregion_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("country"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // sb = new StringBuilder();
                        // sb.AppendFormat("select group_concat(quote(countries_code)) as countries_code from Ref_countries where countries_id in ("+property.Value.ToString()+")");
                        // sqlCommand = new MySqlCommand(sb.ToString());
                        // dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        // hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        // countries = JObject.Parse(hasil);

                        // if(i==0 || i>0){query += " and ";}
                        // query += "RegisteredCountryCode in ("+countries["countries_code"].ToString()+") ";  
                        // i=i+1;

                        if (i == 0 || i > 0) { query += " and "; }
                        query += "ss.SiteCountryCode in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        // query += "t.TerritoryId in (" + property.Value.ToString() + ") ";

                        /*fix issue excel no 263.Show data based role*/

                        if (role == "")
                        {
                            query += "t.TerritoryId in (" + property.Value.ToString() + ") ";
                        }
                        else if (role == "DISTRIBUTOR")
                        {
                            query += "t.TerritoryId in (select rc.TerritoryId from Ref_countries rc INNER JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + orgid + ") GROUP BY rc.TerritoryId) ";
                        }
                        else
                        {
                            query += "t.TerritoryId in (" + property.Value.ToString() + ") AND o.OrgId in (" + orgid + ") ";
                        }

                        /*end fix issue excel no 263.Show data based role*/

                        i = i + 1;
                    }
                }
                if (property.Name.Equals("city"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "ss.SiteCity in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("state"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "ss.SiteStateProvince in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }


                if (property.Name.Equals("filterByRollup"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() == "Y"))
                    {
                        if (i == 0 || i > 0)
                            query += " group by ss.OrgId ";
                        i = i + 1;
                    }
                }

            }

            query += "order by ss.OrgId,ss.SiteId desc";
            List<dynamic> dynamicList = new List<dynamic>();
            try
            {

                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
            }
            return Ok(result);

        }

        //custom get site with checked
        [HttpGet("Affiliate/{id}/{orgid}")]
        public IActionResult Affiliate(string id, string orgid)
        {

            JArray affiliate;
            JArray sites = null;

            try
            {

                if (!string.IsNullOrEmpty(id) && id != "addnew")
                {
                    string[] idsplit = id.Split('-');

                    sb1 = new StringBuilder();
                    sb1.AppendFormat("select * from SiteContactLinks where ContactId in (select ContactId from Contacts_All where ContactIdInt = '" + idsplit[0] + "')");
                    sqlCommand = new MySqlCommand(sb1.ToString());
                    ds1 = oDb.getDataSetFromSP(sqlCommand);
                    result2 = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
                    sites = JArray.Parse(result2);
                }

                /* pupulate status nya by siteroles, poho di ubah (issue 31082018) */

                // string query = "SELECT s.SiteIdInt, s.SiteId, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, " +
                //     "s.OrgId, GROUP_CONCAT(DISTINCT d.KeyValue)  AS `Status`, CONCAT(IFNULL(s.SiteCity,''),' ',IFNULL(s.SiteStateProvince,''),' ',IFNULL(CONCAT('(',s.SiteCountryCode,')'),'')) AS SiteAddress1, SiteCountryCode, geo_name "+
                //     "from Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code INNER JOIN Dictionary d ON sr.`Status` = d.Key AND d.Parent = 'SiteStatus' AND d.Status = 'A' WHERE s.`Status` NOT IN ('X','Z','D') AND trim(sr.`Status`) NOT IN ( 'T', 'X','R') "; /* change from nakarin */

                // if (id != "addnew" && id != "" && id != null)
                // {
                //     string[] idsplit = id.Split('-'); 

                //     if(idsplit.Length > 1){
                //         query += "GROUP BY s.SiteId having OrgId in(select OrgId from Main_organization where OrganizationId =  '" + orgid + "')";
                //     }else{
                //         query += "AND s.SiteId in (select SiteId from SiteContactLinks where ContactId in (select ContactId from Contacts_All where ContactIdInt = '" + id + "')) GROUP BY s.SiteId "; /* hide terminated site */
                //     }
                // }
                // else
                // {
                //     query += "and s.OrgId in(select OrgId from Main_organization where OrganizationId =   '" + orgid + "') GROUP BY s.SiteId";
                // }

                /* pupulate status nya by siteroles, poho di ubah (issue 31082018) */

                /* ganti dulu sama yg ini, buat handle issue detail partner status 14092018 */

                string query = "SELECT SiteIdInt,SiteId,SiteName,OrgId,`Status`,SiteAddress1,SiteCountryCode,geo_name,PartnerStatusDetail,PartnerStatus from " +
                    "(SELECT s.SiteIdInt,s.SiteId,s.SiteName,s.OrgId,GROUP_CONCAT( DISTINCT sr.`Status`) AS PartnerStatus,GROUP_CONCAT( DISTINCT d.KeyValue ) AS `Status`,CONCAT(IFNULL( s.SiteCity, '' ),' ',IFNULL( s.SiteStateProvince, '' ),' ',IFNULL( CONCAT( '(', s.SiteCountryCode, ')' ), '' ) ) AS SiteAddress1,SiteCountryCode,geo_name,GROUP_CONCAT( DISTINCT r.RoleCode,' - ',d.KeyValue SEPARATOR '<br>') as PartnerStatusDetail " +
                    "FROM Main_site s   left join SiteContactLinks scl on scl.SiteId =s.SiteId and  scl.Status NOT IN ('X' , 'D') " +
                     " LEFT join Journals j on scl.ContactId = j.ParentId  LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code INNER JOIN Dictionary d ON sr.`Status` = d.KEY AND d.Parent = 'SiteStatus' AND d.STATUS = 'A' WHERE s.`Status` NOT IN ( 'X', 'Z', 'D' ) AND sr.`Status` NOT IN ('X','D','T') ";

                if (id != "addnew" && id != "" && id != null)
                {
                    string[] idsplit = id.Split('-');

                    if (idsplit.Length > 1)
                    {
                        query += "GROUP BY s.SiteId having OrgId in(select OrgId from Main_organization where OrganizationId =  '" + orgid + "')";
                    }
                    else
                    {
                        query += "AND s.SiteId in (select SiteId from SiteContactLinks where ContactId in (select ContactId from Contacts_All where ContactIdInt = '" + id + "') ) GROUP BY s.SiteId "; /* hide terminated site */
                    }
                }
                else
                {
                    query += "and s.OrgId in(select OrgId from Main_organization where OrganizationId =   '" + orgid + "') GROUP BY s.SiteId";
                }

                query += ") as sc WHERE PartnerStatus NOT IN ( 'T', 'X', 'R' )";

                /* ganti dulu sama yg ini, buat handle issue detail partner status 14092018 */

                sb = new StringBuilder();
                sb.AppendFormat(query);
                // Console.WriteLine("here");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        string encodedata = row[col].ToString();
                        string type = col.DataType.ToString();
                        if (type != "System.DateTime")
                        {
                            encodedata = commonServices.HtmlEncoder(encodedata);
                            row[col] = encodedata;
                        }

                    }

                }
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                affiliate = JArray.Parse(result);

                if (id != "addnew" && id != "" && id != null)
                {
                    foreach (var item in affiliate)
                    {
                        bool isthere = false;
                        foreach (var item2 in sites)
                        {
                            if (item["SiteId"].ToString() == item2["SiteId"].ToString())
                            {
                                isthere = true;
                            }

                            if (isthere)
                            {
                                if (item2["Status"].ToString() == "A")
                                {
                                    item["CheckedSites"] = true;
                                    item["SiteContactLinkId"] = item2["SiteContactLinkId"].ToString();
                                }
                                else if (item2["Status"].ToString() == "X")
                                {
                                    item["CheckedSites"] = false;
                                    item["SiteContactLinkId"] = item2["SiteContactLinkId"].ToString();
                                }
                                break;
                            }
                            else
                            {
                                item["CheckedSites"] = false;
                                item["SiteContactLinkId"] = "InsertNew";
                            }
                        }
                    }
                }

            }
            catch
            {
                affiliate = null;
            }
            finally
            {
            }

            return Ok(affiliate);
        }

        /* affiliate by site search (issue31082018)*/

        [HttpGet("AffiliateBySite/{id}/{siteid}")]
        public IActionResult AffiliateBySite(string id, string siteid)
        {
            try
            {
                string query = "SELECT DISTINCT s.SiteIdInt, s.SiteId, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, " +
                    "s.OrgId, GROUP_CONCAT(DISTINCT d.KeyValue)  AS `Status`, CONCAT(IFNULL(s.SiteCity,''),' ',IFNULL(s.SiteStateProvince,''),' ',IFNULL(CONCAT('(',s.SiteCountryCode,')'),'')) AS SiteAddress1, SiteCountryCode, geo_name " +
                    "from Main_site s LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code LEFT JOIN Dictionary d ON sr.`Status` = d.Key AND d.Parent = 'SiteStatus' AND d.Status = 'A' " +
                    "GROUP BY s.SiteId having s.SiteId in(select SiteId from Main_site where SiteId = '" + siteid + "')";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine("here");
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        /* affiliate by site search (issue31082018)*/

        //custom get site with checked
        [HttpGet("AffiliateContact/{id}")]
        public IActionResult AffiliateContact(string id)
        {

            JArray affiliate;
            JArray sites;
            // JObject contact;
            JObject sitesbyorg;

            try
            {
                sb3 = new StringBuilder();
                sb3.AppendFormat("select * from Main_site where SiteIdInt = '" + id + "'");
                sqlCommand = new MySqlCommand(sb3.ToString());
                ds3 = oDb.getDataSetFromSP(sqlCommand);
                result4 = MySqlDb.GetJSONObjectString(ds3.Tables[0]);
                sitesbyorg = JObject.Parse(result4);

                sb = new StringBuilder();
                sb.AppendFormat("select * from Contacts_All where ContactId in(select ContactId from SiteContactLinks where SiteId in(select SiteId from Main_site where OrgId = '" + sitesbyorg["OrgId"].ToString() + "')) ORDER BY ContactIdInt ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                affiliate = JArray.Parse(result);

                sb1 = new StringBuilder();
                sb1.AppendFormat("select * from SiteContactLinks where SiteId in(select SiteId from Main_site where OrgId = '" + sitesbyorg["OrgId"].ToString() + "') ORDER BY SiteContactLinkId ");
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
                result2 = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
                sites = JArray.Parse(result2);

                foreach (var item in affiliate)
                {
                    item["CheckedContact"] = false;
                }

                if (sites.Count > 0)
                {
                    foreach (var item in affiliate)
                    {
                        foreach (var item2 in sites)
                        {
                            if (item["ContactId"].ToString() == item2["ContactId"].ToString() && item2["SiteId"].ToString() == sitesbyorg["SiteId"].ToString() && item2["Status"].ToString() == "A")
                            {
                                item["CheckedContact"] = true;
                                item["SiteContactLinkId"] = item2["SiteContactLinkId"].ToString();
                                item["InsertNewSite"] = "No";
                            }
                            else if (item["ContactId"].ToString() == item2["ContactId"].ToString() && item2["SiteId"].ToString() == sitesbyorg["SiteId"].ToString() && item2["Status"].ToString() == "X")
                            {
                                item["CheckedContact"] = false;
                                item["SiteContactLinkId"] = item2["SiteContactLinkId"].ToString();
                                item["InsertNewSite"] = "No";
                            }

                            /* buka dulu say, tar tutup lagi kalo ada issue */

                            else if (item["ContactId"].ToString() == item2["ContactId"].ToString() && item2["SiteId"].ToString() != sitesbyorg["SiteId"].ToString())
                            {
                                item["CheckedContact"] = false;
                                item["SiteContactLinkId"] = item2["SiteContactLinkId"].ToString();
                                item["InsertNewSite"] = "Yes";
                            }

                            /* buka dulu say, tar tutup lagi kalo ada issue */
                        }
                    }
                }
                else
                {
                    affiliate = null;
                }


            }
            catch
            {
                affiliate = null;
            }
            finally
            {
            }

            return Ok(affiliate);
        }

        //custom get site with checked
        [HttpGet("InvoicesSites/{id}")]
        public IActionResult InvoicesSites(string id)
        {

            JArray invoicessites;
            JArray sites;
            JObject invoices;

            try
            {



                sb2 = new StringBuilder();
                sb2.AppendFormat("select * from Invoices where InvoiceId = '" + id + "'");
                Console.WriteLine(sb2.ToString());
                sqlCommand = new MySqlCommand(sb2.ToString());
                ds2 = oDb.getDataSetFromSP(sqlCommand);
                result3 = MySqlDb.GetJSONObjectString(ds2.Tables[0]);
                invoices = JObject.Parse(result3);

                sb = new StringBuilder();
                sb.AppendFormat("SELECT s.SiteId, s.OrgId, d.KeyValue AS `Status`, s.SiteIdInt, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, rc.countries_name AS SiteCountryCode FROM Main_site s INNER JOIN Dictionary d ON d.`Key` = (SELECT GROUP_CONCAT( DISTINCT Status ORDER BY Status) FROM SiteRoles WHERE Status <> 'X' AND SiteId = s.SiteId) AND d.`Status` = 'A' INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code WHERE OrgId = '" + invoices["OrgId"].ToString() + "' GROUP BY s.SiteId");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                sites = JArray.Parse(result);


                sb1 = new StringBuilder();
                sb1.AppendFormat("select * from InvoicesSites where SiteId in (select SiteId from Main_site where OrgId = '" + invoices["OrgId"].ToString() + "') and InvoiceId = '" + id + "'");
                Console.WriteLine(sb1.ToString());
                sqlCommand = new MySqlCommand(sb1.ToString());
                ds1 = oDb.getDataSetFromSP(sqlCommand);
                result2 = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
                invoicessites = JArray.Parse(result2);

                foreach (var item in sites)
                {
                    item["CheckedSites"] = false;
                }


                int index = 0;
                foreach (var item in sites)
                {
                    foreach (var item2 in invoicessites)
                    {
                        if (item["SiteId"].ToString() == item2["SiteId"].ToString() && item2["Status"].ToString() == "A")
                        {
                            item["CheckedSites"] = true;
                        }
                    }
                    if (index < invoicessites.Count)
                    {
                        item["InvoicesSitesId"] = invoicessites[index]["InvoicesSitesId"].ToString();
                    }
                    else if (index >= invoicessites.Count)
                    {
                        item["InvoicesSitesId"] = "InsertNew";
                    }
                    index++;
                }
            }
            catch
            {
                sites = null;
            }
            finally
            {
            }

            return Ok(sites);
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {
            List<dynamic> dynamicModel = new List<dynamic>();
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    // "SELECT s.SiteIdInt, s.SiteId, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, " +
                    "SELECT s.SiteIdInt, s.SiteId, s.SiteName, " +
                    "s.EnglishSiteName, s.CommercialSiteName, s.Workstations, s.MagellanId, s.SAPNumber_retired, s.SAPShipTo_retired, s.SiteTelephone, s.SiteFax, " +
                    "s.SiteEmailAddress, s.SiteWebAddress, s.SiteDepartment, s.SiteAddress1, s.SiteAddress2, s.SiteAddress3, s.SiteCity, s.SiteStateProvince, " +
                    "s.SiteCountryCode, s.SitePostalCode, s.MailingDepartment, s.MailingAddress1, s.MailingAddress2, s.MailingAddress3, s.MailingCity, " +
                    "s.MailingStateProvince, s.MailingCountryCode, s.MailingPostalCode, s.ShippingDepartment, s.ShippingAddress1, s.ShippingAddress2, " +
                    "s.ShippingAddress3, s.ShippingCity, s.ShippingStateProvince, s.ShippingCountryCode, s.ShippingPostalCode, s.SiteAdminFirstName, " +
                    "s.SiteAdminLastName, s.SiteAdminEmailAddress, s.SiteAdminTelephone, s.SiteAdminFax, s.SiteManagerFirstName, s.SiteManagerLastName, " +
                    "s.SiteManagerEmailAddress, s.SiteManagerTelephone, s.SiteManagerFax, s.AdminNotes, DATE_FORMAT(s.DateAdded,'%m/%d/%Y') AS DateAdded, s.LastAdminBy, s.OrgId, o.OrganizationId, " +
                    "rc.countries_name, rc.geo_name, rc.region_name, " +
                    // "replace( replace( replace( o.OrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as OrgName, s.`Status` " +
                    "o.OrgName, s.`Status` " +
                    "FROM Main_site s JOIN Main_organization o ON s.OrgId = o.OrgId JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code WHERE " +
                    "s.SiteIdInt = '" + id + "' GROUP BY s.SiteId"
                    );
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        string notes = row[column].ToString();
                        notes = commonServices.HtmlEncoder(notes);
                        row[column] = notes;
                    }
                }

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                if (dynamicModel.Count > 0)
                {
                    //  result = JsonConvert.SerializeObject(dynamicModel.FirstOrDefault(), Formatting.Indented,
                    //  settings);
                    if (dynamicModel.FirstOrDefault() != null)
                    {
                        var returnModel = dynamicModel.FirstOrDefault();
                        string siteId = returnModel.SiteId;
                        var orgSite = _mySQLContext.OrgSiteAttachment.Where(o => o.SiteId == siteId && o.DocType == "Add").Select(o => new { o.Name, o.Id }).ToList();
                        returnModel.Files = orgSite;
                        result = JsonConvert.SerializeObject(dynamicModel.FirstOrDefault(), Formatting.Indented,
                            settings);
                    }

                }
                else
                {
                    result = "Not Found";
                }

                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("sitename/{name}")]
        public IActionResult WhereSiteName(string name)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "select * from Main_site where SiteName = '" + name + "'"
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("partnertype/{id}")]
        public IActionResult WherePartnerTypeId(int id)
        {
            JArray siteroles;
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select ParamValue, RoleName, SiteRoles.*, KeyValue from Main_site " +
                    "LEFT JOIN SiteRoles ON Main_site.SiteId = SiteRoles.SiteId " +
                    "LEFT JOIN Roles ON SiteRoles.RoleId = Roles.RoleId " +
                    "LEFT JOIN RoleParams ON SiteRoles.RoleParamId = RoleParams.RoleParamId " +
                    "LEFT JOIN Dictionary ON SiteRoles.Status = Dictionary.Key AND Dictionary.Parent = 'SiteStatus' AND Dictionary.Status = 'A' " +
                    "where SiteIdInt = '" + id + "' and SiteRoles.Status <> 'X' and SiteRoles.Status <> 'Z' GROUP BY SiteRoleId");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                // siteroles = JArray.Parse(MySqlDb.GetJSONArrayString(ds.Tables[0]));
                // if (siteroles[0]["SiteRoleId"].ToString() == "" || siteroles[0]["SiteRoleId"] == null)
                // {
                //     result = "Not Found";
                // }
                // else
                // {
                //     result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                // }

                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("partnertypeonsiteorg/{id}")]
        public IActionResult WherePartnerTypeOnSiteOrgId(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select Roles.RoleName, Main_site.SiteId from Main_organization join Main_site on Main_organization.OrgId = Main_site.OrgId join SiteRoles on Main_site.SiteId = SiteRoles.SiteId join Roles on SiteRoles.RoleId = Roles.RoleId where SiteRoles.Status <> 'X' and Main_organization.OrgId = '" + id + "' AND SiteRoles.`Status` = 'A'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("partnertypeonorg/{id}")]
        public IActionResult WherePartnerTypeOnOrgId(int id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select DISTINCT Roles.RoleName from Main_organization join Main_site on Main_organization.OrgId = Main_site.OrgId join SiteRoles on Main_site.SiteId = SiteRoles.SiteId join Roles on SiteRoles.RoleId = Roles.RoleId where SiteRoles.Status <> 'X' and Main_organization.OrganizationId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        //custom get site with count contact
        [HttpGet("getsiteswithcountcontact/{id}")]
        public IActionResult WhereSitesCountContact(string id)
        {
            JObject sitesCount;
            JArray sites;
            List<dynamic> dynamicModel = new List<dynamic>();
            try
            {
                // string query = "SELECT s.SiteIdInt, s.SiteId, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, " +
                //     "s.EnglishSiteName, s.CommercialSiteName, s.Workstations, s.MagellanId, s.SAPNumber_retired, s.SAPShipTo_retired, s.SiteTelephone, s.SiteFax, " +
                //     "s.SiteEmailAddress, s.SiteWebAddress, s.SiteDepartment, s.SiteAddress1, s.SiteAddress2, s.SiteAddress3, s.SiteCity, s.SiteStateProvince, " +
                //     "s.SiteCountryCode, s.SitePostalCode, s.MailingDepartment, s.MailingAddress1, s.MailingAddress2, s.MailingAddress3, s.MailingCity, " +
                //     "s.MailingStateProvince, s.MailingCountryCode, s.MailingPostalCode, s.ShippingDepartment, s.ShippingAddress1, s.ShippingAddress2, " +
                //     "s.ShippingAddress3, s.ShippingCity, s.ShippingStateProvince, s.ShippingCountryCode, s.ShippingPostalCode, s.SiteAdminFirstName, " +
                //     "s.SiteAdminLastName, s.SiteAdminEmailAddress, s.SiteAdminTelephone, s.SiteAdminFax, s.SiteManagerFirstName, s.SiteManagerLastName, " +
                //     "s.SiteManagerEmailAddress, d.KeyValue AS Status, s.SiteManagerTelephone, s.SiteManagerFax, s.AdminNotes, DATE_FORMAT(s.DateAdded,'%m/%d/%Y') AS DateAdded, s.LastAdminBy from Main_site s INNER JOIN Dictionary d ON d.`Key` = (SELECT GROUP_CONCAT( DISTINCT Status ORDER BY Status) FROM SiteRoles WHERE Status <> 'X' AND SiteId = s.SiteId) where s.OrgId = '" + id + "' AND s.`Status` != 'X'";
                // sb = new StringBuilder();
                // sb.AppendFormat(query);
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);

                // if (ds.Tables[0].Rows.Count != 0)
                // {
                //     //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                //     ds = MainOrganizationController.EscapeSpecialChar(ds);
                // }

                // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                // sites = JArray.Parse(result);

                // for (int i = 0; i < sites.Count; i++)
                // {
                //     sb1 = new StringBuilder();
                //     sb1.AppendFormat("select SiteContactLinkId from SiteContactLinks where SiteId = '" + sites[i]["SiteId"] + "'");
                //     sqlCommand = new MySqlCommand(sb1.ToString());
                //     ds1 = oDb.getDataSetFromSP(sqlCommand);
                //     result2 = MySqlDb.GetJSONObjectString(ds1.Tables[0]);

                //     if (result2.ToString() == "{}")
                //     {
                //         sites[i]["count"] = 0;
                //     }
                //     else
                //     {
                //         sb2 = new StringBuilder();
                //         sb2.AppendFormat(
                //             "select count(*) as count from Main_site left join SiteContactLinks on Main_site.SiteId = SiteContactLinks.SiteId " +
                //             "left join Main_organization on Main_site.OrgId = Main_organization.OrgId " +
                //             "where SiteContactLinks.SiteId = '" + sites[i]["SiteId"] + "' and Main_site.OrgId = '" + id + "' and SiteContactLinks.`Status` = 'A'"
                //         );
                //         sqlCommand = new MySqlCommand(sb2.ToString());
                //         ds2 = oDb.getDataSetFromSP(sqlCommand);
                //         result3 = MySqlDb.GetJSONObjectString(ds2.Tables[0]);
                //         sitesCount = JObject.Parse(result3);
                //         if (sitesCount["count"] != null)
                //         {
                //             sites[i]["count"] = Int32.Parse(sitesCount["count"].ToString());
                //         }
                //         else
                //         {
                //             sites[i]["count"] = 0;
                //         }
                //     }
                // }


                string query =
                    "select * from (SELECT s.SiteIdInt, s.SiteId, s.SiteName, " +
                    "s.EnglishSiteName, s.CommercialSiteName, s.Workstations, s.MagellanId, s.SAPNumber_retired, s.SAPShipTo_retired, s.SiteTelephone, s.SiteFax, " +
                    "s.SiteEmailAddress, s.SiteWebAddress, s.SiteDepartment, s.SiteAddress1, s.SiteAddress2, s.SiteAddress3, s.SiteCity, s.SiteStateProvince, " +
                    "s.SiteCountryCode, s.SitePostalCode, s.MailingDepartment, s.MailingAddress1, s.MailingAddress2, s.MailingAddress3, s.MailingCity, " +
                    "s.MailingStateProvince, s.MailingCountryCode, s.MailingPostalCode, s.ShippingDepartment, s.ShippingAddress1, s.ShippingAddress2, " +
                    "s.ShippingAddress3, s.ShippingCity, s.ShippingStateProvince, s.ShippingCountryCode, s.ShippingPostalCode, s.SiteAdminFirstName, " +
                    "s.SiteAdminLastName, s.SiteAdminEmailAddress, s.SiteAdminTelephone, s.SiteAdminFax, s.SiteManagerFirstName, s.SiteManagerLastName, " +
                    "s.SiteManagerEmailAddress, (SELECT GROUP_CONCAT( DISTINCT d.KeyValue ORDER BY d.KeyValue) FROM SiteRoles sr INNER JOIN Dictionary d ON d.`Key` = sr.Status WHERE d.Status <> 'X' AND SiteId = s.SiteId order by sr.RoleId) AS Status, s.SiteManagerTelephone, s.SiteManagerFax, s.AdminNotes, DATE_FORMAT(s.DateAdded,'%m/%d/%Y') AS DateAdded, s.LastAdminBy, " +
                    "(select COUNT(c.ContactId) from SiteContactLinks sc INNER JOIN Contacts_All c ON sc.ContactId = c.ContactId AND c.`Status` = 'A' where SiteId = s.SiteId AND sc.`Status`='A') as count, (SELECT GROUP_CONCAT(DISTINCT concat(r.RoleCode ,'(',d.KeyValue,')' ) SEPARATOR ', ') from SiteRoles sr INNER JOIN Roles r ON sr.RoleId = r.RoleId Inner join Dictionary d on d.Key =sr.Status where sr.SiteId = s.SiteId AND sr.Status <> 'X'  and sr.Status <> 'Z' order by sr.RoleId) as PartnerType " +

                    ", CONCAT(s.SiteAddress1,'<br/>',s.SiteCity,', ',(select countries_name from Ref_countries where countries_code = SiteCountryCode)) as Location " + /* add location detail issue 12092018*/
                    "from Main_site s " +
                    "where OrgId = '" + id + "' ) d where PartnerType is not null";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    // ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }

            }
            catch
            {
                // sites = null;
                result = "";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
                //result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            // return Ok(sites);
            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";
            // string convert = "\\\\";
            string territory = "";

            query += "SELECT s.SiteId, o.OrganizationId, o.OrgId, sr.CSN, s.SiteIdInt, rc.geo_name, rc.countries_name, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, GROUP_CONCAT(DISTINCT r.RoleName ) AS PartnerType, GROUP_CONCAT(DISTINCT d.KeyValue ) AS PartnerTypeStatus FROM Main_site s LEFT JOIN Main_organization o ON s.OrgId = o.OrgId LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code LEFT JOIN Contacts_All c ON c.PrimarySiteId = s.SiteId LEFT JOIN Dictionary d ON d.Parent = 'SiteStatus' and sr.`Status` = d.`Key`";
            int i = 0;
            JObject json = JObject.FromObject(o1);

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("UserRoleTerritory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        territory = property.Value.ToString();
                    }
                }
            }

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.OrgId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteStateProvince"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteStateProvince like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteCountryName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteCountryCode in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ContactName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.ContactName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "r.RoleName = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CSN"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.CSN like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.`Status` in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("JustThisOrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.OrgId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Region"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.region_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Subregion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.subregion_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.TerritoryId in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                    else
                    {
                        if (territory != "")
                        {
                            if (i == 0) { query += " where "; }
                            if (i > 0) { query += " and "; }
                            query += "rc.TerritoryId in ( " + territory + " ) ";
                        }
                        i = i + 1;
                    }
                }
            }

            query += " AND s.Status <> 'X' GROUP BY s.SiteName asc";
            //Console.WriteLine(query);

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("FilterSite")]
        public IActionResult FilterSite([FromBody] dynamic data)
        {
            string query = "";
            string tmpsitename = "";
            //string territory="";
            // query += "SELECT s.SiteId, o.OrganizationId, o.OrgId, sr.CSN, s.SiteIdInt, rc.geo_name, rc.countries_name, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, GROUP_CONCAT(DISTINCT r.RoleName ) AS PartnerType, GROUP_CONCAT(DISTINCT d.KeyValue ) AS PartnerTypeStatus FROM Main_site s LEFT JOIN Main_organization o ON s.OrgId = o.OrgId LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code LEFT JOIN Contacts_All c ON c.PrimarySiteId = s.SiteId LEFT JOIN Dictionary d ON d.Parent = 'SiteStatus' and sr.`Status` = d.`Key`";
            query += "SELECT s.SiteId, o.OrganizationId, o.OrgId, s.SiteIdInt, rc.geo_name, rc.countries_name, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName,sr.CSN, " +
                "(SELECT  GROUP_CONCAT(concat(r.RoleCode,'(',d.KeyValue,')','-',sr.CSN) SEPARATOR ', ')  From SiteRoles scl, Roles r, Dictionary d  WHERE s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  and d.Parent = 'SiteStatus'  and d.`Status` = 'A' and d.`Key` = scl.Status) As PartnerType, " +
                "(SELECT GROUP_CONCAT(DISTINCT d.KeyValue ORDER BY d.KeyValue SEPARATOR ', ')  From SiteRoles scl, Roles r, Dictionary d  WHERE s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  and d.Parent = 'SiteStatus' and d.`Status` = 'A' and d.`Key` = scl.Status) AS PartnerTypeStatus " +
                "FROM Main_site s inner JOIN Main_organization o ON s.OrgId = o.OrgId  inner join (select  SiteId, RoleId, max(SiteRoleId) maxid from SiteRoles group by SiteId, RoleId) MaxRole on s.SiteId = MaxRole.SiteId " +
                " INNER JOIN SiteRoles sr ON MaxRole.maxid = sr.SiteRoleId INNER JOIN Roles r ON r.RoleId = sr.RoleId  left join RoleParams rp on rp.RoleParamId =sr.RoleParamId  LEFT JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code LEFT JOIN Contacts_All c ON c.PrimarySiteId = s.SiteId ";
            int i = 0;

            JObject json = JObject.FromObject(data);

            // foreach (JProperty property in json.Properties())
            // {
            //     if(property.Name.Equals("UserRoleTerritory")){   
            //         if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
            //             territory = property.Value.ToString();
            //         }
            //     }
            // }

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.OrgId = '" + property.Value.ToString().Trim() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteId like '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (string.IsNullOrEmpty(tmpsitename))
                        {
                          tmpsitename =  "(s.SiteName like '%" + property.Value.ToString().ToLowerInvariant().Trim() + "%' or s.EnglishSiteName like '%" + property.Value.ToString().ToLowerInvariant().Trim() + "%') ";
                        }
                        //if (i == 0) { query += " where "; }
                        //if (i > 0) { query += " and "; }
                        //query += "(s.SiteName like '%" + property.Value.ToString().ToLowerInvariant().Trim() + "%' or s.EnglishSiteName like '%" + property.Value.ToString().ToLowerInvariant().Trim() + "%') ";
                        //i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteNameSpecial"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (!string.IsNullOrEmpty(tmpsitename))
                        {
                            if (i == 0) { query += " where "; }
                            if (i > 0) { query += " and "; }
                            query += "(" + tmpsitename +  " or (s.SiteName like '%" + property.Value.ToString().ToLowerInvariant().Trim() + "%' or s.EnglishSiteName like '%" + property.Value.ToString().ToLowerInvariant().Trim() + "%')) ";
                            i = i + 1;
                        }

                    }
                }
                if (property.Name.Equals("SiteStateProvince"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteStateProvince in (select StateName from States where StateID in ( " + property.Value.ToString() + " ))";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteCountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteCountryCode in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ContactName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.ContactName like '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "r.RoleCode = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SubPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "(rp.ParamValue IS NULL OR rp.`RoleParamId` in ( " + property.Value.ToString() + " )) ";
                        i = i + 1;
                    }
                }


                if (property.Name.Equals("CSN"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.CSN like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.`Status` in ( " + property.Value.ToString() + " ) ";
                        // query += "s.`Status` in ( " + property.Value.ToString() + " ) "; /* change filter partner type status to status site */
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("JustThisOrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.OrgId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                /* new request nambah user role -__ */

                if (property.Name.Equals("OrgAdmin"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.OrgId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteAdmin"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "s.SiteId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                /* new request nambah user role -__ */

                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Region"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.region_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Subregion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.subregion_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.TerritoryId in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                    // else{
                    //     if(territory != ""){
                    //         if(i==0){query += " where ";}
                    //         if(i>0){query += " and ";} 
                    //         query += "rc.TerritoryId in ( "+territory+" ) "; 
                    //     }
                    //     i = i + 1; 
                    // }
                }
                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query +=
                            "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " ) GROUP BY o.OrgId) > 0 " +
                            "THEN s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) " +
                            "ELSE s.SiteCountryCode IN (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( " + property.Value.ToString() + " )) END ";
                        i = i + 1;
                    }
                }

                // if(property.Name.Equals("SiteAdmin")){   
                //     if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                //         if(i==0){query += " where ";}
                //         if(i>0){query += " and ";} 
                //         query += "s.OrgId IN  ( "+property.Value.ToString()+" ) "; 
                //         i=i+1;
                //     }
                // }
            }

         
                query += "  GROUP BY s.SiteId ORDER BY s.SiteName asc";

           
            List<dynamic> dynamicModel = new List<dynamic>();

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables.Count > 0)
                {
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {
                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
                //result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("whereGeneral/{obj}")]
        public IActionResult whereGeneral(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";
            // string convert = "\\\\";

            query += "SELECT s.SiteId, o.OrganizationId, o.OrgId, sr.CSN, s.SiteIdInt, rc.geo_name, rc.countries_name, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, " +
                "(SELECT GROUP_CONCAT(DISTINCT RoleName ORDER BY RoleName SEPARATOR ', ')  From SiteRoles scl, Roles r  WHERE s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  AND scl.Status <> 'X' ) AS PartnerType, " +
                "(SELECT GROUP_CONCAT(DISTINCT d.KeyValue ORDER BY d.KeyValue SEPARATOR ', ')  From SiteRoles scl, Roles r, Dictionary d  WHERE s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  AND scl.Status <> 'X' and d.Parent = 'SiteStatus' and d.`Status` = 'A' and d.`Key` = scl.Status) AS PartnerTypeStatus " +
                "FROM Main_site s LEFT JOIN Main_organization o ON s.OrgId = o.OrgId LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code LEFT JOIN Contacts_All c ON c.PrimarySiteId = s.SiteId ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("id_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "s.SiteId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("name_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "s.SiteName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("address_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "s.SiteAddress1 like '%" + property.Value.ToString() + "%' or s.SiteAddress2 like '%" + property.Value.ToString() + "%' or s.SiteAddress3 like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("email_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "s.SiteEmailAddress like '%" + property.Value.ToString() + "%' or s.SiteAdminEmailAddress like '%" + property.Value.ToString() + "%' or s.SiteManagerEmailAddress like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("contact_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "c.ContactName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CSN_site") || property.Name.Equals("serialnumber_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "sr.CSN like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("notes_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // if (i == 0) { query += " where "; }
                        // if (i > 0) { query += " or "; }
                        // query += "c.AdminNotes like '%" + property.Value.ToString() + "%' ";

                        if (i == 0) { query += ""; }
                        if (i > 0) { query += ""; }
                        query += "";
                        i = i + 1;
                    }
                }
            }

            query += " GROUP BY s.SiteName asc";

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }


        private string ValidateFile(dynamic files)
        {
            string invalidFile =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Invalid input file\"" +
                "}";
            string exceed =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Maximum file exceed \"" +
                "}";

            string invalidFileSize =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Ivalid file size \"" +
                "}";
            JArray arryFiles = JsonConvert.DeserializeObject(files.ToString());
            if (files.Count > 5)
            {
                return exceed;
            }
            foreach (var item in files)
            {
                string base64String = item.ToString();
                var file = JsonConvert.DeserializeObject<FileModel>(base64String);
                file.metadata = file.data.Remove(file.data.LastIndexOf(','), file.data.Length - file.data.LastIndexOf(','));
                file.type = file.metadata.GetFileTypeFromBase64();
                if (string.IsNullOrEmpty(file.type))
                {
                    return invalidFile;
                }
                file.Content = Convert.FromBase64String(file.data.Remove(0, file.data.LastIndexOf(',') + 1));
                if (file.Content.Length > 2000000)
                {
                    return invalidFileSize;
                }

            }
            return "true";

        }


        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {
            JObject OldData = new JObject();
            var listFile = new List<FileModel>();
            if (data != null)
            {


                if (data.Files != null)
                {
                    var validRes = ValidateFile(data.Files);
                    if (validRes != "true")
                    {
                        return Ok(validRes);
                    }

                    foreach (var item in data.Files)
                    {
                        string base64String = item.ToString();
                        var file = JsonConvert.DeserializeObject<FileModel>(base64String);
                        file.metadata = file.data.Remove(file.data.LastIndexOf(','), file.data.Length - file.data.LastIndexOf(','));
                        file.type = file.metadata.GetFileTypeFromBase64();
                        file.Content = Convert.FromBase64String(file.data.Remove(0, file.data.LastIndexOf(',') + 1));
                        listFile.Add(file);
                    }
                }
            }
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            int MaxSiteIntId = 0;

            JObject o1;
            JObject o2;
            JObject o3 = null;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            //sb = new StringBuilder();
            //sb.AppendFormat("select count(*) as count from Main_organization;");
            //sqlCommand = new MySqlCommand(sb.ToString());
            //ds = oDb.getDataSetFromSP(sqlCommand);

            //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //o1 = JObject.Parse(result);
            //count1 = Int32.Parse(o1["count"].ToString());
            //// Console.WriteLine(count1); 
            //count3 = count1 + 1;

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count, MAX(SiteIdInt) as SiteIdInt from Main_site;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            int isInsert = 0;
            int isComplete = 0;
            JObject site = new JObject();
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            //  count2 = Int32.Parse(o1["count"].ToString());
            MaxSiteIntId = Int32.Parse(o1["SiteIdInt"].ToString());
            MaxSiteIntId = MaxSiteIntId + 1;
            // Console.WriteLine(count21); 
            //  count3 = count2 + 1;






            string SiteId = null;
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select SUBSTRING(c.geo_name,1,2) as Geo from Main_organization m left join Ref_countries c on m.RegisteredCountryCode = c.countries_code" +
                                " where m.OrgId = '" + data.OrgId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);



                //Modified by SMH on 2019-03-05(for duplicate SiteId)
                var SiteName = o3["Geo"].ToString().Substring(0, 2);
                sb = new StringBuilder();
                sb.AppendFormat("select  max(Cast(substring(SiteId, length('" + SiteName + "') +1) as UNSIGNED INTEGER))+1 as SiteIdCount from Main_site where SiteId like '" + SiteName + "%' ;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o1 = JObject.Parse(result);
                count3 = Int32.Parse(o1["SiteIdCount"].ToString());


                SiteId = o3["Geo"].ToString() + count3.ToString("0000");




                #region olddata_beforeupdate

                sb = new StringBuilder();
                sb.AppendFormat("select * from Main_site where SiteId = '" + SiteId + "' and OrgId ='" + data.OrgId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

                #endregion
                //sb = new StringBuilder();
                //sb.AppendFormat("select o.OrganizationId, o.OrgId, replace( replace( replace( o.OrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as OrgName," +
                //" replace( replace( replace( o.EnglishOrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as EnglishOrgName," +
                //" replace( replace( replace( o.CommercialOrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as CommercialOrgName, OrgWebAddress, " +
                //" geo_name from Main_organization o left join Ref_countries on RegisteredCountryCode = countries_code" +
                //" where OrganizationId = (SELECT MAX(OrganizationId) FROM Main_organization)");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);
                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                //o3 = JObject.Parse(result);

                //string nameorgsite = o3["OrgName"].ToString().Replace("\'", "\\\'");

                //added by NX on 29 Nov 2018
                //string geoName = "";
                //string siteId = "";
                //if (o3["geo_name"] != null && !string.IsNullOrEmpty(o3["geo_name"].ToString()))
                //{
                //    geoName = o3["geo_name"].ToString();
                //    if (geoName.Length > 3)
                //    {
                //        siteId = geoName.Substring(0, 2) + count3.ToString("0000");
                //    }
                //}
                //else
                //{
                //    res =
                //        "{" +
                //        "\"code\":\"0\"," +
                //        "\"message\":\"Failed to generate SiteId\"" +
                //        "}";
                //    return Ok(res);
                //}




             sb = new StringBuilder();
                sb.AppendFormat("insert into Main_site " +
                    "(SiteId, OrgId, SiteName, EnglishSiteName, CommercialSiteName, Workstations, MagellanId, SiteTelephone, SiteFax, SiteEmailAddress, SiebelSiteName, SiteWebAddress, " +
                    "SiebelDepartment, SiebelAddress1, SiebelAddress2, SiebelAddress3, SiebelCity, SiebelStateProvince, SiebelCountryCode, SiebelPostalCode, " +
                    "SiteDepartment, SiteAddress1, SiteAddress2, SiteAddress3, SiteCity, SiteStateProvince, SiteCountryCode, SitePostalCode, " +
                    "MailingDepartment, MailingAddress1, MailingAddress2, MailingAddress3, MailingCity, MailingStateProvince, MailingCountryCode, MailingPostalCode, " +
                    "ShippingDepartment, ShippingAddress1, ShippingAddress2, ShippingAddress3, ShippingCity, ShippingStateProvince, ShippingCountryCode, ShippingPostalCode, " +
                    "SiteManagerFirstName, SiteManagerLastName, SiteManagerEmailAddress, SiteManagerTelephone, SiteManagerFax, " +
                    "SiteAdminFirstName, SiteAdminLastName, SiteAdminEmailAddress, SiteAdminTelephone, SiteAdminFax, " +
                    "AdminNotes, Status, DateAdded, AddedBy,DateLastAdmin,LastAdminBy)" +
                    "values ('" + SiteId + "','" + data.OrgId + "','" + data.SiteName + "','" + data.EnglishSiteName + "','" + data.CommercialSiteName + "','" + data.Workstations + "','" + data.MagellanId + "','" + data.SiteTelephone + "','" + data.SiteFax + "','" + data.SiteEmailAddress + "','" + data.SiebelSiteName + "','" + data.SiteWebAddress + "','" +
                    data.SiebelDepartment + "','" + data.SiebelAddress1 + "','" + data.SiebelAddress2 + "','" + data.SiebelAddress3 + "','" + data.SiebelCity + "','" + data.SiebelStateProvince + "','" + data.SiebelCountryCode + "','" + data.SiebelPostalCode + "','" +
                    data.SiteDepartment + "','" + data.SiteAddress1 + "','" + data.SiteAddress2 + "','" + data.SiteAddress3 + "','" + data.SiteCity + "','" + data.SiteStateProvince + "','" + data.SiteCountryCode + "','" + data.SitePostalCode + "','" +
                    data.MailingDepartment + "','" + data.MailingAddress1 + "','" + data.MailingAddress2 + "','" + data.MailingAddress3 + "','" + data.MailingCity + "','" + data.MailingStateProvince + "','" + data.MailingCountryCode + "','" + data.MailingPostalCode + "','" +
                    data.ShippingDepartment + "','" + data.ShippingAddress1 + "','" + data.ShippingAddress2 + "','" + data.ShippingAddress3 + "','" + data.ShippingCity + "','" + data.ShippingStateProvince + "','" + data.ShippingCountryCode + "','" + data.ShippingPostalCode + "','" +
                    data.SiteManagerFirstName + "','" + data.SiteManagerLastName + "','" + data.SiteManagerEmailAddress + "','" + data.SiteManagerTelephone + "','" + data.SiteManagerFax + "','" +
                    data.SiteAdminFirstName + "','" + data.SiteAdminLastName + "','" + data.SiteAdminEmailAddress + "','" + data.SiteAdminTelephone + "','" + data.SiteAdminFax + "','" +
                    data.AdminNotes + "','" + data.Status + "','" + data.DateAdded + "','" + data.AddedBy + "',Now(),'"+ data.cuid + "');"
                );
                // Replaced with NX changes on 29/Nov/2018
                /*"values ('" + o3["geo_name"].ToString().Substring(0, 2) + "" + count23.ToString("0000") + "','" + o3["OrgId"] + "','" + nameorgsite + "','" +
                    o3["EnglishOrgName"] + "','" + o3["CommercialOrgName"] + "','" + o3["OrgWebAddress"] + "','" + data.RegisteredCountryCode + "','','','','',''," +
                    "'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''," +
                    "'','I',NOW(),'" + data.AddedBy + "')"*/
                var st = sb.ToString();
                sqlCommand = new MySqlCommand(sb.ToString());
                isInsert = oDb.temporaryUpdateFunction(sqlCommand);

              

                if (isInsert > 0)
                {
                    if (data.PartnerType.Count > 0)
                    {
                        for (int i = 0; i < data.PartnerType.Count ; i++)
                        {
                            if (data.PartnerType[i][1] == "")
                            {
                                data.PartnerType[i][1] = 2;
                            }
                            if (data.PartnerType[i][2] == "")
                            {
                                data.PartnerType[i][2] = "P";
                            }
                            // string date = data.PartnerType[i][5].ToString();
                            //  if (string.IsNullOrEmpty(data.PartnerType[i][5].ToString())) date = "NOW()";
                            sb = new StringBuilder();
                                sb.AppendFormat(
                                    "insert into SiteRoles " +
                                    "(SiteId, RoleId, DateAdded,RoleParamId, Status,  AddedBy, PartyId, CSN, ParentCSN, UParent_CSN)" +
                                    "values ('" + SiteId + "','" + data.PartnerType[i][0] + "', NOW(), '" + data.PartnerType[i][1] +
                                    "', '" + data.PartnerType[i][2] + "', '" + data.AddedBy + "', '1', '', '', '');"
                                );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            isComplete = oDb.temporaryUpdateFunction(sqlCommand);
                            if (isComplete > 0)
                            {
                                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                                site = JObject.Parse(result);
                            }

                        }
                    }
                    else
                    {


                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "insert into SiteRoles " +
                            "(SiteId, RoleId, RoleParamId,DateAdded, Status, AddedBy, PartyId, CSN, ParentCSN, UParent_CSN)" +
                            "values ('" + SiteId + "', '1','2', NOW(), 'P', '" + data.AddedBy + "', '1', '', '', '' );"
                        );
                        sqlCommand = new MySqlCommand(sb.ToString());
                        isComplete = oDb.temporaryUpdateFunction(sqlCommand);
                        if (isComplete > 0)
                        {
                            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                            site = JObject.Parse(result);
                        }
                    }

                    


                    UpdateMainSiteStatus(SiteId);
                    UpdateOrgStatus(data.OrgId.ToString());
                }
                //NX changes on 27
                if (listFile.Count > 0 && site != null)
                {
                    var listorgSiteAttachment = new List<OrgSiteAttachment>();
                    foreach (var item in listFile)
                    {
                        var orgSiteAttachmentEntity = new OrgSiteAttachment
                        {
                            SiteId = SiteId,
                            OrgId = data.OrgId,
                            Type = item.type,
                            Content = item.Content,
                            DocType = "Add",
                            Name = item.name,
                            Metadata = item.metadata
                        };
                        listorgSiteAttachment.Add(orgSiteAttachmentEntity);
                    }
                    _mySQLContext.OrgSiteAttachment.AddRange(listorgSiteAttachment);
                    _mySQLContext.SaveChanges();
                }

                // sb = new StringBuilder();
                // sb.AppendFormat(
                //     "insert into Main_site "+
                //     "(SiteId, OrgId, SiteName, EnglishSiteName, CommercialSiteName,  SiteWebAddress, Status, SiteCountryCode) "+
                //     "values ('"+o3["region_code"].ToString().Substring(0,2)+""+count23.ToString("0000")+"','"+o3["OrgId"]+"','"+o3["OrgName"]+"','"+o3["EnglishOrgName"]+"','"+o3["CommercialOrgName"]+"','"+o3["OrgWebAddress"]+"','A','"+data.RegisteredCountryCode+"');"
                // );
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch (Exception ex)
            {
            }
            finally
            {
            }

            //sb = new StringBuilder();
            //sb.AppendFormat("select OrganizationId, count(*) as count from Main_organization ORDER BY OrganizationId DESC");
            //sqlCommand = new MySqlCommand(sb.ToString());
            //ds = oDb.getDataSetFromSP(sqlCommand);

            //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //o2 = JObject.Parse(result);
            //count2 = Int32.Parse(o2["count"].ToString());
            //// Console.WriteLine(count2);

            if (isComplete > 0)
            {

                #region newdata_afterupdate
                sb = new StringBuilder();
                sb.AppendFormat("select * from Main_site where SiteId = '" + SiteId + "' and OrgId ='" + data.OrgId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
                #endregion

                _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Main_site, data.UserId.ToString(), data.cuid.ToString(), " where SiteId = " + SiteId + " and OrgId =" + data.OrgId + "");


                //string description = "Insert data Organization" + "<br/>" +
                //    "Organization Id = ORG" + count3.ToString("0000") + "<br/>" +
                //    "OrgName = " + data.OrgName;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());


                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"," +
                    "\"OrganizationId\":\"" + data.OrgId + "\"," +
                    "\"SiteIdInt\":\"" + MaxSiteIntId + "\"" +
                    "}";
            }


            return Ok(res);


        }

        public void UpdateMainSiteStatus(string SiteId)
        {
            try
            {
                var SiteActiveList = _mySQLContext.SiteRoles.Where(x => x.Status.Equals("A") && x.SiteId.Equals(SiteId)).ToList();

                var tmpSitestatus = "";
                var Sitestatus = "";
                if (SiteActiveList.Count > 0)
                {
                    Sitestatus = "A";
                }
                else
                {
                    var SiteLists = _mySQLContext.SiteRoles.Where(x => x.SiteId.Equals(SiteId)).ToList();

                    for (int j = 0; j < SiteLists.Count; j++)
                    {
                        if (j == 0)
                        {
                            tmpSitestatus = SiteLists[j].Status.ToString();
                        }
                        else
                            tmpSitestatus = tmpSitestatus + "," + SiteLists[j].Status.ToString();
                    }
                    if (!String.IsNullOrEmpty(tmpSitestatus))
                    {
                        string[] orgstatuscount = tmpSitestatus.Split(',');
                        if (orgstatuscount.Length == 1)
                        {
                            Sitestatus = tmpSitestatus;
                        }
                        else if (orgstatuscount.Length > 1 && !tmpSitestatus.Contains("A"))
                        {
                            for (int i = 0; i < orgstatuscount.Length; i++)
                            {
                                if (orgstatuscount[i] != orgstatuscount[0])
                                {
                                    Sitestatus = "I";
                                    break;
                                }
                                else
                                {
                                    Sitestatus = orgstatuscount[0];
                                }
                            }

                        }
                        else
                        {
                            if (tmpSitestatus.Contains("A"))
                            {
                                Sitestatus = "A";
                            }
                            else
                            {
                                Sitestatus = "I";
                            }
                        }
                    }

                }


                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Main_site set " +
                     "Status='" + Sitestatus + "' " +
                      "where SiteId='" + SiteId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch (Exception ex)
            {

            }



        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {

            #region olddata_beforeupdate
            
            sb = new StringBuilder();
          //  sb.AppendFormat("select * from Main_site where SiteIdInt =" + id + "");
            sb.AppendFormat(@"select SiteIdInt,SiteId,ATCSiteId,OrgId,SiteStatus_retired,REPLACE(SiteName,"""""""","""") as SiteName,SiebelSiteName,EnglishSiteName,CommercialSiteName,Workstations,MagellanId,SAPNumber_retired,SAPShipTo_retired,SiteTelephone,SiteFax,SiteEmailAddress, SiteWebAddress, SiteDepartment, SiteAddress1, SiteAddress2, SiteAddress3, SiteCity, SiteStateProvince, SiteCountryCode, SitePostalCode, MailingDepartment, MailingAddress1, MailingAddress2, MailingAddress3, MailingCity, MailingStateProvince, MailingCountryCode, MailingPostalCode, ShippingDepartment, ShippingAddress1, ShippingAddress2, ShippingAddress3, ShippingCity, ShippingStateProvince, ShippingCountryCode, ShippingPostalCode, SiebelDepartment, SiebelAddress1, SiebelAddress2, SiebelAddress3, SiebelCity, SiebelStateProvince, SiebelCountryCode, SiebelPostalCode, SiteAdminFirstName, SiteAdminLastName, SiteAdminEmailAddress, SiteAdminTelephone,SiteAdminFax, SiteManagerFirstName, SiteManagerLastName, SiteManagerEmailAddress, SiteManagerTelephone, SiteManagerFax, AdminNotes, Status, DateAdded, AddedBy, DateLastAdmin, LastAdminBy, CSOLocationUUID, CSOLocationName, CSOLocationNumber,CSOAuthorizationCodes, CSOUpdatedOn, CSOVersion, CSOpartner_type from Main_site where SiteIdInt =" + id + ";");
           sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //    var json = JsonConvert.SerializeObject(result);
          
         JObject OldData = JsonConvert.DeserializeObject<JObject>(result);
            #endregion



            var listFile = new List<FileModel>();
            if (data != null)
            {
                if (data.Files != null)
                {
                    var validRes = ValidateFile(data.Files);
                    if (validRes != "true")
                    {
                        return Ok(validRes);
                    }

                    foreach (var item in data.Files)
                    {
                        string base64String = item.ToString();
                        var file = JsonConvert.DeserializeObject<FileModel>(base64String);
                        file.type = file.data.GetFileTypeFromBase64();
                        file.Content = Convert.FromBase64String(file.data.Remove(0, file.data.LastIndexOf(',') + 1));
                        listFile.Add(file);
                    }
                }
            }

            //JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Main_site set " +
                    "SiteName='" + data.SiteName + "'," +
                    "EnglishSiteName='" + data.EnglishSiteName + "'," +
                    "CommercialSiteName='" + data.CommercialSiteName + "'," +
                    "Workstations='" + data.Workstations + "'," +
                    "MagellanId='" + data.MagellanId + "'," +
                    "SiteTelephone='" + data.SiteTelephone + "'," +
                    "SiteFax='" + data.SiteFax + "'," +
                    "SiteEmailAddress='" + data.SiteEmailAddress + "'," +
                    "SiebelSiteName='" + data.SiebelSiteName + "'," +
                    "SiteWebAddress='" + data.SiteWebAddress + "'," +
                    "SiteDepartment='" + data.SiteDepartment + "'," +
                    "SiteAddress1='" + data.SiteAddress1 + "'," +
                    "SiteAddress2='" + data.SiteAddress2 + "'," +
                    "SiteAddress3='" + data.SiteAddress3 + "'," +
                    "SiteCity='" + data.SiteCity + "'," +
                    "SiteStateProvince='" + data.SiteStateProvince + "'," +
                    "SiteCountryCode='" + data.SiteCountryCode + "'," +
                    "SitePostalCode='" + data.SitePostalCode + "'," +
                    "MailingDepartment='" + data.MailingDepartment + "'," +
                    "MailingAddress1='" + data.MailingAddress1 + "'," +
                    "MailingAddress2='" + data.MailingAddress2 + "'," +
                    "MailingAddress3='" + data.MailingAddress3 + "'," +
                    "MailingCity='" + data.MailingCity + "'," +
                    "MailingStateProvince='" + data.MailingStateProvince + "'," +
                    "MailingCountryCode='" + data.MailingCountryCode + "'," +
                    "MailingPostalCode='" + data.MailingPostalCode + "'," +
                    "ShippingDepartment='" + data.ShippingDepartment + "'," +
                    "ShippingAddress1='" + data.ShippingAddress1 + "'," +
                    "ShippingAddress2='" + data.ShippingAddress2 + "'," +
                    "ShippingAddress3='" + data.ShippingAddress3 + "'," +
                    "ShippingCity='" + data.ShippingCity + "'," +
                    "ShippingStateProvince='" + data.ShippingStateProvince + "'," +
                    "ShippingCountryCode='" + data.ShippingCountryCode + "'," +
                    "ShippingPostalCode='" + data.ShippingPostalCode + "'," +
                    "SiebelDepartment='" + data.SiebelDepartment + "'," +
                    "SiebelAddress1='" + data.SiebelAddress1 + "'," +
                    "SiebelAddress2='" + data.SiebelAddress2 + "'," +
                    "SiebelAddress3='" + data.SiebelAddress3 + "'," +
                    "SiebelCity='" + data.SiebelCity + "'," +
                    "SiebelStateProvince='" + data.SiebelStateProvince + "'," +
                    "SiebelCountryCode='" + data.SiebelCountryCode + "'," +
                    "SiebelPostalCode='" + data.SiebelPostalCode + "'," +
                    "SiteAdminFirstName='" + data.SiteAdminFirstName + "'," +
                    "SiteAdminLastName='" + data.SiteAdminLastName + "'," +
                    "SiteAdminEmailAddress='" + data.SiteAdminEmailAddress + "'," +
                    "SiteAdminTelephone='" + data.SiteAdminTelephone + "'," +
                    "SiteAdminFax='" + data.SiteAdminFax + "'," +
                    "SiteManagerFirstName='" + data.SiteManagerFirstName + "'," +
                    "SiteManagerLastName='" + data.SiteManagerLastName + "'," +
                    "SiteManagerEmailAddress='" + data.SiteManagerEmailAddress + "'," +
                    "SiteManagerTelephone='" + data.SiteManagerTelephone + "'," +
                    "SiteManagerFax='" + data.SiteManagerFax + "'," +
                    "AdminNotes='" + data.AdminNotes + "'," +
                    "Status='" + data.Status + "'," +
                    // "DateAdded='"+data.DateAdded+"',"+
                    // "AddedBy='"+data.AddedBy+"',"+
                    "DateLastAdmin= NOW()," +
                    "LastAdminBy='" + data.LastAdminBy + "'" +
                    "where SiteIdInt='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                string tmpStatus = "";
                string realStatus = "";
                for (int i = 0; i < data.PartnerType.Count; i++)
                {
                    if (data.PartnerType[i][1] == "insert")
                    {
                        string date = data.PartnerType[i][5].ToString();
                        if (string.IsNullOrEmpty(data.PartnerType[i][5].ToString())) date = "NOW()";
                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "insert into SiteRoles " +
                            "(SiteId, RoleId, DateAdded, Status, CSN, ParentCSN, UParent_CSN, AccreditationDate, RoleParamId,ContractCSN,ExternalId)" +
                            "values ('" + data.SiteId + "','" + data.PartnerType[i][0] + "', NOW(), '" + data.PartnerType[i][4] +
                            "', '" + data.PartnerType[i][7] + "', '" + data.PartnerType[i][8] + "', '" + data.PartnerType[i][9] +
                            "', " + date + ", '" + data.PartnerType[i][10] + "','',0);"
                        );
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        if (!String.IsNullOrEmpty(tmpStatus))
                        {
                            tmpStatus = tmpStatus + "," + data.PartnerType[i][4].ToString();
                        }
                        else
                        {
                            tmpStatus = data.PartnerType[i][4].ToString();
                        }

                    }
                    else if (data.PartnerType[i][1] == "delete")
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat("delete from SiteRoles where SiteRoleId = '" + data.PartnerType[i][2] + "'");

                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }
                    else if (data.PartnerType[i][1] == "exist")
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "update SiteRoles set " +
                            "Status='" + data.PartnerType[i][4] + "'," +
                            "CSN='" + data.PartnerType[i][7] + "'," +
                            "ParentCSN='" + data.PartnerType[i][8] + "'," +
                            "UParent_CSN='" + data.PartnerType[i][9] + "'," +
                            "RoleParamId='" + data.PartnerType[i][10] + "'," +
                            "AccreditationDate='" + data.PartnerType[i][5] + "'" +
                            "where SiteRoleId='" + data.PartnerType[i][2] + "'"
                        );
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        if (!String.IsNullOrEmpty(tmpStatus))
                        {
                            tmpStatus = tmpStatus + "," + data.PartnerType[i][4].ToString();
                        }
                        else
                        {
                            tmpStatus = data.PartnerType[i][4].ToString();
                        }
                    }
                }
                if (!String.IsNullOrEmpty(tmpStatus))
                {
                    string[] statuscount = tmpStatus.Split(',');
                    if (statuscount.Length == 1)
                    {
                        realStatus = tmpStatus;
                    }
                    else if (statuscount.Length > 1 && !tmpStatus.Contains("A") )
                    {
                        for (int i = 0; i < statuscount.Length; i++)
                        {
                            if (statuscount[i] != statuscount[0])
                            {
                                realStatus = "I";
                                break;
                            }
                            else
                            {
                                realStatus = statuscount[0];
                            }
                        }

                    }
                    else
                    {
                        if (tmpStatus.Contains("A"))
                        {
                            realStatus = "A";
                        }
                        else
                        {
                            realStatus = "I";
                        }
                    }
                }

                if (!String.IsNullOrEmpty(realStatus) && realStatus.Length == 1)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "update Main_site set " +
                         "Status='" + realStatus + "' " +
                          "where SiteIdInt='" + id + "'");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }


                if (listFile?.Count > 0)
                {


                    var listorgSiteAttachment = new List<OrgSiteAttachment>();
                    foreach (var item in listFile)
                    {
                        var orgSiteAttachmentEntity = new OrgSiteAttachment
                        {
                            OrgId = data.OrgId,
                            SiteId = data.SiteId,
                            Type = item.type,
                            Content = item.Content,
                            DocType = "Add",
                            Name = item.name
                        };
                        listorgSiteAttachment.Add(orgSiteAttachmentEntity);
                    }
                    _mySQLContext.OrgSiteAttachment.AddRange(listorgSiteAttachment);
                    _mySQLContext.SaveChanges();


                }

                string OrgId = data.OrgId?.ToString();
                // var isActiveSite = _mySQLContext.MainSite.Where(x => x.Status.Equals("A") && x.OrgId.Equals(OrgId)).ToList();

                //if (isActiveSite.Count >0)
                //{
                // var siteList = _mySQLContext.MainSite.Where(x => x.OrgId.Equals(OrgId)).Select(x => x.SiteId).ToList();
                //    if (siteList.Count > 0)
                //    {
                //        var siteContactList = _mySQLContext.ContactsAll
                //                                                .Join(_mySQLContext.SiteContactLinks, c => c.ContactId, sc => sc.ContactId,
                //                                                 (c, sc) => new { Contact = c, SiteContact = sc })
                //                                                .Where(x => siteList.Contains(x.SiteContact.SiteId)).Select(x => new ContactsAll
                //                                                {
                //                                                    ContactId = x.Contact.ContactId,
                //                                                    ContactName = x.Contact.ContactName,
                //                                                    UserLevelId = x.Contact.UserLevelId,
                //                                                    PrimarySiteId = x.Contact.PrimarySiteId,
                //                                                    Status = "I",
                //                                                    CountryCode = x.Contact.CountryCode,
                //                                                    InstructorId = x.Contact.InstructorId,
                //                                                    EmailAddress = x.Contact.EmailAddress,
                //                                                    TerminationReason = "All Site Are Suspended",
                //                                                    DateAdded = x.Contact.DateAdded,
                //                                                    DateLastAdmin = x.Contact.DateLastAdmin,
                //                                                    LastAdminBy = x.Contact.LastAdminBy,
                //                                                    AddedBy = x.Contact.AddedBy,
                //                                                    PrimaryRoleId = x.Contact.PrimaryRoleId,
                //                                                    AcimemberId = x.Contact.AcimemberId,
                //                                                    Action = x.Contact.Action,
                //                                                    Address1 = x.Contact.Address1,
                //                                                    Address2 = x.Contact.Address2,
                //                                                    Address3 = x.Contact.Address3,
                //                                                    AdminSystems = x.Contact.AdminSystems,
                //                                                    Administrator = x.Contact.Administrator,
                //                                                    Atcrole = x.Contact.Atcrole,
                //                                                    Bio = x.Contact.Bio,
                //                                                    City = x.Contact.City,
                //                                                    Comments = x.Contact.Comments,
                //                                                    CompanyName = x.Contact.CompanyName,
                //                                                    ContactIdInt = x.Contact.ContactIdInt,
                //                                                    CsoupdatedOn = x.Contact.CsoupdatedOn,
                //                                                    CsouserUuid = x.Contact.CsouserUuid,
                //                                                    Csoversion = x.Contact.Csoversion,
                //                                                    DateBirth = x.Contact.DateBirth,
                //                                                    Dedicated = x.Contact.Dedicated,
                //                                                    Department = x.Contact.Department,
                //                                                    Designation = x.Contact.Designation,
                //                                                    DoNotEmail = x.Contact.DoNotEmail,
                //                                                    EmailAddress2 = x.Contact.EmailAddress2,
                //                                                    EmailAddress3 = x.Contact.EmailAddress3,
                //                                                    EmailAddress4 = x.Contact.EmailAddress4,
                //                                                    EnglishContactName = x.Contact.EnglishContactName,
                //                                                    ExperiencedCompetitiveProducts = x.Contact.ExperiencedCompetitiveProducts,
                //                                                    ExperiencedCompetitiveProductsList = x.Contact.ExperiencedCompetitiveProductsList,
                //                                                    FirstName = x.Contact.FirstName,
                //                                                    Gender = x.Contact.Gender,
                //                                                    HireDate = x.Contact.HireDate,
                //                                                    HowLongAutodeskProducts = x.Contact.HowLongAutodeskProducts,
                //                                                    HowLongInIndustry = x.Contact.HowLongInIndustry,
                //                                                    HowLongWithOrg = x.Contact.HowLongWithOrg,
                //                                                    InternalSales = x.Contact.InternalSales,
                //                                                    LastIpaddress = x.Contact.LastIpaddress,
                //                                                    LastLogin = x.Contact.LastLogin,
                //                                                    LastName = x.Contact.LastName,
                //                                                    Level = x.Contact.Level,
                //                                                    LoginCount = x.Contact.LoginCount,
                //                                                    Mobile1 = x.Contact.Mobile1,
                //                                                    Mobile2 = x.Contact.Mobile2,
                //                                                    MobileCode = x.Contact.MobileCode,
                //                                                    Password = x.Contact.Password,
                //                                                    PasswordPlain = x.Contact.PasswordPlain,
                //                                                    PercentPostSales = x.Contact.PercentPostSales,
                //                                                    PercentPreSales = x.Contact.PercentPreSales,
                //                                                    PermissionsGroup = x.Contact.PermissionsGroup,
                //                                                    PostalCode = x.Contact.PostalCode,
                //                                                    PrimaryIndustryId = x.Contact.PrimaryIndustryId,
                //                                                    PrimaryLanguage = x.Contact.PrimaryLanguage,
                //                                                    PrimaryRoleExperience = x.Contact.PrimaryRoleExperience,
                //                                                    PrimarySubIndustryId = x.Contact.PrimarySubIndustryId,
                //                                                    ProfilePicture = x.Contact.ProfilePicture,
                //                                                    PwdchangeRequired = x.Contact.PwdchangeRequired,
                //                                                    QuestionnaireDate = x.Contact.QuestionnaireDate,
                //                                                    ResetPasswordCode = x.Contact.ResetPasswordCode,
                //                                                    ResetPasswordDate = x.Contact.ResetPasswordDate,
                //                                                    ResetToken = x.Contact.ResetToken,
                //                                                    ResetTokenExpiration = x.Contact.ResetTokenExpiration,
                //                                                    Salutation = x.Contact.Salutation,
                //                                                    SecondaryLanguage = x.Contact.SecondaryLanguage,
                //                                                    ShareEmail = x.Contact.ShareEmail,
                //                                                    ShareMobile = x.Contact.ShareMobile,
                //                                                    ShareTelephone = x.Contact.ShareTelephone,
                //                                                    ShowDuplicates = x.Contact.ShowDuplicates,
                //                                                    ShowInSearch = x.Contact.ShowInSearch,
                //                                                    SiebelId = x.Contact.SiebelId,
                //                                                    SiebelStatus = x.Contact.SiebelStatus,
                //                                                    StateProvince = x.Contact.StateProvince,
                //                                                    StatusEmailSend = x.Contact.StatusEmailSend,
                //                                                    Telephone1 = x.Contact.Telephone1,
                //                                                    Telephone2 = x.Contact.Telephone2,
                //                                                    TelephoneCode = x.Contact.TelephoneCode,
                //                                                    TimeZoneUtc = x.Contact.TimeZoneUtc,
                //                                                    Timezone = x.Contact.Timezone,
                //                                                    TitlePosition = x.Contact.TitlePosition,
                //                                                    Username = x.Contact.Username,
                //                                                    WebsiteUrl = x.Contact.WebsiteUrl
                //                                                }).ToList();
                //        if (siteContactList.Count > 0)
                //        {
                //            _mySQLContext.ContactsAll.UpdateRange(siteContactList);
                //            _mySQLContext.SaveChangesAsync();
                //        }
                //    }


                // }
                var isActiveSite = _mySQLContext.MainSite.Where(x => x.Status.Equals("S") && x.OrgId.Equals(OrgId)).ToList();
                if (isActiveSite.Count > 0)
                {
                    var siteList = _mySQLContext.MainSite.Where(x => x.OrgId.Equals(OrgId)).Select(x => x.SiteId).ToList();
                    if (siteList.Count > 0)
                    {
                        var siteContactList = _mySQLContext.ContactsAll
                                                                .Join(_mySQLContext.SiteContactLinks, c => c.ContactId, sc => sc.ContactId,
                                                                 (c, sc) => new { Contact = c, SiteContact = sc })
                                                                .Where(x => siteList.Contains(x.SiteContact.SiteId)).Select(x => new ContactsAll
                                                                {
                                                                    ContactId = x.Contact.ContactId,
                                                                    ContactName = x.Contact.ContactName,
                                                                    UserLevelId = x.Contact.UserLevelId,
                                                                    PrimarySiteId = x.Contact.PrimarySiteId,
                                                                    Status = "I",
                                                                    CountryCode = x.Contact.CountryCode,
                                                                    InstructorId = x.Contact.InstructorId,
                                                                    EmailAddress = x.Contact.EmailAddress,
                                                                    TerminationReason = "All Site Are Suspended",
                                                                    DateAdded = x.Contact.DateAdded,
                                                                    DateLastAdmin = x.Contact.DateLastAdmin,
                                                                    LastAdminBy = x.Contact.LastAdminBy,
                                                                    AddedBy = x.Contact.AddedBy,
                                                                    PrimaryRoleId = x.Contact.PrimaryRoleId,
                                                                    AcimemberId = x.Contact.AcimemberId,
                                                                    Action = x.Contact.Action,
                                                                    Address1 = x.Contact.Address1,
                                                                    Address2 = x.Contact.Address2,
                                                                    Address3 = x.Contact.Address3,
                                                                    AdminSystems = x.Contact.AdminSystems,
                                                                    Administrator = x.Contact.Administrator,
                                                                    Atcrole = x.Contact.Atcrole,
                                                                    Bio = x.Contact.Bio,
                                                                    City = x.Contact.City,
                                                                    Comments = x.Contact.Comments,
                                                                    CompanyName = x.Contact.CompanyName,
                                                                    ContactIdInt = x.Contact.ContactIdInt,
                                                                    CsoupdatedOn = x.Contact.CsoupdatedOn,
                                                                    CsouserUuid = x.Contact.CsouserUuid,
                                                                    Csoversion = x.Contact.Csoversion,
                                                                    DateBirth = x.Contact.DateBirth,
                                                                    Dedicated = x.Contact.Dedicated,
                                                                    Department = x.Contact.Department,
                                                                    Designation = x.Contact.Designation,
                                                                    DoNotEmail = x.Contact.DoNotEmail,
                                                                    EmailAddress2 = x.Contact.EmailAddress2,
                                                                    EmailAddress3 = x.Contact.EmailAddress3,
                                                                    EmailAddress4 = x.Contact.EmailAddress4,
                                                                    EnglishContactName = x.Contact.EnglishContactName,
                                                                    ExperiencedCompetitiveProducts = x.Contact.ExperiencedCompetitiveProducts,
                                                                    ExperiencedCompetitiveProductsList = x.Contact.ExperiencedCompetitiveProductsList,
                                                                    FirstName = x.Contact.FirstName,
                                                                    Gender = x.Contact.Gender,
                                                                    HireDate = x.Contact.HireDate,
                                                                    HowLongAutodeskProducts = x.Contact.HowLongAutodeskProducts,
                                                                    HowLongInIndustry = x.Contact.HowLongInIndustry,
                                                                    HowLongWithOrg = x.Contact.HowLongWithOrg,
                                                                    InternalSales = x.Contact.InternalSales,
                                                                    LastIpaddress = x.Contact.LastIpaddress,
                                                                    LastLogin = x.Contact.LastLogin,
                                                                    LastName = x.Contact.LastName,
                                                                    Level = x.Contact.Level,
                                                                    LoginCount = x.Contact.LoginCount,
                                                                    Mobile1 = x.Contact.Mobile1,
                                                                    Mobile2 = x.Contact.Mobile2,
                                                                    MobileCode = x.Contact.MobileCode,
                                                                    Password = x.Contact.Password,
                                                                    PasswordPlain = x.Contact.PasswordPlain,
                                                                    PercentPostSales = x.Contact.PercentPostSales,
                                                                    PercentPreSales = x.Contact.PercentPreSales,
                                                                    PermissionsGroup = x.Contact.PermissionsGroup,
                                                                    PostalCode = x.Contact.PostalCode,
                                                                    PrimaryIndustryId = x.Contact.PrimaryIndustryId,
                                                                    PrimaryLanguage = x.Contact.PrimaryLanguage,
                                                                    PrimaryRoleExperience = x.Contact.PrimaryRoleExperience,
                                                                    PrimarySubIndustryId = x.Contact.PrimarySubIndustryId,
                                                                    ProfilePicture = x.Contact.ProfilePicture,
                                                                    PwdchangeRequired = x.Contact.PwdchangeRequired,
                                                                    QuestionnaireDate = x.Contact.QuestionnaireDate,
                                                                    ResetPasswordCode = x.Contact.ResetPasswordCode,
                                                                    ResetPasswordDate = x.Contact.ResetPasswordDate,
                                                                    ResetToken = x.Contact.ResetToken,
                                                                    ResetTokenExpiration = x.Contact.ResetTokenExpiration,
                                                                    Salutation = x.Contact.Salutation,
                                                                    SecondaryLanguage = x.Contact.SecondaryLanguage,
                                                                    ShareEmail = x.Contact.ShareEmail,
                                                                    ShareMobile = x.Contact.ShareMobile,
                                                                    ShareTelephone = x.Contact.ShareTelephone,
                                                                    ShowDuplicates = x.Contact.ShowDuplicates,
                                                                    ShowInSearch = x.Contact.ShowInSearch,
                                                                    SiebelId = x.Contact.SiebelId,
                                                                    SiebelStatus = x.Contact.SiebelStatus,
                                                                    StateProvince = x.Contact.StateProvince,
                                                                    StatusEmailSend = x.Contact.StatusEmailSend,
                                                                    Telephone1 = x.Contact.Telephone1,
                                                                    Telephone2 = x.Contact.Telephone2,
                                                                    TelephoneCode = x.Contact.TelephoneCode,
                                                                    TimeZoneUtc = x.Contact.TimeZoneUtc,
                                                                    Timezone = x.Contact.Timezone,
                                                                    TitlePosition = x.Contact.TitlePosition,
                                                                    Username = x.Contact.Username,
                                                                    WebsiteUrl = x.Contact.WebsiteUrl
                                                                }).ToList();
                        if (siteContactList.Count > 0)
                        {
                            _mySQLContext.ContactsAll.UpdateRange(siteContactList);
                            _mySQLContext.SaveChangesAsync();
                        }
                    }


                }


                UpdateOrgStatus(OrgId);


            }
            catch (Exception ex)
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Update Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            sb = new StringBuilder();
            //  sb.AppendFormat("select * from Main_site where SiteIdInt =" + id + ";");
            sb.AppendFormat(@"select SiteIdInt,SiteId,ATCSiteId,OrgId,SiteStatus_retired,REPLACE(SiteName,"""""""","""") as SiteName,SiebelSiteName,EnglishSiteName,CommercialSiteName,Workstations,MagellanId,SAPNumber_retired,SAPShipTo_retired,SiteTelephone,SiteFax,SiteEmailAddress, SiteWebAddress, SiteDepartment, SiteAddress1, SiteAddress2, SiteAddress3, SiteCity, SiteStateProvince, SiteCountryCode, SitePostalCode, MailingDepartment, MailingAddress1, MailingAddress2, MailingAddress3, MailingCity, MailingStateProvince, MailingCountryCode, MailingPostalCode, ShippingDepartment, ShippingAddress1, ShippingAddress2, ShippingAddress3, ShippingCity, ShippingStateProvince, ShippingCountryCode, ShippingPostalCode, SiebelDepartment, SiebelAddress1, SiebelAddress2, SiebelAddress3, SiebelCity, SiebelStateProvince, SiebelCountryCode, SiebelPostalCode, SiteAdminFirstName, SiteAdminLastName, SiteAdminEmailAddress, SiteAdminTelephone,SiteAdminFax, SiteManagerFirstName, SiteManagerLastName, SiteManagerEmailAddress, SiteManagerTelephone, SiteManagerFax, AdminNotes, Status, DateAdded, AddedBy, DateLastAdmin, LastAdminBy, CSOLocationUUID, CSOLocationName, CSOLocationNumber,CSOAuthorizationCodes, CSOUpdatedOn, CSOVersion, CSOpartner_type from Main_site where SiteIdInt =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 

            //string description = "Update data Site <br/>" +
            //        "Organization Id = "+ data.OrgId + "<br/>" +
            //        "Site Id = "+ data.SiteId + "<br/>" +
            //        "SiteName = "+ data.SiteName;


        _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Main_site, data.UserId.ToString(), data.cuid.ToString(), "where SiteIdInt =" + id + "");
            // _audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

            res =
                "{" +
                "\"code\":\"1\"," +
                "\"message\":\"Update Data Success\"" +
                "}";
            //} 

            return Ok(res);
        }

        public void UpdateOrgStatus(string OrgId)
        {
            try
            {
                var OrgActiveList = _mySQLContext.MainSite.Where(x => x.Status.Equals("A") && x.OrgId.Equals(OrgId)).ToList();

                var tmpOrgstatus = "";
                var Orgstatus = "";
                if (OrgActiveList.Count > 0)
                {
                    Orgstatus = "A";
                }
                else
                {
                    var OrgLists = _mySQLContext.MainSite.Where(x => x.OrgId.Equals(OrgId)).ToList();

                    for (int j = 0; j < OrgLists.Count; j++)
                    {
                        if (j == 0)
                        {
                            tmpOrgstatus = OrgLists[j].Status.ToString();
                        }
                        else
                            tmpOrgstatus = tmpOrgstatus + "," + OrgLists[j].Status.ToString();
                    }
                    if (!String.IsNullOrEmpty(tmpOrgstatus))
                    {
                        string[] orgstatuscount = tmpOrgstatus.Split(',');
                        if (orgstatuscount.Length == 1)
                        {
                            Orgstatus = tmpOrgstatus;
                        }
                        else if (orgstatuscount.Length > 1 && !tmpOrgstatus.Contains("A"))
                        {
                            for (int i = 0; i < orgstatuscount.Length; i++)
                            {
                                if (orgstatuscount[i] != orgstatuscount[0])
                                {
                                    Orgstatus = "I";
                                    break;
                                }
                                else
                                {
                                    Orgstatus = orgstatuscount[0];
                                }
                            }

                        }
                        else
                        {
                            if (tmpOrgstatus.Contains("A"))
                            {
                                Orgstatus = "A";
                            }
                            else
                            {
                                Orgstatus = "I";
                            }
                        }
                    }

                }


                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Main_organization set " +
                     "Status='" + Orgstatus + "' " +
                      "where OrgId='" + OrgId + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch (Exception ex)
            {

            }
            


            }

        [HttpPost("Delete")]
        public IActionResult Delete([FromBody] dynamic data)
        {
            string res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Data Failed\"," +
                        "\"SiteId\":\"" + data.siteId + "\"" +
                        "}";
            JObject OldData, NewData = null;
            int? siteIdInt = data.id;

            var userId = HttpContext.User.GetUserId();

            var contact = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactId == userId);

            try
            {
                if (siteIdInt != null && contact != null)
                {
                    var siteEntity = _mySQLContext.MainSite.FirstOrDefault(o => o.SiteIdInt == siteIdInt && o.Status != AutodeskConst.StatusConts.X);
                   

                  

                    if (siteEntity != null)
                    {
                       
                        OldData = JObject.FromObject(siteEntity);

                        siteEntity.Status = AutodeskConst.StatusConts.X;
                        siteEntity.DateLastAdmin = DateTime.Now;
                        siteEntity.LastAdminBy = contact.ContactName;
                        _mySQLContext.MainSite.Update(siteEntity);
                        _mySQLContext.SaveChanges();

                        var siteroleEntity = _mySQLContext.SiteRoles.FirstOrDefault(o => o.SiteId == siteEntity.SiteId);
                        if (siteroleEntity != null)
                        {
                            siteroleEntity.Status = AutodeskConst.StatusConts.X;
                            siteroleEntity.DateLastAdmin = DateTime.Now;
                            siteroleEntity.LastAdminBy = contact.ContactName;

                            _mySQLContext.SiteRoles.Update(siteroleEntity);
                            _mySQLContext.SaveChanges();
                        }

                        NewData = JObject.FromObject(siteEntity);
                        _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Main_site, userId, contact.ContactName, " SiteIdInt  =" + data.id + "");
                    }

                 

                    var listFile = new List<FileModel>();
                    if (data != null)
                    {
                        if (data.files != null && data.files.Count > 0)
                        {
                            var validRes = ValidateFile(data.files);
                            if (validRes != "true")
                            {
                                return Ok(validRes);
                            }

                            foreach (var item in data.files)
                            {
                                string base64String = item.ToString();
                                var file = JsonConvert.DeserializeObject<FileModel>(base64String);
                                file.type = file.data.GetFileTypeFromBase64();
                                file.Content = Convert.FromBase64String(file.data.Remove(0, file.data.LastIndexOf(',') + 1));
                                listFile.Add(file);
                            }
                        }
                    }


                    if (listFile.Count > 0)
                    {
                        var listorgSiteAttachment = new List<OrgSiteAttachment>();
                        foreach (var item in listFile)
                        {
                            var orgSiteAttachmentEntity = new OrgSiteAttachment
                            {
                                OrgId = siteEntity.OrgId,
                                SiteId = siteEntity.SiteId,
                                Type = item.type,
                                Content = item.Content,
                                DocType = "Delete",
                                Name = item.name
                            };
                            listorgSiteAttachment.Add(orgSiteAttachmentEntity);
                        }
                        _mySQLContext.OrgSiteAttachment.AddRange(listorgSiteAttachment);
                        _mySQLContext.SaveChanges();
                    }
                    res =
                          "{" +
                          "\"code\":\"1\"," +
                          "\"message\":\"Delete Data Success\"," +
                          "\"OrgId\":\"" + data.orgId + "\"" +
                          "}";

                }
                return Ok(res);

            }
            catch (Exception e)
            {

                return Ok(res);
            }

        }

        [HttpGet("getSiteCountry/{obj}")]
        public IActionResult WhereCountry(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";
            JArray sitearr;
            JArray sitepartnertypearr;

            query += "select distinct SiteIdInt,CountryCode,Main_site.SiteId, SiteName, CSOpartner_type, OrgId, EDistributorType, PrimaryD from Main_site " +
            "join SiteCountryDistributor on Main_site.SiteId = SiteCountryDistributor.SiteID ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "Main_site.SiteCountryCode in ('" + property.Value.ToString() + "') ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("EDistributorType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "EDistributorType = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
            }

            query += "GROUP BY Main_site.SiteId";

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                sitearr = JArray.Parse(result);

                for (int j = 0; j < sitearr.Count; j++)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat("select Roles.RoleName, Main_site.SiteId from Main_site join SiteRoles on Main_site.SiteId = SiteRoles.SiteId join Roles on SiteRoles.RoleId = Roles.RoleId where Main_site.SiteId = '" + sitearr[j]["SiteId"] + "'");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                    sitepartnertypearr = JArray.Parse(result);

                    string[] partnertype = new string[sitepartnertypearr.Count];
                    if (sitepartnertypearr.Count > 0)
                    {
                        for (int k = 0; k < sitepartnertypearr.Count; k++)
                        {
                            partnertype[k] = sitepartnertypearr[k]["RoleName"].ToString();
                        }
                        sitearr[j]["PartnerType"] = string.Join(", ", partnertype);
                    }
                    else
                    {
                        sitearr[j]["PartnerType"] = "";
                    }
                }
            }
            catch
            {
                sitearr = null;
            }
            finally
            {
            }
            return Ok(sitearr);
        }

        [HttpGet("SiteCourse/{obj}/{value}")]
        public IActionResult SiteCourse(string obj, string value)
        {
            var o1 = JObject.Parse(obj);
            string query = "";
            JObject resultfinal;

            //Tambah RIGHT OUTER JOIN ketika join dari tabel SiteContactLinks ke  tabel Main_site, dan Status Site Innactive ditambahkan karena zwe pas nyoba bikin site baru dia mau add course site baru nya kagak ada
            // query += "SELECT ms.SiteId, replace( replace( replace( ms.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, c.ContactId, c.ContactName FROM Contacts_All c JOIN Main_site ms ON ms.SiteId = c.PrimarySiteId";
            query += "SELECT DISTINCT CONCAT(ms.SiteId,' | ', REPLACE ( REPLACE ( REPLACE ( ms.SiteName, '\"', '\\\\\"' ), '\\\\', '\\\\\\\\' ), '\\\\\"', '\\\"' )) AS Sites FROM Contacts_All c JOIN SiteContactLinks sc ON sc.ContactId = c.ContactId AND sc.`Status` = 'A' RIGHT OUTER JOIN Main_site ms ON sc.SiteId = ms.SiteId JOIN Main_organization o ON ms.OrgId = o.OrgId JOIN SiteRoles sr ON ms.SiteId = sr.SiteId ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Status"))
                {
                    if (i == 0) query += " WHERE ";
                    if (i > 0) query += " AND ";
                    query += "ms.`Status` IN (" + property.Value.ToString() + ") ";
                }

                if (property.Name.Equals("Organization"))
                {
                    if (i == 0) query += " WHERE ";
                    if (i > 0) query += " AND ";
                    query += "o.OrgId in (" + property.Value.ToString() + ") ";
                }

                if (property.Name.Equals("Distributor"))
                {
                    if (i == 0) query += " WHERE ";
                    if (i > 0) query += " AND ";
                    query += "ms.SiteCountryCode IN ( select sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) ";
                }

                if (property.Name.Equals("ContactId"))
                {
                    if (i == 0) query += " WHERE ";
                    if (i > 0) query += " AND ";
                    query += "c.ContactId = '" + property.Value.ToString() + "' ";
                }

                if (property.Name.Equals("SiteId"))
                {
                    if (i == 0) query += " WHERE ";
                    if (i > 0) query += " AND ";
                    query += "ms.SiteId IN (" + property.Value.ToString() + ") ";
                }

                if (property.Name.Equals("PartnerTypeId"))
                {
                    if (i == 0) query += " WHERE ";
                    if (i > 0) query += " AND ";                 
                    query += "sr.RoleId =" + property.Value.ToString()+ " " ;
                }

                if (property.Name.Equals("EditCourse"))
                {
                    if (property.Value.ToString() == "false")
                    {
                        if (i == 0) query += " WHERE ";
                        if (i > 0) query += " AND ";
                        query += " sr.Status = 'A'";
                    }
                }

                i = i + 1;
            }

            if (!string.IsNullOrEmpty(value) && value != "null")
            {
                if (value.Contains("|"))
                {
                    string[] val = value.Split("|");
                     query += " AND ( ms.SiteId LIKE '%" + val[0] + "%' OR ms.SiteName LIKE '%" + val[0] + "%' ) AND sr.`Status` NOT IN ('D','X','T')";
                }
                else
                {
                     query += " AND ( ms.SiteId LIKE '%" + value + "%' OR ms.SiteName LIKE '%" + value + "%' ) AND sr.`Status` NOT IN ('D','X','T')";
                }

            }

            //query += " GROUP BY ms.SiteId ASC";

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                   "{" +
                   "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                   "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }
            return Ok(resultfinal);
        }

        [HttpGet("OldSiteCourse/{obj}/{value}")]
        public IActionResult OldSiteCourse(string obj, string value)
        {
            var o1 = JObject.Parse(obj);
            string query = "";
            JObject resultfinal;

            //Tambah RIGHT OUTER JOIN ketika join dari tabel SiteContactLinks ke  tabel Main_site, dan Status Site Innactive ditambahkan karena zwe pas nyoba bikin site baru dia mau add course site baru nya kagak ada
            // query += "SELECT ms.SiteId, replace( replace( replace( ms.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, c.ContactId, c.ContactName FROM Contacts_All c JOIN Main_site ms ON ms.SiteId = c.PrimarySiteId";
            query += "SELECT DISTINCT CONCAT(ms.SiteId,' | ', REPLACE ( REPLACE ( REPLACE ( ms.SiteName, '\"', '\\\\\"' ), '\\\\', '\\\\\\\\' ), '\\\\\"', '\\\"' )) AS Sites FROM Contacts_All c JOIN SiteContactLinks sc ON sc.ContactId = c.ContactId AND sc.`Status` = 'A' RIGHT OUTER JOIN Main_site ms ON sc.SiteId = ms.SiteId JOIN Main_organization o ON ms.OrgId = o.OrgId JOIN SiteRoles sr ON ms.SiteId = sr.SiteId ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (i == 0)
                {
                    query += " WHERE ";
                }
                if (i > 0)
                {
                    query += " AND ";
                }

                if (property.Name.Equals("Status"))
                {
                    query += "ms.`Status` IN (" + property.Value.ToString() + ") ";
                }

                if (property.Name.Equals("Organization"))
                {
                    query += "o.OrgId in (" + property.Value.ToString() + ") ";
                }

                if (property.Name.Equals("Distributor"))
                {
                    query += "ms.SiteCountryCode IN ( select sd.CountryCode from Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd on sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) ";
                }

                if (property.Name.Equals("ContactId"))
                {
                    query += "c.ContactId = '" + property.Value.ToString() + "' ";
                }

                if (property.Name.Equals("SiteId"))
                {
                    query += "ms.SiteId IN (" + property.Value.ToString() + ") ";
                }

                i = i + 1;
            }

            //if(value.Contains("|")){
            //    string[] val = value.Split("|");
            //    query += " AND ( ms.SiteId LIKE '%" + val[0] + "%' OR ms.SiteName LIKE '%" + val[0] + "%' ) ";
            //} else{
            //    query += " AND ( ms.SiteId LIKE '%" + value + "%' OR ms.SiteName LIKE '%" + value + "%' ) ";
            //}
            if (value.Contains("|"))
            {
                string[] val = value.Split("|");
                query += " AND ( ms.SiteId = '" + val[0] + "') ";
            }
            else
            {
                query += " AND ( ms.SiteId = '" + value + "' ) ";
            }

            //query += " GROUP BY ms.SiteId ASC";

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                   "{" +
                   "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                   "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }
            return Ok(resultfinal);
        }

        [HttpGet("Instructor/{id}")]
        public IActionResult GetInstructorBySiteId(string id)
        {
            string query = "";

            query += "SELECT c.ContactId , CONCAT(InstructorId,' | ',ContactName) AS Instructor, EmailAddress, DATE_FORMAT(LastLogin, '%b %d, %Y') as LastLogin," +
            "(select (case when RoleName = 'Owner/Principal' then 'Owner' else RoleName end) from Roles where RoleId = PrimaryRoleId) as PrimaryRoleId," +
            "ATCRole , PrimarySiteId, CSOUserUUID , Username, Password ,  GROUP_CONCAT(DISTINCT scl.SiteId ORDER BY scl.SiteId SEPARATOR ', ') As RelatedSites " +
            "FROM SiteContactLinks scl, Main_site s, Contacts_All c " +
            "WHERE c.ContactId = scl.ContactId AND s.SiteId = '" + id + "' AND (c.InstructorId <> '' OR c.InstructorId <> null) AND scl.SiteId = s.SiteId  AND scl.Status = 'A'  AND c.Status <> 'X'  AND c.Status <> 'D'  AND s.Status <> 'D' AND s.Status <> 'X' GROUP BY c.ContactId ORDER BY c.ContactName";
            try
            {
                //Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("JournalActivities/{id}/{activity_type}")]
        public IActionResult GetJournal(string id, string activity_type)
        {
            string query = "";

            query += "SELECT ActivityId, ActivityName  FROM JournalActivities  WHERE ActivityType = '" + activity_type + "' AND Status <> 'X' " +
            "AND ActivityId NOT  IN (  SELECT DISTINCT ActivityId  FROM Journals  WHERE ParentId = '" + id + "'  AND Status = 'A' " +
            "AND ActivityId IN (SELECT ActivityId From JournalActivities Where IsUnique = 'Y') ) ORDER BY ActivityName ASC";
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("ATC_AAP_REPORT/{role}/{orgid}")]
        public IActionResult ATC_AAP_REPORT(string role, string orgid)
        {

            string whereclause = "";

            if (role == "Distributor")
            {
                whereclause = "WHERE s.SiteCountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + orgid + " ))";
            }
            else if (role == "SiteAdmin")
            {
                whereclause = "WHERE s.OrgId IN (" + orgid + ")";
            }


            JArray arr1;
            string arrstring1 = "";
            string query1 =
                "select rc.countries_name as'Country', rc.geo_name as 'GEO', rc.region_name as 'Region', rc.subregion_name as 'Sub-region', rt.Territory_Name as 'Territory Name', mt.MarketType as 'Market Name' , COUNT(DISTINCT o.OrgId) as 'Active Training Partner ORGs' from " +
                "(select SiteId from " +
                "(select GROUP_CONCAT(DISTINCT RoleId) as OnlyATC, SiteId from SiteRoles where `Status` = 'A' GROUP BY SiteId ) as atconly ) as sr " +
                "INNER JOIN Main_site s ON sr.SiteId = s.SiteId  " +
                "INNER JOIN Main_organization o ON s.OrgId = o.OrgId   " +
                "INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
                "INNER JOIN Ref_territory rt ON rc.TerritoryId = rt.TerritoryId " +
                "INNER JOIN MarketType mt ON rc.MarketTypeId = mt.MarketTypeId " +
                "" + whereclause + " GROUP BY rc.countries_name";

            JArray arr2;
            string arrstring2 = "";
            string query2 =
                "select rc.countries_name, COUNT(DISTINCT s.SiteId) as 'Active ATC only Sites' from " +
                "(select SiteId from " +
                "(select GROUP_CONCAT(DISTINCT RoleId) as OnlyATC, SiteId from SiteRoles where `Status` = 'A' GROUP BY SiteId ) as atconly where OnlyATC in ('1') ) as sr " +
                "INNER JOIN Main_site s ON sr.SiteId = s.SiteId  " +
                "INNER JOIN Main_organization o ON s.OrgId = o.OrgId " +
                "INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
                "" + whereclause + " GROUP BY rc.countries_name";

            JArray arr3;
            string arrstring3 = "";
            string query3 =
                "select rc.countries_name, COUNT(DISTINCT s.SiteId) as 'Active Academic Partner only' from " +
                "(select SiteId from " +
                "(select GROUP_CONCAT(DISTINCT RoleId) as OnlyATC, SiteId from SiteRoles where `Status` = 'A' GROUP BY SiteId ) as atconly where OnlyATC in ('58') ) as sr " +
                "INNER JOIN Main_site s ON sr.SiteId = s.SiteId " +
                "INNER JOIN Main_organization o ON s.OrgId = o.OrgId " +
                "INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
                "" + whereclause + " GROUP BY rc.countries_name";

            JArray arr4;
            string arrstring4 = "";
            string query4 =
                "select rc.countries_name, COUNT(DISTINCT s.SiteId) as 'Active ATC and AAP' from " +
                "(select SiteId from " +
                "(select GROUP_CONCAT(DISTINCT RoleId) as OnlyATC, SiteId from SiteRoles where `Status` = 'A' GROUP BY SiteId ) as atconly where OnlyATC in ('1,58','58,1') ) as sr " +
                "INNER JOIN Main_site s ON sr.SiteId = s.SiteId  " +
                "INNER JOIN Main_organization o ON s.OrgId = o.OrgId  " +
                "INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
                "" + whereclause + " GROUP BY rc.countries_name";

            JArray arr5;
            string arrstring5 = "";
            string query5 =
                "select rc.countries_name, COUNT(DISTINCT s.SiteId) as 'Total Training Partner sites' from " +
                "(select SiteId from " +
                "(select GROUP_CONCAT(DISTINCT RoleId) as OnlyATC, SiteId from SiteRoles where `Status` = 'A' GROUP BY SiteId ) as atconly where OnlyATC in ('1,58','58,1','1','58') ) as sr " +
                "INNER JOIN Main_site s ON sr.SiteId = s.SiteId  " +
                "INNER JOIN Main_organization o ON s.OrgId = o.OrgId  " +
                "INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
                "" + whereclause + " GROUP BY rc.countries_name";

            JArray arr6;
            string arrstring6 = "";
            string query6 =
               //"SELECT rc.countries_name, COUNT(DISTINCT ContactId) as ATCInstructor FROM " +
               //"(SELECT SiteId,ContactId FROM " +
               //// "(select GROUP_CONCAT(DISTINCT RoleId) as OnlyATC, SiteId from SiteRoles where `Status` = 'A' GROUP BY SiteId ) as atconly where OnlyATC in ('1,58','58,1','1','58') ) as sr "+
               //"(SELECT ss.ContactId,GROUP_CONCAT(DISTINCT RoleId) AS OnlyATC, ss.SiteId FROM SiteContactLinks ss INNER JOIN SiteRoles rr ON rr.SiteId = ss.SiteId AND rr.Status = 'A' " +
               //"INNER JOIN Contacts_All c ON ss.ContactId = c.ContactId AND c.`Status` = 'A' AND PrimaryRoleId = '23' WHERE ss.Status = 'A' GROUP BY ss.ContactId) as atconly WHERE OnlyATC in ('1,58','58,1','1','58') ) as sr " +
               //"INNER JOIN Main_site s ON sr.SiteId = s.SiteId AND s.`Status` = 'A' " +
               //"INNER JOIN Main_organization o ON s.OrgId = o.OrgId AND o.`Status` = 'A' " +
               //"INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
               ////"INNER JOIN SiteContactLinks sc ON s.SiteId = sc.SiteId AND sc.`Status` = 'A' "+
               ////"INNER JOIN Contacts_All c ON sc.ContactId = c.ContactId AND c.`Status` = 'A' and PrimaryRoleId = '23' "+
               "SELECT  rc.countries_name, COUNT(Distinct ContactId) AS 'ATC Instructors' " +
               "FROM(SELECT SiteId, ContactId FROM " +
               "(SELECT ss.ContactId, GROUP_CONCAT(DISTINCT RoleId) AS OnlyATC, ss.SiteId as 'SiteId' " +
               "FROM SiteContactLinks ss INNER JOIN SiteRoles rr ON rr.SiteId = ss.SiteId AND rr.Status = 'A' " +
               "INNER JOIN Contacts_All c ON ss.ContactId = c.ContactId AND c.`Status` = 'A' " +
               "and(c.InstructorId IS NOT NULL or  c.InstructorId <> 'None') " +
               "WHERE ss.Status = 'A' GROUP BY ss.ContactId) AS atconly " +
               "WHERE OnlyATC IN('1,58', '58,1', '1', '58')  union all " +
               "SELECT SiteId, ContactId FROM(SELECT ss.ContactId, GROUP_CONCAT(DISTINCT RoleId) AS OnlyATC, ss.PrimarySiteId as 'SiteId' " +
               "FROM Contacts_All ss INNER JOIN SiteRoles rr ON rr.SiteId = ss.PrimarySiteId AND rr.Status = 'A' AND ss.`Status` = 'A' " +
               "and(ss.InstructorId IS NOT NULL or  ss.InstructorId <> 'None') " +
               "WHERE ss.Status = 'A' GROUP BY ss.ContactId) AS atconly WHERE OnlyATC IN('1,58', '58,1', '1', '58')) as sr " +
               "INNER JOIN Main_site s ON sr.SiteId = s.SiteId AND s.`Status` = 'A' " +
               "INNER JOIN Main_organization o ON s.OrgId = o.OrgId " +
               "INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
               "" + whereclause + " GROUP BY rc.countries_name";

            //JArray arr7;
            //string arrstring7 = "";
            //string query7 =
            //    "select rc.countries_name, COUNT(p.productId) as count, GROUP_CONCAT(DISTINCT productName) as productname " +
            //    "from (select SiteId, productId FROM (select CourseId, productId " +
            //    "from CourseSoftware) as cs " +
            //    "INNER JOIN Courses c ON cs.CourseId = c.CourseId AND PartnerType in ('ATC','AAP Course','AAP Event','AAP Project') AND FYIndicatorKey = '2016' GROUP BY SiteId) as c " +
            //    "INNER JOIN Products p ON c.productId = p.productId " +
            //    "INNER JOIN Main_site s ON c.SiteId = s.SiteId AND s.`Status` = 'A' " +
            //    "INNER JOIN Main_organization o ON s.OrgId = o.OrgId AND o.`Status` = 'A' " +
            //    "INNER JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
            //    "GROUP BY rc.countries_name,p.productId";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query1);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                arrstring1 = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr1 = JArray.Parse(arrstring1);

                sb = new StringBuilder();
                sb.AppendFormat(query2);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                arrstring2 = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr2 = JArray.Parse(arrstring2);

                foreach (var itemarr1 in arr1)
                {
                    itemarr1["Active ATC only Sites"] = 0;
                    foreach (var itemarr2 in arr2)
                    {
                        if (itemarr1["Country"].ToString() == itemarr2["countries_name"].ToString())
                        {
                            itemarr1["Active ATC only Sites"] = itemarr2["Active ATC only Sites"];
                        }
                    }
                }

                sb = new StringBuilder();
                sb.AppendFormat(query3);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                arrstring3 = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr3 = JArray.Parse(arrstring3);

                foreach (var itemarr1 in arr1)
                {
                    itemarr1["Active Academic Partner only"] = 0;
                    foreach (var itemarr3 in arr3)
                    {
                        if (itemarr1["Country"].ToString() == itemarr3["countries_name"].ToString())
                        {
                            itemarr1["Active Academic Partner only"] = itemarr3["Active Academic Partner only"];
                        }
                    }
                }

                sb = new StringBuilder();
                sb.AppendFormat(query4);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                arrstring4 = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr4 = JArray.Parse(arrstring4);

                foreach (var itemarr1 in arr1)
                {
                    itemarr1["Active ATC and AAP"] = 0;
                    foreach (var itemarr4 in arr4)
                    {
                        if (itemarr1["Country"].ToString() == itemarr4["countries_name"].ToString())
                        {
                            itemarr1["Active ATC and AAP"] = itemarr4["Active ATC and AAP"];
                        }
                    }
                }

                sb = new StringBuilder();
                sb.AppendFormat(query5);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                arrstring5 = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr5 = JArray.Parse(arrstring5);

                foreach (var itemarr1 in arr1)
                {
                    itemarr1["Total Training Partner sites"] = 0;
                    foreach (var itemarr5 in arr5)
                    {
                        if (itemarr1["Country"].ToString() == itemarr5["countries_name"].ToString())
                        {
                            itemarr1["Total Training Partner sites"] = itemarr5["Total Training Partner sites"];
                        }
                    }
                }

                sb = new StringBuilder();
                sb.AppendFormat(query6);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                arrstring6 = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                arr6 = JArray.Parse(arrstring6);

                foreach (var itemarr1 in arr1)
                {
                    itemarr1["ATC Instructors"] = 0;
                    foreach (var itemarr6 in arr6)
                    {
                        if (itemarr1["Country"].ToString() == itemarr6["countries_name"].ToString())
                        {
                            itemarr1["ATC Instructors"] = itemarr6["ATC Instructors"];
                        }
                    }
                }

                //sb = new StringBuilder();
                //sb.AppendFormat(query7);
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);
                //arrstring7 = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                //arr7 = JArray.Parse(arrstring7);

                //foreach (var itemarr1 in arr1)
                //{
                //    foreach (var itemarr7 in arr7)
                //    {
                //        itemarr1[itemarr7["productname"].ToString()] = "0";
                //    }
                //    foreach (var itemarr7 in arr7)
                //    {
                //        if (itemarr1["countries_name"].ToString() == itemarr7["countries_name"].ToString())
                //        {
                //            itemarr1[itemarr7["productname"].ToString()] = itemarr7["count"];
                //        }
                //    }
                //}
            }
            catch(Exception ex)
            {
                arr1 = null;
            }
            finally
            {
            }

            return Ok(arr1);
        }

        [HttpPost("ATC_AAP")]
        public IActionResult SelectActivityBoth([FromBody] dynamic data)
        {
            string query = "";

            query += "SELECT s.SiteCountryCode, c.countries_name, c.geo_name, r.region_name, c.subregion_name, COUNT(s.SiteId) AS ActiveATCOnly, t.Territory_Name, (SELECT MarketType FROM MarketType m where m.MarketTypeId = c.MarketTypeId) AS market_name FROM Main_site s, Ref_countries c, Ref_region r, Ref_territory t " +
            "WHERE SiteCountryCode IS NOT NULL AND SiteCountryCode <> '' AND s.SiteCountryCode = c.countries_code AND c.region_code = r.region_code AND c.geo_code = r.geo_code AND s.STATUS = 'A' AND s.SiteId IN " +
            "(SELECT SiteId FROM SiteRoles sr WHERE s.SiteId = sr.SiteId";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("PartnerStatus_1"))
                {
                    if ((property.Value.ToString() != null) && (property.Value.ToString() != ""))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "sr.Status = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType_1"))
                {
                    if ((property.Value.ToString() != null) && (property.Value.ToString() != ""))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "sr.RoleId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
            }

            query += "AND s.SiteId NOT IN (SELECT SiteId FROM SiteRoles WHERE ";

            int j = 0;
            JObject sjson = JObject.FromObject(data);
            foreach (JProperty property in sjson.Properties())
            {
                if (property.Name.Equals("PartnerStatus_2"))
                {
                    if ((property.Value.ToString() != null) && (property.Value.ToString() != ""))
                    {
                        query += "Status = '" + property.Value.ToString() + "' ";
                        j = j + 1;
                    }
                }

                if (property.Name.Equals("PartnerType_2"))
                {
                    if ((property.Value.ToString() != null) && (property.Value.ToString() != ""))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "RoleId = '" + property.Value.ToString() + "' ";
                        j = j + 1;
                    }
                }
            }

            query += ")) GROUP BY s.SiteCountryCode ORDER BY c.countries_name";
            List<dynamic> dynamicModel = new List<dynamic>();

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                    DataTable dt = ds.Tables[0];
                    dynamicModel = dataService.ToDynamic(dt);
                }
            }
            catch
            {
                result = "[]";
            }
            //finally
            //{
            //    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            //}
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("SiteAccreditationReport/{obj}")]
        public IActionResult SiteAccreditation(string obj)
        {
            var o1 = JObject.Parse(obj);
            JArray columnOrg;
            JArray columnSite;
            string query = "";
            string selectedColumns = "";

            selectedColumns += "select DISTINCT ";

            int n = 0;
            JObject column = JObject.FromObject(o1);
            foreach (JProperty property in column.Properties())
            {
                if (property.Name.Equals("FieldOrg"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        selectedColumns += string.Join(",", property.Value.ToString());
                        n = n + 1;
                    }
                    else
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat("DESCRIBE Main_organization");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        columnOrg = JArray.Parse(hasil);

                        string[] columnarrtmp = new string[columnOrg.Count];
                        for (int index = 0; index < columnOrg.Count; index++)
                        {
                            columnarrtmp[index] = "o." + columnOrg[index]["Field"].ToString();
                        }
                        selectedColumns += string.Join(",", columnarrtmp);
                        n = n + 1;
                    }
                }

                if (property.Name.Equals("FieldSite"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (selectedColumns != "")
                        {
                            selectedColumns += "," + string.Join(",", property.Value.ToString());
                        }
                        n = n + 1;
                    }
                    else
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat("DESCRIBE Main_site");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        columnSite = JArray.Parse(hasil);

                        string[] columnarrtmp = new string[columnSite.Count];
                        for (int index = 0; index < columnSite.Count; index++)
                        {
                            if (columnSite[index]["Field"].ToString() != "SiteIdInt")
                            {
                                columnarrtmp[index] = "s." + columnSite[index]["Field"].ToString();
                            }
                        }
                        selectedColumns += string.Join(",", columnarrtmp);
                        n = n + 1;
                    }
                }

                if (property.Name.Equals("AnotherField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n > 0) { selectedColumns += "," + string.Join(",", property.Value.ToString()); }
                        n = n + 1;
                    }
                }
            }

            query += selectedColumns + " FROM Main_site s, Journals j, JournalActivities ja, Main_organization o, SiteRoles sr, " +
            "(SELECT SiteId, group_concat( NULLIF( ITS_ID, '' ) ) AS ITS_ID, group_concat( NULLIF( CSN, '' ) ) AS CSN, group_concat( NULLIF( NewParent_CSN, '' ) ) AS NewParent_CSN, group_concat( NULLIF( Uparent_CSN, '' ) ) AS UParent_CSN" +
            " FROM SiteRoles WHERE STATUS NOT IN ('X','D') ";

            JObject partner = JObject.FromObject(o1);
            foreach (JProperty property in partner.Properties())
            {
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND RoleId IN (" + property.Value.ToString() + ") GROUP BY SiteId ASC) AS viewsr,";
                    }
                    else
                    {
                        query += "GROUP BY SiteId ASC) AS viewsr,";
                    }
                }
            }

            query += " Ref_countries c LEFT JOIN Ref_territory t ON t.TerritoryId = c.TerritoryId INNER JOIN MarketType m ON c.MarketTypeId = m.MarketTypeId " +
            "WHERE s.SiteId = j.ParentId AND s.OrgId = o.OrgId AND SiteCountryCode = c.countries_code AND viewsr.SiteId = s.SiteId AND s.SiteId = sr.SiteId AND sr.STATUS <> 'X' AND o.STATUS <> 'X' AND o.STATUS <> 'D' AND j.STATUS <> 'X' " +
            "AND ja.STATUS <> 'X' AND s.STATUS <> 'X' AND s.STATUS <> 'D' ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("IsPresent"))
                {
                    if ((property.Value.ToString() != null) && (property.Value.ToString() == "Present"))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "j.ActivityId = ja.ActivityId";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ActivityId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "j.ActivityId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ActivityDate"))
                {
                    if ((property.Value.ToString() != ",") && (property.Value.ToString() != null))
                    {
                        string[] dateRange = property.Value.ToString().Split(',');
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "j.ActivityDate BETWEEN '" + dateRange[0] + "' AND '" + dateRange[1] + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteCountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "SiteCountryCode IN (SELECT countries_code FROM Ref_countries WHERE countries_id IN (" + property.Value.ToString() + ")) ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "sr.RoleId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("PartnerStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "sr.STATUS IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("MarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "m.MarketTypeId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " AND "; }
                        query += "t.TerritoryId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

            }

            query += " ORDER BY s.SiteId";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("ListInvoiceSite/{id}")]
        public IActionResult InvoiceSite(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT s.SiteId, s.OrgId, d.KeyValue AS `Status`, s.SiteIdInt, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, rc.countries_name AS SiteCountryCode FROM Main_site s LEFT JOIN Dictionary d ON d.`Key` = (SELECT GROUP_CONCAT( DISTINCT Status ORDER BY Status) FROM SiteRoles WHERE Status <> 'X' AND SiteId = s.SiteId) AND d.`Status` = 'A' INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code WHERE OrgId = '" + id + "' GROUP BY s.SiteId");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost("FilterGeneral")]
        public IActionResult FilterGeneral([FromBody] dynamic data)
        {
            string query = "";
            JObject json = JObject.FromObject(data);
            query += "SELECT s.SiteId, o.OrganizationId, o.OrgId, sr.CSN, s.SiteIdInt, rc.geo_name, rc.countries_name, s.SiteName, " +
                "(SELECT GROUP_CONCAT(DISTINCT RoleName ORDER BY RoleName SEPARATOR ', ')  From SiteRoles scl, Roles r  WHERE s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  AND scl.Status <> 'X' ) AS PartnerType, " +
                "(SELECT GROUP_CONCAT(DISTINCT d.KeyValue ORDER BY d.KeyValue SEPARATOR ', ')  From SiteRoles scl, Roles r, Dictionary d  WHERE s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  AND scl.Status <> 'X' and d.Parent = 'SiteStatus' and d.`Status` = 'A' and d.`Key` = scl.Status) AS PartnerTypeStatus " +
                "FROM Main_site s LEFT JOIN Main_organization o ON s.OrgId = o.OrgId LEFT JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Roles r ON sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code LEFT JOIN Contacts_All c ON c.PrimarySiteId = s.SiteId ";
            int i = 0;

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("id_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where ( "; }
                        if (i > 0) { query += " or "; }
                        query += "s.SiteId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("name_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where ( "; }
                        if (i > 0) { query += " or "; }
                        query += "s.SiteName like '%" + property.Value.ToString().ToLowerInvariant().Trim() + "%' or s.EnglishSiteName like '%" + property.Value.ToString().ToLowerInvariant().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("address_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where ( "; }
                        if (i > 0) { query += " or "; }
                        query += "s.SiteAddress1 like '%" + property.Value.ToString() + "%' or s.SiteAddress2 like '%" + property.Value.ToString() + "%' or s.SiteAddress3 like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("email_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where ( "; }
                        if (i > 0) { query += " or "; }
                        query += "s.SiteEmailAddress like '%" + property.Value.ToString() + "%' or s.SiteAdminEmailAddress like '%" + property.Value.ToString() + "%' or s.SiteManagerEmailAddress like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("contact_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where ( "; }
                        if (i > 0) { query += " or "; }
                        query += "c.ContactName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CSN_site") || property.Name.Equals("serialnumber_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where ( "; }
                        if (i > 0) { query += " or "; }
                        query += "sr.CSN like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("notes_site"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += ""; }
                        if (i > 0) { query += ""; }
                        query += "";
                        i = i + 1;
                    }
                }
            }

            query += " )";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " ) ) ";
                    }
                }
            }

            if (query.Contains("where"))
            {
                query += " And sr.`Status` in ( 'A','V','X','I','O','P','R','B','S','T' ) AND s.Status NOT IN ('X','Z','D') GROUP BY s.SiteId asc";
            }
            else
            {
                query += " WHERE sr.`Status` in ( 'A','V','X','I','O','P','R','B','S','T' ) And s.Status NOT IN ('X','Z','D') GROUP BY s.SiteId asc";
            }

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        //custom get site with checked for distributor
        [HttpGet("AffiliateDistributor/{id}/{orgid}/{orgdistributor}")]
        public IActionResult AffiliateDistributor(string id, string orgid, string orgdistributor)
        {

            JArray affiliate;
            JArray sites = null;

            try
            {

                if (id != "addnew" && id != "" && id != null)
                {
                    string[] idsplit = id.Split('-');

                    sb1 = new StringBuilder();
                    sb1.AppendFormat("select * from SiteContactLinks where ContactId in (select ContactId from Contacts_All where ContactIdInt = '" + idsplit[0] + "')");
                    sqlCommand = new MySqlCommand(sb1.ToString());
                    ds1 = oDb.getDataSetFromSP(sqlCommand);
                    result2 = MySqlDb.GetJSONArrayString(ds1.Tables[0]);
                    sites = JArray.Parse(result2);
                }

                // string query = "SELECT s.SiteIdInt, s.SiteId, replace( replace( replace( s.SiteName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) AS SiteName, " +
                // "s.OrgId, GROUP_CONCAT(DISTINCT d.KeyValue) as `Status`, CONCAT(IFNULL(s.SiteCity,''),' ',IFNULL(s.SiteStateProvince,''),' ',IFNULL(CONCAT('(',s.SiteCountryCode,')'),'')) AS SiteAddress1, SiteCountryCode, geo_name "+
                // "from Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code INNER JOIN Dictionary d ON sr.`Status` = d.Key AND d.Parent = 'SiteStatus' AND d.Status = 'A' WHERE s.`Status` NOT IN ('X','Z','D') AND trim(sr.`Status`) NOT IN ( 'T', 'X' ) "; /* change from nakarin */

                // if (id != "addnew" && id != "" && id != null)
                // {
                //     string[] idsplit = id.Split('-'); 

                //     if(idsplit.Length > 1){
                //         query += "AND s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId in ("+orgdistributor+")) GROUP BY s.SiteId having OrgId in(select OrgId from Main_organization where OrganizationId =  '" + orgid + "')"; /* fix show affiliate by distributor issue 05092018 (safalya.wanwase@vinsys.com) */
                //     }else{
                //         query += "AND s.SiteId in(select SiteId from SiteContactLinks where ContactId in (select ContactId from Contacts_All where ContactIdInt = '" + id + "')) AND s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId in ("+orgdistributor+")) GROUP BY s.SiteId"; /* fix show affiliate by distributor issue 05092018 (safalya.wanwase@vinsys.com) */
                //     }
                // }
                // else
                // {
                //     query += "AND s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId in ("+orgdistributor+")) GROUP BY s.SiteId having OrgId in(select OrgId from Main_organization where OrganizationId =  '" + orgid + "')"; /* fix show affiliate by distributor issue 05092018 (safalya.wanwase@vinsys.com) */
                // }

                /* ganti dulu sama yg ini, buat handle issue detail partner status 14092018 */

                string query = "SELECT SiteIdInt,SiteId,SiteName,OrgId,`Status`,SiteAddress1,SiteCountryCode,geo_name,PartnerStatusDetail,PartnerStatus from " +
                    "(SELECT s.SiteIdInt,s.SiteId,s.SiteName,s.OrgId,GROUP_CONCAT( DISTINCT sr.`Status`) AS PartnerStatus,GROUP_CONCAT( DISTINCT d.KeyValue ) AS `Status`,CONCAT(IFNULL( s.SiteCity, '' ),' ',IFNULL( s.SiteStateProvince, '' ),' ',IFNULL( CONCAT( '(', s.SiteCountryCode, ')' ), '' ) ) AS SiteAddress1,SiteCountryCode,geo_name,GROUP_CONCAT( DISTINCT r.RoleCode,' - ',d.KeyValue SEPARATOR '<br/>') as PartnerStatusDetail " +
                    "FROM Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId INNER JOIN Roles r ON sr.RoleId = r.RoleId INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code INNER JOIN Dictionary d ON sr.`Status` = d.KEY AND d.Parent = 'SiteStatus' AND d.STATUS = 'A' WHERE s.`Status` NOT IN ( 'X', 'Z', 'D' ) AND sr.`Status` NOT IN ('X','D') ";

                if (id != "addnew" && id != "" && id != null)
                {
                    string[] idsplit = id.Split('-');

                    if (idsplit.Length > 1)
                    {
                        query += "AND s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId in (" + orgdistributor + ")) GROUP BY s.SiteId having OrgId in(select OrgId from Main_organization where OrganizationId =  '" + orgid + "')"; /* fix show affiliate by distributor issue 05092018 (safalya.wanwase@vinsys.com) */
                    }
                    else
                    {
                        query += "AND s.SiteId in(select SiteId from SiteContactLinks where ContactId in (select ContactId from Contacts_All where ContactIdInt = '" + id + "')) AND s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId in (" + orgdistributor + ")) GROUP BY s.SiteId"; /* fix show affiliate by distributor issue 05092018 (safalya.wanwase@vinsys.com) */
                    }
                }
                else
                {
                    query += "AND s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId in (" + orgdistributor + ")) GROUP BY s.SiteId having OrgId in(select OrgId from Main_organization where OrganizationId =  '" + orgid + "')"; /* fix show affiliate by distributor issue 05092018 (safalya.wanwase@vinsys.com) */
                }

                query += ") as sc WHERE PartnerStatus NOT IN ( 'T', 'X', 'R' )";

                /* ganti dulu sama yg ini, buat handle issue detail partner status 14092018 */

                // Console.WriteLine(query);

                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        string encodedata = row[col].ToString();
                        string type = col.DataType.ToString();
                        if (type != "System.DateTime")
                        {
                            encodedata = commonServices.HtmlEncoder(encodedata);
                            row[col] = encodedata;
                        }

                    }

                }
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                affiliate = JArray.Parse(result);

                if (id != "addnew" && id != "" && id != null)
                {
                    foreach (var item in affiliate)
                    {
                        bool isthere = false;
                        foreach (var item2 in sites)
                        {
                            if (item["SiteId"].ToString() == item2["SiteId"].ToString())
                            {
                                isthere = true;
                            }

                            if (isthere)
                            {
                                if (item2["Status"].ToString() == "A")
                                {
                                    item["CheckedSites"] = true;
                                    item["SiteContactLinkId"] = item2["SiteContactLinkId"].ToString();
                                }
                                else if (item2["Status"].ToString() == "X")
                                {
                                    item["CheckedSites"] = false;
                                    item["SiteContactLinkId"] = item2["SiteContactLinkId"].ToString();
                                }
                                break;
                            }
                            else
                            {
                                item["CheckedSites"] = false;
                                item["SiteContactLinkId"] = "InsertNew";
                            }
                        }
                    }
                }

            }
            catch
            {
                affiliate = null;
            }
            finally
            {
            }

            return Ok(affiliate);
        }



        //Added By Soe 10/12/2018
        [HttpGet("AffiliateSite/{id}/{orgid}")]
        public IActionResult AffiliateSite(string id, string orgid)

        {

            JArray affiliate;

            JArray sites = null;

            try

            {

                string[] idsplit = id.Split('-');

                sb1 = new StringBuilder();

                sb1.AppendFormat("select * from SiteContactLinks where ContactId in (select ContactId from Contacts_All where ContactIdInt = '" + idsplit[0] + "')");

                sqlCommand = new MySqlCommand(sb1.ToString());

                ds1 = oDb.getDataSetFromSP(sqlCommand);

                result2 = MySqlDb.GetJSONArrayString(ds1.Tables[0]);

                sites = JArray.Parse(result2);

                string query = "SELECT SiteIdInt,SiteId,SiteName,OrgId,`Status`,SiteAddress1,SiteCountryCode,geo_name,PartnerStatusDetail,PartnerStatus from " +

                "(SELECT s.SiteIdInt,s.SiteId,s.SiteName,s.OrgId,GROUP_CONCAT( DISTINCT sr.`Status`) AS PartnerStatus,GROUP_CONCAT( DISTINCT d.KeyValue ) AS `Status`,CONCAT(IFNULL( s.SiteCity, '' ),' ',IFNULL( s.SiteStateProvince, '' ),' ',IFNULL( CONCAT( '(', s.SiteCountryCode, ')' ), '' ) ) AS SiteAddress1,SiteCountryCode,geo_name,GROUP_CONCAT( DISTINCT r.RoleCode,' - ',d.KeyValue SEPARATOR '<br/>') as PartnerStatusDetail " +

                "FROM Main_site s INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId INNER JOIN Roles r ON sr.RoleId = r.RoleId INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code INNER JOIN Dictionary d ON sr.`Status` = d.KEY AND d.Parent = 'SiteStatus' AND d.STATUS = 'A' WHERE s.`Status` NOT IN ( 'X', 'Z', 'D' ) AND sr.`Status` NOT IN ('X','D','T') ";

                if (idsplit.Length > 1)

                {

                    query += "GROUP BY s.SiteId having OrgId in(select OrgId from Main_organization where OrganizationId =  '" + orgid + "')";

                }

                else

                {

                    query += "AND s.SiteId in (select SiteId from SiteContactLinks where ContactId in (select ContactId from Contacts_All where ContactIdInt = '" + id + "') ) GROUP BY s.SiteId "; /* hide terminated site */

                }

                query += ") as sc WHERE PartnerStatus NOT IN ( 'T', 'X', 'R' )";

                sb = new StringBuilder();

                sb.AppendFormat(query);

                sqlCommand = new MySqlCommand(sb.ToString());

                ds = oDb.getDataSetFromSP(sqlCommand);

                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)

                {

                    ds = MainOrganizationController.EscapeSpecialChar(ds);

                }

                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);

                affiliate = JArray.Parse(result);

                foreach (var item in affiliate)

                {

                    bool isthere = false;

                    foreach (var item2 in sites)

                    {

                        if (item["SiteId"].ToString() == item2["SiteId"].ToString())

                        {

                            isthere = true;

                        }



                        if (isthere)

                        {

                            if (item2["Status"].ToString() == "A")

                            {

                                item["CheckedSites"] = true;

                                item["SiteContactLinkId"] = item2["SiteContactLinkId"].ToString();

                            }

                            else if (item2["Status"].ToString() == "X")

                            {

                                item["CheckedSites"] = false;

                                item["SiteContactLinkId"] = item2["SiteContactLinkId"].ToString();

                            }

                            break;

                        }

                        else

                        {

                            item["CheckedSites"] = false;

                            item["SiteContactLinkId"] = "InsertNew";

                        }

                    }

                }

            }

            catch

            {

                affiliate = null;

            }

            finally

            {

            }



            return Ok(affiliate);

        }
    }
}
