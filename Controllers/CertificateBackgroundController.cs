﻿using autodesk.Code;
using autodesk.Code.AuditLogServices;
using autodesk.Code.EmailTrackingServices;
using autodesk.Code.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace autodesk.Controllers
{
    [Route("api/CertificateBackground")]
    public class CertificateBackgroundController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        private readonly MySQLContext _mySQLContext;
        private readonly IQueueServices _queueServices;
        private readonly IAuditLogServices _auditLogServices;
        public CertificateBackgroundController(MySqlDb sqlDb, MySQLContext mySQLContext, IQueueServices queueServices, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _mySQLContext = mySQLContext;
            _queueServices = queueServices;
            _auditLogServices = auditLogServices;
        }
        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select cb.CertificateBackgroundId,cb.Name,cb.FileLocation,cb.Code,cb.Status from CertificateBackground cb");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }



        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {
            var currentDomain = AppConfig.Config["Auth0:Domain"];
            var defaultPath= AppConfig.Config["CertificateBackground:CertificateDirectory"];
            var displayPath = AppConfig.Config["CertificateBackground:DisplayPath"];
            DirectoryInfo directoryInfo = new DirectoryInfo(Path.Combine(Directory.GetCurrentDirectory(),defaultPath));
            var listFileInfo = directoryInfo.GetFiles();

            var cert = _mySQLContext.CertificateBackground.FirstOrDefault(c => c.CertificateBackgroundId == id);
            if (cert!=null)
            {
                var file = listFileInfo.FirstOrDefault(x => x.Name == cert.Name);
                if (file != null)
                {
                    var result = Path.Combine(currentDomain, displayPath, cert.Name);
                    return Ok(new { Name=cert.Name,Data=result });
                }
            }



            //try
            //{
            //    sb = new StringBuilder();
            //    sb.AppendFormat("select cb.CertificateBackgroundId,cb.Name,cb.FileLocation,cb.Code,cb.Status from CertificateBackground cb where CertificateBackgroundId = '" + id + "'");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);
            //}
            //catch
            //{
            //    result = "{}";
            //}
            //finally
            //{
            //    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //}

            //var img = (from cb in _mySQLContext.CertificateBackground join cbb in _mySQLContext.CertificateBackgroundBlobs on cb.CertificateBackgroundId equals cbb.CertificateBackgroundId where cb.CertificateBackgroundId==id select new { Name= cb.Name,Data=cbb.Data }).FirstOrDefault();


            //var cert = _mySQLContext.CertificateBackgroundBlobs.Where(c => c.CertificateBackgroundId == id).Select(c=> new { c.ChunkData ,c.Id}).OrderBy(c=>c.Id).ToList();

            //while (cert != null && cert.Count<=20)
            //{
            //    cert = _mySQLContext.CertificateBackgroundBlobs.Where(c => c.CertificateBackgroundId == id).Select(c => new { c.ChunkData, c.Id }).OrderBy(c => c.Id).ToList();
            //}

            //if (cert != null)
            //{
            //    var name = _mySQLContext.CertificateBackground.FirstOrDefault(c => c.CertificateBackgroundId == id).Name;
            //    var res = string.Concat(cert.Select(c => c.ChunkData));
            //    var contentType = res.Substring(0, res.LastIndexOf(","));
            //    res = res.Remove(0, res.LastIndexOf(",") +2);
            //    using (var stream = new MemoryStream(Convert.FromBase64String(res)))
            //    {
            //        var img = Image.FromStream(stream);
            //        img = Utilities.ResizeImage(img, 15);
            //        using (var memStream = new MemoryStream())
            //        {
            //            img.Save(memStream, (System.Drawing.Imaging.ImageFormat.Png));
            //            var data =contentType +","+ Convert.ToBase64String(memStream.ToArray());
            //            return Ok(new CertBackgroundImage { Data =  data,Name= name });
            //        }
            //    }
            //}
            return Ok();
        }
        public class CertBackgroundImage
        {
            public string Name { get; set; }
            public string Data { get; set; }
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {
            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            // JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CertificateBackground;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1);

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into CertificateBackground " +
                    "( CertificateBackgroundId, Code, Name, FileLocation, Status) " +
                    "values ( '" + data.CertificateBackgroundId + "','" + data.Code + "','" + data.Name + "','" + data.HTMLDesign + "','" + data.Status + "');"
                );

                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from CertificateBackground;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            if (count2 > count1)
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
            }

            return Ok(res);

        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update CertificateBackground set " +
                    "Code='" + data.Code + "', " +
                    "Name='" + data.Name + "', " +
                    "FileLocation='" + data.HTMLDesign + "'" +
                    "where CertificateBackgroundId='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                Console.WriteLine("success");
            }
            catch
            {
                Console.WriteLine("failed");
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from CertificateBackground where CertificateBackgroundId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o3 = JObject.Parse(result);

            if (o3["Name"].ToString() == data.Name.ToString())
            {
                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Update Data Success\"" +
                    "}";
            }

            return Ok(res);
        }


        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Delete Data Failed\"" +
                "}";
            try
            {
                //sb = new StringBuilder();
                //sb.AppendFormat("delete from CertificateBackground where CertificateBackgroundId = '" + id + "'");
                //sqlCommand = new MySqlCommand(sb.ToString());
                //ds = oDb.getDataSetFromSP(sqlCommand);

                var certBgr = _mySQLContext.CertificateBackground.FirstOrDefault(x => x.CertificateBackgroundId == id);
                if (certBgr!=null)
                {
                    _mySQLContext.CertificateBackground.Remove(certBgr);
                    var deleted = _mySQLContext.SaveChanges();
                    if (deleted>0)
                    {
                        var admin = HttpContext.User.GetUserId();
                        _auditLogServices.LogHistory(new History { Admin = admin, Date = DateTime.Now, Description = "Delete CertificateBackground <br/> CertificateBackgroundId : " + id,Id=id.ToString() });
                        res =
                       "{" +
                       "\"code\":\"1\"," +
                       "\"message\":\"Delete Data Success\"" +
                       "}";
                    }

                }
            }
            catch
            {
            }
            //finally
            //{
            //    sb = new StringBuilder();
            //    sb.AppendFormat("select * from CertificateBackground where CertificateBackgroundId  =" + id + ";");
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);

            //    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            //    o3 = JObject.Parse(result);
            //    Console.WriteLine(o3.Count);
            //    if (o3.Count < 1)
            //    {
            //        res =
            //            "{" +
            //            "\"code\":\"1\"," +
            //            "\"message\":\"Delete Data Success\"" +
            //            "}";
            //    }
            //}

            return Ok(res);
        }

        [HttpPost("UploadFileBackground")]
        public async Task UploadFileBackground(IFormFile file)
        {
            string DefaultPath = AppConfig.Config["CertificateBackground:CertificateDirectory"];
          
            if (file == null)
            {
                throw new Exception("File is null");
            }

            if (file.Length == 0)
            {
                throw new Exception("File is empty");
            }

            var path =
            Path.Combine(
                Directory.GetCurrentDirectory(),
                DefaultPath,
                // "assets\\certificate\\", 
                file.FileName);

            Console.WriteLine(path);


            //   var filePath = Path.GetTempFileName();
            //  var filePath = file.FileName;
            /*
            var filePath = Path.Combine(
                        Directory.GetCurrentDirectory(), "Assets/CertificateBackground/",
                        file.FileName);
            */

            // var filePath = DEFAULT_FILE_PATH + "" + file.FileName;
            // var filePathdb = DEFAULT_FILE_PATH_DB + "" + file.FileName;

            // Console.WriteLine("file path" + filePathdb);

            // using (var stream = new FileStream(filePathdb, FileMode.Create))
            //using (var stream = new FileStream(path, FileMode.Create))
            //{
            //    await file.CopyToAsync(stream);

            //}
            try
            {
              
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                    var certBack = new CertificateBackground
                    {
                        Name = file.FileName,
                        Code = file.FileName,
                        FileLocation = string.Empty,
                        Status = AutodeskConst.StatusConts.A
                    };
                    _mySQLContext.CertificateBackground.Add(certBack);
                    _mySQLContext.SaveChanges();

                    //var base64 = Convert.ToBase64String(memStream.ToArray());
                    //base64 = "data:" + file.ContentType + ";base64, " + base64;
                    //var chunks = base64.SplitToList(250000);

                    //_queueServices.Enqueue(() => AddCertBackground(certBack, chunks));
                    //_queueServices.Process();
                    //Thread.Sleep(2000);
                }
            }
            catch (Exception ex)
            {

                return;
            }

            //untuk path yang disimpan ke database
            //var path_db = "assets/certificate/" + file.FileName;

            //try
            //{
            //    sb = new StringBuilder();
            //    sb.AppendFormat(
            //        "insert into CertificateBackground " +
            //        "( Code, Name, FileLocation, Status) " +
            //        // "values ( '" + file.FileName + "','" + file.FileName + "','" + filePathdb + "','A');"
            //        "values ( '" + file.FileName + "','" + file.FileName + "','" + path_db + "','A');"
            //    );

            //    Console.WriteLine(sb.ToString());
            //    sqlCommand = new MySqlCommand(sb.ToString());
            //    ds = oDb.getDataSetFromSP(sqlCommand);
            //}
            //catch
            //{

            //}
            //finally
            //{
            //}

            //  return(res);
            /*
           using (Stream stream = file.OpenReadStream())
           {
               using (var binaryReader = new BinaryReader(stream))
               {
                   var fileContent = binaryReader.ReadBytes((int)file.Length);
                  // await _uploadService.AddFile(fileContent, file.FileName, file.ContentType);
               }
           }
           */
        }

        //private void AddCertBackground(CertificateBackground certBack, System.Collections.Generic.IEnumerable<string> chunks)
        //{
        //    using (var _mySQLContext = new MySQLContext())
        //    {

        //        foreach (var item in chunks)
        //        {
        //            var certBackBlob = new CertificateBackgroundBlob
        //            {
        //                CertificateBackgroundId = certBack.CertificateBackgroundId,
        //                ChunkData = item,
        //            };

        //            _mySQLContext.CertificateBackgroundBlobs.Add(certBackBlob);
        //            _mySQLContext.SaveChanges();
        //        }
        //    }
        //}
    }
}
