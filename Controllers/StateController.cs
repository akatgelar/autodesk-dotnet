using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using Newtonsoft.Json;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/State")]
    public class StateController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public StateController(MySqlDb sqlDb)
        {
            oDb = sqlDb;

        }

        // [HttpGet]
        // public IActionResult Select()
        // {
        //     try
        //     {
        //         sb = new StringBuilder();
        //         sb.Append("select * from States ");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "{}";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
        //     }

        //     return Ok(result);                  
        // }



        // [HttpGet("{id}")]
        // public IActionResult WhereId(int id)
        // { 
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from States where State_id = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     {
        //         result = "{}";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // } 

        // [HttpGet("filter/{id}")]
        // public IActionResult WhereIdGeo(string id)
        // { 
        //     try
        //     {  
        //         if(id!=""){
        //             sb = new StringBuilder();
        //             sb.AppendFormat("select * from States where geo_code in ( "+id+" );");
        //             sqlCommand = new MySqlCommand(sb.ToString());
        //             ds = oDb.getDataSetFromSP(sqlCommand);
        //         }
        //         else{
        //             result = "";
        //         }

        //     }
        //     catch
        //     {
        //         result = "[]";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // } 

        // [HttpGet("filterregion/{id}")]
        // public IActionResult WhereIdRegion(string id)
        // { 
        //     try
        //     {  
        //         if(id!=""){
        //             sb = new StringBuilder();
        //             sb.AppendFormat("select * from States where region_code in ( "+id+" );");
        //             sqlCommand = new MySqlCommand(sb.ToString());
        //             ds = oDb.getDataSetFromSP(sqlCommand);
        //         }
        //         else{
        //             result = "";
        //         }

        //     }
        //     catch
        //     {
        //         result = "[]";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // } 

        // [HttpGet("filtersubregion/{id}")]
        // public IActionResult WhereIdSubRegion(string id)
        // { 
        //     try
        //     {  
        //         if(id!=""){
        //             sb = new StringBuilder();
        //             sb.AppendFormat("select * from States where subregion_code in ( "+id+" );");
        //             sqlCommand = new MySqlCommand(sb.ToString());
        //             ds = oDb.getDataSetFromSP(sqlCommand);
        //         }
        //         else{
        //             result = "";
        //         }

        //     }
        //     catch
        //     {
        //         result = "[]";
        //     }
        //     finally
        //     { 
        //         result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
        //     }

        //     return Ok(result);
        // } 

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {

            
            var o1 = JObject.Parse(obj);
            string query = ""; 

            query += "select * from States ";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("CountryCode")){   
                    query += "CountryCode = '"+property.Value.ToString()+"' "; 
                }
                i=i+1;
            } 
 
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpGet("whereState/{obj}")]
        public IActionResult WhereStateObj(string obj)
        {

           string value = obj.Replace("\"", "'");
            //   var o1 = JObject.Parse(obj);
            string query = "";

            query += "select * from States  where CountryCode in ("+ value + ")";
           
            

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        // [HttpPost]
        // public IActionResult Insert([FromBody] dynamic data)
        // {    

        //     int count1 = 0;
        //     int count2 = 0;
        //     JObject o1;
        //     JObject o2;
        //     // JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Insert Data Failed\""+
        //         "}";  

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from States;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o1 = JObject.Parse(result); 
        //     count1 = Int32.Parse(o1["count"].ToString());
        //     Console.WriteLine(count1); 

        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "insert into States "+
        //             "(geo_code, territory_code, region_code, subregion_code, State_code, State_name, State_tel, cuid, cdate, muid, mdate) "+
        //             "values ('"+data.geo_code+"','"+data.territory_code+"','"+data.region_code+"','"+data.subregion_code+"','"+data.State_code+"','"+data.State_name+"','"+data.State_tel+"','"+data.cuid+"','"+data.cdate+"','"+data.muid+"','"+data.mdate+"');"
        //         );
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //     }
        //     finally
        //     { 
        //     }

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select count(*) as count from States;");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);   

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     o2 = JObject.Parse(result); 
        //     count2 = Int32.Parse(o2["count"].ToString());
        //     Console.WriteLine(count2);

        //     if(count2 > count1)
        //     { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Insert Data Success\""+
        //             "}";  
        //     } 

        //     return Ok(res);
        // }

        // [HttpPut("{id}")]
        // public IActionResult Update(int id, [FromBody] dynamic data)
        // {  
        //     //JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Update Data Failed\""+
        //         "}";  

        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat(
        //             "update States set "+
        //             "geo_code='"+data.geo_code+"',"+
        //             "territory_code='"+data.territory_code+"',"+
        //             "region_code='"+data.region_code+"',"+
        //             "subregion_code='"+data.subregion_code+"',"+
        //             "State_code='"+data.State_code+"',"+
        //             "State_name='"+data.State_name+"',"+
        //             "State_tel='"+data.State_tel+"',"+
        //             "cuid='"+data.cuid+"',"+
        //             "cdate='"+data.cdate+"',"+
        //             "muid='"+data.muid+"',"+
        //             "mdate='"+data.mdate+"'"+
        //             "where State_id='"+id+"'"
        //         );
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //         Console.WriteLine("success");
        //     }
        //     catch
        //     {
        //         Console.WriteLine("failed");  
        //     }
        //     finally
        //     {  
        //     }

        //     sb = new StringBuilder();
        //     sb.AppendFormat("select * from States where State_id ="+id+";");
        //     sqlCommand = new MySqlCommand(sb.ToString());
        //     ds = oDb.getDataSetFromSP(sqlCommand);

        //     result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //     //o3 = JObject.Parse(result);  

        //     // if(o3["OrgId"].ToString() == data.OrgId.ToString())
        //     // { 
        //         res =
        //             "{"+
        //             "\"code\":\"1\","+
        //             "\"message\":\"Update Data Success\""+
        //             "}"; 
        //     //} 

        //     return Ok(res);  
        // } 



        // [HttpDelete("{id}")]
        // public IActionResult Delete(int id)
        // { 
        //     JObject o3;
        //     string res =
        //         "{"+
        //         "\"code\":\"0\","+
        //         "\"message\":\"Delete Data Failed\""+
        //         "}";  
        //     try
        //     {  
        //         sb = new StringBuilder();
        //         sb.AppendFormat("delete from States where State_id = '"+id+"'");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);
        //     }
        //     catch
        //     { 
        //     }
        //     finally
        //     { 
        //         sb = new StringBuilder();
        //         sb.AppendFormat("select * from States where State_id  ="+id+";");
        //         sqlCommand = new MySqlCommand(sb.ToString());
        //         ds = oDb.getDataSetFromSP(sqlCommand);

        //         result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
        //         o3 = JObject.Parse(result);  
        //         Console.WriteLine(o3.Count);
        //         if(o3.Count < 1){ 
        //             res =
        //                 "{"+
        //                 "\"code\":\"1\","+
        //                 "\"message\":\"Delete Data Success\""+
        //                 "}"; 
        //         }
        //     }

        //     return Ok(res);
        // } 


    }
}
