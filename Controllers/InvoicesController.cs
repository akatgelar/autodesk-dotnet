using autodesk.Code;
using autodesk.Code.AuditLogServices;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using autodesk.Code.Models.Dto.InvoiceDto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
// using autodesk.Models;

namespace autodesk.Controllers
{
    //[AllowAnonymous]
    [Route("api/MainInvoices")]
    public class InvoicesController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;
        DataSet dsTemp = new DataSet();
        private IDataServices dataServices;
        String hasil;

        public Audit _audit; //= new Audit();
        private readonly MySQLContext _mySQLContext;
        private readonly IEmailService _emailSender;
        private readonly IAuditLogServices _auditLogServices;

        public InvoicesController(MySqlDb sqlDb, MySQLContext mySQLContext, IEmailService emailSender, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _audit = new Audit(oDb);
            _mySQLContext = mySQLContext;
            dataServices = new DataServices(oDb, _mySQLContext);

            _emailSender = emailSender;
            _auditLogServices = auditLogServices;
        }


        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Invoices ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }



        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select InvoiceNumber, DATE_FORMAT(InvoiceDate,'%m/%d/%Y') AS InvoiceDate, TotalInvoicedAmount, TotalPaymentAmount, NumberOfLicenses, InvoiceId, FinancialYear, OrgId, SiteCountryDistributorSKUId, InvoicedCurrency, InvoiceDescription, DocuSign, DistributorFee from Invoices where InvoiceId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetSKUList/{id}")]
        public IActionResult GetSKUList(string id)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("Select isku.SiteCountryDistributorSKUId, sku.SKUDescription, sku.CountryCode from InvoiceSKU isku JOIN SiteCountryDistributorSKU sku ON isku.SiteCountryDistributorSKUId = sku.SiteCountryDistributorSKUId where isku.InvoiceId = '" + id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public async Task<IActionResult> WhereObj(string obj)
        {
            var orgId = string.Empty;
            JObject json = JObject.Parse(obj);
            var props = json.Properties();
            var orgProp = props.FirstOrDefault(x => x.Name.Equals("OrgId"));
            if (orgProp != null)
            {
                orgId = orgProp.Value.ToString();
            }

            var query = "select Invoices.InvoiceId," +
                "InvoiceSKU.SiteId, " +

                "InvoiceNumber," +
                 "DATE_FORMAT(InvoiceDate, '%Y-%m-%d') as InvoiceDate," +
                "(CASE " +
                "when Invoices.Comments = 'Approved' then " +
                "(case when Invoices.Status = 'A' then 'Approved' when  Invoices.Status = 'C' then 'Cancelled' else 'Paid' end) " +
                "when Invoices.Comments = 'Rejected' then " +
                "(case when Invoices.Status = 'A' then 'Rejected' when  Invoices.Status = 'C' then 'Cancelled' else   'Paid' end) " +
                "else (case when Invoices.Status = 'A' then 'Pending' when Invoices.Status = 'C' then 'Cancelled' else   'Paid' end) " +
                "END) as InvoiceStatus," +

                "sum(InvoiceSKU.QTY) as QTY," +
                "(CASE " +
                " when InvoiceDiscount.AmountType = 'Percentage' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - (sum((cast(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100))" +
                " when InvoiceDiscount.AmountType = 'Exactly' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - InvoiceDiscount.Amount" +
                " else sum(cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY) " +
                "END) as TotalInvoiceAmmount, " +
                " sum(InvoiceSKU.QTY * cast(SiteCountryDistributorSKU.License as UNSIGNED)) as NumberOfLicenses," +
                "Contacts_All.ContactName," +
                "Contacts_All.ContactIdInt," +
                "group_concat(concat(InvoiceSKU.SiteId, '(',SKUName, '*',QTY,')') separator '\r\n,') as SKUDescription," +
                "(CASE " +
                "when Invoices.Comments = 'Approved' then concat(Invoices.LastAdminBy, ' at ', date_format(Invoices.DateLastAdmin,'%Y-%m-%d')) " +
                "END) as ApprovedBy," +
                "(CASE " +
                "when Invoices.Comments = 'Rejected' then concat(Invoices.LastAdminBy, ' at ', date_format(Invoices.DateLastAdmin,'%Y-%m-%d')) " +
                "END) as RejectedBy " +

                " from Invoices join InvoiceSKU on Invoices.InvoiceId = InvoiceSKU.InvoiceId" +
                " join InvoicePricingSKU on InvoiceSKU.PricingSKUId = InvoicePricingSKU.Id" +
                " join SiteCountryDistributorSKU on InvoicePricingSKU.SiteCountryDistributorSKUId = SiteCountryDistributorSKU.SiteCountryDistributorSKUId" +
                " left join InvoiceDiscount on Invoices.InvoiceId = InvoiceDiscount.InvoiceId" +
                " left join Contacts_All on Invoices.AddedBy = Contacts_All.ContactId " +
                //" join SiteRoles on SiteRoles.SiteId = InvoiceSKU.SiteId " +
                " where OrgId = '" + orgId + "' " +
                //" and SiteRoles.Status <> 'X' and SiteRoles.Status <> 'T'" +
                " group by Invoices.InvoiceId;";








            //foreach (JProperty property in json.Properties())
            //{
            //    if (property.Name.Equals("OrgId"))
            //    {
            //        orgId = property.Value.ToString();
            //        break;
            //    }

            //}

            //var _query = (from iv in _mySQLContext.Invoices
            //              join isku in _mySQLContext.InvoiceSku on iv.InvoiceId equals isku.InvoiceId
            //              join psku in _mySQLContext.InvoicePricingSKU on isku.PricingSKUId equals psku.Id
            //              join msku in _mySQLContext.MasterSKU on psku.SiteCountryDistributorSKUId equals msku.SiteCountryDistributorSKUId

            //              where iv.OrgId == orgId

            //              select new
            //              {
            //                  iv.InvoiceId,
            //                  Licences = msku.License,
            //                  Qty = isku.QTY,
            //                  SKUDes = string.Join("", isku.SiteId, "(", msku.SKUName, "*", isku.QTY, ")", "\r\n")
            //              }
            //              into list
            //              group list by list.InvoiceId
            //              ).Select(x => new
            //              {
            //                  InvoiceId = x.Key,
            //                  SKUDes = string.Join(" ", x.Select(s => s.SKUDes)),
            //                  NumberOfLicenses = x.Sum(l => (l.Qty * long.Parse(l.Licences)))
            //              });

            //var _totalAmmount = (from _i in _mySQLContext.InvoiceSku
            //                     join _p in _mySQLContext.InvoicePricingSKU on _i.PricingSKUId equals _p.Id
            //                     join dis in _mySQLContext.InvoiceDiscount on _i.InvoiceId equals dis.InvoiceId into discount
            //                     from _dis in discount.DefaultIfEmpty()
            //                     where _i.TotalAmmount.HasValue && _i.TotalAmmount.Value > 0
            //                     select new { _i.InvoiceId, Amount = GetToTalWithDiscount(_dis, _i.QTY * Convert.ToInt32(_p.Price)), QTY = _i.QTY }
            //                    into list
            //                     group list by list.InvoiceId
            //                    ).Select(x => new
            //                    {
            //                        InvoiceId = x.Key,
            //                        TotalAmmount = x.Sum(s => s.Amount),
            //                        Quatity = x.Sum(q => q.QTY)
            //                    });

            //var join = await Task.Run(() => (from _q in _query
            //                                 join iv in _mySQLContext.Invoices on _q.InvoiceId equals iv.InvoiceId
            //                                 join tt in _totalAmmount on iv.InvoiceId equals tt.InvoiceId
            //                                 join ca in _mySQLContext.ContactsAll on iv.AddedBy equals ca.ContactId
            //                                      into invoice_contact
            //                                 from _invoice_contact in invoice_contact.DefaultIfEmpty()
            //                                 join siteLink in _mySQLContext.SiteContactLinks on iv.AddedBy equals siteLink.ContactId into profile
            //                                 from _siteLink in profile.DefaultIfEmpty()
            //                                 where iv.OrgId == orgId
            //                                 select new
            //                                 {
            //                                     iv.InvoiceId,
            //                                     iv.InvoiceNumber,
            //                                     InvoiceStatus = ReturnDisplayInvoiceStatus(iv.Status, iv.Comments),
            //                                     InvoiceDate = GetInvoiceDate(iv.InvoiceDate),
            //                                     QTY = tt.Quatity,
            //                                     TotalInvoicedAmount = tt.TotalAmmount,
            //                                     NumberOfLicenses = _q.NumberOfLicenses,
            //                                     ContactName = _invoice_contact != null ? _invoice_contact.ContactName : string.Empty,
            //                                     ContactIdInt = _invoice_contact != null ? _invoice_contact.ContactIdInt : 0,
            //                                     SiteId = _siteLink != null ? _siteLink.SiteId : string.Empty,
            //                                     SKUDescription = _q.SKUDes,
            //                                     ApprovedBy = iv.Comments == AutodeskConst.DiscountAction.Approved ? (iv.LastAdminBy + " at " + iv.DateLastAdmin) : string.Empty,
            //                                     RejectedBy = iv.Comments == AutodeskConst.DiscountAction.Rejected ? (iv.LastAdminBy + " at " + iv.DateLastAdmin) : string.Empty
            //                                 }).ToList());

            //var _joinQuery = (from _q in _query
            //                  join iv in _mySQLContext.Invoices on _q.InvoiceId equals iv.InvoiceId
            //                  join isku in _mySQLContext.InvoiceSku on iv.InvoiceId equals isku.InvoiceId
            //                  join psku in _mySQLContext.InvoicePricingSKU on isku.PricingSKUId equals psku.Id
            //                  //join msku in _mySQLContext.MasterSKU on psku.SiteCountryDistributorSKUId equals msku.SiteCountryDistributorSKUId
            //                  join dis in _mySQLContext.InvoiceDiscount on iv.InvoiceId equals dis.InvoiceId into discount
            //                  from _dis in discount.DefaultIfEmpty()
            //                  join ca in _mySQLContext.ContactsAll on iv.AddedBy equals ca.ContactId
            //                  into invoice_contact
            //                  from _invoice_contact in invoice_contact.DefaultIfEmpty()
            //                  join siteLink in _mySQLContext.SiteContactLinks on iv.AddedBy equals siteLink.ContactId into profile
            //                  from _siteLink in profile.DefaultIfEmpty()
            //                  where iv.OrgId == orgId
            //                  let _qty = _mySQLContext.InvoiceSku.Where(x => x.InvoiceId == iv.InvoiceId).Sum(x => x.QTY)
            //                  select new
            //                  {
            //                      iv.InvoiceId,
            //                      iv.InvoiceNumber,
            //                      InvoiceStatus = ReturnDisplayInvoiceStatus(iv.Status, iv.Comments),
            //                      InvoiceDate = GetInvoiceDate(iv.InvoiceDate),
            //                      QTY = _qty,
            //                      TotalInvoicedAmount = GetToTalWithDiscount(_dis, (from _i in _mySQLContext.InvoiceSku
            //                                                                        join _p in _mySQLContext.InvoicePricingSKU
            //                                    on _i.PricingSKUId equals _p.Id
            //                                                                        where _i.InvoiceId == iv.InvoiceId && _i.TotalAmmount.HasValue && _i.TotalAmmount.Value > 0
            //                                                                        select new { Amount = _i.QTY * Convert.ToInt32(_p.Price) }
            //                                                                       ).Sum(s => s.Amount)),
            //                      NumberOfLicenses = _mySQLContext.MasterSKU.Where(x => x.SiteCountryDistributorSKUId == psku.SiteCountryDistributorSKUId).Sum(x => long.Parse(x.License)) * _qty,
            //                      ContactName = _invoice_contact != null ? _invoice_contact.ContactName : string.Empty,
            //                      ContactIdInt = _invoice_contact != null ? _invoice_contact.ContactIdInt : 0,
            //                      SiteId = _siteLink != null ? _siteLink.SiteId : string.Empty,
            //                      SKUDescription = _q.SKUDes,
            //                      ApprovedBy = iv.Comments == AutodeskConst.DiscountAction.Approved ? (iv.LastAdminBy + " at " + iv.DateLastAdmin) : string.Empty,
            //                      RejectedBy = iv.Comments == AutodeskConst.DiscountAction.Rejected ? (iv.LastAdminBy + " at " + iv.DateLastAdmin) : string.Empty

            //                  }).DistinctBy(x => x.InvoiceId).ToList();

            {
                //var settings = new JsonSerializerSettings
                //{

                //};
                //settings.Converters.Add(new DataSetConverter());
                //settings.ContractResolver = new DefaultContractResolver();
                //result = JsonConvert.SerializeObject(join, Formatting.Indented,
                //   settings);
                using (var command = _mySQLContext.Database.GetDbConnection().CreateCommand())
                {
                    command.CommandText = query;

                    try
                    {
                        _mySQLContext.Database.OpenConnection();
                        using (var reader = command.ExecuteReader())
                        {
                            result = await Task.Run(() => reader.ToJsonString());
                            return Ok(result);
                        }
                    }
                    catch (Exception ex)
                    {

                        return Ok();
                    }
                }
                //var ds = await Task.Run(() => oDb.getDataSetFromSP(new MySqlCommand(query)));
                //if (ds.Tables.Any())
                //{
                //    result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                //}

            }
            return Ok(result);
        }
        private string ReturnDisplayInvoiceStatus(string invoiceStatus, string comment)
        {
            if (comment == "Approved")
            {
                if (invoiceStatus == AutodeskConst.StatusConts.A)
                {
                    return "Approved";
                }
                else
                {
                    return GetInvoiceStatus(invoiceStatus);
                }

            }
            if ((comment == "Saved" || comment == "Added"))
            {
                {
                    return GetInvoiceStatus(invoiceStatus);
                }

            }
            if (comment == "Rejected")
            {
                if (invoiceStatus == AutodeskConst.StatusConts.A)
                {
                    return "Rejected";
                }
                else
                {
                    return GetInvoiceStatus(invoiceStatus);
                }
            }
            return string.Empty;

        }
        private string GetInvoiceDate(DateTime? invoiceDate)
        {
            if (invoiceDate.HasValue)
            {
                return invoiceDate.Value.ToString("yyyy/MM/dd");
            }
            return string.Empty;
        }
        private double GetToTalWithDiscount(InvoiceDiscount discount, double? _total)
        {

            ////double? _total = 0.0;

            ////foreach (var item in totalAmountDto)
            ////{
            ////    _total = _total + (item.QTY * item.Price);
            ////}
            if (discount != null && discount.AmountType == AutodeskConst.AmountType.Percentage && _total.HasValue)
            {
                return _total.Value - (_total.Value * (discount.Amount / 100));
            }
            else if (discount != null && discount.AmountType == AutodeskConst.AmountType.Exactly && _total.HasValue)
            {
                return _total.Value - discount.Amount;
            }
            return _total.Value;
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {

            int count1 = 0;
            int count2 = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            JObject o4;
            JObject o5;
            JObject o6;
            JObject o7;
            JObject o8;
            JObject o9;
            string invoiceid = "";
            string orgid = "";
            string siteid = "";
            string activityid = "";
            string activityidsite = "";
            string recentactivityid = "";
            string recentactivityidsite = "";
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Invoices;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count1 = Int32.Parse(o1["count"].ToString());

            try
            {
                //insert invoices
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Invoices " +
                    "(OrgId, InvoiceNumber, InvoiceDescription, InvoiceDate, InvoicedCurrency, NumberOfLicenses, FinancialYear, DateAdded, AddedBy, TotalInvoicedAmount, TotalPaymentAmount, DocuSign, DistributorFee) " +
                    "values ('" + data.OrgId + "','" + data.InvoiceNumber + "','" + data.InvoiceDescription + "','" + data.POReceivedDate + "','" + data.InvoicedCurrency + "','" + data.NumberOfLicenses + "','" + data.FinancialYear + "','" + data.DateAdded + "','" + data.AddedBy + "','" + data.TotalInvoicedAmount + "','" + data.TotalPaymentAmount + "','" + data.DocuSign + "','" + data.DistributorFee + "');"
                );
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                //get activity id
                sb = new StringBuilder();
                sb.AppendFormat("select ActivityId from JournalActivities where ActivityName = '" + data.FinancialYear.ToString().Substring(0, 4) + " Activated & Payment Received' and ActivityType = 'Organization Journal Entry'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o6 = JObject.Parse(result);

                if (o6["ActivityId"] != null)
                {
                    activityid = o6["ActivityId"].ToString();
                }

                if (activityid != "")
                {
                    //insert journal entries
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "insert into Journals " +
                        "(ParentId, ActivityId, Notes, DateAdded, AddedBy, ActivityDate) " +
                        "values ('" + data.OrgId + "','" + activityid + "','" + data.InvoiceDescription + "',NOW(),'" + data.AddedBy + "','" + data.POReceivedDate + "');"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }
                else
                {
                    //insert journal activities
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "insert into JournalActivities " +
                        "(DateAdded, AddedBy, Status, ActivityName, ActivityType) " +
                        "values (NOW(),'" + data.AddedBy + "','A','" + data.FinancialYear.ToString().Substring(0, 4) + " Activated & Payment Received','Organization Journal Entry');"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);

                    //get recent activityid
                    sb = new StringBuilder();
                    sb.AppendFormat("SELECT MAX(ActivityId) from JournalActivities ");
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                    o7 = JObject.Parse(result);
                    recentactivityid = o7["MAX(ActivityId)"].ToString();

                    //insert journal entries
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "insert into Journals " +
                        "(ParentId, ActivityId, Notes, DateAdded, AddedBy, ActivityDate) " +
                        "values ('" + data.OrgId + "','" + recentactivityid + "','" + data.InvoiceDescription + "',NOW(),'" + data.AddedBy + "','" + data.POReceivedDate + "');"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }

                //get invoices id
                sb = new StringBuilder();
                sb.AppendFormat("select InvoiceId from Invoices where InvoiceId in (select MAX(InvoiceId) from Invoices);");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);
                invoiceid = o3["InvoiceId"].ToString();

                //update invoicessites and site status
                for (int i = 0; i < data.SitesArr.Count; i++)
                {
                    if (data.SitesArr[i][1] == true)
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "insert into InvoicesSites " +
                            "(InvoiceId, SiteId, Status) " +
                            "values ('" + invoiceid + "','" + data.SitesArr[i][0] + "','A');"
                        );
                        // Console.WriteLine(sb.ToString());
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        sb = new StringBuilder();
                        sb.AppendFormat("select SiteIdInt from Main_site where SiteId = '" + data.SitesArr[i][0] + "' ;");
                        // Console.WriteLine(sb.ToString());
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                        o5 = JObject.Parse(result);
                        siteid = o5["SiteIdInt"].ToString();

                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "update Main_site set " +
                            "Status='A'" +
                            "where SiteIdInt='" + siteid + "'"
                        );
                        // Console.WriteLine(sb.ToString());
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        /* update status partner type nya */

                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "update SiteRoles set " +
                            "Status='A'" +
                            "where SiteId = '" + data.SitesArr[i][0] + "'"
                        );
                        // Console.WriteLine(sb.ToString());
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        /* update status partner type nya */

                        //get activity id
                        sb = new StringBuilder();
                        sb.AppendFormat("select ActivityId from JournalActivities where ActivityName = '" + data.FinancialYear.ToString().Substring(0, 4) + " Renewed' and ActivityType = 'Site Journal Entry'");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);

                        result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                        o8 = JObject.Parse(result);

                        if (o8["ActivityId"] != null)
                        {
                            activityidsite = o8["ActivityId"].ToString();
                        }

                        if (activityid != "")
                        {
                            //insert journal entries
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "insert into Journals " +
                                "(ParentId, ActivityId, Notes, DateAdded, AddedBy, ActivityDate) " +
                                "values ('" + data.SitesArr[i][0] + "','" + activityidsite + "','" + data.InvoiceDescription + "',NOW(),'" + data.AddedBy + "','" + data.POReceivedDate + "');"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);
                        }
                        else
                        {
                            //insert journal activities
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "insert into JournalActivities " +
                                "(DateAdded, AddedBy, Status, ActivityName, ActivityType) " +
                                "values (NOW(),'" + data.AddedBy + "','A','" + data.FinancialYear.ToString().Substring(0, 4) + " Renewed','Site Journal Entry');"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            //get recent activityid
                            sb = new StringBuilder();
                            sb.AppendFormat("SELECT MAX(ActivityId) from JournalActivities ");
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);
                            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                            o9 = JObject.Parse(result);
                            recentactivityidsite = o9["MAX(ActivityId)"].ToString();

                            //insert journal entries
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "insert into Journals " +
                                "(ParentId, ActivityId, Notes, DateAdded, AddedBy, ActivityDate) " +
                                "values ('" + data.SitesArr[i][0] + "','" + recentactivityidsite + "','" + data.InvoiceDescription + "',NOW(),'" + data.AddedBy + "','" + data.POReceivedDate + "');"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);
                        }
                    }
                    else
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat(
                            "insert into InvoicesSites " +
                            "(InvoiceId, SiteId, Status) " +
                            "values ('" + invoiceid + "','" + data.SitesArr[i][0] + "','X');"
                        );
                        sqlCommand = new MySqlCommand(sb.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                    }
                }

                //add multiple sku
                /* change invoice */

                // string[] skuarr = data.SiteCountryDistributorSKUId.ToString().Split(',');
                // Console.WriteLine(skuarr.Length);
                // Console.WriteLine(skuarr);
                // for(int i=0;i<skuarr.Length;i++){
                //     sb = new StringBuilder();
                //     sb.AppendFormat(
                //         "insert into InvoiceSKU "+
                //         "(InvoiceId, SiteCountryDistributorSKUId) "+
                //         "values ('"+invoiceid+"','"+skuarr[i]+"');"
                //     );
                //     sqlCommand = new MySqlCommand(sb.ToString());
                //     ds = oDb.getDataSetFromSP(sqlCommand);
                // }

                JArray skuarr = JArray.Parse(data.MultiSKU.ToString());
                // Console.WriteLine(skuarr);
                for (int i = 0; i < skuarr.Count; i++)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "insert into InvoiceSKU " +
                        "(InvoiceId, SiteCountryDistributorSKUId, QTY, TotalAmmount) " +
                        "values ('" + invoiceid + "','" + skuarr[i]["SiteCountryDistributorSKUId"] + "','" + skuarr[i]["QTY"] + "','" + skuarr[i]["Total"] + "');"
                    );
                    // Console.WriteLine(sb.ToString());
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }

                /* end change invoice */

                //update org status
                sb = new StringBuilder();
                sb.AppendFormat("select OrganizationId from Main_organization where OrgId = '" + data.OrgId + "' ;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o4 = JObject.Parse(result);
                orgid = o4["OrganizationId"].ToString();

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Main_organization set " +
                    "Status = 'A' " +
                    "where OrganizationId = '" + orgid + "'"
                );
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

            }
            catch
            {
                res =
                    "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Insert Data Failed\"" +
                    "}";
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Invoices;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());

            if (count2 > count1)
            {
                string description = "Insert data Invoice <br/>" +
                    "OrgId = " + data.OrgId + "<br/>" +
                    "Invoice Number = " + data.InvoiceNumber + "<br/>" +
                    "Invoice Description = " + data.InvoiceDescription;


                _audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());


                res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"Insert Data Success\"" +
                    "}";
            }

            return Ok(res);

        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {
            JObject o1;
            // JObject o2;
            // JObject o3;
            JObject o4;
            JObject o5;
            // string invoiceid= "";
            string orgid = "";
            string siteid = "";
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";
            try
            {
                //update invoice
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Invoices set " +
                    "InvoiceNumber='" + data.InvoiceNumber + "'," +
                    // "ContractID='"+data.ContractID+"',"+
                    // "InvoiceType='"+data.InvoiceType+"',"+
                    // "ContractStatus='"+data.ContractStatus+"',"+
                    "InvoiceDescription='" + data.InvoiceDescription + "'," +
                    // "InvoiceDate='"+data.InvoiceDate+"',"+
                    "InvoiceDate='" + data.POReceivedDate + "'," +
                    // "POID='"+data.POID+"',"+
                    // "ContractStartDate='"+data.ContractStartDate+"',"+
                    // "ContractEndDate='"+data.ContractEndDate+"',"+
                    // "OldContractId='"+data.OldContractId+"',"+
                    "InvoicedCurrency='" + data.InvoicedCurrency + "'," +
                    // "SiteCountryDistributorSKUId='"+data.SiteCountryDistributorSKUId+"',"+
                    "NumberOfLicenses='" + data.NumberOfLicenses + "'," +
                    "FinancialYear='" + data.FinancialYear + "'," +
                    // "DateAdded='"+data.DateAdded+"',"+
                    // "AddedBy='"+data.AddedBy+"',"+
                    "TotalInvoicedAmount='" + data.TotalInvoicedAmount + "'," +
                    "DocuSign='" + data.DocuSign + "'," +
                    "DistributorFee='" + data.DistributorFee + "'," +
                    "TotalPaymentAmount='" + data.TotalPaymentAmount + "'" +
                    "where InvoiceId='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                //update org status
                sb = new StringBuilder();
                sb.AppendFormat("select OrganizationId from Main_organization where OrgId = '" + data.OrgId + "' ;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o4 = JObject.Parse(result);
                orgid = o4["OrganizationId"].ToString();

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Main_organization set " +
                    "Status='A'" +
                    "where OrganizationId='" + orgid + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                //update invoicessites and site status
                for (int i = 0; i < data.SitesArr.Count; i++)
                {
                    if (data.SitesArr[i][1] == true)
                    {
                        if (data.SitesArr[i][2] == "InsertNew")
                        {
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "insert into InvoicesSites " +
                                "(InvoiceId, SiteId, Status) " +
                                "values ('" + id + "','" + data.SitesArr[i][0] + "','A');"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            sb = new StringBuilder();
                            sb.AppendFormat("select SiteIdInt from Main_site where SiteId = '" + data.SitesArr[i][0] + "' ;");
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                            o5 = JObject.Parse(result);
                            siteid = o5["SiteIdInt"].ToString();

                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update Main_site set " +
                                "Status='A'" +
                                "where SiteIdInt='" + siteid + "'"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            /* update status partner type nya */

                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update SiteRoles set " +
                                "Status='A'" +
                                "where SiteId = '" + data.SitesArr[i][0] + "'"
                            );
                            // Console.WriteLine(sb.ToString());
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            /* update status partner type nya */
                        }
                        else
                        {
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update InvoicesSites set " +
                                "Status='A'" +
                                "where InvoicesSitesId='" + data.SitesArr[i][2] + "'"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            sb = new StringBuilder();
                            sb.AppendFormat("select SiteIdInt from Main_site where SiteId = '" + data.SitesArr[i][0] + "' ;");
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                            o5 = JObject.Parse(result);
                            siteid = o5["SiteIdInt"].ToString();

                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update Main_site set " +
                                "Status='A'" +
                                "where SiteIdInt='" + siteid + "'"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            /* update status partner type nya */

                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update SiteRoles set " +
                                "Status='A'" +
                                "where SiteId = '" + data.SitesArr[i][0] + "'"
                            );
                            // Console.WriteLine(sb.ToString());
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            /* update status partner type nya */
                        }
                    }
                    else
                    {
                        if (data.SitesArr[i][2] == "InsertNew")
                        {
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "insert into InvoicesSites " +
                                "(InvoiceId, SiteId, Status) " +
                                "values ('" + id + "','" + data.SitesArr[i][0] + "','X');"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            sb = new StringBuilder();
                            sb.AppendFormat("select SiteIdInt from Main_site where SiteId = '" + data.SitesArr[i][0] + "' ;");
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                            o5 = JObject.Parse(result);
                            siteid = o5["SiteIdInt"].ToString();

                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update Main_site set " +
                                "Status='X'" +
                                "where SiteIdInt='" + siteid + "'"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            /* update status partner type nya */

                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update SiteRoles set " +
                                "Status='X'" +
                                "where SiteId = '" + data.SitesArr[i][0] + "'"
                            );
                            // Console.WriteLine(sb.ToString());
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            /* update status partner type nya */
                        }
                        else
                        {
                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update InvoicesSites set " +
                                "Status='X'" +
                                "where InvoicesSitesId='" + data.SitesArr[i][2] + "'"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            sb = new StringBuilder();
                            sb.AppendFormat("select SiteIdInt from Main_site where SiteId = '" + data.SitesArr[i][0] + "' ;");
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                            o5 = JObject.Parse(result);
                            siteid = o5["SiteIdInt"].ToString();

                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update Main_site set " +
                                "Status='T'" +
                                "where SiteIdInt='" + siteid + "'"
                            );
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            /* update status partner type nya */

                            sb = new StringBuilder();
                            sb.AppendFormat(
                                "update SiteRoles set " +
                                "Status='T'" +
                                "where SiteId = '" + data.SitesArr[i][0] + "'"
                            );
                            // Console.WriteLine(sb.ToString());
                            sqlCommand = new MySqlCommand(sb.ToString());
                            ds = oDb.getDataSetFromSP(sqlCommand);

                            /* update status partner type nya */
                        }
                    }
                }

                /* change invoice */

                // for(int i=0; i<data.SKUarr.Count;i++){
                //     if(data.SKUarr[i][1] == "insert"){
                //         sb = new StringBuilder();
                //         sb.AppendFormat(
                //             "insert into InvoiceSKU "+
                //             "(InvoiceId, SiteCountryDistributorSKUId) "+
                //             "values ('"+id+"','"+data.SKUarr[i][0]+"');"
                //         );
                //         sqlCommand = new MySqlCommand(sb.ToString());
                //         ds = oDb.getDataSetFromSP(sqlCommand);
                //     }
                //     else if(data.SKUarr[i][1] == "delete"){
                //         sb = new StringBuilder();
                //         sb.AppendFormat(
                //             "delete from InvoiceSKU "+
                //             "where InvoiceId='"+id+"' AND SiteCountryDistributorSKUId = '"+data.SKUarr[i][0]+"'"
                //         ); 
                //         sqlCommand = new MySqlCommand(sb.ToString());
                //         ds = oDb.getDataSetFromSP(sqlCommand);
                //     }
                // }

                // delete -> add aja biar gampang, kajen nyampah id
                sb = new StringBuilder();
                sb.AppendFormat(
                    "delete from InvoiceSKU " +
                    "where InvoiceId='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                JArray skuarr = JArray.Parse(data.MultiSKU.ToString());
                for (int i = 0; i < skuarr.Count; i++)
                {
                    sb = new StringBuilder();
                    sb.AppendFormat(
                        "insert into InvoiceSKU " +
                        "(InvoiceId, SiteCountryDistributorSKUId, QTY, TotalAmmount) " +
                        "values ('" + id + "','" + skuarr[i]["SiteCountryDistributorSKUId"] + "','" + skuarr[i]["QTY"] + "','" + skuarr[i]["Total"] + "');"
                    );
                    sqlCommand = new MySqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                }

                /* end change invoice */
            }
            catch
            {
                Console.WriteLine("failed");
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from Invoices where InvoiceId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);

            string description = "Update data Invoice <br/>" +
                "OrgId = " + data.OrgId + "<br/>" +
                "Invoice Number = " + data.InvoiceNumber + "<br/>" +
                "Invoice Description = " + data.InvoiceDescription;

            _audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());

            res =
                "{" +
                "\"code\":\"1\"," +
                "\"message\":\"Update Data Success\"" +
                "}";

            return Ok(res);
        }



        [HttpPost("DeleteInvoice/{id}")]
        public IActionResult Delete(int id)
        {

            string res = "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Error when processing your request\"" +
                    "}";
            try
            {
                //var userId = Request.Headers["UserId"];
                var userId = HttpContext.User.GetUserId();

                if (!string.IsNullOrEmpty(userId))
                {
                    var contact = _mySQLContext.ContactsAll.FirstOrDefault(u => u.ContactId == userId.ToString());
                    if (contact != null)
                    {
                        var invoiceEntity = _mySQLContext.Invoices.FirstOrDefault(x => x.InvoiceId == id);
                        if (contact.UserLevelId != AutodeskConst.UserLevelIdEnum.SUPERADMIN && invoiceEntity != null && invoiceEntity.Status == AutodeskConst.StatusConts.X)
                        {
                            res = "{" +
                    "\"code\":\"0\"," +
                    "\"message\":\"Distributor is not allowed to delete Paid invoice\"" +
                    "}";
                            return Ok(res);
                        }

                        //delete from InvoiceSKU
                        var listInvoiceSKU = _mySQLContext.InvoiceSku.Where(isku => isku.InvoiceId == id);
                        _mySQLContext.InvoiceSku.RemoveRange(listInvoiceSKU);

                        _mySQLContext.Invoices.Remove(invoiceEntity);

                        var log = Utilities.SetHistoryData(invoiceEntity.OrgId, contact.ContactName, AutodeskConst.LogDescription.Delete, "Invoice");
                        _auditLogServices.LogHistory(log);
                        _mySQLContext.SaveChanges();
                        res =
                             "{" +
                             "\"code\":\"1\"," +
                             "\"message\":\"Delete Data Success\"" +
                             "}";

                        return Ok(res);
                    }

                }

                return Ok(res);
            }
            catch (Exception)
            {

                return Ok(res);
            }
        }

        [HttpPost("reportInvoices")]
        public IActionResult findBy([FromBody] dynamic data)
        {
            JArray columnOrg;
            string query = "";
            string selectedColumns = "";
            string anotherfiled = "";
            selectedColumns += "select ";

            /*fix issue excel no 263.Show data based role*/

            string role = "";
            string orgid = "";

            /*end fix issue excel no 263.Show data based role*/

            int n = 0;
            JObject column = JObject.FromObject(data);
            foreach (JProperty property in column.Properties())
            {
                if (property.Name.Equals("fieldOrg"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        StringBuilder builder = new StringBuilder(property.Value.ToString());

                        builder.Replace("o.DateAdded", "DATE_FORMAT(o.DateAdded, '%d-%m-%Y') AS DateAdded");
                        builder.Replace("o.DateLastAdmin", "DATE_FORMAT(o.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        string kolomOrg = builder.ToString();
                        if (kolomOrg.Contains("Status"))
                        {
                            kolomOrg = kolomOrg.Replace("o.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= o.Status)  'Status'");
                        }
                        if (kolomOrg.Contains("RegisteredCountryCode"))
                        {
                            kolomOrg = kolomOrg.Replace("o.RegisteredCountryCode", "rc.countries_name as 'Org Country'");
                        }
                        selectedColumns += kolomOrg;

                        n = n + 1;
                    }
                    else
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat("DESCRIBE Main_organization");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        hasil = MySqlDb.GetJSONArrayString(dsTemp.Tables[0]);
                        columnOrg = JArray.Parse(hasil);

                        string[] columnarrtmp = new string[columnOrg.Count];
                        for (int index = 0; index < columnOrg.Count; index++)
                        {
                            columnarrtmp[index] = "o." + columnOrg[index]["Field"].ToString();
                        }

                        StringBuilder builder = new StringBuilder(string.Join(",", columnarrtmp));

                        builder.Replace("o.DateAdded", "DATE_FORMAT(o.DateAdded, '%d-%m-%Y') AS DateAdded");
                        builder.Replace("o.DateLastAdmin", "DATE_FORMAT(o.DateLastAdmin, '%d-%m-%Y') AS DateLastAdmin");
                        string kolomOrg = builder.ToString();
                        if (kolomOrg.Contains("Status"))
                        {
                            kolomOrg = kolomOrg.Replace("o.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= o.Status)  'Status'");
                        }
                        if (kolomOrg.Contains("RegisteredCountryCode"))
                        {
                            kolomOrg = kolomOrg.Replace("o.RegisteredCountryCode", "rc.countries_name as 'Org Country'");
                        }
                        selectedColumns += kolomOrg;
                        n = n + 1;
                    }
                }

                if (property.Name.Equals("anotherField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            anotherfiled = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            anotherfiled += "," + property.Value.ToString();
                        }
                        anotherfiled = anotherfiled.Replace("m.MarketType", "m.MarketType as 'Market Type'");
                        anotherfiled = anotherfiled.Replace("r.RoleName", "r.RoleName as 'Partner Type'");
                        anotherfiled = anotherfiled.Replace("sr.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= sr.Status) as 'Partner Type Status'");
                        anotherfiled = anotherfiled.Replace("sr.CSN", "sr.CSN as 'Partner Type CSN'");
                        anotherfiled = anotherfiled.Replace("sr.ITS_ID", "sr.ITS_ID as 'ITS Site ID'");
                        anotherfiled = anotherfiled.Replace("sr.UParent_CSN", "sr.UParent_CSN as 'Ultimate Parent CSN'");
                        anotherfiled = anotherfiled.Replace("rg.geo_name", "rg.geo_name as 'Geo'");
                        anotherfiled = anotherfiled.Replace("rr.region_name", "rr.region_name as 'Region'");
                        anotherfiled = anotherfiled.Replace("rsr.subregion_name", "rsr.subregion_name as 'SubRegion'");
                        anotherfiled = anotherfiled.Replace("t.Territory_Name", "t.Territory_Name as 'Territory'");
                        anotherfiled = anotherfiled.Replace("ja.ActivityName", "ja.ActivityName as 'Organization Journal Entries'");
                        anotherfiled = anotherfiled.Replace("j.ActivityDate", "j.ActivityDate as 'Organization Journal Entries Date'");
                        anotherfiled = anotherfiled.Replace("sku.Currency", "sku.Currency as 'Currency'");
                        selectedColumns += anotherfiled;
                        n = n + 1;
                    }
                }

                if (property.Name.Equals("invoicesField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            selectedColumns += property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            selectedColumns += "," + property.Value.ToString();
                        }
                        n = n + 1;
                    }
                }

                /*fix issue excel no 263.Show data based role*/

                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        role = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        orgid = property.Value.ToString();
                    }
                }

                /*end fix issue excel no 263.Show data based role*/
            }

            query += selectedColumns + " FROM Main_organization o " +
                 " join Invoices i on o.OrgId = i.OrgId " +
                 " LEFT join SiteCountryDistributorSKU sku on i.SiteCountryDistributorSKUId = sku.SiteCountryDistributorSKUId " +
                 " join Main_site s on o.OrgId = s.OrgId" +
                 " join Ref_countries rc on o.RegisteredCountryCode = rc.countries_code" +
                 " join MarketType m on rc.MarketTypeId = m.MarketTypeId " +
                 " join SiteRoles sr on s.SiteId = sr.SiteId " +
                 " join Ref_geo rg on rc.geo_code = rg.geo_code " +
                 " join Ref_region rr on rc.region_code = rr.region_code " +
                 " join Ref_subregion rsr on rc.subregion_code = rsr.subregion_code " +
                 " join Ref_territory t on rc.TerritoryId = t.TerritoryId " +
                 " join Roles r on sr.RoleId = r.RoleId " +
                 " join Journals j ON i.OrgId = j.ParentId " +
                 " join Dictionary d ON i.InvoicedCurrency = d.Key and d.Parent = 'Currencies' and d.`Status` = 'A'" +
                 " join JournalActivities ja ON j.ActivityId = ja.ActivityId";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {

                /*fy indikator na null kabeh di tabel invoice coba monyet */
                // if(property.Name.Equals("FinancialYear")){   
                //     if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
                //         if (i == 0) { query += " where "; }
                //         if (i > 0) { query += " and "; }

                //         JArray fyi;
                //         sb = new StringBuilder();
                //         sb.AppendFormat("select * from Dictionary where Parent = 'FYIndicator' and `Key` = '"+property.Value.ToString()+"'");
                //         Console.WriteLine(sb.ToString());
                //         sqlCommand = new MySqlCommand(sb.ToString());
                //         ds = oDb.getDataSetFromSP(sqlCommand);
                //         string tmp = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                //         Console.WriteLine(tmp); 
                //         fyi = JArray.Parse(tmp);
                //         Console.WriteLine(fyi[0]["KeyValue"]); 
                //         query += "i.FinancialYear = '"+fyi[0]["KeyValue"].ToString()+"' ";  
                //         i=i+1;
                //     }
                // }

                if (property.Name.Equals("filterByInvoicesNumber"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "i.InvoiceNumber like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPOReceivedDate"))
                {
                    if ((property.Value.ToString() != ",") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        string[] startendfilter = property.Value.ToString().Split(',');
                        query += "i.POReceivedDate BETWEEN '" + startendfilter[0] + "' AND '" + startendfilter[1] + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.RoleId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.Status in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByMarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.MarketTypeId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByMarketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.MarketTypeId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByRegion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.region_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySubRegion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.subregion_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByTerritory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "rc.TerritoryId in (" + property.Value.ToString() + ") ";

                        /*fix issue excel no 263.Show data based role*/

                        if (role == "")
                        {
                            query += "rc.TerritoryId in (" + property.Value.ToString() + ") ";
                        }
                        else if (role == "DISTRIBUTOR")
                        {
                            query += "rc.TerritoryId in (select rc.TerritoryId from Ref_countries rc INNER JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + orgid + ") GROUP BY rc.TerritoryId) ";
                        }
                        else
                        {
                            query += "rc.TerritoryId in (" + property.Value.ToString() + ") AND o.OrgId in (" + orgid + ") ";
                        }

                        /*end fix issue excel no 263.Show data based role*/

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountries"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.countries_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

            }

            query += " group by o.OrgId desc";
            List<dynamic> dynamicList = new List<dynamic>();

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {

                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    // ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicList = dataServices.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);    
            }
            return Ok(result);
        }

        [HttpGet("InvoiceTable")]
        public IActionResult InvoiceTable()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("DESCRIBE Invoices");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("GetListSKU")]
        public async Task<IActionResult> GetMasterSKUList(int SKUType/*, string FYYear = null, string territory = null, string country = null*/)
        {
            switch (SKUType)
            {
                case AutodeskConst.SKUType.Master:
                    var result = await Task.Run(() => _mySQLContext.SiteCountryDistributorSKU.Where(m => m.Status == AutodeskConst.StatusConts.A).Select(m => m).ToList());
                    return Ok(result);
                case AutodeskConst.SKUType.Pricing:
                    var query = await Task.Run(() => (from msku in _mySQLContext.SiteCountryDistributorSKU
                                                      join psku in _mySQLContext.InvoicePricingSKU on msku.SiteCountryDistributorSKUId equals psku.SiteCountryDistributorSKUId
                                                      //join isku in _mySQLContext.InvoiceSKU on psku.Id equals isku.PricingSKUId
                                                      where psku.Status == AutodeskConst.StatusConts.A
                                                      select new
                                                      {
                                                          psku.Id,
                                                          psku.SiteId,
                                                          msku.FYIndicatorKey,
                                                          msku.SKUName,
                                                          psku.Territory,
                                                          psku.Region,
                                                          psku.Country,
                                                          psku.Distributor,
                                                          psku.Description,
                                                          msku.License,
                                                          psku.Price,
                                                          Currency = "USD",
                                                          psku.Status,
                                                          msku.PartnerType

                                                      }).ToList());



                    if (query?.Count > 0)
                    {
                        //var res =  query.Select(q => q).ToList() ;

                        //if (!string.IsNullOrEmpty(FYYear))
                        //{
                        //    res.PricingSKUs.Where(p => p.FYIndicatorKey == FYYear).ToList();
                        //}
                        //if (!string.IsNullOrEmpty(territory))
                        //{
                        //    res.PricingSKUs.Where(p => p.Territory == territory).ToList();
                        //}
                        //if (!string.IsNullOrEmpty(country))
                        //{
                        //    res.PricingSKUs.Where(p => p.Country == country).ToList();
                        //}
                        return Ok(query);
                    }
                    else
                    {
                        return Ok();
                    }

                default:
                    return Ok();
            }

        }

        [HttpGet("GetMasterSKUDetails")]
        public async Task<IActionResult> GetMasterSKUDetails(int id)
        {
            var res = await Task.Run(() => _mySQLContext.SiteCountryDistributorSKU.FirstOrDefault(m => m.SiteCountryDistributorSKUId == id && m.Status == AutodeskConst.StatusConts.A));
            if (res != null)
            {
                return Ok(new
                {
                    res.IsForPrimarySite,
                    res.SiteCountryDistributorSKUId,
                    res.License,
                    res.FYIndicatorKey,
                    res.PartnerType,
                    res.SKUName,
                    res.Status,
                });
            }
            return Ok();


        }

        [HttpGet("GetPricingSKUDetails")]
        public async Task<IActionResult> GetPricingSKUDetails(int id)
        {
            var res = await Task.Run(() => _mySQLContext.InvoicePricingSKU.FirstOrDefault(m => m.Id == id));
            if (res != null)
            {
                return Ok(new
                {
                    siteCountryDistributorSKUId = res.SiteCountryDistributorSKUId,
                    res.Id,
                    res.Price,
                    res.Region,
                    res.SiteId,
                    res.Territory,
                    res.Country,
                    res.Description,
                    res.Distributor,
                    res.Status,
                });
            }
            return Ok();


        }


        [HttpPost("AddMasterSKU")]
        public async Task<IActionResult> AddMasterSKU([FromBody] dynamic data)
        {
            var res =
                          "{" +
                          "\"code\":\"0\"," +
                          "\"message\":\"Insert Data Failed\"" +
                          "}";
            try
            {
                string jsonData = data.ToString();
                var entity = JsonConvert.DeserializeObject<SiteCountryDistributorSKU>(jsonData);
                entity.SiteId = AutodeskConst.NAN;
                entity.CountryCode = AutodeskConst.NAN;
                entity.EdistributorType = AutodeskConst.NAN;
                entity.Skudescription = entity.SKUName;
                entity.Currency = AutodeskConst.Currency.USD;
                entity.Price = "0";

                await _mySQLContext.SiteCountryDistributorSKU.AddAsync(entity);
                _mySQLContext.SaveChanges();
                res =
                          "{" +
                          "\"code\":\"1\"," +
                          "\"message\":\"Insert Data Success\"" +
                          "}";
                return Ok(res);

            }
            catch (Exception ex)
            {

                return Ok(res);
            }

        }
        [HttpPost("EditMasterSKU")]
        public async Task<IActionResult> EditMasterSKU([FromBody] dynamic data)
        {
            var res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Update Data Failed\"" +
                        "}";

            if (data != null)
            {
                try
                {
                    string jsonData = data.ToString();
                    var model = JsonConvert.DeserializeObject<SiteCountryDistributorSKU>(jsonData);
                    int id = model.SiteCountryDistributorSKUId;
                    var editedEnt = await Task.Run(() => _mySQLContext.SiteCountryDistributorSKU.FirstOrDefault(x => x.SiteCountryDistributorSKUId == id));
                    if (editedEnt != null)
                    {
                        editedEnt.CountryCode = AutodeskConst.NAN;
                        editedEnt.Currency = AutodeskConst.Currency.USD;
                        editedEnt.SiteId = AutodeskConst.NAN;
                        editedEnt.EdistributorType = AutodeskConst.NAN;

                        editedEnt.FYIndicatorKey = model.FYIndicatorKey;
                        editedEnt.License = model.License;
                        editedEnt.PartnerType = model.PartnerType;
                        editedEnt.Price = "0";
                        editedEnt.SKUName = model.SKUName;
                        editedEnt.Skudescription = model.SKUName;
                        editedEnt.Status = model.Status;
                        editedEnt.IsForPrimarySite = model.IsForPrimarySite;

                        _mySQLContext.SiteCountryDistributorSKU.Update(editedEnt);
                        _mySQLContext.SaveChanges();
                        res =
                                  "{" +
                                  "\"code\":\"1\"," +
                                  "\"message\":\"Update Data Success\"" +
                                  "}";
                    }
                }
                catch (Exception ex)
                {

                    return Ok(res);
                }
            }


            return Ok(res);
        }

        [HttpPost("AddPricingSKU")]
        public async Task<IActionResult> AddPricingSKU([FromBody] dynamic data)
        {
            var res =
                          "{" +
                          "\"code\":\"0\"," +
                          "\"message\":\"Insert Data Failed\"" +
                          "}";
            try
            {
                string jsonData = data.ToString();
                var entity = JsonConvert.DeserializeObject<InvoicePricingSKU>(jsonData);
                await _mySQLContext.InvoicePricingSKU.AddAsync(entity);
                _mySQLContext.SaveChanges();
                res =
                          "{" +
                          "\"code\":\"1\"," +
                          "\"message\":\"Insert Data Success\"" +
                          "}";
                return Ok(res);

            }
            catch (Exception ex)
            {

                return Ok(res);
            }

        }

        [HttpPost("EditPricingSKU")]
        public async Task<IActionResult> EditPricingSKU([FromBody] dynamic data)
        {
            var res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Update Data Failed\"" +
                        "}";
            if (data != null)
            {
                try
                {
                    string jsonData = data.ToString();
                    var entity = JsonConvert.DeserializeObject<InvoicePricingSKU>(jsonData);
                    int id = entity.Id;
                    var editedEnt = _mySQLContext.InvoicePricingSKU.FirstOrDefault(x => x.Id == id);
                    if (editedEnt != null)
                    {
                        editedEnt.Territory = entity.Territory;
                        editedEnt.Region = entity.Region;
                        editedEnt.Country = entity.Country;
                        editedEnt.Distributor = entity.Distributor;
                        editedEnt.SiteCountryDistributorSKUId = entity.SiteCountryDistributorSKUId;
                        editedEnt.Price = entity.Price;
                        editedEnt.Status = entity.Status;
                        editedEnt.Description = entity.Description;
                        editedEnt.SiteId = entity.SiteId;

                        _mySQLContext.InvoicePricingSKU.Update(editedEnt);
                        await _mySQLContext.SaveChangesAsync();
                        res =
                                  "{" +
                                  "\"code\":\"1\"," +
                                  "\"message\":\"Update Data Success\"" +
                                  "}";
                    }
                }
                catch (Exception ex)
                {

                    return Ok(res);
                }
            }

            return Ok(res);
        }

        [HttpGet("GetListFY")]
        public async Task<IActionResult> GetListFY()
        {
            var query = await Task.Run(() => _mySQLContext.Dictionary.Where(c => c.Parent == "FYIndicator" && c.Status == AutodeskConst.StatusConts.A).OrderBy(d => d.Key).Select(c => new { Name = c.KeyValue, Value = c.KeyValue, Key = c.Key }).Distinct().ToList());

            if (query?.Count > 0)
            {
                return Ok(query);
            }
            return Ok();
        }
        [HttpGet("GetListPartnerType")]
        public async Task<IActionResult> GetListPartnerType()
        {
            //var res = await Task.Run(() => _mySQLContext.Roles.Where(c => c.RoleCode == "ATC" || c.RoleCode == "AAP").Select(c => c.RoleCode).Distinct().ToList());
            var res = await Task.Run(() => _mySQLContext.Roles.Where(c => c.RoleType == "Company").Select(c => c.RoleCode).Distinct().ToList());

            if (res?.Count > 0)
            {
                return Ok(res);
            }
            return Ok();
        }

        [HttpGet("GetListDistributor")]
        public async Task<IActionResult> GetListDistributor()
        {
            var query = await Task.Run(() => _mySQLContext.Roles.Where(c => c.RoleType == "Company").Select(c => c.RoleCode).Distinct().ToList());


            if (query?.Count > 0)
            {
                return Ok(query);
            }
            return Ok();
        }

        [HttpGet("GetListTerritory")]
        public async Task<IActionResult> GetListTerritory()
        {
            var query = await Task.Run(() => _mySQLContext.RefTerritory.Select(c => new { TerritoryId = c.TerritoryId, TerritoryName = c.TerritoryName }).Distinct().ToList());


            if (query?.Count > 0)
            {
                return Ok(query);
            }
            return Ok();
        }

        [HttpGet("GetListTerritoryById")]
        public async Task<IActionResult> GetListTerritoryById(int Tid)
        {
            RefTerritory territory = new RefTerritory();
            if (Tid > 0)
            {
                territory = await dataServices.GetERefTerritoryById(Tid);
                if (territory != null)
                {
                    return Ok(new { TerritoryId = territory.TerritoryId, TerritoryName = territory.TerritoryName });
                }
            }


            return Ok();
        }

        [HttpGet("GetListCountry")]
        public async Task<IActionResult> GetListCountry(int territoryId)
        {

            var query = await Task.Run(() => _mySQLContext.RefCountries.Where(c => c.TerritoryId == territoryId).Select(c => new { RegionCode = c.RegionCode, RegionName = c.RegionName }).Distinct().ToList());


            if (query?.Count > 0)
            {
                return Ok(query);
            }
            return Ok();
        }

        [HttpGet("GetListInvoiceCountry")]
        public async Task<IActionResult> GetListInvoiceCountry(int territoryId, string orgId = null)
        {
            var query = await Task.Run(() => _mySQLContext.RefCountries.Where(c => c.TerritoryId == territoryId).Select(c => new { CountriesCode = c.CountriesCode, CountriesName = c.CountriesName }).Distinct().ToList());
            if (!string.IsNullOrEmpty(orgId))
            {
                var orgEntity = _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == orgId);
                if (orgEntity != null)
                {
                    query = query.Where(c => c.CountriesCode == orgEntity.InvoicingCountryCode).ToList();
                }
            }

            if (query?.Count > 0)
            {
                return Ok(query);
            }
            return Ok();
        }

        [HttpGet("GetListPricingSKU")]
        public async Task<IActionResult> GetListPricingSKU(string orgId, string countryCode)
        {
            var organization = _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == orgId);
            if (organization != null)
            {
                //fix issue from customer feedback 28-1-2019
                //get org region
                var orgCtry = _mySQLContext.RefCountries.FirstOrDefault(c => c.CountriesCode == organization.RegisteredCountryCode);

                //var siteCountryCodes = _mySQLContext.MainSite.Where(s => s.OrgId == orgId).Select(s => s.SiteCountryCode).ToList();
                var listPartnerType = (from s in _mySQLContext.MainSite
                                       join sr in _mySQLContext.SiteRoles on s.SiteId equals sr.SiteId
                                       join r in _mySQLContext.Roles on sr.RoleId equals r.RoleId
                                       where r.RoleType == "Company" && s.OrgId == orgId
                                       select r.RoleCode
                                      ).ToList();

                var listPricingSKU = new List<ListPricingSKUDto>();

                var _query = (from msku in _mySQLContext.SiteCountryDistributorSKU
                              join psku in _mySQLContext.InvoicePricingSKU on msku.SiteCountryDistributorSKUId equals psku.SiteCountryDistributorSKUId
                              //join isku in _mySQLContext.InvoiceSKU on psku.Id equals isku.PricingSKUId
                              //join s in _mySQLContext.MainSite on psku.SiteId equals s.SiteId
                              //where psku.Region == regionCode
                              where listPartnerType.Contains(msku.PartnerType) && (!string.IsNullOrEmpty(psku.Status) && psku.Status != AutodeskConst.StatusConts.X)
                              select new ListPricingSKUDto
                              {
                                  IsForPrimarySite = msku.IsForPrimarySite,
                                  SKUName = msku.SKUName,
                                  Region = psku.Region,
                                  CountryCode = psku.Country,
                                  SiteId = string.Empty,
                                  Id = psku.Id,
                                  Distributor = msku.PartnerType,
                                  Description = psku.Description,
                                  QTY = 0,
                                  License = msku.License,
                                  Price = psku.Price,
                              }).ToList();


                //var siteQuery = (from s in _mySQLContext.MainSite
                //                 join c in _mySQLContext.RefCountries on s.SiteCountryCode equals c.CountriesCode
                //                 where siteCountryCodes.Contains(c.CountriesCode)
                //                 select c.RegionCode).Distinct();

                //foreach (var item in siteQuery)
                //{
                //    listPricingSKU.AddRange(_query.Where(s => s.Region == item));
                //}
                if (orgCtry != null)
                {
                    listPricingSKU.AddRange(_query.Where(s => s.Region == orgCtry.RegionCode));
                }

                listPricingSKU = listPricingSKU.DistinctBy(l => l.Id).ToList();
                //var query = await Task.Run(() => (from msku in _mySQLContext.MasterSKU
                //                                  join psku in _mySQLContext.InvoicePricingSKU on msku.SiteCountryDistributorSKUId equals psku.SiteCountryDistributorSKUId
                //                                  //join isku in _mySQLContext.InvoiceSKU on psku.Id equals isku.PricingSKUId
                //                                  //join s in _mySQLContext.MainSite on psku.SiteId equals s.SiteId
                //                                  where psku.Region == regionCode
                //                                  select new
                //                                  {
                //                                      Region = psku.Region,
                //                                      CountryCode = psku.Country,
                //                                      SiteId = string.Empty,
                //                                      psku.Id,
                //                                      Distributor = msku.PartnerType,
                //                                      psku.Description,
                //                                      QTY = 0,
                //                                      msku.License,
                //                                      psku.Price
                //                                  }).ToList());


                //return Ok(query);


                var result = await Task.Run(() => listPricingSKU.Select(l => new
                {
                    l.SKUName,
                    l.IsForPrimarySite,
                    l.Region,
                    l.CountryCode,
                    l.SiteId,
                    l.Id,
                    l.Distributor,
                    l.Description,
                    l.QTY,
                    l.License,
                    l.Price

                }));
                return Ok(result);

            }

            return Ok();
        }

        private string GetRegionCode(string countryCode)
        {
            var country = _mySQLContext.RefCountries.FirstOrDefault(c => c.CountriesCode == countryCode);
            if (country != null)
            {
                return country.RegionCode;
            }
            return string.Empty;
        }
        private bool IsPrimary(string orgId, string siteId)
        {
            var query = (from o in _mySQLContext.MainOrganization
                         join s in _mySQLContext.MainSite on o.OrgId equals s.OrgId
                         where o.OrgId == orgId
                         select o.PrimarySiteId).ToList();
            if (query.Contains(siteId))
            {
                return true;
            }
            return false;

        }
        private bool? IsPrimary(bool? IsForPrimarySite)
        {
            return IsForPrimarySite.HasValue ? IsForPrimarySite.Value : false;

        }
        [HttpGet("GetListSite")]
        public async Task<IActionResult> GetListSite(string orgId, string countrycode)
        {
            //var _query = await Task.Run(() => _mySQLContext.MainSite.Where(c => c.OrgId == orgId && !string.IsNullOrEmpty(c.Status) && c.Status != AutodeskConst.StatusConts.X)
            //.Join(_mySQLContext.SiteRoles,x=>x.SiteId,sr=>sr.SiteId,(s,sr)=> new {
            //    s.SiteId,
            //    s.SiteName,
            //    s.Status,
            //    RegionCode = GetRegionCode(s.SiteCountryCode),
            //    IsPrimary = IsPrimary(orgId, s.SiteId),
            //    sr.RoleId
            //}).Join(_mySQLContext.Roles,x=>x.RoleId,r=>r.RoleId,(s,r)=>new {
            //    s,
            //    r.RoleCode
            //})
            //);
            var groupRole = (from s in _mySQLContext.MainSite.Where(c => c.OrgId == orgId && !string.IsNullOrEmpty(c.Status) && c.Status != AutodeskConst.StatusConts.X && c.Status != AutodeskConst.StatusConts.D)
                             join sr in _mySQLContext.SiteRoles on s.SiteId equals sr.SiteId
                             join r in _mySQLContext.Roles on sr.RoleId equals r.RoleId
                             join dic in _mySQLContext.Dictionary.Where(d => d.Status == AutodeskConst.StatusConts.A && d.Parent == "SiteStatus") on sr.Status equals dic.Key
                             where sr.Status != AutodeskConst.StatusConts.X && sr.Status != AutodeskConst.StatusConts.T
                             select new
                             {
                                 s.SiteId,
                                 r.RoleCode,
                                 Status = dic.KeyValue
                             }
                             into listRole
                             group listRole by new { listRole.SiteId }

                             ).Select(x => new
                             {
                                 SiteId = x.Key.SiteId,
                                 Status = string.Join(',', x.Select(r => r.Status).Distinct()),
                                 PartnerType = string.Join(',', x.Select(r => r.RoleCode).Distinct())
                             });
            //var listStatus = _mySQLContext.Dictionary.Where(d => d.Status == AutodeskConst.StatusConts.A && d.Parent == "SiteStatus");
            var _res = await Task.Run(() => (from s in _mySQLContext.MainSite
                                             join r in groupRole on s.SiteId equals r.SiteId
                                             join ctry in _mySQLContext.RefCountries on s.SiteCountryCode equals ctry.CountriesCode
                                             join o in _mySQLContext.MainOrganization.Where(x => x.OrgId == orgId) on s.OrgId equals o.OrgId
                                             where s.OrgId == orgId && !string.IsNullOrEmpty(r.Status) && r.Status != AutodeskConst.StatusConts.X

                                             select new
                                             {
                                                 s.SiteId,
                                                 s.SiteName,
                                                 Status = /*(r.Status != AutodeskConst.StatusConts.A && r.Status != AutodeskConst.StatusConts.I) ? AutodeskConst.StatusConts.I :*/ r.Status/*.Contains(',') ? string.Join(',', listStatus.Where(d => r.Status.Contains(d.Key)).Select(s => s.KeyValue)) : listStatus.FirstOrDefault(x => x.Key == r.Status).KeyValue,*/,
                                                 RegionCode = ctry.RegionCode,
                                                 IsPrimary = o.PrimarySiteId == s.SiteId ? true : false,
                                                 r.PartnerType
                                             }).ToList());


            //var listStatus = new List<string> { "B", "R", "S", "V" };
            //var _query = await Task.Run(() => (from s in _mySQLContext.MainSite
            //                                   join sr in _mySQLContext.SiteRoles on s.SiteId equals sr.SiteId
            //                                   join r in _mySQLContext.Roles on sr.RoleId equals r.RoleId
            //                                   where s.OrgId == orgId && s.SiteCountryCode == countrycode /*&& listStatus.Contains(s.Status) */

            //                                   select new
            //                                   {
            //                                       s,
            //                                       r.RoleCode
            //                                   }).ToList());

            if (_res?.Count > 0)
            {
                //var result = await Task.Run(() => _query.Select(s => new
                //{
                //    s.SiteId,
                //    s.SiteName,
                //    s.Status,
                //    RegionCode = GetRegionCode(s.SiteCountryCode),
                //    IsPrimary = IsPrimary(orgId, s.SiteId)
                //}));
                //return Ok(result);
                return Ok(_res);
            }
            return Ok();
        }
        private string GetSiteStatus(List<Dictionary> dictionaries, string status)
        {
            var split = status.Split(',');
            if (split.Length > 0)
            {
                string res = string.Empty;
                var listKey = new List<string>();
                foreach (var item in split)
                {
                    foreach (var _dic in dictionaries)
                    {
                        if (_dic.Key == item)
                        {
                            listKey.Add(_dic.KeyValue);
                        }
                    }

                }

                return string.Join(',', listKey);
            }
            //foreach (var item in dictionaries)
            //{
            //    if (item.Key == status)
            //    {
            //        return item.KeyValue;
            //    }
            //}

            return string.Empty;
        }

        [HttpGet("GetPartnerTypes")]
        public async Task<IActionResult> GetPartnerTypes()
        {

            //var listStatus = new List<string> { "B", "R", "S", "V" };
            //var _query = await Task.Run(() => (from s in _mySQLContext.MainSite
            //                                   join sr in _mySQLContext.SiteRoles on s.SiteId equals sr.SiteId
            //                                   join r in _mySQLContext.Roles on sr.RoleId equals r.RoleId
            //                                   where listStatus.Contains(sr.Status)

            //                                   select new
            //                                   {
            //                                       ParterType = r.RoleCode
            //                                   }).Distinct().ToList());


            var _query = await Task.Run(() => _mySQLContext.Roles.Where(r => r.RoleType == "Company" && r.RoleName != "RED" && r.RoleName != "MED").Select(r => new
            {
                ParterType = r.RoleCode
            }).Distinct().ToList());

            if (_query?.Count > 0)
            {
                return Ok(_query);
            }
            return Ok();
        }

        [HttpGet("PopulateTerritoryAndCountry")]
        public async Task<IActionResult> PopulateTerritoryAndCountry(string OrganizationId)
        {
            var query = await Task.Run(() => _mySQLContext.MainOrganization.FirstOrDefault(c => c.OrgId == OrganizationId));

            if (query != null)
            {
                var country = _mySQLContext.RefCountries.FirstOrDefault(c => c.CountriesCode == query.InvoicingCountryCode);
                if (country != null)
                {

                    var result = new { CountriesCode = country.CountriesCode, TerritoryId = country.TerritoryId };
                    return Ok(result);
                }
            }
            return Ok();
        }

        [HttpPost("AddInvoice")]
        public async Task<IActionResult> AddInvoice([FromBody]dynamic dynamicData)
        {
            var res =
                       "{" +
                       "\"code\":\"0\"," +
                       "\"message\":\"Insert Data Failed\"" +
                       "}";
            var log = new History();
            if (dynamicData != null)
            {
                try
                {
                    var jsonData = dynamicData.ToString();
                    AddInvoiceDto addInvoiceDto = JsonConvert.DeserializeObject<AddInvoiceDto>(jsonData);

                    //var currentYear = DateTime.Now.Year;
                    //var _1stFeb = new DateTime(currentYear, 2, 1);
                    //var _31stJul = new DateTime(currentYear, 7, 31);
                    //var _1stAug = new DateTime(currentYear, 8, 1);
                    //var _31stJan = new DateTime(currentYear + 1, 1, 31);
                    //var _1stAugLastYear = new DateTime(currentYear - 1, 8, 1);
                    //var _31stJanCurrentYear = new DateTime(currentYear, 1, 31);
                    var invoiceEntity = new Invoices
                    {
                        OrgId = addInvoiceDto.OrgId,
                        InvoiceDate = addInvoiceDto.InvoiceDate,
                        InvoiceNumber = addInvoiceDto.InvoiceNumber,
                        InvoiceDescription = addInvoiceDto.InvoiceDescription,
                        FinancialYear = addInvoiceDto.FinancialYear,
                        Status = addInvoiceDto.Status == AutodeskConst.InvoiceStatus.Pending ? AutodeskConst.StatusConts.A : AutodeskConst.StatusConts.X,
                        //Status = AutodeskConst.StatusConts.A,
                        DateAdded = DateTime.Now,
                        Comments = "Added"
                    };
                    //var userId = Request.Headers["UserId"];
                    var userId = HttpContext.User.GetUserId();
                    if (!string.IsNullOrEmpty(userId))
                    {
                        //invoiceEntity.DateLastAdmin = DateTime.Now;
                        invoiceEntity.DateAdded = DateTime.Now;
                        var contact = _mySQLContext.ContactsAll.FirstOrDefault(u => u.ContactId == userId.ToString());
                        if (contact != null)
                        {
                            invoiceEntity.AddedBy = contact.ContactId;
                            //invoiceEntity.LastAdminBy = contact.ContactName;
                            log = Utilities.SetHistoryData(invoiceEntity.OrgId, contact.ContactName);

                        }

                    }
                    await _mySQLContext.Invoices.AddAsync(invoiceEntity);
                    //_mySQLContext.SaveChanges();
                    log.AddDescription(string.Format(AutodeskConst.LogDescription.Insert, "Invoice", "InvoiceId=" + invoiceEntity.InvoiceId));

                    int proRataDiscountId = 0;
                    //if (addInvoiceDto.InvoiceDate == null)
                    //{
                    //    addInvoiceDto.InvoiceDate = DateTime.Now;
                    //}

                    if ((addInvoiceDto.Discounts.ProRata != null && addInvoiceDto.Discounts.ProRata.Amount != null) || (addInvoiceDto.Discounts.Discount != null && addInvoiceDto.Discounts.Discount.Amount != null))
                    {

                        if (addInvoiceDto.Discounts.Discount != null && addInvoiceDto.Discounts.Discount.Amount != null)
                        {
                            if (addInvoiceDto.Discounts.Discount.AmountType == AutodeskConst.AmountType.Percentage && addInvoiceDto.Discounts.Discount.Amount > 100)

                            {
                                res =
                   "{" +
                   "\"code\":\"0\"," +
                   "\"message\":\"Discount must not greater than total ammount\"" +
                   "}";
                                return Ok(res);
                            }
                            else if (addInvoiceDto.Discounts.Discount.AmountType == AutodeskConst.AmountType.Exactly)
                            {
                                double totalAmount = 0;
                                foreach (var item in addInvoiceDto.PricingSKUs)
                                {
                                    totalAmount += ((int.Parse(item.Price) * item.QTY));
                                }
                                if (addInvoiceDto.Discounts.Discount.Amount > totalAmount)
                                {
                                    res =
                  "{" +
                  "\"code\":\"0\"," +
                  "\"message\":\"Discount must not greater than total ammount\"" +
                  "}";
                                    return Ok(res);
                                }
                            }

                            await AddDiscount(addInvoiceDto, invoiceEntity, log);
                        }
                        _mySQLContext.SaveChanges();
                        if (addInvoiceDto.Discounts.ProRata != null && addInvoiceDto.Discounts.ProRata.Amount != null)//pro-rata
                        {
                            var discount = addInvoiceDto.Discounts.ProRata;
                            if (!string.IsNullOrEmpty(discount.Name) && discount.Amount.HasValue && discount.Amount.Value > 0)
                            {
                                var discountEntity = new InvoiceDiscount
                                {
                                    InvoiceId = invoiceEntity.InvoiceId,
                                    Name = AutodeskConst.DiscountName.ProRata,
                                    Amount = 50,
                                    AmountType = AutodeskConst.AmountType.Percentage
                                };
                                _mySQLContext.InvoiceDiscount.Add(discountEntity);

                                invoiceEntity.Status = AutodeskConst.StatusConts.A;
                                _mySQLContext.Invoices.Update(invoiceEntity);
                                //_mySQLContext.SaveChanges();
                                log.AddDescription("With ProRata:" + discountEntity.Amount + "%");
                                proRataDiscountId = discountEntity.Id;
                            }

                        }
                        //update due to customer feedback
                        //change to SendInvoiceNotificationEmail

                        //await SendEmail(addInvoiceDto, invoiceEntity);
                        await SendInvoiceNotificationEmail(string.Empty, invoiceEntity, AutodeskConst.EmailType.ApprovedDiscount, false);
                        invoiceEntity.DocuSign = AutodeskConst.EmailType.MailSent;
                    }


                    //var pricingGroup = addInvoiceDto.PricingSKUs.GroupBy(x => x.SiteId).Select(x => new { SiteId = x.Key, PricingSKUs = x.Select(p => p) });
                    //var listSKU = new List<ListPricingSKUDto>();
                    //foreach (var item in pricingGroup)
                    //{
                    //    var listParnerType = GetListPartnerType(item.SiteId);
                    //    if (listParnerType.Any(x=>x.Contains("ATC")&&x.Contains("AAP")))
                    //    {

                    //    }
                    //    if (item.PricingSKUs.Any(x => x.Distributor == "ATC") && item.PricingSKUs.Any(x => x.Distributor == "AAP"))
                    //    {
                    //        var listAAP = item.PricingSKUs.Where(x => x.Distributor == "AAP").Select(x => new ListPricingSKUDto
                    //        {
                    //            CountryCode = x.CountryCode,
                    //            Description = x.Description,
                    //            Distributor = x.Distributor,
                    //            Id = x.Id,
                    //            IsForPrimarySite = x.IsForPrimarySite,
                    //            License = x.License,
                    //            Price = "0",
                    //            QTY = x.QTY,
                    //            Region = x.Region,
                    //            SiteId = x.SiteId,
                    //            SKUName = x.SKUName
                    //        }).ToList();
                    //        var listATC = item.PricingSKUs.Where(x => x.Distributor == "ATC").ToList();
                    //        listSKU.AddRange(listAAP);
                    //        listSKU.AddRange(listATC);
                    //    }
                    //    else if (item.PricingSKUs.Any(x => x.Distributor == "ATC") && !item.PricingSKUs.Any(x => x.Distributor == "AAP"))
                    //    {
                    //        listSKU.AddRange(item.PricingSKUs);
                    //    }
                    //    else if (!item.PricingSKUs.Any(x => x.Distributor == "ATC") && item.PricingSKUs.Any(x => x.Distributor == "AAP"))
                    //    {
                    //        listSKU.AddRange(item.PricingSKUs);
                    //    }
                    //}
                    if (addInvoiceDto.PricingSKUs.Any(x => x.QTY == 0))
                    {
                        res =
                          "{" +
                          "\"code\":\"0\"," +
                          "\"message\":\"Quantity must be at least  1.\"" +
                          "}";
                        return Ok(res);
                    }
                    else
                    {
                        //_mySQLContext.SaveChanges();
                        foreach (var item in addInvoiceDto.PricingSKUs.Where(x => x.QTY > 0))
                        {

                            var invoiceSKU = new InvoiceSku();

                            invoiceSKU.InvoiceId = invoiceEntity.InvoiceId;
                            invoiceSKU.PricingSKUId = item.Id;
                            invoiceSKU.Qty = item.QTY;
                            invoiceSKU.SiteId = item.SiteId;
                            //if (proRataDiscountId > 0)
                            //{
                            //    invoiceSKU.TotalAmmount = (Convert.ToDouble(item.Price) * item.QTY) * 0.5;
                            //}
                            //else
                            {
                                invoiceSKU.TotalAmmount = (Convert.ToDouble(item.Price) * item.QTY);
                            }


                            await _mySQLContext.InvoiceSku.AddAsync(invoiceSKU);
                            if (item.IsForPrimarySite.HasValue && item.IsForPrimarySite == true)
                            {
                                invoiceEntity.HasMainSKU = string.Join(',', invoiceEntity.FinancialYear);
                            }

                            _mySQLContext.Invoices.Update(invoiceEntity);

                        }
                        _mySQLContext.SaveChanges();
                    }




                    _auditLogServices.LogHistory(log);
                    res =
                              "{" +
                              "\"code\":\"1\"," +
                              "\"message\":\"Insert Data Success\"" +
                              "}";
                    return Ok(res);

                }
                catch (Exception ex)
                {

                    return Ok(res);
                }
            }
            else
            {
                res =
                         "{" +
                         "\"code\":\"0\"," +
                         "\"message\":\"Data can not be null.\"" +
                         "}";
                return Ok(res);
            }

        }
        private List<string> GetListPartnerType(string siteId)
        {
            var groupRole = (from s in _mySQLContext.MainSite.Where(c => c.SiteId == siteId)
                             join sr in _mySQLContext.SiteRoles on s.SiteId equals sr.SiteId
                             join r in _mySQLContext.Roles on sr.RoleId equals r.RoleId
                             select new
                             {
                                 s.SiteId,
                                 r.RoleCode,

                             }
                             into listRole
                             group listRole by listRole.SiteId

                             ).Select(x => new
                             {
                                 PartnerType = string.Join(',', x.Select(r => r.RoleCode).Distinct())
                             });

            return groupRole.Select(x => x.PartnerType).ToList();
        }

        private async Task AddDiscount(AddInvoiceDto addInvoiceDto, Invoices invoiceEntity, History log = null)
        {
            if (addInvoiceDto.Discounts.Discount != null && addInvoiceDto.Discounts.Discount.Amount != null)
            {
                var discount = addInvoiceDto.Discounts.Discount;
                var discountEntity = new InvoiceDiscount
                {
                    InvoiceId = invoiceEntity.InvoiceId,
                    Name = AutodeskConst.DiscountName.Discount,
                    Amount = discount.Amount.HasValue ? discount.Amount.Value : 0,
                    AmountType = discount.AmountType
                };

                await _mySQLContext.InvoiceDiscount.AddAsync(discountEntity);
                invoiceEntity.Status = AutodeskConst.StatusConts.A;
                _mySQLContext.Invoices.Update(invoiceEntity);
                //_mySQLContext.SaveChanges();
                if (log != null)
                {
                    log.AddDescription("With Discount:" + discountEntity.Amount + discountEntity.AmountType == AutodeskConst.AmountType.Exactly ? "$" : "%");
                }

            }
        }
        private async Task SendEmail(AddInvoiceDto addInvoiceDto, Invoices invoiceEntity)
        {

            //send mail to approve
            var list = _mySQLContext.EmailBodyType.ToList();
            var emailBodyType = _mySQLContext.EmailBodyType.Where(t => t.EmailBodyTypeName.Contains("Approve Invoice Discount"));
            if (emailBodyType != null && emailBodyType.FirstOrDefault() != null)
            {
                var entity = emailBodyType.FirstOrDefault();
                var emailBody = _mySQLContext.EmailBody.FirstOrDefault(b => b.BodyType == entity.EmailBodyTypeId.ToString());
                if (emailBody != null)
                {
                    string domainName = AppConfig.Config["EmailSender:ApproveDiscountHost"];
                    var bodyContent = emailBody.BodyEmail;
                    bodyContent = bodyContent.Replace("[ReceiverName]", AppConfig.Config["EmailSender:ApproveDiscountReceiverName"]);
                    bodyContent = bodyContent.Replace("[DateAdded]", invoiceEntity.DateAdded.Value.ToShortDateString());
                    bodyContent = bodyContent.Replace("[InvoiceNumber]", invoiceEntity.InvoiceNumber);
                    var discountDes = string.Empty;
                    if (addInvoiceDto.Discounts.ProRata != null && addInvoiceDto.Discounts.ProRata.Amount != null)
                    {
                        var _proRate = addInvoiceDto.Discounts.ProRata;
                        discountDes += "Discount include:</br>Pro-Rata:" + _proRate.Amount + " %</br>";
                    }
                    if (addInvoiceDto.Discounts.Discount != null && addInvoiceDto.Discounts.Discount.Amount != null)
                    {
                        var _discount = addInvoiceDto.Discounts.Discount;
                        discountDes += "Discount:" + _discount.Amount + " " + (_discount.AmountType == AutodeskConst.AmountType.Percentage ? "%" : "USD");
                    }

                    bodyContent = bodyContent.Replace("[DiscountDescription]", discountDes);
                    bodyContent = bodyContent.Replace("[InvoiceUrl]", string.Format("<a href='" + AppConfig.Config["EmailSender:ApproveDiscountUrl"] + "' target='_blank' >here</a>", domainName, invoiceEntity.InvoiceId));

                    var emailRes = await _emailSender.SendMailAsync(AppConfig.Config["EmailSender:ApproveDiscountReceiverEmail"], emailBody.Subject, bodyContent);
                }

            }
        }

        private double GetDouble(string price, int? qty)
        {
            if (!string.IsNullOrEmpty(price) && qty.HasValue)
            {
                return double.Parse(price) * qty.Value;
            }
            return 0.0;
        }
        private async Task<List<SummaryDto>> GetSummary(int invoiceId)
        {
            var query = await Task.Run(() => (from i in _mySQLContext.Invoices
                                              join isku in _mySQLContext.InvoiceSku on i.InvoiceId equals isku.InvoiceId
                                              join psku in _mySQLContext.InvoicePricingSKU on isku.PricingSKUId equals psku.Id
                                              join msku in _mySQLContext.SiteCountryDistributorSKU on psku.SiteCountryDistributorSKUId equals msku.SiteCountryDistributorSKUId
                                              where i.InvoiceId == invoiceId
                                              select new
                                              {
                                                  Distributor = msku.PartnerType,
                                                  isku.Qty,
                                                  TotalAmmount = GetDouble(psku.Price, isku.Qty)

                                              } into summary
                                              group summary by summary.Distributor
                                        ).Select(s => new SummaryDto
                                        {
                                            Distributor = s.Key,
                                            QTY = s.Sum(a => a.Qty),
                                            TotalAmount = s.Sum(a => a.TotalAmmount)

                                        }).ToList());

            return query;

        }
        private async Task<List<SummaryDto>> GetSummary(string finacialYear)
        {
            var query = await Task.Run(() => (from msku in _mySQLContext.SiteCountryDistributorSKU
                                              join psku in _mySQLContext.InvoicePricingSKU on msku.SiteCountryDistributorSKUId equals psku.SiteCountryDistributorSKUId
                                              where msku.FYIndicatorKey == finacialYear
                                              select new SummaryDto
                                              {
                                                  Distributor = msku.PartnerType,
                                                  QTY = 0,
                                                  TotalAmount = 0,

                                              }).ToList());

            return query;

        }
        [HttpGet("GetInvoice")]
        public async Task<IActionResult> GetInvoice(int invoiceId)
        {
            var query = await Task.Run(() => _mySQLContext.Invoices.FirstOrDefault(i => i.InvoiceId == invoiceId));
            if (query != null)
            {

                query.Status = GetInvoiceStatus(query.Status);
                string regionCode = string.Empty;
                var countryCode = _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == query.OrgId)?.InvoicingCountryCode;
                if (!string.IsNullOrEmpty(countryCode))
                {
                    var country = _mySQLContext.RefCountries.FirstOrDefault(c => c.CountriesCode == countryCode);
                    if (country != null)
                    {
                        regionCode = country.RegionCode;
                    }


                }
                var listPartnerType = (from s in _mySQLContext.MainSite
                                       join sr in _mySQLContext.SiteRoles on s.SiteId equals sr.SiteId
                                       join r in _mySQLContext.Roles on sr.RoleId equals r.RoleId
                                       where r.RoleType == "Company" && s.OrgId == query.OrgId
                                       select r.RoleCode
                                    ).ToList();

                var Discount = _mySQLContext.InvoiceDiscount.FirstOrDefault(d => d.InvoiceId == invoiceId && d.Name == AutodeskConst.DiscountName.Discount);
                var ProRata = _mySQLContext.InvoiceDiscount.FirstOrDefault(d => d.InvoiceId == invoiceId && d.Name == AutodeskConst.DiscountName.ProRata);
                var discoundto = new { Discount, ProRata };
                var Summary = await GetSummary(invoiceId);
                var listPricingSKU = await Task.Run(() => (from i in _mySQLContext.Invoices
                                                           join isku in _mySQLContext.InvoiceSku on i.InvoiceId equals isku.InvoiceId
                                                           join psku in _mySQLContext.InvoicePricingSKU on isku.PricingSKUId equals psku.Id
                                                           join msku in _mySQLContext.SiteCountryDistributorSKU on psku.SiteCountryDistributorSKUId equals msku.SiteCountryDistributorSKUId
                                                           where i.InvoiceId == invoiceId && (!string.IsNullOrEmpty(psku.Status) && psku.Status != AutodeskConst.StatusConts.X)
                                                           where psku.Region == regionCode && listPartnerType.Contains(msku.PartnerType)
                                                           select new
                                                           {
                                                               IsForPrimarySite = msku.IsForPrimarySite,
                                                               isku.SiteId,
                                                               msku.SKUName,
                                                               psku.Id,
                                                               Distributor = msku.PartnerType,
                                                               psku.Description,
                                                               isku.Qty,
                                                               msku.License,
                                                               psku.Price,
                                                               psku.Region
                                                           }).ToList());

                string lastAdminBy = string.Empty;
                DateTime? lastUpdate = null;

                if (query.DateLastUpdate.HasValue && query.DateLastAdmin.HasValue)
                {
                    var dateLastAdmin = query.DateLastAdmin.Value;
                    if (DateTime.MinValue != dateLastAdmin)
                    {

                        if (query.DateLastUpdate.Value > query.DateLastAdmin.Value)
                        {
                            var user = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactName == query.LastUpdateBy);
                            if (user != null)
                            {
                                lastAdminBy = !string.IsNullOrEmpty(user.ContactName) ? user.ContactName : user.EnglishContactName;
                                lastUpdate = query.DateLastUpdate;
                            }
                        }
                        else
                        {
                            var user = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactName == query.LastAdminBy);
                            if (user != null)
                            {
                                lastAdminBy = !string.IsNullOrEmpty(user.ContactName) ? user.ContactName : user.EnglishContactName;
                                lastUpdate = query.DateLastAdmin;
                            }
                        }
                    }


                }
                else if (query.DateLastUpdate.HasValue)
                {
                    var user = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactName == query.LastUpdateBy);
                    if (user != null)
                    {
                        lastAdminBy = !string.IsNullOrEmpty(user.ContactName) ? user.ContactName : user.EnglishContactName;
                        lastUpdate = query.DateLastUpdate;
                    }
                }
                else if (query.DateLastAdmin.HasValue)
                {
                    var dateLastAdmin = query.DateLastAdmin.Value;
                    if (DateTime.MinValue != dateLastAdmin)
                    {
                        var user = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactName == query.LastAdminBy);
                        if (user != null)
                        {
                            lastAdminBy = !string.IsNullOrEmpty(user.ContactName) ? user.ContactName : user.EnglishContactName;
                            lastUpdate = query.DateLastAdmin;
                        }
                    }

                }

                return Ok(new
                {
                    lastAdminBy,
                    lastUpdate,
                    ApprovedBy = query.Comments == AutodeskConst.DiscountAction.Approved && query.DateLastAdmin.HasValue ? (query.LastAdminBy + " on " + query.DateLastAdmin.Value.ToString("yyyy-MM-dd")) : string.Empty,
                    RejectedBy = query.Comments == AutodeskConst.DiscountAction.Rejected && query.DateLastAdmin.HasValue ? (query.LastAdminBy + " on " + query.DateLastAdmin.Value.ToString("yyyy-MM-dd")) : string.Empty,
                    query.InvoiceNumber,
                    query.InvoiceId,
                    query.InvoiceDescription,
                    query.InvoiceDate,
                    query.OrgId,
                    OrganizationId = _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == query.OrgId).OrganizationId,
                    query.Status,
                    query.FinancialYear,
                    query.DateAdded,
                    query.Comments,
                    Discounts = discoundto,
                    Summary,
                    listPricingSKU
                });
            }

            return Ok();

        }


        [HttpPost("ApplyDiscount")]
        public async Task<IActionResult> ApplyDiscount([FromBody]dynamic dynamicData)
        {
            var res =
                       "{" +
                       "\"code\":\"0\"," +
                       "\"message\":\"Update Data Failed\"" +
                       "}";
            var log = new History();
            try
            {
                if (dynamicData != null)
                {
                    var jsonData = dynamicData.ToString();
                    UpdateInvoiceDto updateInvoiceDto = JsonConvert.DeserializeObject<UpdateInvoiceDto>(jsonData);
                    try
                    {

                        var invoiceEntity = await Task.Run(() => _mySQLContext.Invoices.FirstOrDefault(i => i.InvoiceId == updateInvoiceDto.InvoiceId));
                        var invoiceDiscountEntity = _mySQLContext.InvoiceDiscount.Where(i => i.InvoiceId == updateInvoiceDto.InvoiceId);

                        if (invoiceEntity != null)
                        {
                            invoiceEntity.InvoiceDate = updateInvoiceDto.InvoiceDate;
                            invoiceEntity.InvoiceNumber = updateInvoiceDto.InvoiceNumber;
                            invoiceEntity.InvoiceDescription = updateInvoiceDto.InvoiceDescription;
                            invoiceEntity.FinancialYear = updateInvoiceDto.FinancialYear;
                            if (invoiceDiscountEntity?.Count() > 0 && invoiceEntity.Comments == "Approved")
                            {
                                res =
                                "{" +
                                "\"code\":\"0\"," +
                                "\"message\":\"This Invoice has already approved.\"" +
                                "}";
                                return Ok(res);
                            }


                            //var userId = Request.Headers["UserId"];
                            var userId = HttpContext.User.GetUserId();
                            if (!string.IsNullOrEmpty(userId))
                            {
                                var contact = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactId == userId.ToString());
                                if (contact != null)
                                {
                                    log = Utilities.SetHistoryData(invoiceEntity.OrgId, contact.ContactName, AutodeskConst.LogDescription.Update, "Invoice", "InvoiceId=" + invoiceEntity.InvoiceId, "Approve Discount");
                                    invoiceEntity.DateLastAdmin = DateTime.Now;
                                    invoiceEntity.LastAdminBy = contact.ContactName;
                                    _mySQLContext.Invoices.Update(invoiceEntity);

                                }
                            }
                            if (updateInvoiceDto.InvoiceDate == null)
                            {
                                updateInvoiceDto.InvoiceDate = DateTime.Now;
                            }
                            if (updateInvoiceDto.Discounts != null && updateInvoiceDto.Discounts.Discount != null && updateInvoiceDto.Discounts.Discount.Amount > 0)
                            {
                                var _discount = updateInvoiceDto.Discounts.Discount;
                                if (_discount.AmountType == AutodeskConst.AmountType.Percentage && _discount.Amount > 100)
                                {
                                    res =
              "{" +
              "\"code\":\"0\"," +
              "\"message\":\"Discount must not greater than total ammount\"" +
              "}";
                                    return Ok(res);
                                }
                                else if (_discount.AmountType == AutodeskConst.AmountType.Exactly)
                                {
                                    double totalAmount = 0;
                                    foreach (var item in updateInvoiceDto.PricingSKUs)
                                    {
                                        totalAmount += ((int.Parse(item.Price) * item.QTY));
                                    }
                                    if (_discount.Amount > totalAmount)
                                    {
                                        res =
                      "{" +
                      "\"code\":\"0\"," +
                      "\"message\":\"Discount must not greater than total ammount\"" +
                      "}";
                                        return Ok(res);
                                    }
                                }
                                if (_discount.Id != null && _discount.Id != 0)
                                {
                                    var discountEntity = _mySQLContext.InvoiceDiscount.FirstOrDefault(d => d.Id == _discount.Id);
                                    discountEntity.Amount = _discount.Amount.HasValue ? _discount.Amount.Value : 0;
                                    discountEntity.AmountType = _discount.AmountType;
                                    _mySQLContext.InvoiceDiscount.Update(discountEntity);

                                    if (invoiceEntity.Status == AutodeskConst.StatusConts.X)
                                    {
                                        invoiceEntity.Status = AutodeskConst.StatusConts.A;
                                    }

                                    if (updateInvoiceDto.PricingSKUs.Any(x => x.QTY == 0))
                                    {
                                        res =
                     "{" +
                     "\"code\":\"0\"," +
                     "\"message\":\"Quantity must be at least  1.\"" +
                     "}";
                                        return Ok(res);
                                    }
                                    else
                                    {
                                        invoiceEntity.Status = SetInvoiceStatus(updateInvoiceDto.Status);
                                        await UpdateInvoiceSKU(updateInvoiceDto, invoiceEntity, discountEntity);

                                    }
                                }
                                else
                                {
                                    if (invoiceEntity.Status == AutodeskConst.StatusConts.X)
                                    {
                                        invoiceEntity.Status = AutodeskConst.StatusConts.A;
                                    }
                                    var discountEntity = new InvoiceDiscount();
                                    discountEntity.Amount = _discount.Amount.HasValue ? _discount.Amount.Value : 0;
                                    discountEntity.AmountType = _discount.AmountType;
                                    discountEntity.Name = AutodeskConst.DiscountName.Discount;
                                    discountEntity.InvoiceId = invoiceEntity.InvoiceId;
                                    _mySQLContext.InvoiceDiscount.Add(discountEntity);


                                    if (updateInvoiceDto.PricingSKUs.Any(x => x.QTY == 0))
                                    {
                                        res =
                     "{" +
                     "\"code\":\"0\"," +
                     "\"message\":\"Quantity must be at least  1.\"" +
                     "}";
                                        return Ok(res);
                                    }
                                    else
                                    {
                                        invoiceEntity.Status = SetInvoiceStatus(updateInvoiceDto.Status);
                                        await UpdateInvoiceSKU(updateInvoiceDto, invoiceEntity, discountEntity);

                                    }
                                }
                                //send email notification to submitter.

                                invoiceEntity.Comments = "Approved";
                                _mySQLContext.Invoices.Update(invoiceEntity);
                                _mySQLContext.SaveChanges();
                                var user = _mySQLContext.ContactsAll.FirstOrDefault(u => u.ContactId == invoiceEntity.AddedBy);
                                if (user != null)
                                {
                                    await SendInvoiceNotificationEmail(user.EmailAddress, invoiceEntity, AutodeskConst.EmailType.ApprovedDiscount);
                                }



                                _auditLogServices.LogHistory(log);
                                res =
                           "{" +
                           "\"code\":\"1\"," +
                           "\"message\":\"Update Data Success\"" +
                           "}";
                                return Ok(res);
                            }
                            var newInvoiceSKUs = new List<InvoiceSku>();

                            if (updateInvoiceDto.PricingSKUs.Any(x => x.QTY == 0))
                            {
                                res =
                     "{" +
                     "\"code\":\"0\"," +
                     "\"message\":\"Quantity must be at least  1.\"" +
                     "}";
                                return Ok(res);
                            }
                            else
                            {

                                var invoiceSKUs = _mySQLContext.InvoiceSku.Where(i => i.InvoiceId == updateInvoiceDto.InvoiceId).ToList();
                                _mySQLContext.InvoiceSku.RemoveRange(invoiceSKUs);


                                foreach (var item in updateInvoiceDto.PricingSKUs.Where(x => x.QTY > 0))
                                {
                                    var _invoiceSKU = new InvoiceSku();

                                    _invoiceSKU.InvoiceId = invoiceEntity.InvoiceId;
                                    _invoiceSKU.PricingSKUId = item.Id;
                                    _invoiceSKU.Qty = item.QTY;
                                    _invoiceSKU.SiteId = item.SiteId;

                                    {
                                        _invoiceSKU.TotalAmmount = (Convert.ToDouble(item.Price) * item.QTY);
                                    }


                                    newInvoiceSKUs.Add(_invoiceSKU);
                                }
                                await _mySQLContext.AddRangeAsync(newInvoiceSKUs);

                            }
                            _mySQLContext.SaveChanges();
                            if (updateInvoiceDto.Discounts != null && updateInvoiceDto.Discounts.ProRata != null && updateInvoiceDto.Discounts.ProRata.Amount != null)
                            {
                                var discount = updateInvoiceDto.Discounts.ProRata;

                                if (discount.Id == null || discount.Id == 0)
                                {
                                    discount.AmountType = AutodeskConst.AmountType.Percentage;

                                    var proRataDiscount = new InvoiceDiscount
                                    {
                                        Amount = 50,
                                        InvoiceId = invoiceEntity.InvoiceId,
                                        Name = AutodeskConst.DiscountName.ProRata,
                                        AmountType = discount.AmountType
                                    };
                                    _mySQLContext.InvoiceDiscount.Add(proRataDiscount);

                                    if (invoiceEntity.Status == AutodeskConst.StatusConts.X)
                                    {
                                        invoiceEntity.Status = AutodeskConst.StatusConts.A;
                                    }


                                }
                                foreach (var item in newInvoiceSKUs)
                                {
                                    item.TotalAmmount = item.TotalAmmount * 0.5;
                                    _mySQLContext.InvoiceSku.Update(item);
                                    _mySQLContext.SaveChanges();
                                }

                                invoiceEntity.Comments = "Approved";
                                invoiceEntity.DateLastAdmin = DateTime.Now;

                                _mySQLContext.Invoices.Update(invoiceEntity);
                                _mySQLContext.SaveChanges();

                                var user = _mySQLContext.ContactsAll.FirstOrDefault(u => u.ContactId == invoiceEntity.AddedBy);
                                if (user != null)
                                {
                                    await SendInvoiceNotificationEmail(user.EmailAddress, invoiceEntity, AutodeskConst.EmailType.ApprovedDiscount);
                                }
                                _auditLogServices.LogHistory(log);
                                res =
                               "{" +
                               "\"code\":\"1\"," +
                               "\"message\":\"Update Data Success\"" +
                               "}";
                                return Ok(res);

                            }
                        }
                        res =
                          "{" +
                          "\"code\":\"0\"," +
                          "\"message\":\"Data can not be null.\"" +
                          "}";
                        return Ok(res);

                    }
                    catch (Exception ex)
                    {

                        return Ok(res);
                    }
                }
                else
                {
                    res =
                             "{" +
                             "\"code\":\"0\"," +
                             "\"message\":\"Data can not be null.\"" +
                             "}";
                    return Ok(res);
                }
            }
            catch (Exception ex)
            {

                return Ok(res);
            }

        }
        [Route("CheckInvoiceMainSKU")]
        [HttpGet]
        public async Task<IActionResult> CheckInvoiceMainSKU(string orgId, string financialYear)
        {
            //var res = await Task.Run(() => CheckMainSKU(orgId, financialYear));

            var listInvoice = await Task.Run(() => _mySQLContext.Invoices.Where(x => x.OrgId == orgId && !string.IsNullOrEmpty(x.HasMainSKU) && x.HasMainSKU.ToString().Contains(financialYear) && x.Status != AutodeskConst.StatusConts.C).Select(x => new { InvoiceId = x.InvoiceId, HasMainSKU = x.HasMainSKU }).ToList());
            if (listInvoice?.Count > 0)
            {
                return Json(new { data = listInvoice });
            }
            return Json(new { data = listInvoice });
        }
        private bool CheckMainSKU(string orgId, string FY)
        {
            var exist = _mySQLContext.Invoices.Any(x => x.OrgId == orgId && !string.IsNullOrEmpty(x.HasMainSKU) && x.HasMainSKU.ToString().Contains(FY) && x.Status != AutodeskConst.StatusConts.C);
            return exist;
        }
        private async Task UpdateInvoiceSKU(UpdateInvoiceDto updateInvoiceDto, Invoices invoiceEntity, InvoiceDiscount discountEntity = null)
        {
            var invoiceSKUs = _mySQLContext.InvoiceSku.Where(i => i.InvoiceId == updateInvoiceDto.InvoiceId).ToList();
            _mySQLContext.InvoiceSku.RemoveRange(invoiceSKUs);
            _mySQLContext.SaveChanges();
            var newInvoiceSKUs = new List<InvoiceSku>();
            foreach (var item in updateInvoiceDto.PricingSKUs.Where(x => x.QTY > 0))
            {
                var _invoiceSKU = new InvoiceSku();

                _invoiceSKU.InvoiceId = invoiceEntity.InvoiceId;
                _invoiceSKU.PricingSKUId = item.Id;
                _invoiceSKU.Qty = item.QTY;
                _invoiceSKU.SiteId = item.SiteId;

                _invoiceSKU.TotalAmmount = (Convert.ToDouble(item.Price) * item.QTY);
                var checkMainSKU = CheckMainSKU(invoiceEntity.OrgId, invoiceEntity.FinancialYear);
                if (item.IsForPrimarySite.HasValue && item.IsForPrimarySite == true && !checkMainSKU)
                {
                    invoiceEntity.HasMainSKU = string.Join(',', invoiceEntity.FinancialYear);
                    _mySQLContext.Invoices.Update(invoiceEntity);
                }


                newInvoiceSKUs.Add(_invoiceSKU);
            }
            await _mySQLContext.AddRangeAsync(newInvoiceSKUs);
            _mySQLContext.SaveChanges();
            foreach (var item in newInvoiceSKUs)
            {
                if (discountEntity != null)
                {


                    if (discountEntity.AmountType == AutodeskConst.AmountType.Percentage)
                    {
                        var amount = item.TotalAmmount * (discountEntity.Amount / 100);
                        item.TotalAmmount = item.TotalAmmount - amount;
                    }
                    else
                    {
                        item.TotalAmmount = item.TotalAmmount - discountEntity.Amount;
                    }
                    _mySQLContext.InvoiceSku.Update(item);
                }
            }
            _mySQLContext.SaveChanges();
        }

        [HttpPost("RejectInvoice")]
        public async Task<IActionResult> RejectInvoice([FromBody]RejectInvoiceDto rejectInvoiceDto)
        {
            var res =
                       "{" +
                       "\"code\":\"0\"," +
                       "\"message\":\"Update Data Failed\"" +
                       "}";
            var log = new History();
            try
            {
                if (!string.IsNullOrEmpty(rejectInvoiceDto.RejectReason) && rejectInvoiceDto.RejectReason.Length < 500)
                {

                    var invoice = _mySQLContext.Invoices.Where(iv => iv.InvoiceId == rejectInvoiceDto.InvoiceId);
                    if (invoice != null && invoice.FirstOrDefault() != null)
                    {
                        var invoiceEntity = invoice.FirstOrDefault();

                        invoiceEntity.Comments = "Rejected";
                        invoiceEntity.RejectReason = rejectInvoiceDto.RejectReason;
                        invoiceEntity.Status = AutodeskConst.StatusConts.A;
                        invoiceEntity.DateLastAdmin = DateTime.Now;

                        var userId = HttpContext.User.GetUserId();
                        if (!string.IsNullOrEmpty(userId))
                        {
                            var user = _mySQLContext.ContactsAll.FirstOrDefault(u => u.ContactId == userId.ToString());
                            if (user != null)
                            {
                                invoiceEntity.LastAdminBy = user.ContactName;
                                log = Utilities.SetHistoryData(invoiceEntity.OrgId, user.ContactName, AutodeskConst.LogDescription.Update, "Invoice", "InvoiceId=" + invoiceEntity.InvoiceId, "Reject Discount");
                            }

                        }
                        _mySQLContext.Invoices.Update(invoiceEntity);
                        _mySQLContext.SaveChanges();
                        _auditLogServices.LogHistory(log);
                        //send reject mail
                        var contact = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactId == invoiceEntity.AddedBy);
                        if (contact != null)
                        {
                            await SendInvoiceNotificationEmail(contact.EmailAddress, invoiceEntity, AutodeskConst.EmailType.RejectedDiscount);
                        }
                        res =
                           "{" +
                           "\"code\":\"1\"," +
                           "\"message\":\"Update Data Success\"" +
                           "}";
                    }

                }
            }
            catch (Exception ex)
            {

                return Ok(res);
            }

            return Ok(res);

        }

        /// <summary>
        /// Send invoice email notificaiton for SA or Distributor
        /// </summary>
        /// <param name="email"></param>
        /// <param name="invoiceEntity"></param>
        /// <param name="emailType"></param>
        /// <param name="isIncludeDistributor"></param>
        /// <returns></returns>
        private async Task SendInvoiceNotificationEmail(string email, Invoices invoiceEntity, string emailType, bool isIncludeDistributor = true)
        {

            var emailBodyType = await Task.Run(() => _mySQLContext.EmailBodyType.Select(e => e));
            string emailSubject = string.Empty;
            string saEmailSubject = string.Empty;
            string saEmailContent = string.Empty;
            string saAction = string.Empty;
            var saEmailBodyType = emailBodyType.Where(e => e.EmailBodyTypeName.Contains("SuperAdmin Invoice Notification"));
            var territory = (from c in _mySQLContext.RefCountries
                             join t in _mySQLContext.RefTerritory on c.TerritoryId equals t.TerritoryId
                             join o in _mySQLContext.MainOrganization on c.CountriesCode equals o.RegisteredCountryCode
                             where o.OrgId == invoiceEntity.OrgId
                             select t.TerritoryName
                             ).FirstOrDefault();
            if (emailType == AutodeskConst.EmailType.RejectedDiscount)
            {
                emailBodyType = emailBodyType.Where(e => e.EmailBodyTypeName.Contains("Rejected Invoice"));
                emailSubject = string.Format(AppConfig.Config["EmailSender:DistributorInvoiceEmailSubject"], AutodeskConst.DiscountAction.Rejected.ToLower(), invoiceEntity.InvoiceNumber);
                //change due to customer feedback 28-1-2019 
                //using email subject from appsetting 
                saEmailSubject = string.Format(AppConfig.Config["EmailSender:SAInvoiceEmailSubject"], invoiceEntity.InvoiceNumber, territory);
                saAction = "Rejected";
            }
            else if (emailType == AutodeskConst.EmailType.ApprovedDiscount)
            {
                emailBodyType = emailBodyType.Where(e => e.EmailBodyTypeName.Contains("Distributor Notification"));
                emailSubject = string.Format(AppConfig.Config["EmailSender:DistributorInvoiceEmailSubject"], AutodeskConst.DiscountAction.Approved.ToLower(), invoiceEntity.InvoiceNumber);
                //change due to customer feedback 28-1-2019 
                //using email subject from appsetting 
                saEmailSubject = string.Format(AppConfig.Config["EmailSender:SAInvoiceEmailSubject"], invoiceEntity.InvoiceNumber, territory);
                saAction = "Created";
            }
            if (emailBodyType != null && saEmailBodyType != null && emailBodyType.FirstOrDefault() != null)
            {
                var entity = emailBodyType.FirstOrDefault();
                var saEntity = saEmailBodyType.FirstOrDefault();
                var emailBody = _mySQLContext.EmailBody.FirstOrDefault(b => b.BodyType == entity.EmailBodyTypeId.ToString());
                var saEmailBody = _mySQLContext.EmailBody.FirstOrDefault(b => b.BodyType == saEntity.EmailBodyTypeId.ToString());
                if (emailBody != null && saEmailBody != null)
                {


                    var bodyContent = emailBody.BodyEmail;
                    var saBodyContent = saEmailBody.BodyEmail;

                    //add checking null email
                    if (!string.IsNullOrEmpty(email))
                    {
                        var contact = await Task.Run(() => _mySQLContext.ContactsAll.SingleOrDefault(c => c.EmailAddress == email || c.EmailAddress2 == email || c.EmailAddress3 == email || c.EmailAddress4 == email));

                        if (contact != null)
                        {
                            bodyContent = bodyContent.Replace("[ReceiverName]", contact.ContactName);
                        }

                    }
                    if (emailType == AutodeskConst.EmailType.RejectedDiscount)
                    {
                        bodyContent = bodyContent.Replace("[DateRejected]", invoiceEntity.DateLastAdmin.ToString());
                        saBodyContent = saBodyContent.Replace("[ActionDate]", invoiceEntity.DateLastAdmin.ToString());
                    }
                    else if (emailType == AutodeskConst.EmailType.ApprovedDiscount)
                    {

                        bodyContent = bodyContent.Replace("[DateApproved]", invoiceEntity.DateLastAdmin.ToString());
                        saBodyContent = saBodyContent.Replace("[ActionDate]", invoiceEntity.DateAdded.ToString());
                    }
                    saBodyContent = saBodyContent.Replace("[Action]", saAction);
                    //change due to customer feedback 28-1-2019 
                    //add OrgId  and OrgName and ContactName to bodyContent
                    bodyContent = bodyContent.Replace("[InvoiceNumber]", invoiceEntity.InvoiceNumber);
                    bodyContent = bodyContent.Replace("[OrgId]", invoiceEntity.OrgId);
                    bodyContent = bodyContent.Replace("[OrgName]", _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == invoiceEntity.OrgId)?.OrgName);

                    saBodyContent = saBodyContent.Replace("[InvoiceNumber]", invoiceEntity.InvoiceNumber);
                    saBodyContent = saBodyContent.Replace("[OrgId]", invoiceEntity.OrgId);
                    saBodyContent = saBodyContent.Replace("[OrgName]", _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == invoiceEntity.OrgId)?.OrgName);

                    //var userId = Request.Headers["UserId"];
                    var userId = HttpContext.User.GetUserId();
                    if (!string.IsNullOrEmpty(userId))
                    {

                        invoiceEntity.DateLastAdmin = DateTime.Now;
                        var saContact = _mySQLContext.ContactsAll.FirstOrDefault(u => u.ContactId == userId);
                        if (saContact != null)
                        {
                            //add lastAdminBy and [Approver]
                            invoiceEntity.LastAdminBy = saContact.ContactName;
                            bodyContent = bodyContent.Replace("[Approver]", saContact.ContactName);
                            saBodyContent = saBodyContent.Replace("[ContactName]", saContact.ContactName);
                        }

                    }
                    _mySQLContext.Invoices.Update(invoiceEntity);
                    _mySQLContext.SaveChanges();
                    string domainName = AppConfig.Config["EmailSender:ApproveDiscountHost"];

                    bodyContent = bodyContent.Replace("[InvoiceUrl]", string.Format("<a href='" + AppConfig.Config["EmailSender:ApproveDiscountUrl"] + "' target='_blank' >here</a>", domainName, invoiceEntity.InvoiceId));
                    saBodyContent = saBodyContent.Replace("[InvoiceUrl]", string.Format("<a href='" + AppConfig.Config["EmailSender:ApproveDiscountUrl"] + "' target='_blank' >here</a>", domainName, invoiceEntity.InvoiceId));

                    // Fix auto send approval request when apply discount or reject discount
                    if (isIncludeDistributor == false)
                    {
                        var toSAEmails = await _emailSender.SendMailAsync(AppConfig.Config["EmailSender:ApproveDiscountReceiverEmail"], saEmailSubject, saBodyContent);
                    }

                    //add check isIncludeDistributor
                    if (isIncludeDistributor == true)
                    {
                        var emailRes = await _emailSender.SendMailAsync(email, emailSubject, bodyContent);
                    }

                }

            }
        }

        [HttpGet("GetOrganizationName")]
        public async Task<IActionResult> GetOrganizationName(string orgId)
        {
            var query = (from o in _mySQLContext.MainOrganization
                         join s in _mySQLContext.MainSite on o.OrgId equals s.OrgId
                         join sr in _mySQLContext.SiteRoles on s.SiteId equals sr.SiteId
                         join r in _mySQLContext.Roles on sr.RoleId equals r.RoleId
                         where o.OrgId == orgId
                         select new
                         {
                             o.OrgName,
                             o.EnglishOrgName,
                             o.PrimarySiteId,
                             PartnerType = r.RoleCode,

                         }
                         ).ToList();
            var res = query.FirstOrDefault();
            var listPartner = string.Join(',', query.Select(p => p.PartnerType));
            var result = await Task.Run(() => _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == orgId));
            if (res != null)
            {
                return Ok(new { res.EnglishOrgName, res.OrgName, PartnerType = listPartner });
            }
            return Ok();
        }

        [HttpPost("InvoiceReporting0")]
        public async Task<IActionResult> ReportInvoice([FromBody] dynamic data)
        {
            try
            {
                if (data != null)
                {
                    var jsonData = data.ToString();
                    InvoiceReportInputDto dto = JsonConvert.DeserializeObject<InvoiceReportInputDto>(jsonData);

                    var _role = !string.IsNullOrEmpty(dto.Role) ? dto.Role : string.Empty;
                    var orgId = !string.IsNullOrEmpty(dto.OrgId) ? dto.OrgId : string.Empty;
                    var filterByInvoicesNumber = dto.filterByInvoicesNumber;

                    var filterByPartnerType = !string.IsNullOrEmpty(dto.filterByPartnerType) ? dto.filterByPartnerType.Replace("'", string.Empty).Split(',').ToList() : new List<string>();
                    var _filterByPartnerType = new List<int>();
                    foreach (var item in filterByPartnerType)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            _filterByPartnerType.Add(int.Parse(item));
                        }
                    }
                    var regex = new Regex("\\w+");
                    var input = !string.IsNullOrEmpty(dto.filterByPartnerTypeStatus) ? dto.filterByPartnerTypeStatus : string.Empty;
                    var match = regex.Match(input);

                    var filterByPartnerTypeStatus = !string.IsNullOrEmpty(dto.filterByPartnerTypeStatus) ? dto.filterByPartnerTypeStatus.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

                    //var filterByPartnerTypeStatus = new List<string>();


                    var filterByMarketType = !string.IsNullOrEmpty(dto.filterByMarketType) ? dto.filterByMarketType.Replace("'", string.Empty).Split(',').ToList() : new List<string>();
                    var _filterByMarketType = new List<int>();
                    foreach (var item in filterByMarketType)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            _filterByMarketType.Add(int.Parse(item));
                        }
                    }
                    var filterByGeo = !string.IsNullOrEmpty(dto.filterByGeo) ? dto.filterByGeo.Replace("'", string.Empty).Split(',').ToList() : new List<string>();
                    var filterByRegion = !string.IsNullOrEmpty(dto.filterByRegion) ? dto.filterByRegion.Replace("'", string.Empty).Split(',').ToList() : new List<string>();
                    var filterBySubRegion = !string.IsNullOrEmpty(dto.filterBySubRegion) ? dto.filterBySubRegion.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

                    var filterByTerritory = !string.IsNullOrEmpty(dto.filterByTerritory) ? dto.filterByTerritory.Replace("'", string.Empty).Split(',').ToList() : new List<string>();
                    var _filterByTerritory = new List<int>();
                    foreach (var item in filterByTerritory)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            _filterByTerritory.Add(int.Parse(item));
                        }
                    }

                    var filterByCountries = !string.IsNullOrEmpty(dto.filterByCountries) ? dto.filterByCountries.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

                    //var FinancialYear = !string.IsNullOrEmpty(dto.FinancialYear) ? dto.FinancialYear : string.Empty;
                    var filterByFicancialYear = !string.IsNullOrEmpty(dto.FinancialYear) ? dto.FinancialYear.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

                    var _invoiceStatus = !string.IsNullOrEmpty(dto.filterByInvoiceStatus) ? dto.filterByInvoiceStatus.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

                    var fromDate = dto.filterFromDate.HasValue ? dto.filterFromDate.Value : (DateTime?)null;
                    var toDate = dto.filterToDate.HasValue ? dto.filterToDate.Value : (DateTime?)null;
                    var ORGReport = dto.ORGReport;
                    IOrderedQueryable<ReportDto> _query;
                    // For Site info Sheet
                    if (ORGReport == "SKU")
                    {
                        _query = await Task.Run(() => (from
                                                           org in _mySQLContext.MainOrganization
                                                       join invoice in _mySQLContext.Invoices on org.OrgId equals invoice.OrgId
                                                       join site in _mySQLContext.MainSite on org.OrgId equals site.OrgId
                                                       join country in _mySQLContext.RefCountries on org.RegisteredCountryCode equals country.CountriesCode
                                                       join territory in _mySQLContext.RefTerritory on country.TerritoryId equals territory.TerritoryId
                                                       join countrySite in _mySQLContext.RefCountries on site.SiteCountryCode equals countrySite.CountriesCode
                                                       join territorySite in _mySQLContext.RefTerritory on countrySite.TerritoryId equals territorySite.TerritoryId
                                                       join invoiceSKU in _mySQLContext.InvoiceSku on invoice.InvoiceId equals invoiceSKU.InvoiceId
                                                       join pricingSKU in _mySQLContext.InvoicePricingSKU on invoiceSKU.PricingSKUId equals pricingSKU.Id
                                                       join masterSKU in _mySQLContext.SiteCountryDistributorSKU on pricingSKU.SiteCountryDistributorSKUId equals masterSKU.SiteCountryDistributorSKUId
                                                       join invoiceDiscount in _mySQLContext.InvoiceDiscount on invoice.InvoiceId equals invoiceDiscount.InvoiceId into discount_invoice
                                                       from dis in discount_invoice.DefaultIfEmpty()
                                                       join marketType in _mySQLContext.MarketType on country.MarketTypeId equals marketType.MarketTypeId
                                                       join geo in _mySQLContext.RefGeo on country.GeoCode equals geo.GeoCode
                                                       join region in _mySQLContext.RefRegion on country.RegionCode equals region.RegionCode
                                                       join subRegion in _mySQLContext.RefSubregion on country.SubregionCode equals subRegion.SubregionCode
                                                       join siteRole in _mySQLContext.SiteRoles on site.SiteId equals siteRole.SiteId
                                                       join r in _mySQLContext.Roles on siteRole.RoleId equals r.RoleId
                                                       where
                                                       org.Status == AutodeskConst.StatusConts.A &&
                                                       site.SiteId == invoiceSKU.SiteId && siteRole.Status != AutodeskConst.StatusConts.T && siteRole.Status != AutodeskConst.StatusConts.X &&
                                                       _filterByPartnerType.Count > 0 && _filterByPartnerType.Contains(r.RoleId)
                                                       && filterByPartnerTypeStatus.Count > 0 && filterByPartnerTypeStatus.Contains(siteRole.Status)
                                                        && (!string.IsNullOrEmpty(filterByInvoicesNumber) ? invoice.InvoiceNumber.Contains(filterByInvoicesNumber) : true)
                                                        && (filterByCountries.Count > 0 ? filterByCountries.Contains(country.CountriesCode) : true)

                                         && (filterByGeo.Count > 0 ? filterByGeo.Contains(country.GeoCode) : true)
                                         && (filterByRegion.Count > 0 ? filterByRegion.Contains(country.RegionCode) : true)
                                         && (filterBySubRegion.Count > 0 ? filterBySubRegion.Contains(country.SubregionCode) : true)
                                         && (_filterByTerritory.Count > 0 ? _filterByTerritory.Contains(territory.TerritoryId) : true)
                                         && (_filterByMarketType.Count > 0 ? _filterByMarketType.Contains(marketType.MarketTypeId) : true)
                                         //&& invoice.FinancialYear.Contains(FinancialYear)
                                         && (filterByFicancialYear.Count > 0 ? filterByFicancialYear.Contains(invoice.FinancialYear) : true)
                                         //&& (_invoiceStatus != null ? _invoiceStatus == invoice.Status : true)
                                         && (_invoiceStatus.Count > 0 ? _invoiceStatus.Contains(invoice.Status) : true)
                                                       //&& (!string.IsNullOrEmpty(_invoiceStatus) ? FilterByInvoiceStatus(invoice.Status,invoice.InvoiceDate,invoice.DateAdded, invoice.Comments == "Rejected" ? invoice.DateLastAdmin : null, _invoiceStatus, fromDate, toDate) : true)
                                                       //&&(_invoiceStatus!=null && _invoiceStatus == AutodeskConst.StatusConts.X && (fromDate.HasValue || toDate.HasValue)?
                                                       //((fromDate.HasValue && toDate.HasValue)? (invoice.Status == _invoiceStatus && invoice.InvoiceDate>= fromDate && invoice.InvoiceDate <= toDate): (fromDate.HasValue && !toDate.HasValue)? (invoice.Status == _invoiceStatus && invoice.InvoiceDate >= fromDate): (!fromDate.HasValue && toDate.HasValue)? (invoice.Status == _invoiceStatus && invoice.InvoiceDate <= toDate):true):true)
                                                       let _totalAmount = invoiceSKU.TotalAmmount.HasValue ? invoiceSKU.TotalAmmount.Value : 0
                                                       select new ReportDto
                                                       {
                                                           OrgId = org.OrgId,
                                                           OrgIdInt = org.OrganizationId,
                                                           OrgName = org.OrgName,
                                                           SiteId = site.SiteId,
                                                           SiteIdInt = site.SiteIdInt,
                                                           SiteName = site.SiteName,
                                                           SiteStatus = siteRole.Status,
                                                           InvoiceId = invoice.InvoiceId,
                                                           InvoiceSKUId = invoiceSKU.InvoiceSkuid,
                                                           InvoiceNumber = invoice.InvoiceNumber,
                                                           InvoiceDescription = invoice.InvoiceDescription,
                                                           InvoiceStatus = invoice.Status,
                                                           CountriesName = country.CountriesName,
                                                           //QTY = _mySQLContext.InvoiceSku.Where(i => i.InvoiceId == invoice.InvoiceId).Sum(a => a.QTY).Value,
                                                           //TotalAmmount = _mySQLContext.InvoiceSku.Where(i => i.InvoiceId == invoice.InvoiceId).Sum(a => a.TotalAmmount).Value /*invoiceSKU.TotalAmmount.HasValue ? invoiceSKU.TotalAmmount.Value : 0*/,
                                                           QTY = invoiceSKU.Qty.HasValue ? invoiceSKU.Qty.Value : 0,
                                                           TotalAmmount = _totalAmount,
                                                           SKUName = masterSKU.SKUName,
                                                           SKUDescription = masterSKU.Skudescription,
                                                           PriceBand = pricingSKU.Price,
                                                           Discount = dis != null ? dis.Amount : 0,
                                                           DiscountType = dis != null ? dis.Name : string.Empty,
                                                           PartnerType = /*masterSKU.PartnerType,*/ r.RoleCode,
                                                           //FinancialYear = masterSKU.FYIndicatorKey,
                                                           TerritoryId = country.TerritoryId.HasValue ? country.TerritoryId.Value : 0,
                                                           TerritoryName = territory.TerritoryName,
                                                           CountriesCode = country.CountriesCode,
                                                           //RoleId = siteRole.RoleId.HasValue ? siteRole.RoleId.Value : 0,
                                                           MarketTypeId = marketType.MarketTypeId,
                                                           GeoCode = country.GeoCode,
                                                           RegionCode = country.RegionCode,
                                                           SubregionCode = country.SubregionCode,
                                                           FYIndicator = invoice.FinancialYear,
                                                           LastAdminBy = invoice.LastAdminBy,
                                                           PaymentReceivedDate = invoice.InvoiceDate,
                                                           Description = invoice.InvoiceDescription,
                                                           Approver = invoice.Comments == "Approved" ? invoice.LastAdminBy : string.Empty,
                                                           AmountType = dis != null ? dis.AmountType : AutodeskConst.AmountType.Exactly,
                                                           InvoiceCreatedDate = invoice.DateAdded,
                                                           InvoiceRejectedDate = invoice.Comments == "Rejected" ? invoice.DateLastAdmin : null,
                                                           TotalDiscountAmmount = dis != null && dis.AmountType == AutodeskConst.AmountType.Exactly ? dis.Amount : dis != null && dis.AmountType == AutodeskConst.AmountType.Percentage ? (_totalAmount * (dis.Amount / 100)) : 0,
                                                           NumberOfLicenses = string.IsNullOrEmpty(masterSKU.License) ? 0 : long.Parse(masterSKU.License),
                                                           InvoiceCreatedBy = invoice.AddedBy,
                                                           InvoiceApproverDate = invoice.Comments == "Approved" ? invoice.DateLastAdmin : null,
                                                           InvoiceRejectedBy = invoice.Comments == "Rejected" ? invoice.LastAdminBy : string.Empty,
                                                           SiteTerritory = territorySite.TerritoryName,
                                                           InvoiceSKUFYIndicatorKey = masterSKU.FYIndicatorKey,
                                                           InvoiceLicense = masterSKU.License,
                                                           InvoiceProrateYN = dis.Name == AutodeskConst.DiscountName.ProRata ? "Y" : "N",
                                                           RejectReason = invoice.RejectReason,
                                                           Comments = invoice.Comments,
                                                           OrgStatus = org.Status
                                                       }

                                      )
                                      //          .Where(q =>

                                      //          (_filterByPartnerType.Count > 0 && _filterByPartnerType.Contains(q.RoleId))
                                      //                                && (!string.IsNullOrEmpty(filterByInvoicesNumber) ? q.InvoiceNumber.Contains(filterByInvoicesNumber) : true)
                                      //                                && (filterByCountries.Count > 0 ? filterByCountries.Contains(q.CountriesCode) : true)

                                      //                 && (filterByGeo.Count > 0 ? filterByGeo.Contains(q.GeoCode) : true)
                                      //                 && (filterByRegion.Count > 0 ? filterByRegion.Contains(q.RegionCode) : true)
                                      //                 && (filterBySubRegion.Count > 0 ? filterBySubRegion.Contains(q.SubregionCode) : true)
                                      //                 && (_filterByTerritory.Count > 0 /*&& q.TerritoryId.HasValue */? _filterByTerritory.Contains(q.TerritoryId) : true)
                                      //                 && (filterByMarketType.Count > 0 ? filterByMarketType.Contains(q.MarketTypeId.ToString()) : true)
                                      //                 && q.FYIndicatorKey.Contains(FinancialYear)
                                      //                 && (!string.IsNullOrEmpty(_invoiceStatus) ? FilterByInvoiceStatus(q, _invoiceStatus, fromDate, toDate) : true)

                                      //)
                                      .OrderBy(o => o.OrgId)
                                      //.DistinctBy(x => x.InvoiceSKUId)
                                      );
                    }
                    else
                    {
                        _query = await Task.Run(() => (from
                                                           org in _mySQLContext.MainOrganization
                                                       join invoice in _mySQLContext.Invoices on org.OrgId equals invoice.OrgId into invoice_l
                                                       from invoice in invoice_l.DefaultIfEmpty()
                                                       join site in _mySQLContext.MainSite on org.OrgId equals site.OrgId into site_l
                                                       from site in site_l.DefaultIfEmpty()
                                                       join country in _mySQLContext.RefCountries on org.RegisteredCountryCode equals country.CountriesCode into country_l
                                                       from country in country_l.DefaultIfEmpty()
                                                       join territory in _mySQLContext.RefTerritory on country.TerritoryId equals territory.TerritoryId into territory_l
                                                       from territory in territory_l.DefaultIfEmpty()
                                                       join countrySite in _mySQLContext.RefCountries on site.SiteCountryCode equals countrySite.CountriesCode into country_site
                                                       from countrySite in country_site.DefaultIfEmpty()
                                                       join territorySite in _mySQLContext.RefTerritory on countrySite.TerritoryId equals territorySite.TerritoryId into territory_site
                                                       from territorySite in territory_site.DefaultIfEmpty()
                                                       join invoiceSKU in _mySQLContext.InvoiceSku on invoice.InvoiceId equals invoiceSKU.InvoiceId into invoice_SKU
                                                       from invoiceSKU in invoice_SKU.DefaultIfEmpty()
                                                       join pricingSKU in _mySQLContext.InvoicePricingSKU on invoiceSKU.PricingSKUId equals pricingSKU.Id into pricing_SKU
                                                       from pricingSKU in pricing_SKU.DefaultIfEmpty()
                                                       join masterSKU in _mySQLContext.SiteCountryDistributorSKU on pricingSKU.SiteCountryDistributorSKUId equals masterSKU.SiteCountryDistributorSKUId into master_SKU
                                                       from masterSKU in master_SKU.DefaultIfEmpty()
                                                       join invoiceDiscount in _mySQLContext.InvoiceDiscount on invoice.InvoiceId equals invoiceDiscount.InvoiceId into discount_invoice
                                                       from dis in discount_invoice.DefaultIfEmpty()
                                                       join marketType in _mySQLContext.MarketType on country.MarketTypeId equals marketType.MarketTypeId into market_type
                                                       from marketType in market_type.DefaultIfEmpty()
                                                       join geo in _mySQLContext.RefGeo on country.GeoCode equals geo.GeoCode into geo_l
                                                       from geo in geo_l.DefaultIfEmpty()
                                                       join region in _mySQLContext.RefRegion on country.RegionCode equals region.RegionCode into region_l
                                                       from region in region_l.DefaultIfEmpty()
                                                       join subRegion in _mySQLContext.RefSubregion on country.SubregionCode equals subRegion.SubregionCode into sub_region
                                                       from subRegion in sub_region.DefaultIfEmpty()
                                                       join siteRole in _mySQLContext.SiteRoles on site.SiteId equals siteRole.SiteId into site_role
                                                       from siteRole in site_role.DefaultIfEmpty()
                                                       join r in _mySQLContext.Roles on siteRole.RoleId equals r.RoleId into r_l
                                                       from r in r_l.DefaultIfEmpty()
                                                       where
                                                           //site.SiteId == invoiceSKU.SiteId && 
                                                           //(org.Status == AutodeskConst.StatusConts.A || org.Status == AutodeskConst.StatusConts.S) && site.Status != AutodeskConst.StatusConts.X && site.Status != AutodeskConst.StatusConts.T && site.Status != AutodeskConst.StatusConts.D 
                                                           //&&
                                                           //siteRole.Status != AutodeskConst.StatusConts.T && siteRole.Status != AutodeskConst.StatusConts.X && siteRole.Status != AutodeskConst.StatusConts.D && siteRole.Status != AutodeskConst.StatusConts.S 
                                                           //&&
                                                           _filterByPartnerType.Count > 0 && _filterByPartnerType.Contains(r.RoleId)
                                                           && filterByPartnerTypeStatus.Count > 0 && filterByPartnerTypeStatus.Contains(org.Status)
                                                            //&& (!string.IsNullOrEmpty(filterByInvoicesNumber) ? invoice.InvoiceNumber.Contains(filterByInvoicesNumber) : true)
                                                            && (filterByCountries.Count > 0 ? filterByCountries.Contains(country.CountriesCode) : true)
                                                            && (filterByGeo.Count > 0 ? filterByGeo.Contains(country.GeoCode) : true)
                                                            && (filterByRegion.Count > 0 ? filterByRegion.Contains(country.RegionCode) : true)
                                                            && (filterBySubRegion.Count > 0 ? filterBySubRegion.Contains(country.SubregionCode) : true)
                                                            && (_filterByTerritory.Count > 0 ? _filterByTerritory.Contains(territory.TerritoryId) : true)
                                                            && (_filterByMarketType.Count > 0 ? _filterByMarketType.Contains(marketType.MarketTypeId) : true)
                                                       let _totalAmount = invoiceSKU.TotalAmmount.HasValue ? invoiceSKU.TotalAmmount.Value : 0
                                                       select new ReportDto
                                                       {
                                                           InvoiceSKUId = invoiceSKU.InvoiceSkuid,
                                                           OrgId = org.OrgId,
                                                           OrgIdInt = org.OrganizationId,
                                                           OrgName = org.OrgName,
                                                           SiteId = site.SiteId,
                                                           SiteName = site.SiteName,
                                                           SiteStatus = siteRole.Status,
                                                           InvoiceDescription = invoice.InvoiceDescription,
                                                           InvoiceStatus = invoice.Status,
                                                           CountriesName = country.CountriesName,
                                                           QTY = invoiceSKU.Qty.HasValue ? invoiceSKU.Qty.Value : 0,
                                                           TotalAmmount = _totalAmount,
                                                           SKUName = masterSKU.SKUName,
                                                           SKUDescription = masterSKU.Skudescription,
                                                           PriceBand = pricingSKU.Price,
                                                           PartnerType = masterSKU.PartnerType, //,r.RoleCode
                                                           SitePartnerType = r.RoleCode,
                                                           //FinancialYear = masterSKU.FYIndicatorKey,
                                                           TerritoryName = territory.TerritoryName,
                                                           FYIndicator = invoice.FinancialYear,
                                                           LastAdminBy = invoice.LastAdminBy,
                                                           PaymentReceivedDate = invoice.InvoiceDate,
                                                           Description = invoice.InvoiceDescription,
                                                           Approver = invoice.Comments == "Approved" ? invoice.LastAdminBy : string.Empty,
                                                           AmountType = dis != null ? dis.AmountType : AutodeskConst.AmountType.Exactly,
                                                           InvoiceCreatedDate = invoice.DateAdded,
                                                           InvoiceRejectedDate = invoice.Comments == "Rejected" ? invoice.DateLastAdmin : null,
                                                           TotalDiscountAmmount = dis != null && dis.AmountType == AutodeskConst.AmountType.Exactly ? dis.Amount : dis != null && dis.AmountType == AutodeskConst.AmountType.Percentage ? (_totalAmount * (dis.Amount / 100)) : 0,
                                                           NumberOfLicenses = string.IsNullOrEmpty(masterSKU.License) ? 0 : long.Parse(masterSKU.License),
                                                           InvoiceCreatedBy = invoice.AddedBy,
                                                           InvoiceApproverDate = invoice.Comments == "Approved" ? invoice.DateLastAdmin : null,
                                                           InvoiceRejectedBy = invoice.Comments == "Rejected" ? invoice.LastAdminBy : string.Empty,
                                                           SiteTerritory = territorySite.TerritoryName,
                                                           //InvoiceSKUFYIndicatorKey = masterSKU.FYIndicatorKey,
                                                           InvoiceLicense = masterSKU.License,
                                                           InvoiceProrateYN = dis.Name == AutodeskConst.DiscountName.ProRata ? "Y" : "N",
                                                           RejectReason = invoice.RejectReason,
                                                           Comments = invoice.Comments,
                                                           OrgStatus = org.Status
                                                       }

                                      )
                                      .OrderBy(o => o.OrgId)
                                      );
                    }

                    //await _query.ForEachAsync(x => { x.TotalAmmount = Math.Round(x.TotalAmmount, 2); });

                    string _res = string.Empty;
                    using (var command = _mySQLContext.Database.GetDbConnection().CreateCommand())
                    {
                        command.CommandText = _query.ToSql();
                        try
                        {
                            _mySQLContext.Database.OpenConnection();
                            using (var reader = command.ExecuteReader())
                            {
                                _res = await Task.Run(() => reader.ToJsonString());

                            }
                        }
                        catch (Exception ex)
                        {

                        }
                    }

                    var lstTemp = JsonConvert.DeserializeObject<List<ReportDto>>(_res);
                    var siteOrgIdWithSKU = lstTemp.Where(x => x.InvoiceSKUId.HasValue).Select(x => x.OrgId).Distinct().ToList();
                    // Remove site without invoice in partnertype
                    var reports = lstTemp.Where(x => siteOrgIdWithSKU.Contains(x.OrgId) && !string.IsNullOrEmpty(x.PartnerType)).ToList();
                    var lstSiteWithoutSKU = lstTemp.Where(x => !siteOrgIdWithSKU.Contains(x.OrgId)).ToList();

                    // Remove duplidate data
                    lstSiteWithoutSKU = lstSiteWithoutSKU.GroupBy(x => new { x.SiteId, x.SiteStatus, x.SitePartnerType, x.TerritoryName, x.SiteTerritory, x.QTY, x.InvoiceLicense }).Select(x => x.First()).ToList();
                    reports = reports.GroupBy(x => new { x.SiteId, x.SiteStatus, x.PartnerType, x.TerritoryName, x.SiteTerritory, x.QTY, x.InvoiceLicense }).Select(x => x.First()).ToList();

                    reports.AddRange(lstSiteWithoutSKU);
                    reports = reports.OrderBy(o => o.OrgId).ToList();

                    string _resOrg = string.Empty;
                    var orgIds = reports.Select(x => x.OrgId).Distinct().ToList();
                    if (orgIds != null && orgIds.Count > 0)
                    {
                        // Get data for OrgInfo sheet
                        List<string> whereOrgIds = new List<string>();
                        List<string> whereStatus = new List<string>();
                        foreach (var item in orgIds)
                        {
                            whereOrgIds.Add("'" + item + "'");
                        }
                        foreach (var item in _invoiceStatus)
                        {
                            whereStatus.Add("'" + item + "'");
                        }
                        string queryOrg = "";
                        string andWhereStatus = whereStatus.Count > 0 ? " AND Invoices.Status IN (" + whereStatus.Join() + " ) " : " ";
                        string suspendedStatus = " AND Main_organization.Status IN ('S') ";
                        //For Org info Sheet
                        if (ORGReport == "SKU")
                        {
                            queryOrg = "select Main_organization.OrgId, Main_organization.Status AS OrgStatus, Main_organization.OrgName,InvoiceDiscount.AmountType, InvoiceDiscount.Amount as Discount, Ref_territory.Territory_Name, " +
                                "InvoiceNumber, Ref_countries.countries_name, Invoices.FinancialYear as FYIndicatorKey, Invoices.RejectReason, " +
                                 "DATE_FORMAT(InvoiceDate, '%Y-%m-%d') as PaymentReceiveDate, Invoices.DateAdded as InvoiceCreatedDate, " +
                                "(CASE " +
                                "when Invoices.Comments = 'Approved' then " +
                                "(case when Invoices.Status = 'A' then 'Approved' when  Invoices.Status = 'C' then 'Cancelled' else 'Paid' end) " +
                                "when Invoices.Comments = 'Rejected' then " +
                                "(case when Invoices.Status = 'A' then 'Rejected' when  Invoices.Status = 'C' then 'Cancelled' else   'Paid' end) " +
                                "else ( case when Invoices.InvoiceId IS NULL then '' else case when Invoices.Status = 'A' then 'Pending' when Invoices.Status = 'C' then 'Cancelled' else   'Paid' end end) " +
                                "END) as InvoiceStatus," +
                                //"sum(InvoiceSKU.QTY) as QTY," +
                                "(CASE " +
                                " when InvoiceDiscount.AmountType = 'Percentage' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - (sum((cast(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100))" +
                                " when InvoiceDiscount.AmountType = 'Exactly' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - InvoiceDiscount.Amount" +
                                " else sum(cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY) " +
                                "END) as TotalInvoiceAmmount, " +
                                " sum(InvoiceSKU.QTY * cast(SiteCountryDistributorSKU.License as UNSIGNED)) as NumberOfLicenses," +
                                "Contacts_All.ContactName as InvoiceCreatedBy," +
                                "Contacts_All.ContactIdInt," +
                                "group_concat(concat(InvoiceSKU.SiteId, '(',SKUName, '*',QTY,')') separator '\r\n,') as SKUDescription," +
                                "(CASE " +
                                "when Invoices.Comments = 'Approved' then Invoices.LastAdminBy " +
                                "END) as ApprovedBy," +
                                "(CASE " +
                                "when Invoices.Comments = 'Rejected' then Invoices.LastAdminBy " +
                                "END) as RejectedBy, " +
                                " Invoices.Comments, Invoices.DateLastAdmin " +
                                " from Invoices join InvoiceSKU on Invoices.InvoiceId = InvoiceSKU.InvoiceId" +
                                " join Main_organization on Invoices.OrgId = Main_organization.OrgId" +
                                " join InvoicePricingSKU on InvoiceSKU.PricingSKUId = InvoicePricingSKU.Id" +
                                " join SiteCountryDistributorSKU on InvoicePricingSKU.SiteCountryDistributorSKUId = SiteCountryDistributorSKU.SiteCountryDistributorSKUId" +
                                " left join InvoiceDiscount on Invoices.InvoiceId = InvoiceDiscount.InvoiceId" +
                                " left join Contacts_All on Invoices.AddedBy = Contacts_All.ContactId " +
                                " left join Ref_countries on Main_organization.RegisteredCountryCode = Ref_countries.countries_code " +
                                " left join Ref_territory on Ref_countries.TerritoryId = Ref_territory.TerritoryId " +
                                " join MarketType on Ref_countries.MarketTypeId = MarketType.MarketTypeId" +

                                " where Main_organization.OrgId IN (" + whereOrgIds.Join() + " ) " +
                                andWhereStatus +
                                " group by Invoices.InvoiceId  ORDER BY Main_organization.OrgId;";
                        }
                        else if (ORGReport == "SUSPENDED")
                        {
                            queryOrg = "select Main_organization.OrgId, Main_organization.Status AS OrgStatus, Main_organization.OrgName,InvoiceDiscount.AmountType, InvoiceDiscount.Amount as Discount, Ref_territory.Territory_Name, " +
                                  "InvoiceNumber, Ref_countries.countries_name, Invoices.FinancialYear as FYIndicatorKey, Invoices.RejectReason, " +
                                   "DATE_FORMAT(InvoiceDate, '%Y-%m-%d') as InvoiceDate, Invoices.DateAdded as InvoiceCreatedDate, " +
                                  "(CASE " +
                                  "when Invoices.Comments = 'Approved' then " +
                                  "(case when Invoices.Status = 'A' then 'Approved' when  Invoices.Status = 'C' then 'Cancelled' else 'Paid' end) " +
                                  "when Invoices.Comments = 'Rejected' then " +
                                  "(case when Invoices.Status = 'A' then 'Rejected' when  Invoices.Status = 'C' then 'Cancelled' else   'Paid' end) " +
                                  "else ( case when Invoices.InvoiceId IS NULL then '' else case when Invoices.Status = 'A' then 'Pending' when Invoices.Status = 'C' then 'Cancelled' else   'Paid' end end) " +
                                  "END) as InvoiceStatus," +
                                  //"sum(InvoiceSKU.QTY) as QTY," +
                                  "(CASE " +
                                  " when InvoiceDiscount.AmountType = 'Percentage' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - (sum((cast(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100))" +
                                  " when InvoiceDiscount.AmountType = 'Exactly' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - InvoiceDiscount.Amount" +
                                  " else sum(cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY) " +
                                  "END) as TotalInvoiceAmmount, " +
                                  " sum(InvoiceSKU.QTY * cast(SiteCountryDistributorSKU.License as UNSIGNED)) as NumberOfLicenses," +
                                  "Contacts_All.ContactName as InvoiceCreatedBy," +
                                  "Contacts_All.ContactIdInt," +
                                  "group_concat(concat(InvoiceSKU.SiteId, '(',SKUName, '*',QTY,')') separator '\r\n,') as SKUDescription," +
                                  "(CASE " +
                                  "when Invoices.Comments = 'Approved' then Invoices.LastAdminBy " +
                                  "END) as ApprovedBy," +
                                  "(CASE " +
                                  "when Invoices.Comments = 'Rejected' then Invoices.LastAdminBy " +
                                  "END) as RejectedBy, " +
                                  " Invoices.Comments, Invoices.DateLastAdmin " +
                                  " from Main_organization left join Invoices on Main_organization.OrgId = Invoices.OrgId  " +
                                  " left join InvoiceSKU on Invoices.InvoiceId = InvoiceSKU.InvoiceId" +
                                  " left join InvoicePricingSKU on InvoiceSKU.PricingSKUId = InvoicePricingSKU.Id" +
                                  " left join SiteCountryDistributorSKU on InvoicePricingSKU.SiteCountryDistributorSKUId = SiteCountryDistributorSKU.SiteCountryDistributorSKUId" +
                                  " left join InvoiceDiscount on Invoices.InvoiceId = InvoiceDiscount.InvoiceId" +
                                  " left join Contacts_All on Invoices.AddedBy = Contacts_All.ContactId " +
                                  " left join Ref_countries on Main_organization.RegisteredCountryCode = Ref_countries.countries_code " +
                                  " left join Ref_territory on Ref_countries.TerritoryId = Ref_territory.TerritoryId " +
                                  " left join MarketType on Ref_countries.MarketTypeId = MarketType.MarketTypeId" +
                                  " left join Main_site on Main_site.OrgId = Main_organization.OrgId " +
                                  " left join SiteRoles on Main_site.SiteId = SiteRoles.SiteId " +
                                  " where Main_organization.OrgId IN (" + whereOrgIds.Join() + " ) " + suspendedStatus +
                                  andWhereStatus +
                                  " group by Main_organization.OrgId, Invoices.InvoiceId  ORDER BY Main_organization.OrgId;";
                        }
                        else
                        {
                            //queryOrg = "select Main_organization.OrgId, Main_organization.Status AS OrgStatus, Main_organization.OrgName,InvoiceDiscount.AmountType, InvoiceDiscount.Amount as Discount, Ref_territory.Territory_Name, " +
                            //    "InvoiceNumber, Ref_countries.countries_name, Invoices.FinancialYear as FYIndicatorKey, Invoices.RejectReason, " +
                            //     "DATE_FORMAT(InvoiceDate, '%Y-%m-%d') as InvoiceDate, Invoices.DateAdded as InvoiceCreatedDate, " +
                            //    "(CASE " +
                            //    "when Invoices.Comments = 'Approved' then " +
                            //    "(case when Invoices.Status = 'A' then 'Approved' when  Invoices.Status = 'C' then 'Cancelled' else 'Paid' end) " +
                            //    "when Invoices.Comments = 'Rejected' then " +
                            //    "(case when Invoices.Status = 'A' then 'Rejected' when  Invoices.Status = 'C' then 'Cancelled' else   'Paid' end) " +
                            //    "else ( case when Invoices.InvoiceId IS NULL then '' else case when Invoices.Status = 'A' then 'Pending' when Invoices.Status = 'C' then 'Cancelled' else   'Paid' end end) " +
                            //    "END) as InvoiceStatus," +
                            //    //"sum(InvoiceSKU.QTY) as QTY," +
                            //    "(CASE " +
                            //    " when InvoiceDiscount.AmountType = 'Percentage' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - (sum((cast(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100))" +
                            //    " when InvoiceDiscount.AmountType = 'Exactly' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - InvoiceDiscount.Amount" +
                            //    " else sum(cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY) " +
                            //    "END) as TotalInvoiceAmmount, " +
                            //    " sum(InvoiceSKU.QTY * cast(SiteCountryDistributorSKU.License as UNSIGNED)) as NumberOfLicenses," +
                            //    "Contacts_All.ContactName as InvoiceCreatedBy," +
                            //    "Contacts_All.ContactIdInt," +
                            //    "group_concat(concat(InvoiceSKU.SiteId, '(',SKUName, '*',QTY,')') separator '\r\n,') as SKUDescription," +
                            //    "(CASE " +
                            //    "when Invoices.Comments = 'Approved' then Invoices.LastAdminBy " +
                            //    "END) as ApprovedBy," +
                            //    "(CASE " +
                            //    "when Invoices.Comments = 'Rejected' then Invoices.LastAdminBy " +
                            //    "END) as RejectedBy, " +
                            //    " Invoices.Comments, Invoices.DateLastAdmin, " +
                            //    " SiteRoles.Status AS SiteRoleStatus " +
                            //    " from Main_organization left join Invoices on Main_organization.OrgId = Invoices.OrgId  " +
                            //    " left join InvoiceSKU on Invoices.InvoiceId = InvoiceSKU.InvoiceId" +
                            //    " left join InvoicePricingSKU on InvoiceSKU.PricingSKUId = InvoicePricingSKU.Id" +
                            //    " left join SiteCountryDistributorSKU on InvoicePricingSKU.SiteCountryDistributorSKUId = SiteCountryDistributorSKU.SiteCountryDistributorSKUId" +
                            //    " left join InvoiceDiscount on Invoices.InvoiceId = InvoiceDiscount.InvoiceId" +
                            //    " left join Contacts_All on Invoices.AddedBy = Contacts_All.ContactId " +
                            //    " left join Ref_countries on Main_organization.RegisteredCountryCode = Ref_countries.countries_code " +
                            //    " left join Ref_territory on Ref_countries.TerritoryId = Ref_territory.TerritoryId " +
                            //    " left join MarketType on Ref_countries.MarketTypeId = MarketType.MarketTypeId" +
                            //    " left join Main_site on Main_site.OrgId = Main_organization.OrgId " +
                            //    " left join SiteRoles on Main_site.SiteId = SiteRoles.SiteId " +

                            //    " where Main_organization.OrgId IN (" + whereOrgIds.Join() + " ) AND Main_organization.Status <> 'S' " +
                            //    andWhereStatus +
                            //    " group by Main_organization.OrgId, Invoices.InvoiceId  ORDER BY Main_organization.OrgId;";
                            //    //"ORDER BY Main_organization.OrgId;";
                            //queryOrg = "select Main_organization.OrgId, Main_organization.Status AS OrgStatus, Main_organization.OrgName,InvoiceDiscount.AmountType, InvoiceDiscount.Amount as Discount, Ref_territory.Territory_Name, " +
                            //    "InvoiceNumber, Ref_countries.countries_name, Invoices.FinancialYear as FYIndicatorKey, Invoices.RejectReason, " +
                            //     "DATE_FORMAT(InvoiceDate, '%Y-%m-%d') as InvoiceDate, Invoices.DateAdded as InvoiceCreatedDate, " +
                            //    "(CASE " +
                            //    "when Invoices.Comments = 'Approved' then " +
                            //    "(case when Invoices.Status = 'A' then 'Approved' when  Invoices.Status = 'C' then 'Cancelled' else 'Paid' end) " +
                            //    "when Invoices.Comments = 'Rejected' then " +
                            //    "(case when Invoices.Status = 'A' then 'Rejected' when  Invoices.Status = 'C' then 'Cancelled' else   'Paid' end) " +
                            //    "else ( case when Invoices.InvoiceId IS NULL then '' else case when Invoices.Status = 'A' then 'Pending' when Invoices.Status = 'C' then 'Cancelled' else   'Paid' end end) " +
                            //    "END) as InvoiceStatus," +
                            //    //"sum(InvoiceSKU.QTY) as QTY," +
                            //    "(CASE " +
                            //    " when InvoiceDiscount.AmountType = 'Percentage' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - (sum((cast(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100))" +
                            //    " when InvoiceDiscount.AmountType = 'Exactly' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY)) - InvoiceDiscount.Amount" +
                            //    " else sum(cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY) " +
                            //    "END) as TotalInvoiceAmmount, " +
                            //    " sum(InvoiceSKU.QTY * cast(SiteCountryDistributorSKU.License as UNSIGNED)) as NumberOfLicenses," +
                            //    "Contacts_All.ContactName as InvoiceCreatedBy," +
                            //    "Contacts_All.ContactIdInt," +
                            //    "group_concat(concat(InvoiceSKU.SiteId, '(',SKUName, '*',QTY,')') separator '\r\n,') as SKUDescription," +
                            //    "(CASE " +
                            //    "when Invoices.Comments = 'Approved' then Invoices.LastAdminBy " +
                            //    "END) as ApprovedBy," +
                            //    "(CASE " +
                            //    "when Invoices.Comments = 'Rejected' then Invoices.LastAdminBy " +
                            //    "END) as RejectedBy, " +
                            //    " Invoices.Comments, Invoices.DateLastAdmin " +
                            //    " from Invoices join InvoiceSKU on Invoices.InvoiceId = InvoiceSKU.InvoiceId" +
                            //    " join Main_organization on Invoices.OrgId = Main_organization.OrgId" +
                            //    " join InvoicePricingSKU on InvoiceSKU.PricingSKUId = InvoicePricingSKU.Id" +
                            //    " join SiteCountryDistributorSKU on InvoicePricingSKU.SiteCountryDistributorSKUId = SiteCountryDistributorSKU.SiteCountryDistributorSKUId" +
                            //    " left join InvoiceDiscount on Invoices.InvoiceId = InvoiceDiscount.InvoiceId" +
                            //    " left join Contacts_All on Invoices.AddedBy = Contacts_All.ContactId " +
                            //    " left join Ref_countries on Main_organization.RegisteredCountryCode = Ref_countries.countries_code " +
                            //    " left join Ref_territory on Ref_countries.TerritoryId = Ref_territory.TerritoryId " +
                            //    " join MarketType on Ref_countries.MarketTypeId = MarketType.MarketTypeId" +

                            //    " where Main_organization.OrgId IN (" + whereOrgIds.Join() + " ) " +
                            //    andWhereStatus +
                            //    " group by Invoices.InvoiceId  ORDER BY Main_organization.OrgId;";
                            queryOrg = "select Main_organization.OrgId, Main_organization.Status AS OrgStatus, Main_organization.OrgName,  Ref_territory.Territory_Name,Ref_countries.countries_name,Invoices.* From Main_organization  left join( " +
                               " select Invoices.OrgId, InvoiceDiscount.AmountType, InvoiceDiscount.Amount as Discount, InvoiceNumber,   " +
                               " Invoices.FinancialYear as FYIndicatorKey, Invoices.RejectReason, DATE_FORMAT(InvoiceDate, '%Y-%m-%d') as PaymentReceiveDate,  " +
                               " Invoices.DateAdded as InvoiceCreatedDate, (CASE when Invoices.Comments = 'Approved' then(case when Invoices.Status = 'A' then 'Approved' when  Invoices.Status = 'C' then 'Cancelled' else 'Paid' end)  " +
                               " when Invoices.Comments = 'Rejected' then(case when Invoices.Status = 'A' then 'Rejected' when  Invoices.Status = 'C' then 'Cancelled' else   'Paid' end) else ( case when Invoices.InvoiceId IS NULL then '' else case when Invoices.Status = 'A' then 'Pending' when Invoices.Status = 'C' then 'Cancelled' else   'Paid' end end) END) as InvoiceStatus,  " +
                               "  (CASE  when InvoiceDiscount.AmountType = 'Percentage' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) *InvoiceSKU.QTY)) -(sum((cast(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100)) when InvoiceDiscount.AmountType = 'Exactly' then sum((cast(InvoicePricingSKU.Price as UNSIGNED) *InvoiceSKU.QTY)) -InvoiceDiscount.Amount else sum(cast(InvoicePricingSKU.Price as UNSIGNED) * InvoiceSKU.QTY) END) as TotalInvoiceAmmount,   " +
                               "  sum(InvoiceSKU.QTY * cast(SiteCountryDistributorSKU.License as UNSIGNED)) as NumberOfLicenses,Contacts_All.ContactName as InvoiceCreatedBy,  " +
                               " Contacts_All.ContactIdInt,group_concat(concat(InvoiceSKU.SiteId, '(', SKUName, '*', QTY, ')') separator ' , ') as SKUDescription " +
                               ",(CASE when Invoices.Comments = 'Approved' then Invoices.LastAdminBy END) as ApprovedBy,(CASE when Invoices.Comments = 'Rejected' then Invoices.LastAdminBy END) as RejectedBy,  " +

                               " Invoices.Comments, Invoices.DateLastAdmin, Invoices.Status, Invoices.InvoiceId  from    " +
                               " Invoices   join InvoiceSKU on Invoices.InvoiceId = InvoiceSKU.InvoiceId  " +
                               " join InvoicePricingSKU on InvoiceSKU.PricingSKUId = InvoicePricingSKU.Id  join SiteCountryDistributorSKU on InvoicePricingSKU.SiteCountryDistributorSKUId = SiteCountryDistributorSKU.SiteCountryDistributorSKUId  " +
                               " left  join InvoiceDiscount on Invoices.InvoiceId = InvoiceDiscount.InvoiceId  " +
                               "  left  join Contacts_All on Invoices.AddedBy = Contacts_All.ContactId group by Invoices.OrgId) Invoices on Main_organization.OrgId = Invoices.OrgId left join Ref_countries on Main_organization.RegisteredCountryCode = Ref_countries.countries_code  " +
                               "  left join Ref_territory on Ref_countries.TerritoryId = Ref_territory.TerritoryId left join MarketType on Ref_countries.MarketTypeId = MarketType.MarketTypeId  " +
                               " where Main_organization.OrgId is not null and Main_organization.OrgId IN (" + whereOrgIds.Join() + " ) " +
                               andWhereStatus +
                               " ORDER BY Main_organization.OrgId;";
                        }
                        using (var command = _mySQLContext.Database.GetDbConnection().CreateCommand())
                        {
                            command.CommandText = queryOrg;
                            try
                            {
                                _mySQLContext.Database.OpenConnection();
                                using (var reader = command.ExecuteReader())
                                {
                                    _resOrg = await Task.Run(() => reader.ToJsonString());
                                }
                            }
                            catch (Exception ex)
                            {

                                return Ok();
                            }
                        }
                    }

                    var reportsOrg = JsonConvert.DeserializeObject<List<ReportOrgDto>>(_resOrg);
                    if (reportsOrg == null)
                        reportsOrg = new List<ReportOrgDto>();
                    List<string> filterStatus = new List<string>();
                    foreach (var s in _invoiceStatus)
                    {
                        if (s == "X")
                            filterStatus.Add("Paid");
                        else if (s == "A")
                        {
                            filterStatus.Add("Approved");
                            filterStatus.Add("Rejected");
                            filterStatus.Add("Pending");
                        }
                        else if (s == "C")
                            filterStatus.Add("Cancelled");
                    }
                    string _resOrgWithoutSKU = string.Empty;
                    if (ORGReport == "ALL" || ORGReport == "SUSPENDED")
                    {
                        var orgWithSKU = reportsOrg.Select(x => x.OrgId).Distinct().ToList();
                        var orgWithoutSKU = orgIds.Except(orgWithSKU).ToList();
                        var queryWithoutSKU = await Task.Run(() => (from
                                                           org in _mySQLContext.MainOrganization
                                                                    join invoice in _mySQLContext.Invoices on org.OrgId equals invoice.OrgId into invoice_l
                                                                    from invoice in invoice_l.DefaultIfEmpty()
                                                                    join country in _mySQLContext.RefCountries on org.RegisteredCountryCode equals country.CountriesCode into country_l
                                                                    from country in country_l.DefaultIfEmpty()
                                                                    join territory in _mySQLContext.RefTerritory on country.TerritoryId equals territory.TerritoryId into territory_l
                                                                    from territory in territory_l.DefaultIfEmpty()
                                                                    join contact in _mySQLContext.ContactsAll on invoice.AddedBy equals contact.ContactId into contact_l
                                                                    from contact in contact_l.DefaultIfEmpty()
                                                                    where
                                                                    orgWithoutSKU.Count > 0 && orgWithoutSKU.Contains(org.OrgId)
                                                                    select new ReportOrgDto
                                                                    {
                                                                        OrgId = org.OrgId,
                                                                        OrgName = org.OrgName,
                                                                        Territory_Name = territory.TerritoryName,
                                                                        countries_name = country.CountriesName,
                                                                        //InvoiceStatus = invoice.Status,
                                                                        InvoiceStatus = invoice.Comments == "Approved" ? invoice.Status == "A" ? "Approved" : invoice.Status == "C" ? "Cancelled" : "Paid" : invoice.Comments == "Rejected" ? invoice.Status == "A" ? "Rejected" : invoice.Status == "C" ? "Cancelled" : "Paid" :
                                                                        invoice.InvoiceId == null ? "" : invoice.Status == "A" ? "Pending" : invoice.Status == "C" ? "Cancelled" : "Paid",
                                                                        FYIndicatorKey = invoice.FinancialYear,
                                                                        InvoiceCreatedDate = invoice.DateAdded,
                                                                        InvoiceCreatedBy = contact.ContactName,
                                                                        RejectReason = invoice.RejectReason,
                                                                        DateLastAdmin = invoice.DateLastAdmin,
                                                                        ApprovedBy = invoice.Comments == "Approved" ? invoice.LastAdminBy : "",
                                                                        RejectedBy = invoice.Comments == "Rejected" ? invoice.LastAdminBy : ""
                                                                    }

                                      )
                                      .OrderBy(o => o.OrgId)
                                      );

                        using (var command = _mySQLContext.Database.GetDbConnection().CreateCommand())
                        {
                            command.CommandText = queryWithoutSKU.ToSql();
                            try
                            {
                                _mySQLContext.Database.OpenConnection();
                                using (var reader = command.ExecuteReader())
                                {
                                    _resOrgWithoutSKU = await Task.Run(() => reader.ToJsonString());
                                }
                            }
                            catch (Exception ex)
                            {
                                return Ok();
                            }
                        }

                        var reportsOrgWithoutSKU = JsonConvert.DeserializeObject<List<ReportOrgDto>>(_resOrgWithoutSKU);
                        reportsOrg.AddRange(reportsOrgWithoutSKU);
                        reportsOrg = reportsOrg.OrderBy(x => x.OrgId).ToList();

                        if (filterByFicancialYear.Count > 0 || _invoiceStatus.Count > 0)
                        {
                            foreach (var item in reportsOrg)
                            {
                                if ((filterByFicancialYear.Count > 0 && !filterByFicancialYear.Contains(item.FYIndicatorKey)) || (filterStatus.Count > 0 && !filterStatus.Contains(item.InvoiceStatus)))
                                {
                                    item.TotalAmount = null;
                                    item.InvoiceNumber = null;
                                    item.InvoiceId = null;
                                    item.TotalInvoiceAmmount = null;
                                    item.Discount = null;
                                    item.DiscountType = null;
                                    item.AmountType = null;
                                    item.InvoiceStatus = null;
                                    item.FYIndicatorKey = null;
                                    item.License = null;
                                    item.NumberOfLicenses = null;
                                    item.ApprovedBy = null;
                                    item.RejectedBy = null;
                                    item.DateLastAdmin = null;
                                    item.RejectReason = null;
                                }
                            }

                            foreach (var item in reports)
                            {
                                if ((filterByFicancialYear.Count > 0 && !filterByFicancialYear.Contains(item.FYIndicator)) || (_invoiceStatus.Count > 0 && !_invoiceStatus.Contains(item.InvoiceStatus)))
                                {
                                    item.SKUDescription = null;
                                    item.SKUName = null;
                                    item.InvoiceLicense = null;
                                    item.InvoiceSKUFYIndicatorKey = null;
                                    item.PriceBand = null;
                                    item.QTY = null;
                                    item.InvoiceProrateYN = null;
                                    item.InvoiceStatus = null;
                                    item.Discount = null;
                                    item.DiscountType = null;
                                    item.InvoiceRejectedBy = null;
                                    item.NumberOfLicenses = null;
                                    item.InvoiceSKUId = null;
                                    item.InvoiceRejectedDate = null;
                                    item.RejectReason = null;
                                }
                            }
                        }
                        if (ORGReport == "SUSPENDED")
                        {
                            reportsOrg = reportsOrg.Where(x => (x.OrgStatus == AutodeskConst.StatusConts.S)).ToList();
                            reports = reports.Where(x => (x.OrgStatus == AutodeskConst.StatusConts.S)).ToList();
                        }
                    }
                    else
                    {
                        reportsOrg = reportsOrg.Where(x => filterByFicancialYear.Contains(x.FYIndicatorKey)).ToList();
                        reports = reports.Where(x => filterByFicancialYear.Contains(x.FYIndicator)).ToList();
                        reports = reports.Where(invoice => (_invoiceStatus.Count > 0 ? FilterByInvoiceStatus(invoice.InvoiceStatus, invoice.PaymentReceivedDate, invoice.InvoiceCreatedDate, invoice.InvoiceRejectedDate, _invoiceStatus, fromDate, toDate) : true)).DistinctBy(x => x.InvoiceSKUId).ToList();
                    }

                    reports.ForEach(x => { x.TotalAmmount = Math.Round(x.TotalAmmount.Value, 2); });
                    var discountGrp = reports.DistinctBy(x => x.InvoiceId).GroupBy(x => new { x.OrgId, x.InvoiceId });
                    var disTotal = discountGrp.Select(x => new
                    {
                        OrgId = x.Key.OrgId,
                        x.Key.InvoiceId,
                        DiscountTotal = x.Sum(d => d.TotalDiscountAmmount)
                    });

                    var siteGrp = reports.GroupBy(x => new { x.SiteId, x.OrgId });
                    var siteTotal = siteGrp.Select(x => new
                    {
                        x.Key.OrgId,
                        SiteId = x.Key.SiteId,
                        SiteTotal = x.Sum(s => s.TotalAmmount)
                    });

                    var grpOrg = reports.GroupBy(x => x.OrgId);
                    var orgTotal = grpOrg.Select(x => new
                    {
                        OrgId = x.Key,
                        Total = x.Sum(s => s.TotalAmmount),
                        //TotalLicense = x.Sum(z=>z.QTY * double.Parse(z.InvoiceLicense)),
                        TotalWithDiscount = x.Sum(s => s.TotalAmmount) - disTotal.Where(d => d.OrgId == x.Key && x.Any(i => i.InvoiceId == d.InvoiceId)).Sum(s => s.DiscountTotal)

                    }).ToList();

                    //Filter Null Out before sending to frontend - Nakarin
                    reportsOrg = reportsOrg.Where(x => !string.IsNullOrEmpty(x.OrgId)).ToList();

                    var res = new ReportListDto
                    {
                        Reports = reports,
                        ReportsOrg = reportsOrg,
                        OrgTotals = orgTotal.Select(x => new OrgTotal
                        {
                            OrgId = x.OrgId,
                            Total = x.Total.Value,
                            //TotalLicense = x.TotalLicense.Value,
                            TotalWithDiscount = x.TotalWithDiscount,
                            SiteTotal = siteTotal.Where(s => s.OrgId == x.OrgId).Select(s => new SiteTotal { SiteId = s.SiteId, Total = s.SiteTotal.Value }).ToList()
                        }).ToList(),
                        ReportTotalAmount = reportsOrg.Sum(x => x.TotalInvoiceAmmount.HasValue ? x.TotalInvoiceAmmount.Value : 0)//reports.DistinctBy(x => x.InvoiceSKUId).Sum(x => x.TotalAmmount.Value) - disTotal.Sum(x => x.DiscountTotal.Value)

                    };

                    var settings = new JsonSerializerSettings
                    {
                        DateFormatString = "yyyy-MM-dd"
                    };
                    settings.Converters.Add(new DataSetConverter());
                    settings.ContractResolver = new DefaultContractResolver();

                    result = JsonConvert.SerializeObject(res, Formatting.Indented,
                        settings);
                    //result = MySqlDb.SerializeDataTable(ds.Tables[0]);    
                    //}
                    return Ok(result);
                    //return Ok(_query);
                }
                return Ok();
            }
            catch (Exception ex)
            {

                return Ok();
            }


        }
        [HttpPost("InvoiceReporting")]
        public async Task<IActionResult> ReportInvoiceNew([FromBody] InvoiceReportInputDto dto)
        {
            var filterByInvoicesNumber = dto.filterByInvoicesNumber;

            var filterByPartnerType = !string.IsNullOrEmpty(dto.filterByPartnerType) ? dto.filterByPartnerType.Replace("'", string.Empty).Split(',').Select(x => int.Parse(x)).ToList() : new List<int>();


            var filterByPartnerTypeStatus = !string.IsNullOrEmpty(dto.filterByPartnerTypeStatus) ? dto.filterByPartnerTypeStatus.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

            var filterByMarketType = !string.IsNullOrEmpty(dto.filterByMarketType) ? dto.filterByMarketType.Replace("'", string.Empty).Split(',').Select(x => int.Parse(x)).ToList() : new List<int>();

            var filterByGeo = !string.IsNullOrEmpty(dto.filterByGeo) ? dto.filterByGeo.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

            var filterByTerritory = !string.IsNullOrEmpty(dto.filterByTerritory) ? dto.filterByTerritory.Replace("'", string.Empty).Split(',').Select(x => int.Parse(x)).ToList() : new List<int>();

            var filterByCountries = !string.IsNullOrEmpty(dto.filterByCountries) ? dto.filterByCountries.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

            var filterByFicancialYear = !string.IsNullOrEmpty(dto.FinancialYear) ? dto.FinancialYear.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

            var filterByInvoiceStatus = !string.IsNullOrEmpty(dto.filterByInvoiceStatus) ? dto.filterByInvoiceStatus.Replace("'", string.Empty).Split(',').ToList() : new List<string>();

            var fromDate = dto.filterFromDate.HasValue ? dto.filterFromDate.Value.ToShortDateString() : string.Empty;
            var toDate = dto.filterToDate.HasValue ? dto.filterToDate.Value.ToShortDateString() : string.Empty;
            var ORGReport = dto.ORGReport;

            //            string query = " SELECT DISTINCT Main_organization.OrgId,Main_organization.Status AS OrgStatus, "
            //+ " Main_organization.OrgName, "
            //+ " Ref_territory.Territory_Name AS TerritoryName, "
            //+ " Ref_countries.countries_name AS CountriesName, "
            //+ " Invoices.*, "
            //+ " Sites.* "
            //+ " FROM "
            //+ " Main_organization "
            //+ " INNER JOIN "
            //+ " (SELECT "
            //+ "  Main_site.OrgId AS SiteOrgId, "
            //+ " SiteRoles.Status AS SiteStatus, "
            //+ " Roles.RoleName AS PartnerType, "
            //+ " Roles.RoleId, "
            //+ " Main_site.SiteName, "
            //+ " Main_site.SiteId, "
            //+ " Ref_territory.Territory_Name AS SiteTerritory, "
            //+ " Ref_countries.countries_name AS SiteCountry "
            //+ "  FROM Main_organization LEFT JOIN "
            //+ "  Main_site ON Main_organization.OrgId = Main_site.OrgId "
            //+ " LEFT JOIN  SiteRoles ON Main_site.SiteId = SiteRoles.SiteId "
            //+ " LEFT JOIN  Roles ON SiteRoles.RoleId = Roles.RoleId "
            //+ "  LEFT JOIN  Ref_countries ON Main_site.SiteCountryCode = Ref_countries.countries_code "
            //+ " LEFT JOIN  Ref_territory ON Ref_countries.TerritoryId = Ref_territory.TerritoryId) Sites ON Main_organization.OrgId = Sites.SiteOrgId "
            //+ "  LEFT JOIN ( "
            //+ "  SELECT Invoices.OrgId AS InvoiceOrgId, InvoiceDiscount.AmountType, InvoiceDiscount.Amount AS Discount, "
            //+ "  Invoices.InvoiceNumber, Invoices.FinancialYear AS FYIndicatorKey, Invoices.RejectReason, DATE_FORMAT(InvoiceDate, '%Y-%m-%d') AS PaymentReceiveDate, "
            //+ "  Invoices.DateAdded AS  InvoiceCreatedDate, (CASE WHEN Invoices.Comments = 'Approved' THEN(CASE  WHEN Invoices.Status = 'A' THEN 'Approved' WHEN Invoices.Status = 'C' THEN 'Cancelled' ELSE 'Paid' END) "
            //+ "  WHEN Invoices.Comments = 'Rejected' THEN(CASE  WHEN Invoices.Status = 'A' THEN 'Rejected' WHEN Invoices.Status = 'C' THEN 'Cancelled' ELSE 'Paid' END) "
            //+ "  ELSE(CASE WHEN Invoices.InvoiceId IS NULL THEN '' ELSE CASE WHEN Invoices.Status = 'A' THEN 'Pending' WHEN Invoices.Status = 'C' THEN 'Cancelled' ELSE 'Paid' END  END) END) AS InvoiceStatus, "
            //+ "  (CASE WHEN InvoiceDiscount.AmountType = 'Percentage' THEN SUM((CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) - (SUM((CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100)) "
            //+ "  WHEN InvoiceDiscount.AmountType = 'Exactly' THEN SUM((CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) - InvoiceDiscount.Amount ELSE SUM(CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY) END) "
            //+ "  AS TotalInvoiceAmmount,  (CASE WHEN InvoiceDiscount.AmountType = 'Percentage' THEN(SUM((CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100)) "
            //+ "  WHEN InvoiceDiscount.AmountType = 'Exactly'THEN SUM(CAST(InvoiceDiscount.Amount AS UNSIGNED))  END) AS OrgTotalDiscount, SUM(InvoiceSKU.QTY * CAST(SiteCountryDistributorSKU.License AS UNSIGNED)) AS NumberOfLicenses, "
            //+ "  Contacts_All.ContactName AS InvoiceCreatedBy, Contacts_All.ContactIdInt, GROUP_CONCAT(CONCAT(InvoiceSKU.SiteId, '(', SKUName, '*', QTY, ')') SEPARATOR ' , ') AS SKUDescription, "
            //+ "  (CASE WHEN Invoices.Comments = 'Approved' THEN Invoices.LastAdminBy END) AS ApprovedBy, (CASE WHEN Invoices.Comments = 'Rejected' THEN Invoices.LastAdminBy END) AS RejectedBy, Invoices.Comments, "
            //+ "  Invoices.DateLastAdmin, Invoices.Status, Invoices.InvoiceId,  InvoiceSKU.InvoiceSKUId , (CASE WHEN Invoices.Comments = 'Approved' THEN Invoices.DateLastAdmin END) AS InvoiceApprovedDate, "
            //+ "  (CASE WHEN Invoices.Comments = 'Rejected' THEN Invoices.DateLastAdmin END) AS InvoiceRejectedDate, SiteCountryDistributorSKU.SKUName,  (CASE  WHEN(InvoiceDiscount.Name = 'Discount') THEN 'N' "
            //+ "  WHEN(InvoiceDiscount.Name = 'Pro-Rata') THEN 'Y'  WHEN(InvoiceDiscount.Name = '') THEN ''  END ) AS ProratedYN, SiteCountryDistributorSKU.FYIndicatorKey AS SKUFY, InvoicePricingSKU.Price AS PriceBand, "
            //+ "  InvoiceSKU.QTY AS SKUQTY, Invoices.InvoiceDescription, Invoices.InvoiceDate AS PaymentReceivedDate FROM Invoices JOIN InvoiceSKU ON Invoices.InvoiceId = InvoiceSKU.InvoiceId "
            //+ "  JOIN InvoicePricingSKU ON InvoiceSKU.PricingSKUId = InvoicePricingSKU.Id "
            //+ "  JOIN SiteCountryDistributorSKU ON InvoicePricingSKU.SiteCountryDistributorSKUId = SiteCountryDistributorSKU.SiteCountryDistributorSKUId "
            //+ "   LEFT JOIN InvoiceDiscount ON Invoices.InvoiceId = InvoiceDiscount.InvoiceId "
            //+ "   LEFT JOIN Contacts_All ON Invoices.AddedBy = Contacts_All.ContactId "
            //+ "  ";
            var query = " SELECT DISTINCT Main_organization.OrgId,Main_organization.OrganizationId AS OrgIdInt,Main_organization.Status AS OrgStatus,Main_organization.OrgName,Ref_territory.Territory_Name AS TerritoryName,Ref_countries.countries_name AS CountriesName,Invoices.*, Sites.*, GROUP_CONCAT(DISTINCT Roles.RoleName) as PartnerType, SiteRoles.Status as SiteStatus FROM " +
            " Main_organization INNER JOIN  (SELECT   Main_site.OrgId AS SiteOrgId, " +
                   " Ref_territory.Territory_Name AS SiteTerritory, " +
                       " Ref_countries.countries_name AS SiteCountry, " +
                       " Main_site.SiteName, " +
                       " Main_site.SiteId, " +
                       " Main_site.SiteIdInt " +
                      "  FROM Main_site " +
                      "  INNER JOIN Ref_countries ON Main_site.SiteCountryCode = Ref_countries.countries_code " +
                      "  INNER JOIN Ref_territory ON Ref_countries.TerritoryId = Ref_territory.TerritoryId " +
                       " )" +
                  "  Sites ON Main_organization.OrgId = Sites.SiteOrgId LEFT JOIN " +
            " (" +
            " SELECT Invoices.OrgId AS InvoiceOrgId, InvoiceSKU.SiteId, InvoiceDiscount.AmountType, InvoiceDiscount.Amount AS Discount, Invoices.InvoiceNumber, " +
            "Invoices.FinancialYear AS FYIndicatorKey, Invoices.RejectReason, DATE_FORMAT(InvoiceDate, '%Y-%m-%d') AS PaymentReceiveDate, " +
            " Invoices.DateAdded AS  InvoiceCreatedDate, " +
            " (CASE WHEN Invoices.Comments = 'Approved' THEN(CASE  WHEN Invoices.Status = 'A' THEN 'Pending (Approved)' WHEN Invoices.Status = 'C' THEN 'Cancelled' ELSE 'Paid' END) " +
            " WHEN Invoices.Comments = 'Rejected' THEN(CASE  WHEN Invoices.Status = 'A' THEN 'Pending (Rejected)' WHEN Invoices.Status = 'C' THEN 'Cancelled' ELSE 'Paid' END) " +
            " ELSE(CASE WHEN Invoices.InvoiceId IS NULL THEN '' ELSE CASE WHEN Invoices.Status = 'A' THEN 'Pending' WHEN Invoices.Status = 'C' THEN 'Cancelled' ELSE 'Paid' END  END) " +
            " END) AS InvoiceStatus, (CASE WHEN InvoiceDiscount.AmountType = 'Percentage' THEN SUM((CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) - (SUM((CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100)) WHEN InvoiceDiscount.AmountType = 'Exactly' THEN SUM((CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) - InvoiceDiscount.Amount " +
            " ELSE SUM(CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY) END) AS TotalInvoiceAmmount,  (CASE WHEN InvoiceDiscount.AmountType = 'Percentage' THEN(SUM((CAST(InvoicePricingSKU.Price AS UNSIGNED) * InvoiceSKU.QTY)) * (InvoiceDiscount.Amount / 100)) WHEN InvoiceDiscount.AmountType = 'Exactly' THEN InvoiceDiscount.Amount END) AS OrgTotalDiscount, SUM(InvoiceSKU.QTY * CAST(SiteCountryDistributorSKU.License AS UNSIGNED)) AS NumberOfLicenses," +
            "  SiteCountryDistributorSKU.License AS QTYPerSetup, " +
            " Contacts_All.ContactName AS InvoiceCreatedBy, Contacts_All.ContactIdInt, GROUP_CONCAT(CONCAT(InvoiceSKU.SiteId, '(', SKUName, '*', QTY, ')') SEPARATOR ' , ') AS SKUDescription, " +
            " (CASE WHEN Invoices.Comments = 'Approved' THEN Invoices.LastAdminBy END) AS ApprovedBy, (CASE WHEN Invoices.Comments = 'Rejected' THEN Invoices.LastAdminBy END) AS RejectedBy, " +
            " Invoices.Comments, Invoices.DateLastAdmin, Invoices.Status, Invoices.InvoiceId,  InvoiceSKU.InvoiceSKUId , (CASE WHEN Invoices.Comments = 'Approved' THEN Invoices.DateLastAdmin END) AS InvoiceApprovedDate, (CASE WHEN Invoices.Comments = 'Rejected' THEN Invoices.DateLastAdmin END) AS InvoiceRejectedDate, SiteCountryDistributorSKU.SKUName,  (CASE  WHEN(InvoiceDiscount.Name = 'Discount') THEN 'N'  WHEN(InvoiceDiscount.Name = 'Pro-Rata') THEN 'Y'  WHEN(InvoiceDiscount.Name = '') THEN ''  END ) AS ProratedYN, " +
            " SiteCountryDistributorSKU.FYIndicatorKey AS SKULFY, InvoicePricingSKU.Price AS PriceBand,  InvoiceSKU.QTY AS SKUQTY, Invoices.InvoiceDescription, Invoices.InvoiceDate AS PaymentReceivedDate FROM Invoices JOIN InvoiceSKU ON Invoices.InvoiceId = InvoiceSKU.InvoiceId  JOIN InvoicePricingSKU ON InvoiceSKU.PricingSKUId = InvoicePricingSKU.Id " +
            " JOIN SiteCountryDistributorSKU ON InvoicePricingSKU.SiteCountryDistributorSKUId = SiteCountryDistributorSKU.SiteCountryDistributorSKUId " +
            " LEFT JOIN InvoiceDiscount ON Invoices.InvoiceId = InvoiceDiscount.InvoiceId " +
            " LEFT JOIN Contacts_All ON Invoices.AddedBy = Contacts_All.ContactId  ";

            var i = 0;



            if (filterByFicancialYear?.Count > 0)
            {
                if (i == 1)
                { query += " AND Invoices.FinancialYear IN (" + dto.FinancialYear + ") "; }
                else
                {
                    query += " WHERE  Invoices.FinancialYear IN (" + dto.FinancialYear + ") ";
                    i = 1;
                }

            }

            if (filterByInvoiceStatus?.Count > 0)
            {
                if (i == 1)
                {
                    query += " AND Invoices.Status IN (" + dto.filterByInvoiceStatus + ") ";
                }
                else
                {
                    query += " WHERE Invoices.Status IN (" + dto.filterByInvoiceStatus + ") ";
                    i = 1;
                }


            }

            query += "  GROUP BY Invoices.OrgId , InvoiceSKU.SiteId, Invoices.InvoiceId) Invoices  " +
 " ON Invoices.SiteId = Sites.SiteId " +
 " JOIN SiteRoles ON Sites.SiteId = SiteRoles.SiteId " +
 " JOIN Roles ON SiteRoles.RoleId = Roles.RoleId " +
 " Inner JOIN Ref_countries ON Main_organization.RegisteredCountryCode = Ref_countries.countries_code " +
 " Inner JOIN Ref_territory ON Ref_countries.TerritoryId = Ref_territory.TerritoryId " +
 " Inner JOIN MarketType ON Ref_countries.MarketTypeId = MarketType.MarketTypeId ";

            if (ORGReport == "SKU")
            {
                if (i == 1)
                {
                    query += " WHERE Invoices.InvoiceSKUId IS NOT NULL ";
                }
                else
                {
                    query += " WHERE Invoices.InvoiceSKUId IS NOT NULL ";
                }
            }

            if (filterByMarketType?.Count > 0)
            {
                query += " AND MarketType.MarketTypeId IN (" + dto.filterByMarketType + ") ";
            }
            if (filterByPartnerType?.Count > 0)
            {
                query += " AND SiteRoles.RoleId IN (" + dto.filterByPartnerType + ") ";
            }

            if (filterByPartnerTypeStatus?.Count > 0)
            {
                query += " AND SiteRoles.Status IN (" + dto.filterByPartnerTypeStatus + ") ";
            }

            if (filterByCountries?.Count > 0)
            {
                query += " AND Ref_countries.countries_code IN (" + dto.filterByCountries + ") ";
            }

            if (!string.IsNullOrEmpty(filterByInvoicesNumber))
            {
                query += " AND Invoices.InvoiceNumber LIKE ('%" + dto.filterByInvoicesNumber + "%') ";
            }

            if (!string.IsNullOrEmpty(fromDate) || !string.IsNullOrEmpty(toDate) && filterByInvoiceStatus.Contains("X"))
            {
                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    query += " AND( Invoices.PaymentReceivedDate >= '" + fromDate + "' AND Invoices.PaymentReceivedDate <= '" + toDate + "' ) ";
                }
                else if (!string.IsNullOrEmpty(fromDate) && !!string.IsNullOrEmpty(toDate))
                {
                    query += " AND( Invoices.PaymentReceivedDate >= '" + fromDate + "' ) ";

                }
                else if (!!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    query += " AND( Invoices.PaymentReceivedDate <= '" + toDate + "' ) ";
                }

            }
            else if (filterByInvoiceStatus.Contains("A") && (!string.IsNullOrEmpty(fromDate) || !string.IsNullOrEmpty(toDate)))
            {
                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    query += " AND( Invoices.InvoiceCreatedDate >= '" + fromDate + "' AND Invoices.InvoiceCreatedDate <= '" + toDate + "' ) ";

                }
                else if (!string.IsNullOrEmpty(fromDate) && !!string.IsNullOrEmpty(toDate))
                {
                    query += " AND( Invoices.InvoiceCreatedDate >= '" + fromDate + "'  ) ";
                }
                else if (!!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    query += " AND(  Invoices.InvoiceCreatedDate <= '" + toDate + "' ) ";
                }

            }

            else if (filterByInvoiceStatus.Contains("C") && (!string.IsNullOrEmpty(fromDate) || !string.IsNullOrEmpty(toDate)))
            {

                if (!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    query += " AND( Invoices.InvoiceRejectedDate >= '" + fromDate + "' AND Invoices.InvoiceRejectedDate <= '" + toDate + "' ) ";
                }
                else if (!string.IsNullOrEmpty(fromDate) && !!string.IsNullOrEmpty(toDate))
                {
                    query += " AND( Invoices.InvoiceRejectedDate >= '" + fromDate + "'  ) ";
                }
                else if (!!string.IsNullOrEmpty(fromDate) && !string.IsNullOrEmpty(toDate))
                {
                    query += " AND(  Invoices.InvoiceRejectedDate <= '" + toDate + "' ) ";
                }
            }

            query += " group by Invoices.InvoiceId, Sites.SiteId ORDER BY Main_organization.OrgId;";
            using (var command = _mySQLContext.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                try
                {
                    _mySQLContext.Database.OpenConnection();
                    using (var reader = command.ExecuteReader())
                    {
                        var res = await Task.Run(() => reader.ToJsonString());
                        var data = JsonConvert.DeserializeObject<List<InvoiceReportDtoNew>>(res);


                        var result = new ReportDataDto();
                        {
                            result.WebInfo = data.Select(x => new
                            {
                                OrgId = x.OrgId,
                                OrgIdInt = x.OrgIdInt,
                                SiteIdInt = x.SiteIdInt,
                                OrgName = x.OrgName,
                                Territory = x.TerritoryName,
                                Country = x.CountriesName,
                                OrgStatus = x.OrgStatus,
                                SiteId = x.SiteId,
                                SiteName = x.SiteName,
                                PartnerType = x.PartnerType,
                                SKUName = x.SKUName,
                                QTY = x.SKUQTY,
                                InvoiceStatus = x.InvoiceStatus,
                                SiteStatus = x.SiteStatus,
                                PriceBand = x.PriceBand,
                                SKUSRP = x.PriceBand,
                                Discount = x.Discount,
                                OrgTotal = x.TotalInvoiceAmmount,
                                ORGTotalInclDiscount = x.TotalInvoiceAmmount == 0 ? 0 : x.OrgTotalDiscount != null ? x.TotalInvoiceAmmount - x.OrgTotalDiscount : x.TotalInvoiceAmmount,
                                ValidAgreementInPlace = string.Empty,
                                PaymentReceiveDate = x.PaymentReceiveDate,
                                Description = x.InvoiceDesciption,
                                LastAdmin = x.Comments == "Approved" ? x.ApprovedBy : x.Comments == "Rejected" ? x.RejectedBy : x.Comments == "Saved" || x.Comments == "Added" ? x.InvoiceCreatedBy : string.Empty,
                                Approver = x.Comments == "Approved" ? x.ApprovedBy : string.Empty,
                                AmountType = x.AmountType,
                                SiteTotal = x.SKUQTY != null && !string.IsNullOrEmpty(x.PriceBand) ? (x.SKUQTY * int.Parse(x.PriceBand)) : null

                            }).Distinct();
                            var excelOrgInfo = data.Select(x => new
                            {
                                OrgId = x.OrgId,
                                x.InvoiceId,
                                OrgName = x.OrgName,
                                OrgCountry = x.CountriesName,
                                Territory = x.TerritoryName,
                                ReferenceNo = x.InvoiceNumber,
                                //OrgTotal = x.TotalInvoiceAmmount,
                                DiscountPercentage = (x.AmountType == "Percentage" ? x.Discount : 0),
                                Discount = x.Discount,
                                OrgTotalDiscount = x.OrgTotalDiscount,
                                InvoiceStatus = x.InvoiceStatus,
                                FY = x.FYIndicatorKey,
                                //TotalLicenseCount = x.NumberOfLicenses,
                                CreatedID = x.InvoiceCreatedBy,
                                CreatedDate = x.InvoiceCreatedDate,
                                PaymentReceiveDate = x.PaymentReceiveDate,
                                ApproverID = x.ApprovedBy,
                                ApproverDate = x.InvoiceApprovedDate,
                                RejectedID = x.RejectedBy,
                                RejectedDate = x.InvoiceRejectedDate,
                                RejectReason = x.RejectReason,
                            }).Distinct();
                            var groupTotalAmount = data.Select(x => new
                            {

                                x.OrgId,
                                x.InvoiceId,
                                x.TotalInvoiceAmmount,
                                x.OrgTotalDiscount,
                                x.NumberOfLicenses

                            }).GroupBy(x => new { x.OrgId, x.InvoiceId }).Select(x => new
                            {
                                OrgId = x.Key.OrgId,
                                InvoiceId = x.Key.InvoiceId,
                                OrgTotal = x.Sum(s => s.TotalInvoiceAmmount),
                                TotalDiscount = x.Sum(s => s.OrgTotalDiscount),
                                TotalLicense = x.Sum(s => s.NumberOfLicenses)
                            }).DistinctBy(x => x.InvoiceId).ToList();

                            var joinExelOrgInfo = (from info in excelOrgInfo
                                                   join total in groupTotalAmount on info.OrgId equals total.OrgId
                                                   where info.InvoiceId == total.InvoiceId
                                                   select new
                                                   {
                                                       OrgId = info.OrgId,
                                                       OrgName = info.OrgName,
                                                       OrgCountry = info.OrgCountry,
                                                       Territory = info.Territory,
                                                       ReferenceNo = info.ReferenceNo,
                                                       OrgTotal = total.OrgTotal,
                                                       info.DiscountPercentage,
                                                       Discount = info.Discount,
                                                       OrgTotalDiscount = total.TotalDiscount,
                                                       InvoiceStatus = info.InvoiceStatus,
                                                       FY = info.FY,
                                                       TotalLicenseCount = total.TotalLicense,
                                                       CreatedID = info.CreatedID,
                                                       CreatedDate = info.CreatedDate,
                                                       PaymentReceiveDate = info.PaymentReceiveDate,
                                                       ApproverID = info.ApproverID,
                                                       ApproverDate = info.ApproverDate,
                                                       RejectedID = info.RejectedID,
                                                       RejectedDate = info.RejectedDate,
                                                       RejectReason = info.RejectReason,
                                                   }
                                                 ).Distinct();

                            result.ExcelOrgInfo = joinExelOrgInfo;

                            result.ExcelSiteInfo = data.Select(x => new
                            {
                                SiteOrgId = x.SiteOrgId,
                                SiteName = x.SiteName,
                                SiteId = x.SiteId,
                                PartnerType = x.PartnerType,
                                SiteCountry = x.SiteCountry,
                                SiteStatus = x.SiteStatus,
                                SiteTerritory = x.SiteTerritory,
                                SKUDescription = x.SKUDescription,
                                SKUName = x.SKUName,
                                SKUQTYASPerSetup = x.QTYPerSetup,
                                SKUFY = x.SKUFY,
                                SKUValue = x.PriceBand,
                                SKUQTY = x.SKUQTY,
                                SKULicenseCount = 0,
                                ProratedYN = x.ProratedYN

                            }).Distinct();
                        }


                        return Ok(JsonConvert.SerializeObject(result));
                    }
                }
                catch (Exception ex)
                {

                }
            }

            return Ok();

        }
        private DiscountTotal SetDiscountTotal(InvoiceDiscount dis, double _totalAmount, int invoiceId)
        {
            return new DiscountTotal
            {
                Amount = dis != null && dis.AmountType == AutodeskConst.AmountType.Exactly ? dis.Amount : dis != null && dis.AmountType == AutodeskConst.AmountType.Percentage ? _totalAmount - (_totalAmount * (dis.Amount / 100)) : 0,
                InvoiceId = invoiceId
            };
        }

        private bool FilterByInvoiceStatus(string status, DateTime? PaymentReceivedDate, DateTime? InvoiceCreatedDate, DateTime? InvoiceRejectedDate, List<string> invoiceStatus, DateTime? fromDate, DateTime? toDate)
        {
            //if (invoiceStatus.Count > 0)
            //{
            //    if (invoiceStatus.Contains("X") && (fromDate.HasValue || toDate.HasValue))
            //    {
            //        if (fromDate.HasValue && toDate.HasValue)
            //        {
            //            return PaymentReceivedDate.HasValue ?
            //                  invoiceStatus.Contains(status) && PaymentReceivedDate.Value.Date >= fromDate.Date && PaymentReceivedDate.Value.Date <= toDate.Date : false;
            //            //return status == invoiceStatus && PaymentReceivedDate >= fromDate && PaymentReceivedDate <= toDate;
            //        }
            //        else if (fromDate.HasValue && !toDate.HasValue)
            //        {
            //            return PaymentReceivedDate.HasValue ?
            //                invoiceStatus.Contains(status) && PaymentReceivedDate.Value.Date >= fromDate.Date : false;
            //        }
            //        else if (!fromDate.HasValue && toDate.HasValue)
            //        {
            //            return PaymentReceivedDate.HasValue ? invoiceStatus.Contains(status) && PaymentReceivedDate.Value.Date <= toDate.Date : false;
            //        }

            //    }

            //    else if (invoiceStatus.Contains("A") && (fromDate.HasValue || toDate.HasValue))
            //    {
            //        if (fromDate.HasValue && toDate.HasValue)
            //        {
            //            return InvoiceCreatedDate.HasValue ? invoiceStatus.Contains(status) && InvoiceCreatedDate.Value.Date >= fromDate.Date && InvoiceCreatedDate.Value.Date <= toDate.Date : false;
            //        }
            //        else if (fromDate.HasValue && !toDate.HasValue)
            //        {
            //            return InvoiceCreatedDate.HasValue ? invoiceStatus.Contains(status) && InvoiceCreatedDate.Value.Date >= fromDate.Date : false;
            //        }
            //        else if (!fromDate.HasValue && toDate.HasValue)
            //        {
            //            return InvoiceCreatedDate.HasValue ? invoiceStatus.Contains(status) && InvoiceCreatedDate.Value.Date <= toDate.Date : false;
            //        }

            //    }

            //    else if (invoiceStatus.Contains("C") && (fromDate.HasValue || toDate.HasValue))
            //    {

            //        if (fromDate.HasValue && toDate.HasValue)
            //        {
            //            return InvoiceRejectedDate.HasValue ? invoiceStatus.Contains(status) && InvoiceCreatedDate.Value.Date >= fromDate.Date && InvoiceRejectedDate.Value.Date <= toDate.Date : false;
            //        }
            //        else if (fromDate.HasValue && !toDate.HasValue)
            //        {
            //            return InvoiceRejectedDate.HasValue ? invoiceStatus.Contains(status) && InvoiceCreatedDate.Value.Date >= fromDate.Date : false;
            //        }
            //        else if (!fromDate.HasValue && toDate.HasValue)
            //        {
            //            return InvoiceRejectedDate.HasValue ? invoiceStatus.Contains(status) && InvoiceCreatedDate.Value.Date <= toDate.Date : false;
            //        }
            //    }
            //    else
            //    {
            //        return invoiceStatus.Contains(status);
            //    }
            //}
            return true;
        }
        private double GetPrice(string price)
        {
            if (string.IsNullOrEmpty(price))
            {
                return 0;
            }
            return double.Parse(price);
        }
        private async Task<Tuple<int, double>> GetQTYAndAmount(int? invoiceId)
        {
            if (invoiceId.HasValue)
            {
                var invoiceSKU = await Task.Run(() => _mySQLContext.InvoiceSku.Where(iv => iv.InvoiceId == invoiceId.Value).ToList());
                var quantity = 0;
                double amount = 0;
                foreach (var item in invoiceSKU)
                {
                    if (item.Qty.HasValue)
                    {
                        quantity = quantity + item.Qty.Value;

                    }
                    if (item.TotalAmmount.HasValue)
                    {
                        amount = amount + item.TotalAmmount.Value;
                    }
                }
                return new Tuple<int, double>(quantity, amount);
            }
            return new Tuple<int, double>(0, 0);

        }
        private static List<DateTime> ParsePOReceivedDate(List<string> filterByPOReceivedDate, List<DateTime> _filterByPOReceivedDate)
        {
            try
            {
                foreach (var item in filterByPOReceivedDate)
                {
                    if (DateTime.TryParse(item, out DateTime _date))
                    {
                        _filterByPOReceivedDate.Add(_date);
                    }
                }
                return _filterByPOReceivedDate;
            }
            catch (Exception ex)
            {
                return _filterByPOReceivedDate;

            }
        }
        [HttpPost("UpdateInvoice")]
        public async Task<IActionResult> SaveInvoice([FromBody] dynamic data)
        {
            var res =
                       "{" +
                       "\"code\":\"0\"," +
                       "\"message\":\"Update Data Failed\"" +
                       "}";
            var log = new History();
            try
            {
                if (data != null)
                {
                    var jsonData = data.ToString();
                    UpdateInvoiceDto updateInvoiceDto = JsonConvert.DeserializeObject<UpdateInvoiceDto>(jsonData);
                    try
                    {

                        var invoiceEntity = await Task.Run(() => _mySQLContext.Invoices.FirstOrDefault(i => i.InvoiceId == updateInvoiceDto.InvoiceId));
                        var invoiceDiscountEntity = _mySQLContext.InvoiceDiscount.Where(x => x.InvoiceId == updateInvoiceDto.InvoiceId);

                        if (invoiceEntity != null)
                        {

                            //var userId = Request.Headers["UserId"];
                            var userId = HttpContext.User.GetUserId();
                            if (!string.IsNullOrEmpty(userId))
                            {
                                var contact = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactId == userId.ToString());
                                if (contact != null && contact.UserLevelId != "SUPERADMIN" && invoiceEntity.Status == AutodeskConst.StatusConts.X)
                                {
                                    res =
                           "{" +
                           "\"code\":\"0\"," +
                           "\"message\":\"Unable to update Paid Invoice\"" +
                           "}";
                                    return Ok(res);
                                }
                                if (updateInvoiceDto.Status == AutodeskConst.StatusConts.Paid && invoiceDiscountEntity?.Count() > 0 && invoiceEntity.Comments != "Approved")
                                {
                                    res =
                          "{" +
                          "\"code\":\"0\"," +
                          "\"message\":\"Unable to update invoice status to paid.This invoice need discount approval first.\"" +
                          "}";
                                    return Ok(res);
                                }

                                if (contact != null && contact.UserLevelId == "SUPERADMIN")
                                {
                                    invoiceEntity.LastAdminBy = contact.ContactName;
                                    invoiceEntity.DateLastAdmin = DateTime.Now;

                                }
                                if (contact != null && contact.UserLevelId != "SUPERADMIN")
                                {

                                    invoiceEntity.LastUpdateBy = contact.ContactName;
                                    invoiceEntity.DateLastUpdate = DateTime.Now;
                                }

                                log = Utilities.SetHistoryData(invoiceEntity.OrgId, contact.ContactName, AutodeskConst.LogDescription.Update, "Invoice", "InvoiceId=" + invoiceEntity.InvoiceId);
                            }

                            invoiceEntity.InvoiceDate = updateInvoiceDto.InvoiceDate;
                            invoiceEntity.InvoiceNumber = updateInvoiceDto.InvoiceNumber;
                            invoiceEntity.InvoiceDescription = updateInvoiceDto.InvoiceDescription;
                            invoiceEntity.FinancialYear = updateInvoiceDto.FinancialYear;



                            if (updateInvoiceDto.InvoiceDate == null)
                            {
                                updateInvoiceDto.InvoiceDate = DateTime.Now;
                            }


                            if (updateInvoiceDto.Discounts != null && ((updateInvoiceDto.Discounts.ProRata != null && updateInvoiceDto.Discounts.ProRata.Amount != null) || (updateInvoiceDto.Discounts.Discount != null && updateInvoiceDto.Discounts.Discount.Amount != null)))
                            {


                                if (updateInvoiceDto.Discounts != null && updateInvoiceDto.Discounts.ProRata != null && updateInvoiceDto.Discounts.ProRata.Amount != null)
                                {
                                    var discountEntity = _mySQLContext.InvoiceDiscount.FirstOrDefault(d => d.InvoiceId == updateInvoiceDto.InvoiceId && d.Name == AutodeskConst.DiscountName.ProRata);
                                    if (discountEntity != null)
                                    {
                                        discountEntity.Amount = 50;
                                        discountEntity.AmountType = AutodeskConst.AmountType.Percentage;
                                        _mySQLContext.InvoiceDiscount.Update(discountEntity);
                                        _mySQLContext.SaveChanges();
                                        log.AddDescription("With ProRata:" + discountEntity.Amount + "%");
                                    }
                                    else
                                    {
                                        var _discountEntity = new InvoiceDiscount
                                        {
                                            Amount = 50,
                                            AmountType = AutodeskConst.AmountType.Percentage,
                                            InvoiceId = updateInvoiceDto.InvoiceId,
                                            Name = AutodeskConst.DiscountName.ProRata
                                        };
                                        _mySQLContext.InvoiceDiscount.Add(_discountEntity);
                                        _mySQLContext.SaveChanges();
                                        log.AddDescription("With ProRata:" + _discountEntity.Amount + "%");
                                    }

                                }


                                if (updateInvoiceDto.Discounts != null && updateInvoiceDto.Discounts.Discount != null && updateInvoiceDto.Discounts.Discount.Amount != null)
                                {
                                    var _discount = updateInvoiceDto.Discounts.Discount;
                                    if (_discount.AmountType == AutodeskConst.AmountType.Percentage && _discount.Amount > 100)
                                    {
                                        res =
                  "{" +
                  "\"code\":\"0\"," +
                  "\"message\":\"Discount must not greater than total ammount\"" +
                  "}";
                                        return Ok(res);
                                    }
                                    else if (_discount.AmountType == AutodeskConst.AmountType.Exactly)
                                    {
                                        double totalAmount = 0;
                                        foreach (var item in updateInvoiceDto.PricingSKUs)
                                        {
                                            totalAmount += ((int.Parse(item.Price) * item.QTY));
                                        }
                                        if (_discount.Amount > totalAmount)
                                        {
                                            res =
                          "{" +
                          "\"code\":\"0\"," +
                          "\"message\":\"Discount must not greater than total ammount\"" +
                          "}";
                                            return Ok(res);
                                        }
                                    }

                                    if (_discount.Amount != null)
                                    {
                                        var discountEntity = _mySQLContext.InvoiceDiscount.FirstOrDefault(d => d.InvoiceId == updateInvoiceDto.InvoiceId && d.Name == AutodeskConst.DiscountName.Discount);
                                        if (discountEntity != null)
                                        {
                                            discountEntity.Amount = _discount.Amount.HasValue ? _discount.Amount.Value : 0;
                                            discountEntity.AmountType = _discount.AmountType;
                                            _mySQLContext.InvoiceDiscount.Update(discountEntity);
                                            _mySQLContext.SaveChanges();
                                            log.AddDescription("With Discount:" + (discountEntity.AmountType == AutodeskConst.AmountType.Exactly ? discountEntity.Amount + "$" : discountEntity.Amount + "%"));
                                        }
                                        else
                                        {
                                            var _discountEntity = new InvoiceDiscount
                                            {
                                                Amount = _discount.Amount.HasValue ? _discount.Amount.Value : 0,
                                                AmountType = _discount.AmountType,
                                                Name = AutodeskConst.DiscountName.Discount,
                                                InvoiceId = updateInvoiceDto.InvoiceId
                                            };
                                            _mySQLContext.InvoiceDiscount.Add(_discountEntity);
                                            _mySQLContext.SaveChanges();
                                            log.AddDescription("With Discount:" + (_discountEntity.AmountType == AutodeskConst.AmountType.Exactly ? _discountEntity.Amount + "$" : _discountEntity.Amount + "%"));
                                        }



                                    }
                                }
                                else
                                {
                                }

                                if (invoiceEntity.Comments == "Added" && invoiceEntity.DocuSign != AutodeskConst.EmailType.MailSent)
                                {
                                    await SendInvoiceNotificationEmail(string.Empty, invoiceEntity, AutodeskConst.EmailType.ApprovedDiscount, false);
                                    invoiceEntity.DocuSign = AutodeskConst.EmailType.MailSent;
                                }

                            }
                            if (updateInvoiceDto.Discounts.ProRata.Amount == null)
                            {

                                var proDidiscountEntity = _mySQLContext.InvoiceDiscount.FirstOrDefault(d => d.InvoiceId == updateInvoiceDto.InvoiceId && d.Name == AutodeskConst.DiscountName.ProRata);
                                if (proDidiscountEntity != null)
                                {
                                    _mySQLContext.InvoiceDiscount.Remove(proDidiscountEntity);
                                    _mySQLContext.SaveChanges();
                                }

                            }
                            if (updateInvoiceDto.Discounts.Discount.Amount == null)
                            {

                                var discountEntity = _mySQLContext.InvoiceDiscount.FirstOrDefault(d => d.InvoiceId == updateInvoiceDto.InvoiceId && d.Name == AutodeskConst.DiscountName.Discount);
                                if (discountEntity != null)
                                {
                                    _mySQLContext.InvoiceDiscount.Remove(discountEntity);
                                    _mySQLContext.SaveChanges();
                                }
                            }
                            if (invoiceEntity.Comments != "Approved" && invoiceEntity.Comments != "Rejected")
                            {
                                invoiceEntity.Comments = "Saved";
                                invoiceEntity.Status = SetInvoiceStatus(updateInvoiceDto.Status);
                            }
                            else
                            {
                                invoiceEntity.Status = SetInvoiceStatus(updateInvoiceDto.Status);
                            }


                            _mySQLContext.Invoices.Update(invoiceEntity);
                            _mySQLContext.SaveChanges();

                            if (updateInvoiceDto.PricingSKUs.Any(x => x.QTY == 0))
                            {
                                res =
                      "{" +
                      "\"code\":\"0\"," +
                      "\"message\":\"Quantity must be at least  1.\"" +
                      "}";
                                return Ok(res);
                            }
                            else
                            {
                                await UpdateInvoiceSKU(updateInvoiceDto, invoiceEntity, null);
                            }

                            _auditLogServices.LogHistory(log);
                            res =
                              "{" +
                              "\"code\":\"1\"," +
                              "\"message\":\"Update Data Success\"" +
                              "}";
                            return Ok(res);



                        }
                        res =
                             "{" +
                             "\"code\":\"0\"," +
                             "\"message\":\"Data can not be null.\"" +
                             "}";
                        return Ok(res);

                    }
                    catch (Exception ex)
                    {

                        return Ok(res);
                    }
                }
                else
                {
                    res =
                             "{" +
                             "\"code\":\"0\"," +
                             "\"message\":\"Data can not be null.\"" +
                             "}";
                    return Ok(res);
                }
            }
            catch (Exception ex)
            {

                return Ok(res);
            }
        }
        private string SetInvoiceStatus(string invoiceStatus)
        {
            invoiceStatus = invoiceStatus.Trim();
            if (invoiceStatus == AutodeskConst.InvoiceStatus.Pending)
            {
                return AutodeskConst.StatusConts.A;
            }
            if (invoiceStatus == AutodeskConst.InvoiceStatus.Paid)
            {
                return AutodeskConst.StatusConts.X;
            }
            if (invoiceStatus == AutodeskConst.InvoiceStatus.Cancelled)
            {
                return AutodeskConst.StatusConts.C;
            }
            return string.Empty;
        }
        private string GetInvoiceStatus(string invoiceStatus)
        {
            invoiceStatus = invoiceStatus.Trim();
            if (invoiceStatus == AutodeskConst.StatusConts.A)
            {
                return AutodeskConst.InvoiceStatus.Pending;
            }
            if (invoiceStatus == AutodeskConst.StatusConts.X)
            {
                return AutodeskConst.InvoiceStatus.Paid;
            }
            if (invoiceStatus == AutodeskConst.StatusConts.C)
            {
                return AutodeskConst.InvoiceStatus.Cancelled;
            }
            return string.Empty;
        }
    }
}
