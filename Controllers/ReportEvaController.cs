using System;

using System.Collections.Generic;
using System.Linq;
using System.Dynamic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.DataServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using autodesk.Code.Models;
using Microsoft.Net.Http.Headers;
using System.IO;
using ClosedXML.Excel;
using System.Text.RegularExpressions;
using static autodesk.Code.DataServices.DataServices;
using autodesk.Code.AuditLogServices;

namespace autodesk.Controllers
{
    [Route("api/ReportEva")]
    public class ReportEvaController : Controller
    {
        StringBuilder sb = new StringBuilder();
        StringBuilder sb1 = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        private IDataServices dataService;
        String result;
        String result1;
        String result2;
        String result3;
        String result4;
        String result5;
        String result6;
        String finalresult;
        DataSet dsTemp = new DataSet();

        //string equipment = "";
        //string ctl = "";
        //string cs = "";
        //string ht = "";
        //string ctf = "";
        //string ctm = "";
        string fakingmistek = "";
        // JObject equipRes;
        // JObject ctlRes;
        // JObject csRes;
        // JObject htRes;
        // JObject ctfRes;
        // JObject ctmRes;
        dynamic eva_result = new JObject();
        private MySQLContext db;
        private readonly IAuditLogServices _auditLogServices;
        private readonly IEmailService _emailService;
        public ReportEvaController(MySqlDb sqlDb, MySQLContext _db, IAuditLogServices auditLogServices, IEmailService emailService)
        {
            oDb = sqlDb;
            db = _db;
            dataService=new DataServices(oDb,db);
            _auditLogServices = auditLogServices;
            _emailService = emailService;
        }

        [HttpPost("InstructorPerformance")]
        public IActionResult IntructorPerformance([FromBody] dynamic data)
        {

            string query = "";

            query +=
                "select s.SiteId,SiteName,RoleId,InstructorId,ContactName,COUNT(Evals) AS Evals,"+
                "CASE WHEN SUM(NbAnswerIQ * 100.00) = 0 THEN 0.00 ELSE CAST(SUM(ScoreIQ) / SUM(NbAnswerIQ * 100.00) * 100 AS DECIMAL(7 , 2 )) END WDT "+
                "From "+
                "(SELECT COUNT(er.StudentID) AS Evals,er.SiteId,SiteName,er.RoleId,ca.InstructorId,ca.ContactName,ca.ContactId,NbAnswerIQ,ScoreIQ "+
                "FROM EvaluationReport er "+
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId "+
                "INNER JOIN Courses c ON er.CourseId = c.CourseId "+
                "INNER JOIN Main_site s ON c.SiteId = s.SiteId "+
                "INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId AND er.RoleId = sr.RoleId "+
                " INNER JOIN Ref_countries rc ON er.CountryCode = rc.countries_code " +
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId ";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.GeoCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountries"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CountryCode IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if(property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4"){
                            query += "er.Month = " + property.Value.ToString() + " ";
                        } else{
                            switch(property.Value.ToString()){
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySiteStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        //query += "er.`Status` = '" + property.Value.ToString() + "'";
                        if (property.Value.ToString() != "A")
                        {
                            query += "s.`Status` <> 'A' ";
                        }
                        else
                        {
                            query += "s.`Status` = '" + property.Value.ToString() + "' ";
                        }
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        // query += "er.CertificateType in (" + property.Value.ToString() + ")";
                        
                        /* fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */

                        query += "(er.RoleId IN ('1','58') OR er.CertificateType IN (" + property.Value.ToString() + ")) ";

                        /* end line fix issue 17102018 - Select only ATC and ATC+AAP with certificate wrong result */
                        
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "ca.ContactId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.OrgId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.SiteCountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( "+ property.Value.ToString() +" ))";
                        i = i + 1;
                    }
                }

            }
            query += "GROUP BY er.StudentID, er.CourseId) AS s ";
            query += " GROUP BY s.SiteId,s.ContactId ORDER BY InstructorId ASC";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("TotalEvaluationResponse")]
        public IActionResult TotalEvaluationResponse([FromBody] dynamic data)
        {
            string query = "";
            string eventType = "";
            var partnerType = "";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.GeoCode in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountry"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CountryCode in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "eva.Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "eva.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "eva.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "eva.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "eva.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySiteStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.`Status` = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.RoleId in (" + property.Value.ToString() + ")";
                        partnerType = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CertificateType in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.ContactId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.SiteId IN (SELECT SiteId FROM Main_site WHERE OrgId IN (" + property.Value.ToString() + "))";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.SiteId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + property.Value.ToString() + " ))";
                        i = i + 1;
                    }
                }
            }

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("EventType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { eventType += " WHERE "; }
                        if (i > 0) { eventType += " AND "; }
                        eventType += " c.EventType2 IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }
            var settings = new JsonSerializerSettings
            {

            };
            settings.Converters.Add(new DataSetConverter());
            settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
            //Course equipment
            fakingmistek +=
                //Course Equipment
                "SELECT COUNT(c.CourseId) AS TotalEvaluation, IFNULL(SUM(CASE WHEN ATCFacility = 1 THEN 1 ELSE 0 END),0) AS Facility, IFNULL(SUM(CASE WHEN ATCComputer = 1 THEN 1 ELSE 0 END),0) AS Computer  FROM " +
                "(SELECT eva.CourseId FROM EvaluationReport eva inner join EvaluationAnswer ea on ea.CourseId =eva.CourseId and eva.StudentID =ea.StudentId inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId,eva.CourseId ) AS er INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + "; ";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(fakingmistek);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch (Exception e)
            {
                if (ds.Tables.Count == 0)
                {
                    finalresult = "Not Found";
                }
            }
            finally
            {

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }
        [HttpPost("EvaluationResponseNew")]
        public async Task<IActionResult> EvaluationResponseNew([FromBody] dynamic data)
        {
            object[] objectArray;
            string query = "";
            string tmpQuery = "";
            string eventType = "";
            var partnerType = "";
            string tmpFYyear = "";
            string tmpDate = "";
            List<dynamic> dynamicModel = new List<dynamic>();
            List<dynamic> dynamicModel1 = new List<dynamic>();
            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.GeoCode in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountry"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CountryCode in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        tmpFYyear = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "eva.Month = " + property.Value.ToString() + " ";
                            tmpDate = property.Value.ToString();
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "eva.Quarter = 1 ";
                                    tmpDate = "Quater 1";
                                    break;
                                case "q2":
                                    query += "eva.Quarter = 2 ";
                                    tmpDate = "Quater 2";
                                    break;
                                case "q3":
                                    query += "eva.Quarter = 3 ";
                                    tmpDate = "Quater 3";
                                    break;
                                case "q4":
                                    query += "eva.Quarter = 4 ";
                                    tmpDate = "Quater 4";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySiteStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.`Status` = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.RoleId in (" + property.Value.ToString() + ")";
                        partnerType = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CertificateType in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.ContactId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.SiteId IN (SELECT SiteId FROM Main_site WHERE OrgId IN (" + property.Value.ToString() + "))";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.SiteId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + property.Value.ToString() + " ))";
                        i = i + 1;
                    }
                }
            }

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("EventType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { eventType += " WHERE "; }
                        if (i > 0) { eventType += " AND "; }
                        eventType += " c.EventType2 IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }
            var settings = new JsonSerializerSettings
            {

            };
            settings.Converters.Add(new DataSetConverter());
            settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
            string query1 =      //Course Equipment
               "SELECT COUNT(c.CourseId) AS TotalEvaluation, IFNULL(SUM(CASE WHEN ATCFacility = 1 THEN 1 ELSE 0 END),0) AS Facility, IFNULL(SUM(CASE WHEN ATCComputer = 1 THEN 1 ELSE 0 END),0) AS Computer  FROM " +
               "(SELECT eva.CourseId FROM EvaluationReport eva INNER JOIN EvaluationAnswer ea ON eva.CourseId = ea.CourseId AND eva.StudentID = ea.StudentId    inner join Main_site s on s.SiteId =eva.SiteId INNER JOIN SiteRoles sr ON eva.SiteId = sr.SiteId AND eva.RoleId = sr.RoleId  INNER JOIN Ref_countries rc ON eva.CountryCode = rc.countries_code   " + query + " group by eva.StudentId,eva.CourseId) AS er INNER JOIN Courses c ON er.CourseId = c.CourseId " + eventType + "; ";

            string query2 = //   //Course Teaching Level
                "SELECT IFNULL(SUM(CASE WHEN `Update` = 1 THEN 1 ELSE 0 END),0) AS `Update`," +
                "IFNULL(SUM(CASE WHEN Essentials = 1 THEN 1 ELSE 0 END),0) AS Essentials," +
                "IFNULL(SUM(CASE WHEN Intermediate = 1 THEN 1 ELSE 0 END),0) AS Intermediate," +
                "IFNULL(SUM(CASE WHEN Advanced = 1 THEN 1 ELSE 0 END),0) AS Advanced," +
                "IFNULL(SUM(CASE WHEN Customized = 1 THEN 1 ELSE 0 END),0) AS Customized," +
                "IFNULL(SUM(CASE WHEN Other = 1 THEN 1 ELSE 0 END),0) AS Other FROM " +
                "(SELECT c.CourseId FROM (SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
                "INNER JOIN CourseTeachingLevel ctl ON ctl.CourseId = c.CourseId; " ;
            string query3 =  //    //Course Software
                "SELECT COUNT(pv.productId) AS TotalUse,CONCAT(productName,' ',Version) AS productName FROM " +
                "(SELECT productId,ProductVersionsId FROM " +
                "(SELECT c.CourseId FROM " +
                "(SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
                "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId) AS cs " +
                "LEFT JOIN Products p ON cs.productId = p.productId " +
                "LEFT JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
                "GROUP BY pv.ProductVersionsId ORDER BY pv.ProductVersionsId ASC; " ;
            string query4 =  //    //Training Hours
                "SELECT HoursTraining,COUNT(HoursTraining) AS SelectedHour FROM " +
                "(SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er INNER JOIN Courses c ON er.CourseId = c.CourseId GROUP BY HoursTraining;";
            string query5 = //    //Course Training Format
                            "SELECT IFNULL(SUM(CASE WHEN InstructorLed = 1 THEN 1 ELSE 0 END),0) AS InstructorLed,IFNULL(SUM(CASE WHEN `Online` = 1 THEN 1 ELSE 0 END),0) AS `Online` FROM " +
                            "(SELECT c.CourseId FROM " +
                            "(SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er " +
                            "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
                            "INNER JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId; ";
            string query6 =  //    //Course Training Material
                "SELECT IFNULL(SUM(CASE WHEN Autodesk = 1 THEN 1 ELSE 0 END),0) AS Autodesk," +
                "IFNULL(SUM(CASE WHEN AAP = 1 THEN 1 ELSE 0 END),0) AS AAP," +
                "IFNULL(SUM(CASE WHEN ATC = 1 THEN 1 ELSE 0 END),0) AS ATC," +
                "IFNULL(SUM(CASE WHEN Independent = 1 THEN 1 ELSE 0 END),0) AS Independent," +
                "IFNULL(SUM(CASE WHEN IndependentOnline = 1 THEN 1 ELSE 0 END),0) AS IndependentOnline," +
                "IFNULL(SUM(CASE WHEN ATCOnline = 1 THEN 1 ELSE 0 END),0) AS ATCOnline," +
                "IFNULL(SUM(CASE WHEN Other = 1 THEN 1 ELSE 0 END),0) AS Other FROM " +
                "(SELECT c.CourseId FROM " +
                "(SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
                "INNER JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId;";
            
           
            //Course equipment
            //fakingmistek +=
            //    //Course Equipment
            //    "SELECT COUNT(c.CourseId) AS TotalEvaluation, IFNULL(SUM(CASE WHEN ATCFacility = 1 THEN 1 ELSE 0 END),0) AS Facility, IFNULL(SUM(CASE WHEN ATCComputer = 1 THEN 1 ELSE 0 END),0) AS Computer  FROM " +
            //    "(SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId  " + query + " group by eva.studentId) AS er INNER JOIN Courses c ON er.CourseId = c.CourseId " + eventType + "; " +


            //    //Course Teaching Level
            //    "SELECT IFNULL(SUM(CASE WHEN `Update` = 1 THEN 1 ELSE 0 END),0) AS `Update`," +
            //    "IFNULL(SUM(CASE WHEN Essentials = 1 THEN 1 ELSE 0 END),0) AS Essentials," +
            //    "IFNULL(SUM(CASE WHEN Intermediate = 1 THEN 1 ELSE 0 END),0) AS Intermediate," +
            //    "IFNULL(SUM(CASE WHEN Advanced = 1 THEN 1 ELSE 0 END),0) AS Advanced," +
            //    "IFNULL(SUM(CASE WHEN Customized = 1 THEN 1 ELSE 0 END),0) AS Customized," +
            //    "IFNULL(SUM(CASE WHEN Other = 1 THEN 1 ELSE 0 END),0) AS Other FROM " +
            //    "(SELECT c.CourseId FROM (SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er " +
            //    "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
            //    "INNER JOIN CourseTeachingLevel ctl ON ctl.CourseId = c.CourseId; " +

            //    //Course Software
            //    "SELECT COUNT(pv.productId) AS TotalUse,CONCAT(productName,' ',Version) AS productName FROM " +
            //    "(SELECT productId,ProductVersionsId FROM " +
            //    "(SELECT c.CourseId FROM " +
            //    "(SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er " +
            //    "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
            //    "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId) AS cs " +
            //    "LEFT JOIN Products p ON cs.productId = p.productId " +
            //    "LEFT JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
            //    "GROUP BY pv.ProductVersionsId ORDER BY pv.ProductVersionsId ASC; " +

            //    //Training Hours
            //    "SELECT HoursTraining,COUNT(HoursTraining) AS SelectedHour FROM " +
            //    "(SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er INNER JOIN Courses c ON er.CourseId = c.CourseId GROUP BY HoursTraining;" +

            //    //Course Training Format
            //    "SELECT IFNULL(SUM(CASE WHEN InstructorLed = 1 THEN 1 ELSE 0 END),0) AS InstructorLed,IFNULL(SUM(CASE WHEN `Online` = 1 THEN 1 ELSE 0 END),0) AS `Online` FROM " +
            //    "(SELECT c.CourseId FROM " +
            //    "(SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er " +
            //    "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
            //    "INNER JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId; " +

            //    //Course Training Material
            //    "SELECT IFNULL(SUM(CASE WHEN Autodesk = 1 THEN 1 ELSE 0 END),0) AS Autodesk," +
            //    "IFNULL(SUM(CASE WHEN AAP = 1 THEN 1 ELSE 0 END),0) AS AAP," +
            //    "IFNULL(SUM(CASE WHEN ATC = 1 THEN 1 ELSE 0 END),0) AS ATC," +
            //    "IFNULL(SUM(CASE WHEN Independent = 1 THEN 1 ELSE 0 END),0) AS Independent," +
            //    "IFNULL(SUM(CASE WHEN IndependentOnline = 1 THEN 1 ELSE 0 END),0) AS IndependentOnline," +
            //    "IFNULL(SUM(CASE WHEN ATCOnline = 1 THEN 1 ELSE 0 END),0) AS ATCOnline," +
            //    "IFNULL(SUM(CASE WHEN Other = 1 THEN 1 ELSE 0 END),0) AS Other FROM " +
            //    "(SELECT c.CourseId FROM " +
            //    "(SELECT eva.CourseId FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId ) AS er " +
            //    "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
            //    "INNER JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId;";

            //     //"SELECT StudentEvaluationID as EvalID,EvaluationAnswerJson,EvaluationQuestionCode FROM (SELECT eva.* FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + ") AS er INNER JOIN EvaluationAnswer ea " +
            //     //"ON er.StudentID = ea.StudentId AND er.CourseId = ea.CourseId  where ea.EvaluationAnswerJson IS NOT NULL;";

            ////eva respons - issue 30-10-2018
            ////"SELECT StudentEvaluationID as EvalID,EvaluationAnswerJson,EvaluationQuestionCode FROM (SELECT * FROM EvaluationReport " + query +") AS er INNER JOIN EvaluationAnswer ea "+
            ////"ON er.StudentID = ea.StudentId AND er.CourseId = ea.CourseId where ea.EvaluationAnswerJson IS NOT NULL;";

            try
            {
                int distinctCount = 0;
                List<dynamic> resultQList = new List<dynamic>();
                List<dynamic> RList = new List<dynamic>();
                List<dynamic> resultlist1 = new List<dynamic>();
                sb = new StringBuilder();
                sb.AppendFormat(query1);
                // sb.AppendFormat(fakingmistek);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
               // ds = oDb.getDataSetFromSP(sqlCommand);
             //   ds = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand));

                sb = new StringBuilder();
                sb.AppendFormat(query2);
                MySqlCommand sqlCommand1 = new MySqlCommand(sb.ToString());
               // DataSet ds2 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand1));
                sb = new StringBuilder();
                sb.AppendFormat(query3);
                MySqlCommand sqlCommand2 = new MySqlCommand(sb.ToString());
               // DataSet ds3 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand2));
                sb = new StringBuilder();
                sb.AppendFormat(query4);
                MySqlCommand sqlCommand3 = new MySqlCommand(sb.ToString());
              //  DataSet ds4 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand3));
                sb = new StringBuilder();
                sb.AppendFormat(query5);
                MySqlCommand sqlCommand4 = new MySqlCommand(sb.ToString());
              //  DataSet ds5 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand4));
                sb = new StringBuilder();
                sb.AppendFormat(query6);
                MySqlCommand sqlCommand5 = new MySqlCommand(sb.ToString());
             //   DataSet ds6 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand5));

                
                ds = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand)); 
                DataSet ds2 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand1));
                DataSet ds3 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand2));
                DataSet ds4 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand3));
                DataSet ds5 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand4));
                DataSet ds6 = await Task.Run(() => oDb.getDataSetFromSP(sqlCommand5));

                // dynamicModel = await Task.Run(() => EvalanswerReformaterExcel(ds.Tables[0], TypeName, ds1.Tables[0]));
                //if (ds.Tables.Count != 0)
                //{
                //await Task.Run(() =>
                //{
                //    string queryDistinct = "SELECT Distinct EvaluationQuestionCode FROM (SELECT eva.* FROM EvaluationReport eva inner join Main_site s on s.SiteId =eva.SiteId " + query + ") AS er INNER JOIN EvaluationAnswer ea " +
                //   "ON er.StudentID = ea.StudentId AND er.CourseId = ea.CourseId where ea.EvaluationAnswerJson IS NOT NULL group by EvaluationQuestionCode;";
                //    var evalQuestionCode = "";
                //    sb = new StringBuilder();
                //    sb.AppendFormat(queryDistinct);
                //    sqlCommand = new MySqlCommand(sb.ToString());
                //    DataSet dsDistinct = oDb.getDataSetFromSP(sqlCommand);
                //    if (dsDistinct.Tables[0].Rows.Count != 0)
                //    {
                //        foreach (DataRow row in dsDistinct.Tables[0].Rows)
                //        {
                //            string evalCode = row["EvaluationQuestionCode"].ToString();
                //            if (string.IsNullOrEmpty(evalQuestionCode))
                //            {
                //                evalQuestionCode = "'" + evalCode + "'";
                //            }
                //            else
                //            {
                //                evalQuestionCode += ",'" + evalCode + "'";
                //            }
                //        }
                //    }

                //    string query1 = "SELECT Distinct EvaluationQuestionJson FROM EvaluationQuestion where EvaluationQuestionCode in (" + evalQuestionCode + ") and LanguageiD =35 and Year='" + tmpFYyear + "' group by EvaluationQuestionJson LIMIT 1;";
                //    MySqlCommand sqlCommand1 = new MySqlCommand(query1);
                //    DataSet dsEQ = oDb.getDataSetFromSP(sqlCommand1);
                //    DataTable dd = dsEQ.Tables[0];

                //    string TypeName = "";
                //    if (partnerType == "'1'")
                //    {
                //        TypeName = "ATC";
                //    }
                //    else
                //    {

                //        TypeName = "AAP";

                //    }
                //    dynamicModel1 = dataService.EvalquestionReformater(dd, TypeName);
                //    //result = JsonConvert.SerializeObject(dynamicModel1, Formatting.Indented,
                //    //settings);
                //    dynamicModel = dataService.EvalanswerReformatertwo(ds.Tables[6]);

                //    DataTable dt = dataService.ListToDatatable(dynamicModel);
                //    DataTable dt1 = dataService.ListToDatatable(dynamicModel1);
                //    var columns = dt.Columns.Cast<DataColumn>().Where(c => c.ColumnName != "PID").ToArray();
                //    var query2 = dt.AsEnumerable()
                //      .GroupBy(x => x.Field<string>("PID"));
                //    Dictionary<string, int> qTxtDictionary = new Dictionary<string, int>();
                //    DataTable rdTable = dt.Clone();
                //    foreach (var group in query2)
                //    {
                //        DataRow nRow = rdTable.NewRow();
                //        nRow["PID"] = group.Key;
                //        foreach (var column in columns)
                //        {
                //            nRow[column.ColumnName] = string.Join("|", group.Select(r => r.Field<string>(column, DataRowVersion.Current)));
                //        }
                //        rdTable.Rows.Add(nRow);
                //    }
                //    string[] tmpAll;
                //    List<dynamic> tmpLists = new List<dynamic>();
                //    DataTable dtData = rdTable.Copy();
                //    dtData.Columns.Remove("PID");
                //    var tmplist = default(dynamic);
                //    foreach (DataColumn dc in dtData.Columns)
                //    {
                //        string name = dc.ColumnName;
                //        tmpAll = dtData.Rows[0][dc].ToString().Split('|');
                //        tmplist = tmpAll.GroupBy(x => x)
                //       .OrderByDescending(x => x.Count())
                //       .Select(grp => new
                //       {
                //           ansData = grp.Key,
                //           Count = grp.Count(),
                //           name = dc.ColumnName
                //       }).AsEnumerable().
                //            Cast<dynamic>()
                //           .ToList<dynamic>();
                //        resultlist1.AddRange(tmplist);
                //    }

                //    foreach (DataColumn d in dt1.Columns)
                //    {
                //        // tmpAll = dtData.Rows[0][d].ToString().Split('|');
                //        // IEnumerable<string> uniqueItems = tmpAll.Select(x => x).Distinct<string>();
                //        var dd1 = dt1.AsEnumerable()
                //       .GroupBy(x => x).OrderByDescending(x => x.Count())
                //        .Select(grp => new
                //        {
                //            qdata = dt1.Rows[0][d.ColumnName].ToString(),
                //            name = d.ColumnName
                //        }).ToList();

                //        resultQList.AddRange(dd1);

                //    }



                //     distinctCount = dynamicModel.Distinct().Count();
                //});


                int total_eva = Convert.ToInt32(ds.Tables[0].Rows[0]["TotalEvaluation"]);
                    int facility = Convert.ToInt32(ds.Tables[0].Rows[0]["Facility"]);
                    int computer = Convert.ToInt32(ds.Tables[0].Rows[0]["Computer"]);
                    double facility_percent = 0;
                    double computer_percent = 0;
                    if (total_eva != 0)
                    {
                        facility_percent = (double)Math.Round((double)(100 * facility) / total_eva, 2);
                        computer_percent = (double)Math.Round((double)(100 * computer) / total_eva, 2); 
                    }

                    using (XLWorkbook wb = new XLWorkbook())
                    {
                        var ws = wb.Worksheets.Add("Question 1-5");
                        ws.Column(1).Width = -1;
                        ws.Cell(2, 2).Value = "Evaluation Response Report";
                        ws.Cell(2, 2).Style.Font.Bold = true;
                        ws.Cell(2, 2).Style.Font.FontSize = 16;
                        ws.Range(2, 2, 2, 12).Merge();
                        ws.Cell(5, 2).Value= "Total Evaluation : ";
                        ws.Cell(6, 2).Value = "Onsite Facility : ";
                        ws.Cell(7, 2).Value = "Onsite Computer : ";
                        ws.Cell(5, 3).Value = total_eva;
                        ws.Cell(6, 3).Value = facility + "(" + facility_percent + "%)";
                        ws.Cell(7, 3).Value = computer + "(" + computer_percent + "%)";
                        ws.Cell(7, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        ws.Cell(6, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        ws.Cell(5, 3).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        ws.Cell(9, 2).Value = "1.What level are you going to teach?";
                        ws.Range("B9:C9").Merge();
                        ws.Cell(9, 2).Style.Font.Bold = true;

                        int r = 10; int c = 2; int totalcount = 0;
                        foreach (DataColumn dc in ds2.Tables[0].Columns)
                        {
                            totalcount += Convert.ToInt32(ds2.Tables[0].Rows[0][dc]);
                        }
                        foreach (DataColumn dc in ds2.Tables[0].Columns)
                        {
                            r++;
                            switch (dc.ColumnName)
                            {
                                case "Update":
                                    tmpQuery = "Update";
                                    break;
                                case "Essentials":
                                    tmpQuery = "Level 1: Essentials";
                                    break;
                                case "Intermediate":
                                    tmpQuery = "Level 2: Intermediate";
                                    break;
                                case "Advanced":
                                    tmpQuery = "Level 3: Advanced";
                                    break;
                                case "Customized":
                                    tmpQuery = "Customized";
                                    break;
                                case "Other":
                                    tmpQuery = "Other";
                                    break;
                            }
                            int tmpcount = Convert.ToInt32(ds2.Tables[0].Rows[0][dc]);
                            ws.Cell(r, c).Value = tmpcount + "(" + (double)Math.Round((double)(100 * tmpcount) / totalcount, 2) + "%)";
                            ws.Cell(r, c + 1).Value = tmpQuery;
                        }
                        r += 2;
                        ws.Cell(r, c).Value = "2. What is going to be the primary software used?";
                        ws.Range("B"+r+":C"+r).Merge();
                        ws.Cell(r, c).Style.Font.Bold = true;

                        Int64 total = ds3.Tables[0].AsEnumerable()
                        .Sum(x => x.Field<Int64>("TotalUse"));
                        r++; 
                        foreach (DataRow dr in ds3.Tables[0].Rows)
                        {
                            r++;
                            Int64 tmpcount = Convert.ToInt64(dr["TotalUse"]);
                            ws.Cell(r, c).Value = dr["productName"].ToString(); 
                            ws.Cell(r, c + 1).Value = (double)Math.Round((double)(100 * tmpcount) / total, 2) + "%";
                            ws.Cell(r, c+1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                            ws.Cell(r, c + 2).Value = tmpcount;
                        }
                        r += 2;
                        ws.Cell(r, c).Value = "3. How many hours of training will be delivered?";
                        ws.Range("B" + r + ":C" + r).Merge();
                        ws.Cell(r, c).Style.Font.Bold = true;
                        total = ds4.Tables[0].AsEnumerable()
                        .Sum(x => x.Field<Int64>("SelectedHour"));
                        foreach (DataRow dr in ds4.Tables[0].Rows)
                        {
                            r++;
                            Int64 tmpcount = Convert.ToInt64(dr["SelectedHour"]);
                            ws.Cell(r, c).Value = tmpcount + "(" + (double)Math.Round((double)(100 * tmpcount) / total, 2) + "%)";
                            ws.Cell(r, c + 1).Value = dr["HoursTraining"].ToString();
                            ws.Cell(r, c + 1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                        }
                        r += 2;
                        ws.Cell(r, c).Value = "4. What will be the training format (Check all that apply)?";
                        ws.Range("B" + r + ":C" + r).Merge();
                        ws.Cell(r, c).Style.Font.Bold = true;
                        foreach (DataColumn dc in ds5.Tables[0].Columns)
                        {
                            totalcount += Convert.ToInt32(ds5.Tables[0].Rows[0][dc]);
                        }
                        foreach (DataColumn dc in ds5.Tables[0].Columns)
                        {
                            r++;
                            switch (dc.ColumnName)
                            {
                                case "InstructorLed":
                                    tmpQuery = "Instructor-led in the classroom";
                                    break;
                                case "Online":
                                    tmpQuery = "Online or e-learning";
                                    break;
                            }
                            int tmpcount = Convert.ToInt32(ds5.Tables[0].Rows[0][dc]);
                            ws.Cell(r, c).Value = tmpcount + "(" + (double)Math.Round((double)(100 * tmpcount) / totalcount, 2) + "%)";
                            ws.Cell(r, c + 1).Value = tmpQuery;
                        }


                        r += 2;
                        ws.Cell(r, c).Value = "5. What training materials will you use in this class (Check all that apply)?";
                        ws.Range("B" + r + ":C" + r).Merge();
                        ws.Cell(r, c).Style.Font.Bold = true;
                        foreach (DataColumn dc in ds6.Tables[0].Columns)
                        {
                            totalcount += Convert.ToInt32(ds6.Tables[0].Rows[0][dc]);
                        }
                        foreach (DataColumn dc in ds6.Tables[0].Columns)
                        {
                            r++;
                            switch (dc.ColumnName)
                            {
                                case "Autodesk":
                                    tmpQuery = "Autodesk Official Courseware (any Autodesk branded material)";
                                    break;
                                case "AAP":
                                    tmpQuery = "Autodesk Authors and Publishers (AAP) Program Courseware";
                                    break;
                                case "ATC":
                                    tmpQuery = "Course material developed by the ATC";
                                    break;
                                case "Independent":
                                    tmpQuery = "Course material developed by an independent vendor or instructor";
                                    break;
                                case "IndependentOnline":
                                    tmpQuery = "Online e-learning course material developed by an independent vendor or instructor";
                                    break;
                                case "ATCOnline":
                                    tmpQuery = "Online e-learning course material developed by the ATC";
                                    break;
                                case "Other":
                                    tmpQuery = "Other";
                                    break;
                            }
                            int tmpcount = Convert.ToInt32(ds6.Tables[0].Rows[0][dc]);
                            ws.Cell(r, c).Value = tmpcount + "(" + (double)Math.Round((double)(100 * tmpcount) / totalcount, 2) + "%)";
                            ws.Cell(r, c + 1).Value = tmpQuery;
                        }
                        //int qNumber = 6;
                        //for (var q = 0; q < resultQList.Count; q++)
                        //{
                        //    string dddd = resultQList[q].name;
                        //    r += 2;
                        //    var duplicates = resultlist1.Where(x => x.name.Equals(resultQList[q].name)).ToList();
                        //  //  RList = resultlist1.Where(x => x.name.Equals(resultQList[q].name)).ToList();
                        //    // RList = resultlist1.Where(x => resultQList[q].name).ToList();
                        //    // RList = resultlist1.FindAll(resultQList[q].name);
                        //    ws.Cell(r, c).Value = qNumber + "."+ resultQList[q].qdata;
                        //    ws.Range("B" + r + ":C" + r).Merge();
                        //    ws.Cell(r, c).Style.Font.Bold = true;
                        //    int cal = 0;
                        //    for (var w = 0; w < duplicates.Count; w++)
                        //    {
                        //        r++;
                        //        if (cal == 0) cal = duplicates[w].Count;
                        //        var countcal = (double)Math.Round((double)(100 * cal) / distinctCount, 2);
                        //            ws.Cell(r, c).Value = cal + " (" + countcal + "%)";
                        //            ws.Cell(r, c+1).Value = duplicates[w].ansData;
                        //        cal = 0;
                        //    }
                        //    qNumber++;
                        //}


                        var cols = ws.Columns();
                        cols.Style.Alignment.WrapText = true;
                        cols.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                        cols.AdjustToContents(1, 5, 40);

                        using (MemoryStream stream = new MemoryStream())
                        {
                            wb.SaveAs(stream);
                            string name = "filedownload.xlsx";
                            var contentType = new MediaTypeHeaderValue("application/vnd.ms-excel");

                            //var _httpContextAccessor = new HttpContextAccessor();
                            //_httpContextAccessor.HttpContext.Response.ContentType = "application/vnd.ms-excel";//application/octet-stream
                            //_httpContextAccessor.HttpContext.Response.Headers.Add("Access-Control-Expose-Headers", "Content-Disposition");

                            return new FileContentResult(stream.ToArray(), contentType) //"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                            {
                                FileDownloadName = name
                            };
                        }
                    }



               // }
                

            }
            catch (Exception ex)
            {
                return null;
            }
            
        }

        [HttpPost("EvaluationResponseAnswerNew")]
        public async Task<IActionResult> EvaluationResponseAnswerNew([FromBody] dynamic data)
        {
            if (data != null)
            {
                 var partnerType = 1;
                string jsonString = data.ToString();
                var _json = JsonConvert.DeserializeObject<JObject>(jsonString);
                foreach (JProperty property in _json.Properties())
                {

                    if (property.Name.Equals("filterByPartnerType"))
                    {
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            var regex = new Regex("\\d+");
                            var match = regex.Match(property.Value.ToString());
                            if (match.Success&&!string.IsNullOrEmpty(match.Groups[0].Value))
                            {
                                partnerType = int.Parse(match.Groups[0].Value);
                                break;
                            }
                          
                        }
                    }

                }

                var evalReportFilter = new EvaluationReportFilter
                {
                    FilterString = data.ToString(),
                    LastUpdated = DateTime.Now,
                    PartnerType = partnerType,
                    ReportName = "Evaluation Response Report",
                    Status = "New",
                    Submiter = HttpContext.User.GetUserId(),
                    SumitedDate = DateTime.Now
                };

                db.EvaluationReportFilter.Add(evalReportFilter);
                if (db.SaveChanges() > 0)
                {
                    object[] objectArray;
                    string fyYear = "";
                    string _query = "";
                    var tmpQuestion = ""; var tmpAnswer = "";
                    //var partnerType = string.Empty;
                    int i = 0;
                    JObject json = JObject.FromObject(data);
                    List<dynamic> dynamicModel = new List<dynamic>();
                    List<dynamic> dynamicModel1 = new List<dynamic>();
                    List<dynamic> resultlist1 = new List<dynamic>();
                    foreach (JProperty property in json.Properties())
                    {
                        if (property.Name.Equals("filterByGeo"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "GeoCode in (" + property.Value.ToString() + ") ";
                                i = i + 1;
                            }
                        }
                        if (property.Name.Equals("filterByCountry"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "CountryCode in (" + property.Value.ToString() + ") ";
                                i = i + 1;
                            }
                        }
                        if (property.Name.Equals("filterBySurveyYear"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "FYIndicatorKey = '" + property.Value.ToString() + "' ";
                                fyYear = property.Value.ToString();
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("filterByDate"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }

                                if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                                {
                                    _query += "Month = " + property.Value.ToString() + " ";
                                }
                                else
                                {
                                    switch (property.Value.ToString())
                                    {
                                        case "q1":
                                            _query += "Quarter = 1 ";
                                            break;
                                        case "q2":
                                            _query += "Quarter = 2 ";
                                            break;
                                        case "q3":
                                            _query += "Quarter = 3 ";
                                            break;
                                        case "q4":
                                            _query += "Quarter = 4 ";
                                            break;
                                    }
                                }

                                i = i + 1;
                            }
                        }
                        if (property.Name.Equals("filterBySiteStatus"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "`Status` = '" + property.Value.ToString() + "'";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("filterByPartnerType"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "RoleId in (" + property.Value.ToString() + ")";
                                var regex = new Regex("\\d+");
                                var match = regex.Match(property.Value.ToString());
                                if (match.Success)
                                {
                                    partnerType = int.Parse(match.Groups[0].Value);
                                }
                                i = i + 1;
                            }
                        }
                        if (property.Name.Equals("filterByCertificateType"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "CertificateType in (" + property.Value.ToString() + ")";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("ContactId"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "ContactId = '" + property.Value.ToString() + "'";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("OrgId"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "SiteId IN (SELECT SiteId FROM Main_site WHERE OrgId IN (" + property.Value.ToString() + "))";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("SiteId"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "SiteId IN (" + property.Value.ToString() + ")";
                                i = i + 1;
                            }
                        }

                        if (property.Name.Equals("Distributor"))
                        {
                            if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                            {
                                if (i == 0) { _query += " WHERE "; }
                                if (i > 0) { _query += " AND "; }
                                _query += "CountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + property.Value.ToString() + " ))";
                                i = i + 1;
                            }
                        }
                    }

                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());
                    settings.ContractResolver = new NullToEmptyStringResolver();

                    var fakingmistek = "SELECT StudentEvaluationID as EvalID,EvaluationAnswerJson,EvaluationQuestionCode FROM (SELECT * FROM EvaluationReport " + _query + ") AS er INNER JOIN EvaluationAnswer ea " +
                          "ON er.StudentID = ea.StudentId AND er.CourseId = ea.CourseId where ea.EvaluationAnswerJson IS NOT NULL;";

                    try
                    {
                        var sb = new StringBuilder();
                        sb.AppendFormat(fakingmistek);
                        var sqlCommand = new MySqlCommand(sb.ToString());
                        var ds = oDb.getDataSetFromSP(sqlCommand);
                        string queryDistinct = "SELECT Distinct EvaluationQuestionCode FROM (SELECT * FROM EvaluationReport " + _query + ") AS er INNER JOIN EvaluationAnswer ea " +
                       "ON er.StudentID = ea.StudentId AND er.CourseId = ea.CourseId where ea.EvaluationAnswerJson IS NOT NULL group by EvaluationQuestionCode;";
                        var evalQuestionCode = "";
                        sb = new StringBuilder();
                        sb.AppendFormat(queryDistinct);
                        sqlCommand = new MySqlCommand(sb.ToString());
                        DataSet dsDistinct = oDb.getDataSetFromSP(sqlCommand);
                        if (dsDistinct.Tables[0].Rows.Count != 0)
                        {
                            DataTable dt1 = dsDistinct.Tables[0];
                            foreach (DataRow row in dt1.Rows)
                            {
                                string evalCode = row["EvaluationQuestionCode"].ToString();
                                if (string.IsNullOrEmpty(evalQuestionCode))
                                {
                                    evalQuestionCode = "'" + evalCode + "'";
                                }
                                else
                                {
                                    evalQuestionCode += ",'" + evalCode + "'";
                                }
                                //if (!string.IsNullOrEmpty(evalQuestionCode))
                                //{
                                //    break;
                                //}
                            }


                            string query1 = "SELECT Distinct EvaluationQuestionJson FROM EvaluationQuestion where EvaluationQuestionCode in (" + evalQuestionCode + ") and LanguageiD =35 and Year='" + fyYear + "' group by EvaluationQuestionJson LIMIT 1;";
                            MySqlCommand sqlCommand1 = new MySqlCommand(query1);
                            DataSet dsEQ = oDb.getDataSetFromSP(sqlCommand1);
                            DataTable dd = dsEQ.Tables[0];

                            string TypeName = string.Empty;
                            if (partnerType == 1)
                            {
                                TypeName = "ATC";
                            }
                            else
                            {

                                TypeName = "AAP";

                            }
                            dynamicModel1 = dataService.EvalquestionReformater(dd, TypeName);
                            dynamicModel = dataService.EvalanswerReformatertwo(ds.Tables[0], dd);
                            DataTable dt = dataService.ListToDatatable(dynamicModel);
                            DataTable dtQ = dataService.ListToDatatable(dynamicModel1);
                            var columns = dt.Columns.Cast<DataColumn>().Where(c => c.ColumnName != "PID").ToArray();
                            var queryA = dt.AsEnumerable()
                              .GroupBy(x => x.Field<string>("PID"));
                            Dictionary<string, int> qTxtDictionary = new Dictionary<string, int>();
                            DataTable rdTable = dt.Clone();
                            foreach (var group in queryA)
                            {
                                DataRow nRow = rdTable.NewRow();
                                nRow["PID"] = group.Key;
                                foreach (var column in columns)
                                {
                                    nRow[column.ColumnName] = string.Join("|", group.Select(r => r.Field<string>(column, DataRowVersion.Current)));
                                }
                                rdTable.Rows.Add(nRow);
                            }
                            string[] tmpAll;
                            List<dynamic> tmpLists = new List<dynamic>();

                            //var tmplist = (dynamic)null;
                            DataTable dtData = rdTable.Copy();
                            dtData.Columns.Remove("PID");
                            var tmplist = default(dynamic);
                            //  Dictionary<string, string> temp = new Dictionary<string, string>();
                            foreach (DataColumn dc in dtData.Columns)
                            {
                                string name = dc.ColumnName;
                                tmpAll = dtData.Rows[0][dc].ToString().Split('|');
                                //IEnumerable<string> uniqueItems = tmpAll.Select(x => x).Distinct<string>();
                                tmplist = tmpAll.GroupBy(x => x)
                               .OrderByDescending(x => x.Count())
                               .Select(grp => new
                               {
                                   ansData = grp.Key,
                                   Count = grp.Count(),
                                   name = dc.ColumnName
                               }).AsEnumerable().
                                    Cast<dynamic>()
                                   .ToList<dynamic>();

                                resultlist1.AddRange(tmplist);

                            }
                            List<dynamic> resultQList = new List<dynamic>();
                            foreach (DataColumn d in dtQ.Columns)
                            {
                                // tmpAll = dtData.Rows[0][d].ToString().Split('|');
                                // IEnumerable<string> uniqueItems = tmpAll.Select(x => x).Distinct<string>();
                                var tmpQ = dtQ.AsEnumerable()
                               .GroupBy(x => x).OrderByDescending(x => x.Count())
                                .Select(grp => new
                                {
                                    qdata = dtQ.Rows[0][d.ColumnName].ToString(),
                                    name = d.ColumnName
                                }).ToList();

                                resultQList.AddRange(tmpQ);

                            }
                            int distinctCount = dynamicModel.Distinct().Count();
                            //tmpQuestion = JsonConvert.SerializeObject(resultQList, Formatting.Indented,
                            //    settings);
                            //tmpAnswer = JsonConvert.SerializeObject(resultlist1, Formatting.Indented,
                            //    settings);
                            using (XLWorkbook wb = new XLWorkbook())
                            {
                                var ws = wb.Worksheets.Add("Question 6-above");
                                ws.Column(1).Width = -1;
                                ws.Cell(2, 2).Value = "Evaluation Response Report";
                                ws.Cell(2, 2).Style.Font.Bold = true;
                                ws.Cell(2, 2).Style.Font.FontSize = 16;
                                ws.Range(2, 2, 2, 12).Merge();
                                int qNumber = 6; int r = 5; int c = 2;
                                for (var q = 0; q < resultQList.Count; q++)
                                {
                                    string dddd = resultQList[q].name;

                                    var duplicates = resultlist1.Where(x => x.name.Equals(resultQList[q].name)).ToList();
                                    //  RList = resultlist1.Where(x => x.name.Equals(resultQList[q].name)).ToList();
                                    // RList = resultlist1.Where(x => resultQList[q].name).ToList();
                                    // RList = resultlist1.FindAll(resultQList[q].name);
                                    ws.Cell(r, c).Value = qNumber + "." + resultQList[q].qdata;
                                    ws.Range("B" + r + ":C" + r).Merge();
                                    ws.Cell(r, c).Style.Font.Bold = true;
                                    int cal = 0;
                                    for (var w = 0; w < duplicates.Count; w++)
                                    {
                                        r++;
                                        if (cal == 0) cal = duplicates[w].Count;
                                        var countcal = (double)Math.Round((double)(100 * cal) / distinctCount, 2);
                                        ws.Cell(r, c).Value = cal + " (" + countcal + "%)";
                                        ws.Cell(r, c + 1).Value = duplicates[w].ansData;
                                        cal = 0;
                                    }
                                    qNumber++;
                                    r += 2;
                                }


                                var cols = ws.Columns();
                                cols.Style.Alignment.WrapText = true;
                                cols.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
                                cols.AdjustToContents(1, 5, 40);
                                
                                using (MemoryStream stream = new MemoryStream())
                                {
                                    wb.SaveAs(stream);
                                    string name = "filedownload.xlsx";
                                    var contentType = new MediaTypeHeaderValue("application/vnd.ms-excel");
                                    return new FileContentResult(stream.ToArray(), contentType) //"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                    {
                                        FileDownloadName = name
                                    };

                                }
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        _auditLogServices.LogHistory(new History { Admin = "System", Date = DateTime.Now, Description = ex.Message, Id = partnerType.ToString() });

                        await _emailService.SendMailAsync(AppConfig.Config["EmailSender:SupervisorEmail"], "Error when generate Evaluation Response Report ", string.Format("Hi, {0}<br/>An error occur when generate the Evaluation Response Report<br/>Error message:{1}", AppConfig.Config["EmailSender:SupervisorEmail"], ex.Message), null, null);
                    }

                    return Ok();
                }
            }
            return BadRequest();

        }



        [HttpPost("EvaluationResponse")]
        public IActionResult EvaResponse([FromBody] dynamic data)
        {
            object[] objectArray;
            string query = "";
            string tmpQuery = "";
            string eventType = "";
			var partnerType="";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.GeoCode in (" + property.Value.ToString() + ") ";                       
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountry"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CountryCode in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {

                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "eva.Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "eva.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "eva.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "eva.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "eva.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySiteStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "s.`Status` = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.RoleId in (" + property.Value.ToString() + ")";
                        partnerType = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CertificateType in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.ContactId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.SiteId IN (SELECT SiteId FROM Main_site WHERE OrgId IN (" + property.Value.ToString() + "))";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.SiteId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "eva.CountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + property.Value.ToString() + " ))";
                        i = i + 1;
                    }
                }
            }

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("EventType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { eventType += " WHERE "; }
                        if (i > 0) { eventType += " AND "; }
                        eventType += " c.EventType2 IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }
            var settings = new JsonSerializerSettings
            {

            };
            settings.Converters.Add(new DataSetConverter());
            settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
            //Course equipment
            fakingmistek +=
                //Course Equipment
                "SELECT COUNT(c.CourseId) AS TotalEvaluation, IFNULL(SUM(CASE WHEN ATCFacility = 1 THEN 1 ELSE 0 END),0) AS Facility, IFNULL(SUM(CASE WHEN ATCComputer = 1 THEN 1 ELSE 0 END),0) AS Computer  FROM " +
                "(SELECT eva.CourseId FROM EvaluationReport eva   INNER JOIN EvaluationAnswer ea ON eva.CourseId = ea.CourseId AND eva.StudentID = ea.StudentId inner join Main_site s on s.SiteId =eva.SiteId  " + query + " group by eva.studentId,eva.CourseId) AS er INNER JOIN Courses c ON er.CourseId = c.CourseId " + eventType + "; " +


                //Course Teaching Level
                "SELECT IFNULL(SUM(CASE WHEN `Update` = 1 THEN 1 ELSE 0 END),0) AS `Update`," +
                "IFNULL(SUM(CASE WHEN Essentials = 1 THEN 1 ELSE 0 END),0) AS Essentials," +
                "IFNULL(SUM(CASE WHEN Intermediate = 1 THEN 1 ELSE 0 END),0) AS Intermediate," +
                "IFNULL(SUM(CASE WHEN Advanced = 1 THEN 1 ELSE 0 END),0) AS Advanced," +
                "IFNULL(SUM(CASE WHEN Customized = 1 THEN 1 ELSE 0 END),0) AS Customized," +
                "IFNULL(SUM(CASE WHEN Other = 1 THEN 1 ELSE 0 END),0) AS Other FROM " +
                "(SELECT c.CourseId FROM (SELECT eva.CourseId FROM EvaluationReport eva inner join EvaluationAnswer ea on eva.CourseId =ea.CourseId and eva.StudentID =ea.StudentId  inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId,eva.CourseId ) AS er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
                "INNER JOIN CourseTeachingLevel ctl ON ctl.CourseId = c.CourseId; " +

                //Course Software
                "SELECT COUNT(pv.productId) AS TotalUse,CONCAT(productName,' ',Version) AS productName FROM " +
                "(SELECT productId,ProductVersionsId FROM " +
                "(SELECT c.CourseId FROM " +
                "(SELECT eva.CourseId FROM EvaluationReport eva inner join EvaluationAnswer ea on eva.CourseId =ea.CourseId and eva.StudentID =ea.StudentId  inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId,eva.CourseId ) AS er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
                "INNER JOIN CourseSoftware cs ON c.CourseId = cs.CourseId) AS cs " +
                "LEFT JOIN Products p ON cs.productId = p.productId " +
                "LEFT JOIN ProductVersions pv ON cs.ProductVersionsId = pv.ProductVersionsId " +
                "GROUP BY pv.ProductVersionsId ORDER BY pv.ProductVersionsId ASC; " +

                //Training Hours
                "SELECT HoursTraining,COUNT(HoursTraining) AS SelectedHour FROM " +
                "(SELECT eva.CourseId FROM EvaluationReport eva inner join EvaluationAnswer ea on eva.CourseId =ea.CourseId and eva.StudentID =ea.StudentId  inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId,eva.CourseId ) AS er INNER JOIN Courses c ON er.CourseId = c.CourseId GROUP BY HoursTraining;" +

                //Course Training Format
                "SELECT IFNULL(SUM(CASE WHEN InstructorLed = 1 THEN 1 ELSE 0 END),0) AS InstructorLed,IFNULL(SUM(CASE WHEN `Online` = 1 THEN 1 ELSE 0 END),0) AS `Online` FROM " +
                "(SELECT c.CourseId FROM " +
                "(SELECT eva.CourseId FROM EvaluationReport eva inner join EvaluationAnswer ea on eva.CourseId =ea.CourseId and eva.StudentID =ea.StudentId  inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId,eva.CourseId ) AS er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
                "INNER JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId; " +

                //Course Training Material
                "SELECT IFNULL(SUM(CASE WHEN Autodesk = 1 THEN 1 ELSE 0 END),0) AS Autodesk," +
                "IFNULL(SUM(CASE WHEN AAP = 1 THEN 1 ELSE 0 END),0) AS AAP," +
                "IFNULL(SUM(CASE WHEN ATC = 1 THEN 1 ELSE 0 END),0) AS ATC," +
                "IFNULL(SUM(CASE WHEN Independent = 1 THEN 1 ELSE 0 END),0) AS Independent," +
                "IFNULL(SUM(CASE WHEN IndependentOnline = 1 THEN 1 ELSE 0 END),0) AS IndependentOnline," +
                "IFNULL(SUM(CASE WHEN ATCOnline = 1 THEN 1 ELSE 0 END),0) AS ATCOnline," +
                "IFNULL(SUM(CASE WHEN Other = 1 THEN 1 ELSE 0 END),0) AS Other FROM " +
                "(SELECT c.CourseId FROM " +
                "(SELECT eva.CourseId FROM EvaluationReport eva inner join EvaluationAnswer ea on eva.CourseId =ea.CourseId and eva.StudentID =ea.StudentId  inner join Main_site s on s.SiteId =eva.SiteId " + query + " group by eva.StudentId,eva.CourseId ) AS er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId" + eventType + ") AS c " +
                "INNER JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId;";

                //eva respons - issue 30-10-2018
                //"SELECT StudentEvaluationID as EvalID,EvaluationAnswerJson,EvaluationQuestionCode FROM (SELECT * FROM EvaluationReport " + query +") AS er INNER JOIN EvaluationAnswer ea "+
                //"ON er.StudentID = ea.StudentId AND er.CourseId = ea.CourseId where ea.EvaluationAnswerJson IS NOT NULL;";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(fakingmistek);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables.Count != 0)
                {
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);

                    //Course Equipment
                    //  result = (ds.Tables[0].Rows.Count != 0) ? MySqlDb.GetJSONObjectString(ds.Tables[0]) : null;
                    var d1=dataService.ToDynamic(ds.Tables[0]);
                   result = JsonConvert.SerializeObject(d1, Formatting.Indented,
                       settings);
                    //Course Teaching Level
                    //result1 = (ds.Tables[1].Rows.Count != 0) ? MySqlDb.GetJSONObjectString(ds.Tables[1]) : null;
                    var d2=dataService.ToDynamic(ds.Tables[1]);
                    result1 = JsonConvert.SerializeObject(d2, Formatting.Indented,
                        settings);
                    //Course Software
                   // result2 = (ds.Tables[2].Rows.Count != 0) ? MySqlDb.GetJSONArrayString(ds.Tables[2]) : "null";
                   var d3=dataService.ToDynamic(ds.Tables[2]);
                   result2 = JsonConvert.SerializeObject(d3, Formatting.Indented,
                       settings);
                    //Training Hours
                   // result3 = (ds.Tables[3].Rows.Count != 0) ? MySqlDb.GetJSONArrayString(ds.Tables[3]) : "null";
                   var d4=dataService.ToDynamic(ds.Tables[3]);
                   result3 = JsonConvert.SerializeObject(d4, Formatting.Indented,
                       settings);

                    //Course Training Format
                    //result4 = (ds.Tables[4].Rows.Count != 0) ? MySqlDb.GetJSONObjectString(ds.Tables[4]) : null;
                    var d5=dataService.ToDynamic(ds.Tables[4]);
                    result4 = JsonConvert.SerializeObject(d5, Formatting.Indented,
                        settings);

                    //Course Training Material
                    //result5 = (ds.Tables[5].Rows.Count != 0) ? MySqlDb.GetJSONObjectString(ds.Tables[5]) : null;
                    var d6=dataService.ToDynamic(ds.Tables[5]);
                    result5 = JsonConvert.SerializeObject(d6, Formatting.Indented,
                        settings);
					
                }

                //eva respons - issue 30-10-2018
                var evalQuestionCode = "";
                List<dynamic> dynamicModel = new List<dynamic>();
                //IDataServices dataService = new DataServices();
                //if (ds.Tables[6].Rows.Count != 0)
                //{
                //    try
                //    {
                //        //foreach (DataRow dataRow in ds.Tables[6].Rows)
                //        //{
                //        //    var dr = JsonConvert.DeserializeObject(dataRow["EvaluationAnswerJson"].ToString());
                //        //    dynamicModel.Add(dr);
                //        //}

                //        //var dm = dynamicModel;
                //    }
                //    catch (Exception e)
                //    {
                //        Console.WriteLine(e);
                //        throw;
                //    }
                //    //result6 = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                //    //settings);


                //}



            }
            catch(Exception e)
            {
                if(ds.Tables.Count == 0){
                    finalresult = "Not Found";
                }
            }
            finally
            {

                //string[] temp = {result,result1,result2,result3,result4,result5,result6};
                string[] temp = {result,result1,result2,result3,result4,result5};
               //finalresult = string.Join(",",temp);
              
               // if(finalresult == ",,,,,,"){
               //     finalresult =
               //         "{"+
               //         "\"TotalEvaluation\":\"0\""+
               //         "}";
               // }
                
                objectArray = temp.Cast<object>().ToArray();
            }
       
            return Ok(objectArray);
        }

        [HttpPost("EvaluationResponseAnswer")]
        public IActionResult EvaluationResponseAnswer([FromBody] dynamic data)
        {
            object[] objectArray;
            string fyYear = "";
            string query = "";
            var tmpQuestion = ""; var tmpAnswer = "";
            var partnerType = "";
            int i = 0;
            JObject json = JObject.FromObject(data);
            List<dynamic> dynamicModel = new List<dynamic>();
            List<dynamic> dynamicModel1 = new List<dynamic>();
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "GeoCode in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("filterByCountry"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CountryCode in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        fyYear = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }
                if (property.Name.Equals("filterBySiteStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "`Status` = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "RoleId in (" + property.Value.ToString() + ")";
                        partnerType = property.Value.ToString();
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("filterByCertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CertificateType in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("ContactId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "ContactId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "SiteId IN (SELECT SiteId FROM Main_site WHERE OrgId IN (" + property.Value.ToString() + "))";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "SiteId IN (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + property.Value.ToString() + " ))";
                        i = i + 1;
                    }
                }
            }

            var settings = new JsonSerializerSettings
            {

            };
            settings.Converters.Add(new DataSetConverter());
            settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

            fakingmistek += "SELECT StudentEvaluationID as EvalID,EvaluationAnswerJson,EvaluationQuestionCode FROM (SELECT * FROM EvaluationReport " + query + ") AS er INNER JOIN EvaluationAnswer ea " +
                "ON er.StudentID = ea.StudentId AND er.CourseId = ea.CourseId where ea.EvaluationAnswerJson IS NOT NULL;";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(fakingmistek);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                string queryDistinct = "SELECT Distinct EvaluationQuestionCode FROM (SELECT * FROM EvaluationReport " + query + ") AS er INNER JOIN EvaluationAnswer ea " +
               "ON er.StudentID = ea.StudentId AND er.CourseId = ea.CourseId where ea.EvaluationAnswerJson IS NOT NULL group by EvaluationQuestionCode;";
                var evalQuestionCode = "";
                sb = new StringBuilder();
                sb.AppendFormat(queryDistinct);
                sqlCommand = new MySqlCommand(sb.ToString());
                DataSet dsDistinct = oDb.getDataSetFromSP(sqlCommand);
                if (dsDistinct.Tables[0].Rows.Count != 0)
                {
                    DataTable dt1 = dsDistinct.Tables[0];
                    foreach (DataRow row in dt1.Rows)
                    {
                       string evalCode = row["EvaluationQuestionCode"].ToString();
                        if (string.IsNullOrEmpty(evalQuestionCode))
                        {
                            evalQuestionCode = "'" + evalCode + "'";
                        }
                        else
                        {
                            evalQuestionCode += ",'" + evalCode + "'";
                        }
                        //if (!string.IsNullOrEmpty(evalQuestionCode))
                        //{
                        //    break;
                        //}
                    }
                  
                    try
                    {
                        string query1 = "SELECT Distinct EvaluationQuestionJson FROM EvaluationQuestion where EvaluationQuestionCode in (" + evalQuestionCode + ") and LanguageiD =35 and Year='"+ fyYear + "' group by EvaluationQuestionJson LIMIT 1;";
                        MySqlCommand sqlCommand1 = new MySqlCommand(query1);
                        DataSet dsEQ = oDb.getDataSetFromSP(sqlCommand1);
                        DataTable dd = dsEQ.Tables[0];

                        string TypeName = "";
                        if (partnerType == "'1'")
                        {
                            TypeName = "ATC";
                        }
                        else
                        {

                            TypeName = "AAP";

                        }
                        dynamicModel1 = dataService.EvalquestionReformater(dd, TypeName);
                        result = JsonConvert.SerializeObject(dynamicModel1, Formatting.Indented,
                        settings);
                        //tmpQuestion = JsonConvert.SerializeObject(dynamicModel1, Formatting.Indented,
                        //settings);
                        dynamicModel = dataService.EvalanswerReformatertwo(ds.Tables[0],dd);

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        throw;
                    }
                    result2 = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
                    


                }
            }
            catch (Exception e)
            {
                if (ds.Tables.Count == 0)
                {
                    finalresult = "Not Found";
                }
            }
            finally
            {
                List<dynamic> resultlist1 = new List<dynamic>();
               
                DataTable dt = dataService.ListToDatatable(dynamicModel);
                DataTable dt1 = dataService.ListToDatatable(dynamicModel1);
    //            

                
                var columns = dt.Columns.Cast<DataColumn>().Where(c => c.ColumnName != "PID").ToArray();
                var query1 = dt.AsEnumerable()
                  .GroupBy(x => x.Field<string>("PID"));
                Dictionary<string, int> qTxtDictionary = new Dictionary<string, int>();
                DataTable rdTable = dt.Clone();
                foreach (var group in query1)
                {
                    DataRow nRow = rdTable.NewRow();
                    nRow["PID"] = group.Key;
                    foreach (var column in columns)
                    {
                        nRow[column.ColumnName] = string.Join("|", group.Select(r => r.Field<string>(column, DataRowVersion.Current)));
                    }
                    rdTable.Rows.Add(nRow);
                }
                string[] tmpAll;
                List<dynamic> tmpLists = new List<dynamic>();
                DataTable dtData = rdTable.Copy();
                dtData.Columns.Remove("PID");
                var tmplist = default(dynamic);
                foreach (DataColumn dc in dtData.Columns)
                {
                    string name = dc.ColumnName;
                    tmpAll = dtData.Rows[0][dc].ToString().Split('|');
                    tmplist = tmpAll.GroupBy(x => x)
                   .OrderByDescending(x => x.Count())
                   .Select(grp => new
                   {
                       ansData = grp.Key,
                       Count = grp.Count(),
                       name = dc.ColumnName
                   }).AsEnumerable().
                        Cast<dynamic>()
                       .ToList<dynamic>();

                    resultlist1.AddRange(tmplist);

                }
               

                List<dynamic> resultQList =new List<dynamic>();
                foreach (DataColumn d in dt1.Columns)
                {

                    var dd = dt1.AsEnumerable()
                   .GroupBy(x => x).OrderByDescending(x => x.Count())
                    .Select(grp => new
                    {
                        data = dt1.Rows[0][d.ColumnName].ToString(),
                        name = d.ColumnName
                    }).ToList();

                    resultQList.AddRange(dd);

                }
                tmpQuestion = JsonConvert.SerializeObject(resultQList, Formatting.Indented,
                    settings);
                tmpAnswer = JsonConvert.SerializeObject(resultlist1, Formatting.Indented,
                    settings);

              
                int distinctCount = dynamicModel.Distinct().Count();


               
                string[] temp = { tmpQuestion, tmpAnswer, dt.Rows.Count.ToString() };
                //finalresult = string.Join(",", temp);

                //if (finalresult == ",,,,,,")
                //{
                //    finalresult =
                //        "{" +
                //        "\"TotalEvaluation\":\"0\"" +
                //        "}";
                //}
                objectArray = temp.Cast<object>().ToArray();
            }
            return Ok(objectArray);
        }
       
        [HttpGet("SiteInfo/{site_id}")]
        public IActionResult InfoSite(string site_id)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT DISTINCT SiteName,countries_name FROM Main_site s INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code WHERE SiteId = '" + site_id + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("SiteDetailPerf/{obj}")]
        public IActionResult SiteDetailPerformance(string obj)
        {
            var o1 = JObject.Parse(obj);

            string query = "";

            query +=
                "SELECT s.SiteId,SiteName,countries_name,COUNT(EvaluationReportId) AS Total,SAPShipTo_retired," +
                "CASE WHEN SUM( er.NbAnswerCCM * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( er.ScoreCCM ) / SUM( er.NbAnswerCCM * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END CCM," +
                "CASE WHEN SUM( er.NbAnswerFR * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( er.ScoreFR ) / SUM( er.NbAnswerFR * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END FR," +
                "CASE WHEN SUM( er.NbAnswerIQ * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( er.ScoreIQ ) / SUM( er.NbAnswerIQ * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END IQ," +
                "CASE WHEN SUM( er.NbAnswerOP * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( er.ScoreOP ) / SUM( er.NbAnswerOP * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END OP," +
                "CASE WHEN SUM( er.NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( er.ScoreOE ) / SUM( er.NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END OE " +
                "FROM EvaluationReport er " +
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = ea.CourseId AND er.StudentID = ea.StudentId " +
                "INNER JOIN tblStudents st ON ea.StudentId = st.StudentID " +
                "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
                "INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code "+
                "INNER JOIN Courses c on er.CourseId = c.CourseId " +
                "INNER JOIN CourseSoftware cs on c.CourseId = cs.CourseId";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId = '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() != "ALL"))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if(property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4"){
                            query += "er.Month = " + property.Value.ToString() + " ";
                        } else{
                            switch(property.Value.ToString()){
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }
                        
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySiteStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.`Status` = '"+property.Value.ToString()+"'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId in ('"+property.Value.ToString()+"')";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CertificateType in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CourseId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CourseId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Quarter"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if(property.Value.ToString() == "q1" || property.Value.ToString() == "q2" || property.Value.ToString() == "q3" || property.Value.ToString() == "q4"){
                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            switch(property.Value.ToString()){
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ProductID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "cs.productId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
            }

            query += " GROUP BY s.SiteId";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("SiteInstructorPerf/{obj}")]
        public IActionResult SiteInstructorPerformance(string obj)
        {
            var o1 = JObject.Parse(obj);

            string query = "";

            query +=
                "SELECT ca.ContactId,InstructorId, COUNT(EvaluationReportId) AS Evals," +
                "CASE WHEN SUM(NbAnswerIQ*100.00) = 0 THEN 0.00 ELSE CAST(SUM(ScoreIQ)/ SUM(NbAnswerIQ*100.00) * 100 AS DECIMAL(7,2)) END WDT " +
                "FROM EvaluationReport er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                "INNER JOIN CourseSoftware cs on c.CourseId = cs.CourseId";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId = '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if(property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4"){
                            query += "er.Month = " + property.Value.ToString() + " ";
                        } else{
                            switch(property.Value.ToString()){
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }
                        
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySiteStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.`Status` = '"+property.Value.ToString()+"'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.RoleId in ('"+property.Value.ToString()+"')";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.CertificateType in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("InstructorId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "ca.InstructorId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("ProductID"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "cs.productId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

            }

            query += " GROUP BY InstructorId ORDER BY Evals DESC";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpGet("SiteProfile/{obj}")]
        public IActionResult SiteProfile(string obj)
        {
            var o1 = JObject.Parse(obj);

            string query = "";

            query +=
                "SELECT SiteIdInt,SiteName,ca.CountryCode,ca.SiteId,ContactIdInt,ca.ContactId,ContactName,COUNT(StudentID) AS Evals,ca.CourseId," +
                "CASE WHEN SUM( NbAnswerFR * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreFR ) / SUM( NbAnswerFR * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 ) ) END FR," +
                "CASE WHEN SUM( NbAnswerIQ * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreIQ ) / SUM( NbAnswerIQ * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END IQ," +
                "CASE WHEN SUM( NbAnswerCCM * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreCCM ) / SUM( NbAnswerCCM * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END CCM," +
                "CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END OE," +
                "CASE WHEN SUM( NbAnswerOP * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOP ) / SUM( NbAnswerOP * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END OP," +
                "CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 )) END COE," +
                "CASE WHEN SUM( NbAnswerIQ * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreIQ ) / SUM( NbAnswerIQ * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 4 ) ) END WDT " +
                "FROM (SELECT ContactIdInt,c.CountryCode,c.ContactId,ContactName,SiteId,StudentID,c.CourseId,NbAnswerCCM,ScoreCCM,NbAnswerFR,ScoreFR,NbAnswerIQ,ScoreIQ,NbAnswerOE,ScoreOE,NbAnswerOP,ScoreOP FROM " +
                "(SELECT ContactId,er.CountryCode,er.SiteId,StudentID,er.CourseId,NbAnswerCCM,ScoreCCM,NbAnswerFR,ScoreFR,NbAnswerIQ,ScoreIQ,NbAnswerOE,ScoreOE,NbAnswerOP,ScoreOP FROM " +
                "(SELECT * FROM EvaluationReport ";

            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "SiteId = '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "FYIndicatorKey = '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "Month = "+property.Value.ToString()+" ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySiteStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "`Status` = '"+property.Value.ToString()+"'";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "RoleId in ('"+property.Value.ToString()+"')";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CertificateType in (" + property.Value.ToString() + ")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("CourseId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "CourseId = '" + property.Value.ToString() + "'";
                        i = i + 1;
                    }
                }

                //if (property.Name.Equals("filterByCertificateType"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        string[] certificatearr = property.Value.ToString().Split(',');

                //        for(int index=0; index<certificatearr.Length;index++){
                //            if(certificatearr[index] == "1"){
                //                if(index==0){query += "and (";}else{query += "or ";}
                //                query += "c.TrainingTypeKey <> '' ";
                //            }
                //            else if(certificatearr[index] == "2"){
                //                if(index==0){query += "and (";}else{query += "or ";}
                //                query += "c.ProjectTypeKey <> '' ";
                //            }
                //            else{
                //                if(index==0){query += "and (";}else{query += "or ";}
                //                query += "c.EventTypeKey <> '' ";
                //            }
                //        }
                //        query += ") ";
                //    }
                //}

            }

            query += " LIMIT 1000) AS er INNER JOIN Courses c ON er.CourseId = c.CourseId) AS c " +
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId) AS ca " +
                "INNER JOIN Main_site s ON ca.SiteId = s.SiteId " +
                "GROUP BY ca.CourseId ORDER BY ca.CourseId";

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }

        [HttpPost("GetStudentData")]
        public IActionResult GetStudentData([FromBody] dynamic data)
        {   
            string query = 
                "SELECT DISTINCT st.StudentId, st.FirstName, st.LastName, IFNULL(st.Phone,'') AS Phone, IFNULL(st.Company,'') AS Company, Email, IFNULL(st.Address,'') AS Address, KeyValue AS `Language`," +
                "rc.countries_name AS Country " +
                "FROM tblStudents st " +
                "INNER JOIN Ref_countries rc ON st.CountryID = rc.countries_code " +
                "INNER JOIN Dictionary d ON st.LanguageID = d.`Key` AND d.Parent = 'Languages' " +
                "WHERE st.StudentID IN (" + data.StudentID + ");";
            List<dynamic> dynamicModel = new List<dynamic>();
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
              
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch 
            {
                result = "Not Found";
            }
            finally
            {
                if(ds.Tables.Count == 0){
                    result = null;
                } else{
                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());
                    settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                    // result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                    result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                        settings);
                }
            }
            return Ok(result);
        }

        [HttpPost("CourseSetting")]
        public IActionResult GetCourseSetting([FromBody] dynamic data)
        {   
            string query = 
                "SELECT DISTINCT c.CourseId, IFNULL(CAST(ATCFacility AS UNSIGNED),'0') AS ATCFacility,IFNULL(CAST(ATCComputer AS UNSIGNED),'0') AS ATCComputer,HoursTraining,`Update`,Essentials,Intermediate,Advanced,Customized,ctl.Other AS OtherCTL, IFNULL(ctl.`Comment`,'') AS CommentCTL,InstructorLed,`Online`, Autodesk,AAP,ATC,Independent,IndependentOnline,ATCOnline,ctm.Other AS OtherCTM, IFNULL(ctm.`Comment`,'') AS CommentCTM,CONCAT(p.productName,' ',pv.Version) AS primaryProduct " +
                "FROM Courses c " +
                "INNER JOIN CourseTeachingLevel ctl ON ctl.CourseId = c.CourseId " +
                "INNER JOIN CourseTrainingFormat ctf ON ctf.CourseId = c.CourseId " +
                "INNER JOIN CourseTrainingMaterial ctm ON ctm.CourseId = c.CourseId " +
                "INNER JOIN CourseSoftware cf ON cf.CourseId = c.CourseId " +
                "INNER JOIN ProductVersions pv ON cf.ProductVersionsId = pv.ProductVersionsId " +
                "INNER JOIN Products p ON pv.productId = p.productId " +
                "WHERE c.CourseId IN (" + data.CourseID + ");";
            List<dynamic> dynamicModel = new List<dynamic>();
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
          
              
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    // ds = MainOrganizationController.EscapeSpecialChar(ds);
                    
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch 
            {
                result = "Not Found";
            }
            finally
            {
                if(ds.Tables.Count == 0){
                    result = null;
                } else{
                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());
                    settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                    //  result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                    result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                        settings);
                }
            }
            return Ok(result);
        }

        [HttpPost("TotalStudentEvaCount")]
        public IActionResult TotalStudentEvaCount([FromBody] dynamic data)
        {
            string fyYear = "";
            int limit = 0;
            int ofset = 0;
            string query = "";
            string query2 = "SELECT DISTINCT StudentID FROM tblStudents st INNER JOIN Ref_countries rc ON st.CountryID = rc.countries_code ";
            string roleID = "";
            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByFirstName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "st.Firstname LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByLastName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "st.Lastname LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "rc.geo_code IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountries"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "rc.countries_code IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("studentStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "st.`Status` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
            }
            query +=
                "SELECT count(StudentEvaluationID) AS EvalID " +
                "FROM EvaluationReport er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "INNER JOIN Main_site s ON er.SiteId = s.SiteId ";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.OrgId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }

            query +=
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = c.CourseId AND er.StudentId = ea.StudentId ";

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterBySiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        fyYear = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "er.Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ")";
                        roleID = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        string cerType = "";
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            if (roleID == "58")
                            {
                                cerType = " er.CertificateType IN (" + property.Value.ToString() + ") ";
                            }
                            else
                            {
                                cerType = " (er.CertificateType IN (" + property.Value.ToString() + ",0) or er.CertificateType is NULL) ";
                            }

                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += cerType;
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("EventType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "c.EventType2 IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }

            query += " AND er.StudentId IN (" + query2 + ") ";
            
            List<dynamic> dynamicModel = new List<dynamic>();
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine("test");
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                    //dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }

               
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                if (ds.Tables.Count == 0)
                {
                    result = null;
                }
                else
                {
                    //var settings = new JsonSerializerSettings
                    //{

                    //};
                    //settings.Converters.Add(new DataSetConverter());
                    //settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                    //// result = MySqlDb.SerializeDataTable(ds.Tables[0]);
                    //result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    //    settings);
                    result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                }

            }
            return Ok(result);
        }
        [HttpPost("SearchingStudent")]
        public IActionResult SearchingStudent([FromBody] dynamic data)
        {
            string fyYear = "";
            int limit = 0;
            int ofset = 0;
            string query = "";
            string query2 = "SELECT DISTINCT StudentID FROM tblStudents st INNER JOIN Ref_countries rc ON st.CountryID = rc.countries_code ";
            string roleID = "";
            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByFirstName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "st.Firstname LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByLastName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "st.Lastname LIKE '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "rc.geo_code IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountries"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "rc.countries_code IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("studentStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "st.`Status` = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
            }

            // query += 
            //     "SELECT er.StudentId, StudentEvaluationID AS EvalID, s.SiteId, s.SiteName, ca.ContactName AS InstructorName, DATE_FORMAT(er.ActualDateSubmitted,'%d-%M-%Y') AS DateSubmitted, c.CourseId, c.`Name` AS CourseTitle, DATE_FORMAT(c.StartDate,'%d-%M-%Y') AS StartDate, DATE_FORMAT(c.CompletionDate,'%d-%M-%Y') AS EndDate, er.ScoreOP, EvaluationAnswerJson, er.RoleId " +
            //     "FROM EvaluationReport er " +
            //     "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
            //     "INNER JOIN Main_site s ON er.SiteId = s.SiteId " +
            //     "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
            //     "INNER JOIN EvaluationAnswer ea ON er.CourseId = c.CourseId AND er.StudentId = ea.StudentId ";

            query +=
                "SELECT er.StudentId, StudentEvaluationID AS EvalID, s.SiteId, s.SiteName, ca.ContactName AS InstructorName, DATE_FORMAT(er.ActualDateSubmitted,'%d-%M-%Y') AS DateSubmitted, c.CourseId, c.`Name` AS CourseTitle, DATE_FORMAT(c.StartDate,'%d-%M-%Y') AS StartDate, DATE_FORMAT(c.CompletionDate,'%d-%M-%Y') AS EndDate, er.ScoreOP, EvaluationAnswerJson, er.RoleId,ea.EvaluationQuestionCode " +
                "FROM EvaluationReport er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "INNER JOIN Main_site s ON er.SiteId = s.SiteId ";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.OrgId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.SiteId IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }

            query +=
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = c.CourseId AND er.StudentId = ea.StudentId ";

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterBySiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '" + property.Value.ToString() + "' ";
                        fyYear = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }

                        if (property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4")
                        {
                            query += "er.Month = " + property.Value.ToString() + " ";
                        }
                        else
                        {
                            switch (property.Value.ToString())
                            {
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }

                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "er.RoleId IN (" + property.Value.ToString() + ")";
                        roleID = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        //if (i == 0) { query += " where "; }
                        //if (i > 0) { query += " and "; }
                        //query += "er.CertificateType IN ("+property.Value.ToString()+")";
                        //i = i + 1;
                        string cerType = "";
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            if (roleID == "58")
                            {
                                cerType = " er.CertificateType IN (" + property.Value.ToString() + ") ";
                            }
                            else
                            {
                                cerType = " (er.CertificateType IN (" + property.Value.ToString() + ",0) or er.CertificateType is NULL) ";
                            }

                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += cerType;
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("EventType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "c.EventType2 IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("limit"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        limit = Int32.Parse(property.Value.ToString());
                    }
                    else
                    {
                        limit = 0;
                    }
                }

                if (property.Name.Equals("ofset"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        ofset = Int32.Parse(property.Value.ToString());
                    }
                    else
                    {
                        ofset = 0;
                    }
                }

            }

            query += " AND er.StudentId IN (" + query2 + ") ";
            if (limit != 0 || ofset != 0)
            {
                query += " LIMIT " + limit + " OFFSET " + ofset + " ";
            }

            List<dynamic> dynamicModel = new List<dynamic>();
            try
            {


                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine("test");
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }

                if (ds.Tables[0].Rows.Count != 0)
                {
                    var evalQuestionCode = "";
                    //added by Soe 28/11/2018
                    DataTable dt1 = ds.Tables[0];
                    foreach (DataRow row in dt1.Rows)
                    {
                        string evalCode = row["EvaluationQuestionCode"].ToString();
                        if (string.IsNullOrEmpty(evalQuestionCode))
                        {
                            evalQuestionCode = "'" + evalCode + "'";
                        }
                        else
                        {
                            evalQuestionCode += ",'" + evalCode + "'";
                        }
                    }
                    //  string query1 = "SELECT EvaluationQuestionJson FROM EvaluationQuestion where EvaluationQuestionCode='" + evalQuestionCode + "' and LanguageiD =35;";
                    string query1 = "SELECT Distinct EvaluationQuestionJson FROM EvaluationQuestion where EvaluationQuestionCode in (" + evalQuestionCode + ") and LanguageiD =35 and Year='" + fyYear + "' group by EvaluationQuestionJson LIMIT 1;";
                    MySqlCommand sqlCommand1 = new MySqlCommand(query1);
                    DataSet ds1 = oDb.getDataSetFromSP(sqlCommand1);

                    string TypeName = "";
                    if (roleID == "58")
                    {
                        TypeName = "AAP";
                    }
                    else
                    {

                        TypeName = "ATC";

                    }
                    dynamicModel = dataService.EvalanswerReformater(ds.Tables[0], TypeName, ds1.Tables[0]);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                if (ds.Tables.Count == 0)
                {
                    result = null;
                }
                else
                {
                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());
                    settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                    // result = MySqlDb.SerializeDataTable(ds.Tables[0]);
                    result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                        settings);
                }
            }
            return Ok(result);
        }
        [HttpPost("SearchingStudentExcel")]
        public IActionResult SearchingStudentExcel([FromBody] dynamic data)
        {
            bool status = false;
            string fyYear = "";
            int limit = 0;
            int ofset = 0;
            string query = "";
            string query2 = "SELECT DISTINCT StudentID FROM tblStudents st INNER JOIN Ref_countries rc ON st.CountryID = rc.countries_code ";
            string roleID = "";
            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByFirstName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "st.Firstname LIKE '%"+property.Value.ToString()+"%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByLastName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "st.Lastname LIKE '%"+property.Value.ToString()+"%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "rc.geo_code IN ("+property.Value.ToString()+") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountries"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "rc.countries_code IN ("+property.Value.ToString()+") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("studentStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query2 += " WHERE "; }
                        if (i > 0) { query2 += " AND "; }
                        query2 += "st.`Status` = '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }
            }
            query +=
                "SELECT er.StudentId, StudentEvaluationID AS EvalID, s.SiteId, s.SiteName, ca.ContactName AS InstructorName, DATE_FORMAT(er.ActualDateSubmitted,'%d-%M-%Y') AS DateSubmitted, c.CourseId, c.`Name` AS CourseTitle, DATE_FORMAT(c.StartDate,'%d-%M-%Y') AS StartDate, DATE_FORMAT(c.CompletionDate,'%d-%M-%Y') AS EndDate, er.ScoreOP, EvaluationAnswerJson, er.RoleId,ea.EvaluationQuestionCode " +
                "FROM EvaluationReport er " +
                "INNER JOIN Courses c ON er.CourseId = c.CourseId " +
                "INNER JOIN Main_site s ON er.SiteId = s.SiteId ";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.OrgId IN ("+property.Value.ToString()+") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.SiteId IN ("+property.Value.ToString()+") ";
                        i = i + 1;
                    }
                }
            }

            query +=    
                "INNER JOIN Contacts_All ca ON c.ContactId = ca.ContactId " +
                "INNER JOIN EvaluationAnswer ea ON er.CourseId = c.CourseId AND er.StudentId = ea.StudentId ";

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterBySiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.SiteId = '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "er.FYIndicatorKey = '"+property.Value.ToString()+"' ";
                        fyYear = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        
                        if(property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4"){
                            query += "er.Month = " + property.Value.ToString() + " ";
                        } else{
                            switch(property.Value.ToString()){
                                case "q1":
                                    query += "er.Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "er.Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "er.Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "er.Quarter = 4 ";
                                    break;
                            }
                        }
                        
                        i = i + 1;
                    }
                }
               
                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "er.RoleId IN ("+property.Value.ToString()+")";
                        roleID = property.Value.ToString();
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        //if (i == 0) { query += " where "; }
                        //if (i > 0) { query += " and "; }
                        //query += "er.CertificateType IN ("+property.Value.ToString()+")";
                        //i = i + 1;
                        string cerType = "";
                        if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                        {
                            if (roleID == "58")
                            {
                                cerType = " er.CertificateType IN (" + property.Value.ToString() + ") ";
                            }
                            else
                            {
                                cerType = " (er.CertificateType IN (" + property.Value.ToString() + ",0) or er.CertificateType is NULL) ";
                            }

                            if (i == 0) { query += " WHERE "; }
                            if (i > 0) { query += " AND "; }
                            query += cerType;
                            i = i + 1;
                        }
                    }
                }

                if (property.Name.Equals("EventType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "c.EventType2 IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                

            }

            query += " AND er.StudentId IN (" + query2 + ") ";
           
            List<dynamic> dynamicModel = new List<dynamic>();
            try
            {
                
                
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine("test");
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
               
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicModel= dataService.ToDynamic(ds.Tables[0]);
                }

                if (ds.Tables[0].Rows.Count != 0)
                {
                    var evalQuestionCode = "";
                    //added by Soe 28/11/2018
                    DataTable dt1 = ds.Tables[0];
                    foreach (DataRow row in dt1.Rows)
                    {
                        string evalCode = row["EvaluationQuestionCode"].ToString();
                        if (string.IsNullOrEmpty(evalQuestionCode))
                        {
                            evalQuestionCode = "'" + evalCode + "'";
                        }
                        else
                        {
                            evalQuestionCode += ",'" + evalCode + "'";
                        }
                    }
                    //  string query1 = "SELECT EvaluationQuestionJson FROM EvaluationQuestion where EvaluationQuestionCode='" + evalQuestionCode + "' and LanguageiD =35;";
                    string query1 = "SELECT Distinct EvaluationQuestionJson FROM EvaluationQuestion where EvaluationQuestionCode in (" + evalQuestionCode + ") and LanguageiD =35 and Year='" + fyYear + "' group by EvaluationQuestionJson LIMIT 1;";
                    MySqlCommand sqlCommand1 = new MySqlCommand(query1);
                    DataSet ds1 = oDb.getDataSetFromSP(sqlCommand1);

                    string TypeName = "";
                    if (roleID == "58")
                    {
                        TypeName = "AAP";
                    }
                    else
                    {

                        TypeName = "ATC";

                    }
                    dynamicModel = dataService.EvalanswerReformater(ds.Tables[0], TypeName, ds1.Tables[0]);
                }
            }
            catch 
            {
                result = "Not Found";
            }
            finally
            {
                if(ds.Tables.Count == 0){
                    result = null;
                } else{
                    var settings = new JsonSerializerSettings
                    {

                    };
                    settings.Converters.Add(new DataSetConverter());
                    settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                    // result = MySqlDb.SerializeDataTable(ds.Tables[0]);
                    result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                        settings);
                    if (dynamicModel.Count > 0 && result.Length > 0)
                    {
                        status = true;
                    }
                    else if (dynamicModel.Count > 0 && (String.IsNullOrEmpty(result)))
                    {
                        status = false;
                        result = "Some data are not incorrect to export to excel";
                    }
                    else
                    {
                        status = false;
                        result = "Sorry, unable to generate excel for this moment!";
                    }
                }
            }
            return Ok(new { success = status, data = result });
        }

        [HttpPost("StudentSearch")]
        public IActionResult StudentSearch([FromBody] dynamic data)
        {
            string query = 
                // "select ea.EvaluationAnswerId, s.SiteIdInt, s.SiteId, r.RoleCode, s.SiteName, ca.ContactName, ea.CreatedDate, ts.Firstname, "+
                // "ts.Lastname, ca.Mobile1, o.OrgName, ca.EmailAddress, ca.Address1, ca.PrimaryLanguage, rc.countries_name, "+
                // "c.CourseCode, c.`Name`, c.StartDate, c.CompletionDate, c.TrainingTypeKey, c.ProjectTypeKey, "+
                // "COUNT(DISTINCT ea.EvaluationAnswerId) AS totalEvaluation, "+
                // "CASE WHEN c.ATCFacility = 1 THEN CONCAT(COUNT(c.ATCFacility),' (',ROUND((COUNT(c.ATCFacility)/COUNT(ea.EvaluationAnswerId) * 100),2),'%)') ELSE CONCAT(0,' (',0,'%)') END ATCFacility, "+
                // "CASE WHEN c.ATCComputer = 1 THEN CONCAT(COUNT(c.ATCComputer),' (',ROUND((COUNT(c.ATCComputer)/COUNT(ea.EvaluationAnswerId) * 100),2),'%)') ELSE CONCAT(0,' (',0,'%)') END ATCComputer, "+
                // "CASE WHEN SUM(er.NbAnswerOP*100.00) = 0 THEN 0.00 ELSE CAST((SUM(er.ScoreOP)/SUM(er.NbAnswerOP*100.00))*100.00 AS DECIMAL(7,4)) END OP, "+
                // "ea.EvaluationAnswerJson, eq.EvaluationQuestionJson, "+
                // "CASE WHEN c.ProjectTypeKey = 1 THEN 'Student Coursework Project' END AS ProjectType,"+
                // "CASE WHEN ctl.Update = 1 THEN 'Update' END AS UpdateLvl,"+
                // "CASE WHEN ctl.Essentials = 1 THEN 'Level 1: Essentials' END AS Essens,"+
                // "CASE WHEN ctl.Intermediate = 1 THEN 'Level 2: Intermediate' END AS Intermed,"+
                // "CASE WHEN ctl.Advanced = 1 THEN 'Level 3: Advanced' END AS Adv,"+
                // "CASE WHEN ctl.Customized = 1 THEN 'Customized' END AS Cust,"+
                // "CASE WHEN ctl.Other = 1 THEN CONCAT('Other:',' ',ctl.`Comment`) END AS OtherCTL,"+
                // "CONCAT(p.productName,' ',pv.Version) AS PrimaryProduct,"+
                // "c.HoursTraining,"+
                // "CASE WHEN ctf.InstructorLed = 1 THEN 'Instructor-led in the classroom' END AS InstLed,"+
                // "CASE WHEN ctf.Online = 1 THEN 'Online or e-learning' END AS InstOnline,"+
                // "CASE WHEN ctm.Autodesk = 1 THEN 'Autodesk Official Courseware (any Autodesk branded material)' END AS Autodesk,"+
                // "CASE WHEN ctm.AAP = 1 THEN 'Autodesk Authors and Publishers (AAP) Program Courseware' END AS AAP,"+
                // "CASE WHEN ctm.ATC = 1 THEN 'Course material developed by the ATC' END AS ATC,"+
                // "CASE WHEN ctm.Independent = 1 THEN 'Course material developed by an independent vendor or instructor' END AS Independent,"+
                // "CASE WHEN ctm.IndependentOnline = 1 THEN 'Online e-learning course material developed by an independent vendor or instructor' END AS IndependentOnline,"+
                // "CASE WHEN ctm.ATCOnline = 1 THEN 'Online e-learning course material developed by the ATC' END AS ATCOnline,"+
                // "CASE WHEN ctm.Other = 1 THEN CONCAT('Other:',' ',ctm.`Comment`) END AS OtherCTM "+
                // "from EvaluationAnswer ea "+
                // "LEFT JOIN EvaluationQuestion eq on eq.EvaluationQuestionCode = ea.EvaluationQuestionCode "+
                // "LEFT JOIN tblStudents ts on ea.StudentId = ts.StudentID "+
                // // "LEFT JOIN Courses c on eq.CourseId = c.CourseCode "+ 
                // "LEFT JOIN Courses c ON ea.CourseId = c.CourseId "+
                // "LEFT JOIN EvaluationReport er on ea.StudentId = er.StudentId "+
                // "LEFT JOIN Main_site s on c.SiteId = s.SiteId "+
                // "LEFT JOIN Roles r on eq.PartnerType = r.RoleId "+
                // "LEFT JOIN Contacts_All ca on c.ContactId = ca.ContactId "+
                // "LEFT JOIN Main_organization o on s.OrgId = o.OrgId "+
                // "LEFT JOIN Ref_countries rc on ca.CountryCode = rc.countries_code "+
                // "LEFT JOIN CourseTeachingLevel ctl ON c.CourseId = ctl.CourseId "+
                // "LEFT JOIN CourseTrainingFormat ctf ON c.CourseId = ctf.CourseId "+
                // "LEFT JOIN CourseTrainingMaterial ctm ON c.CourseId = ctm.CourseId "+
                // "LEFT JOIN CourseSoftware cp ON c.CourseId = cp.CourseId "+
                // "LEFT JOIN Products p ON cp.productId = p.productId "+
                // "LEFT JOIN ProductVersions pv ON p.productId = pv.productId";
            
            "SELECT ContactName,Mobile1,EmailAddress,Address1,KeyValue AS PrimaryLanguage,ts.Firstname,ts.Lastname,SiteIdInt,SiteId,SiteName,CourseTitle,StartDate,CompletionDate,TrainingTypeKey,ProjectTypeKey,CourseId,StudentID,ca.ContactId,RoleCode,countries_name,QP1,QP2,QP3,QP4,QP5,OrgName,RoleId,`Month`,FYIndicatorKey FROM " +
            "(SELECT Firstname,Lastname,SiteIdInt,SiteId,SiteName,CourseTitle,StartDate,CompletionDate,TrainingTypeKey,ProjectTypeKey,CourseId,s.StudentID,ContactId,RoleCode,countries_name,QP1,QP2,QP3,QP4,QP5,OrgName,RoleId,`Month`,FYIndicatorKey FROM " +
            "(SELECT SiteIdInt,c.SiteId,SiteName,CourseTitle,StartDate,CompletionDate,TrainingTypeKey,ProjectTypeKey,CourseId,StudentID,ContactId,CASE WHEN RoleId = 1 THEN 'ATC' ELSE 'AAP' END AS RoleCode,countries_name,QP1,QP2,QP3,QP4,QP5,OrgName,RoleId,`Month`,FYIndicatorKey FROM " +
            "(SELECT `Name` AS CourseTitle,DATE_FORMAT(StartDate,'%m/%d/%Y') AS StartDate,DATE_FORMAT(CompletionDate,'%m/%d/%Y') AS CompletionDate,TrainingTypeKey,ProjectTypeKey,r.CourseId,r.SiteId,StudentID,ContactId,RoleId,`Month`,r.FYIndicatorKey," +
            "CASE WHEN SUM( NbAnswerCCM * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreCCM ) / SUM( NbAnswerCCM * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 ) ) END QP1," +
            "CASE WHEN SUM( NbAnswerFR * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreFR ) / SUM( NbAnswerFR * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 ) ) END QP2," +
            "CASE WHEN SUM( NbAnswerIQ * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreIQ ) / SUM( NbAnswerIQ * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 ) ) END QP3," +
            "CASE WHEN SUM( NbAnswerOE * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOE ) / SUM( NbAnswerOE * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 ) ) END QP4," +
            "CASE WHEN SUM( NbAnswerOP * 100.00 ) = 0 THEN 0.00 ELSE CAST(( SUM( ScoreOP ) / SUM( NbAnswerOP * 100.00 ) ) * 100.00 AS DECIMAL ( 7, 2 ) ) END QP5 FROM " +
            "(SELECT CourseId,SiteId,StudentID,NbAnswerOP,ScoreOP,RoleId,NbAnswerCCM,ScoreCCM,NbAnswerFR,ScoreFR,NbAnswerIQ,ScoreIQ,NbAnswerOE,ScoreOE,`Month`,FYIndicatorKey FROM EvaluationReport ";
            
            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterBySiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "SiteId like '%"+property.Value.ToString()+"%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterBySurveyYear"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "FYIndicatorKey = '"+property.Value.ToString()+"' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByDate"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        
                        if(property.Value.ToString() != "q1" && property.Value.ToString() != "q2" && property.Value.ToString() != "q3" && property.Value.ToString() != "q4"){
                            query += "Month = " + property.Value.ToString() + " ";
                        } else{
                            switch(property.Value.ToString()){
                                case "q1":
                                    query += "Quarter = 1 ";
                                    break;
                                case "q2":
                                    query += "Quarter = 2 ";
                                    break;
                                case "q3":
                                    query += "Quarter = 3 ";
                                    break;
                                case "q4":
                                    query += "Quarter = 4 ";
                                    break;
                            }
                        }
                        
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "RoleId in ("+property.Value.ToString()+")";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCertificateType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "CertificateType in ("+property.Value.ToString()+")";
                        i = i + 1;
                    }
                }

            }

            query += " LIMIT 500) AS r " +
            "INNER JOIN Courses c ON r.CourseId = c.CourseId ";

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("EventType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " WHERE "; }
                        if (i > 0) { query += " AND "; }
                        query += "c.EventType2 IN (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
            }
            query += "GROUP BY StudentID) AS c " +
            "INNER JOIN Main_site s ON c.SiteId = s.SiteId " +
            "INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code " +
            "INNER JOIN Main_organization o ON s.OrgId = o.OrgId ";

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in ("+property.Value.ToString()+") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByCountries"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.countries_code in ("+property.Value.ToString()+") ";
                        i = i + 1;
                    }
                }
            }

            query += " ) AS s " +
            "INNER JOIN tblStudents ts ON s.StudentID = ts.StudentID ";

            i = 0;
            json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByFirstName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "ts.Firstname like '%"+property.Value.ToString()+"%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("filterByLastName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "ts.Lastname like '%"+property.Value.ToString()+"%' ";
                        i = i + 1;
                    }
                }
            }

            query += " ) AS ts " +
            "INNER JOIN Contacts_All ca ON ts.ContactId = ca.ContactId " +
            "INNER JOIN Dictionary d ON ca.PrimaryLanguage = d.`Key` " +
            "WHERE d.Parent = 'Languages'";
            
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(query);
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch 
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                if(result == "[]"){
                    result = "Not Found";
                }
            }
            return Ok(result);
        }

    }
}