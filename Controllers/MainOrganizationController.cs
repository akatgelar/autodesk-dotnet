using autodesk.Code;
using autodesk.Code.DataServices;
using autodesk.Code.Models;
using autodesk.Code.CommonServices;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Reflection;
using Newtonsoft.Json.Serialization;

using Microsoft.EntityFrameworkCore;
using autodesk.Code.Models.Dto;



// using autodesk.Models;

namespace autodesk.Controllers
{

    [Route("api/MainOrganization")]
    public class MainOrganizationController : Controller
    {
        StringBuilder sb = new StringBuilder();

        private MySqlDb oDb;
        //= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        DataSet dsTemp = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        private IDataServices dataService;
        private ICommonServices commonServices;
        String result;
        String hasil;

        public Audit _audit;//= new Audit();
        private readonly MySQLContext _mySQLContext;

        public MainOrganizationController(MySqlDb mySql, MySQLContext mySQLContext)
        {
            oDb = mySql;
            _audit = new Audit(oDb);
            _mySQLContext = mySQLContext;
            dataService = new DataServices(oDb, _mySQLContext);
            commonServices = new CommonServices();

        }

        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {

                            dr[y] = dr[y].ToString().Replace(@"\", @"\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("//"))
                        {
                            dr[y] = dr[y].ToString().Replace("//", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // [HttpGet("FindOrganization/{role}/{org}/{value}")]
        [HttpGet("FindOrganization/{obj}/{value}")]
        public IActionResult FindOrganization(string obj, string value)
        {
            obj = obj.Replace("\':\'","\":\"").Replace("{\'", "{\"").Replace("\'}", "\"}").Replace("\',\'org","\",\"org");
            var o1 = JObject.Parse(obj);
            string query = "";
            string org = "";
            JObject resultfinal;
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var orgId = value.Split('|').FirstOrDefault();
                    var orgName = value.Split('|').LastOrDefault();

                    query = "SELECT DISTINCT o.OrganizationId, o.OrgId, REPLACE ( REPLACE ( REPLACE ( o.OrgName, '\"', '\\\\\"' ), '\\\\', '\\\\\\\\' ), '\\\\\"', '\\\"' ) AS OrgName, REPLACE ( REPLACE ( REPLACE ( CONCAT(o.OrgId, ' | ', o.OrgName), '\"', '\\\\\"' ), '\\\\', '\\\\\\\\' ), '\\\\\"', '\\\"' ) AS Organization FROM Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId LEFT JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
                        "WHERE (o.OrgId like '%" + (!string.IsNullOrEmpty(orgId) ? orgId.Trim() : value) + "%' OR o.OrgName like '%" + (!string.IsNullOrEmpty(orgName) ? orgName.Trim() : value) + "%') AND sr.`Status` <> '' ";
                    //  AND o.`Status` NOT IN ( 'X', 'D' ) AND s.`Status` NOT IN ( 'X', 'D' )
                }
                int i = 0;
                Boolean itsdistributor = false;
                Boolean itsadmin = false;
                JObject json = JObject.FromObject(o1);
                foreach (JProperty property in json.Properties())
                {
                    if (property.Name.Equals("UserLevelId"))
                    {
                        string[] arruser = property.Value.ToString().Split(',');
                        for (int j = 0; j < arruser.Length; j++)
                        {
                            if (arruser[j] == "DISTRIBUTOR")
                            {
                                itsdistributor = true;
                            }
                            if (arruser[j] == "SUPERADMIN" || arruser[j] == "ADMIN")
                            {
                                itsadmin = true;
                            }
                        }
                    }
                    if (property.Name.Equals("org"))
                    {
                        org = property.Value.ToString();
                    }
                    i = i + 1;
                }



                if (!itsadmin)
                {
                    if (itsdistributor)
                    {
                        // query += "AND rc.TerritoryId in (select rc.TerritoryId from Main_organization o JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code where o.OrgId in ("+org+"))";
                        query += "AND s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgID INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId in (" + org + "))"; //ganti jadi sesuai country distributor
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(org))
                        {
                            query += "and o.OrgId in (" + org + ")";
                        }

                    }
                }

                // Console.WriteLine(query);

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                    "{" +
                    "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }

            return Ok(resultfinal);
        }

        [HttpGet("FindOrganizationDistributor/{value}")]
        public IActionResult FindOrganizationDistributor(string value)
        {
            JObject resultfinal;
            try
            {
                string query =
                    //"select o.OrgId, REPLACE ( REPLACE ( REPLACE ( CONCAT(o.OrgId, ' | ', o.OrgName), '\"', '\\\\\"' ), '\\\\', '\\\\\\\\' ), '\\\\\"', '\\\"' ) AS Organization from " +
                    "select o.OrgId, CONCAT(o.OrgId, ' | ', o.OrgName) AS Organization from " +
                    "(select SiteID from SiteCountryDistributor GROUP BY SiteId) as scd " +
                    "INNER JOIN Main_site s ON scd.SiteId = s.SiteId " +
                    "INNER JOIN Main_organization o ON s.OrgId = o.OrgId " +
                    "WHERE ( o.OrgId LIKE '%" + value + "%' OR o.OrgName LIKE '%" + value + "%' ) AND o.`Status` <> 'X' AND s.`Status` <> 'X'" +
                    "GROUP BY OrgId ";

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                    "{" +
                    "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }

            return Ok(resultfinal);
        }

        [HttpGet("FindOrganizationSiteAdmin/{value}")]
        public IActionResult CariOrgSiteAdmin(string value)
        {
            JObject resultfinal;
            try
            {
                string query =
                    "SELECT DISTINCT o.OrgId, REPLACE ( REPLACE ( REPLACE ( CONCAT(o.OrgId, ' | ', o.OrgName), '\"', '\\\\\"' ), '\\\\', '\\\\\\\\' ), '\\\\\"', '\\\"' ) AS Organization FROM Main_organization o " +
                    "WHERE o.OrgId LIKE '%" + value + "%' OR o.OrgName LIKE '%" + value + "%' AND o.`Status` <> 'X' ORDER BY o.OrgName ASC";

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                    "{" +
                    "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }

            return Ok(resultfinal);
        }

        [HttpGet("FindSite/{role}/{org}/{value}")]
        public IActionResult FindSite(string role, string org, string value)
        {
            JObject resultfinal;
            try
            {
                string query = "SELECT DISTINCT s.SiteId, s.SiteName, CONCAT( s.SiteId, ' | ', s.SiteName ) AS Sites FROM Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId LEFT JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
                    "WHERE (s.SiteId like '%" + value + "%' OR s.SiteName like '%" + value + "%') AND o.`Status` <> 'X' AND s.`Status` <> 'X' ";

                Boolean itsdistributor = false;
                Boolean itsadmin = false;
                string[] arruser = role.Split(',');
                for (int i = 0; i < arruser.Length; i++)
                {
                    if (arruser[i] == "DISTRIBUTOR")
                    {
                        itsdistributor = true;
                    }
                    if (arruser[i] == "SUPERADMIN" || arruser[i] == "ADMIN")
                    {
                        itsadmin = true;
                    }
                }

                if (!itsadmin)
                {
                    if (itsdistributor)
                    {
                        query += "AND rc.TerritoryId in (select rc.TerritoryId from Main_organization o JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code where o.OrgId in (" + org + "))";
                    }
                    else
                    {
                        query += "and o.OrgId in (" + org + ")";
                    }
                }

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }

                result =
                    "{" +
                    "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }

            return Ok(resultfinal);
        }

        //[HttpPost("FindSiteNew/{role}/{org}/{value}")]
        [HttpPost("FindSiteNew/{role}/{value}")]
        public IActionResult FindSiteNew(string role, string value, [FromBody] dynamic data)
        {
            // JObject tmpData = JObject.FromObject(data);
            JObject resultfinal;
            try
            {
                string query = "SELECT DISTINCT s.SiteId FROM Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId LEFT JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code INNER JOIN SiteRoles sr ON s.SiteId = sr.SiteId " +
                    "WHERE s.OrgId like '%" + value + "%' AND o.`Status` <> 'X' AND s.`Status` <> 'X' ";

                Boolean itsdistributor = false;
                Boolean itsadmin = false;
                Boolean itsorg = false;
                Boolean itssite_admin = false;
                string[] arruser = role.Split(',');
                for (int i = 0; i < arruser.Length; i++)
                {
                    if (arruser[i] == "DISTRIBUTOR")
                    {
                        itsdistributor = true;
                    }
                    if (arruser[i] == "SUPERADMIN" || arruser[i] == "ADMIN")
                    {
                        itsadmin = true;
                    }
                    if (arruser[i] == "ORGANIZATION")
                    {
                        itsorg = true;
                    }
                    if (arruser[i] == "SITE")
                    {
                        itssite_admin = true;
                    }
                }

                if (!itsadmin)
                {
                    if (itsdistributor)
                    {
                        query += "AND s.SiteCountryCode in (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + data.id + " ))";
                    }
                    else
                    {
                        if (itsorg)
                        {
                            query += "AND o.OrgId IN (" + data.id + ")";  //variable org di sini sebenarnya berisi org id untuk Organization Admin
                        }
                        else if (itssite_admin)
                        {
                            query += "AND s.SiteId IN (" + data.id + ")"; //variable org di sini sebenarnya berisi site id untuk Site Admin
                        }
                        else
                        {
                            query += "AND s.SiteId IN (SELECT DISTINCT s.SiteId FROM SiteContactLinks sc INNER JOIN Main_site s ON sc.SiteId = s.SiteId WHERE sc.`Status` = 'A' AND s.`Status` NOT IN ('X','D','T') AND s.OrgId IN (" + data.id + "))";
                        }
                    }
                }

                // query += " AND sr.`Status` NOT IN ('D','X','T')";

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                    "{" +
                    "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }

            return Ok(resultfinal);
        }

        [HttpGet("FindOrganizationEditAffiliate/{role}/{sitearr}/{value}")]
        public IActionResult FindOrganizationEditAffiliate(string role, string sitearr, string value)
        {
            JObject resultfinal;
            try
            {
                string query = "SELECT DISTINCT o.OrganizationId, o.OrgId, REPLACE ( REPLACE ( REPLACE ( o.OrgName, '\"', '\\\\\"' ), '\\\\', '\\\\\\\\' ), '\\\\\"', '\\\"' ) AS OrgName, REPLACE ( REPLACE ( REPLACE ( CONCAT(o.OrgId, ' | ', o.OrgName), '\"', '\\\\\"' ), '\\\\', '\\\\\\\\' ), '\\\\\"', '\\\"' ) AS Organization FROM Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId LEFT JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code " +
                    "WHERE (o.OrgId like '%" + value + "%' OR o.OrgName like '%" + value + "%') AND o.`Status` <> 'X' AND s.`Status` <> 'X' ";

                Boolean itsdistributor = false;
                Boolean itsadmin = false;
                string[] arruser = role.Split(',');
                for (int i = 0; i < arruser.Length; i++)
                {
                    if (arruser[i] == "DISTRIBUTOR")
                    {
                        itsdistributor = true;
                    }
                    if (arruser[i] == "SUPERADMIN" || arruser[i] == "ADMIN")
                    {
                        itsadmin = true;
                    }
                }

                if (!itsadmin)
                {
                    if (itsdistributor)
                    {
                        // query += "AND rc.TerritoryId in (select rc.TerritoryId from Main_organization o JOIN Ref_countries rc ON o.RegisteredCountryCode = rc.countries_code JOIN Main_site s ON o.OrgId = s.OrgId where s.SiteId not in ("+sitearr+"))"; /* cigana salah */
                        query += "AND s.SiteCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( SELECT OrgId FROM Main_site where SiteId IN ( " + sitearr + " ) ) ) and o.OrgId not in (SELECT OrgId FROM Main_site where SiteId IN ( " + sitearr + " ))";
                    }
                    else
                    {
                        query += "AND o.OrgId NOT IN (SELECT OrgId FROM Main_site where SiteId IN ( " + sitearr + " ))";
                    }
                }
                else
                {
                    query += "AND o.OrgId NOT IN (SELECT OrgId FROM Main_site where SiteId IN ( " + sitearr + " ))";
                }

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                    "{" +
                    "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }

            return Ok(resultfinal);
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Main_organization ");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("FindOrg/{value}")]
        public IActionResult FindOrg(string value)
        {
            JObject resultfinal;
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT OrgId, replace( replace( replace( OrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as OrgName, CONCAT(OrgId,' | ',replace( replace( replace( OrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' )) AS Org " +
                "FROM Main_organization WHERE OrgId like '%" + value + "%'");
                // Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result =
                    "{" +
                    "\"results\":" + MySqlDb.GetJSONArrayString(ds.Tables[0]) + "" +
                    "}";
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                resultfinal = JObject.Parse(result);
            }

            return Ok(resultfinal);
        }

        [HttpGet("AddContact")]
        public IActionResult AddContact()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("SELECT DISTINCT o.OrganizationId, o.OrgId, o.OrgName FROM Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId WHERE o.`Status` <> 'X' AND s.`Status` <> 'X'");
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("AddContactOrg/{userlvl}/{orgids}")]
        public IActionResult AddContactOrg(string userlvl, string orgids)
        {
            try
            {
                Boolean itsdistributor = false;
                string[] arruser = userlvl.Split(',');
                for (int i = 0; i < arruser.Length; i++)
                {
                    if (arruser[i] == "DISTRIBUTOR")
                    {
                        itsdistributor = true;
                    }
                }

                string query = "SELECT DISTINCT o.OrganizationId, o.OrgId, o.OrgName FROM Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId WHERE o.`Status` <> 'X' AND s.`Status` <> 'X' and o.OrgId in (" + orgids + ")";

                if (itsdistributor)
                {
                    query = "SELECT DISTINCT o.OrganizationId, o.OrgId, o.OrgName FROM Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId WHERE o.`Status` <> 'X' AND s.`Status` <> 'X' AND o.RegisteredCountryCode IN (SELECT CountryCode FROM SiteCountryDistributor scd INNER JOIN Main_site s ON scd.SiteID = s.SiteId WHERE s.OrgId IN ( " + orgids + " ))";
                }

                sb = new StringBuilder();
                sb.Append(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = MainOrganizationController.EscapeSpecialChar(ds);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("getOrgId")]
        public IActionResult selectOrgId()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select OrgId from Main_organization");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        public MainOrganization selectMainOrg()
        {
            MainOrganization m = new MainOrganization();
            try
            {
                sb = new StringBuilder();
                sb.Append("select OrgId from Main_organization");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                //   m = "{}";
            }
            finally
            {

                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return m;
        }
        [HttpGet("getColumns_Journal")]
        public IActionResult SelectColumnsJournal()
        {
            DataTable dt1 = new DataTable();
            try
            {
                sb = new StringBuilder();
                sb.Append("DESCRIBE Main_organization; SHOW COLUMNS FROM Journals LIKE '%Notes%';");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                dt1 = ds.Tables[0];
                DataTable dt2 = ds.Tables[1];
                dt1.Merge(dt2, false, MissingSchemaAction.Add);

            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }
        [HttpGet("getColumns")]
        public IActionResult SelectColumns()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("DESCRIBE Main_organization ");
              
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        {
            List<dynamic> dynamicModel = new List<dynamic>();

            try
            {


                sb = new StringBuilder();
                sb.AppendFormat("select o.OrganizationId, o.OrgId, o.OrgName, " +
                    "o.EnglishOrgName, o.CommercialOrgName, o.PrimarySiteId, o.YearJoined, o.TaxExempt, o.VATNumber, o.SAPSoldTo, o.VARCSN, o.ATCCSN, o.AUPCSN, o.OrgWebAddress, " +
                    "o.RegisteredDepartment, o.RegisteredAddress1, o.RegisteredAddress2, o.RegisteredAddress3, o.RegisteredCity, o.RegisteredStateProvince, " +
                    "o.RegisteredPostalCode, o.RegisteredCountryCode, o.InvoicingDepartment, o.InvoicingAddress1, o.InvoicingAddress2, o.InvoicingAddress3, o.InvoicingCity, " +
                    "o.InvoicingStateProvince, o.InvoicingPostalCode, o.InvoicingCountryCode, o.ContractDepartment, o.ContractAddress1, o.ContractAddress2, " +
                    "o.ContractAddress3, o.ContractCity, o.ContractStateProvince, o.ContractPostalCode, o.ContractCountryCode, o.ATCDirectorFirstName, o.ATCDirectorLastName, " +
                    "o.ATCDirectorEmailAddress, o.ATCDirectorTelephone, o.ATCDirectorFax, o.LegalContactFirstName, o.LegalContactLastName, o.LegalContactEmailAddress, " +
                    "o.LegalContactTelephone, o.LegalContactFax, o.BillingContactFirstName, o.BillingContactLastName, o.BillingContactEmailAddress, o.BillingContactTelephone, " +
                    "o.BillingContactFax, o.AdminNotes, DATE_FORMAT(o.DateAdded,'%m/%d/%Y') AS DateAdded, o.LastAdminBy, " +
                    "(SELECT GROUP_CONCAT(DISTINCT d.KeyValue ORDER BY d.KeyValue SEPARATOR ', ')  From Main_site s, SiteRoles scl, Roles r, Dictionary d  WHERE s.OrgId = o.OrgId  AND s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  AND scl.Status <> 'X'   and d.Parent = 'SiteStatus' and d.`Status` = 'A' and d.`Key` = scl.Status) as `Status` " +
                    ", geo_name, countries_name, region_name " +
                    "from Main_organization o left join Ref_countries on RegisteredCountryCode = countries_code " +
                    "where OrganizationId = '" + id + "'");

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                // dt = dataService.ChangeSpecialCharacter(dt);
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        string encodedata = row[column].ToString();
                        string type = column.DataType.ToString();
                        if (type != "System.DateTime" && column.ColumnName != "DateAdded")
                        {
                            encodedata = commonServices.HtmlEncoder(encodedata);
                            row[column] = encodedata;
                        }
                        else
                        {

                        }
                    }
                    
                }
                if (ds.Tables[0].Rows.Count != 0)
                {

                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    // ds = MainOrganizationController.EscapeSpecialChar(ds);
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                if (dynamicModel.Count > 0)
                {
                    if (dynamicModel.FirstOrDefault() != null)
                    {
                        var returnModel = dynamicModel.FirstOrDefault();
                        string orgId = returnModel.OrgId;
                        var orgSite = _mySQLContext.OrgSiteAttachment.Where(o => o.OrgId == orgId && o.DocType == "Add" && o.SiteId == null).Select(o => new { o.Name, o.Id }).ToList();
                        returnModel.Files = orgSite;
                        var Logo = _mySQLContext.OrgSiteAttachment.FirstOrDefault(o => o.OrgId == orgId && o.DocType == "logo" && o.SiteId == null);
                        if (Logo != null)
                        {
                            returnModel.LogoName = Logo.Name;
                            returnModel.orgLogo = Logo.Content;
                            returnModel.LogoType = Logo.Type;
                            returnModel.LogoId = Logo.Id;
                        }
                        else
                        {
                            returnModel.LogoName = "";
                            returnModel.orgLogo = "";
                            returnModel.LogoType = "";
                            returnModel.LogoId = 0;
                        }

                        result = JsonConvert.SerializeObject(dynamicModel.FirstOrDefault(), Formatting.Indented,
                      settings);
                    }

                }
                else
                {
                    result = "{}";
                }

                //result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("orgname/{name}")]
        public IActionResult WhereOrgName(string name)
        {
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat("select * from Main_organization where OrgName = '" + name + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";
            query += "select o.OrganizationId, o.OrgId, replace( replace( replace( o.OrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as OrgName, GROUP_CONCAT(DISTINCT r.RoleName) as PartnerType, GROUP_CONCAT(DISTINCT d.KeyValue) as PartnerTypeStatus, o.RegisteredCountryCode, o.RegisteredStateProvince, rc.geo_name, rc.countries_name from Main_organization o LEFT JOIN Main_site s on o.OrgId = s.OrgId LEFT JOIN SiteRoles sr on s.SiteId = sr.SiteId LEFT JOIN Roles r on sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc on o.RegisteredCountryCode = rc.countries_code LEFT JOIN Dictionary d ON d.Parent = 'SiteStatus' and d.`Status` = 'A' and sr.`Status` = d.`Key` ";
            int i = 0;
            JObject json = JObject.FromObject(o1);

            var territory = "";
            var organization = "";

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("UserRoleTerritory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        territory = property.Value.ToString();
                    }
                }
            }

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Organization"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.OrgId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.OrgId = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("OrgName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.OrgName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("RegisteredCountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.RegisteredCountryCode in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("RegisteredStateProvince"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.RegisteredStateProvince like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "r.RoleName = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "sr.`Status` in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Region"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.region_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Subregion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.subregion_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.TerritoryId in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                    else
                    {
                        if (territory != "")
                        {
                            if (i == 0) { query += " where "; }
                            if (i > 0) { query += " and "; }
                            query += "rc.TerritoryId in ( " + territory + " ) ";
                        }
                        i = i + 1;
                    }
                }
            }

            if (query.Contains("where"))
            {
                query += "  AND o.Status NOT IN ('X','Z','D') GROUP BY o.OrgName asc";
            }
            else
            {
                query += "  where o.Status NOT IN ('X','Z','D') GROUP BY o.OrgName asc";
            }
            List<dynamic> dynamicModel = new List<dynamic>();

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                if (ds.Tables.Count > 0)
                {
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
                //  result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost("FilterOrg")]
        public IActionResult FilterOrg([FromBody] dynamic data)
        {
            string query = "";
            string pstatus = "";
            string tmporgname = "";
            if (data.PartnerTypeStatus.ToString() != "" && data.PartnerTypeStatus.ToString() != null)
            {
                pstatus = "and scl.Status in (" + data.PartnerTypeStatus.ToString() + ")";
            }
            // query += "select o.OrganizationId, o.OrgId, replace( replace( replace( o.OrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as OrgName, GROUP_CONCAT(DISTINCT r.RoleName) as PartnerType, GROUP_CONCAT(DISTINCT d.KeyValue) as PartnerTypeStatus, o.RegisteredCountryCode, o.RegisteredStateProvince, rc.geo_name, rc.countries_name from Main_organization o LEFT JOIN Main_site s on o.OrgId = s.OrgId LEFT JOIN SiteRoles sr on s.SiteId = sr.SiteId LEFT JOIN Roles r on sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc on o.RegisteredCountryCode = rc.countries_code LEFT JOIN Dictionary d ON d.Parent = 'SiteStatus' and d.`Status` = 'A' and sr.`Status` = d.`Key` ";
            query += "select o.OrganizationId, o.OrgId, replace( replace( replace( o.OrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as OrgName, " +
                "case when (SELECT GROUP_CONCAT(concat(r.RoleCode,'(',d.KeyValue,')') SEPARATOR ', ')  From Main_site s,SiteRoles scl, Roles r , Dictionary d  WHERE s.OrgId = o.OrgId  AND s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  and d.Parent = 'SiteStatus'  and d.`Status` = 'A' and d.`Key` = scl.Status ) is not null then (SELECT GROUP_CONCAT(concat(r.RoleCode,'(',d.KeyValue,')') SEPARATOR ', ')  From Main_site s,SiteRoles scl, Roles r , Dictionary d  WHERE s.OrgId = o.OrgId  AND s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId   and d.Parent = 'SiteStatus'  and d.`Status` = 'A' and d.`Key` = scl.Status ) else '-' end as PartnerType , " +
                "(SELECT GROUP_CONCAT(DISTINCT d.KeyValue ORDER BY d.KeyValue SEPARATOR ', ')  From Main_site s, SiteRoles scl, Roles r, Dictionary d  WHERE s.OrgId = o.OrgId  AND s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  and d.Parent = 'SiteStatus' and d.`Status` = 'A' and d.`Key` = scl.Status) as `Status`, " +
                "o.RegisteredCountryCode, o.RegisteredStateProvince, rc.geo_name, rc.countries_name from Main_organization o LEFT JOIN Main_site s on o.OrgId = s.OrgId   inner join (select  SiteId, RoleId, max(SiteRoleId) maxid from SiteRoles group by SiteId, RoleId) MaxRole on s.SiteId = MaxRole.SiteId " +
                " INNER JOIN SiteRoles sr ON MaxRole.maxid = sr.SiteRoleId INNER JOIN Roles r ON r.RoleId = sr.RoleId   left Join RoleParams rp on rp.RoleParamId =sr.RoleParamId   LEFT JOIN Ref_countries rc on o.RegisteredCountryCode = rc.countries_code  ";

            int i = 0;

            JObject json = JObject.FromObject(data);

            var territory = "";
            var organization = "";

            // foreach (JProperty property in json.Properties())
            // {
            //     if(property.Name.Equals("UserRoleTerritory")){   
            //         if((property.Value.ToString() != "") && (property.Value.ToString() != null)){
            //             territory = property.Value.ToString();
            //         }
            //     }
            // }

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Organization"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.OrgId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.OrgId like '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("OrgName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        tmporgname = " (o.OrgName like '%" + property.Value.ToString().Trim() + "%' or o.EnglishOrgName like '%" + property.Value.ToString().Trim() + "%') ";
                        //if (i == 0) { query += " where "; }
                        //if (i > 0) { query += " and "; }
                        //query += " (o.OrgName like '%" + property.Value.ToString().Trim() + "%' or o.EnglishOrgName like '%" + property.Value.ToString().Trim() + "%') ";
                        //i = i + 1;
                    }
                }

                if (property.Name.Equals("OrgNameSpecial"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        if (!string.IsNullOrEmpty(tmporgname))
                        {
                            query += "(" + tmporgname + "OR (o.OrgName like '%" + property.Value.ToString().Trim() + "%' or o.EnglishOrgName like '%" + property.Value.ToString().Trim() + "%')) ";
                        }
                        else
                        {
                            query += " (o.OrgName like '%" + property.Value.ToString().Trim() + "%' or o.EnglishOrgName like '%" + property.Value.ToString().Trim() + "%') ";
                        }
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("RegisteredCountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.RegisteredCountryCode in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("RegisteredStateProvince"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        if (i == 0) { query += " where "; }
                //        if (i > 0) { query += " and "; }
                //        query += "o.RegisteredStateProvince like '%" + property.Value.ToString() + "%' ";
                //        i = i + 1;
                //    }
                //}

                if (property.Name.Equals("RegisteredStateProvince"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "o.`Status` in ( "+property.Value.ToString()+" ) "; /* change filter partner type status to status site */
                        query += "(o.`RegisteredStateProvince` in (select StateName from States where StateID in (  " + property.Value.ToString() + " ) )) ";
                        i = i + 1;
                    }
                }


                if (property.Name.Equals("PartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "r.RoleCode = '" + property.Value.ToString() + "' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SubPartnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "o.`Status` in ( "+property.Value.ToString()+" ) "; /* change filter partner type status to status site */
                        query += "(rp.`RoleParamId` in ( " + property.Value.ToString() + " )  or  rp.`RoleParamId`is null or  rp.`RoleParamId` = '') ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("PartnerTypeStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        // query += "o.`Status` in ( "+property.Value.ToString()+" ) "; /* change filter partner type status to status site */
                        query += "(sr.`Status` in ( " + property.Value.ToString() + " )  or  sr.`Status`= null or  sr.`Status` ='') ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.geo_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Region"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.region_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("Subregion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.subregion_code in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query +=
                            "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " ) GROUP BY o.OrgId) > 0 " +
                            "THEN o.RegisteredCountryCode IN (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) " +
                            "ELSE o.RegisteredCountryCode IN (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( " + property.Value.ToString() + " )) END ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("SiteAdmin"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "o.OrgId IN  ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("Territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "rc.TerritoryId in ( " + property.Value.ToString() + " ) ";
                        i = i + 1;
                    }
                    // else{
                    //     if(territory != ""){
                    //         if(i==0){query += " where ";}
                    //         if(i>0){query += " and ";} 
                    //         query += "rc.TerritoryId in ( "+territory+" ) "; 
                    //         i = i + 1; 
                    //     }
                    // }
                }

                // search general
                if (property.Name.Equals("id_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.OrgId like '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("name_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.OrgName like '%" + property.Value.ToString().Trim() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("address_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.RegisteredAddress1 like '%" + property.Value.ToString() + "%' or o.RegisteredAddress2 like '%" + property.Value.ToString() + "%' or o.RegisteredAddress3 like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("email_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.ATCDirectorEmailAddress like '%" + property.Value.ToString().Trim() + "%' or o.BillingContactEmailAddress like '%" + property.Value.ToString() + "%' or o.LegalContactEmailAddress like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CSN_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "sr.CSN like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("notes_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.AdminNotes like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("contact_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "c.ContactName like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("invoices_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        Regex re = new Regex(@"\d+");
                        Match m = re.Match("property.Value.ToString()");
                        if (m.Success)
                        {
                            if (i == 0) { query += " where ( "; }
                            if (i > 0) { query += " or "; }
                            query += "i.InvoiceNumber = " + property.Value.ToString() + "'";
                        }
                        i = i + 1;
                    }
                }
            }

                //remove this due to this value can be null query += " and (rp.`RoleParamId`is not null or  rp.`RoleParamId` <>'') "
                query += " GROUP BY o.OrgId,o.OrgName,rc.countries_name asc ";

         

            // Console.WriteLine(query);
            List<dynamic> dynamicModel = new List<dynamic>();

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                DataTable dt = ds.Tables[0];
                foreach (DataRow row in dt.Rows)
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        string notes = row[column].ToString();
                        notes = commonServices.HtmlEncoder(notes);
                        row[column] = notes;
                    }

                }
                if (ds.Tables.Count > 0)
                {
                    dynamicModel = dataService.ToDynamic(ds.Tables[0]);
                }
                else
                {
                    result = "Not Found";
                }
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();
                //result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
                result = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented,
                    settings);
            }

            return Ok(result);
        }

        [HttpGet("whereGeneral/{obj}")]
        public IActionResult whereGeneral(string obj)
        {
            var o1 = JObject.Parse(obj);
            string query = "";
            query += "select o.OrganizationId, o.OrgId, replace( replace( replace( o.OrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as OrgName, " +
                "(SELECT GROUP_CONCAT(DISTINCT RoleName ORDER BY RoleName SEPARATOR ', ')  From Main_site s, SiteRoles scl, Roles r  WHERE s.OrgId = o.OrgId  AND s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  AND scl.Status <> 'X'  AND s.Status = 'A') As PartnerType , " +
                "(SELECT GROUP_CONCAT(DISTINCT d.KeyValue ORDER BY d.KeyValue SEPARATOR ', ')  From Main_site s, SiteRoles scl, Roles r, Dictionary d  WHERE s.OrgId = o.OrgId  AND s.SiteId = scl.SiteId  AND scl.RoleId = r.RoleId  AND scl.Status <> 'X'  AND s.Status = 'A' and d.Parent = 'SiteStatus' and d.`Status` = 'A' and d.`Key` = scl.Status) as `Status`, " +
                "o.RegisteredCountryCode, o.RegisteredStateProvince, rc.geo_name, rc.countries_name from Main_organization o LEFT JOIN Main_site s on o.OrgId = s.OrgId LEFT JOIN SiteRoles sr on s.SiteId = sr.SiteId LEFT JOIN Roles r on sr.RoleId = r.RoleId LEFT JOIN Ref_countries rc on o.RegisteredCountryCode = rc.countries_code " +
                "LEFT JOIN SiteContactLinks sc ON sc.SiteId = s.SiteId LEFT JOIN Contacts_All c ON sc.ContactId = c.ContactId LEFT JOIN Invoices i ON o.OrgId = i.OrgId ";
            int i = 0;
            JObject json = JObject.FromObject(o1);

            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("id_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.OrgId like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("name_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.OrgName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("address_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.RegisteredAddress1 like '%" + property.Value.ToString() + "%' or o.RegisteredAddress2 like '%" + property.Value.ToString() + "%' or o.RegisteredAddress3 like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("email_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.ATCDirectorEmailAddress like '%" + property.Value.ToString() + "%' or o.BillingContactEmailAddress like '%" + property.Value.ToString() + "%' or o.LegalContactEmailAddress like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("CSN_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "sr.CSN like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("notes_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "o.AdminNotes like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("contact_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "c.ContactName like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("invoices_org"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " or "; }
                        query += "i.InvoiceNumber like '%" + property.Value.ToString() + "%'";
                        i = i + 1;
                    }
                }
            }

            query += " GROUP BY o.OrgName asc";
            // Console.WriteLine(query);

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpPost("reportOrg")]

        public IActionResult findBy([FromBody] dynamic data)
        {
            JObject geo;
            JObject region;
            JObject subregion;
            JObject countries;
            JObject columnOrg;
            string anotherfiled = "";
            string query = "";
            string selectedColumns = "";
            string role = "";
            string orgid = "";
            string tmpstate = "";

            selectedColumns += "select DISTINCT ";

            int n = 0;
            JObject column = JObject.FromObject(data);
            foreach (JProperty property in column.Properties())
            {
                if (property.Name.Equals("fieldOrg"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        selectedColumns += property.Value.ToString();
                        //var kolomOrg = string.Join(",",property.Value.ToString());
                        if (selectedColumns.Contains("RegisteredCountryCode"))
                        {
                            selectedColumns = selectedColumns.Replace("o.RegisteredCountryCode", "tc.countries_name as 'Org Country'");

                        }
                        if (selectedColumns.Contains("ContractCountryCode"))
                        {
                            selectedColumns = selectedColumns.Replace("o.ContractCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = o.ContractCountryCode Limit 1) as 'Contract Country'");

                        }
                        if (selectedColumns.Contains("InvoicingCountryCode"))
                        {
                            selectedColumns = selectedColumns.Replace("o.InvoicingCountryCode", "(select Ref_countries.countries_name from Ref_countries where Ref_countries.countries_code = o.InvoicingCountryCode Limit 1) as 'Invoicing Country'");

                        }
                        if (selectedColumns.Contains("Status"))
                        {
                            selectedColumns = selectedColumns.Replace("o.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= o.Status)  'Status'");
                        }
                        n = n + 1;
                    }
                    else
                    {
                        sb = new StringBuilder();
                        sb.AppendFormat("SET group_concat_max_len = 1209;" +
                            "SELECT GROUP_CONCAT('o.',COLUMN_NAME) AS kolomOrg FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Main_organization'");
                        sqlCommand = new MySqlCommand(sb.ToString());
                        dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        columnOrg = JObject.Parse(hasil);
                        selectedColumns += columnOrg["kolomOrg"].ToString();
                        n = n + 1;
                    }
                }

                if (property.Name.Equals("anotherField"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (n == 0)
                        {
                            anotherfiled = property.Value.ToString();
                        }
                        else if (n > 0)
                        {
                            anotherfiled += "," + property.Value.ToString();
                        }
                        anotherfiled = anotherfiled.Replace("m.MarketType", "m.MarketType as 'Market Type'");
                        anotherfiled = anotherfiled.Replace("r.RoleName", "r.RoleName as 'Partner Type'");
                        anotherfiled = anotherfiled.Replace("sr.Status", "(select KeyValue from Dictionary d where d.Parent ='SiteStatus' and d.Key= sr.Status) as 'Partner Type Status'");
                        anotherfiled = anotherfiled.Replace("sr.CSN", "sr.CSN as 'Partner Type CSN'");
                        anotherfiled = anotherfiled.Replace("sr.ITS_ID", "sr.ITS_ID as 'ITS Site ID'");
                        anotherfiled = anotherfiled.Replace("sr.UParent_CSN", "sr.UParent_CSN as 'Ultimate Parent CSN'");
                        anotherfiled = anotherfiled.Replace("tc.geo_name", "tc.geo_name as 'Geo'");
                        anotherfiled = anotherfiled.Replace("tc.region_name", "tc.region_name as 'Region'");
                        anotherfiled = anotherfiled.Replace("tc.subregion_name", "tc.subregion_name as 'SubRegion'");
                        anotherfiled = anotherfiled.Replace("t.Territory_Name", "t.Territory_Name as 'Territory'");
                        selectedColumns += anotherfiled;
                        n = n + 1;

                    }
                }

                if ((property.Name.Equals("filterByRollup")))
                {

                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() == "Y"))
                    {

                        anotherfiled = selectedColumns.Replace("r.RoleName as 'Partner Type'", "group_concat(DISTINCT RoleName ORDER BY RoleName ) as 'Partner Type'");
                        selectedColumns = anotherfiled;
                    }


                }


                if (property.Name.Equals("Role"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        role = property.Value.ToString();
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        orgid = property.Value.ToString();
                    }
                }
                if (property.Name.Equals("state"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        tmpstate = "INNER JOIN States st on st.CountryCode = tc.countries_code ";
                    }
                }

            }

            query += selectedColumns + " FROM Main_organization o " +
            "INNER JOIN Ref_countries tc ON o.RegisteredCountryCode = tc.countries_code " +
            "LEFT JOIN Ref_territory t ON t.TerritoryId = tc.TerritoryId " + /*tmpstate +*/
            "INNER JOIN MarketType m ON tc.MarketTypeId = m.MarketTypeId " +
            "INNER JOIN Main_site ss ON ss.OrgId = o.OrgId inner join (select  SiteId, RoleId, max(SiteRoleId) maxid from SiteRoles group by SiteId, RoleId) MaxRole on ss.SiteId = MaxRole.SiteId INNER JOIN SiteRoles sr ON MaxRole.maxid = sr.SiteRoleId  " +
            "INNER JOIN Roles r ON r.RoleId = sr.RoleId " +
             " left Join RoleParams rp on rp.RoleParamId = sr.RoleParamId " +
            "WHERE  ";

            int i = 0;
            JObject json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("filterByName"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if ( i > 0) { query += " and "; }
                        query += "o.OrgName like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("partnerType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if ( i > 0) { query += " and "; }
                        query += "sr.RoleId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("partnerStatus"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if ( i > 0) { query += " and "; }
                        query += " ( sr.`Status` in (" + property.Value.ToString() + ") or  sr.`Status`= null or  sr.`Status` ='') ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("marketType"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if ( i > 0) { query += " and "; }
                        query += "m.MarketTypeId in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("geo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        sb = new StringBuilder();
                        // sb.AppendFormat("select group_concat(quote(geo_code)) as geo_code from Ref_geo where geo_id in ("+property.Value.ToString()+")");
                        // sqlCommand = new MySqlCommand(sb.ToString());
                        // dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        // hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        // geo = JObject.Parse(hasil);

                        // if(i==0 || i>0){query += " and ";} 
                        // query += "tc.geo_code in ("+geo["geo_code"].ToString()+") ";  
                        // i=i+1;

                        if ( i > 0) { query += " and "; }
                        query += "tc.geo_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("region"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // sb = new StringBuilder();
                        // sb.AppendFormat("select group_concat(quote(region_code)) as region_code from Ref_region where region_id in ("+property.Value.ToString()+")");
                        // sqlCommand = new MySqlCommand(sb.ToString());
                        // dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        // hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        // region = JObject.Parse(hasil);

                        // if(i==0 || i>0){query += " and ";} 
                        // query += "tc.region_code in ("+region["region_code"].ToString()+") ";  
                        // i=i+1;

                        if (i == 0 || i > 0) { query += " and "; }
                        query += "tc.region_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("subregion"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // sb = new StringBuilder();
                        // sb.AppendFormat("select group_concat(quote(subregion_code)) as subregion_code from Ref_subregion where subregion_id in ("+property.Value.ToString()+")");
                        // sqlCommand = new MySqlCommand(sb.ToString());
                        // dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        // hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        // subregion = JObject.Parse(hasil);

                        // if(i==0 || i>0){query += " and ";} 
                        // query += "tc.subregion_code in ("+subregion["subregion_code"].ToString()+") ";  
                        // i=i+1;

                        if (i == 0 || i > 0) { query += " and "; }
                        query += "tc.region_code in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("country"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        // sb = new StringBuilder();
                        // sb.AppendFormat("select group_concat(quote(countries_code)) as countries_code from Ref_countries where countries_id in ("+property.Value.ToString()+")");
                        // sqlCommand = new MySqlCommand(sb.ToString());
                        // dsTemp = oDb.getDataSetFromSP(sqlCommand);
                        // hasil = MySqlDb.GetJSONObjectString(dsTemp.Tables[0]);
                        // countries = JObject.Parse(hasil);

                        // if(i==0 || i>0){query += " and ";}
                        // query += "RegisteredCountryCode in ("+countries["countries_code"].ToString()+") ";  
                        // i=i+1;

                        if (i == 0 || i > 0) { query += " and "; }
                        query += "RegisteredCountryCode in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }

                if (property.Name.Equals("territory"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }

                        /*fix issue excel no 263.Show data based role*/

                        if (role == "")
                        {
                            query += "t.TerritoryId in (" + property.Value.ToString() + ") ";
                        }
                        else if (role == "DISTRIBUTOR")
                        {
                            query += "t.TerritoryId in (select rc.TerritoryId from Ref_countries rc INNER JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + orgid + ") GROUP BY rc.TerritoryId) ";
                        }
                        else
                        {
                            query += "t.TerritoryId in (" + property.Value.ToString() + ") AND o.OrgId in (" + orgid + ") ";
                        }

                        /*end fix issue excel no 263.Show data based role*/

                        i = i + 1;
                    }
                }
                if (property.Name.Equals("city"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "RegisteredCity in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("state"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0 || i > 0) { query += " and "; }
                        query += "RegisteredStateProvince in (" + property.Value.ToString() + ") ";
                        i = i + 1;
                    }
                }
                if (property.Name.Equals("filterByRollup"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null) && (property.Value.ToString() == "Y"))
                    {
                        if (i == 0 || i > 0)
                            query += " group by o.OrgId,o.OrgName,tc.countries_name";
                        i = i + 1;
                    }
                }

            }



            query += "  order by o.OrgId desc";
            List<dynamic> dynamicList = new List<dynamic>();
            try
            {

                sb = new StringBuilder();
                sb.AppendFormat(query);

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    //ds = EscapeSpecialChar(ds);
                    dynamicList = dataService.ToDynamic(ds.Tables[0]);
                }
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                //result = MySqlDb.SerializeDataTable(ds.Tables[0]);    
                var settings = new JsonSerializerSettings
                {

                };
                settings.Converters.Add(new DataSetConverter());
                settings.ContractResolver = new DataServices.NullToEmptyStringResolver();

                result = JsonConvert.SerializeObject(dynamicList, Formatting.Indented,
                    settings);
            }
            return Ok(result);
        }

        private string ValidateFile(dynamic files)
        {
            string invalidFile =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Invalid input file\"" +
                "}";
            string exceed =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Maximum file exceed \"" +
                "}";

            string invalidFileSize =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Ivalid file size \"" +
                "}";
            JArray arryFiles = JsonConvert.DeserializeObject(files.ToString());
            if (files.Count > 5)
            {
                return exceed;
            }
            foreach (var item in files)
            {
                string base64String = item.ToString();
                var file = JsonConvert.DeserializeObject<FileModel>(base64String);
                file.metadata = file.data.Remove(file.data.LastIndexOf(','), file.data.Length - file.data.LastIndexOf(','));
                file.type = file.metadata.GetFileTypeFromBase64();
                if (string.IsNullOrEmpty(file.type))
                {
                    return invalidFile;
                }
                file.Content = Convert.FromBase64String(file.data.Remove(0, file.data.LastIndexOf(',') + 1));
                if (file.Content.Length > 2000000)
                {
                    return invalidFileSize;
                }

            }
            return "true";

        }


        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {
            JObject NewData = new JObject();
            var listFile = new List<FileModel>();
            if (data != null)
            {
                if (data.Files != null)
                {
                    var validRes = ValidateFile(data.Files);
                    if (validRes != "true")
                    {
                        return Ok(validRes);
                    }

                    foreach (var item in data.Files)
                    {
                        string base64String = item.ToString();
                        var file = JsonConvert.DeserializeObject<FileModel>(base64String);
                        file.metadata = file.data.Remove(file.data.LastIndexOf(','), file.data.Length - file.data.LastIndexOf(','));
                        file.type = file.metadata.GetFileTypeFromBase64();
                        file.Content = Convert.FromBase64String(file.data.Remove(0, file.data.LastIndexOf(',') + 1));
                        listFile.Add(file);
                    }
                }
            }

            //var logoFile = new List<FileModel>();
            //if (data != null)
            //{
            //    if (data.orgLogo != null)
            //    {
            //        //var validRes = ValidateFile(data.Files);
            //        //if (validRes != "true")
            //        //{
            //        //    return Ok(validRes);
            //        //}

            //        string base64String = data.orgLogo.ToString();
            //        var logo = JsonConvert.DeserializeObject<FileModel>(base64String);
            //        logo.metadata = logo.name;
            //        logo.type = logo.type;
            //        logo.Content = Convert.FromBase64String(logo.data.Remove(0, logo.data.LastIndexOf(',') + 1));
            //        logoFile.Add(logo);
            //    }
            //}
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;

            int count21 = 0;
            // int count22 = 0;
            int count23 = 0;
            JObject o1;
            JObject o2;
            JObject o3 = null;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Insert Data Failed\"" +
                "}";

            ////sb = new StringBuilder();
            ////sb.AppendFormat("select count(*) as count from Main_organization;");
            ////sqlCommand = new MySqlCommand(sb.ToString());
            ////ds = oDb.getDataSetFromSP(sqlCommand);

            ////result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            ////o1 = JObject.Parse(result);
            ////count1 = Int32.Parse(o1["count"].ToString());
            ////// Console.WriteLine(count1); 
            ////count3 = count1 + 1;

            // var OrgName = o3["OrgName"].ToString().Substring(0, 2);
            sb = new StringBuilder();
            sb.AppendFormat("select  max(Cast(substring(OrgId, length('ORG') +1) as UNSIGNED INTEGER))+1 as OrgIdCount from Main_organization where OrgId like 'ORG%' ;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result);
            count3 = Int32.Parse(o1["OrgIdCount"].ToString());

            // sb = new StringBuilder();
            // sb.AppendFormat("select count(*) as count from Main_site;");
            // sqlCommand = new MySqlCommand(sb.ToString());
            // ds = oDb.getDataSetFromSP(sqlCommand);

            // result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            // o1 = JObject.Parse(result);
            // count21 = Int32.Parse(o1["count"].ToString());

            //count23 = count21 + 1;
          
            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Main_organization " +
                    "(OrgId, OrgName, EnglishOrgName, CommercialOrgName, ATCCSN, VARCSN, AUPCSN, TaxExempt, VATNumber, YearJoined, OrgWebAddress, " +
                    "RegisteredDepartment, RegisteredAddress1, RegisteredAddress2, RegisteredAddress3, RegisteredCity, RegisteredStateProvince, RegisteredPostalCode, RegisteredCountryCode, " +
                    "ContractDepartment, ContractAddress1, ContractAddress2, ContractAddress3, ContractCity, ContractStateProvince, ContractPostalCode, ContractCountryCode, " +
                    "InvoicingDepartment, InvoicingAddress1, InvoicingAddress2, InvoicingAddress3, InvoicingCity, InvoicingStateProvince, InvoicingPostalCode, InvoicingCountryCode, " +
                    "ATCDirectorFirstName, ATCDirectorLastName, ATCDirectorEmailAddress, ATCDirectorTelephone, ATCDirectorFax, " +
                    "LegalContactFirstName, LegalContactLastName, LegalContactEmailAddress, LegalContactTelephone, LegalContactFax, " +
                    "BillingContactFirstName, BillingContactLastName, BillingContactEmailAddress, BillingContactTelephone, BillingContactFax, " +
                    "AdminNotes, Status, DateAdded, AddedBy,DateLastAdmin,LastAdminBy)" +
                    "values ('ORG" + count3.ToString("0000") + "','" + data.OrgName + "','" + data.EnglishOrgName + "','" + data.CommercialOrgName + "','" + data.ATCCSN + "','" + data.VARCSN + "','" + data.AUPCSN + "','" + data.TaxExempt + "','" + data.VATNumber + "','" + data.YearJoined + "','" + data.OrgWebAddress + "','" +
                    data.RegisteredDepartment + "','" + data.RegisteredAddress1 + "','" + data.RegisteredAddress2 + "','" + data.RegisteredAddress3 + "','" + data.RegisteredCity + "','" + data.RegisteredStateProvince + "','" + data.RegisteredPostalCode + "','" + data.RegisteredCountryCode + "','" +
                    data.ContractDepartment + "','" + data.ContractAddress1 + "','" + data.ContractAddress2 + "','" + data.ContractAddress3 + "','" + data.ContractCity + "','" + data.ContractStateProvince + "','" + data.ContractPostalCode + "','" + data.ContractCountryCode + "','" +
                    data.InvoicingDepartment + "','" + data.InvoicingAddress1 + "','" + data.InvoicingAddress2 + "','" + data.InvoicingAddress3 + "','" + data.InvoicingCity + "','" + data.InvoicingStateProvince + "','" + data.InvoicingPostalCode + "','" + data.InvoicingCountryCode + "','" +
                    data.ATCDirectorFirstName + "','" + data.ATCDirectorLastName + "','" + data.ATCDirectorEmailAddress + "','" + data.ATCDirectorTelephone + "','" + data.ATCDirectorFax + "','" +
                    data.LegalContactFirstName + "','" + data.LegalContactLastName + "','" + data.LegalContactEmailAddress + "','" + data.LegalContactTelephone + "','" + data.LegalContactFax + "','" +
                    data.BillingContactFirstName + "','" + data.BillingContactLastName + "','" + data.BillingContactEmailAddress + "','" + data.BillingContactTelephone + "','" + data.BillingContactFax + "','" +
                    data.AdminNotes + "','P',NOW(),'" + data.AddedBy + ",Now(),'"+ data.cuid + "');"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat("select o.OrganizationId, o.OrgId, replace( replace( replace( o.OrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as OrgName," +
                " replace( replace( replace( o.EnglishOrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as EnglishOrgName," +
                " replace( replace( replace( o.CommercialOrgName, '\"' , '\\\\\"' ), '\\\\','\\\\\\\\'), '\\\\\"' , '\\\"' ) as CommercialOrgName, OrgWebAddress, " +
                " geo_name from Main_organization o left join Ref_countries on RegisteredCountryCode = countries_code" +
                " where OrganizationId = (SELECT MAX(OrganizationId) FROM Main_organization)");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);

                if (data.orgLogo != null)
                {
                    var listOrgLogo = new List<OrgSiteAttachment>();
                    var orgLogoEntity = new OrgSiteAttachment
                    {
                        OrgId = o3["OrgId"].ToString(),
                        Type = data.orgLogo["filetype"].ToString(),
                        Content = Convert.FromBase64String(data.orgLogo["value"].ToString()),
                        DocType = "logo",
                        Name = data.orgLogo["filename"].ToString(),
                        Metadata = ""
                    };
                    listOrgLogo.Add(orgLogoEntity);
                    _mySQLContext.OrgSiteAttachment.AddRange(listOrgLogo);
                    _mySQLContext.SaveChanges();
                }


                //Modified by SMH on 2019-03-05(for duplicate SiteId)
                var SiteName = o3["geo_name"].ToString().Substring(0, 2);
                sb = new StringBuilder();
                sb.AppendFormat("select  max(Cast(substring(SiteId, length('" + SiteName + "') +1) as UNSIGNED INTEGER))+1 as SiteIdCount from Main_site where SiteId like '" + SiteName + "%' ;");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o1 = JObject.Parse(result);
                count23 = Int32.Parse(o1["SiteIdCount"].ToString());




                string nameorgsite = o3["OrgName"].ToString().Replace("\'", "\\\'");
                var SiteId = o3["geo_name"].ToString().Substring(0, 2) + "" + count23.ToString("0000");
                sb = new StringBuilder();
                //changes by Soe on 11/12/2018
                sb.AppendFormat(
                   "insert into Main_site " +
                   "(SiteId, OrgId, SiteName, EnglishSiteName, CommercialSiteName, SiteWebAddress, SiteCountryCode, Workstations, MagellanId, SiteTelephone, SiteFax, SiteEmailAddress, SiebelSiteName, " +
                   "SiebelDepartment, SiebelAddress1, SiebelAddress2, SiebelAddress3, SiebelCity, SiebelStateProvince, SiebelCountryCode, SiebelPostalCode, " +
                   "SiteDepartment, SiteAddress1, SiteAddress2, SiteAddress3, SiteCity, SiteStateProvince, SitePostalCode, " +
                   "MailingDepartment, MailingAddress1, MailingAddress2, MailingAddress3, MailingCity, MailingStateProvince, MailingCountryCode, MailingPostalCode, " +
                   "ShippingDepartment, ShippingAddress1, ShippingAddress2, ShippingAddress3, ShippingCity, ShippingStateProvince, ShippingCountryCode, ShippingPostalCode, " +
                   "SiteManagerFirstName, SiteManagerLastName, SiteManagerEmailAddress, SiteManagerTelephone, SiteManagerFax, " +
                   "SiteAdminFirstName, SiteAdminLastName, SiteAdminEmailAddress, SiteAdminTelephone, SiteAdminFax, " +
                   "AdminNotes, Status, DateAdded, AddedBy,DateLastAdmin,LastAdminBy)" +
                   "values ('" + SiteId + "','" + o3["OrgId"] + "','" + nameorgsite + "','" +
                   o3["EnglishOrgName"] + "','" + o3["CommercialOrgName"] + "','" + o3["OrgWebAddress"] + "','" + data.RegisteredCountryCode + "','" + 0 + "','','','',''," +
                   "'','','','','','','','','','','" + data.RegisteredAddress1 + "','" + data.RegisteredAddress2 + "','" + data.RegisteredAddress3 + "','" + data.RegisteredCity +
                   "','" + data.RegisteredStateProvince + "','" + data.RegisteredPostalCode + "','','" + data.InvoicingAddress1 + "','" + data.InvoicingAddress2 + "','" + data.InvoicingAddress3 + "','" + data.InvoicingCity + "','" + data.InvoicingStateProvince + "','" + data.InvoicingCountryCode + "','" + data.InvoicingPostalCode +
                   "','','" + data.ContractAddress1 + "','" + data.ContractAddress2 + "','" + data.ContractAddress3 + "','" + data.ContractCity + "','" + data.ContractStateProvince + "','" + data.ContractCountryCode + "','" + data.ContractPostalCode +
                   "','" + data.ATCDirectorFirstName + "','" + data.ATCDirectorLastName + "','" + data.ATCDirectorEmailAddress + "','" + data.ATCDirectorTelephone + "','','" + data.ATCDirectorFirstName +
                   "','" + data.ATCDirectorLastName + "','" + data.ATCDirectorEmailAddress + "','" + data.ATCDirectorTelephone + "',''," +
                   "'','I',NOW(),'" + data.AddedBy + "',Now(),'" + data.cuid + "');"
               );

                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Main_organization set " +
                    "PrimarySiteId='" + SiteId + "' " +
                    "where OrgId = '" + o3["OrgId"] + "'");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);


                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into SiteRoles " +
                    "(SiteId, RoleId, RoleParamId,DateAdded, Status, AddedBy, PartyId, CSN, ParentCSN, UParent_CSN,ContractCSN,ExternalId)" +
                    "Values ('" + SiteId + "', '1','2', NOW(), 'P', '" + data.AddedBy + "', '1', '', '', '','',0 );");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);


                if (listFile?.Count > 0)
                {
                    var listorgSiteAttachment = new List<OrgSiteAttachment>();
                    foreach (var item in listFile)
                    {
                        var orgSiteAttachmentEntity = new OrgSiteAttachment
                        {
                            OrgId = o3["OrgId"].ToString(),
                            Type = item.type,
                            Content = item.Content,
                            DocType = "Add",
                            Name = item.name,
                            Metadata = item.metadata
                        };
                        listorgSiteAttachment.Add(orgSiteAttachmentEntity);
                    }
                    _mySQLContext.OrgSiteAttachment.AddRange(listorgSiteAttachment);
                    _mySQLContext.SaveChanges();
                }


                //if (count2 > count1)
                //{
                //    string description = "Insert data Main_site" + "<br/>" +
                //        "SiteId= " + SiteId + "<br/>" +
                //    "OrgId = " + o3["OrgId"] + "<br/>" +
                //  "SiteName = " + nameorgsite + "<br/>" +
                //        "EnglishSiteName = " + o3["EnglishOrgName"] + "<br/>" +
                //        "CommercialSiteName = " + o3["CommercialOrgName"] + "<br/>" +
                //        "SiteWebAddress = " + o3["OrgWebAddress"] + "<br/>" +
                //        "SiteCountryCode = " + data.RegisteredCountryCode + "<br/>" +
                //        "Workstations = " + 0 + "<br/>" +
                //        "MagellanId = " + "" + "<br/>" +
                //        "SiteTelephone = " + "" + "<br/>" +
                //        "SiteFax = " + "" + "<br/>" +
                //        "SiteEmailAddress = " + "" + "<br/>" +
                //        "SiebelSiteName = " + "" + "<br/>" +
                //        "SiebelDepartment = " + "" + "<br/>" +
                //        "SiebelAddress1 = " + "" + "<br/>" +
                //        "SiebelAddress2 = " + "" + "<br/>" +
                //        "SiebelAddress3 = " + "" + "<br/>" +
                //        "SiebelCity = " + "" + "<br/>" +
                //        "SiebelStateProvince = " + "" + "<br/>" +
                //        "SiebelCountryCode = "+""+"<br/>" +
                //        "SiebelPostalCode = " + "" + "<br/>" +
                //        "SiteDepartment = " +"" + "<br/>" +
                //        "SiteAddress1 = " + data.RegisteredAddress1 + "<br/>" +
                //        "SiteAddress2 = " + data.RegisteredAddress2 + "<br/>" +
                //        "SiteAddress3 = " + data.RegisteredAddress3 + "<br/>" +
                //        "SiteCity = " + data.RegisteredCity + "<br/>" +
                //        "SiteStateProvince = " + data.InvoicingStateProvince + "<br/>" +
                //        "SitePostalCode = " + data.RegisteredPostalCode + "<br/>" +
                //        "MailingDepartment = " + "" + "<br/>" +
                //        "MailingAddress1 = " + data.InvoicingAddress1 + "<br/>" +
                //        "MailingAddress2 = " + data.InvoicingAddress2 + "<br/>" +
                //        "MailingAddress3 = " + data.InvoicingAddress3 + "<br/>" +
                //        "MailingCity = " + data.InvoicingCity + "<br/>" +
                //        "MailingStateProvince = " + data.InvoicingStateProvince + "<br/>" +
                //        "MailingCountryCode = " + data.InvoicingCountryCode + "<br/>" +
                //         "MailingPostalCode = " + data.InvoicingPostalCode + "<br/>" +
                //        "ShippingDepartment = " + "" + "<br/>" +
                //        "ShippingAddress1 = " + data.ContractAddress1 + "<br/>" +
                //        "ShippingAddress2 = " + data.ContractAddress2 + "<br/>" +
                //         "ShippingAddress3 = " + data.ContractAddress3 + "<br/>" +
                //         "ShippingCity = " + data.ContractCity + "<br/>" +
                //         "ShippingStateProvince = " + data.ContractStateProvincee + "<br/>" +
                //         "ShippingCountryCode = " + data.ContractCountryCode + "<br/>" +
                //         "ShippingPostalCode = " + data.ContractPostalCode + "<br/>" +
                //         "SiteManagerFirstName = " + data.ATCDirectorFirstName + "<br/>" +
                //         "SiteManagerLastName = " + data.ATCDirectorLastName + "<br/>" +
                //          "SiteManagerEmailAddress = " + data.ATCDirectorEmailAddress + "<br/>" +
                //          "SiteManagerTelephone = " + data.ATCDirectorTelephone + "<br/>" +
                //          "SiteManagerFax = " + "" + "<br/>" +
                //          "SiteAdminFirstName = " + data.ATCDirectorFirstName + "<br/>" +
                //          "SiteAdminLastName = " + data.ATCDirectorLastName + "<br/>" +
                //          "SiteAdminEmailAddress = " + data.ATCDirectorEmailAddress + "<br/>" +
                //          "SiteAdminTelephone = " + data.ATCDirectorTelephone + "<br/>" +
                //          "SiteAdminFax = " + "" + "<br/>" +
                //           "AdminNotes = " + "" + "<br/>" +
                //           "Status = " + "I" + "<br/>" +
                //           "DateAdded = " + "Now()" + "<br/>" +
                //           " AddedBy = " + data.AddedBy;








                //    _audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());


                //    res =
                //        "{" +
                //        "\"code\":\"1\"," +
                //        "\"message\":\"Insert Data Success\"," +
                //        "\"OrganizationId\":\"" + o3["OrganizationId"].ToString() + "\"" +
                //        "}";
                //}



                // sb = new StringBuilder();
                // sb.AppendFormat(
                //     "insert into Main_site "+
                //     "(SiteId, OrgId, SiteName, EnglishSiteName, CommercialSiteName,  SiteWebAddress, Status, SiteCountryCode) "+
                //     "values ('"+o3["region_code"].ToString().Substring(0,2)+""+count23.ToString("0000")+"','"+o3["OrgId"]+"','"+o3["OrgName"]+"','"+o3["EnglishOrgName"]+"','"+o3["CommercialOrgName"]+"','"+o3["OrgWebAddress"]+"','A','"+data.RegisteredCountryCode+"');"
                // );
                // sqlCommand = new MySqlCommand(sb.ToString());
                // ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select OrganizationId, count(*) as count from Main_organization ORDER BY OrganizationId DESC");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result);
            count2 = Int32.Parse(o2["count"].ToString());

            sb = new StringBuilder();
            sb.AppendFormat("select * from Main_organization where OrganizationId =" + o3["OrganizationId"] + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            // Console.WriteLine(count2);

            if (count2 > count1)
            {


                JObject OldData = new JObject();
                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Main_organization, data.UserId.ToString(), data.cuid.ToString(), " where OrganizationId = " + o3["OrganizationId"] + ""))
                {
                    res =
                    "{" +
                    "\"code\":\"1\"," +
                    "\"message\":\"insert data success\"," +
                    "\"organizationid\":\"" + o3["OrganizationId"].ToString() + "\"" +
                    "}";
                }


            }

            return Ok(res);

        }





        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {


            sb = new StringBuilder();
            sb.AppendFormat("select * from Main_organization where OrganizationId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());



            //JObject o3;
            string res =
                "{" +
                "\"code\":\"0\"," +
                "\"message\":\"Update Data Failed\"" +
                "}";
            var listFile = new List<FileModel>();
            if (data != null)
            {


                if (data.Files != null)
                {
                    var validRes = ValidateFile(data.Files);
                    if (validRes != "true")
                    {
                        return Ok(validRes);
                    }

                    foreach (var item in data.Files)
                    {
                        string base64String = item.ToString();
                        var file = JsonConvert.DeserializeObject<FileModel>(base64String);
                        file.metadata = file.data.Remove(file.data.LastIndexOf(','), file.data.Length - file.data.LastIndexOf(','));
                        file.type = file.metadata.GetFileTypeFromBase64();
                        file.Content = Convert.FromBase64String(file.data.Remove(0, file.data.LastIndexOf(',') + 1));
                        listFile.Add(file);
                    }
                }
            }

            try
            {
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Main_organization set " +
                    "OrgName='" + data.OrgName + "'," +
                    "PrimarySiteId='" + data.PrimarySiteId + "'," +
                    "EnglishOrgName='" + data.EnglishOrgName + "'," +
                    "CommercialOrgName='" + data.CommercialOrgName + "'," +
                    "VARCSN='" + data.VARCSN + "'," +
                    "ATCCSN='" + data.ATCCSN + "'," +
                    "AUPCSN='" + data.AUPCSN + "'," +
                    "TaxExempt='" + data.TaxExempt + "'," +
                    "VATNumber='" + data.VATNumber + "'," +
                    "YearJoined='" + data.YearJoined + "'," +
                    "OrgWebAddress='" + data.OrgWebAddress + "'," +
                    "RegisteredDepartment='" + data.RegisteredDepartment + "'," +
                    "RegisteredAddress1='" + data.RegisteredAddress1 + "'," +
                    "RegisteredAddress2='" + data.RegisteredAddress2 + "'," +
                    "RegisteredAddress3='" + data.RegisteredAddress3 + "'," +
                    "RegisteredCity='" + data.RegisteredCity + "'," +
                    "RegisteredStateProvince='" + data.RegisteredStateProvince + "'," +
                    "RegisteredPostalCode='" + data.RegisteredPostalCode + "'," +
                    "RegisteredCountryCode='" + data.RegisteredCountryCode + "'," +
                    "InvoicingDepartment='" + data.InvoicingDepartment + "'," +
                    "InvoicingAddress1='" + data.InvoicingAddress1 + "'," +
                    "InvoicingAddress2='" + data.InvoicingAddress2 + "'," +
                    "InvoicingAddress3='" + data.InvoicingAddress3 + "'," +
                    "InvoicingCity='" + data.InvoicingCity + "'," +
                    "InvoicingStateProvince='" + data.InvoicingStateProvince + "'," +
                    "InvoicingPostalCode='" + data.InvoicingPostalCode + "'," +
                    "InvoicingCountryCode='" + data.InvoicingCountryCode + "'," +
                    "ContractDepartment='" + data.ContractDepartment + "'," +
                    "ContractAddress1='" + data.ContractAddress1 + "'," +
                    "ContractAddress2='" + data.ContractAddress2 + "'," +
                    "ContractAddress3='" + data.ContractAddress3 + "'," +
                    "ContractCity='" + data.ContractCity + "'," +
                    "ContractStateProvince='" + data.ContractStateProvince + "'," +
                    "ContractPostalCode='" + data.ContractPostalCode + "'," +
                    "ContractCountryCode='" + data.ContractCountryCode + "'," +
                    "ATCDirectorFirstName='" + data.ATCDirectorFirstName + "'," +
                    "ATCDirectorLastName='" + data.ATCDirectorLastName + "'," +
                    "ATCDirectorEmailAddress='" + data.ATCDirectorEmailAddress + "'," +
                    "ATCDirectorTelephone='" + data.ATCDirectorTelephone + "'," +
                    "ATCDirectorFax='" + data.ATCDirectorFax + "'," +
                    "LegalContactFirstName='" + data.LegalContactFirstName + "'," +
                    "LegalContactLastName='" + data.LegalContactLastName + "'," +
                    "LegalContactEmailAddress='" + data.LegalContactEmailAddress + "'," +
                    "LegalContactTelephone='" + data.LegalContactTelephone + "'," +
                    "LegalContactFax='" + data.LegalContactFax + "'," +
                    "BillingContactFirstName='" + data.BillingContactFirstName + "'," +
                    "BillingContactLastName='" + data.BillingContactLastName + "'," +
                    "BillingContactEmailAddress='" + data.BillingContactEmailAddress + "'," +
                    "BillingContactTelephone='" + data.BillingContactTelephone + "'," +
                    "BillingContactFax='" + data.BillingContactFax + "'," +
                    "AdminNotes='" + data.AdminNotes + "'," +
                    "Status='A'," +
                    "DateLastAdmin=NOW()," +
                    "LastAdminBy='" + data.LastAdminBy + "'" +
                    "where OrganizationId='" + id + "'"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

            }
            catch
            {
                // Console.WriteLine("failed");  
            }
            finally
            {
            }

            sb = new StringBuilder();
            sb.AppendFormat("select * from Main_organization where OrganizationId =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            if (listFile?.Count > 0)
            {
                var listorgSiteAttachment = new List<OrgSiteAttachment>();
                foreach (var item in listFile)
                {
                    var orgSiteAttachmentEntity = new OrgSiteAttachment
                    {
                        OrgId = data.OrgId,
                        Type = item.type,
                        Content = item.Content,
                        DocType = "Add",
                        Name = item.name,
                        Metadata = item.metadata
                    };
                    listorgSiteAttachment.Add(orgSiteAttachmentEntity);
                }
                _mySQLContext.OrgSiteAttachment.AddRange(listorgSiteAttachment);
                _mySQLContext.SaveChanges();
            }

            if (data.orgLogo != null)
            {
                string OrgId = data.OrgId.ToString();
                if (data.LogoId != 0)
                {
                    var updateLogo = _mySQLContext.OrgSiteAttachment.Where(o => o.OrgId == OrgId && o.DocType == "logo").ToList();
                    foreach (var item in updateLogo)
                    {
                        item.Type = data.orgLogo["filetype"].ToString();
                        item.Content = Convert.FromBase64String(data.orgLogo["value"].ToString());
                        item.Name = data.orgLogo["filename"].ToString();
                        _mySQLContext.OrgSiteAttachment.Update(item);
                    }
                }
                else
                {
                    var listOrgLogo = new List<OrgSiteAttachment>();
                    var orgLogoEntity = new OrgSiteAttachment
                    {
                        OrgId = data.OrgId,
                        Type = data.orgLogo["filetype"].ToString(),
                        Content = Convert.FromBase64String(data.orgLogo["value"].ToString()),
                        DocType = "logo",
                        Name = data.orgLogo["filename"].ToString(),
                        Metadata = ""
                    };
                    listOrgLogo.Add(orgLogoEntity);
                    _mySQLContext.OrgSiteAttachment.AddRange(listOrgLogo);
                }
                _mySQLContext.SaveChanges();
            }



            if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Main_organization, data.UserId.ToString(), data.cuid.ToString(), " where OrganizationId = " + id + ""))
            {
                res =
                "{" +
                "\"code\":\"1\"," +
                "\"message\":\"Update Data Success\"" +
                "}";
            }
            return Ok(res);
        }



        [HttpPost("Delete")]
        public IActionResult Delete([FromBody] dynamic data)
        {
            string res =
             "{" +
             "\"code\":\"0\"," +
             "\"message\":\"Delete Data Failed\"" +
             "}";
            JObject OldData, NewData = null;
            int? orgId = data.id;

            var userId = HttpContext.User.GetUserId();

            var contact = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactId == userId);

            try
            {
                if (orgId != null && contact != null)
                {
                    var orgEntity = _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrganizationId == orgId && o.Status != AutodeskConst.StatusConts.X);

                    if (orgEntity != null)
                    {
                        OldData = JObject.FromObject(orgEntity);

                        orgEntity.Status = AutodeskConst.StatusConts.X;
                        orgEntity.DateLastAdmin = DateTime.Now;
                        orgEntity.LastAdminBy = contact.ContactName;

                        _mySQLContext.MainOrganization.Update(orgEntity);

                        var listSite = _mySQLContext.MainSite.Where(s => s.OrgId == orgEntity.OrgId && s.Status != AutodeskConst.StatusConts.X).ToList();
                        listSite.ForEach(x =>
                        {
                            x.Status = AutodeskConst.StatusConts.X;
                            x.DateLastAdmin = DateTime.Now;
                            x.LastAdminBy = contact.ContactName;
                        });
                        _mySQLContext.MainSite.UpdateRange(listSite);

                        _mySQLContext.SaveChanges();

                        NewData = JObject.FromObject(orgEntity);

                        _audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Main_organization, userId, contact.ContactName, " OrganizationId  =" + data.id + "");
                    }
                    var listFile = new List<FileModel>();
                    if (data != null)
                    {
                        if (data.files != null && data.files.Count > 0)
                        {
                            var validRes = ValidateFile(data.files);
                            if (validRes != "true")
                            {
                                return Ok(validRes);
                            }

                            foreach (var item in data.files)
                            {
                                string base64String = item.ToString();
                                var file = JsonConvert.DeserializeObject<FileModel>(base64String);
                                file.type = file.data.GetFileTypeFromBase64();
                                file.Content = Convert.FromBase64String(file.data.Remove(0, file.data.LastIndexOf(',') + 1));
                                listFile.Add(file);
                            }
                        }
                    }


                    if (listFile.Count > 0)
                    {
                        var listorgSiteAttachment = new List<OrgSiteAttachment>();
                        foreach (var item in listFile)
                        {
                            var orgSiteAttachmentEntity = new OrgSiteAttachment
                            {
                                OrgId = data.orgId.ToString(),
                                SiteId = null,
                                Type = item.type,
                                Content = item.Content,
                                DocType = "Delete",
                                Name = item.name
                            };
                            listorgSiteAttachment.Add(orgSiteAttachmentEntity);
                        }
                        _mySQLContext.OrgSiteAttachment.AddRange(listorgSiteAttachment);
                        _mySQLContext.SaveChanges();
                    }
                    res =
                       "{" +
                       "\"code\":\"1\"," +
                       "\"message\":\"Delete Data Success\"," +
                       "\"OrgId\":\"" + data.orgId + "\"" +
                       "}";



                }
                return Ok(res);
            }
            catch (Exception e)
            {
                return Ok(res);
            }

        }

        [HttpPost("FilterGeneral")]
        public async Task<IActionResult> FilterGeneral([FromBody] GeneralSearchDto data)
        {

            if (string.IsNullOrEmpty(data.searchText))
            {
                return Ok("[]");
            }

            var settings = new JsonSerializerSettings { ContractResolver = new DefaultContractResolver() };
            var generalSearchResult = new GeneralSearchResult();
            var showDeleted = 0 ;
            if (data.showDataBy == "showdeletedonly")
            {
                showDeleted = 0;
            }
            else if (data.showDataBy == "hidedeleted")
            {
                showDeleted = 1;
            }
            else
                showDeleted = 2;

            var trimSearchText = data.searchText.Trim();
            string orgQuery = $"call GeneralSearchOrg('{trimSearchText}','{showDeleted}');";
            string siteQuery = $"call GeneralSearchSite('{trimSearchText}','{showDeleted}');";
            var contactQuery = $"call GeneralSearchContact('{trimSearchText}','{showDeleted}')";
            using (var command = _mySQLContext.Database.GetDbConnection().CreateCommand())
            {

                command.CommandText = orgQuery;

                _mySQLContext.Database.OpenConnection();

                using (var reader = command.ExecuteReader())
                {
                    var json = await Task.Run(() => reader.ToJsonString());
                    var model = JsonConvert.DeserializeObject<List<GeneralSearchOrgDto>>(json);
                    var listSearchRecord = await Task.Run(() => model.Select(x => x.Map<GeneralSearchOrgResultDto>()).ToList());
                    foreach (var item in listSearchRecord)
                    {
                        SetFoundInformation(item, trimSearchText);
                    }
                    generalSearchResult.Orgs = listSearchRecord.Where(d => d.FoundInformation != null).ToList(); ;
                }

                command.CommandText = siteQuery;

                using (var reader = command.ExecuteReader())
                {
                    var json = await Task.Run(() => reader.ToJsonString());
                    var model = JsonConvert.DeserializeObject<List<GeneralSearchSiteDto>>(json);
                    var listSearchRecord = await Task.Run(() => model.Select(x => x.Map<GeneralSearchSiteResultDto>()).ToList());
                    foreach (var item in listSearchRecord)
                    {
                        SetFoundInformation(item, trimSearchText);
                    }
                    generalSearchResult.Sites = listSearchRecord.Where(d => d.FoundInformation != null).ToList();
                }

                command.CommandText = contactQuery;


                using (var reader = command.ExecuteReader())
                {
                    var json = await Task.Run(() => reader.ToJsonString());
                    var model = JsonConvert.DeserializeObject<List<GeneralSearchContactDto>>(json);
                    var listSearchRecord = await Task.Run(() => model.Select(x => x.Map<GeneralSearchContactResultDto>()).ToList());
                    foreach (var item in listSearchRecord)
                    {

                        SetFoundInformation(item, trimSearchText);

                    }
                    generalSearchResult.Contacts = listSearchRecord.Where(d => d.FoundInformation != null).ToList();
                }



            }

            try
            {
                //return Ok(generalSearchResult);
                return Ok(JsonConvert.SerializeObject(generalSearchResult, settings));
            }
            catch (Exception)
            {

                return Ok("[]");
            }


        }

        private void SetFoundInformation<T>(T item, string searchText)
        {

            var listInfor = new List<string>();
            foreach (var propertyInfo in item.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                var propValue = propertyInfo.GetValue(item, null);
                if (propValue != null && propValue.ToString().ToLower().Contains(searchText.ToLower()))
                {
                    listInfor.Add(string.Join(" - ", propValue.ToString(), propertyInfo.Name));
                    var foundInformation = item.GetType().GetProperty("FoundInformation");
                    foundInformation.SetValue(item, listInfor);

                }

            }
        }

        [HttpPost("populateByCountry")]
        public IActionResult populateByCountry([FromBody] dynamic data)
        {
            string query = "";

            var json = JObject.FromObject(data);
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("Country"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "SELECT o.OrgId FROM Main_organization o INNER JOIN Main_site s ON o.OrgId = s.OrgId WHERE o.RegisteredCountryCode in (" + property.Value.ToString() + ") ";
                    }
                }
            }

            /* poho kudu based role hm */
            foreach (JProperty property in json.Properties())
            {
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.SiteId in (" + property.Value.ToString() + ") ";
                    }
                }

                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND o.OrgId in (" + property.Value.ToString() + ") ";
                    }
                }

                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        query += "AND s.SiteCountryCode in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) ";
                    }
                }
            }

            query += "GROUP BY o.OrgId";

            /* poho kudu based role hm */

            // Console.WriteLine(query);

            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("getFile")]
        public async Task<IActionResult> GetOrgSiteAttachmentFile(int id)
        {
            var orgSiteAttachment = await Task.Run(() => _mySQLContext.OrgSiteAttachment.Find(id));
            if (orgSiteAttachment != null)
            {

                System.Net.Mime.ContentDisposition cd = new System.Net.Mime.ContentDisposition
                {
                    FileName = orgSiteAttachment.Name,
                    Inline = false  // false = prompt the user for downloading;  true = browser to try to show the file inline
                };
                Response.Headers.Add("Content-Disposition", cd.ToString());
                return File(orgSiteAttachment.Content, "application/octet-stream");
            }

            //var base64Str = Convert.ToBase64String(orgSiteAttachment.Content);
            //return Ok(orgSiteAttachment.Metadata + base64Str);

            return Ok();
        }

        [HttpPost("DeleteFile")]
        public async Task<IActionResult> DeleteOrgSiteAttachmentFile([FromBody]List<int> ids)
        {
            var res =
                "{" +
                "\"code\":\"1\"," +
                "\"message\":\"Files Not Found\"" +
                "}";

            var orgSiteAttachments = await Task.Run(() => _mySQLContext.OrgSiteAttachment.Where(a => ids.Contains(a.Id)).ToList());
            if (orgSiteAttachments?.Count > 0)
            {
                try
                {
                    _mySQLContext.OrgSiteAttachment.RemoveRange(orgSiteAttachments);
                    _mySQLContext.SaveChanges();
                    res =
                        "{" +
                        "\"code\":\"1\"," +
                        "\"message\":\"Delete Files Success\"" +
                        "}";
                }
                catch (Exception ex)
                {

                    res =
                        "{" +
                        "\"code\":\"0\"," +
                        "\"message\":\"Delete Files Failed\"" +
                        "}";
                    return Ok(res);
                }


            }

            return Ok(res);
        }

    }
}
