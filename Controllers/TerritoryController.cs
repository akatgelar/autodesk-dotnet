using System;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using MySql.Data.MySqlClient;
using System.Text;
using System.Data;
using autodesk.Code;
using autodesk.Code.AuditLogServices;
using autodesk.Code.Models;
// using autodesk.Models;

namespace autodesk.Controllers
{
    [Route("api/Territory")]
    public class TerritoryController : Controller
    {
        StringBuilder sb = new StringBuilder();
        private MySqlDb oDb;//= new MySqlDb("autodesk", true);
        DataSet ds = new DataSet();
        MySqlCommand sqlCommand = new MySqlCommand();
        String result;

        public Audit _audit;//= new Audit();
        private readonly IAuditLogServices _auditLogServices;

        public TerritoryController(MySqlDb sqlDb, IAuditLogServices auditLogServices)
        {
            oDb = sqlDb;
            _audit=new Audit(oDb);
            _auditLogServices = auditLogServices;
        }

        [HttpGet]
        public IActionResult Select()
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Ref_territory");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]); 
            }
 
            return Ok(result);                  
        }

        [HttpPost("SelectAdmin")]
        public IActionResult SelectAdmin([FromBody] dynamic data)
        {
            try
            {
                sb = new StringBuilder();
                sb.Append("select * from Ref_territory");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }


        [HttpGet("{id}")]
        public IActionResult WhereId(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("select Ref_territory.TerritoryId,Territory_Name,countries_id,countries_name from Ref_territory left join Ref_countries on Ref_territory.TerritoryId = Ref_countries.TerritoryId where Ref_territory.TerritoryId = "+id+"");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "{}";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }

        [HttpGet("SiteTerritory/{id}")]
        public IActionResult WhereSiteTerritory(string id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "SELECT TerritoryId FROM Main_site s INNER JOIN Ref_countries rc ON s.SiteCountryCode = rc.countries_code WHERE s.SiteId IN (" + id + ") "
                    );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        } 

        [HttpGet("SKU")]
        public IActionResult SKU(int id)
        { 
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("SELECT rt.TerritoryId, rt.Territory_Name, rc.countries_id, rc.countries_name FROM Ref_territory rt LEFT JOIN Ref_countries rc ON rt.TerritoryId = rc.TerritoryId INNER JOIN SiteCountryDistributor scd ON rc.countries_code = scd.CountryCode INNER JOIN Main_site s ON scd.SiteID = s.SiteId AND s.`Status` <> 'X' GROUP BY rt.TerritoryId");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "Not Found";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }

            return Ok(result);
        }
        [HttpPost("wherenew/{obj}")]
        public IActionResult wherenew(string obj, [FromBody] dynamic data)
        {
           // var o1 = JObject.Parse(obj);
            string query = "";
            JObject json = JObject.FromObject(data);
            query += "select c.geo_code, c.geo_name, t.TerritoryId,c.countries_code,c.countries_name,Territory_Name from Ref_countries c left join Ref_territory t on c.TerritoryId = t.TerritoryId";
            int i = 0;
            //JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            {
                //if (i == 0)
                //{
                //    query += " where ";
                //}
                //if (i > 0)
                //{
                //    query += " and ";
                //}
                if (property.Name.Equals("CountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.countries_code like '%" + property.Value.ToString() + "%' ";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("CountryCode"))
                //{
                //    query += "c.countries_code like '%" + property.Value.ToString() + "%' ";
                //}
                if (property.Name.Equals("ArrCountryCode"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.countries_code in (" + property.Value.ToString() + ") GROUP BY t.TerritoryId";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("ArrCountryCode"))
                //{
                //    query += "c.countries_code in (" + property.Value.ToString() + ") GROUP BY t.TerritoryId";
                //}
                if (property.Name.Equals("OrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.countries_code in (Select SiteCountryCode from Main_site where OrgId in (" + property.Value.ToString() + ")) GROUP BY t.TerritoryId";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("OrgId"))
                //{
                //    query += "c.countries_code in (Select SiteCountryCode from Main_site where OrgId in (" + property.Value.ToString() + ")) GROUP BY t.TerritoryId";
                //}
                if (property.Name.Equals("orgidgeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.countries_code in (select sitecountrycode from main_site where orgid in (" + property.Value.ToString() + ")) group by c.geo_code";
                        i = i + 1;
                    }
                }
                //if (obj == "OrgIdGeo")
                //if (property.Name.Equals("orgidgeo"))
                //{
                //    query += "c.countries_code in (select sitecountrycode from main_site where orgid in (" + property.Value.ToString() + ")) group by c.geo_code";
                //}

                /* new request nambah user role -__ */
                if (property.Name.Equals("SiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.countries_code in (Select SiteCountryCode from Main_site where SiteId in (" + property.Value.ToString() + ")) GROUP BY t.TerritoryId";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("SiteId"))
                //{
                //    query += "c.countries_code in (Select SiteCountryCode from Main_site where SiteId in (" + property.Value.ToString() + ")) GROUP BY t.TerritoryId";
                //}
                ////if (obj == "SiteIdGeo")
                 if (property.Name.Equals("SiteIdGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.countries_code in (Select SiteCountryCode from Main_site where SiteId in (" + property.Value.ToString() + ")) GROUP BY c.geo_code";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("SiteIdGeo"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        query += "c.countries_code in (Select SiteCountryCode from Main_site where SiteId in (" + property.Value.ToString() + ")) GROUP BY c.geo_code";
                //    }

                //}

                /* new request nambah user role -__ */
                if (property.Name.Equals("CountryOrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.geo_code in (Select rc.geo_code from Ref_countries rc LEFT JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + property.Value.ToString() + ")) GROUP BY c.countries_code";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("CountryOrgId"))
                //{
                //    query += "c.geo_code in (Select rc.geo_code from Ref_countries rc LEFT JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in (" + property.Value.ToString() + ")) GROUP BY c.countries_code";
                //}
                if (property.Name.Equals("OnlyCountryOrgId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.countries_code in (Select distinct rc.countries_code from Main_organization o  INNER JOIN Main_site s on o.OrgId = s.OrgId INNER JOIN  Ref_countries rc  ON rc.countries_code = s.SiteCountryCode  where o.OrgId in (" + property.Value.ToString() + ")) GROUP BY c.countries_code";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("OnlyCountryOrgId"))
                //{
                //    query += "c.countries_code in (Select distinct rc.countries_code from Main_organization o  INNER JOIN Main_site s on o.OrgId = s.OrgId INNER JOIN  Ref_countries rc  ON rc.countries_code = s.SiteCountryCode  where o.OrgId in (" + property.Value.ToString() + ")) GROUP BY c.countries_code";
                //}

                /* issue 15102018 - populate country org report */
                if (property.Name.Equals("OnlyCountryOrgIdNew"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.countries_code in (Select RegisteredCountryCode from Main_organization where OrgId in (" + property.Value.ToString() + ")) GROUP BY c.countries_code";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("OnlyCountryOrgIdNew"))
                //{
                //    query += "c.countries_code in (Select RegisteredCountryCode from Main_organization where OrgId in (" + property.Value.ToString() + ")) GROUP BY c.countries_code";
                //}

                /* issue 15102018 - populate country org report */

                /* fixed issue 24092018 - populate country by role site admin */
                if (property.Name.Equals("OnlyCountrySiteId"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query += "c.countries_code in (Select rc.countries_code from Ref_countries rc LEFT JOIN Main_site s ON rc.countries_code = s.SiteCountryCode where s.SiteId in (" + property.Value.ToString() + ")) GROUP BY c.countries_code";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("OnlyCountrySiteId"))
                //{
                //    query += "c.countries_code in (Select rc.countries_code from Ref_countries rc LEFT JOIN Main_site s ON rc.countries_code = s.SiteCountryCode where s.SiteId in (" + property.Value.ToString() + ")) GROUP BY c.countries_code";
                //}
                /* end line fixed issue 24092018 - populate country by role site admin */
                if (property.Name.Equals("Distributor"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query +=
                        "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " ) GROUP BY o.OrgId) > 0 " +
                        "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) " +
                        "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( " + property.Value.ToString() + " )) END " +
                        "GROUP BY t.TerritoryId";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("Distributor"))
                //{
                //    query +=
                //        "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " ) GROUP BY o.OrgId) > 0 " +
                //        "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) " +
                //        "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( " + property.Value.ToString() + " )) END " +
                //        "GROUP BY t.TerritoryId";
                //}
                if (property.Name.Equals("DistributorGeo"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query +=
                                                 "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " ) GROUP BY o.OrgId) > 0 " +
                                                 "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) " +
                                                 "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( " + property.Value.ToString() + " )) END " +
                                                 "GROUP BY c.geo_code";
                        i = i + 1;
                    }
                }
                //if(obj == "DistributorGeo")
                //if (property.Name.Equals("DistributorGeo"))
                //{
                //    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                //    {
                //        query +=
                //                                "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " ) GROUP BY o.OrgId) > 0 " +
                //                                "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) " +
                //                                "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( " + property.Value.ToString() + " )) END " +
                //                                "GROUP BY c.geo_code";
                //    }

                //}
                if (property.Name.Equals("DistributorCountry"))
                {
                    if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                    {
                        if (i == 0) { query += " where "; }
                        if (i > 0) { query += " and "; }
                        query +=
                        "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " ) GROUP BY o.OrgId) > 0 " +
                        "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) " +
                        "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( " + property.Value.ToString() + " )) END " +
                        "GROUP BY c.countries_code";
                        i = i + 1;
                    }
                }
                //if (property.Name.Equals("DistributorCountry"))
                //{
                //    query +=
                //        "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " ) GROUP BY o.OrgId) > 0 " +
                //        "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + property.Value.ToString() + " )) " +
                //        "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( " + property.Value.ToString() + " )) END " +
                //        "GROUP BY c.countries_code";
                //}

                //i = i + 1;
            }
            //if (obj == "'SiteIdGeo'")
            ////if (property.Name.Equals("SiteIdGeo"))
            //{
            //    query += "c.countries_code in (Select SiteCountryCode from Main_site where SiteId in (" + data.SiteIdGeo.ToString() + ")) GROUP BY c.geo_code";
            //}
            //// Console.WriteLine(query);
            //if (obj == "'DistributorGeo'")
            ////if (property.Name.Equals("DistributorGeo"))
            //{
            //    query +=
            //        "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + data.DistributorGeo.ToString() + " ) GROUP BY o.OrgId) > 0 " +
            //        "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( " + data.DistributorGeo.ToString() + " )) " +
            //        "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( " + data.DistributorGeo.ToString() + " )) END " +
            //        "GROUP BY c.geo_code";
            //}

            //if (obj == "'OrgIdGeo'")
            ////if (property.Name.Equals("OrgIdGeo"))
            //{
            //    query += "c.countries_code in (Select SiteCountryCode from Main_site where OrgId in (" + data.OrgIdGeo.ToString() + ")) GROUP BY c.geo_code";
            //}
            try
            {
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            {
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);
            }
            return Ok(result);
        }
        [HttpPost("where/{obj}")]
        public IActionResult WhereObj(string obj)
        {     
            var o1 = JObject.Parse(obj);
            string query =""; 

            query += "select c.geo_code, c.geo_name, t.TerritoryId,c.countries_code,c.countries_name,Territory_Name from Ref_countries c left join Ref_territory t on c.TerritoryId = t.TerritoryId";
            int i = 0;
            JObject json = JObject.FromObject(o1);
            foreach (JProperty property in json.Properties())
            { 
                if(i==0){
                    query += " where ";
                }
                if(i>0){
                    query += " and ";
                }

                if(property.Name.Equals("CountryCode")){   
                    query += "c.countries_code like '%"+property.Value.ToString()+"%' "; 
                }

                if(property.Name.Equals("ArrCountryCode")){   
                    query += "c.countries_code in ("+property.Value.ToString()+") GROUP BY t.TerritoryId"; 
                }

                if(property.Name.Equals("OrgId")){   
                    query += "c.countries_code in (Select SiteCountryCode from Main_site where OrgId in ("+property.Value.ToString()+")) GROUP BY t.TerritoryId"; 
                }

                if(property.Name.Equals("OrgIdGeo")){   
                    query += "c.countries_code in (Select SiteCountryCode from Main_site where OrgId in ("+property.Value.ToString()+")) GROUP BY c.geo_code"; 
                }

                /* new request nambah user role -__ */

                if(property.Name.Equals("SiteId")){   
                    query += "c.countries_code in (Select SiteCountryCode from Main_site where SiteId in ("+property.Value.ToString()+")) GROUP BY t.TerritoryId"; 
                }

                if(property.Name.Equals("SiteIdGeo")){   
                    query += "c.countries_code in (Select SiteCountryCode from Main_site where SiteId in ("+property.Value.ToString()+")) GROUP BY c.geo_code"; 
                }

                /* new request nambah user role -__ */

                if(property.Name.Equals("CountryOrgId")){   
                    query += "c.geo_code in (Select rc.geo_code from Ref_countries rc LEFT JOIN Main_organization o ON rc.countries_code = o.RegisteredCountryCode where o.OrgId in ("+property.Value.ToString()+")) GROUP BY c.countries_code"; 
                }

                if(property.Name.Equals("OnlyCountryOrgId")){   
                    query += "c.countries_code in (Select distinct rc.countries_code from Main_organization o  INNER JOIN Main_site s on o.OrgId = s.OrgId INNER JOIN  Ref_countries rc  ON rc.countries_code = s.SiteCountryCode  where o.OrgId in (" + property.Value.ToString()+")) GROUP BY c.countries_code"; 
                }
                
                /* issue 15102018 - populate country org report */

                if(property.Name.Equals("OnlyCountryOrgIdNew")){   
                    query += "c.countries_code in (Select RegisteredCountryCode from Main_organization where OrgId in (" + property.Value.ToString()+")) GROUP BY c.countries_code"; 
                }

                /* issue 15102018 - populate country org report */

                /* fixed issue 24092018 - populate country by role site admin */
                if(property.Name.Equals("OnlyCountrySiteId")){   
                    query += "c.countries_code in (Select rc.countries_code from Ref_countries rc LEFT JOIN Main_site s ON rc.countries_code = s.SiteCountryCode where s.SiteId in ("+property.Value.ToString()+")) GROUP BY c.countries_code"; 
                }
                /* end line fixed issue 24092018 - populate country by role site admin */

                if(property.Name.Equals("Distributor")){   
                    query += 
                        "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" ) GROUP BY o.OrgId) > 0 "+
                        "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" )) "+ 
                        "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( "+property.Value.ToString()+" )) END "+
                        "GROUP BY t.TerritoryId"; 
                }

                if(property.Name.Equals("DistributorGeo")){   
                    query += 
                        "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" ) GROUP BY o.OrgId) > 0 "+
                        "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" )) "+ 
                        "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( "+property.Value.ToString()+" )) END "+
                        "GROUP BY c.geo_code"; 
                }

                if(property.Name.Equals("DistributorCountry")){   
                    query += 
                        "CASE WHEN (SELECT count(*) AS count FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" ) GROUP BY o.OrgId) > 0 "+
                        "THEN c.countries_code in (SELECT sd.CountryCode FROM Main_organization o INNER JOIN Main_site s ON o.OrgID = s.OrgId INNER JOIN SiteCountryDistributor sd ON sd.SiteId = s.SiteId WHERE o.OrgId IN ( "+property.Value.ToString()+" )) "+ 
                        "ELSE c.countries_code in (SELECT DISTINCT SiteCountryCode FROM Main_site WHERE OrgId IN ( "+property.Value.ToString()+" )) END "+
                        "GROUP BY c.countries_code"; 
                }

                i=i+1;
            } 

            // Console.WriteLine(query);
            
            try
            {  
                sb.AppendFormat(query);
                sqlCommand = new MySqlCommand(sb.ToString());
                // Console.WriteLine(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                result = "[]";
            }
            finally
            { 
                result = MySqlDb.GetJSONArrayString(ds.Tables[0]);    
            }
            return Ok(result);  
        }

        [HttpPost]
        public IActionResult Insert([FromBody] dynamic data)
        {    
            
            int count1 = 0;
            int count2 = 0;
            int lastId = 0;
            JObject o1;
            JObject o2;
            JObject o3;
            // JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Insert Data Failed\""+
                "}";  
             
            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_territory;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
         
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o1 = JObject.Parse(result); 
            count1 = Int32.Parse(o1["count"].ToString());
            Console.WriteLine(count1); 
            
            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "insert into Ref_territory "+
                    "(Territory_Name,cuid, cdate) "+
                    "values ('"+data.Territory_Name+"','"+data.cuid+"',NOW()); "+
                    "select LAST_INSERT_ID() as last_insert_id;"
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result); 
                lastId = Int32.Parse(o3["last_insert_id"].ToString());

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Ref_countries set TerritoryId = "+lastId+" where countries_id in ("+data.countries_id+");"
                );
                Console.WriteLine(sb.ToString());
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

            }
            catch
            { 
            }
            finally
            { 
            }

            sb = new StringBuilder();
            sb.AppendFormat("select count(*) as count from Ref_territory;");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);   

            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            o2 = JObject.Parse(result); 
            count2 = Int32.Parse(o2["count"].ToString());
            Console.WriteLine(count2);

            sb.AppendFormat("select * from Ref_territory where TerritoryId = " + lastId + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            if (count2 > count1)
            {
                //string description = "Insert data Territory" + "<br/>" +
                //    "Territory Name = "+ data.Territory_Name + "<br/>" +
                //    "Country Code = "+ data.countries_id;

                //_audit.ActionPost(data.UserId.ToString(), description, data.cuid.ToString());
                JObject OldData = new JObject();
                if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_territory, data.UserId.ToString(), data.cuid.ToString(), "where TerritoryId =" + lastId + ""))
                {
                    res =
                     "{" +
                     "\"code\":\"1\"," +
                     "\"message\":\"Insert Data Success\"" +
                     "}";
                }

            } 
 
            return Ok(res);  
            
        }


        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody] dynamic data)
        {  
            //JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Update Data Failed\""+
                "}";
            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_territory where TerritoryId = " + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Ref_territory set "+
                    "Territory_Name='"+data.Territory_Name+"',"+
                    "muid='"+data.muid+"',"+
                    "mdate=NOW()"+
                    "where TerritoryId="+id+""
                );
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
               

                sb = new StringBuilder();
                sb.AppendFormat(
                    "update Ref_countries set TerritoryId = 0 where TerritoryId = "+id+";update Ref_countries set TerritoryId = "+id+" where countries_id in ("+data.countries_id+")"
                );
                
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            {
                
            }
            finally
            {  
            }
 
            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_territory where TerritoryId = "+id+";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
        
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject NewData = JsonConvert.DeserializeObject<JObject>(result.ToString());
            //o3 = JObject.Parse(result);  

            // if(o3["OrgId"].ToString() == data.OrgId.ToString())
            // { 
            //string description = "Update data Territory" + "<br/>" +
            //        "Territory Name = "+ data.Territory_Name + "<br/>" +
            //        "Country Code = "+ data.countries_id;

            //    _audit.ActionPost(data.UserId.ToString(), description, data.muid.ToString());
            //if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_territory, data. UserId.ToString(), data.cuid.ToString(), "where TerritoryId =" + id + ""))
            //{
            //    res =
            //     "{" +
            //     "\"code\":\"1\"," +
            //     "\"message\":\"Update Data Success\"" +
            //     "}";
            //}
            _auditLogServices.LogHistory(new History {
                Admin = HttpContext.User.GetUserId(),
                Date=DateTime.Now,
                Description= "OldData:"+JsonConvert.SerializeObject(OldData)+ "<br/>"+"NewData:"+JsonConvert.SerializeObject(NewData),
                Id= id.ToString()
            });
            res =
                 "{" +
                 "\"code\":\"1\"," +
                 "\"message\":\"Update Data Success\"" +
                 "}";
            

            return Ok(res);  
        } 



        [HttpDelete("{id}/{cuid}/{UserId}")]
        public IActionResult Delete(int id, string cuid, string UserId)
        { 
            JObject o3;
            string res =
                "{"+
                "\"code\":\"0\","+
                "\"message\":\"Delete Data Failed\""+
                "}";

            sb = new StringBuilder();
            sb.AppendFormat("select * from Ref_territory where TerritoryId  =" + id + ";");
            sqlCommand = new MySqlCommand(sb.ToString());
            ds = oDb.getDataSetFromSP(sqlCommand);
            result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
            JObject OldData = JsonConvert.DeserializeObject<JObject>(result.ToString());

            try
            {  
                sb = new StringBuilder();
                sb.AppendFormat("delete from Ref_territory where TerritoryId = "+id+"");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            }
            catch
            { 
            }
            finally
            { 
                sb = new StringBuilder();
                sb.AppendFormat("select * from Ref_territory where TerritoryId  ="+id+";");
                sqlCommand = new MySqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
            
                result = MySqlDb.GetJSONObjectString(ds.Tables[0]);
                o3 = JObject.Parse(result);  
                if(o3.Count < 1){

                    //string description = "Delete data Territory" + "<br/>" +
                    //"Territory ID = "+ id;

                    //_audit.ActionPost(UserId.ToString(), description, cuid.ToString());
                    JObject NewData = new JObject();
                    if (_audit.AuditLog(OldData, NewData, AutodeskConst.AuditInfoEnum.Ref_territory, UserId.ToString(), cuid.ToString(), "where TerritoryId =" + id +  ""))
                    {
                        res =
                         "{" +
                         "\"code\":\"1\"," +
                         "\"message\":\"Delete Data Success\"" +
                         "}";
                    } 
                }
            }

            return Ok(res);
        } 


    }
}
