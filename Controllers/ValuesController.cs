﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using autodesk.Code;
using Microsoft.AspNetCore.Mvc;

namespace autodesk.Controllers
{
    [Route("")]
    public class ValuesController : MyController
    {
        // GET api/values
        [HttpGet]
        public string Get()
        {  
            int jwt = this.getHeader();
            string res = "";

            if(jwt == 0){res = this.noToken();}
            else if(jwt == 2){res = this.wrongToken();}
            else if(jwt == 1)
            {
                string var = "";
                var += "welcome to autodesk api \n\n";
                var += "list of api :\n";
                var += " - ActivityCategory \n";
                var += " - Activity \n";
                var += " - ActivityType \n";
                var += " - Countries \n";
                var += " - Geo \n";
                var += " - Glossary \n";
                var += " - MainInvoices \n";
                var += " - JournalActivities \n";
                var += " - LineItemSummary \n";
                var += " - MainContact \n";
                var += " - MainCourses \n";
                var += " - MainOrganization \n";
                var += " - MainSite \n";
                var += " - OrganizationJournalEntries \n";
                var += " - ProductCategory \n";
                var += " - Product \n";
                var += " - Region \n";
                var += " - RoleParams \n";
                var += " - Roles \n";
                var += " - SiteService \n";
                var += " - SubCountries \n";
                var += " - SubRegion \n";
                var += " - Territory \n";
                var += " - User \n";
                var += " - VariableCategory \n";
                var += " - Variable \n";
                
                res = var;

            }

            return res;
        }

        // // GET api/values/5
        // [HttpGet("{id}")]
        // public string Get(int id)
        // {
        //     return "value";
        // }

        // // POST api/values
        // [HttpPost]
        // public void Post([FromBody]string value)
        // {
        // }

        // // PUT api/values/5
        // [HttpPut("{id}")]
        // public void Put(int id, [FromBody]string value)
        // {
        // }

        // // DELETE api/values/5
        // [HttpDelete("{id}")]
        // public void Delete(int id)
        // {
        // }
        public ValuesController(MySqlDb sqlDb) : base(sqlDb)
        {
        }
    }
}
