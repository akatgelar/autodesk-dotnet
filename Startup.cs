﻿// using autodesk.Models;

using System;
using autodesk.Code;
using autodesk.Code.AuditLogServices;
using autodesk.Code.CommonServices;
//using autodesk.Code.CustomMiddleware;
using autodesk.Code.DataServices;
using autodesk.Code.EmailTrackingServices;
using autodesk.Code.MergeServices;
using autodesk.Code.Models;
using autodesk.Code.SqlAdminServices;
using autodesk.CustomMiddleware;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using autodesk.Code.BackUpDbContext;
using Microsoft.AspNetCore.Http;

namespace autodesk
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // services.AddDbContext<HRContext>(options => options.UseSqlServer(Configuration.GetConnectionString("SQLServerConnection")));
            // services.AddDbContext<MySQLContext>(options =>options.UseMySql(Configuration.GetConnectionString("MySQLConnection")));  
            // services.Configure<EmailConfig>(Configuration.GetSection("Email"));
            // services.AddTransient<MySqlDb>(_ => new MySqlDb(Configuration["ConnectionStrings:MySQLConnection"]));
            // services.AddTransient<IEmailService, EmailService>();
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]));
            var singingCredentials = new SigningCredentials(key, SecurityAlgorithms.RsaSha256); 

            services.AddTransient<MySqlDb>(_ => new MySqlDb(Configuration["ConnectionStrings:MySQLConnection"], true));
            services.AddDbContext<MySQLContext>(options => options.UseMySQL(Configuration.GetConnectionString("MySQLConnection")));
            services.AddDbContext<backupdbContext>(options => options.UseMySQL(Configuration.GetConnectionString("MySQLBackUpConnection")));
            services.AddTransient<DataServices>();
            services.AddResponseCaching();
            services.AddAuthentication(
     options =>
                           {
                            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                           })
                         .AddJwtBearer(options =>
                          {
                           options.TokenValidationParameters = new TokenValidationParameters
                                                                {
                                                                 ClockSkew = TimeSpan.FromMinutes(5),
                                                                 RequireSignedTokens = true,
                                                                 RequireExpirationTime = true,
                                                                 ValidateIssuer = true,
                                                                 ValidateAudience = true,
                                                                 ValidateLifetime = true,
                                                                 ValidateIssuerSigningKey = true,
                                                                 ValidIssuer = Configuration["Jwt:Issuer"],
                                                                 ValidAudience = Configuration["Auth0:Audience"],
                                                                 IssuerSigningKey = singingCredentials.Key
                                                                };
                           
                          });
            services.AddCors(options =>
            {
             options.AddPolicy("CorsPolicy",
              builder => builder.AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader()
               .AllowCredentials()
               .Build());
            });

            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<IAgreementService, AgreementService>();
            services.AddTransient<IEmailService, EmailService>();
            services.AddTransient<IAuditLogServices, AuditLogServices>();
            services.AddTransient<ISqlAdminServices, SqlAdminServices>();
            services.AddTransient<ICommonServices, CommonServices>();
            services.AddTransient<IMergeServices, MergeServices>();
            services.AddTransient<IQueueServices,QueueServices>();
            services.AddTransient<IEmailTrackingServices, EmailTrackingServices>();
            services.AddMvc(
             options =>
             {
             }).AddXmlSerializerFormatters();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseCors(builder =>
                //    builder.WithOrigins("https://localhost").AllowAnyMethod().AllowAnyHeader()

                //);
            }

            //app.UseCors(builder =>
            //    builder.WithOrigins("https://education.autodesk.com")
            //        .AllowAnyHeader()
            //);
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseResponseCaching();
            app.Use(async (context, next) =>
            {
                // For GetTypedHeaders, add: using Microsoft.AspNetCore.Http;
                context.Response.GetTypedHeaders().CacheControl =
                    new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
                    {
                       NoStore=true,
                       NoCache=true
                    };
                context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] =
                    new string[] { "Accept-Encoding" };

                await next();
            });

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
            // app.UseWhen(c => c.Request.Headers.ContainsKey("Authorization"),
            //   builder =>
            //   {
            //            builder.UseMiddleware<HttpHandlerMiddleware>();

            //      });
            app.UseWhen(context => context.Request.Path.StartsWithSegments("/api"),
            appBuilder =>
            {
                appBuilder.UseMiddleware<HttpHandlerMiddleware>();

            });
            // var DB = app.ApplicationServices.GetRequiredService<MySQLContext>();
            // DB.Database.EnsureCreated();

            app.UseMvc();
        }
    }
}
