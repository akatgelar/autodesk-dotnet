/*
Navicat MySQL Data Transfer

Source Server         : autodesk
Source Server Version : 50559
Source Host           : 35.197.146.155:3306
Source Database       : autodesk

Target Server Type    : MYSQL
Target Server Version : 50559
File Encoding         : 65001

Date: 2018-01-31 01:03:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for __efmigrationshistory
-- ----------------------------
DROP TABLE IF EXISTS `__efmigrationshistory`;
CREATE TABLE `__efmigrationshistory` (
  `MigrationId` varchar(95) NOT NULL,
  `ProductVersion` varchar(32) NOT NULL,
  PRIMARY KEY (`MigrationId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of __efmigrationshistory
-- ----------------------------
INSERT INTO `__efmigrationshistory` VALUES ('20180112031652_InitialCreate', '2.0.1-rtm-125');

-- ----------------------------
-- Table structure for Activity
-- ----------------------------
DROP TABLE IF EXISTS `Activity`;
CREATE TABLE `Activity` (
  `activity_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_category` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Invoice',
  `activity_name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `unique` char(1) COLLATE utf8_unicode_ci DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`activity_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_category`)
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Activity
-- ----------------------------
INSERT INTO `Activity` VALUES ('123', 'Contact Attribute', 'zxczxczxc3', 'zxczxcz', 'Y', 'admin', '2018-01-30 15:09:31', 'admin', '2018-01-30 15:09:31');
INSERT INTO `Activity` VALUES ('115', 'Contact Attribute', 'bbbb', 'asdasd', 'Y', null, '2018-01-13 00:00:00', null, '0001-01-01 00:00:00');
INSERT INTO `Activity` VALUES ('2', 'Contact Journal Entry', 'asdasdasd', 'asdasdasd', 'Y', 'ATCDB000', '2006-10-11 17:51:18', 'ATCDB000', '2018-01-13 00:00:00');
INSERT INTO `Activity` VALUES ('116', 'Contact Migration Anomalies', 'zzzz', 'xxxxxx', 'Y', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:56:55');
INSERT INTO `Activity` VALUES ('3', 'Contact Security Role', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - EC2', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', 'Y', 'ATCDB000', '2006-10-11 18:07:58', 'morganj', '2008-03-05 04:49:54');
INSERT INTO `Activity` VALUES ('4', 'Organization Attribute', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC2', 'Fees for an increase in ATC Software License Entitlements 2007-2008', 'Y', 'ATCDB000', '2006-10-11 18:08:54', 'morganj', '2008-03-05 04:48:58');
INSERT INTO `Activity` VALUES ('5', 'Organization Journal Entry', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - UK', 'Fees for an increase in ATC Software License Entitlements 2007-2008', 'Y', 'ATCDB000', '2006-10-11 18:09:10', 'morganj', '2008-03-05 04:49:29');
INSERT INTO `Activity` VALUES ('6', 'Site Accreditation', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - UK', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', 'Y', 'ATCDB000', '2006-10-11 18:09:25', 'morganj', '2008-03-05 04:50:17');
INSERT INTO `Activity` VALUES ('9', 'Site Attribute', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'Fees for an increase in ATC Software License Entitlements 2007-2008', 'Y', 'ATCDB000', '2006-10-13 07:56:53', 'morganj', '2008-03-05 04:48:45');
INSERT INTO `Activity` VALUES ('12', 'Site Event', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - UK', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', 'Y', '41118', '2007-05-22 08:00:14', 'morganj', '2008-03-05 04:48:32');
INSERT INTO `Activity` VALUES ('15', 'Site Journal Entry', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - ME', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', 'Y', '41118', '2007-05-22 08:06:06', 'morganj', '2008-03-05 04:48:19');
INSERT INTO `Activity` VALUES ('117', 'Site Migration Anomalies', 'new', 'new', 'Y', null, '2018-01-13 14:59:57', null, '2018-01-13 14:59:57');
INSERT INTO `Activity` VALUES ('18', 'VAR Contact Attribute', 'AMER: 2008 (FY09) Renewal', 'ATC Program Renewal Fee in the Program year 2008.', 'Y', '41118', '2007-09-04 12:15:54', '41118', '2007-09-17 11:04:31');
INSERT INTO `Activity` VALUES ('19', 'VAR Contact Journal Entry', 'Apac: 2007 (FY08) Renewal ANZ/Australia', 'Program membership Fee for 2007', 'Y', '41118', '2007-09-05 04:44:35', '41118', '2008-01-09 04:14:39');
INSERT INTO `Activity` VALUES ('20', 'VAR Organization Attribute', 'AMER: 2008 (FY09) ATC Program Fee', 'ATC Program Fee for new ATC Applications in the Program year 2008.', 'Y', '41118', '2007-09-17 11:02:23', 't_barrw', '2007-09-17 11:55:39');
INSERT INTO `Activity` VALUES ('21', 'VAR Site Attribute', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - EC1', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', 'Y', 'morganj', '2007-09-25 10:49:57', 'morganj', '2008-03-05 04:50:29');
INSERT INTO `Activity` VALUES ('22', 'Contact Certification', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - EC2', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', 'Y', 'morganj', '2007-09-25 10:51:11', 'morganj', '2008-03-05 04:50:42');
INSERT INTO `Activity` VALUES ('23', 'Contact Attribute', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - ME', 'Fees for an increase in ATC Software License Entitlements 2008-2009', 'N', 'morganj', '2007-09-25 10:52:10', 'morganj', '2008-03-05 04:51:57');
INSERT INTO `Activity` VALUES ('24', 'Contact Journal Entry', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - UK', 'Fees for an increase in ATC Software License Entitlements 2008-2009', 'N', 'morganj', '2007-09-25 10:52:59', 'morganj', '2008-03-05 04:52:08');
INSERT INTO `Activity` VALUES ('25', 'Contact Migration Anomalies', 'EMEA:  2008 (FY09) INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'Fees for an increase in ATC Software License Entitlements 2008-2009', 'N', 'morganj', '2007-09-25 10:53:56', 'morganj', '2007-09-26 08:08:51');
INSERT INTO `Activity` VALUES ('26', 'Contact Security Role', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'Fees for an increase in ATC Software License Entitlements 2008-2009', 'N', 'morganj', '2007-09-25 10:55:45', 'morganj', '2008-03-05 04:51:27');
INSERT INTO `Activity` VALUES ('27', 'Organization Attribute', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC2', 'Fees for an increase in ATC Software License Entitlements 2008-2009', 'N', 'morganj', '2007-09-25 10:56:37', 'morganj', '2008-03-05 04:51:45');
INSERT INTO `Activity` VALUES ('28', 'Organization Journal Entry', 'APAC: FY09  NEW APPLICATION (INDIA)', null, 'N', '41118', '2007-09-26 06:24:15', '41118', '2008-01-09 04:15:26');
INSERT INTO `Activity` VALUES ('29', 'Site Accreditation', 'EMIA 2008 (FY09) CREDIT NOTE ANNUAL RENEWAL', 'Credit Notes or refunds', 'N', 'morganj', '2007-12-18 11:47:43', 'morganj', '2008-03-05 04:52:19');
INSERT INTO `Activity` VALUES ('30', 'Site Attribute', 'Apac FY09 ATC PROG New App Annual Fee', 'ATC Program Fee for new ATC Applications', 'N', '41118', '2008-01-09 04:10:46', 'jiangp', '2008-01-15 06:43:10');
INSERT INTO `Activity` VALUES ('31', 'Site Event', 'AAPAC ATC Program', 'ATC Annual Program Fee', 'N', 'jiangp', '2008-01-15 06:45:46', 'chongl', '2012-03-21 01:21:36');
INSERT INTO `Activity` VALUES ('32', 'Site Journal Entry', 'AAPAC ATC Certification', 'ATC Certification Fee', 'N', 'jiangp', '2008-01-15 06:46:43', 'chongl', '2012-03-14 21:29:39');
INSERT INTO `Activity` VALUES ('33', 'Site Migration Anomalies', 'AAPac AOTG DSL', 'AOTG DSL fee', 'N', 'jiangp', '2008-01-15 06:47:43', 'chongl', '2012-03-14 21:29:17');
INSERT INTO `Activity` VALUES ('34', 'VAR Contact Attribute', 'AAPac AOTG Courseware', 'AOTG hard copy Courseware fee', 'N', 'jiangp', '2008-01-15 06:48:32', 'chongl', '2012-03-14 21:28:55');
INSERT INTO `Activity` VALUES ('35', 'VAR Contact Journal Entry', 'EMIA 2008 (FY09) CREDIT NOTE NEW APPLICATION', null, 'N', 'morganj', '2008-01-18 11:32:38', 'morganj', '2008-03-05 04:52:30');
INSERT INTO `Activity` VALUES ('36', 'VAR Organization Attribute', 'EMIA 2008 (FY09) CREDIT NOTE SLEI', null, 'N', 'morganj', '2008-01-18 11:33:14', 'morganj', '2008-03-05 04:52:42');
INSERT INTO `Activity` VALUES ('37', 'VAR Site Attribute', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - SAARC/INDIA', null, 'N', 'morganj', '2008-04-16 10:21:53', 'morganj', '2008-04-16 10:32:22');
INSERT INTO `Activity` VALUES ('38', 'Contact Certification', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - SAARC/INDIA', null, 'N', 'morganj', '2008-04-16 10:22:49', 'morganj', '2008-04-16 10:46:53');
INSERT INTO `Activity` VALUES ('39', 'Contact Attribute', 'EMIA 2008 (FY09) ATC PROGRAM AOTC DSL - SAARC/INDIA', null, 'N', 'morganj', '2008-04-16 10:23:43', 'morganj', '2008-04-16 10:46:35');
INSERT INTO `Activity` VALUES ('40', 'Contact Journal Entry', 'AMER: 2009 (FY10) ATC Program Fee LA (12) -  Model 2', 'LA ATC Fee Model 2 (1 Essentials DSL Title)', 'N', 'herculg', '2008-09-26 18:00:07', 'herculg', '2008-09-26 18:28:52');
INSERT INTO `Activity` VALUES ('41', 'Contact Migration Anomalies', 'AMER: 2009 (FY10) ATC Program Fee', 'ATC Program Fee for new ATC Applications in the Program year 2009.', 'N', 'herculg', '2008-09-29 12:17:40', 'herculg', '2009-01-06 17:15:38');
INSERT INTO `Activity` VALUES ('42', 'Contact Security Role', 'AMER: 2009 (FY10) Renewal', 'ATC Program Renewal Fee in the Program year 2009.', 'N', 'herculg', '2008-09-29 12:18:48', 'herculg', '2008-09-29 12:19:31');
INSERT INTO `Activity` VALUES ('43', 'Organization Attribute', 'AMER: 2009 (FY10) ATC Program Fee LA (12) SAT-  Model 2', 'LA ATC Fee Model 2 (1 Essentials DSL Title)', 'N', 'herculg', '2008-10-02 16:04:32', 'herculg', '2008-10-22 18:17:54');
INSERT INTO `Activity` VALUES ('44', 'Organization Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry', 'N', 'herculg', '2008-10-02 17:09:21', 'herculg', '2008-10-02 17:09:57');
INSERT INTO `Activity` VALUES ('45', 'Site Accreditation', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry / SAT', 'Y', 'herculg', '2008-10-02 17:10:56', 'herculg', '2008-10-02 17:11:42');
INSERT INTO `Activity` VALUES ('46', 'Site Attribute', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry', 'Y', 'herculg', '2008-10-02 17:12:26', 'herculg', '2008-10-02 17:12:43');
INSERT INTO `Activity` VALUES ('47', 'Site Event', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry / SAT', 'Y', 'herculg', '2008-10-02 17:13:22', 'herculg', '2008-10-02 17:13:31');
INSERT INTO `Activity` VALUES ('48', 'Site Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry', 'Y', 'herculg', '2008-10-02 17:15:24', 'herculg', '2008-10-02 17:15:39');
INSERT INTO `Activity` VALUES ('49', 'Site Migration Anomalies', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry / SAT', 'Y', 'herculg', '2008-10-02 17:16:02', 'herculg', '2008-10-02 17:16:23');
INSERT INTO `Activity` VALUES ('50', 'VAR Contact Attribute', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry', 'Y', 'herculg', '2008-10-02 17:17:26', 'herculg', '2008-10-02 17:17:38');
INSERT INTO `Activity` VALUES ('51', 'VAR Contact Journal Entry', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry / SAT', 'Y', 'herculg', '2008-10-02 17:18:03', 'herculg', '2008-10-02 17:18:15');
INSERT INTO `Activity` VALUES ('52', 'Contact Certification', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry', 'Y', 'herculg', '2008-10-02 17:18:55', 'herculg', '2008-10-02 17:19:07');
INSERT INTO `Activity` VALUES ('53', 'Contact Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry / SAT', 'Y', 'herculg', '2008-10-02 17:19:30', 'herculg', '2008-10-02 17:19:42');
INSERT INTO `Activity` VALUES ('54', 'Contact Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry', 'Y', 'herculg', '2008-10-02 17:20:07', 'herculg', '2008-10-02 17:20:21');
INSERT INTO `Activity` VALUES ('55', 'Contact Migration Anomalies', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry / SAT', 'Y', 'herculg', '2008-10-02 17:20:49', 'herculg', '2008-10-02 17:21:04');
INSERT INTO `Activity` VALUES ('56', 'Contact Security Role', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Essentials Title', 'Y', 'herculg', '2008-10-22 17:32:34', 'herculg', '2008-10-22 18:18:00');
INSERT INTO `Activity` VALUES ('57', 'Organization Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Industry / SAT', 'Y', 'herculg', '2008-10-22 17:44:45', 'herculg', '2008-10-22 18:18:16');
INSERT INTO `Activity` VALUES ('58', 'Organization Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Essentials', 'Y', 'herculg', '2008-10-22 17:46:31', 'herculg', '2008-10-22 18:17:44');
INSERT INTO `Activity` VALUES ('59', 'Site Accreditation', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Essentials Title / SAT', 'Y', 'herculg', '2008-10-22 17:56:23', 'herculg', '2008-10-22 18:18:25');
INSERT INTO `Activity` VALUES ('60', 'Site Attribute', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title', 'Y', 'herculg', '2008-10-22 17:58:17', 'herculg', '2008-10-22 18:23:23');
INSERT INTO `Activity` VALUES ('61', 'Site Event', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title / SAT', 'Y', 'herculg', '2008-10-22 18:06:05', 'herculg', '2008-10-22 18:23:49');
INSERT INTO `Activity` VALUES ('62', 'Site Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'LA ATC Fee Model 2  + 2 Essentials Title', 'Y', 'herculg', '2008-10-22 18:07:04', 'herculg', '2008-10-22 18:23:35');
INSERT INTO `Activity` VALUES ('63', 'Site Migration Anomalies', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title', 'Y', 'herculg', '2008-10-22 18:07:50', 'herculg', '2008-10-22 19:01:14');
INSERT INTO `Activity` VALUES ('64', 'VAR Contact Attribute', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title', 'Y', 'herculg', '2008-10-22 18:21:08', 'herculg', '2008-10-22 18:23:09');
INSERT INTO `Activity` VALUES ('65', 'VAR Contact Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title', 'Y', 'herculg', '2008-10-22 18:21:33', 'herculg', '2008-10-22 18:24:33');
INSERT INTO `Activity` VALUES ('66', 'VAR Organization Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title / SAT', 'Y', 'herculg', '2008-10-22 18:26:02', 'herculg', '2008-10-22 18:27:05');
INSERT INTO `Activity` VALUES ('67', 'VAR Site Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title / SAT', 'Y', 'herculg', '2008-10-22 18:26:40', 'herculg', '2008-10-22 18:27:36');
INSERT INTO `Activity` VALUES ('68', 'Contact Certification', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title', 'Y', 'herculg', '2008-10-22 18:55:28', 'herculg', '2008-10-22 18:56:41');
INSERT INTO `Activity` VALUES ('69', 'Contact Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title / SAT', 'Y', 'herculg', '2008-10-22 18:57:21', 'herculg', '2008-10-22 18:57:41');
INSERT INTO `Activity` VALUES ('70', 'Contact Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title', 'Y', 'herculg', '2008-10-22 18:58:16', 'herculg', '2008-10-22 18:58:48');
INSERT INTO `Activity` VALUES ('71', 'Contact Migration Anomalies', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title / SAT', 'Y', 'herculg', '2008-10-22 18:59:15', 'herculg', '2008-10-22 19:07:37');
INSERT INTO `Activity` VALUES ('72', 'Contact Security Role', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title / SAT', 'Y', 'herculg', '2008-10-22 19:08:45', 'herculg', '2008-10-22 19:09:04');
INSERT INTO `Activity` VALUES ('73', 'Organization Attribute', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, 'Y', 'Marcov', '2008-11-05 06:28:20', 'morganj', '2008-11-13 10:54:44');
INSERT INTO `Activity` VALUES ('74', 'Organization Journal Entry', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, 'Y', 'Marcov', '2008-11-05 06:29:19', 'morganj', '2008-11-13 10:50:41');
INSERT INTO `Activity` VALUES ('75', 'Site Accreditation', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, 'Y', 'Marcov', '2008-11-05 06:30:13', 'morganj', '2008-11-13 10:55:32');
INSERT INTO `Activity` VALUES ('76', 'Site Attribute', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, 'Y', 'Marcov', '2008-11-05 06:30:54', 'morganj', '2008-11-13 10:53:56');
INSERT INTO `Activity` VALUES ('77', 'Site Event', 'EMIA: 2009 (FY10) ATC PROGRAM - EC3 CIS - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, 'Y', 'Marcov', '2008-11-05 06:31:29', 'morganj', '2008-11-13 10:48:24');
INSERT INTO `Activity` VALUES ('78', 'Site Journal Entry', 'EMIA: 2009 (FY10) ATC PROGRAM - SAPAC - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, 'Y', 'Marcov', '2008-11-05 06:32:07', 'morganj', '2008-12-15 09:10:34');
INSERT INTO `Activity` VALUES ('79', 'Site Migration Anomalies', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, 'Y', 'Marcov', '2008-11-05 06:32:43', 'morganj', '2008-11-17 05:24:32');
INSERT INTO `Activity` VALUES ('80', 'VAR Contact Attribute', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, 'Y', 'Marcov', '2008-11-05 06:33:13', 'morganj', '2008-11-17 05:23:49');
INSERT INTO `Activity` VALUES ('81', 'VAR Contact Journal Entry', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, 'Y', 'Marcov', '2008-11-05 06:33:39', 'morganj', '2008-11-17 05:24:11');
INSERT INTO `Activity` VALUES ('82', 'VAR Organization Attribute', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, 'Y', 'Marcov', '2008-11-05 06:34:09', 'morganj', '2008-11-17 05:24:52');
INSERT INTO `Activity` VALUES ('120', 'Contact Security Role', 'asdsad', 'asdasd', 'a', 'admin', '0001-01-01 00:00:00', null, '0001-01-01 00:00:00');
INSERT INTO `Activity` VALUES ('121', 'Site Accreditation', 'aaaa', 'asdasda', 'f', 'admin', '0001-01-01 00:00:00', null, '0001-01-01 00:00:00');
INSERT INTO `Activity` VALUES ('122', 'Contact Journal Entry test', 'asdasdasd', 'asdasdasd', 'Y', 'ATCDB000', '2006-10-11 17:51:18', 'ATCDB000', '2018-01-13 00:00:00');
INSERT INTO `Activity` VALUES ('124', 'Contact Attribute', 'zxczxczxc3', 'zxczxcz', 'Y', 'admin', '2018-01-30 15:09:31', 'admin', '2018-01-30 15:09:31');
INSERT INTO `Activity` VALUES ('125', 'Contact Attribute', 'zxczxczxc3', 'zxczxcz', 'Y', 'admin', '2018-01-30 15:09:31', 'admin', '2018-01-30 15:09:31');
INSERT INTO `Activity` VALUES ('126', '', '', '', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for Activity_category
-- ----------------------------
DROP TABLE IF EXISTS `Activity_category`;
CREATE TABLE `Activity_category` (
  `activity_category_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_category` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`activity_category_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_category`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Activity_category
-- ----------------------------
INSERT INTO `Activity_category` VALUES ('2', 'Contact Certification', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('1', 'Contact Attribute', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('3', 'Contact Journal Entry', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('119', 'Contact Migration Anomalies', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('120', 'Contact Security Role', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('121', 'Organization Attribute', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('122', 'Organization Journal Entry', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('123', 'Site Accreditation', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('124', 'Site Attribute', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('125', 'Site Event', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('126', 'Site Journal Entry', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('127', 'Site Migration Anomalies', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('128', 'VAR Contact Attribute', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('129', 'VAR Contact Journal Entry', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('130', 'VAR Organization Attribute', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');
INSERT INTO `Activity_category` VALUES ('131', 'VAR Site Attribute', null, '2006-10-11 17:51:18', null, '2006-10-11 17:51:18');

-- ----------------------------
-- Table structure for Activity_type
-- ----------------------------
DROP TABLE IF EXISTS `Activity_type`;
CREATE TABLE `Activity_type` (
  `activity_type_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`activity_type_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_type`)
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Activity_type
-- ----------------------------
INSERT INTO `Activity_type` VALUES ('2', 'A&P Invoice', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Activity_type` VALUES ('4', 'VAR Invoice', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Activity_type` VALUES ('3', 'ATC Invoice', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');

-- ----------------------------
-- Table structure for Dictionary
-- ----------------------------
DROP TABLE IF EXISTS `Dictionary`;
CREATE TABLE `Dictionary` (
  `Key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Parent` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `KeyValue` text COLLATE utf8_unicode_ci NOT NULL,
  `Status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`Key`,`Parent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Dictionary
-- ----------------------------
INSERT INTO `Dictionary` VALUES ('A', 'SiteStatus', 'Active', 'A');
INSERT INTO `Dictionary` VALUES ('P', 'SiteStatus', 'Pending', 'A');
INSERT INTO `Dictionary` VALUES ('S', 'SiteStatus', 'Suspended', 'A');
INSERT INTO `Dictionary` VALUES ('T', 'SiteStatus', 'Terminated', 'A');
INSERT INTO `Dictionary` VALUES ('R', 'SiteStatus', 'Rejected', 'A');
INSERT INTO `Dictionary` VALUES ('1', 'Permissions', 'Guest', 'A');
INSERT INTO `Dictionary` VALUES ('2', 'Permissions', 'Read Only', 'A');
INSERT INTO `Dictionary` VALUES ('3', 'Permissions', 'Read/Write', 'A');
INSERT INTO `Dictionary` VALUES ('4', 'Permissions', 'Full Access', 'A');
INSERT INTO `Dictionary` VALUES ('5', 'Permissions', 'System Admin', 'A');
INSERT INTO `Dictionary` VALUES ('DSG', 'BusinessFocus', 'DSG', 'A');
INSERT INTO `Dictionary` VALUES ('3DS_MAX', 'BusinessFocus', 'MED', 'A');
INSERT INTO `Dictionary` VALUES ('Apac_PTD', 'BusinessFocus', 'APAC Platform Technology (PTD)', 'A');
INSERT INTO `Dictionary` VALUES ('Apac_BSD', 'BusinessFocus', 'APAC Building Solutions (BSD)', 'A');
INSERT INTO `Dictionary` VALUES ('Apac_MSD', 'BusinessFocus', 'APAC Manufacturing Solutions (MSD)', 'A');
INSERT INTO `Dictionary` VALUES ('Apac_MED', 'BusinessFocus', 'APAC MED', 'A');
INSERT INTO `Dictionary` VALUES ('Apac_ISD', 'BusinessFocus', 'APAC Infrastructure Solutions (ISD)', 'A');
INSERT INTO `Dictionary` VALUES ('Apac_Maya', 'BusinessFocus', 'APAC Maya', 'A');
INSERT INTO `Dictionary` VALUES ('ATC-APAC-PSEB', 'BusinessFocus', 'ATC-APAC-PSEB', 'A');
INSERT INTO `Dictionary` VALUES ('ATC-APAC-AEC', 'BusinessFocus', 'ATC-APAC-AEC', 'A');
INSERT INTO `Dictionary` VALUES ('ATC-APAC-MSD-ENG', 'BusinessFocus', 'ATC-APAC-MSD-ENG', 'A');
INSERT INTO `Dictionary` VALUES ('ATC-APAC-MSD-ID', 'BusinessFocus', 'ATC-APAC-MSD-ID', 'A');
INSERT INTO `Dictionary` VALUES ('ATC-APAC-MED-VC', 'BusinessFocus', 'ATC-APAC-MED-VC', 'A');
INSERT INTO `Dictionary` VALUES ('ATC-APAC-MED-DE', 'BusinessFocus', 'ATC-APAC-MED-DE', 'A');
INSERT INTO `Dictionary` VALUES ('Reseller', 'CompanyTypes', 'Reseller', 'A');
INSERT INTO `Dictionary` VALUES ('Educational', 'CompanyTypes', 'Educational', 'A');
INSERT INTO `Dictionary` VALUES ('TrainingOrg', 'CompanyTypes', 'Training Organization', 'A');
INSERT INTO `Dictionary` VALUES ('Other', 'CompanyTypes', 'Other', 'A');
INSERT INTO `Dictionary` VALUES ('Mr', 'Salutation', 'Mr', 'A');
INSERT INTO `Dictionary` VALUES ('Miss', 'Salutation', 'Miss', 'A');
INSERT INTO `Dictionary` VALUES ('Mrs', 'Salutation', 'Mrs', 'A');
INSERT INTO `Dictionary` VALUES ('Ms', 'Salutation', 'Ms', 'A');
INSERT INTO `Dictionary` VALUES ('Dr', 'Salutation', 'Dr', 'A');
INSERT INTO `Dictionary` VALUES ('Other', 'Salutation', 'Other', 'A');
INSERT INTO `Dictionary` VALUES ('ZH', 'Languages', 'Chinese', 'x');
INSERT INTO `Dictionary` VALUES ('EN', 'Languages', 'English', 'x');
INSERT INTO `Dictionary` VALUES ('FR', 'Languages', 'French', 'x');
INSERT INTO `Dictionary` VALUES ('DE', 'Languages', 'German', 'x');
INSERT INTO `Dictionary` VALUES ('IT', 'Languages', 'Italian', 'x');
INSERT INTO `Dictionary` VALUES ('JA', 'Languages', 'Japanese', 'x');
INSERT INTO `Dictionary` VALUES ('KO', 'Languages', 'Korean', 'x');
INSERT INTO `Dictionary` VALUES ('RU', 'Languages', 'Russian', 'x');
INSERT INTO `Dictionary` VALUES ('ES', 'Languages', 'Spanish', 'x');
INSERT INTO `Dictionary` VALUES ('PO', 'Languages', 'Portuguese', 'x');
INSERT INTO `Dictionary` VALUES ('EN', 'UILanguages', 'English', 'A');
INSERT INTO `Dictionary` VALUES ('DE', 'UILanguages', 'German', 'A');
INSERT INTO `Dictionary` VALUES ('FR', 'UILanguages', 'French', 'A');
INSERT INTO `Dictionary` VALUES ('IT', 'UILanguages', 'Italian', 'A');
INSERT INTO `Dictionary` VALUES ('ES', 'UILanguages', 'Spanish', 'A');
INSERT INTO `Dictionary` VALUES ('JA', 'UILanguages', 'Japanese', 'A');
INSERT INTO `Dictionary` VALUES ('KO', 'UILanguages', 'Korean', 'A');
INSERT INTO `Dictionary` VALUES ('PT-BR', 'UILanguages', 'Portuguese-Brazilian', 'A');
INSERT INTO `Dictionary` VALUES ('ZH-HANS', 'UILanguages', 'Simplified Chinese', 'A');
INSERT INTO `Dictionary` VALUES ('RU', 'UILanguages', 'Russian', 'A');
INSERT INTO `Dictionary` VALUES ('KEYS', 'UILanguages', 'Show Variables', 'A');
INSERT INTO `Dictionary` VALUES ('AUD', 'Currencies', 'Australian Dollar', 'A');
INSERT INTO `Dictionary` VALUES ('EUR', 'Currencies', 'Euro', 'A');
INSERT INTO `Dictionary` VALUES ('GBP', 'Currencies', 'British Pound (UK)', 'A');
INSERT INTO `Dictionary` VALUES ('JPY', 'Currencies', 'Japanese Yen', 'A');
INSERT INTO `Dictionary` VALUES ('NZD', 'Currencies', 'New Zealand Dollar', 'A');
INSERT INTO `Dictionary` VALUES ('RMB', 'Currencies', 'Chinese Yuan', 'A');
INSERT INTO `Dictionary` VALUES ('USD', 'Currencies', 'US Dollar', 'A');
INSERT INTO `Dictionary` VALUES ('WON', 'Currencies', 'Korean Won', 'A');
INSERT INTO `Dictionary` VALUES ('Trained', 'QualificationTypes', 'Trained', 'A');
INSERT INTO `Dictionary` VALUES ('Tested', 'QualificationTypes', 'Tested', 'A');
INSERT INTO `Dictionary` VALUES ('Evidence', 'QualificationTypes', 'Provided Evidence of Experience', 'A');
INSERT INTO `Dictionary` VALUES ('Other', 'QualificationTypes', 'Other', 'A');
INSERT INTO `Dictionary` VALUES ('AppDoc', 'SiteAttachmentTypes', 'Application Document', 'A');
INSERT INTO `Dictionary` VALUES ('Certificate', 'SiteAttachmentTypes', 'Certificate Image', 'A');
INSERT INTO `Dictionary` VALUES ('CV', 'SiteAttachmentTypes', 'Curriculum Vitae', 'A');
INSERT INTO `Dictionary` VALUES ('Sample', 'SiteAttachmentTypes', 'Sample Material', 'A');
INSERT INTO `Dictionary` VALUES ('Other', 'SiteAttachmentTypes', 'Other', 'A');
INSERT INTO `Dictionary` VALUES ('A', 'ContractStatuses', 'Active', 'A');
INSERT INTO `Dictionary` VALUES ('I', 'ContractStatuses', 'Inactive', 'A');
INSERT INTO `Dictionary` VALUES ('S', 'ContractStatuses', 'Suspended', 'A');
INSERT INTO `Dictionary` VALUES ('1', 'PaymentMethods', 'Wire Transfer', 'A');
INSERT INTO `Dictionary` VALUES ('2', 'PaymentMethods', 'CC', 'A');
INSERT INTO `Dictionary` VALUES ('3', 'PaymentMethods', 'Check/Cheque', 'A');
INSERT INTO `Dictionary` VALUES ('4', 'PaymentMethods', 'Net 30 (C004)', 'A');
INSERT INTO `Dictionary` VALUES ('5', 'PaymentMethods', 'Prepaid Check (C014)', 'A');
INSERT INTO `Dictionary` VALUES ('6', 'PaymentMethods', 'American Express (C020)', 'A');
INSERT INTO `Dictionary` VALUES ('7', 'PaymentMethods', 'Discover (C021)', 'A');
INSERT INTO `Dictionary` VALUES ('8', 'PaymentMethods', 'MasterCard (C022)', 'A');
INSERT INTO `Dictionary` VALUES ('9', 'PaymentMethods', 'Visa (C023)', 'A');
INSERT INTO `Dictionary` VALUES ('6', 'JournalActivityTypes', 'Organization Attribute', 'A');
INSERT INTO `Dictionary` VALUES ('5', 'JournalActivityTypes', 'Organization Journal Entry', 'A');
INSERT INTO `Dictionary` VALUES ('7', 'JournalActivityTypes', 'Site Accreditation', 'A');
INSERT INTO `Dictionary` VALUES ('3', 'JournalActivityTypes', 'Site Attribute', 'A');
INSERT INTO `Dictionary` VALUES ('1', 'JournalActivityTypes', 'Site Journal Entry', 'A');
INSERT INTO `Dictionary` VALUES ('2', 'JournalActivityTypes', 'Site Event', 'A');
INSERT INTO `Dictionary` VALUES ('9', 'JournalActivityTypes', 'Site Migration Anomalies', 'A');
INSERT INTO `Dictionary` VALUES ('8', 'JournalActivityTypes', 'Contact Attribute', 'A');
INSERT INTO `Dictionary` VALUES ('4', 'JournalActivityTypes', 'Contact Journal Entry', 'A');
INSERT INTO `Dictionary` VALUES ('10', 'JournalActivityTypes', 'Contact Migration Anomalies', 'A');
INSERT INTO `Dictionary` VALUES ('11', 'JournalActivityTypes', 'VAR Site Attribute', 'A');
INSERT INTO `Dictionary` VALUES ('12', 'JournalActivityTypes', 'VAR Contact Attribute', 'A');
INSERT INTO `Dictionary` VALUES ('13', 'JournalActivityTypes', 'VAR Organization Attribute', 'A');
INSERT INTO `Dictionary` VALUES ('0', 'JournalActivityTypes', 'Contact Security Role', 'A');
INSERT INTO `Dictionary` VALUES ('14', 'JournalActivityTypes', 'VAR Contact Journal Entry', 'A');
INSERT INTO `Dictionary` VALUES ('ATC-LA-MSD', 'BusinessFocus', 'ATC-LA-MSD', 'A');
INSERT INTO `Dictionary` VALUES ('ATC-LA-PSEB', 'BusinessFocus', 'ATC-LA-PSEB', 'A');
INSERT INTO `Dictionary` VALUES ('ATC-LA-AEC', 'BusinessFocus', 'ATC-LA-AEC', 'A');
INSERT INTO `Dictionary` VALUES ('B', 'SiteStatus', 'Submitted', 'A');
INSERT INTO `Dictionary` VALUES ('O', 'SiteStatus', 'Opportunity', 'A');
INSERT INTO `Dictionary` VALUES ('V', 'SiteStatus', 'Approved', 'A');
INSERT INTO `Dictionary` VALUES ('15', 'JournalActivityTypes', 'Contact Certification', 'A');
INSERT INTO `Dictionary` VALUES ('F', 'SiteStatus', 'Fulfillment Center (ATC)', 'X');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3712', 'Live Examinations', 'AEC Building Channel Certification Exam for Sales Representatives', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4173', 'Live Examinations', 'オートデスク・チャネル認定試験　AEC-Building（サポート）', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4171', 'Live Examinations', 'Examen de Certification d\'Autodesk pour les représentant support à la clientèle AEC-Building', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4175', 'Live Examinations', 'Examen de certificación de canal de AEC-Construcción de Autodesk para soporte de product', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4172', 'Live Examinations', 'Esame di certificazione per il canale AEC Building Autodesk per il supporto del prodotto', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4170', 'Live Examinations', 'Autodesk AEC-Building Zertifizierungsprüfung für den Produkt Support', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4169', 'Live Examinations', 'Autodesk AEC-Building Channel Certification 产品支持测试', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4174', 'Live Examinations', 'Autodesk AEC-Building Channel Certification 시험 (Product support)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3731', 'Live Examinations', 'AEC Building Channel Certification Exam for Product Support', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4161', 'Live Examinations', 'オートデスク・チャネル認定試験　AEC-Building（プリセールス）', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4159', 'Live Examinations', 'Examen de Certification d\'Autodesk pour les représentant technical-commerciaux AEC-Building', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4163', 'Live Examinations', 'Examen de certificación de canal de AEC-Construcción de Autodesk para técnicos de preventa.', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4160', 'Live Examinations', 'Esame di certificazione per il canale AEC Building Autodesk per tecnici di prevendita', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4157', 'Live Examinations', 'Autodesk AEC-Building Channel Certification 售前技术测试', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4162', 'Live Examinations', 'Autodesk AEC-Building Channel Certification 시험 (Pre-Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4158', 'Live Examinations', 'Autodesk AEC Building Zertifizierungsprüfung für Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3710', 'Live Examinations', 'AEC Building Channel Certification Exam for Pre-Sales Technical', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4177', 'Live Examinations', 'Autodesk AEC Building Zertifizierungsprüfung für Vertriebsmitarbeiter von Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4181', 'Live Examinations', 'Autodesk AEC-Building Channel Certification 시험 (Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4176', 'Live Examinations', 'Autodesk AEC-Building Channel Certification 销售代表测试', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4179', 'Live Examinations', 'Esame di certificazione per il canale AEC Building Autodesk per i venditori', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4182', 'Live Examinations', 'Examen de certificación de canal de AEC-Construcción de Autodesk para Representantes de Ventas', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4178', 'Live Examinations', 'Examen de Certification d\'Autodesk pour les représentant de vente AEC-Building', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4180', 'Live Examinations', 'オートデスク・チャネル認定試験　AEC-Building（セールス）', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3714', 'Live Examinations', 'AEC Civil Channel Certification Exam for Pre-Sales Technical', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4189', 'Live Examinations', 'Autodesk AEC-Civil Channel Certification 시험 (Pre-Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4184', 'Live Examinations', 'Autodesk AEC-Civil Channel Certification 售前技术测试', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4185', 'Live Examinations', 'Autodesk AEC-Civil Zertifikationsprüfung für Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4187', 'Live Examinations', 'Esame di certificazione per il canale AEC Civil Autodesk per tecnici di prevendita', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4190', 'Live Examinations', 'Examen de certificación de canal de AEC-Civil de Autodesk para técnicos de preventa', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4186', 'Live Examinations', 'Examen de Certification d\'Autodesk pour les représentant technical-commerciaux AEC-Civil', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4188', 'Live Examinations', 'オートデスク・チャネル認定試験　AEC-Civil（プリセールス）', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3729', 'Live Examinations', 'AEC Civil Channel Certification Exam for Product Support', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4196', 'Live Examinations', 'Autodesk AEC-Civil Channel Certification 시험 (Product support)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4191', 'Live Examinations', 'Autodesk AEC-Civil Channel Certification 产品支持测试', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4192', 'Live Examinations', 'Autodesk AEC-Civil Zertifizierungsprüfung für den Produktsupport', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4194', 'Live Examinations', 'Esame di certificazione per il canale AEC Civil Autodesk per il supporto del prodotto', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4197', 'Live Examinations', 'Examen de certificación de canal de AEC-Civil de Autodesk para soporte de producto', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4193', 'Live Examinations', 'Examen de Certification d\'Autodesk pour les représentant support à la clientèle AEC-Civil', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4195', 'Live Examinations', 'オートデスク・チャネル認定試験　AEC-Civil（サポート）', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3716', 'Live Examinations', 'AEC Civil Channel Certification Exam for Sales Representatives', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4198', 'Live Examinations', 'Autodesk AEC-Civil Channel Certification Exam 销售代表测试', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4203', 'Live Examinations', 'Autodesk AEC-Civil Channel Certification 시험 (Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4199', 'Live Examinations', 'Autodesk AEC-Civil Zertifizierungsprüfung für Vertriebsmitarbeiter von Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4201', 'Live Examinations', 'Esame di certificazione per il canale AEC Civil Autodesk per i venditori', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4204', 'Live Examinations', 'Examen de certificación de canal de AEC-Civil de Autodesk para Representantes de Ventas', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4200', 'Live Examinations', 'Examen de Certification d\'Autodesk pour les représentant de vente AEC-Civil', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4202', 'Live Examinations', 'オートデスク・チャネル認定試験　AEC-Civil（セールス）', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4251', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification 시험 (Pre-Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4246', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification 售前技术测试', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4247', 'Live Examinations', 'Autodesk Horizontal Design Zertifizierungsprüfung für Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4248', 'Live Examinations', 'Examen Conception horizontale Autodesk du programme Channel Certification pour les technico-commerciaux', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4252', 'Live Examinations', 'Examen de certificación de canal de diseño horizontal de Autodesk para técnicos de preventa', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3775', 'Live Examinations', 'Horizontal Design Channel Certification Exam for Pre-Sales Technical', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4249', 'Live Examinations', 'Progettazione orizzontale Autodesk - Esame di certificazione canale per tecnici di prevendita', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4250', 'Live Examinations', 'オートデスク・チャネル認定試験　Horizontal Design（プリセールス）', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4259', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification 시험 (Product support)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4254', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification 产品支持测试', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4255', 'Live Examinations', 'Autodesk Horizontal Design Zertifizierungsprüfung für für den Produkt-Support', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4256', 'Live Examinations', 'Examen Conception horizontale du programme Channel Certification pour les techniciens d\'assistance produit', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4260', 'Live Examinations', 'Examen de certificación de canal de diseño horizontal para soporte de producto', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3776', 'Live Examinations', 'Horizontal Design Channel Certification Exam for Product Support', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4257', 'Live Examinations', 'Progettazione orizzontale - Esame di certificazione canale per il supporto del prodotto', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4258', 'Live Examinations', 'オートデスク・チャネル認定試験　Horizontal Design（サポート）', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4266', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification 시험 (Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4261', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification 销售代表测试', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4262', 'Live Examinations', 'Autodesk Horizontal Design Zertifizierungsprüfung für Vertriebsmitarbeiter bei Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4263', 'Live Examinations', 'Examen Conception horizontale Autodesk du programme Channel Certification pour les commerciaux', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4267', 'Live Examinations', 'Examen de certificación de canal de diseño horizontal de Autodesk para Representantes de Ventas', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3774', 'Live Examinations', 'Horizontal Design Channel Certification Exam for Sales Representatives', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4264', 'Live Examinations', 'Progettazione orizzontale Autodesk - Esame di certificazione canale per i venditori', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4265', 'Live Examinations', 'オートデスク・チャネル認定試験　Horizontal Design（セールス）', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-CH-10-O-4060', 'Live Examinations', 'Autodesk Manufacturing Channel Certification - 售前技术测试', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-KO-10-O-4066', 'Live Examinations', 'Autodesk Manufacturing Channel Certification 시험 (Pre-Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-DE-10-O-4062', 'Live Examinations', 'Autodesk Maschinenbau Zertifizierungsprüfung für Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-IT-10-O-4064', 'Live Examinations', 'Esame di certificazione per il Canale Manifatturiero Autodesk per tecnici di Prevendita', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4063', 'Live Examinations', 'Examen Autodesk Manufacturing : programme de certification du Channel pour les technico-commerciaux.', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4067', 'Live Examinations', 'Examen de certificación de canal de manufactura Autodesk para técnicos de preventa', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3428', 'Live Examinations', 'Manufacturing Channel Certification Exam for Pre-Sales Technical', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-JP-10-O-4065', 'Live Examinations', 'オートデスク・チャネル認定試験　製造（プリセールス）', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-KO-10-O-4050', 'Live Examinations', 'Autodesk Manufacturing Channel Certification 시험 (Product support)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-CH-10-O-4044', 'Live Examinations', 'Autodesk Manufacturing Channel Certification 产品支持测试', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-DE-10-O-4046', 'Live Examinations', 'Autodesk Maschinenbau Zertifizierungsprüfung für für den Produkt-Support', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-IT-10-O-4048', 'Live Examinations', 'Esame di certificazione per il Canale Manifatturiero Autodesk per il supporto del prodotto', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4051', 'Live Examinations', 'Examen de certificación de canal de manufactura Autodesk para soporte de producto', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4047', 'Live Examinations', 'Examen Fabrication Autodesk du programme Channel Certification pour les techniciens d\'assistance', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3427', 'Live Examinations', 'Manufacturing Channel Certification Exam for Product Support', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-JP-10-O-4049', 'Live Examinations', 'オートデスク・チャネル認定試験　製造（サポート）', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-KO-10-O-4058', 'Live Examinations', 'Autodesk Manufacturing Channel Certification 시험 (Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-CH-10-O-4052', 'Live Examinations', 'Autodesk Manufacturing Channel Certification 销售代表测试', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-DE-10-O-4054', 'Live Examinations', 'Autodesk Maschinenbau -Zertifizierungsprüfung für Vertriebsmitarbeiter von Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-IT-10-O-4056', 'Live Examinations', 'Esame di certificazione per il Canale Manifatturiero Autodesk per i venditori', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4055', 'Live Examinations', 'Examen Autodesk Manufacturing : programme de certification du Channel pour les commerciaux', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4059', 'Live Examinations', 'Examen de certificación de canal de manufactura Autodesk para Representantes de Ventas', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3418', 'Live Examinations', 'Manufacturing Channel Certification Exam for Sales Representatives', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-JP-10-O-4057', 'Live Examinations', 'オートデスク・チャネル認定試験　製造（セールス）', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4211', 'Live Examinations', 'Autodesk M&E Channel Certification 시험 (Pre-Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4206', 'Live Examinations', 'Autodesk M&E Channel Certification 售前技术测试', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4207', 'Live Examinations', 'Autodesk M&E Zertifizierungsprüfung Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4209', 'Live Examinations', 'Esame di certificazione per il canale M&E Autodesk per tecnici di prevendita', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4212', 'Live Examinations', 'Examen de certificación de canal de M&E de Autodesk para técnicos de preventa', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4208', 'Live Examinations', 'Examen de Certification d\'Autodesk pour les représentant technical-commerciaux, MandE', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3718', 'Live Examinations', 'Media and Entertainment Channel Certification Exam for Pre-Sales Technical', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4210', 'Live Examinations', 'オートデスク・チャネル認定試験　M&E（プリセールス）', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4213', 'Live Examinations', 'AEC Media and Entertainment Channel Certification Exam for Product Support', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4217', 'Live Examinations', 'AEC Media and Entertainment Channel Certification Exam for Product Support', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4218', 'Live Examinations', 'AEC Media and Entertainment Channel Certification Exam for Product Support', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4214', 'Live Examinations', 'Autodesk M&E Zertifizierungsprüfung für den Produkt Support', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4216', 'Live Examinations', 'Esame di certificazione per il canale M&E Autodesk per il supporto del prodotto', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4219', 'Live Examinations', 'Examen de certificación de canal de M&E de Autodesk para soporte de producto.', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4215', 'Live Examinations', 'Examen de Certification d\'Autodesk pour les représentant support à la clientele, MandE', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3727', 'Live Examinations', 'Media and Entertainment Channel Certification Exam for Product Support', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-10-O-4225', 'Live Examinations', 'Autodesk M&E Channel Certification 시험 (Sales)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-10-O-4220', 'Live Examinations', 'Autodesk M&E Channel Certification 销售代表测试', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-10-O-4221', 'Live Examinations', 'Autodesk M&E Zertifizierungsprüfung für Vertriebsmitarbeiter von Vertriebspartnern', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-10-O-4223', 'Live Examinations', 'Esame di certificazione per il canale M&E Autodesk per i venditori', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-10-O-4226', 'Live Examinations', 'Examen de certificación de canal de MyE de Autodesk para Representantes de Ventas', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-10-O-4222', 'Live Examinations', 'Examen de Certification d\'Autodesk pour les représentant de vente, MandE', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-EN-10-O-3720', 'Live Examinations', 'Media and Entertainment Channel Certification Exam for Sales Representatives', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-10-O-4224', 'Live Examinations', 'オートデスク・チャネル認定試験　M&E（セールス）', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWAEC', 'BusinessFocus', 'ATC-WW-AEC (FY12)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWMFG', 'BusinessFocus', 'ATC-WW-MFG (FY12)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWME', 'BusinessFocus', 'ATC-WW-MED (FY12)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWHRZ', 'BusinessFocus', 'ATC-WW-HRZ (FY12)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWSFM2011', 'BusinessFocus', 'ATC-WW-SFM-2011 (FY12)SmkM2011', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWSFM2012', 'BusinessFocus', 'ATC-WW-SFM-2012 (FY12) SmkM2012', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5134', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5135', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5136', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5137', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5138', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5139', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5140', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5141', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5142', 'Live Examinations', 'Проектирование зданий: сертификационный экзамен для партнеров (менеджеры по продажам)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5143', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5144', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5145', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5146', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5147', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5148', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5149', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5150', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5151', 'Live Examinations', 'Проектирование зданий: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5152', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5153', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5154', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5155', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5156', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('kO-CRS-KO-12-O-5157', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5158', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5159', 'Live Examinations', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5160', 'Live Examinations', 'Проектирование зданий: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5161', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5162', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5163', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5164', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5165', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5166', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5167', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5168', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5169', 'Live Examinations', 'Проектирование инфраструктуры: сертификационный экзамен для партнеров (специалисты по продажам)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5170', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5171', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5172', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5173', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5174', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5175', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5176', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5177', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5178', 'Live Examinations', 'Проектирование инфраструктуры: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5179', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5180', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5181', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5182', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5183', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5184', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5185', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5186', 'Live Examinations', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5187', 'Live Examinations', 'Проектирование инфраструктуры: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5188', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5189', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5190', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5191', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5192', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5193', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5194', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5195', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5196', 'Live Examinations', 'Промышленное проектирование: сертификационный экзамен для партнеров (менеджеры по продажам)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5197', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5198', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5199', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5200', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5201', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5202', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5203', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5204', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5205', 'Live Examinations', 'Промышленное проектирование: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5206', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5207', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5208', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5209', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5210', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5211', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5212', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5213', 'Live Examinations', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5214', 'Live Examinations', 'Промышленное проектирование: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5215', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5216', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5217', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5218', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5219', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5220', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5221', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5222', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5223', 'Live Examinations', 'Графика и анимация: сертификационный экзамен для партнеров (специалисты по продажам)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5224', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5225', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5226', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5227', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5228', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5229', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5230', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5231', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5232', 'Live Examinations', 'Графика и анимация: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5233', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5234', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5235', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5236', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5237', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5238', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5239', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5240', 'Live Examinations', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5241', 'Live Examinations', 'Графика и анимация: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5242', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5243', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5244', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5245', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5246', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5247', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5248', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5249', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5250', 'Live Examinations', 'Горизонтальная авторизация: сертификационный экзамен для партнеров (специалисты по продажам)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5251', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5252', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5253', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5254', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5255', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5256', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5257', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5258', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5259', 'Live Examinations', 'Горизонтальная авторизация: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A');
INSERT INTO `Dictionary` VALUES ('CH-CRS-CH-12-O-5260', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A');
INSERT INTO `Dictionary` VALUES ('CE-CRS-DE-12-O-5261', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-FR-12-O-5262', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A');
INSERT INTO `Dictionary` VALUES ('SE-CRS-IT-12-O-5263', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A');
INSERT INTO `Dictionary` VALUES ('JP-CRS-JP-12-O-5264', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A');
INSERT INTO `Dictionary` VALUES ('KO-CRS-KO-12-O-5265', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-ES-12-O-5266', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A');
INSERT INTO `Dictionary` VALUES ('CRS-PB-12-O-5267', 'Live Examinations', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A');
INSERT INTO `Dictionary` VALUES ('EC-CRS-RU-12-O-5268', 'Live Examinations', 'Горизонтальная авторизация: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWAEC13', 'BusinessFocus', 'ATC-WW-AEC (FY13)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWHRZ13', 'BusinessFocus', 'ATC-WW-HRZ (FY13)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWME13', 'BusinessFocus', 'ATC-WW-MED (FY13)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWMFG13', 'BusinessFocus', 'ATC-WW-MFG (FY13)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWSFM201111FY13', 'BusinessFocus', 'ATC-WW-SFM-2011 (FY13)SmkM2011', 'X');
INSERT INTO `Dictionary` VALUES ('ATCWWSFM2011FY13', 'BusinessFocus', 'ATC-WW-SFM-2011 (FY13)SmkM2011', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWSFM2012FY13', 'BusinessFocus', 'ATC-WW-SFM-2012 (FY13) SmkM2012', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWSFM2013FY13', 'BusinessFocus', 'ATC-WW-SFM-2013 (FY13) SmkM2013', 'A');
INSERT INTO `Dictionary` VALUES ('Corporate', 'CompanyTypes', 'Corporate - internal only', 'A');
INSERT INTO `Dictionary` VALUES ('test', 'CompanyTypes', 'test', 'X');
INSERT INTO `Dictionary` VALUES ('ATCWWAEC14', 'BusinessFocus', 'ATC-WW-AEC (FY14)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWHRZ14', 'BusinessFocus', 'ATC-WW-HRZ (FY14)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWMED14', 'BusinessFocus', 'ATC-WW-MED (FY14)', 'A');
INSERT INTO `Dictionary` VALUES ('ATCWWMFG14', 'BusinessFocus', 'ATC-WW-MFG (FY14)', 'A');
INSERT INTO `Dictionary` VALUES ('ADSKAPPRV', 'QualificationTypes', 'Autodesk approved', 'A');
INSERT INTO `Dictionary` VALUES ('1', 'Languages', 'Bulgarian', 'A');
INSERT INTO `Dictionary` VALUES ('10', 'Languages', 'French - Europe', 'A');
INSERT INTO `Dictionary` VALUES ('11', 'Languages', 'German', 'A');
INSERT INTO `Dictionary` VALUES ('12', 'Languages', 'Greek', 'A');
INSERT INTO `Dictionary` VALUES ('13', 'Languages', 'Hungarian', 'A');
INSERT INTO `Dictionary` VALUES ('14', 'Languages', 'Icelandic', 'A');
INSERT INTO `Dictionary` VALUES ('15', 'Languages', 'Indonesian', 'A');
INSERT INTO `Dictionary` VALUES ('16', 'Languages', 'Italian', 'A');
INSERT INTO `Dictionary` VALUES ('17', 'Languages', 'Japanese', 'A');
INSERT INTO `Dictionary` VALUES ('18', 'Languages', 'Korean', 'A');
INSERT INTO `Dictionary` VALUES ('19', 'Languages', 'Latvian', 'A');
INSERT INTO `Dictionary` VALUES ('2', 'Languages', 'Chinese - Simplified', 'A');
INSERT INTO `Dictionary` VALUES ('20', 'Languages', 'Lithuanian', 'A');
INSERT INTO `Dictionary` VALUES ('21', 'Languages', 'Macedonian', 'A');
INSERT INTO `Dictionary` VALUES ('22', 'Languages', 'Norwegian', 'A');
INSERT INTO `Dictionary` VALUES ('23', 'Languages', 'Polish', 'A');
INSERT INTO `Dictionary` VALUES ('24', 'Languages', 'Portuguese - Europe', 'A');
INSERT INTO `Dictionary` VALUES ('25', 'Languages', 'Romanian', 'A');
INSERT INTO `Dictionary` VALUES ('26', 'Languages', 'Russian', 'A');
INSERT INTO `Dictionary` VALUES ('27', 'Languages', 'Croatian', 'A');
INSERT INTO `Dictionary` VALUES ('28', 'Languages', 'Slovenian', 'A');
INSERT INTO `Dictionary` VALUES ('29', 'Languages', 'Spanish - Europe', 'A');
INSERT INTO `Dictionary` VALUES ('3', 'Languages', 'Chinese - Traditional', 'A');
INSERT INTO `Dictionary` VALUES ('30', 'Languages', 'Swedish', 'A');
INSERT INTO `Dictionary` VALUES ('31', 'Languages', 'Turkish', 'A');
INSERT INTO `Dictionary` VALUES ('32', 'Languages', 'Thai', 'A');
INSERT INTO `Dictionary` VALUES ('33', 'Languages', 'Serbian', 'A');
INSERT INTO `Dictionary` VALUES ('34', 'Languages', 'English - Europe', 'A');
INSERT INTO `Dictionary` VALUES ('35', 'Languages', 'English - UK', 'A');
INSERT INTO `Dictionary` VALUES ('36', 'Languages', 'English - South Africa', 'A');
INSERT INTO `Dictionary` VALUES ('37', 'Languages', 'English - APac', 'A');
INSERT INTO `Dictionary` VALUES ('38', 'Languages', 'French - Canada', 'A');
INSERT INTO `Dictionary` VALUES ('39', 'Languages', 'Spanish - Latin America', 'A');
INSERT INTO `Dictionary` VALUES ('4', 'Languages', 'Czech', 'A');
INSERT INTO `Dictionary` VALUES ('40', 'Languages', 'Portuguese - Brazil', 'A');
INSERT INTO `Dictionary` VALUES ('5', 'Languages', 'Danish', 'A');
INSERT INTO `Dictionary` VALUES ('6', 'Languages', 'Dutch', 'A');
INSERT INTO `Dictionary` VALUES ('7', 'Languages', 'English - North America', 'A');
INSERT INTO `Dictionary` VALUES ('8', 'Languages', 'Estonian', 'A');
INSERT INTO `Dictionary` VALUES ('9', 'Languages', 'Finnish', 'A');
INSERT INTO `Dictionary` VALUES ('AT', 'AcademicTargets', 'Students trained', 'A');
INSERT INTO `Dictionary` VALUES ('AP', 'AcademicPrograms', 'Panorama participation', 'A');
INSERT INTO `Dictionary` VALUES ('P', 'AcademicProjects', 'student coursework project', 'A');
INSERT INTO `Dictionary` VALUES ('2015', 'FYIndicator', 'FY15 (2014)', 'A');
INSERT INTO `Dictionary` VALUES ('2016', 'FYIndicator', 'FY16 (2015)', 'A');
INSERT INTO `Dictionary` VALUES ('FSAE', 'AcademicPrograms', 'Formula SAE', 'A');
INSERT INTO `Dictionary` VALUES ('F1S', 'AcademicPrograms', 'Formula 1 in Schools', 'A');
INSERT INTO `Dictionary` VALUES ('ABPA', 'AcademicPrograms', 'Autodesk Building Performance Analysis (BPA) Certificate', 'A');
INSERT INTO `Dictionary` VALUES ('DSTEAM', 'AcademicPrograms', 'Digital STEAM Workshop', 'A');
INSERT INTO `Dictionary` VALUES ('VEX', 'AcademicPrograms', 'VEX Robotics', 'A');
INSERT INTO `Dictionary` VALUES ('WSK', 'AcademicPrograms', 'WorldSkills', 'A');
INSERT INTO `Dictionary` VALUES ('ET', 'AcademicTargets', 'Educators trained', 'A');
INSERT INTO `Dictionary` VALUES ('MoU', 'AcademicTargets', 'Memoranda of Understanding with institutions', 'A');
INSERT INTO `Dictionary` VALUES ('2017', 'FYIndicator', 'FY17 (2016)', 'A');
INSERT INTO `Dictionary` VALUES ('2018', 'FYIndicator', 'FY18 (2017)', 'A');
INSERT INTO `Dictionary` VALUES ('SGD', 'Currencies', 'Singapore Dollar', 'A');
INSERT INTO `Dictionary` VALUES ('INR', 'Currencies', 'Indian Rupee', 'A');
INSERT INTO `Dictionary` VALUES ('CAD', 'Currencies', 'Canadian Dollar', 'A');
INSERT INTO `Dictionary` VALUES ('BRL', 'Currencies', 'Brazilian Real', 'A');
INSERT INTO `Dictionary` VALUES ('MXN', 'Currencies', 'Mexican Peso', 'A');
INSERT INTO `Dictionary` VALUES ('SEK', 'Currencies', 'Swedish Krona', 'A');
INSERT INTO `Dictionary` VALUES ('NOK', 'Currencies', 'Norwegian Krone', 'A');
INSERT INTO `Dictionary` VALUES ('ZAR', 'Currencies', 'South African Rand', 'A');
INSERT INTO `Dictionary` VALUES ('AED', 'Currencies', 'Emirati Dirham', 'A');
INSERT INTO `Dictionary` VALUES ('HKD', 'Currencies', 'Hong Kong Dollar', 'A');
INSERT INTO `Dictionary` VALUES ('TRY', 'Currencies', 'Turkish Lira', 'A');
INSERT INTO `Dictionary` VALUES ('RUB', 'Currencies', 'Russian Ruble', 'A');
INSERT INTO `Dictionary` VALUES ('M', 'Gender', 'Male', 'A');
INSERT INTO `Dictionary` VALUES ('F', 'Gender', 'Female', 'A');

-- ----------------------------
-- Table structure for eva_courses
-- ----------------------------
DROP TABLE IF EXISTS `eva_courses`;
CREATE TABLE `eva_courses` (
  `CourseID` int(11) NOT NULL,
  `CourseCode` varchar(12) DEFAULT NULL,
  `InstructorID` int(11) DEFAULT NULL,
  `SiteID` int(11) DEFAULT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `Title` varchar(150) DEFAULT NULL,
  `SurveyID` int(11) DEFAULT NULL,
  `DateLastUpdated` datetime DEFAULT NULL,
  `IsDeleted` tinyint(4) DEFAULT NULL,
  `ToBeExported` tinyint(4) DEFAULT NULL,
  `EvaluationSystemsID` int(11) DEFAULT NULL,
  `OnSiteFacility` tinyint(4) DEFAULT NULL,
  `OnSiteComputer` tinyint(4) DEFAULT NULL,
  `Institution` varchar(200) DEFAULT NULL,
  `LogoID` int(11) DEFAULT NULL,
  PRIMARY KEY (`CourseID`),
  KEY `InstructorID` (`InstructorID`),
  KEY `LogoID` (`LogoID`),
  KEY `SiteID` (`SiteID`),
  KEY `SurveyID` (`SurveyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_courses
-- ----------------------------
INSERT INTO `eva_courses` VALUES ('1', 'C001', null, null, '2018-01-02 00:00:00', '2018-01-05 00:00:00', 'Math', null, '2018-01-29 16:36:08', null, null, null, null, null, 'Unjani', null);
INSERT INTO `eva_courses` VALUES ('21', 'asd', null, null, '2018-01-14 00:00:00', '2018-01-03 00:00:00', 'asd', null, '2018-01-30 23:28:48', null, null, null, null, null, 'asd', null);
INSERT INTO `eva_courses` VALUES ('123', 'asd', null, null, '2018-01-14 00:00:00', '2018-01-07 00:00:00', 'Science', null, '2018-01-30 13:08:17', null, null, null, null, null, 'Unjani', null);

-- ----------------------------
-- Table structure for eva_courses_answers
-- ----------------------------
DROP TABLE IF EXISTS `eva_courses_answers`;
CREATE TABLE `eva_courses_answers` (
  `CourseAnswerID` int(11) NOT NULL,
  `CourseID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `AnswerID` int(11) DEFAULT NULL,
  `EntityID` int(11) DEFAULT NULL,
  `OtherText` text,
  PRIMARY KEY (`CourseAnswerID`),
  KEY `AnswerID` (`AnswerID`),
  KEY `QuestionID` (`QuestionID`),
  KEY `IX_tblCoursesAnswers_CourseID_AnswerID` (`CourseID`,`AnswerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_courses_answers
-- ----------------------------

-- ----------------------------
-- Table structure for eva_courses_evaluations
-- ----------------------------
DROP TABLE IF EXISTS `eva_courses_evaluations`;
CREATE TABLE `eva_courses_evaluations` (
  `CourseID` int(11) NOT NULL,
  `EvaluationID` int(11) NOT NULL,
  PRIMARY KEY (`CourseID`,`EvaluationID`),
  UNIQUE KEY `IX_tblCoursesEvaluations` (`EvaluationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_courses_evaluations
-- ----------------------------

-- ----------------------------
-- Table structure for eva_eval_answers
-- ----------------------------
DROP TABLE IF EXISTS `eva_eval_answers`;
CREATE TABLE `eva_eval_answers` (
  `EvalAnswerID` int(11) NOT NULL,
  `EvaluationID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `AnswerID` int(11) DEFAULT NULL,
  `EntityID` int(11) DEFAULT NULL,
  `OtherID` int(11) DEFAULT NULL,
  PRIMARY KEY (`EvalAnswerID`),
  KEY `AnswerID` (`AnswerID`),
  KEY `QuestionID` (`QuestionID`),
  KEY `IX_tblEvalAnswers_Evaluation` (`EvaluationID`,`QuestionID`,`AnswerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_eval_answers
-- ----------------------------

-- ----------------------------
-- Table structure for eva_eval_comments
-- ----------------------------
DROP TABLE IF EXISTS `eva_eval_comments`;
CREATE TABLE `eva_eval_comments` (
  `EvalCommentID` int(11) NOT NULL,
  `Text` text NOT NULL,
  `ExternalID` int(11) DEFAULT NULL,
  PRIMARY KEY (`EvalCommentID`),
  KEY `IX_tblEvalComments_ExternalID` (`ExternalID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_eval_comments
-- ----------------------------

-- ----------------------------
-- Table structure for eva_eval_questionsanswers
-- ----------------------------
DROP TABLE IF EXISTS `eva_eval_questionsanswers`;
CREATE TABLE `eva_eval_questionsanswers` (
  `EvaluationID` int(11) NOT NULL,
  `A1` int(11) NOT NULL,
  `A2` int(11) NOT NULL,
  `A3` int(11) NOT NULL,
  `A4` int(11) NOT NULL,
  `A5` int(11) NOT NULL,
  `A6` int(11) NOT NULL,
  `A7` int(11) NOT NULL,
  `A8` int(11) NOT NULL,
  `A9` int(11) NOT NULL,
  `A10` int(11) NOT NULL,
  `A11` int(11) NOT NULL,
  `A12` int(11) NOT NULL,
  `A13` int(11) NOT NULL,
  `A14` int(11) NOT NULL,
  `A15` int(11) NOT NULL,
  `A16` int(11) NOT NULL,
  `A17` int(11) NOT NULL,
  `A18` int(11) NOT NULL,
  `A19` int(11) NOT NULL,
  `A20` int(11) NOT NULL,
  `A21` int(11) NOT NULL,
  `A22` int(11) NOT NULL,
  `A23` int(11) NOT NULL,
  `A24` int(11) NOT NULL,
  `A25` int(11) NOT NULL,
  `A26` int(11) NOT NULL,
  `A27` int(11) NOT NULL,
  `A28` int(11) NOT NULL,
  `A29` int(11) NOT NULL,
  `A30` int(11) NOT NULL,
  `A31` int(11) NOT NULL,
  `A32` int(11) NOT NULL,
  `A33` int(11) NOT NULL,
  `A34` int(11) NOT NULL,
  `A35` int(11) NOT NULL,
  PRIMARY KEY (`EvaluationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_eval_questionsanswers
-- ----------------------------

-- ----------------------------
-- Table structure for eva_evaluations
-- ----------------------------
DROP TABLE IF EXISTS `eva_evaluations`;
CREATE TABLE `eva_evaluations` (
  `EvaluationID` int(11) NOT NULL,
  `StudentEvaluationID` varchar(14) NOT NULL,
  `StudentID` int(11) NOT NULL,
  `SiteID` int(11) NOT NULL,
  `InstructorID` int(11) NOT NULL,
  `SurveyID` int(11) NOT NULL,
  `CountryID` int(11) NOT NULL,
  `StateID` int(11) DEFAULT NULL,
  `CourseStartDate` datetime NOT NULL,
  `CourseEndDate` datetime NOT NULL,
  `AuthorizeAutodesk` tinyint(4) NOT NULL,
  `AuthorizeATC` tinyint(4) NOT NULL,
  `AuthorizePublicity` tinyint(4) NOT NULL,
  `ProductVersionID` int(11) NOT NULL,
  `NbAnswerFR` int(11) NOT NULL,
  `ScoreFR` int(11) NOT NULL,
  `NbAnswerIQ` int(11) NOT NULL,
  `ScoreIQ` int(11) NOT NULL,
  `NbAnswerCCM` int(11) NOT NULL,
  `ScoreCCM` int(11) NOT NULL,
  `NbAnswerOP` int(11) NOT NULL,
  `ScoreOP` int(11) NOT NULL,
  `DateSubmitted` datetime NOT NULL,
  `Quarter` int(11) NOT NULL,
  `FutureTraining` tinyint(4) NOT NULL,
  `EvaluationSystemID` int(11) DEFAULT NULL,
  `ExternalID` varchar(20) DEFAULT NULL,
  `NbAnswerOE` int(11) NOT NULL,
  `ScoreOE` int(11) NOT NULL,
  `ToBeExported` tinyint(4) NOT NULL,
  `NbAnswerFROnSite` int(11) NOT NULL,
  `ScoreFROnSite` int(11) NOT NULL,
  `NbAnswerOPOnSite` int(11) NOT NULL,
  `ScoreOPOnSite` int(11) NOT NULL,
  `ActualDateSubmitted` datetime NOT NULL,
  PRIMARY KEY (`EvaluationID`),
  KEY `InstructorID` (`InstructorID`),
  KEY `StateID` (`StateID`),
  KEY `IX_tblEvaluations` (`SiteID`,`InstructorID`,`SurveyID`),
  KEY `IX_tblEvaluations_1` (`StudentID`,`SurveyID`),
  KEY `IX_tblEvaluations_2` (`CountryID`,`StateID`),
  KEY `IX_tblEvaluations_StudentEvaluationID` (`StudentEvaluationID`),
  KEY `IX_tblEvaluations_3` (`DateSubmitted`),
  KEY `IX_tblEvaluations_Responses` (`SiteID`,`SurveyID`,`DateSubmitted`),
  KEY `IX_tblEvaluations_SurveyCourseDate` (`SurveyID`,`CourseStartDate`,`CourseEndDate`),
  KEY `IX_tblEvaluations_4` (`ProductVersionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_evaluations
-- ----------------------------

-- ----------------------------
-- Table structure for eva_evaluations_certificate
-- ----------------------------
DROP TABLE IF EXISTS `eva_evaluations_certificate`;
CREATE TABLE `eva_evaluations_certificate` (
  `EvaluationID` int(11) NOT NULL,
  `LastPrint` datetime DEFAULT NULL,
  `LastSend` datetime DEFAULT NULL,
  `CertificateNo` varchar(10) NOT NULL,
  PRIMARY KEY (`EvaluationID`),
  KEY `IX_tblEvaluationsCertificate` (`CertificateNo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_evaluations_certificate
-- ----------------------------

-- ----------------------------
-- Table structure for eva_evaluations_systems
-- ----------------------------
DROP TABLE IF EXISTS `eva_evaluations_systems`;
CREATE TABLE `eva_evaluations_systems` (
  `EvaluationSystemsID` int(11) NOT NULL,
  `Description` varchar(50) NOT NULL,
  PRIMARY KEY (`EvaluationSystemsID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_evaluations_systems
-- ----------------------------

-- ----------------------------
-- Table structure for eva_post_evalanswers
-- ----------------------------
DROP TABLE IF EXISTS `eva_post_evalanswers`;
CREATE TABLE `eva_post_evalanswers` (
  `EvalAnswerID` int(11) NOT NULL,
  `PostEvaluationID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `AnswerID` int(11) DEFAULT NULL,
  `EntityID` int(11) DEFAULT NULL,
  `Text` text,
  `ValueDate` datetime DEFAULT NULL,
  PRIMARY KEY (`EvalAnswerID`),
  KEY `QuestionID` (`QuestionID`),
  KEY `IX_tblPostEvalAnswers_PostEval` (`PostEvaluationID`,`QuestionID`,`AnswerID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_post_evalanswers
-- ----------------------------

-- ----------------------------
-- Table structure for eva_post_evaluations
-- ----------------------------
DROP TABLE IF EXISTS `eva_post_evaluations`;
CREATE TABLE `eva_post_evaluations` (
  `PostEvaluationID` int(11) NOT NULL,
  `SurveyID` int(11) DEFAULT NULL,
  `DateSubmitted` datetime DEFAULT NULL,
  `Quarter` int(11) DEFAULT NULL,
  `EvaluationSystemID` int(11) DEFAULT NULL,
  `ExternalID` varchar(20) DEFAULT NULL,
  `InfoIncorrect` tinyint(4) NOT NULL,
  `CountryID` int(11) DEFAULT NULL,
  `SiteID` int(11) DEFAULT NULL,
  `CourseTitle` varchar(200) DEFAULT NULL,
  `CourseStartDate` datetime DEFAULT NULL,
  `ProductVersionID` int(11) DEFAULT NULL,
  `VoucherCodeID` int(11) DEFAULT NULL,
  PRIMARY KEY (`PostEvaluationID`),
  KEY `EvaluationSystemID` (`EvaluationSystemID`),
  KEY `VoucherCodeID` (`VoucherCodeID`),
  KEY `IX_tblPostEvaluations` (`PostEvaluationID`),
  KEY `IX_tblPostEvaluations_Survey` (`SurveyID`,`CourseStartDate`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_post_evaluations
-- ----------------------------

-- ----------------------------
-- Table structure for eva_post_surveymatchquestions
-- ----------------------------
DROP TABLE IF EXISTS `eva_post_surveymatchquestions`;
CREATE TABLE `eva_post_surveymatchquestions` (
  `PostSurveyMatchQuestions` int(11) NOT NULL,
  `SurveyID` int(11) DEFAULT NULL,
  `PostSurveyID` int(11) DEFAULT NULL,
  `PostSurveyName` varchar(30) DEFAULT NULL,
  `SurveyName` varchar(30) DEFAULT NULL,
  `QuestionID` int(11) DEFAULT NULL,
  `PostQuestionID` int(11) DEFAULT NULL,
  `DeviationType` varchar(10) NOT NULL,
  PRIMARY KEY (`PostSurveyMatchQuestions`),
  KEY `PostSurveyID` (`PostSurveyID`,`PostQuestionID`),
  KEY `SurveyID` (`SurveyID`,`QuestionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_post_surveymatchquestions
-- ----------------------------

-- ----------------------------
-- Table structure for eva_post_surveysent
-- ----------------------------
DROP TABLE IF EXISTS `eva_post_surveysent`;
CREATE TABLE `eva_post_surveysent` (
  `PostSurveySentID` int(11) NOT NULL,
  `EvaluationID` int(11) NOT NULL,
  `PostEvaluationID` int(11) DEFAULT NULL,
  `SendDate` datetime NOT NULL,
  `VisitedDate` datetime DEFAULT NULL,
  `ExternalID` int(11) DEFAULT NULL,
  `ToBeExported` tinyint(4) NOT NULL,
  PRIMARY KEY (`PostSurveySentID`),
  KEY `PostEvaluationID` (`PostEvaluationID`),
  KEY `IX_tblPostSurveySent_EvalPsotEval` (`EvaluationID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_post_surveysent
-- ----------------------------

-- ----------------------------
-- Table structure for eva_survey
-- ----------------------------
DROP TABLE IF EXISTS `eva_survey`;
CREATE TABLE `eva_survey` (
  `SurveyID` int(11) NOT NULL,
  `Name` varchar(25) NOT NULL,
  `Year` int(11) NOT NULL,
  `IsActive` tinyint(4) NOT NULL,
  `StartDate` datetime NOT NULL,
  `EndDate` datetime DEFAULT NULL,
  `NbQuestionFR` int(11) NOT NULL,
  `NbQuestionIQ` int(11) NOT NULL,
  `NbQuestionCCM` int(11) NOT NULL,
  `NbQuestionOP` int(11) NOT NULL,
  `FiscalYear` varchar(4) DEFAULT NULL,
  `TypeSurveyID` int(11) NOT NULL,
  `PostSurveyID` int(11) DEFAULT NULL,
  PRIMARY KEY (`SurveyID`),
  KEY `PostSurveyID` (`PostSurveyID`),
  KEY `TypeSurveyID` (`TypeSurveyID`),
  KEY `IX_TblSurveys_YearTypeSurveyID` (`Year`,`TypeSurveyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_survey
-- ----------------------------

-- ----------------------------
-- Table structure for eva_survey_questionconstraint
-- ----------------------------
DROP TABLE IF EXISTS `eva_survey_questionconstraint`;
CREATE TABLE `eva_survey_questionconstraint` (
  `SurveyQuestionConstraintID` int(11) NOT NULL,
  `SurveyID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `ConstraintTypeID` int(11) NOT NULL,
  `QuestionConstraintID` int(11) DEFAULT NULL,
  `ConstraintValue` int(11) DEFAULT NULL,
  `ConditionID` int(11) DEFAULT NULL,
  PRIMARY KEY (`SurveyQuestionConstraintID`),
  KEY `ConditionID` (`ConditionID`),
  KEY `ConstraintTypeID` (`ConstraintTypeID`),
  KEY `SurveyID` (`SurveyID`,`QuestionConstraintID`,`ConstraintValue`),
  KEY `SurveyID_2` (`SurveyID`,`QuestionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_survey_questionconstraint
-- ----------------------------

-- ----------------------------
-- Table structure for eva_survey_questions
-- ----------------------------
DROP TABLE IF EXISTS `eva_survey_questions`;
CREATE TABLE `eva_survey_questions` (
  `SurveyID` int(11) NOT NULL,
  `QuestionID` int(11) NOT NULL,
  `DisplayOrder` int(11) NOT NULL,
  `ParentID` int(11) DEFAULT NULL,
  `IsPreFill` tinyint(4) NOT NULL,
  `InsertLineAfter` tinyint(4) NOT NULL,
  `QuestionDependencies` tinyint(4) NOT NULL,
  `ApplyCertification` tinyint(4) NOT NULL,
  `OrderOptionID` int(11) DEFAULT NULL,
  `Columns` int(11) NOT NULL,
  `Section` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`SurveyID`,`QuestionID`),
  KEY `OrderOptionID` (`OrderOptionID`),
  KEY `QuestionID` (`QuestionID`),
  KEY `SurveyID` (`SurveyID`,`ParentID`),
  KEY `IX_tblSurveyQuestions_` (`SurveyID`,`QuestionID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of eva_survey_questions
-- ----------------------------

-- ----------------------------
-- Table structure for Glossary
-- ----------------------------
DROP TABLE IF EXISTS `Glossary`;
CREATE TABLE `Glossary` (
  `glossary_id` int(8) NOT NULL AUTO_INCREMENT,
  `glossary_term` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Invoice',
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`glossary_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`glossary_term`)
) ENGINE=MyISAM AUTO_INCREMENT=122 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Glossary
-- ----------------------------
INSERT INTO `Glossary` VALUES ('114', 'Active', 'Company or site is an active ATC', 'A', 'admin', '0001-01-01 00:00:00', 'admin', '2018-01-13 14:59:18');
INSERT INTO `Glossary` VALUES ('115', 'Affiliated Sites', 'If an Instructor teaches at a particular site, we say the Instructor is Affiliated with the Site. Use the Site Summary page to affiliate an Instructor to a Site.', 'A', null, '2018-01-13 00:00:00', null, '2018-01-13 00:00:00');
INSERT INTO `Glossary` VALUES ('2', 'Anamolies -', 'Data saved during the migration. Any duplicate data or data that the migration may have had a hard time with is also saved in this area to allow for manual cleanup.', null, null, '0001-01-01 00:00:00', null, '0001-01-01 00:00:00');
INSERT INTO `Glossary` VALUES ('116', 'ATC Director', 'Main company contact for all ATC business, can be ATC Manager', 'A', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:56:55');
INSERT INTO `Glossary` VALUES ('3', 'Attachments', 'Administrators may upload and attach files 2MB or smaller to records within the system.', 'A', 'ATCDB000', '2006-10-11 18:07:58', 'morganj', '2008-03-05 04:49:54');
INSERT INTO `Glossary` VALUES ('4', 'Attributes', 'Tracks specific activities and policies about the organization, site, or instructor agreed by ATC Team', 'A', 'ATCDB000', '2006-10-11 18:08:54', 'morganj', '2008-03-05 04:48:58');
INSERT INTO `Glossary` VALUES ('5', 'Billing Contact', 'Person you send the invoice to', 'A', 'ATCDB000', '2006-10-11 18:09:10', 'morganj', '2008-03-05 04:49:29');
INSERT INTO `Glossary` VALUES ('6', 'Company Type', 'Specifies if the company is a Reseller, Training Organization, or Educational institution. Only one value is allowed.', 'A', 'ATCDB000', '2006-10-11 18:09:25', 'morganj', '2008-03-05 04:50:17');
INSERT INTO `Glossary` VALUES ('9', 'Contract Address', 'Address that the ATC Agreement should be sent to', 'A', 'ATCDB000', '2006-10-13 07:56:53', 'morganj', '2008-03-05 04:48:45');
INSERT INTO `Glossary` VALUES ('12', 'Legal Contact', 'Person who signs the ATC Agreement', 'A', '41118', '2007-05-22 08:00:14', 'morganj', '2008-03-05 04:48:32');
INSERT INTO `Glossary` VALUES ('15', 'Instructor', 'An Instructor is a person who teaches classes', 'A', '41118', '2007-05-22 08:06:06', 'morganj', '2008-03-05 04:48:19');
INSERT INTO `Glossary` VALUES ('117', 'Instructor Qualifications', 'Tracks Products and versions, as well as how an instructor proved their knowledge', 'A', null, '2018-01-13 14:59:57', null, '2018-01-13 14:59:57');
INSERT INTO `Glossary` VALUES ('18', 'Invoice', 'Helps keep track of the Autodesk business contract with a partner company.', 'A', '41118', '2007-09-04 12:15:54', '41118', '2007-09-17 11:04:31');
INSERT INTO `Glossary` VALUES ('19', 'Invoicing Address', 'Address that the Invoice should be sent to', 'A', '41118', '2007-09-05 04:44:35', '41118', '2008-01-09 04:14:39');
INSERT INTO `Glossary` VALUES ('20', 'Journal Entries', 'Tracks daily correspondence with partner', 'A', '41118', '2007-09-17 11:02:23', 't_barrw', '2007-09-17 11:55:39');
INSERT INTO `Glossary` VALUES ('21', 'Magellan Id', 'Old ATC Site ID used in EMEA only historically', 'A', 'morganj', '2007-09-25 10:49:57', 'morganj', '2008-03-05 04:50:29');
INSERT INTO `Glossary` VALUES ('22', 'Mailing Address', 'Mail/Post is received here, can be a PO Box address', 'A', 'morganj', '2007-09-25 10:51:11', 'morganj', '2008-03-05 04:50:42');
INSERT INTO `Glossary` VALUES ('23', 'Organization', 'An Organization represents the Business Entity with whom Autodesk does business', 'A', 'morganj', '2007-09-25 10:52:10', 'morganj', '2008-03-05 04:51:57');
INSERT INTO `Glossary` VALUES ('24', 'Pending', 'Usually a status assigned to new ATC\'s who haven\'t completed registration process', 'A', 'morganj', '2007-09-25 10:52:59', 'morganj', '2008-03-05 04:52:08');
INSERT INTO `Glossary` VALUES ('25', 'Qualifications', 'Tracks instructor product knowledge', 'A', 'morganj', '2007-09-25 10:53:56', 'morganj', '2007-09-26 08:08:51');
INSERT INTO `Glossary` VALUES ('26', 'Registered Address', 'Main Company address for its ATC business', 'A', 'morganj', '2007-09-25 10:55:45', 'morganj', '2008-03-05 04:51:27');
INSERT INTO `Glossary` VALUES ('27', 'Rejected', 'Have not been successful with their application to become an ATC', 'A', 'morganj', '2007-09-25 10:56:37', 'morganj', '2008-03-05 04:51:45');
INSERT INTO `Glossary` VALUES ('28', 'Room', 'Not in use at this time', 'A', '41118', '2007-09-26 06:24:15', '41118', '2008-01-09 04:15:26');
INSERT INTO `Glossary` VALUES ('29', 'SAP', 'Main number used by Autodesk Finance departments. Also used by the Evaluation System reporting.', 'A', 'morganj', '2007-12-18 11:47:43', 'morganj', '2008-03-05 04:52:19');
INSERT INTO `Glossary` VALUES ('30', 'Shipping Address', 'Shipments that need to be signed for, PO Box is not allowed here', 'A', '41118', '2008-01-09 04:10:46', 'jiangp', '2008-01-15 06:43:10');
INSERT INTO `Glossary` VALUES ('31', 'Site', 'A Site is a training location', 'A', 'jiangp', '2008-01-15 06:45:46', 'chongl', '2012-03-21 01:21:36');
INSERT INTO `Glossary` VALUES ('32', 'Site Accreditations', 'ATC Program Accreditaion policy should be referenced here before use', 'A', 'jiangp', '2008-01-15 06:46:43', 'chongl', '2012-03-14 21:29:39');
INSERT INTO `Glossary` VALUES ('33', 'Site Address', 'Location where the ATC Training takes place', 'A', 'jiangp', '2008-01-15 06:47:43', 'chongl', '2012-03-14 21:29:17');
INSERT INTO `Glossary` VALUES ('34', 'Site Administrator', 'Main administration contact, registrar in the US', 'A', 'jiangp', '2008-01-15 06:48:32', 'chongl', '2012-03-14 21:28:55');
INSERT INTO `Glossary` VALUES ('35', 'Site Manager', 'Main ATC business contact for a site, can be ATC Manager', 'A', 'morganj', '2008-01-18 11:32:38', 'morganj', '2008-03-05 04:52:30');
INSERT INTO `Glossary` VALUES ('36', 'Status', 'Tracks ATC company and site status', 'A', 'morganj', '2008-01-18 11:33:14', 'morganj', '2008-03-05 04:52:42');
INSERT INTO `Glossary` VALUES ('37', 'Suspended', 'Used if under investigation or a temporary status between Active and Terminated', 'A', 'morganj', '2008-04-16 10:21:53', 'morganj', '2008-04-16 10:32:22');
INSERT INTO `Glossary` VALUES ('38', 'Tax Exempt', 'Whether the company is exempt from VAT taxes.', 'A', 'morganj', '2008-04-16 10:22:49', 'morganj', '2008-04-16 10:46:53');
INSERT INTO `Glossary` VALUES ('39', 'Terminated', 'Not in the Program anymore', 'A', 'morganj', '2008-04-16 10:23:43', 'morganj', '2008-04-16 10:46:35');
INSERT INTO `Glossary` VALUES ('40', 'VAT', 'Value Added Tax', 'A', 'herculg', '2008-09-26 18:00:07', 'herculg', '2008-09-26 18:28:52');
INSERT INTO `Glossary` VALUES ('41', 'Workstations', 'The total number of workstations available to students at this facility', 'A', 'herculg', '2008-09-29 12:17:40', 'herculg', '2009-01-06 17:15:38');
INSERT INTO `Glossary` VALUES ('42', 'Serial Number', '*** TODO ***', 'A', 'herculg', '2008-09-29 12:18:48', 'herculg', '2008-09-29 12:19:31');
INSERT INTO `Glossary` VALUES ('43', 'Organization / Site Relationsh', 'A site must belong to exactly one Organization. An Organization may have any number of sites.', 'A', 'herculg', '2008-10-02 16:04:32', 'herculg', '2008-10-22 18:17:54');
INSERT INTO `Glossary` VALUES ('44', 'Legacy Site Id', 'Identifier from prior systems. This is used to remember the old ID for this item prior to migration of the data into this database.', 'A', 'herculg', '2008-10-02 17:09:21', 'herculg', '2008-10-02 17:09:57');
INSERT INTO `Glossary` VALUES ('45', 'Old Contract Id', 'AMERICAS ONLY - used for tracking purposes and SAP data creation, this ID is used to link the current contract with a previous one.', 'A', 'herculg', '2008-10-02 17:10:56', 'herculg', '2008-10-02 17:11:42');
INSERT INTO `Glossary` VALUES ('46', 'CC Exp Date', 'AMERICAS ONLY - Credit Card Expiration Date - used to track the expiration date on the credit card used to make a payment.', 'A', 'herculg', '2008-10-02 17:12:26', 'herculg', '2008-10-02 17:12:43');
INSERT INTO `Glossary` VALUES ('47', 'CC Name', 'AMERICAS ONLY - Credit Card Name - used to track the name on the credit card used to make a payment.', 'A', 'herculg', '2008-10-02 17:13:22', 'herculg', '2008-10-02 17:13:31');
INSERT INTO `Glossary` VALUES ('48', 'Site Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'A', 'herculg', '2008-10-02 17:15:24', 'herculg', '2008-10-02 17:15:39');
INSERT INTO `Glossary` VALUES ('49', 'Site Migration Anomalies', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'A', 'herculg', '2008-10-02 17:16:02', 'herculg', '2008-10-02 17:16:23');
INSERT INTO `Glossary` VALUES ('50', 'VAR Contact Attribute', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'A', 'herculg', '2008-10-02 17:17:26', 'herculg', '2008-10-02 17:17:38');
INSERT INTO `Glossary` VALUES ('51', 'VAR Contact Journal Entry', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'A', 'herculg', '2008-10-02 17:18:03', 'herculg', '2008-10-02 17:18:15');
INSERT INTO `Glossary` VALUES ('52', 'Contact Certification', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'A', 'herculg', '2008-10-02 17:18:55', 'herculg', '2008-10-02 17:19:07');
INSERT INTO `Glossary` VALUES ('53', 'Contact Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'A', 'herculg', '2008-10-02 17:19:30', 'herculg', '2008-10-02 17:19:42');
INSERT INTO `Glossary` VALUES ('54', 'Contact Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'A', 'herculg', '2008-10-02 17:20:07', 'herculg', '2008-10-02 17:20:21');
INSERT INTO `Glossary` VALUES ('55', 'Contact Migration Anomalies', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'A', 'herculg', '2008-10-02 17:20:49', 'herculg', '2008-10-02 17:21:04');
INSERT INTO `Glossary` VALUES ('56', 'Contact Security Role', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2', 'A', 'herculg', '2008-10-22 17:32:34', 'herculg', '2008-10-22 18:18:00');
INSERT INTO `Glossary` VALUES ('57', 'Organization Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2', 'A', 'herculg', '2008-10-22 17:44:45', 'herculg', '2008-10-22 18:18:16');
INSERT INTO `Glossary` VALUES ('58', 'Organization Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2', 'A', 'herculg', '2008-10-22 17:46:31', 'herculg', '2008-10-22 18:17:44');
INSERT INTO `Glossary` VALUES ('59', 'Site Accreditation', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2', 'A', 'herculg', '2008-10-22 17:56:23', 'herculg', '2008-10-22 18:18:25');
INSERT INTO `Glossary` VALUES ('60', 'Site Attribute', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 2', 'A', 'herculg', '2008-10-22 17:58:17', 'herculg', '2008-10-22 18:23:23');
INSERT INTO `Glossary` VALUES ('61', 'Site Event', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 2', 'A', 'herculg', '2008-10-22 18:06:05', 'herculg', '2008-10-22 18:23:49');
INSERT INTO `Glossary` VALUES ('62', 'Site Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'A', 'herculg', '2008-10-22 18:07:04', 'herculg', '2008-10-22 18:23:35');
INSERT INTO `Glossary` VALUES ('63', 'Site Migration Anomalies', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'A', 'herculg', '2008-10-22 18:07:50', 'herculg', '2008-10-22 19:01:14');
INSERT INTO `Glossary` VALUES ('64', 'VAR Contact Attribute', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 1', 'A', 'herculg', '2008-10-22 18:21:08', 'herculg', '2008-10-22 18:23:09');
INSERT INTO `Glossary` VALUES ('65', 'VAR Contact Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 1', 'A', 'herculg', '2008-10-22 18:21:33', 'herculg', '2008-10-22 18:24:33');
INSERT INTO `Glossary` VALUES ('66', 'VAR Organization Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 1', 'A', 'herculg', '2008-10-22 18:26:02', 'herculg', '2008-10-22 18:27:05');
INSERT INTO `Glossary` VALUES ('67', 'VAR Site Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 1', 'A', 'herculg', '2008-10-22 18:26:40', 'herculg', '2008-10-22 18:27:36');
INSERT INTO `Glossary` VALUES ('68', 'Contact Certification', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 3', 'A', 'herculg', '2008-10-22 18:55:28', 'herculg', '2008-10-22 18:56:41');
INSERT INTO `Glossary` VALUES ('69', 'Contact Attribute', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 3', 'A', 'herculg', '2008-10-22 18:57:21', 'herculg', '2008-10-22 18:57:41');
INSERT INTO `Glossary` VALUES ('70', 'Contact Journal Entry', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 3', 'A', 'herculg', '2008-10-22 18:58:16', 'herculg', '2008-10-22 18:58:48');
INSERT INTO `Glossary` VALUES ('71', 'Contact Migration Anomalies', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 2', 'A', 'herculg', '2008-10-22 18:59:15', 'herculg', '2008-10-22 19:07:37');
INSERT INTO `Glossary` VALUES ('72', 'Contact Security Role', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 3', 'A', 'herculg', '2008-10-22 19:08:45', 'herculg', '2008-10-22 19:09:04');
INSERT INTO `Glossary` VALUES ('73', 'Organization Attribute', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'A', 'Marcov', '2008-11-05 06:28:20', 'morganj', '2008-11-13 10:54:44');
INSERT INTO `Glossary` VALUES ('74', 'Organization Journal Entry', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'A', 'Marcov', '2008-11-05 06:29:19', 'morganj', '2008-11-13 10:50:41');
INSERT INTO `Glossary` VALUES ('75', 'Site Accreditation', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'A', 'Marcov', '2008-11-05 06:30:13', 'morganj', '2008-11-13 10:55:32');
INSERT INTO `Glossary` VALUES ('76', 'Site Attribute', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'A', 'Marcov', '2008-11-05 06:30:54', 'morganj', '2008-11-13 10:53:56');
INSERT INTO `Glossary` VALUES ('77', 'Site Event', 'EMIA: 2009 (FY10) ATC PROGRAM - EC3 CIS - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'A', 'Marcov', '2008-11-05 06:31:29', 'morganj', '2008-11-13 10:48:24');
INSERT INTO `Glossary` VALUES ('78', 'Site Journal Entry', 'EMIA: 2009 (FY10) ATC PROGRAM - SAPAC - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'A', 'Marcov', '2008-11-05 06:32:07', 'morganj', '2008-12-15 09:10:34');
INSERT INTO `Glossary` VALUES ('79', 'Site Migration Anomalies', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'A', 'Marcov', '2008-11-05 06:32:43', 'morganj', '2008-11-17 05:24:32');
INSERT INTO `Glossary` VALUES ('80', 'VAR Contact Attribute', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'A', 'Marcov', '2008-11-05 06:33:13', 'morganj', '2008-11-17 05:23:49');
INSERT INTO `Glossary` VALUES ('81', 'VAR Contact Journal Entry', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'A', 'Marcov', '2008-11-05 06:33:39', 'morganj', '2008-11-17 05:24:11');
INSERT INTO `Glossary` VALUES ('82', 'VAR Organization Attribute', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'A', 'Marcov', '2008-11-05 06:34:09', 'morganj', '2008-11-17 05:24:52');

-- ----------------------------
-- Table structure for Invoice_type
-- ----------------------------
DROP TABLE IF EXISTS `Invoice_type`;
CREATE TABLE `Invoice_type` (
  `invoice_type_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Invoice',
  `invoice_type` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoiced_amount` decimal(20,2) DEFAULT NULL,
  `invoiced_currency` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contract_start_date` date DEFAULT NULL,
  `contract_end_date` date DEFAULT NULL,
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`invoice_type_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_type`)
) ENGINE=MyISAM AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Invoice_type
-- ----------------------------
INSERT INTO `Invoice_type` VALUES ('3', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - EC2', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', '3710.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'ATCDB000', '2006-10-11 18:07:58', 'morganj', '2008-03-05 04:49:54');
INSERT INTO `Invoice_type` VALUES ('4', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC2', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '8500.00', 'EUR', '2007-02-01', '2008-01-31', 'A', 'ATCDB000', '2006-10-11 18:08:54', 'morganj', '2008-03-05 04:48:58');
INSERT INTO `Invoice_type` VALUES ('5', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - UK', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '6050.00', 'EUR', '2007-02-01', '2008-01-31', 'A', 'ATCDB000', '2006-10-11 18:09:10', 'morganj', '2008-03-05 04:49:29');
INSERT INTO `Invoice_type` VALUES ('6', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - UK', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', '9010.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'ATCDB000', '2006-10-11 18:09:25', 'morganj', '2008-03-05 04:50:17');
INSERT INTO `Invoice_type` VALUES ('9', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '3650.00', 'EUR', '2007-02-01', '2008-01-31', 'A', 'ATCDB000', '2006-10-13 07:56:53', 'morganj', '2008-03-05 04:48:45');
INSERT INTO `Invoice_type` VALUES ('12', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - UK', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', '1420.00', 'EUR', '2007-02-01', '2008-01-31', 'A', '41118', '2007-05-22 08:00:14', 'morganj', '2008-03-05 04:48:32');
INSERT INTO `Invoice_type` VALUES ('15', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - ME', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', '2000.00', 'EUR', '2007-02-01', '2008-01-31', 'A', '41118', '2007-05-22 08:06:06', 'morganj', '2008-03-05 04:48:19');
INSERT INTO `Invoice_type` VALUES ('18', 'ATC Invoice', 'AMER: 2008 (FY09) Renewal', 'ATC Program Renewal Fee in the Program year 2008.', '0.00', 'USD', '2008-02-01', '2009-01-31', 'A', '41118', '2007-09-04 12:15:54', '41118', '2007-09-17 11:04:31');
INSERT INTO `Invoice_type` VALUES ('19', 'ATC Invoice', 'Apac: 2007 (FY08) Renewal ANZ/Australia', 'Program membership Fee for 2007', '0.00', 'NZD', '2007-02-28', '2008-01-31', 'X', '41118', '2007-09-05 04:44:35', '41118', '2008-01-09 04:14:39');
INSERT INTO `Invoice_type` VALUES ('20', 'ATC Invoice', 'AMER: 2008 (FY09) ATC Program Fee', 'ATC Program Fee for new ATC Applications in the Program year 2008.', '0.00', 'USD', '2008-02-01', '2009-01-31', 'A', '41118', '2007-09-17 11:02:23', 't_barrw', '2007-09-17 11:55:39');
INSERT INTO `Invoice_type` VALUES ('21', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - EC1', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', '0.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'morganj', '2007-09-25 10:49:57', 'morganj', '2008-03-05 04:50:29');
INSERT INTO `Invoice_type` VALUES ('22', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - EC2', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', '0.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'morganj', '2007-09-25 10:51:11', 'morganj', '2008-03-05 04:50:42');
INSERT INTO `Invoice_type` VALUES ('23', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - ME', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '0.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'morganj', '2007-09-25 10:52:10', 'morganj', '2008-03-05 04:51:57');
INSERT INTO `Invoice_type` VALUES ('24', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - UK', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '0.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'morganj', '2007-09-25 10:52:59', 'morganj', '2008-03-05 04:52:08');
INSERT INTO `Invoice_type` VALUES ('25', 'ATC Invoice', 'EMEA:  2008 (FY09) INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '0.00', 'EUR', '2008-02-01', '2009-01-31', 'X', 'morganj', '2007-09-25 10:53:56', 'morganj', '2007-09-26 08:08:51');
INSERT INTO `Invoice_type` VALUES ('26', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '0.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'morganj', '2007-09-25 10:55:45', 'morganj', '2008-03-05 04:51:27');
INSERT INTO `Invoice_type` VALUES ('27', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC2', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '0.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'morganj', '2007-09-25 10:56:37', 'morganj', '2008-03-05 04:51:45');
INSERT INTO `Invoice_type` VALUES ('28', 'ATC Invoice', 'APAC: FY09  NEW APPLICATION (INDIA)', null, '0.00', 'USD', '2007-02-01', '2008-01-31', 'X', '41118', '2007-09-26 06:24:15', '41118', '2008-01-09 04:15:26');
INSERT INTO `Invoice_type` VALUES ('29', 'ATC Invoice', 'EMIA 2008 (FY09) CREDIT NOTE ANNUAL RENEWAL', 'Credit Notes or refunds', '0.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'morganj', '2007-12-18 11:47:43', 'morganj', '2008-03-05 04:52:19');
INSERT INTO `Invoice_type` VALUES ('30', 'ATC Invoice', 'Apac FY09 ATC PROG New App Annual Fee', 'ATC Program Fee for new ATC Applications', '0.00', 'USD', '2008-02-01', '2009-01-30', 'X', '41118', '2008-01-09 04:10:46', 'jiangp', '2008-01-15 06:43:10');
INSERT INTO `Invoice_type` VALUES ('31', 'ATC Invoice', 'AAPAC ATC Program', 'ATC Annual Program Fee', '0.00', 'AUD', '2012-02-01', '2013-01-31', 'A', 'jiangp', '2008-01-15 06:45:46', 'chongl', '2012-03-21 01:21:36');
INSERT INTO `Invoice_type` VALUES ('32', 'ATC Invoice', 'AAPAC ATC Certification', 'ATC Certification Fee', '0.00', 'USD', '2012-02-01', '2013-01-31', 'A', 'jiangp', '2008-01-15 06:46:43', 'chongl', '2012-03-14 21:29:39');
INSERT INTO `Invoice_type` VALUES ('33', 'ATC Invoice', 'AAPac AOTG DSL', 'AOTG DSL fee', '0.00', 'USD', '2012-02-01', '2013-01-31', 'A', 'jiangp', '2008-01-15 06:47:43', 'chongl', '2012-03-14 21:29:17');
INSERT INTO `Invoice_type` VALUES ('34', 'ATC Invoice', 'AAPac AOTG Courseware', 'AOTG hard copy Courseware fee', '0.00', 'USD', '2012-02-01', '2013-01-31', 'A', 'jiangp', '2008-01-15 06:48:32', 'chongl', '2012-03-14 21:28:55');
INSERT INTO `Invoice_type` VALUES ('35', 'ATC Invoice', 'EMIA 2008 (FY09) CREDIT NOTE NEW APPLICATION', null, '0.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'morganj', '2008-01-18 11:32:38', 'morganj', '2008-03-05 04:52:30');
INSERT INTO `Invoice_type` VALUES ('36', 'ATC Invoice', 'EMIA 2008 (FY09) CREDIT NOTE SLEI', null, '0.00', 'EUR', '2008-02-01', '2009-01-31', 'A', 'morganj', '2008-01-18 11:33:14', 'morganj', '2008-03-05 04:52:42');
INSERT INTO `Invoice_type` VALUES ('37', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - SAARC/INDIA', null, '0.00', 'USD', '2008-02-01', '2009-01-31', 'A', 'morganj', '2008-04-16 10:21:53', 'morganj', '2008-04-16 10:32:22');
INSERT INTO `Invoice_type` VALUES ('38', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - SAARC/INDIA', null, '0.00', 'USD', '2008-02-01', '2009-01-31', 'A', 'morganj', '2008-04-16 10:22:49', 'morganj', '2008-04-16 10:46:53');
INSERT INTO `Invoice_type` VALUES ('39', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM AOTC DSL - SAARC/INDIA', null, '0.00', 'USD', '2008-02-01', '2009-01-31', 'A', 'morganj', '2008-04-16 10:23:43', 'morganj', '2008-04-16 10:46:35');
INSERT INTO `Invoice_type` VALUES ('40', 'ATC Invoice', 'AMER: 2009 (FY10) ATC Program Fee LA (12) -  Model 2', 'LA ATC Fee Model 2 (1 Essentials DSL Title)', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-09-26 18:00:07', 'herculg', '2008-09-26 18:28:52');
INSERT INTO `Invoice_type` VALUES ('41', 'ATC Invoice', 'AMER: 2009 (FY10) ATC Program Fee', 'ATC Program Fee for new ATC Applications in the Program year 2009.', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-09-29 12:17:40', 'herculg', '2009-01-06 17:15:38');
INSERT INTO `Invoice_type` VALUES ('42', 'ATC Invoice', 'AMER: 2009 (FY10) Renewal', 'ATC Program Renewal Fee in the Program year 2009.', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-09-29 12:18:48', 'herculg', '2008-09-29 12:19:31');
INSERT INTO `Invoice_type` VALUES ('43', 'ATC Invoice', 'AMER: 2009 (FY10) ATC Program Fee LA (12) SAT-  Model 2', 'LA ATC Fee Model 2 (1 Essentials DSL Title)', '0.00', 'USD', '2009-02-01', '2010-01-31', 'X', 'herculg', '2008-10-02 16:04:32', 'herculg', '2008-10-22 18:17:54');
INSERT INTO `Invoice_type` VALUES ('44', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:09:21', 'herculg', '2008-10-02 17:09:57');
INSERT INTO `Invoice_type` VALUES ('45', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:10:56', 'herculg', '2008-10-02 17:11:42');
INSERT INTO `Invoice_type` VALUES ('46', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:12:26', 'herculg', '2008-10-02 17:12:43');
INSERT INTO `Invoice_type` VALUES ('47', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:13:22', 'herculg', '2008-10-02 17:13:31');
INSERT INTO `Invoice_type` VALUES ('48', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:15:24', 'herculg', '2008-10-02 17:15:39');
INSERT INTO `Invoice_type` VALUES ('49', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:16:02', 'herculg', '2008-10-02 17:16:23');
INSERT INTO `Invoice_type` VALUES ('50', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:17:26', 'herculg', '2008-10-02 17:17:38');
INSERT INTO `Invoice_type` VALUES ('51', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:18:03', 'herculg', '2008-10-02 17:18:15');
INSERT INTO `Invoice_type` VALUES ('52', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:18:55', 'herculg', '2008-10-02 17:19:07');
INSERT INTO `Invoice_type` VALUES ('53', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:19:30', 'herculg', '2008-10-02 17:19:42');
INSERT INTO `Invoice_type` VALUES ('54', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:20:07', 'herculg', '2008-10-02 17:20:21');
INSERT INTO `Invoice_type` VALUES ('55', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-02 17:20:49', 'herculg', '2008-10-02 17:21:04');
INSERT INTO `Invoice_type` VALUES ('56', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Essentials Title', '0.00', 'USD', '2009-02-01', '2010-01-31', 'X', 'herculg', '2008-10-22 17:32:34', 'herculg', '2008-10-22 18:18:00');
INSERT INTO `Invoice_type` VALUES ('57', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Industry / SAT', '0.00', 'USD', '2009-02-02', '2010-01-31', 'X', 'herculg', '2008-10-22 17:44:45', 'herculg', '2008-10-22 18:18:16');
INSERT INTO `Invoice_type` VALUES ('58', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Essentials', '0.00', 'USD', '2009-02-01', '2010-10-31', 'X', 'herculg', '2008-10-22 17:46:31', 'herculg', '2008-10-22 18:17:44');
INSERT INTO `Invoice_type` VALUES ('59', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Essentials Title / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'X', 'herculg', '2008-10-22 17:56:23', 'herculg', '2008-10-22 18:18:25');
INSERT INTO `Invoice_type` VALUES ('60', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 17:58:17', 'herculg', '2008-10-22 18:23:23');
INSERT INTO `Invoice_type` VALUES ('61', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:06:05', 'herculg', '2008-10-22 18:23:49');
INSERT INTO `Invoice_type` VALUES ('62', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'LA ATC Fee Model 2  + 2 Essentials Title', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:07:04', 'herculg', '2008-10-22 18:23:35');
INSERT INTO `Invoice_type` VALUES ('63', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title', '0.00', 'USD', '2009-02-01', '2010-01-31', 'X', 'herculg', '2008-10-22 18:07:50', 'herculg', '2008-10-22 19:01:14');
INSERT INTO `Invoice_type` VALUES ('64', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:21:08', 'herculg', '2008-10-22 18:23:09');
INSERT INTO `Invoice_type` VALUES ('65', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:21:33', 'herculg', '2008-10-22 18:24:33');
INSERT INTO `Invoice_type` VALUES ('66', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:26:02', 'herculg', '2008-10-22 18:27:05');
INSERT INTO `Invoice_type` VALUES ('67', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:26:40', 'herculg', '2008-10-22 18:27:36');
INSERT INTO `Invoice_type` VALUES ('68', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:55:28', 'herculg', '2008-10-22 18:56:41');
INSERT INTO `Invoice_type` VALUES ('69', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:57:21', 'herculg', '2008-10-22 18:57:41');
INSERT INTO `Invoice_type` VALUES ('70', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:58:16', 'herculg', '2008-10-22 18:58:48');
INSERT INTO `Invoice_type` VALUES ('71', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 18:59:15', 'herculg', '2008-10-22 19:07:37');
INSERT INTO `Invoice_type` VALUES ('72', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title / SAT', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'herculg', '2008-10-22 19:08:45', 'herculg', '2008-10-22 19:09:04');
INSERT INTO `Invoice_type` VALUES ('73', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:28:20', 'morganj', '2008-11-13 10:54:44');
INSERT INTO `Invoice_type` VALUES ('74', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:29:19', 'morganj', '2008-11-13 10:50:41');
INSERT INTO `Invoice_type` VALUES ('75', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:30:13', 'morganj', '2008-11-13 10:55:32');
INSERT INTO `Invoice_type` VALUES ('76', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:30:54', 'morganj', '2008-11-13 10:53:56');
INSERT INTO `Invoice_type` VALUES ('77', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC3 CIS - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:31:29', 'morganj', '2008-11-13 10:48:24');
INSERT INTO `Invoice_type` VALUES ('78', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - SAPAC - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:32:07', 'morganj', '2008-12-15 09:10:34');
INSERT INTO `Invoice_type` VALUES ('79', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:32:43', 'morganj', '2008-11-17 05:24:32');
INSERT INTO `Invoice_type` VALUES ('80', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:33:13', 'morganj', '2008-11-17 05:23:49');
INSERT INTO `Invoice_type` VALUES ('81', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:33:39', 'morganj', '2008-11-17 05:24:11');
INSERT INTO `Invoice_type` VALUES ('82', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:34:09', 'morganj', '2008-11-17 05:24:52');
INSERT INTO `Invoice_type` VALUES ('83', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC3 CIS - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:34:34', 'morganj', '2008-11-17 05:24:43');
INSERT INTO `Invoice_type` VALUES ('84', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - SAPAC - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'Marcov', '2008-11-05 06:34:58', 'morganj', '2008-12-15 09:10:21');
INSERT INTO `Invoice_type` VALUES ('85', 'ATC Invoice', 'AMER: 2010 (FY11):  A&P Program Fee', 'Standard 1 user', '0.00', 'USD', '2010-02-01', '2010-12-31', 'A', 'garyh', '2009-09-29 11:17:48', 'garyh', '2009-09-29 11:17:48');
INSERT INTO `Invoice_type` VALUES ('86', 'A&P Invoice', 'VIRTUAL: A&P Invoices from ADN', null, '0.00', 'USD', '2016-02-01', '2016-02-01', 'A', 't_barrw', '2009-09-30 09:51:55', 't_barrw', '2009-09-30 09:51:55');
INSERT INTO `Invoice_type` VALUES ('87', 'ATC Invoice', 'ATC Discount', 'Prorated Discount', '0.00', 'USD', '2009-02-01', '2010-01-31', 'A', 'garyh', '2009-10-02 13:41:56', 'garyh', '2009-10-02 13:41:56');
INSERT INTO `Invoice_type` VALUES ('88', 'VAR Invoice', 'VAR Invoice Type Sample', 'Modify as needed', '0.00', 'EUR', '2016-02-01', '2016-02-01', 'A', 't_barrw', '2009-10-24 08:45:23', 't_barrw', '2009-10-24 08:47:03');
INSERT INTO `Invoice_type` VALUES ('89', 'ATC Invoice', 'AMER: 2010 (FY11) ATC Program Fee', 'ATC Program Fee for new ATC Applications in the Program year 2010.', '0.00', 'USD', '2010-02-01', '2011-01-31', 'A', 'garyh', '2009-11-11 15:56:18', 'garyh', '2009-11-11 15:56:18');
INSERT INTO `Invoice_type` VALUES ('90', 'ATC Invoice', 'AMER: 2010 (FY11) Renewal', 'ATC Program Renewal Fee in the Program year 2010.', '0.00', 'USD', '2010-02-01', '2011-01-31', 'A', 'garyh', '2009-11-11 15:57:15', 'garyh', '2009-11-11 15:58:06');
INSERT INTO `Invoice_type` VALUES ('91', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS1 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:46:28', 'janm', '2009-12-08 10:57:31');
INSERT INTO `Invoice_type` VALUES ('92', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS2 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:47:40', 'janm', '2009-12-08 10:58:09');
INSERT INTO `Invoice_type` VALUES ('93', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTON FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:48:19', 'janm', '2009-12-08 10:59:08');
INSERT INTO `Invoice_type` VALUES ('94', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS4 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:49:00', 'janm', '2009-12-08 10:58:58');
INSERT INTO `Invoice_type` VALUES ('95', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS1 - NEW MAIN SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:50:19', 'janm', '2009-12-08 10:57:19');
INSERT INTO `Invoice_type` VALUES ('96', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS2 - NEW MAIN SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:51:07', 'janm', '2009-12-08 10:57:57');
INSERT INTO `Invoice_type` VALUES ('97', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - NEW MAIN SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:53:00', 'janm', '2009-12-08 10:58:45');
INSERT INTO `Invoice_type` VALUES ('98', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS4 - NEW MAIN - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:53:24', 'janm', '2009-12-08 10:59:23');
INSERT INTO `Invoice_type` VALUES ('99', 'ATC Invoice', 'EMEA ATC PROGRAM - ANNUAL SOFTWARE & SUBSCRIPTION FEE - NEW SITE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'A', 'janm', '2009-12-08 10:54:20', 'janm', '2009-12-08 11:00:17');
INSERT INTO `Invoice_type` VALUES ('100', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS2 - NEW ADD SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:54:46', 'janm', '2009-12-08 10:57:45');
INSERT INTO `Invoice_type` VALUES ('101', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - NEW ADD SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:55:10', 'janm', '2009-12-08 10:58:20');
INSERT INTO `Invoice_type` VALUES ('102', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - NEW ADD SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'X', 'janm', '2009-12-08 10:55:32', 'janm', '2009-12-08 10:58:33');
INSERT INTO `Invoice_type` VALUES ('103', 'ATC Invoice', 'EMEA ATC PROGRAM - ANNUAL SOFTWARE & SUBSCRIPTION FEE - RENEWAL', null, '0.00', 'EUR', '2010-02-01', '2011-01-31', 'A', 'janm', '2009-12-08 11:00:33', 'janm', '2009-12-08 11:00:52');
INSERT INTO `Invoice_type` VALUES ('104', 'ATC Invoice', 'APAC: FY17 ATC Program Fee', 'Fees for ATC Software Subscription & ATC Program Membership', '0.00', 'USD', '2016-02-01', '2017-01-31', 'A', 'robneudecker@engageglobalsolutions.com', '2016-07-21 09:39:07', 'selina.shen@autodesk.com', '2016-07-28 23:28:23');
INSERT INTO `Invoice_type` VALUES ('105', 'ATC Invoice', 'AMER: FY17 ATC Program Fee', 'Fees for ATC Software Subscription & ATC Program Membership', '0.00', 'USD', '2016-02-01', '2016-02-01', 'A', 'selina.shen@autodesk.com', '2016-07-25 04:29:00', 'selina.shen@autodesk.com', '2016-07-25 04:29:00');
INSERT INTO `Invoice_type` VALUES ('106', 'ATC Invoice', 'EMEA: FY17 ATC Program Fee', 'Fees for ATC Software Subscription & ATC Program Membership', '0.00', 'USD', '2016-02-01', '2016-02-01', 'A', 'selina.shen@autodesk.com', '2016-07-25 04:31:30', 'selina.shen@autodesk.com', '2016-07-25 04:31:30');
INSERT INTO `Invoice_type` VALUES ('124', 'A&P Invoice', 'test 2', 'sd', null, 'USD', '2018-01-30', '2018-02-02', '', null, '0000-00-00 00:00:00', null, '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for Invoice_type_category_copy
-- ----------------------------
DROP TABLE IF EXISTS `Invoice_type_category_copy`;
CREATE TABLE `Invoice_type_category_copy` (
  `activity_type_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`activity_type_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_type`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Invoice_type_category_copy
-- ----------------------------
INSERT INTO `Invoice_type_category_copy` VALUES ('2', 'A&P Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Invoice_type_category_copy` VALUES ('4', 'VAR Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Invoice_type_category_copy` VALUES ('3', 'ATC Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');

-- ----------------------------
-- Table structure for Invoices
-- ----------------------------
DROP TABLE IF EXISTS `Invoices`;
CREATE TABLE `Invoices` (
  `InvoiceId` int(8) NOT NULL AUTO_INCREMENT,
  `OrgId` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `InvoiceNumber` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractID` varchar(30) COLLATE utf8_unicode_ci DEFAULT '',
  `InvoiceType` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `InvoiceDescription` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractStatus` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `InvoiceDate` date DEFAULT NULL,
  `POReceivedDate` date DEFAULT NULL,
  `POID` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TotalInvoicedAmount` decimal(20,2) DEFAULT '0.00',
  `TotalLineAmount` decimal(20,2) DEFAULT NULL,
  `InvoicedCurrency` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TotalVATAmount` decimal(20,2) DEFAULT '0.00',
  `TotalPaymentAmount` decimal(20,2) DEFAULT NULL,
  `AmountOutstanding` decimal(20,2) DEFAULT NULL,
  `PaymentAmount` decimal(20,2) DEFAULT '0.00',
  `PaymentDate` date DEFAULT NULL,
  `PaymentMonth` int(2) DEFAULT NULL,
  `PaymentCurrency` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PaymentMethod` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PaymentReference` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CCExpDate` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CCName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PaymentAmount2` decimal(20,2) DEFAULT NULL,
  `PaymentDate2` date DEFAULT NULL,
  `PaymentMonth2` int(2) DEFAULT NULL,
  `PaymentMethod2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PaymentReference2` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CCExpDate2` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CCName2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteIdList` varchar(400) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L1_Summary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L1_Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L1_Quantity` int(3) DEFAULT NULL,
  `L1_UnitPrice` decimal(20,2) DEFAULT NULL,
  `L1_TotalAmount` decimal(20,2) DEFAULT NULL,
  `L1_VATRate` decimal(20,2) DEFAULT NULL,
  `L1_VATAmount` decimal(20,2) DEFAULT NULL,
  `L2_Summary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L2_Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L2_Quantity` int(3) DEFAULT NULL,
  `L2_UnitPrice` decimal(20,2) DEFAULT NULL,
  `L2_TotalAmount` decimal(20,2) DEFAULT NULL,
  `L2_VATRate` decimal(20,2) DEFAULT NULL,
  `L2_VATAmount` decimal(20,2) DEFAULT NULL,
  `L3_Summary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L3_Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L3_Quantity` int(3) DEFAULT NULL,
  `L3_UnitPrice` decimal(20,2) DEFAULT NULL,
  `L3_TotalAmount` decimal(20,2) DEFAULT NULL,
  `L3_VATRate` decimal(20,2) DEFAULT NULL,
  `L3_VATAmount` decimal(20,2) DEFAULT NULL,
  `L4_Summary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L4_Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L4_Quantity` int(3) DEFAULT NULL,
  `L4_UnitPrice` decimal(20,2) DEFAULT NULL,
  `L4_TotalAmount` decimal(20,2) DEFAULT NULL,
  `L4_VATRate` decimal(20,2) DEFAULT NULL,
  `L4_VATAmount` decimal(20,2) DEFAULT NULL,
  `L5_Summary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L5_Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L5_Quantity` int(3) DEFAULT NULL,
  `L5_UnitPrice` decimal(20,2) DEFAULT NULL,
  `L5_TotalAmount` decimal(20,2) DEFAULT NULL,
  `L5_VATRate` decimal(20,2) DEFAULT NULL,
  `L5_VATAmount` decimal(20,2) DEFAULT NULL,
  `L6_Summary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L6_Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L6_Quantity` int(3) DEFAULT NULL,
  `L6_UnitPrice` decimal(20,2) DEFAULT NULL,
  `L6_TotalAmount` decimal(20,2) DEFAULT NULL,
  `L6_VATRate` decimal(20,2) DEFAULT NULL,
  `L6_VATAmount` decimal(20,2) DEFAULT NULL,
  `L7_Summary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L7_Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L7_Quantity` int(3) DEFAULT NULL,
  `L7_UnitPrice` decimal(20,2) DEFAULT NULL,
  `L7_TotalAmount` decimal(20,2) DEFAULT NULL,
  `L7_VATRate` decimal(20,2) DEFAULT NULL,
  `L7_VATAmount` decimal(20,2) DEFAULT NULL,
  `L8_Summary` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L8_Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `L8_Quantity` int(3) DEFAULT NULL,
  `L8_UnitPrice` decimal(20,2) DEFAULT NULL,
  `L8_TotalAmount` decimal(20,2) DEFAULT NULL,
  `L8_VATRate` decimal(20,2) DEFAULT NULL,
  `L8_VATAmount` decimal(20,2) DEFAULT NULL,
  `ContractStartDate` date DEFAULT NULL,
  `ContractEndDate` date DEFAULT NULL,
  `OldContractId` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Comments` text COLLATE utf8_unicode_ci,
  `DateAdded` datetime DEFAULT '0000-00-00 00:00:00',
  `AddedBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateLastAdmin` datetime DEFAULT '0000-00-00 00:00:00',
  `LastAdminBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` char(1) COLLATE utf8_unicode_ci DEFAULT '',
  PRIMARY KEY (`InvoiceId`),
  KEY `IDX_INVOICES_OrgId` (`OrgId`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Invoices
-- ----------------------------
INSERT INTO `Invoices` VALUES ('2', 'OE300384', '123', '7550193684', '3', 'ATC Program Annual Fee', 'Active', '2018-04-01', '2018-02-01', '123', '0.00', '0.00', 'Active', '0.00', '0.00', '0.00', '0.00', '2008-07-30', '6', 'USD', '', '', '', '', '0.00', null, null, '', '', '', '', '', 'AAPac', '', null, null, null, null, null, '', '', null, null, null, null, null, '', '', null, null, null, null, null, '', '', null, null, null, null, null, '', '', null, null, null, null, null, '', '', null, null, null, null, null, '', '', null, null, null, null, null, '', '', null, null, null, null, null, '2008-02-01', '2018-02-01', '123', null, '2007-05-02 11:45:25', 'herculg', '2008-08-03 12:37:15', 'nancyj', 'A');
INSERT INTO `Invoices` VALUES ('3', 'ORG2788', 'asd', 'asd', '23', 'asd', 'Active', '2018-01-14', '2018-01-05', 'a12', '0.00', null, 'Active', '0.00', null, null, '0.00', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '2018-01-09', '2018-01-03', '123', null, '0000-00-00 00:00:00', null, '0000-00-00 00:00:00', null, '');

-- ----------------------------
-- Table structure for InvoiceTypes
-- ----------------------------
DROP TABLE IF EXISTS `InvoiceTypes`;
CREATE TABLE `InvoiceTypes` (
  `InvoiceTypeId` int(8) NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `AddedBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateLastAdmin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastAdminBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `InvoiceType` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ActivityType` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Invoice',
  `Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicedAmount` decimal(20,2) DEFAULT NULL,
  `InvoicedCurrency` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractStartDate` date DEFAULT NULL,
  `ContractEndDate` date DEFAULT NULL,
  PRIMARY KEY (`InvoiceTypeId`),
  KEY `IDX_INVOICETYPE_ActivityType` (`ActivityType`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of InvoiceTypes
-- ----------------------------
INSERT INTO `InvoiceTypes` VALUES ('1', '2006-10-11 17:31:12', 'ATCDB000', '2008-03-05 04:51:14', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - UK', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', '0.00', 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('2', '2006-10-11 17:51:18', 'ATCDB000', '2008-03-05 04:49:41', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - EC1', 'ATC Invoice', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', '1500.00', 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('3', '2006-10-11 18:07:58', 'ATCDB000', '2008-03-05 04:49:54', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - EC2', 'ATC Invoice', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', '3710.00', 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('4', '2006-10-11 18:08:54', 'ATCDB000', '2008-03-05 04:48:58', 'morganj', 'A', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC2', 'ATC Invoice', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '8500.00', 'EUR', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('5', '2006-10-11 18:09:10', 'ATCDB000', '2008-03-05 04:49:29', 'morganj', 'A', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - UK', 'ATC Invoice', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '6050.00', 'EUR', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('6', '2006-10-11 18:09:25', 'ATCDB000', '2008-03-05 04:50:17', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - UK', 'ATC Invoice', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', '9010.00', 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('7', '2006-10-11 18:10:20', 'ATCDB000', '2008-03-05 04:50:06', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - ME', 'ATC Invoice', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', '2500.00', 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('9', '2006-10-13 07:56:53', 'ATCDB000', '2008-03-05 04:48:45', 'morganj', 'A', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'ATC Invoice', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '3650.00', 'EUR', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('8', '2006-10-12 10:56:59', 'ATCDB000', '2008-01-15 06:43:33', 'jiangp', 'X', 'Apac: AOTC Site Licensing Fee - 2008', 'ATC Invoice', 'Courseware (AOTC) Site License for use at your Training Facility', '10000.00', 'USD', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('10', '2007-03-01 15:16:28', 'ATCDB000', '2007-09-25 07:59:32', '41118', 'A', 'ATC Contract', 'ATC Invoice', null, null, 'USD', null, null);
INSERT INTO `InvoiceTypes` VALUES ('11', '2007-05-22 07:57:21', '41118', '2008-03-05 04:48:06', 'morganj', 'A', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - EC2', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', '2000.00', 'EUR', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('12', '2007-05-22 08:00:14', '41118', '2008-03-05 04:48:32', 'morganj', 'A', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - UK', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', '1420.00', 'EUR', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('13', '2007-05-22 08:01:12', '41118', '2008-03-05 04:47:53', 'morganj', 'A', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - EC1', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', '860.00', 'EUR', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('14', '2007-05-22 08:02:09', '41118', '2008-03-05 04:49:16', 'morganj', 'A', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - ME', 'ATC Invoice', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '2120.00', 'EUR', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('15', '2007-05-22 08:06:06', '41118', '2008-03-05 04:48:19', 'morganj', 'A', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - ME', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', '2000.00', 'EUR', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('16', '2007-08-16 09:35:07', '41118', '2008-03-05 04:50:53', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - ME', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', '0.00', 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('17', '2007-09-03 10:08:46', '41118', '2007-09-17 11:55:31', 't_barrw', 'A', 'AMER: 2007 ATC Program Fee', 'ATC Invoice', 'ATC Program Fee from February 1, 2007 to January 31, 2008', null, 'USD', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('18', '2007-09-04 12:15:54', '41118', '2007-09-17 11:04:31', '41118', 'A', 'AMER: 2008 (FY09) Renewal', 'ATC Invoice', 'ATC Program Renewal Fee in the Program year 2008.', null, 'USD', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('19', '2007-09-05 04:44:35', '41118', '2008-01-09 04:14:39', '41118', 'X', 'Apac: 2007 (FY08) Renewal ANZ/Australia', 'ATC Invoice', 'Program membership Fee for 2007', null, 'NZD', '2007-02-28', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('20', '2007-09-17 11:02:23', '41118', '2007-09-17 11:55:39', 't_barrw', 'A', 'AMER: 2008 (FY09) ATC Program Fee', 'ATC Invoice', 'ATC Program Fee for new ATC Applications in the Program year 2008.', null, 'USD', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('21', '2007-09-25 10:49:57', 'morganj', '2008-03-05 04:50:29', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - EC1', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('22', '2007-09-25 10:51:11', 'morganj', '2008-03-05 04:50:42', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - EC2', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('23', '2007-09-25 10:52:10', 'morganj', '2008-03-05 04:51:57', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - ME', 'ATC Invoice', 'Fees for an increase in ATC Software License Entitlements 2008-2009', null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('24', '2007-09-25 10:52:59', 'morganj', '2008-03-05 04:52:08', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - UK', 'ATC Invoice', 'Fees for an increase in ATC Software License Entitlements 2008-2009', null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('25', '2007-09-25 10:53:56', 'morganj', '2007-09-26 08:08:51', 'morganj', 'X', 'EMEA:  2008 (FY09) INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'ATC Invoice', 'Fees for an increase in ATC Software License Entitlements 2008-2009', null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('26', '2007-09-25 10:55:45', 'morganj', '2008-03-05 04:51:27', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'ATC Invoice', 'Fees for an increase in ATC Software License Entitlements 2008-2009', null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('27', '2007-09-25 10:56:37', 'morganj', '2008-03-05 04:51:45', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC2', 'ATC Invoice', 'Fees for an increase in ATC Software License Entitlements 2008-2009', null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('28', '2007-09-26 06:24:15', '41118', '2008-01-09 04:15:26', '41118', 'X', 'APAC: FY09  NEW APPLICATION (INDIA)', 'ATC Invoice', null, null, 'USD', '2007-02-01', '2008-01-31');
INSERT INTO `InvoiceTypes` VALUES ('29', '2007-12-18 11:47:43', 'morganj', '2008-03-05 04:52:19', 'morganj', 'A', 'EMIA 2008 (FY09) CREDIT NOTE ANNUAL RENEWAL', 'ATC Invoice', 'Credit Notes or refunds', null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('30', '2008-01-09 04:10:46', '41118', '2008-01-15 06:43:10', 'jiangp', 'X', 'Apac FY09 ATC PROG New App Annual Fee', 'ATC Invoice', 'ATC Program Fee for new ATC Applications', null, 'USD', '2008-02-01', '2009-01-30');
INSERT INTO `InvoiceTypes` VALUES ('31', '2008-01-15 06:45:46', 'jiangp', '2012-03-21 01:21:36', 'chongl', 'A', 'AAPAC ATC Program', 'ATC Invoice', 'ATC Annual Program Fee', null, 'AUD', '2012-02-01', '2013-01-31');
INSERT INTO `InvoiceTypes` VALUES ('32', '2008-01-15 06:46:43', 'jiangp', '2012-03-14 21:29:39', 'chongl', 'A', 'AAPAC ATC Certification', 'ATC Invoice', 'ATC Certification Fee', null, 'USD', '2012-02-01', '2013-01-31');
INSERT INTO `InvoiceTypes` VALUES ('33', '2008-01-15 06:47:43', 'jiangp', '2012-03-14 21:29:17', 'chongl', 'A', 'AAPac AOTG DSL', 'ATC Invoice', 'AOTG DSL fee', null, 'USD', '2012-02-01', '2013-01-31');
INSERT INTO `InvoiceTypes` VALUES ('34', '2008-01-15 06:48:32', 'jiangp', '2012-03-14 21:28:55', 'chongl', 'A', 'AAPac AOTG Courseware', 'ATC Invoice', 'AOTG hard copy Courseware fee', null, 'USD', '2012-02-01', '2013-01-31');
INSERT INTO `InvoiceTypes` VALUES ('35', '2008-01-18 11:32:38', 'morganj', '2008-03-05 04:52:30', 'morganj', 'A', 'EMIA 2008 (FY09) CREDIT NOTE NEW APPLICATION', 'ATC Invoice', null, null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('36', '2008-01-18 11:33:14', 'morganj', '2008-03-05 04:52:42', 'morganj', 'A', 'EMIA 2008 (FY09) CREDIT NOTE SLEI', 'ATC Invoice', null, null, 'EUR', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('37', '2008-04-16 10:21:53', 'morganj', '2008-04-16 10:32:22', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - SAARC/INDIA', 'ATC Invoice', null, null, 'USD', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('38', '2008-04-16 10:22:49', 'morganj', '2008-04-16 10:46:53', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - SAARC/INDIA', 'ATC Invoice', null, null, 'USD', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('39', '2008-04-16 10:23:43', 'morganj', '2008-04-16 10:46:35', 'morganj', 'A', 'EMIA 2008 (FY09) ATC PROGRAM AOTC DSL - SAARC/INDIA', 'ATC Invoice', null, null, 'USD', '2008-02-01', '2009-01-31');
INSERT INTO `InvoiceTypes` VALUES ('40', '2008-09-26 18:00:07', 'herculg', '2008-09-26 18:28:52', 'herculg', 'A', 'AMER: 2009 (FY10) ATC Program Fee LA (12) -  Model 2', 'ATC Invoice', 'LA ATC Fee Model 2 (1 Essentials DSL Title)', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('41', '2008-09-29 12:17:40', 'herculg', '2009-01-06 17:15:38', 'herculg', 'A', 'AMER: 2009 (FY10) ATC Program Fee', 'ATC Invoice', 'ATC Program Fee for new ATC Applications in the Program year 2009.', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('42', '2008-09-29 12:18:48', 'herculg', '2008-09-29 12:19:31', 'herculg', 'A', 'AMER: 2009 (FY10) Renewal', 'ATC Invoice', 'ATC Program Renewal Fee in the Program year 2009.', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('43', '2008-10-02 16:04:32', 'herculg', '2008-10-22 18:17:54', 'herculg', 'X', 'AMER: 2009 (FY10) ATC Program Fee LA (12) SAT-  Model 2', 'ATC Invoice', 'LA ATC Fee Model 2 (1 Essentials DSL Title)', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('44', '2008-10-02 17:09:21', 'herculg', '2008-10-02 17:09:57', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 1 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 1 Industry', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('45', '2008-10-02 17:10:56', 'herculg', '2008-10-02 17:11:42', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 1 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 1 Industry / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('46', '2008-10-02 17:12:26', 'herculg', '2008-10-02 17:12:43', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 1 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 1 Industry', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('47', '2008-10-02 17:13:22', 'herculg', '2008-10-02 17:13:31', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 1 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 1 Industry / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('48', '2008-10-02 17:15:24', 'herculg', '2008-10-02 17:15:39', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 2 Industry', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('49', '2008-10-02 17:16:02', 'herculg', '2008-10-02 17:16:23', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 2 Industry / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('50', '2008-10-02 17:17:26', 'herculg', '2008-10-02 17:17:38', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 2 Industry', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('51', '2008-10-02 17:18:03', 'herculg', '2008-10-02 17:18:15', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 2 Industry / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('52', '2008-10-02 17:18:55', 'herculg', '2008-10-02 17:19:07', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 3 Industry', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('53', '2008-10-02 17:19:30', 'herculg', '2008-10-02 17:19:42', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 3 Industry / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('54', '2008-10-02 17:20:07', 'herculg', '2008-10-02 17:20:21', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 3 Industry', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('55', '2008-10-02 17:20:49', 'herculg', '2008-10-02 17:21:04', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'ATC Invoice', 'LA ATC Fee Model 1 PSEB + 3 Industry / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('56', '2008-10-22 17:32:34', 'herculg', '2008-10-22 18:18:00', 'herculg', 'X', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2', 'ATC Invoice', 'LA ATC Fee Model 2 PSEB + 1 Essentials Title', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('57', '2008-10-22 17:44:45', 'herculg', '2008-10-22 18:18:16', 'herculg', 'X', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2', 'ATC Invoice', 'LA ATC Fee Model 2 PSEB + 1 Industry / SAT', null, 'USD', '2009-02-02', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('58', '2008-10-22 17:46:31', 'herculg', '2008-10-22 18:17:44', 'herculg', 'X', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2', 'ATC Invoice', 'LA ATC Fee Model 2 PSEB + 1 Essentials', null, 'USD', '2009-02-01', '2010-10-31');
INSERT INTO `InvoiceTypes` VALUES ('59', '2008-10-22 17:56:23', 'herculg', '2008-10-22 18:18:25', 'herculg', 'X', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2', 'ATC Invoice', 'LA ATC Fee Model 2 PSEB + 1 Essentials Title / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('60', '2008-10-22 17:58:17', 'herculg', '2008-10-22 18:23:23', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 2', 'ATC Invoice', 'LA ATC Fee Model 2 + 2 Essentials Title', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('61', '2008-10-22 18:06:05', 'herculg', '2008-10-22 18:23:49', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 2', 'ATC Invoice', 'LA ATC Fee Model 2 + 2 Essentials Title / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('62', '2008-10-22 18:07:04', 'herculg', '2008-10-22 18:23:35', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'ATC Invoice', 'LA ATC Fee Model 2  + 2 Essentials Title', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('63', '2008-10-22 18:07:50', 'herculg', '2008-10-22 19:01:14', 'herculg', 'X', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'ATC Invoice', 'LA ATC Fee Model 2 + 2 Essentials Title', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('64', '2008-10-22 18:21:08', 'herculg', '2008-10-22 18:23:09', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 1', 'ATC Invoice', 'LA ATC Fee Model 2 + 1 Essentials Title', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('65', '2008-10-22 18:21:33', 'herculg', '2008-10-22 18:24:33', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 1', 'ATC Invoice', 'LA ATC Fee Model 2 + 1 Essentials Title', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('66', '2008-10-22 18:26:02', 'herculg', '2008-10-22 18:27:05', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 1', 'ATC Invoice', 'LA ATC Fee Model 2 + 1 Essentials Title / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('67', '2008-10-22 18:26:40', 'herculg', '2008-10-22 18:27:36', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 1', 'ATC Invoice', 'LA ATC Fee Model 2 + 1 Essentials Title / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('68', '2008-10-22 18:55:28', 'herculg', '2008-10-22 18:56:41', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 3', 'ATC Invoice', 'LA ATC Fee Model 2 + 3 Essentials Title', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('69', '2008-10-22 18:57:21', 'herculg', '2008-10-22 18:57:41', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 3', 'ATC Invoice', 'LA ATC Fee Model 2 + 3 Essentials Title / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('70', '2008-10-22 18:58:16', 'herculg', '2008-10-22 18:58:48', 'herculg', 'A', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 3', 'ATC Invoice', 'LA ATC Fee Model 2 + 3 Essentials Title', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('71', '2008-10-22 18:59:15', 'herculg', '2008-10-22 19:07:37', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 2', 'ATC Invoice', 'LA ATC Fee Model 2 + 2 Essentials Title / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('72', '2008-10-22 19:08:45', 'herculg', '2008-10-22 19:09:04', 'herculg', 'A', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 3', 'ATC Invoice', 'LA ATC Fee Model 2 + 3 Essentials Title / SAT', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('73', '2008-11-05 06:28:20', 'Marcov', '2008-11-13 10:54:44', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('74', '2008-11-05 06:29:19', 'Marcov', '2008-11-13 10:50:41', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('75', '2008-11-05 06:30:13', 'Marcov', '2008-11-13 10:55:32', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('76', '2008-11-05 06:30:54', 'Marcov', '2008-11-13 10:53:56', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('77', '2008-11-05 06:31:29', 'Marcov', '2008-11-13 10:48:24', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - EC3 CIS - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('78', '2008-11-05 06:32:07', 'Marcov', '2008-12-15 09:10:34', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - SAPAC - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', 'ATC Invoice', null, null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('79', '2008-11-05 06:32:43', 'Marcov', '2008-11-17 05:24:32', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('80', '2008-11-05 06:33:13', 'Marcov', '2008-11-17 05:23:49', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('81', '2008-11-05 06:33:39', 'Marcov', '2008-11-17 05:24:11', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('82', '2008-11-05 06:34:09', 'Marcov', '2008-11-17 05:24:52', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('83', '2008-11-05 06:34:34', 'Marcov', '2008-11-17 05:24:43', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - EC3 CIS - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('84', '2008-11-05 06:34:58', 'Marcov', '2008-12-15 09:10:21', 'morganj', 'A', 'EMIA: 2009 (FY10) ATC PROGRAM - SAPAC - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('85', '2009-09-29 11:17:48', 'garyh', '2009-09-29 11:17:48', 'garyh', 'A', 'AMER: 2010 (FY11):  A&P Program Fee', 'ATC Invoice', 'Standard 1 user', null, 'USD', '2010-02-01', '2010-12-31');
INSERT INTO `InvoiceTypes` VALUES ('86', '2009-09-30 09:51:55', 't_barrw', '2009-09-30 09:51:55', 't_barrw', 'A', 'VIRTUAL: A&P Invoices from ADN', 'A&P Invoice', null, null, 'USD', null, null);
INSERT INTO `InvoiceTypes` VALUES ('87', '2009-10-02 13:41:56', 'garyh', '2009-10-02 13:41:56', 'garyh', 'A', 'ATC Discount', 'ATC Invoice', 'Prorated Discount', null, 'USD', '2009-02-01', '2010-01-31');
INSERT INTO `InvoiceTypes` VALUES ('88', '2009-10-24 08:45:23', 't_barrw', '2009-10-24 08:47:03', 't_barrw', 'A', 'VAR Invoice Type Sample', 'VAR Invoice', 'Modify as needed', null, 'EUR', null, null);
INSERT INTO `InvoiceTypes` VALUES ('89', '2009-11-11 15:56:18', 'garyh', '2009-11-11 15:56:18', 'garyh', 'A', 'AMER: 2010 (FY11) ATC Program Fee', 'ATC Invoice', 'ATC Program Fee for new ATC Applications in the Program year 2010.', null, 'USD', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('90', '2009-11-11 15:57:15', 'garyh', '2009-11-11 15:58:06', 'garyh', 'A', 'AMER: 2010 (FY11) Renewal', 'ATC Invoice', 'ATC Program Renewal Fee in the Program year 2010.', null, 'USD', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('91', '2009-12-08 10:46:28', 'janm', '2009-12-08 10:57:31', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS1 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('92', '2009-12-08 10:47:40', 'janm', '2009-12-08 10:58:09', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS2 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('93', '2009-12-08 10:48:19', 'janm', '2009-12-08 10:59:08', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTON FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('94', '2009-12-08 10:49:00', 'janm', '2009-12-08 10:58:58', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS4 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('95', '2009-12-08 10:50:19', 'janm', '2009-12-08 10:57:19', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS1 - NEW MAIN SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('96', '2009-12-08 10:51:07', 'janm', '2009-12-08 10:57:57', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS2 - NEW MAIN SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('97', '2009-12-08 10:53:00', 'janm', '2009-12-08 10:58:45', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - NEW MAIN SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('98', '2009-12-08 10:53:24', 'janm', '2009-12-08 10:59:23', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS4 - NEW MAIN - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('99', '2009-12-08 10:54:20', 'janm', '2009-12-08 11:00:17', 'janm', 'A', 'EMEA ATC PROGRAM - ANNUAL SOFTWARE & SUBSCRIPTION FEE - NEW SITE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('100', '2009-12-08 10:54:46', 'janm', '2009-12-08 10:57:45', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS2 - NEW ADD SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('101', '2009-12-08 10:55:10', 'janm', '2009-12-08 10:58:20', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - NEW ADD SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('102', '2009-12-08 10:55:32', 'janm', '2009-12-08 10:58:33', 'janm', 'X', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - NEW ADD SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('103', '2009-12-08 11:00:33', 'janm', '2009-12-08 11:00:52', 'janm', 'A', 'EMEA ATC PROGRAM - ANNUAL SOFTWARE & SUBSCRIPTION FEE - RENEWAL', 'ATC Invoice', null, null, 'EUR', '2010-02-01', '2011-01-31');
INSERT INTO `InvoiceTypes` VALUES ('104', '2016-07-21 09:39:07', 'robneudecker@engageglobalsolutions.com', '2016-07-28 23:28:23', 'selina.shen@autodesk.com', 'A', 'APAC: FY17 ATC Program Fee', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership', null, 'USD', '2016-02-01', '2017-01-31');
INSERT INTO `InvoiceTypes` VALUES ('105', '2016-07-25 04:29:00', 'selina.shen@autodesk.com', '2016-07-25 04:29:00', 'selina.shen@autodesk.com', 'A', 'AMER: FY17 ATC Program Fee', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership', null, 'USD', null, null);
INSERT INTO `InvoiceTypes` VALUES ('106', '2016-07-25 04:31:30', 'selina.shen@autodesk.com', '2016-07-25 04:31:30', 'selina.shen@autodesk.com', 'A', 'EMEA: FY17 ATC Program Fee', 'ATC Invoice', 'Fees for ATC Software Subscription & ATC Program Membership', null, 'USD', null, null);

-- ----------------------------
-- Table structure for JournalActivities
-- ----------------------------
DROP TABLE IF EXISTS `JournalActivities`;
CREATE TABLE `JournalActivities` (
  `ActivityId` int(8) NOT NULL AUTO_INCREMENT,
  `DateAdded` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `AddedBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateLastAdmin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LastAdminBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ActivityName` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `ActivityType` varchar(40) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `IsUnique` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ActivityId`),
  KEY `IDX_JOURNALACTIVITIES_Status` (`Status`),
  KEY `IDX_JOURNALACTIVITIES_ActivityType` (`ActivityType`)
) ENGINE=MyISAM AUTO_INCREMENT=532 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of JournalActivities
-- ----------------------------
INSERT INTO `JournalActivities` VALUES ('1', '2006-08-22 15:00:34', 'ATCDB000', '2006-10-10 06:42:40', 'ATCDB000', 'X', 'AOTC Issue', 'Site Journal Entry', null, 'Track AOTC issues for this site');
INSERT INTO `JournalActivities` VALUES ('2', '2006-08-22 15:27:50', 'ATCDB000', '2006-08-22 15:33:59', 'ATCDB000', 'A', 'AIA Member', 'Site Attribute', 'Y', '(Yes/No): whether or not a site is an AIA Memeber');
INSERT INTO `JournalActivities` VALUES ('3', '2006-08-22 15:28:37', 'ATCDB000', '2006-08-22 15:34:07', 'ATCDB000', 'A', 'AUGI Member', 'Site Attribute', 'Y', '(Yes/No): whether or not a SITE is an AUGI member');
INSERT INTO `JournalActivities` VALUES ('4', '2006-08-22 15:30:28', 'ATCDB000', '2012-11-12 09:17:16', 'buckberr', 'A', 'ATC Locator', 'Site Attribute', 'Y', '(ON/OFF): ATCs who want to be published on the ATC web locator');
INSERT INTO `JournalActivities` VALUES ('5', '2006-08-22 15:31:15', 'ATCDB000', '2006-08-22 15:31:15', 'ATCDB000', 'A', 'Sales Leads', 'Site Attribute', 'Y', '(Yes/No): Criteria for the ATC Evaluations link to Aprimo');
INSERT INTO `JournalActivities` VALUES ('6', '2006-08-22 15:35:02', 'ATCDB000', '2007-08-30 09:31:56', '41118', 'A', 'Historical information', 'Site Journal Entry', null, 'For general use and includes information that may have been migrated from Sharepoint or other systems.');
INSERT INTO `JournalActivities` VALUES ('7', '2006-08-22 15:35:52', 'ATCDB000', '2006-10-10 13:36:08', 'ATCDB000', 'A', 'Historical information', 'Contact Journal Entry', null, 'For general use.');
INSERT INTO `JournalActivities` VALUES ('8', '2006-08-22 15:38:48', 'ATCDB000', '2012-11-06 03:40:59', 'buckberr', 'X', '2007 Conference invitation sent', 'Site Event', '', 'Setting indicates that the invitation has been sent');
INSERT INTO `JournalActivities` VALUES ('9', '2006-08-22 15:39:20', 'ATCDB000', '2012-11-06 03:41:06', 'buckberr', 'X', '2007 Conference RSVP received', 'Site Event', '', 'Setting indicates that the site has responded to the invitation');
INSERT INTO `JournalActivities` VALUES ('10', '2006-08-23 09:40:10', 'ATCDB000', '2007-08-30 09:35:22', '41118', 'A', 'General Site Note', 'Site Journal Entry', null, 'General communication took place between partner and Autodesk');
INSERT INTO `JournalActivities` VALUES ('11', '2006-08-23 09:41:46', 'ATCDB000', '2012-11-06 03:41:14', 'buckberr', 'X', '2007 Ready', 'Site Event', '', 'Company is able to train on AutoCAD 2007');
INSERT INTO `JournalActivities` VALUES ('12', '2006-08-23 10:32:58', 'ATCDB000', '2012-06-01 05:23:26', 'buckberr', 'X', '2007 Ready', 'Site Attribute', 'Y', 'after chat with Jan...');
INSERT INTO `JournalActivities` VALUES ('13', '2006-08-23 10:33:40', 'ATCDB000', '2006-08-23 10:34:00', 'ATCDB000', 'X', '2008 Ready', 'Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('14', '2006-08-23 10:45:15', 'ATCDB000', '2013-01-14 11:24:46', 'neudeckerr', 'X', 'FY08 Termination Notice Sent', 'Site Journal Entry', '', 'Can be used to track when a partner was issued with a termination notice.  Usually an ATC is given 30 days notice to comply from the date this notice went out.');
INSERT INTO `JournalActivities` VALUES ('15', '2006-08-23 10:47:39', 'ATCDB000', '2007-08-30 09:35:04', '41118', 'A', 'Terminated', 'Site Journal Entry', null, 'Termination effective  Immediately.');
INSERT INTO `JournalActivities` VALUES ('16', '2006-08-23 10:48:12', 'ATCDB000', '2013-01-14 11:24:22', 'neudeckerr', 'X', 'FY08 Reactivated', 'Site Journal Entry', '', 'An ATC Site thats been reactivited during the FY08 ATC program year (calendar year 2007)');
INSERT INTO `JournalActivities` VALUES ('17', '2006-08-23 11:17:31', 'ATCDB000', '2007-05-18 08:25:44', '41118', 'A', 'Evaluation System User', 'Site Attribute', 'Y', 'for keeping track of who\'s using the online eval');
INSERT INTO `JournalActivities` VALUES ('18', '2006-08-31 04:07:37', 'ATCDB000', '2012-11-06 03:40:02', 'buckberr', 'X', 'AAI Certificate Issued', 'Contact Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('19', '2006-09-05 10:10:05', 'ATCDB000', '2006-11-08 10:47:33', 'ATCDB000', 'A', 'Premier Type', 'Site Attribute', 'Y', 'migrated data');
INSERT INTO `JournalActivities` VALUES ('20', '2006-09-08 15:03:56', 'ATCDB000', '2006-09-08 15:03:56', 'ATCDB000', 'A', 'Historical information', 'Organization Journal Entry', null, 'For general use');
INSERT INTO `JournalActivities` VALUES ('21', '2006-09-25 11:25:43', 'ATCDB000', '2006-10-10 06:29:18', 'ATCDB000', 'X', 'Ready for Testing', 'Organization Attribute', 'Y', 'For testing purposes only');
INSERT INTO `JournalActivities` VALUES ('25', '2006-09-26 09:06:33', 'ATCDB000', '2006-09-26 09:06:33', 'ATCDB000', 'X', 'ADN Commercial', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('26', '2006-09-26 09:07:13', 'ATCDB000', '2006-09-26 10:39:11', 'ATCDB000', 'A', 'ADN Standard', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('24', '2006-09-26 09:06:00', 'ATCDB000', '2006-09-26 09:06:00', 'ATCDB000', 'A', 'ADN Author Publisher', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('27', '2006-09-26 09:07:40', 'ATCDB000', '2006-09-26 09:07:40', 'ATCDB000', 'A', 'ADN Professional', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('28', '2006-09-26 10:40:31', 'ATCDB000', '2006-10-05 02:55:17', 'ATCDB000', 'X', 'ADN Premier Partner', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('29', '2006-09-28 00:03:24', 'ATCDB000', '2013-01-14 11:20:24', 'neudeckerr', 'X', 'MED_V: Design Vizualization - Autodesk 3ds Max', 'Site Accreditation', 'Y', 'includes Autodesk 3ds Max, Autodesk 3ds Max Design, Autodesk Combustion, Autodesk Cleaner XL, Autodesk VIZ and Autodesk Mudbox');
INSERT INTO `JournalActivities` VALUES ('30', '2006-09-28 00:06:00', 'ATCDB000', '2013-01-14 11:19:33', 'neudeckerr', 'X', 'MED_E: 3D Animation - Autodesk Maya', 'Site Accreditation', 'Y', 'includes Autodesk Maya, Autodesk Maya Complete, Autodesk Mudbox and Autodesk MotionBuilder');
INSERT INTO `JournalActivities` VALUES ('52', '2006-10-16 11:42:20', 'ATCDB000', '2013-01-14 11:15:09', 'neudeckerr', 'X', 'AEC_C: Civil', 'Site Accreditation', 'Y', 'Products Included:  AutoCAD Land Desktop, AutoCAD Survey, Autodesk Civil Design, AutoCAD Civil 3D');
INSERT INTO `JournalActivities` VALUES ('31', '2006-10-02 08:04:41', 'ATCDB000', '2013-01-14 11:14:54', 'neudeckerr', 'X', 'AEC_A: Architecture - Revit Architecture', 'Site Accreditation', 'Y', 'Products Included:  Revit Architecture');
INSERT INTO `JournalActivities` VALUES ('32', '2006-10-02 08:05:08', 'ATCDB000', '2013-01-14 11:16:31', 'neudeckerr', 'X', 'AEC_S: Structural Engineering - Revit Structure', 'Site Accreditation', 'Y', 'Products Included:  Revit Structure');
INSERT INTO `JournalActivities` VALUES ('33', '2006-10-02 08:06:21', 'ATCDB000', '2013-01-14 11:14:36', 'neudeckerr', 'X', 'AEC_A: Architecture - AutoCAD Architecture', 'Site Accreditation', 'Y', 'Products Included:  AutoCAD Architecture, Architectural Desktop');
INSERT INTO `JournalActivities` VALUES ('34', '2006-10-02 08:06:43', 'ATCDB000', '2006-11-15 10:40:27', '41118', 'X', 'MSD', 'Site Accreditation', 'Y', 'generic MSD label just in case!');
INSERT INTO `JournalActivities` VALUES ('35', '2006-10-04 16:10:08', 'ATCDB000', '2013-01-14 11:21:22', 'neudeckerr', 'X', 'MFG_M: Manufacturing - Autodesk Inventor', 'Site Accreditation', 'Y', 'Products included:  Autodesk Inventor, Autodesk Inventor Professional, AutoCAD Mechanical, & Autodesk Streamline');
INSERT INTO `JournalActivities` VALUES ('36', '2006-10-04 16:30:09', 'ATCDB000', '2013-01-14 11:21:45', 'neudeckerr', 'X', 'MFG_M: Manufacturing Electrical - AutoCAD Electrical', 'Site Accreditation', 'Y', 'Products included:  AutoCAD Electrical');
INSERT INTO `JournalActivities` VALUES ('37', '2006-10-05 02:51:53', 'ATCDB000', '2007-05-30 08:35:38', '41118', 'X', 'M&E_V: Vizualization - Maya', 'Site Accreditation', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('38', '2006-10-05 02:52:19', 'ATCDB000', '2013-01-14 11:20:31', 'neudeckerr', 'X', 'MED_V: Design Vizualization - Autodesk VIZ', 'Site Accreditation', 'Y', 'Products Included:  Autodesk VIZ (to be replaced by Autodesk 3ds Max Design during FY09)');
INSERT INTO `JournalActivities` VALUES ('39', '2006-10-05 02:53:19', 'ATCDB000', '2013-01-14 11:19:53', 'neudeckerr', 'X', 'MED_S: Entertainment Systems', 'Site Accreditation', 'Y', 'includes Autodesk Lustre, Autodesk Fire, Autodesk Smoke, Autodesk Inferno, Autodesk Flame, Autodesk Flint, Autodesk Toxik');
INSERT INTO `JournalActivities` VALUES ('40', '2006-10-05 02:58:40', 'ATCDB000', '2013-01-14 11:15:35', 'neudeckerr', 'X', 'AEC_E: MEP Engineering - AutoCAD MEP', 'Site Accreditation', 'Y', 'Products Included:  AutoCAD MEP');
INSERT INTO `JournalActivities` VALUES ('41', '2006-10-05 05:41:47', 'ATCDB000', '2013-01-14 11:15:48', 'neudeckerr', 'X', 'AEC_E: MEP Engineering - Revit MEP', 'Site Accreditation', 'Y', 'Products Included:  Revit MEP');
INSERT INTO `JournalActivities` VALUES ('42', '2006-10-12 04:08:25', 'ATCDB000', '2012-11-06 03:39:39', 'buckberr', 'X', 'AAI AutoCAD 2007', 'Contact Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('43', '2006-10-13 08:36:56', 'ATCDB000', '2016-09-28 10:49:59', 'robneudecker@engageglobalsolutions.com', 'X', '1) Agreement sent to ATC for signature', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('44', '2006-10-13 08:38:12', 'ATCDB000', '2016-09-28 10:49:48', 'robneudecker@engageglobalsolutions.com', 'X', '2) Agreements received from ATC by PM', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('45', '2006-10-13 08:39:47', 'ATCDB000', '2007-05-18 08:40:38', '41118', 'A', 'T1)  Termination Notice Sent to ATC', 'Organization Journal Entry', null, 'can be used for all reasons of termination');
INSERT INTO `JournalActivities` VALUES ('46', '2006-10-16 11:30:09', 'ATCDB000', '2013-01-14 11:17:11', 'neudeckerr', 'X', 'HORZ_A:  Horizontal - AutoCAD', 'Site Accreditation', 'Y', 'Products included: AutoCAD, AutoCAD LT, AutoCAD Raster Design & Autodesk VIZ');
INSERT INTO `JournalActivities` VALUES ('47', '2006-10-16 11:31:23', 'ATCDB000', '2006-11-15 10:40:41', '41118', 'X', 'PTD', 'Site Accreditation', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('48', '2006-10-16 11:31:59', 'ATCDB000', '2013-01-14 11:15:21', 'neudeckerr', 'X', 'AEC_C: Collaborative PM - Buzzsaw', 'Site Accreditation', 'Y', 'includes Autodesk Buzzsaw Standard and Autodesk BuzzsawProfessional');
INSERT INTO `JournalActivities` VALUES ('49', '2006-10-16 11:39:47', 'ATCDB000', '2013-01-14 11:20:45', 'neudeckerr', 'X', 'MFG_ID: Industrial Design', 'Site Accreditation', 'Y', 'Products included:  Autodesk AliasStudio, Autodesk DesignStudio, Autodesk ImageStudio, Autodesk PortfolioWall , Autodesk ShowCase & Autodesk SketchBook Pro.');
INSERT INTO `JournalActivities` VALUES ('50', '2006-10-16 11:40:14', 'ATCDB000', '2013-01-14 11:20:38', 'neudeckerr', 'X', 'MFG_DM: Data Management - Autodesk Productstream', 'Site Accreditation', 'Y', 'Products included:  Autodesk ProductStream & Autodesk Productstream Professional (incld. EASY, JOBSERVER, OFFICE & PRO)');
INSERT INTO `JournalActivities` VALUES ('51', '2006-10-16 11:40:41', 'ATCDB000', '2013-01-14 11:17:45', 'neudeckerr', 'X', 'MED_E: 3D Animation - Autodesk 3ds Max', 'Site Accreditation', 'Y', 'includes Autodesk 3ds Max, Autodesk Combustion, Autodesk Cleaner, Autodesk Cleaner XL, Autodesk Mudbox & Autodesk MotionBuilder');
INSERT INTO `JournalActivities` VALUES ('53', '2006-10-16 11:42:40', 'ATCDB000', '2013-01-14 11:15:15', 'neudeckerr', 'X', 'AEC_C: Civil 3D', 'Site Accreditation', 'Y', 'Products Included:  AutoCAD Civil 3D');
INSERT INTO `JournalActivities` VALUES ('54', '2006-11-01 16:18:55', 'ATCDB000', '2006-11-01 16:19:24', 'ATCDB000', 'X', 'Test Instructor Attribute', 'Contact Attribute', 'Y', 'just for testing, will be deleted');
INSERT INTO `JournalActivities` VALUES ('55', '2006-11-02 10:40:12', 'ATCDB000', '2006-11-02 10:40:37', 'ATCDB000', 'A', 'Active for on-line eval', 'Contact Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('56', '2006-11-02 10:57:44', 'ATCDB000', '2007-10-01 09:27:34', 't_barrw', 'A', 'newsletter recipient(s)', 'Site Attribute', '', 'any email addresses passed through from data migration of CONTACT data is placed here.');
INSERT INTO `JournalActivities` VALUES ('57', '2006-11-08 10:46:39', 'ATCDB000', '2006-11-08 10:46:57', 'ATCDB000', 'A', 'Country Type', 'Site Attribute', 'Y', 'migrated data');
INSERT INTO `JournalActivities` VALUES ('58', '2006-11-08 10:49:32', 'ATCDB000', '2007-02-20 09:02:33', '41118', 'A', 'Region', 'Site Attribute', 'Y', 'migrated data');
INSERT INTO `JournalActivities` VALUES ('59', '2006-11-08 10:50:45', 'ATCDB000', '2006-11-08 10:50:45', 'ATCDB000', 'A', 'Tier', 'Site Attribute', 'Y', 'migrated data');
INSERT INTO `JournalActivities` VALUES ('60', '2006-11-14 05:26:18', '41118', '2012-06-01 05:22:44', 'buckberr', 'X', '2008 Ready', 'Site Accreditation', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('61', '2006-11-14 06:06:04', '41118', '2013-09-06 09:29:18', 'buckberr', 'X', 'AAI 2007-2008', 'Contact Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('62', '2006-11-14 06:06:22', '41118', '2013-09-06 09:29:59', 'buckberr', 'X', 'AAI 2008-2009', 'Contact Attribute', 'Y', 'Completed PLP ATC Learning Path & customer satisfaction rating of over 85% FY09 Q1 & Q2.');
INSERT INTO `JournalActivities` VALUES ('63', '2006-11-15 10:09:33', '41118', '2007-05-30 08:51:16', '41118', 'X', 'AEC_B: Building Operation Systems - Desktop FM', 'Site Accreditation', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('64', '2006-11-15 10:37:20', '41118', '2013-01-14 11:16:01', 'neudeckerr', 'X', 'AEC_G: Geospatial - AutoCAD Map 3D', 'Site Accreditation', 'Y', 'Products included:  AutoCAD Map 3D');
INSERT INTO `JournalActivities` VALUES ('65', '2006-11-15 10:38:14', '41118', '2013-01-14 11:16:07', 'neudeckerr', 'X', 'AEC_G: Geospatial - MapGuide', 'Site Accreditation', 'Y', 'Products included:  Autodesk MapGuide, Autodesk MapGuide Enterprize and Autodesk MapGuide Studio');
INSERT INTO `JournalActivities` VALUES ('66', '2006-11-15 22:31:15', 'ATCDB000', '2007-02-20 08:26:17', 'ATCDB000', 'A', 'Addresses', 'Site Migration Anomalies', null, 'Addresses imported from Sharepoint');
INSERT INTO `JournalActivities` VALUES ('67', '2006-11-15 22:40:20', 'ATCDB000', '2007-04-03 09:09:07', 'ATCDB000', 'X', 'Multiple Address', 'Site Migration Anomalies', null, 'More than one Address was found');
INSERT INTO `JournalActivities` VALUES ('68', '2006-11-15 23:10:24', 'ATCDB000', '2007-04-03 09:09:55', 'ATCDB000', 'X', 'Multiple Status', 'Organization Migration Anamolies', null, 'More than one Status was found');
INSERT INTO `JournalActivities` VALUES ('69', '2006-11-15 23:11:26', 'ATCDB000', '2007-04-03 09:10:00', 'ATCDB000', 'X', 'Multiple TaxExempt', 'Organization Migration Anamolies', null, 'More than one Tax Exempt value was found');
INSERT INTO `JournalActivities` VALUES ('70', '2006-11-15 23:12:04', 'ATCDB000', '2007-04-03 09:10:05', 'ATCDB000', 'X', 'Multiple VATNumber', 'Organization Migration Anamolies', null, 'More than one VATNumber was found');
INSERT INTO `JournalActivities` VALUES ('71', '2006-11-15 23:12:35', 'ATCDB000', '2007-04-03 09:10:10', 'ATCDB000', 'X', 'Multiple YearJoined', 'Organization Migration Anamolies', null, 'More than one Year Joined was found');
INSERT INTO `JournalActivities` VALUES ('72', '2006-11-17 00:49:28', 'ATCDB000', '2007-02-19 11:28:09', 'ATCDB000', 'A', 'Phones', 'Site Migration Anomalies', null, 'List of all the telephone numbers associated with this Org in Sharepoint');
INSERT INTO `JournalActivities` VALUES ('73', '2007-02-05 14:44:25', 'ATCDB000', '2007-02-12 16:33:11', 'ATCDB000', 'A', 'Multiple Attributes', 'Site Migration Anomalies', null, 'More than one site has the same siteId/magellanID/SAP, or the siteId/magellanID/SAP is defined twice for the same site.');
INSERT INTO `JournalActivities` VALUES ('74', '2007-02-05 15:00:30', 'ATCDB000', '2007-04-03 09:09:49', 'ATCDB000', 'X', 'Multiple Company Types', 'Organization Migration Anamolies', null, 'Multiple company type attributes found for a single organization');
INSERT INTO `JournalActivities` VALUES ('75', '2007-02-13 08:36:21', 'ATCDB000', '2007-04-03 09:10:51', 'ATCDB000', 'X', 'Multiple Attribute', 'Contact Migration Anomalies', null, null);
INSERT INTO `JournalActivities` VALUES ('76', '2007-02-19 11:06:14', 'ATCDB000', '2008-05-28 04:19:09', '41118', 'A', 'Certification ATC', 'Site Attribute', 'Y', 'Used to identify ATC Partners that have been approved as a Certification site');
INSERT INTO `JournalActivities` VALUES ('77', '2007-02-19 11:20:10', 'ATCDB000', '2007-02-19 11:20:10', 'ATCDB000', 'A', 'Retired', 'Site Migration Anomalies', null, 'Items from Sharepoint. For historical purposes.');
INSERT INTO `JournalActivities` VALUES ('78', '2007-02-19 11:26:55', 'ATCDB000', '2007-02-19 11:26:55', 'ATCDB000', 'A', 'EmailAddresses', 'Site Migration Anomalies', null, 'List of all the email addresses associated with this Org in Sharepoint');
INSERT INTO `JournalActivities` VALUES ('79', '2007-02-19 14:28:14', 'ATCDB000', '2007-02-19 14:28:14', 'ATCDB000', 'A', 'Excess Phone Number', 'Contact Migration Anomalies', null, null);
INSERT INTO `JournalActivities` VALUES ('80', '2007-02-20 09:03:31', '41118', '2011-01-17 06:49:31', 'buckberr', 'A', 'Satellite Active', 'Site Attribute', 'Y', 'migrated data from US');
INSERT INTO `JournalActivities` VALUES ('81', '2007-02-20 11:08:05', 'ATCDB000', '2007-02-20 11:08:05', 'ATCDB000', 'A', 'Contact', 'Site Migration Anomalies', null, 'An contact who was listed as something Other than an Instructor.');
INSERT INTO `JournalActivities` VALUES ('82', '2007-02-28 10:21:40', 'ATCDB000', '2007-02-28 10:37:04', 'ATCDB000', 'A', 'Invoice', 'Site Migration Anomalies', null, 'Save all old contract data, just in case, since Invoices are normally saved against the ORG');
INSERT INTO `JournalActivities` VALUES ('83', '2007-03-22 09:27:33', 'tremblay', '2007-03-22 09:27:33', 'tremblay', 'A', 'Registrar Email', 'Site Attribute', 'Y', 'For use in the ATC Locator export?');
INSERT INTO `JournalActivities` VALUES ('84', '2007-03-26 13:48:15', 'ATCDB000', '2007-03-26 13:48:34', 'ATCDB000', 'X', 'Test', 'Contact Attribute', 'Y', 'THIS IS A TEST');
INSERT INTO `JournalActivities` VALUES ('85', '2007-04-19 09:39:08', '41118', '2007-05-18 08:23:30', '41118', 'X', '2008 Ready', 'Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('90', '2007-05-18 03:57:36', '41118', '2007-08-30 09:30:47', '41118', 'A', 'Event_TBD', 'Site Journal Entry', null, 'just a placeholder at the moment to be discussed with Yoshimi and/or anyone else who would like to track Event registration, for example, of ATC\'s)');
INSERT INTO `JournalActivities` VALUES ('86', '2007-05-03 03:59:13', '41118', '2016-09-28 10:49:40', 'robneudecker@engageglobalsolutions.com', 'X', '3) ATC OPS received Signed agreements from PM or ATC', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('87', '2007-05-03 04:01:43', '41118', '2016-09-28 10:49:33', 'robneudecker@engageglobalsolutions.com', 'X', '4) Endrsd agrmnt  sent to PM from ATC Ops', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('88', '2007-05-03 04:06:27', '41118', '2016-09-28 10:49:24', 'robneudecker@engageglobalsolutions.com', 'X', '5) Endrsd agrmnt sent back to ATC from PM/ATC Ops', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('89', '2007-05-03 04:14:08', '41118', '2007-05-18 08:41:12', '41118', 'A', 'T2)  Termination Letter sent to ATC', 'Organization Journal Entry', null, null);
INSERT INTO `JournalActivities` VALUES ('91', '2007-05-18 08:26:31', '41118', '2007-05-18 08:26:31', '41118', 'A', 'VAD - Value Added Distributor', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('92', '2007-05-18 08:26:57', '41118', '2010-02-12 10:34:22', '41118', 'A', 'DVAR - Direct Value Added Reseller', 'Organization Attribute', 'Y', 'Direct Value Add Reseller');
INSERT INTO `JournalActivities` VALUES ('93', '2007-05-18 08:27:29', '41118', '2007-05-18 08:27:29', '41118', 'A', 'PSP - Premier Solution Partner', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('94', '2007-05-18 08:36:05', '41118', '2007-05-18 08:41:34', '41118', 'A', 'T3)  Acknowledgement of Termination Letter received from ATC', 'Organization Journal Entry', null, null);
INSERT INTO `JournalActivities` VALUES ('95', '2007-05-18 08:45:24', '41118', '2007-05-18 08:47:11', '41118', 'A', 'General Organization Note', 'Organization Journal Entry', null, null);
INSERT INTO `JournalActivities` VALUES ('96', '2007-05-18 08:46:39', '41118', '2013-09-06 09:24:44', 'buckberr', 'A', 'General Instructor Note', 'Contact Journal Entry', '', 'General Notes about ATC instructors');
INSERT INTO `JournalActivities` VALUES ('97', '2007-06-20 09:51:15', '41118', '2007-06-21 10:10:59', '41118', 'A', 'Chosen_for_eval_audit', 'Site Attribute', 'Y', 'Used to flag partners that are scheduled to have their customers (who have completed an evaluation form) called by Program Managers.');
INSERT INTO `JournalActivities` VALUES ('98', '2007-06-21 07:20:29', '41118', '2013-01-14 11:24:39', 'neudeckerr', 'X', 'FY08 Terminated', 'Site Journal Entry', '', 'ATC has been terminated from the ATC Program for the year FY08 (or calendar year 2007)');
INSERT INTO `JournalActivities` VALUES ('99', '2007-06-21 07:20:51', '41118', '2013-01-14 11:24:15', 'neudeckerr', 'X', 'FY08 Activated', 'Site Journal Entry', '', 'New ATC has joined for the FY08 (calendar year 2007) ATC Program year');
INSERT INTO `JournalActivities` VALUES ('100', '2007-06-22 05:38:23', '41118', '2013-01-14 11:24:07', 'neudeckerr', 'X', 'FY07 Terminated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('101', '2007-06-22 05:38:40', '41118', '2013-01-14 11:24:00', 'neudeckerr', 'X', 'FY07 Activated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('102', '2007-08-30 09:13:52', '41118', '2013-01-14 11:24:31', 'neudeckerr', 'X', 'FY08 Renewed', 'Site Journal Entry', '', 'Existing ATC has renewed their FY08 ATC Membership');
INSERT INTO `JournalActivities` VALUES ('103', '2007-08-30 09:14:51', '41118', '2013-01-14 11:25:07', 'neudeckerr', 'X', 'FY09 Renewed', 'Site Journal Entry', '', 'Existing ATC has renewed their ATC Membership for FY09');
INSERT INTO `JournalActivities` VALUES ('104', '2007-08-30 09:16:06', '41118', '2013-01-14 11:25:00', 'neudeckerr', 'X', 'FY09 Activated', 'Site Journal Entry', '', 'New ATC has joined for the FY09 (calendar year 2008) ATC Program year');
INSERT INTO `JournalActivities` VALUES ('105', '2007-08-30 09:17:26', '41118', '2013-01-14 11:25:14', 'neudeckerr', 'X', 'FY09 Terminated', 'Site Journal Entry', '', 'ATC has been terminated from the ATC Program for the year FY09 ( or calendar year 2008)');
INSERT INTO `JournalActivities` VALUES ('106', '2007-09-20 06:11:21', '41118', '2013-01-14 11:25:22', 'neudeckerr', 'X', 'FY09 Termination Notice Sent', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('107', '2007-09-20 06:16:40', '41118', '2013-01-14 11:24:53', 'neudeckerr', 'X', 'FY08 Termination Notice Withdrawn', 'Site Journal Entry', '', 'Can be used to track that a partner has reacted positively to a termination notice being sent.  It also allows Program Team members to track the details around what happened to enable this.');
INSERT INTO `JournalActivities` VALUES ('108', '2007-09-20 06:17:18', '41118', '2013-01-14 11:25:30', 'neudeckerr', 'X', 'FY09 Termination Notice Withdrawn', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('109', '2007-09-27 08:24:48', '41118', '2007-09-27 08:25:38', '41118', 'A', 'Completion Certificates Issued', 'Site Journal Entry', null, 'To keep track of when and how frequently, ATC\'s request Certificates.  Also, how many they order.');
INSERT INTO `JournalActivities` VALUES ('110', '2007-10-03 05:51:18', '41118', '2007-10-03 06:34:17', '41118', 'A', 'ADI - Americas', 'Organization Attribute', 'Y', 'Requested by Gary in the Americas team.');
INSERT INTO `JournalActivities` VALUES ('111', '2007-10-03 06:37:22', '41118', '2007-10-03 06:37:22', '41118', 'A', 'AIA-CES Passport Provider', 'Organization Attribute', 'Y', 'Request from Gary - Americas');
INSERT INTO `JournalActivities` VALUES ('112', '2007-10-03 06:39:05', '41118', '2007-10-03 06:39:05', '41118', 'A', 'AIA-CES Passport Provider', 'Site Journal Entry', '', 'As requested by Gary - Americas (3rd Oct, 2007)');
INSERT INTO `JournalActivities` VALUES ('113', '2007-10-22 06:16:06', 'morganj', '2008-03-05 04:53:25', 'morganj', 'A', 'EMIA: MEMBER OF EU', 'Organization Attribute', '', 'This organisation is located in a European Union country.  All linked sites will also be assigned as being in an EU country.');
INSERT INTO `JournalActivities` VALUES ('114', '2007-10-22 06:16:54', 'morganj', '2008-03-05 04:53:08', 'morganj', 'A', 'EMIA: CANDIDATE MEMBER OF EU', 'Organization Attribute', '', 'This organisation is located in a country that is a CANDIDATE MEMBER of the European Union.  For the purposes of EMEA Renewals they will be treated as a full member as will all linked sites.');
INSERT INTO `JournalActivities` VALUES ('115', '2007-10-22 06:20:01', 'morganj', '2008-03-05 04:53:40', 'morganj', 'A', 'EMIA: NOT A MEMBER OF EU', 'Organization Attribute', '', 'This organisation is located in a country that is not a member of the European Union.');
INSERT INTO `JournalActivities` VALUES ('116', '2007-10-22 06:20:55', 'morganj', '2008-03-05 04:54:43', 'morganj', 'A', 'EMIA: PRICE GROUP - UK', 'Organization Attribute', '', 'This organisation will be charged UK fees for renewals and new applications.');
INSERT INTO `JournalActivities` VALUES ('117', '2007-10-22 06:21:26', 'morganj', '2008-03-05 04:54:26', 'morganj', 'A', 'EMIA: PRICE GROUP - MAIN EURO', 'Organization Attribute', '', 'This organisation will be charged MAIN EURO fees for renewals and new applications.');
INSERT INTO `JournalActivities` VALUES ('118', '2007-10-22 06:22:05', 'morganj', '2008-03-05 04:53:58', 'morganj', 'A', 'EMIA: PRICE GROUP - EC1', 'Organization Attribute', '', 'This organisation will be charged EMERGING COUNTRIES group 1 fees for renewals and new applications.');
INSERT INTO `JournalActivities` VALUES ('119', '2007-10-22 06:22:42', 'morganj', '2008-03-05 04:54:13', 'morganj', 'A', 'EMIA: PRICE GROUP - EC2', 'Organization Attribute', '', 'This organisation will be charged EMERGING COUNTRIES group 2 fees for renewals and new applications.');
INSERT INTO `JournalActivities` VALUES ('120', '2007-10-23 01:47:50', 'morganj', '2013-01-14 11:23:42', 'neudeckerr', 'X', 'FY06 Activated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('121', '2007-10-23 01:48:07', 'morganj', '2013-01-14 11:23:49', 'neudeckerr', 'X', 'FY06 Terminated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('122', '2007-10-23 01:48:22', 'morganj', '2013-01-14 11:23:28', 'neudeckerr', 'X', 'FY05 Activated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('123', '2007-10-23 01:48:36', 'morganj', '2013-01-14 11:23:35', 'neudeckerr', 'X', 'FY05 Terminated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('124', '2007-10-23 01:48:50', 'morganj', '2013-01-14 11:23:14', 'neudeckerr', 'X', 'FY04 Activated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('125', '2007-10-23 01:49:06', 'morganj', '2013-01-14 11:23:21', 'neudeckerr', 'X', 'FY04 Terminated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('126', '2007-10-23 02:30:44', 'morganj', '2013-01-14 11:22:58', 'neudeckerr', 'X', 'FY03 Activated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('127', '2007-10-23 02:31:01', 'morganj', '2013-01-14 11:23:06', 'neudeckerr', 'X', 'FY03 Terminated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('128', '2007-10-23 02:31:16', 'morganj', '2013-01-14 11:22:49', 'neudeckerr', 'X', 'FY02 Activated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('129', '2007-10-23 02:31:29', 'morganj', '2013-01-14 11:22:42', 'neudeckerr', 'X', 'FY01 Activated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('130', '2007-10-23 02:31:58', 'morganj', '2013-01-14 11:22:34', 'neudeckerr', 'X', 'FY00 Activated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('131', '2007-10-23 03:52:25', 'morganj', '2007-11-05 07:43:04', 'morganj', 'A', 'Activation Date Unconfirmed', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('132', '2007-11-05 09:05:32', 'morganj', '2007-11-05 09:05:32', 'morganj', 'A', 'Termination Date Unconfirmed', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('133', '2007-11-21 03:00:16', 'morganj', '2014-04-01 08:53:49', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Renewals - Dealt with enquiry', 'Organization Journal Entry', '', 'Use to enter any comments about enquiries dealt with or changes to invoices made so these can be tracked.');
INSERT INTO `JournalActivities` VALUES ('134', '2007-11-21 03:00:49', 'morganj', '2014-04-01 08:54:10', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Renewals - Renewed', 'Organization Journal Entry', '', 'Use when a site has paid their renewals invoice and been renewed for FY09 2008');
INSERT INTO `JournalActivities` VALUES ('135', '2007-11-27 07:13:59', 'morganj', '2014-04-01 08:54:03', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Renewals - Refund', 'Organization Journal Entry', '', 'Used to note any refund requests');
INSERT INTO `JournalActivities` VALUES ('136', '2007-11-30 07:33:54', 'morganj', '2007-11-30 07:34:49', 'morganj', 'X', 'EMEA FY09 Renewals - Payment received and acknowledgement sent', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('137', '2007-11-30 07:35:44', 'morganj', '2014-04-01 08:53:27', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Renewals - Confirmation of Payment Received sent to ATC', 'Organization Journal Entry', '', 'When a payment has been received, an email is sent to the ORG acknowledgement it\'s receipt.');
INSERT INTO `JournalActivities` VALUES ('138', '2007-12-11 10:59:23', 'morganj', '2014-04-01 08:53:56', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Renewals - Outstanding Query', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('139', '2008-01-02 08:39:53', 'morganj', '2014-04-01 08:54:27', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Renewals - Termination Notice', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('140', '2008-01-02 08:40:11', 'morganj', '2014-04-01 08:54:19', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Renewals - Termination', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('141', '2008-01-10 01:47:11', 'morganj', '2010-12-07 02:41:28', 'morganj', 'A', 'EMEA:  ATC Agreement Active', 'Organization Journal Entry', '', 'Hard copy of 2006 Version of ATC Agreement is in the paper files at Farnborough.');
INSERT INTO `JournalActivities` VALUES ('142', '2008-01-25 06:12:49', 'morganj', '2010-12-07 02:41:15', 'morganj', 'A', 'EMEA:  Agreements received from ATC or PM', 'Organization Journal Entry', '', 'Passed onto Norman Buckberry for counter-signature');
INSERT INTO `JournalActivities` VALUES ('143', '2008-01-25 06:13:15', 'morganj', '2010-12-07 02:41:03', 'morganj', 'A', 'EMEA:  Agreements held by ATC Ops', 'Organization Journal Entry', '', 'Awaiting payment of invoice or other reasons');
INSERT INTO `JournalActivities` VALUES ('144', '2008-01-25 06:14:00', 'morganj', '2010-12-07 02:40:53', 'morganj', 'A', 'EMEA:  Agreement returned to ATC or PM', 'Organization Journal Entry', '', 'Counter-signed and complete.  Original filed in Farnborough.  Second original sent to ORG for distribution to all sites.');
INSERT INTO `JournalActivities` VALUES ('145', '2008-01-25 06:16:48', 'morganj', '2010-12-07 02:41:40', 'morganj', 'A', 'EMEA: ATC Agreement sent to ATC or PM', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('146', '2008-01-31 09:12:35', 'morganj', '2014-04-01 08:53:11', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY08 Terminated', 'Organization Journal Entry', '', 'Terminated at end of current program year. Either voluntary non-renewal, none-compliance with T\'s & C\'s or forced.');
INSERT INTO `JournalActivities` VALUES ('147', '2008-01-31 10:19:00', 'morganj', '2014-04-01 08:53:39', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Renewals - Deactivated Sites', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('148', '2008-01-31 10:21:03', 'morganj', '2008-02-18 02:04:51', 'morganj', 'A', 'EMIA FY09 Renewals - Deactivated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('149', '2008-02-07 06:32:52', 'morganj', '2008-02-18 02:04:37', 'morganj', 'A', 'EMIA FY09 Renewal Pack Sent', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('150', '2008-02-14 00:58:25', 'jiangp', '2014-04-01 08:52:10', 'norman.buckberry@autodesk.com', 'X', 'APAC FY09 Renewed', 'Organization Journal Entry', 'Y', 'Use when order been processed');
INSERT INTO `JournalActivities` VALUES ('151', '2008-02-17 00:46:11', 'jiangp', '2014-04-01 08:52:17', 'norman.buckberry@autodesk.com', 'X', 'APAC FY09 Terminated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('152', '2008-02-17 01:00:37', 'jiangp', '2014-04-01 08:51:55', 'norman.buckberry@autodesk.com', 'X', 'APAC FY09 Activated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('153', '2008-02-18 09:54:52', 'morganj', '2014-04-01 08:53:00', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY07 Terminated', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('154', '2008-02-18 09:55:10', 'morganj', '2014-04-01 08:52:51', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY06 Terminated', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('155', '2008-02-27 21:47:34', 'jiangp', '2014-04-01 08:52:02', 'norman.buckberry@autodesk.com', 'X', 'APAC FY09 Order Processed', 'Organization Journal Entry', '', 'You can enter ZLNT number in the comment field. If you don\'t have ZLNT, you can leave it blank or enter other comments.');
INSERT INTO `JournalActivities` VALUES ('156', '2008-04-01 04:25:55', '41118', '2013-01-14 11:17:21', 'neudeckerr', 'X', 'HORZ_A: Architecture - Autodesk NavisWorks', 'Site Accreditation', 'Y', 'Products included:  Autodesk NavisWorks Manage (includes Autodesk NavisWorks Simulate & Autodesk NavisWorks Review).  Autodesk NavisWorks Freedom is a freebie as part of all products.');
INSERT INTO `JournalActivities` VALUES ('157', '2008-04-30 09:37:48', 'morganj', '2008-04-30 10:00:24', 'morganj', 'X', 'AEC_A: Architecture Building Systems - AutoCAD', 'Site Accreditation', 'Y', 'Products Included:  AutoCAD MEP');
INSERT INTO `JournalActivities` VALUES ('158', '2008-04-30 09:54:58', 'morganj', '2013-01-14 11:15:01', 'neudeckerr', 'X', 'AEC_A: Architecture Building Systems - Revit MEP', 'Site Accreditation', '', 'Products Included:  Revit MEP');
INSERT INTO `JournalActivities` VALUES ('159', '2008-04-30 10:05:16', 'morganj', '2013-01-14 11:15:28', 'neudeckerr', 'X', 'AEC_E: MEP Engineering', 'Site Accreditation', 'Y', 'Products Included:  AutoCAD MEP, Revit MEP');
INSERT INTO `JournalActivities` VALUES ('160', '2008-04-30 10:21:43', 'morganj', '2013-01-14 11:17:29', 'neudeckerr', 'X', 'HORZ_V: Design Vizualization - Autodesk 3ds Max Design', 'Site Accreditation', 'Y', 'Products Included:  Autodesk 3ds Max Design, Autodesk VIZ and Autodesk Mudbox');
INSERT INTO `JournalActivities` VALUES ('161', '2008-04-30 10:36:24', 'morganj', '2013-01-14 11:21:15', 'neudeckerr', 'X', 'MFG_M:  Manufacturing 2D - AutoCAD Mechanical', 'Site Accreditation', 'Y', 'Products included:  AutoCAD Mechanical');
INSERT INTO `JournalActivities` VALUES ('162', '2008-04-30 10:37:43', 'morganj', '2013-01-14 11:20:52', 'neudeckerr', 'X', 'MFG_M:  Manufacturing', 'Site Accreditation', 'Y', 'Products included:  AutoCAD Mechanical, Autodesk Inventor, Autodesk Inventor Professional, Autodesk Mechanical Desktop, Autodesk Productstream, Autodesk Streamline, Autodesk Vault');
INSERT INTO `JournalActivities` VALUES ('163', '2008-04-30 10:44:37', 'morganj', '2013-01-14 11:16:25', 'neudeckerr', 'X', 'AEC_P:  Plant Solutions - AutoCAD P&ID & AutoCAD Plant 3D', 'Site Accreditation', 'Y', 'Products Included:  AutoCAD P&ID and AutoCAD Plant 3D');
INSERT INTO `JournalActivities` VALUES ('164', '2008-04-30 10:46:54', 'morganj', '2013-01-14 11:15:54', 'neudeckerr', 'X', 'AEC_G: Geospatial', 'Site Accreditation', 'Y', 'Products included:  AutoCAD Map 3D, Autodesk MapGujide, Autodesk mapGuide Enterprise, Autodesk MapGuide Studio.');
INSERT INTO `JournalActivities` VALUES ('165', '2008-04-30 10:47:30', 'morganj', '2013-01-14 11:16:13', 'neudeckerr', 'X', 'AEC_G: Geospatial - Topobase', 'Site Accreditation', 'Y', 'Products included:  Autodesk Topobase');
INSERT INTO `JournalActivities` VALUES ('166', '2008-05-28 04:22:40', '41118', '2008-05-28 04:22:40', '41118', 'A', 'Certification Site Activated', 'Site Journal Entry', '', 'To track the Date on which the site was approved as a Certification Site');
INSERT INTO `JournalActivities` VALUES ('167', '2008-05-28 04:24:02', '41118', '2008-05-28 04:24:02', '41118', 'A', 'Certification Site Terminated', 'Site Journal Entry', '', 'To track when a Site was Terminated as a Certification Partner');
INSERT INTO `JournalActivities` VALUES ('168', '2008-06-10 10:22:33', 'morganj', '2008-06-10 10:22:53', 'morganj', 'X', 'AAI 2008', 'Contact Attribute', '', null);
INSERT INTO `JournalActivities` VALUES ('169', '2008-06-10 10:23:40', 'morganj', '2014-04-01 08:51:05', 'norman.buckberry@autodesk.com', 'X', 'Tech Camp 2008 - MSD', 'Contact Attribute', 'Y', 'Attendance at Tech Camp 2008 - MSD');
INSERT INTO `JournalActivities` VALUES ('170', '2008-06-10 10:24:02', 'morganj', '2014-04-01 08:50:58', 'norman.buckberry@autodesk.com', 'X', 'Tech Camp 2008 - M&E', 'Contact Attribute', 'Y', 'Attendance at Tech Camp 2008 - M&E');
INSERT INTO `JournalActivities` VALUES ('171', '2008-06-10 10:24:33', 'morganj', '2014-04-01 08:50:51', 'norman.buckberry@autodesk.com', 'X', 'Tech Camp 2008 - Geospatial', 'Contact Attribute', 'Y', 'Attendance at Tech Camp 2008 - Geospatial');
INSERT INTO `JournalActivities` VALUES ('172', '2008-06-10 10:26:02', 'morganj', '2014-04-01 08:50:41', 'norman.buckberry@autodesk.com', 'X', 'Tech Camp 2008 - AEC', 'Contact Attribute', 'Y', 'Attendance at Tech Camp 2008 - AEC');
INSERT INTO `JournalActivities` VALUES ('173', '2008-06-10 10:28:57', 'morganj', '2008-06-10 10:29:59', 'morganj', 'X', 'PLP ATC Learning Path - AEC', 'Contact Attribute', 'Y', 'Has completed all or some of the online ATC modules on the PLP for AEC');
INSERT INTO `JournalActivities` VALUES ('174', '2008-06-10 10:29:32', 'morganj', '2008-06-10 10:30:05', 'morganj', 'X', 'PLP ATC Learning Path - Geospatial', 'Contact Attribute', 'Y', 'Has completed all or some of the online ATC modules on the PLP for Geospatial');
INSERT INTO `JournalActivities` VALUES ('175', '2008-07-11 07:38:08', '41118', '2008-07-11 07:39:25', '41118', 'X', 'max_modeling', 'Contact Attribute', 'Y', 'Experience in Modeling in Max');
INSERT INTO `JournalActivities` VALUES ('176', '2008-07-31 10:09:28', 'morganj', '2012-11-06 03:39:49', 'buckberr', 'X', 'AAI 2008-2009', 'Contact Journal Entry', '', 'Fulfilled requirements (online training and 85%+ satisfaction rating)');
INSERT INTO `JournalActivities` VALUES ('177', '2008-08-20 10:10:03', 'morganj', '2013-01-14 11:20:00', 'neudeckerr', 'X', 'MED_Systems: Flame', 'Site Accreditation', 'Y', 'Includes Autodesk Flame and Autodesk Smoke');
INSERT INTO `JournalActivities` VALUES ('178', '2008-09-03 02:29:07', 'morganj', '2013-01-14 11:20:08', 'neudeckerr', 'X', 'MED_Systems: Smoke', 'Site Accreditation', 'Y', 'Includes Autodesk Smoke');
INSERT INTO `JournalActivities` VALUES ('179', '2008-09-24 09:36:58', 'morganj', '2008-09-24 09:37:15', 'morganj', 'X', 'M&ETOXIK', 'Site Accreditation', '', 'M&E_Toxik');
INSERT INTO `JournalActivities` VALUES ('180', '2008-09-24 09:37:44', 'morganj', '2013-01-14 11:20:15', 'neudeckerr', 'X', 'MED_Toxik', 'Site Accreditation', 'Y', 'Autodesk Toxik');
INSERT INTO `JournalActivities` VALUES ('181', '2008-09-29 13:00:01', 'herculg', '2008-09-29 13:01:30', 'herculg', 'A', 'IVAR- Indirect Value Added Reseller', 'Organization Attribute', 'Y', 'Indirect Value Add Reseller');
INSERT INTO `JournalActivities` VALUES ('182', '2008-11-05 09:47:33', 'jiangp', '2013-01-14 11:25:52', 'neudeckerr', 'X', 'FY10 Renewed', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('183', '2008-11-05 09:47:54', 'jiangp', '2013-01-14 11:26:00', 'neudeckerr', 'X', 'FY10 Terminated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('184', '2008-11-05 09:48:14', 'jiangp', '2013-01-14 11:25:37', 'neudeckerr', 'X', 'FY10 Activated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('185', '2008-12-05 14:19:20', 't_barrw', '2008-12-05 14:19:20', 't_barrw', 'A', 'Active for LMS', 'VAR Contact Attribute', 'Y', 'For LMS integration after Jan 31, 2009');
INSERT INTO `JournalActivities` VALUES ('186', '2008-12-16 08:07:05', 'morganj', '2014-04-01 08:55:08', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY10 Renewals Query', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('187', '2008-12-16 08:07:35', 'morganj', '2014-04-01 08:55:16', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY10 Renewed', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('188', '2008-12-16 08:07:47', 'morganj', '2014-04-01 08:55:23', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY10 Terminated', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('189', '2008-12-16 08:07:57', 'morganj', '2014-04-01 08:54:54', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY10 Activated', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('190', '2008-12-16 08:08:39', 'morganj', '2014-04-01 08:55:01', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY10 Renewals Deactivated', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('191', '2009-01-07 05:49:26', 'morganj', '2014-04-01 08:55:43', 'norman.buckberry@autodesk.com', 'X', 'EMEA INDIA FY10 Renewal Forms Received', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('192', '2009-01-07 05:50:48', 'morganj', '2009-01-07 05:51:09', 'morganj', 'A', 'EMIA FY10 India Renewals Forms Received', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('193', '2009-01-09 01:16:12', 'jiangp', '2014-04-01 08:52:33', 'norman.buckberry@autodesk.com', 'X', 'APAC FY10 Renewed', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('194', '2009-01-09 01:16:37', 'jiangp', '2014-04-01 08:52:40', 'norman.buckberry@autodesk.com', 'X', 'APAC FY10 Terminated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('195', '2009-01-09 01:17:09', 'jiangp', '2014-04-01 08:52:26', 'norman.buckberry@autodesk.com', 'X', 'APAC FY10 Activated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('196', '2009-01-28 07:50:40', 'morganj', '2014-04-01 08:54:34', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Terminated', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('197', '2009-01-28 09:32:40', 'morganj', '2014-04-01 08:53:18', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY09 Activated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('198', '2009-04-01 13:38:42', 'herculg', '2013-01-14 11:21:53', 'neudeckerr', 'X', 'MFG_M:Manufacturing Electrical –ECSCAD', 'Site Accreditation', 'Y', 'Products Include: Autodesk ECSCAD');
INSERT INTO `JournalActivities` VALUES ('199', '2009-04-07 10:01:31', 'herculg', '2013-01-14 11:16:38', 'neudeckerr', 'X', 'AEC_S:Structural Engineering - Robot Structural Analysis', 'Site Accreditation', '', 'Products included Robot Structural Analysis');
INSERT INTO `JournalActivities` VALUES ('200', '2009-04-20 08:39:48', 'morganj', '2013-01-14 11:25:45', 'neudeckerr', 'X', 'FY10 Evaluation System Compliance Issues', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('201', '2009-05-21 05:40:36', 'morganj', '2014-04-01 08:57:51', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA AOTC:  PSEB', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('202', '2009-05-21 05:40:56', 'morganj', '2014-04-01 08:57:11', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA AOTC:  AEC', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('203', '2009-05-21 05:42:04', 'morganj', '2014-04-01 08:57:23', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA AOTC:  MSD-ENG', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('204', '2009-05-21 05:42:32', 'morganj', '2014-04-01 08:57:31', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA AOTC:  MSD-ID', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('205', '2009-05-21 05:42:51', 'morganj', '2014-04-01 08:57:17', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA AOTC:  M&E', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('206', '2009-05-21 06:16:44', 'morganj', '2017-02-22 04:30:50', 'robneudecker@engageglobalsolutions.com', 'A', 'ACU', 'Contact Attribute', 'Y', 'Autodesk Certified User Exam');
INSERT INTO `JournalActivities` VALUES ('207', '2009-05-21 06:17:12', 'morganj', '2017-02-22 04:34:04', 'robneudecker@engageglobalsolutions.com', 'A', 'ACP', 'Contact Attribute', '', 'Autodesk Certified Professional Exam\r\n(Please add the date of the certification in the comments section)');
INSERT INTO `JournalActivities` VALUES ('208', '2009-05-21 08:10:55', 'morganj', '2014-04-01 08:57:39', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA AOTC:  No order received', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('209', '2009-05-21 08:33:20', 'morganj', '2014-04-01 08:57:45', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA AOTC:  Pending', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('210', '2009-05-21 09:03:12', 'morganj', '2014-04-01 08:57:58', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA AOTC: Not mandated (MED)', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('211', '2009-07-08 06:28:01', 'morganj', '2013-01-14 11:14:47', 'neudeckerr', 'X', 'AEC_A: Architecture - Autodesk Ecotect Analysis', 'Site Accreditation', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('212', '2009-07-14 02:25:32', 'morganj', '2014-04-01 08:58:05', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA:  Deferred Payment (FY10 New Site)', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('213', '2009-07-14 02:26:13', 'morganj', '2014-04-01 08:58:12', 'norman.buckberry@autodesk.com', 'X', '2010 INDIA:  Deferred Payment (FY10 Renewal)', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('214', '2009-08-13 03:35:18', 'morganj', '2009-08-13 03:36:54', 'morganj', 'X', 'India CAT A site', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('215', '2009-08-13 03:35:31', 'morganj', '2009-08-13 03:37:03', 'morganj', 'X', 'India CAT B site', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('216', '2009-08-13 03:35:57', 'morganj', '2009-08-13 03:35:57', 'morganj', 'A', 'India CAT A site', 'Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('217', '2009-08-13 03:36:10', 'morganj', '2009-08-13 03:36:10', 'morganj', 'A', 'India CAT B site', 'Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('218', '2009-08-13 03:36:28', 'morganj', '2009-08-13 03:36:28', 'morganj', 'A', 'India CAT A site', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('219', '2009-08-13 03:36:40', 'morganj', '2009-08-13 03:36:40', 'morganj', 'A', 'India CAT B site', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('220', '2009-08-25 07:14:22', 'janm', '2009-08-25 07:14:22', 'janm', 'A', 'ATC Agreement Active', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('221', '2009-09-15 00:49:18', 'janm', '2009-09-15 00:49:18', 'janm', 'A', 'DSL Agreement', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('222', '2009-09-17 12:58:09', 'garyh', '2017-02-22 04:34:36', 'robneudecker@engageglobalsolutions.com', 'A', 'ACI', 'Contact Attribute', '', 'Autodesk Certified Instructor\r\n(Please add the date of achieving ACI status in the comments section)');
INSERT INTO `JournalActivities` VALUES ('223', '2009-10-05 06:45:42', '41118', '2009-10-17 10:47:52', 'david.sanchez@autodesk.com', 'A', 'Autodesk: Global Administrators', 'Contact Security Role', 'Y', 'Autodesk access to Global Learner Data through ALC Reporting Feature.');
INSERT INTO `JournalActivities` VALUES ('224', '2009-10-05 06:48:34', '41118', '2009-10-17 10:49:36', 'david.sanchez@autodesk.com', 'A', 'Autodesk: EMEA', 'Contact Security Role', 'Y', 'Autodesk access to all EMEA Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('226', '2009-10-05 06:56:33', '41118', '2009-10-17 10:50:27', 'david.sanchez@autodesk.com', 'A', 'Autodesk: EMEA - NE Region', 'Contact Security Role', 'Y', 'Autodesk access to all Learner Data in the NE Region (Northern Europe) of the Geo EMEA through ALC Reporting Feature.');
INSERT INTO `JournalActivities` VALUES ('225', '2009-10-05 06:53:32', '41118', '2009-10-17 10:49:17', 'david.sanchez@autodesk.com', 'A', 'Autodesk: APAC', 'Contact Security Role', 'Y', 'Autodesk access to all Apac Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('227', '2009-10-05 07:06:09', '41118', '2009-10-17 10:51:19', 'david.sanchez@autodesk.com', 'A', 'Autodesk: EMEA - NE Region - UK and IR', 'Contact Security Role', 'Y', 'Autodesk access to all Learner Data in the Sub-Region of UK and Ireland (UKIR) which is part of the NE Northern Europe Region (NE) in the EMEA Geo through ALC Reporting Feature.');
INSERT INTO `JournalActivities` VALUES ('228', '2009-10-05 07:11:45', '41118', '2009-10-22 07:35:25', '41118', 'A', 'Partner: Organization Manager', 'Contact Security Role', 'Y', 'Implied Partner Organization Level Reporting: Access to all Learner Data in all sites affiliated to the Organization ID (passed in the XML Token) that the Contact is affiliated to.');
INSERT INTO `JournalActivities` VALUES ('229', '2009-10-17 09:48:59', 't_barrw', '2009-10-22 07:34:31', '41118', 'A', 'Partner: Site Manager', 'Contact Security Role', 'Y', 'Implied Partner Site Level Reporting: Access to all Learner Data in a specific Site or group of Sites connected to a specific Organization ID that the Contact is affiliated to.');
INSERT INTO `JournalActivities` VALUES ('230', '2009-10-17 09:49:23', 't_barrw', '2009-10-17 11:51:54', 'david.sanchez@autodesk.com', 'A', 'Autodesk: Global Account Manager - Name', 'Contact Security Role', 'Y', 'Specifically for an Autodesk Global Account Manager giving access to Global Learner Data on a specific group of Organizations through ALC Reporting Feature.');
INSERT INTO `JournalActivities` VALUES ('234', '2009-10-27 06:20:22', '41118', '2009-10-27 06:20:22', '41118', 'A', 'Autodesk Consultant', 'Contact Attribute', 'Y', 'To identify contractors for Autodesk such as the old AAC individuals who train on behalf of Autodesk etc.');
INSERT INTO `JournalActivities` VALUES ('231', '2009-10-19 07:43:57', '41118', '2009-10-19 07:43:57', '41118', 'A', 'Autodesk: EMEA - CE Region', 'Contact Security Role', 'Y', 'Autodesk access to all Learner Data in the CE (Central East) Region  of the Geo EMEA in the ALC Reporting Tool.');
INSERT INTO `JournalActivities` VALUES ('232', '2009-10-21 07:59:28', '41118', '2009-10-21 07:59:28', '41118', 'A', 'Autodesk: Amer', 'Contact Security Role', 'Y', 'Autodesk access to all Americas Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('233', '2009-10-21 08:01:32', '41118', '2009-10-21 08:01:32', '41118', 'A', 'Autodesk: Amer - LA Region', 'Contact Security Role', 'Y', 'Autodesk access to all Learner Data in the LA (Latin America) Region of the Geo Amer in the ALC Reporting Tool.');
INSERT INTO `JournalActivities` VALUES ('235', '2009-10-30 06:29:42', 'janine.franz@autodesk.com', '2009-10-30 06:30:20', 'janine.franz@autodesk.com', 'A', 'Autodesk: EMEA - Emerging Region', 'Contact Security Role', 'Y', 'Autodesk access to all Learner Data in the EC (Emerging Region) Region of the Geo EMEA in the ALC Reporting Tool.');
INSERT INTO `JournalActivities` VALUES ('236', '2009-11-03 13:34:15', 'janine.franz@autodesk.com', '2009-11-03 13:34:50', 'janine.franz@autodesk.com', 'A', 'Autodesk: EMEA - SE Region', 'Contact Security Role', 'Y', 'Autodesk access to all Learner Data in the SE (Southern) Region of the Geo EMEA in the ALC Reporting Tool.');
INSERT INTO `JournalActivities` VALUES ('237', '2009-12-03 02:49:34', 'jiangp', '2013-01-14 11:26:08', 'neudeckerr', 'X', 'FY11 Activated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('238', '2009-12-03 02:50:08', 'jiangp', '2013-01-14 11:26:27', 'neudeckerr', 'X', 'FY11 Renewed', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('239', '2009-12-03 02:50:32', 'jiangp', '2013-01-14 11:26:33', 'neudeckerr', 'X', 'FY11 Terminated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('240', '2009-12-15 23:58:17', 'jiangp', '2014-04-01 08:56:16', 'norman.buckberry@autodesk.com', 'X', 'APAC FY11 Renewed', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('241', '2009-12-15 23:58:40', 'jiangp', '2014-04-01 08:56:23', 'norman.buckberry@autodesk.com', 'X', 'APAC FY11 Terminated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('242', '2009-12-15 23:59:14', 'jiangp', '2014-04-01 08:56:08', 'norman.buckberry@autodesk.com', 'X', 'APAC FY11 Activated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('243', '2009-12-16 03:23:27', 'janm', '2013-01-14 11:21:30', 'neudeckerr', 'X', 'MFG_M: Manufacturing - Moldflow', 'Site Accreditation', 'Y', 'Autodesk Moldflow Adviser & Autodesk Moldflow Insight');
INSERT INTO `JournalActivities` VALUES ('244', '2010-01-12 07:12:15', '41118', '2010-01-12 07:13:07', '41118', 'A', 'Autodesk: Amer and EMEA', 'Contact Security Role', 'Y', 'Autodesk access to all EMEA and Americas Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('245', '2010-01-13 06:21:40', 'janm', '2014-04-01 08:55:30', 'norman.buckberry@autodesk.com', 'X', 'EMEA FY11 Renewed', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('247', '2010-01-25 07:35:49', 'janm', '2017-04-21 09:16:54', 'nbuckberry@engageglobalsolutions.com', 'X', 'CIS Distributor: MONT', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('246', '2010-01-13 06:23:34', 'janm', '2014-04-01 08:55:55', 'norman.buckberry@autodesk.com', 'X', 'FY11 New Site - Invoice Paid', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('248', '2010-01-25 07:36:13', 'janm', '2010-01-25 07:37:18', 'janm', 'X', 'CIS Distributor:  KAZCAD', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('249', '2010-01-25 07:37:49', 'janm', '2017-04-21 09:16:39', 'nbuckberry@engageglobalsolutions.com', 'X', 'CIS Distributor:  KAZCAD', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('250', '2010-01-25 07:38:07', 'janm', '2017-04-21 09:16:46', 'nbuckberry@engageglobalsolutions.com', 'X', 'CIS Distributor:  SOFTPROM', 'Organization Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('251', '2010-02-12 08:25:22', '41118', '2010-02-12 08:25:22', '41118', 'A', 'Autodesk: Amer - North America', 'Contact Security Role', 'Y', 'Autodesk access to all Learner Data in North America Region only in the Geo Americas on the ALC Reporting Tool.');
INSERT INTO `JournalActivities` VALUES ('252', '2010-02-12 09:53:16', 'janm', '2010-02-12 09:53:16', 'janm', 'A', 'New Application: AEC', 'Site Attribute', 'Y', 'A new site applicant has requested AEC products for ATC accreditations');
INSERT INTO `JournalActivities` VALUES ('253', '2010-02-12 09:54:05', 'janm', '2010-02-12 09:56:04', 'janm', 'A', 'New Application:  MFG', 'Site Attribute', 'Y', 'A new site applicant has requested Manufacturing products for ATC accreditations');
INSERT INTO `JournalActivities` VALUES ('254', '2010-02-12 09:54:51', 'janm', '2010-02-12 09:54:51', 'janm', 'A', 'New Application:  M&E', 'Site Attribute', 'Y', 'A new site applicant has requested M&E products for ATC accreditations');
INSERT INTO `JournalActivities` VALUES ('255', '2010-02-12 09:55:16', 'janm', '2010-02-12 09:55:16', 'janm', 'A', 'New Application:  M&E Sys', 'Site Attribute', 'Y', 'A new site applicant has requested M&E Systems for ATC accreditations');
INSERT INTO `JournalActivities` VALUES ('256', '2010-02-12 09:55:34', '41118', '2010-02-12 09:56:54', '41118', 'A', 'Tier_Gold', 'VAR Site Attribute', 'Y', 'If needing to track Gold VAR Partner Sites.');
INSERT INTO `JournalActivities` VALUES ('257', '2010-02-12 09:55:48', 'janm', '2010-02-12 09:55:48', 'janm', 'A', 'New Application:  Horiz Des', 'Site Attribute', 'Y', 'A new site applicant has requested Horizontal Design products for ATC accreditations');
INSERT INTO `JournalActivities` VALUES ('258', '2010-02-12 09:56:18', '41118', '2010-02-12 09:56:18', '41118', 'A', 'Tier_Bronze', 'VAR Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('259', '2010-02-12 09:57:22', '41118', '2010-02-12 09:57:22', '41118', 'A', 'Tier_Silver', 'VAR Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('260', '2010-02-12 10:23:56', '41118', '2010-02-12 10:48:53', '41118', 'A', 'ZT_Category', 'VAR Contact Attribute', 'Y', 'Zaptext migrated data to track active or Base Data etc.');
INSERT INTO `JournalActivities` VALUES ('261', '2010-02-12 10:25:47', '41118', '2010-02-12 10:25:47', '41118', 'A', 'ZT_Opt_Out_SMS', 'VAR Contact Attribute', 'Y', 'Opt out = Yes, Opt in = No');
INSERT INTO `JournalActivities` VALUES ('262', '2010-02-12 10:35:58', '41118', '2010-02-12 10:50:13', '41118', 'A', 'ZT_Job_Function', 'VAR Contact Attribute', 'Y', 'For historical purposes. All Contacts will reprofile themselves in ALC..');
INSERT INTO `JournalActivities` VALUES ('263', '2010-02-12 10:36:47', '41118', '2010-02-12 10:36:47', '41118', 'A', 'ZT_External_Notes', 'VAR Contact Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('264', '2010-02-12 10:38:58', '41118', '2010-02-12 10:38:58', '41118', 'A', 'ZT_Manager', 'VAR Contact Attribute', 'Y', 'This is the Contacts Manager at their Company/Site');
INSERT INTO `JournalActivities` VALUES ('265', '2010-02-12 10:40:29', '41118', '2010-02-12 10:40:29', '41118', 'A', 'ZT_AgreedTC', 'VAR Contact Attribute', 'Y', 'Yes or No value, on or off, true or false.');
INSERT INTO `JournalActivities` VALUES ('266', '2010-02-12 10:42:05', '41118', '2010-02-12 10:42:05', '41118', 'A', 'ZT_New_Blood', 'VAR Contact Attribute', 'Y', 'the default string value derived by ZT team.');
INSERT INTO `JournalActivities` VALUES ('267', '2010-02-12 10:42:45', '41118', '2010-02-12 10:42:45', '41118', 'A', 'ZT_Start_Date', 'VAR Contact Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('268', '2010-02-12 10:43:02', '41118', '2010-02-12 10:43:02', '41118', 'A', 'ZT_Join_Date', 'VAR Contact Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('269', '2010-02-12 10:44:38', '41118', '2010-02-12 10:44:38', '41118', 'A', 'ZT_C_AUTH', 'VAR Contact Attribute', 'Y', 'Comma separated list od Contact Authorizations');
INSERT INTO `JournalActivities` VALUES ('270', '2010-02-12 10:46:36', '41118', '2010-02-12 10:46:36', '41118', 'A', 'ZT_Contact_Seibel_ID', 'VAR Contact Attribute', 'Y', 'Seibel ID/record number of the Contact for reference..');
INSERT INTO `JournalActivities` VALUES ('271', '2010-02-12 11:13:06', '41118', '2010-02-12 11:13:06', '41118', 'A', 'ZT_Account_Manager', 'VAR Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('272', '2010-02-12 11:18:54', '41118', '2010-02-12 11:18:54', '41118', 'A', 'ZT_Parent_Account', 'VAR Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('273', '2010-02-12 11:19:27', '41118', '2010-02-12 11:19:27', '41118', 'A', 'ZT_SAP_ID', 'VAR Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('274', '2010-02-12 11:20:02', '41118', '2010-02-12 11:20:02', '41118', 'A', 'ZT_Booking_Regime', 'VAR Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('275', '2010-02-12 11:20:40', '41118', '2010-02-12 11:20:40', '41118', 'A', 'ZT_Training_Authoriser', 'VAR Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('276', '2010-02-12 11:21:43', '41118', '2010-02-12 11:21:43', '41118', 'A', 'ZT_Course_Packages', 'VAR Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('277', '2010-02-12 11:23:47', '41118', '2010-02-12 11:23:47', '41118', 'A', 'ZT_P_AUTH', 'VAR Site Attribute', 'Y', 'list of comma separated values at a Partner Site Level');
INSERT INTO `JournalActivities` VALUES ('278', '2010-02-12 11:24:50', '41118', '2010-02-12 11:24:50', '41118', 'A', 'ZT_External_Agent_Privileges', 'VAR Site Attribute', 'Y', 'Book and/or Cancel');
INSERT INTO `JournalActivities` VALUES ('279', '2010-02-19 10:53:33', 't_barrw', '2010-02-19 10:53:33', 't_barrw', 'A', 'ZT_Zaptext_ContactId', 'VAR Contact Attribute', 'Y', 'Old ID for reference purposes.');
INSERT INTO `JournalActivities` VALUES ('280', '2010-03-22 12:17:34', '41118', '2011-07-12 06:10:21', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Kirk Givens', 'Contact Security Role', 'Y', 'Specific Role (Group) for Kirk Givens in the Americas Geo');
INSERT INTO `JournalActivities` VALUES ('281', '2010-03-22 12:23:46', '41118', '2011-07-12 06:10:05', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Karen Jacobson', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('282', '2010-03-22 12:45:24', '41118', '2011-07-12 06:09:24', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Claude Carmel', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('283', '2010-03-22 12:45:46', '41118', '2011-07-12 06:08:03', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Cathrin Gay', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('284', '2010-03-22 12:46:10', '41118', '2011-07-12 06:07:55', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Bryan Rader', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('285', '2010-03-22 12:46:50', '41118', '2011-07-12 06:10:28', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Marcus Tateishi', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('286', '2010-03-22 12:47:11', '41118', '2011-07-12 06:09:51', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Jon Benz', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('287', '2010-03-22 12:47:35', '41118', '2011-07-12 06:10:36', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Paul Godawa', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('288', '2010-03-22 12:48:21', '41118', '2011-07-12 06:10:44', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Paul Harris', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('289', '2010-03-22 12:48:49', '41118', '2011-07-12 06:10:13', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Kevin McClure', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('290', '2010-03-22 12:49:23', '41118', '2011-07-12 06:11:09', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - TBH Western Territory PM', 'Contact Security Role', 'Y', 'ALC Report access for Western territory of US');
INSERT INTO `JournalActivities` VALUES ('291', '2010-03-22 12:49:44', '41118', '2011-07-12 06:07:46', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Brendan Duffy', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('292', '2010-03-22 12:50:05', '41118', '2011-07-12 06:09:43', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - John Troiano', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('293', '2010-03-22 12:50:27', '41118', '2011-07-12 06:09:35', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Fred Twombly', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('294', '2010-03-29 07:57:21', '41118', '2011-07-12 06:09:58', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Joseph DiMartino', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR Orgs.');
INSERT INTO `JournalActivities` VALUES ('295', '2010-03-29 07:58:49', '41118', '2011-07-12 06:10:51', 'anna.maria.romeo@autodesk.com', 'X', 'Autodesk: Account Manager - Roy Martin', 'Contact Security Role', 'Y', 'ALC Security Role access to specific US VAR sites');
INSERT INTO `JournalActivities` VALUES ('296', '2010-04-14 17:40:07', 'anna.maria.romeo@autodesk.com', '2010-04-14 17:41:06', 'anna.maria.romeo@autodesk.com', 'A', 'Inside Sales Denver', 'Contact Security Role', 'Y', 'partner sites for inside sales');
INSERT INTO `JournalActivities` VALUES ('297', '2010-04-27 15:31:27', 't_barrw', '2010-04-27 15:32:34', 't_barrw', 'A', 'Siebel Authorization Data', 'VAR Contact Journal Entry', '', 'From the May 2010 Siebel  Integration Exercise');
INSERT INTO `JournalActivities` VALUES ('298', '2010-06-25 01:31:36', 'janm', '2013-01-14 11:26:18', 'neudeckerr', 'X', 'FY11 Notice of Termination', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('299', '2010-07-30 05:59:26', 'janm', '2010-07-30 06:01:38', 'janm', 'A', 'EMEA CSN validated', 'Site Attribute', 'Y', 'When a CSN has been checked on Siebel to ensure it is valid for this record.');
INSERT INTO `JournalActivities` VALUES ('300', '2010-08-11 11:15:52', 'morganj', '2010-08-11 11:15:52', 'morganj', 'A', 'EMEA Evaluation System Compliance Check', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('301', '2010-08-12 07:36:50', 'buckberr', '2013-01-14 11:16:19', 'neudeckerr', 'X', 'AEC_G:Geospatial - LandXplorer', 'Site Accreditation', '', 'Products include Autodesk LandXplorer Studio Professional and Autodesk LandXplorer Server');
INSERT INTO `JournalActivities` VALUES ('302', '2010-08-12 07:39:46', 'buckberr', '2013-01-14 11:19:44', 'neudeckerr', 'X', 'MED_E: 3D Animation - Softimage', 'Site Accreditation', '', 'Products included: Autodesk Softimage');
INSERT INTO `JournalActivities` VALUES ('303', '2010-09-28 03:27:08', 'anna.maria.romeo@autodesk.com', '2010-09-28 03:28:09', 'anna.maria.romeo@autodesk.com', 'A', 'Autodesk: APAC - ANZ Region', 'Contact Security Role', 'Y', 'Autodesk access to Apac-ANZ  Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('304', '2010-09-28 04:16:06', 'anna.maria.romeo@autodesk.com', '2012-05-15 08:43:37', '41118', 'X', 'APAC – ASEAN Region', 'Contact Security Role', 'Y', 'Autodesk access to all APAC - ASEAN Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('305', '2010-09-28 04:17:26', 'anna.maria.romeo@autodesk.com', '2012-05-15 08:43:52', '41118', 'X', 'APAC – China Region', 'Contact Security Role', 'Y', 'Autodesk access to all APAC - China Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('306', '2010-09-28 04:18:14', 'anna.maria.romeo@autodesk.com', '2012-05-15 08:44:26', '41118', 'X', 'APAC – Japan Region', 'Contact Security Role', '', 'Autodesk access to all APAC - Japan Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('307', '2010-09-28 04:19:03', 'anna.maria.romeo@autodesk.com', '2012-05-15 08:44:40', '41118', 'X', 'APAC – Korea Region', 'Contact Security Role', 'Y', 'Autodesk access to all APAC - Korea Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('308', '2010-09-28 04:19:59', 'anna.maria.romeo@autodesk.com', '2012-05-15 08:44:09', '41118', 'X', 'APAC – India and SAARC Region', 'Contact Security Role', 'Y', 'Autodesk access to all APAC - India and SAARC region Learner Data through ALC Reporting Feature');
INSERT INTO `JournalActivities` VALUES ('309', '2010-10-12 05:47:19', 'buckberr', '2013-01-14 11:22:01', 'neudeckerr', 'X', 'MFG_M:Manufacturing Simulation - Autodesk Algor', 'Site Accreditation', '', 'Products included: Algor Simulation / Professional, Algor Simulation CFD, Algor Simulation MES; Algor Simulation Solver');
INSERT INTO `JournalActivities` VALUES ('310', '2010-11-01 22:22:54', 'chongl', '2017-04-21 09:11:32', 'nbuckberry@engageglobalsolutions.com', 'X', 'APAC FY12 Activated', 'Organization Journal Entry', '', 'APAC FY12 new ATC');
INSERT INTO `JournalActivities` VALUES ('311', '2010-11-01 22:23:41', 'chongl', '2017-04-21 09:11:39', 'nbuckberry@engageglobalsolutions.com', 'X', 'APAC FY12 Renewed', 'Organization Journal Entry', '', 'APAC FY12 Renewed');
INSERT INTO `JournalActivities` VALUES ('312', '2010-11-01 22:24:07', 'chongl', '2017-04-21 09:11:45', 'nbuckberry@engageglobalsolutions.com', 'X', 'APAC FY12 Terminated', 'Organization Journal Entry', '', 'APAC FY12 Terminated');
INSERT INTO `JournalActivities` VALUES ('313', '2010-11-01 22:25:20', 'chongl', '2013-01-14 11:26:43', 'neudeckerr', 'X', 'FY12 Activated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('314', '2010-11-01 22:25:42', 'chongl', '2013-01-14 11:27:00', 'neudeckerr', 'X', 'FY12 Renewed', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('315', '2010-11-01 22:26:05', 'chongl', '2013-01-14 11:27:08', 'neudeckerr', 'X', 'FY12 Terminated', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('316', '2010-11-01 22:26:29', 'chongl', '2013-01-14 11:26:53', 'neudeckerr', 'X', 'FY12 Notice of Termination', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('317', '2010-11-17 02:32:10', 'morganj', '2013-01-14 11:21:38', 'neudeckerr', 'X', 'MFG_M: Manufacturing - Vault', 'Site Accreditation', 'Y', 'Autodesk Vault Manufacturing');
INSERT INTO `JournalActivities` VALUES ('322', '2011-01-10 07:25:42', 'morganj', '2013-01-14 11:17:37', 'neudeckerr', 'X', 'HORZ_Vault', 'Site Accreditation', 'Y', 'Autodesk Vault Workgroup, Autodesk Vault Collaboration');
INSERT INTO `JournalActivities` VALUES ('318', '2010-11-18 02:44:49', 'morganj', '2010-11-18 02:44:49', 'morganj', 'A', 'Site ID added to WAM record', 'Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('319', '2010-12-07 02:42:01', 'morganj', '2017-04-21 09:12:18', 'nbuckberry@engageglobalsolutions.com', 'X', 'EMEA FY12 Renewed', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('320', '2010-12-07 02:42:20', 'morganj', '2017-04-21 09:12:33', 'nbuckberry@engageglobalsolutions.com', 'X', 'EMEA FY12 Termination Notice', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('321', '2010-12-07 02:42:33', 'morganj', '2017-04-21 09:12:25', 'nbuckberry@engageglobalsolutions.com', 'X', 'EMEA FY12 Terminated', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('323', '2011-01-24 08:24:58', 'buckberr', '2011-01-24 08:24:58', 'buckberr', 'A', 'e-learning authorized ATC', 'Site Attribute', 'Y', 'ATC site authorized to provide Authorized e-learning Courses');
INSERT INTO `JournalActivities` VALUES ('324', '2011-01-24 08:26:27', 'buckberr', '2011-01-24 08:26:27', 'buckberr', 'A', 'e-learning ATC application rejected', 'Site Journal Entry', '', 'to note when and why an ATC has had an application for Site e-learning authorization rejected');
INSERT INTO `JournalActivities` VALUES ('325', '2011-01-24 08:29:51', 'buckberr', '2017-07-04 05:10:07', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC AEC COREXV', 'Site Accreditation', 'Y', 'AEC core and cross-vertical product - \r\nExcludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('326', '2011-01-24 08:30:35', 'buckberr', '2017-07-04 05:10:22', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC AEC RSAPRO', 'Site Accreditation', 'Y', 'AEC Robot Analysis Professional - \r\nSeparate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('327', '2011-01-24 08:31:10', 'buckberr', '2017-07-04 05:10:15', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC AEC LANDX', 'Site Accreditation', 'Y', 'AEC LandXplorer/Infrastructure Modeler - \r\nSeparate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('328', '2011-01-24 08:31:58', 'buckberr', '2017-07-04 05:10:37', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC AEC XV Plant', 'Site Accreditation', 'Y', 'AEC Plant and Plant Design Suite - \r\nSeparate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('329', '2011-01-24 08:32:30', 'buckberr', '2017-07-04 05:11:34', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MFG COREXV', 'Site Accreditation', 'Y', 'MFG core and cross-vertical product - \r\nExcludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('330', '2011-01-24 08:33:11', 'buckberr', '2017-07-04 05:11:21', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MFG ASPRO', 'Site Accreditation', 'Y', 'MFG Algor / Simulation - \r\nSeparate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('331', '2011-01-24 08:33:53', 'buckberr', '2017-07-04 05:11:40', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MFG MOLD', 'Site Accreditation', 'Y', 'MFG Moldflow - \r\nSeparate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('332', '2011-01-24 08:34:47', 'buckberr', '2017-07-04 05:11:47', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MFG PSTRMP', 'Site Accreditation', 'Y', 'MFG Product Stream Pro Office - \r\nSeparate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('333', '2011-01-24 08:35:25', 'buckberr', '2017-07-04 05:11:54', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MFG SURFST', 'Site Accreditation', 'Y', 'MFG Alias Surface - \r\nSeparate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('334', '2011-01-24 08:36:07', 'buckberr', '2017-07-04 05:11:28', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MFG AUTOST', 'Site Accreditation', 'Y', 'MFG Alias Auto - \r\nSeparate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('335', '2011-01-24 08:37:10', 'buckberr', '2017-07-04 05:12:05', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MFG XV Plant', 'Site Accreditation', 'Y', 'MFG Plant and Plant Design Suite - \r\nSeparate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('336', '2011-01-24 08:37:50', 'buckberr', '2017-07-04 05:11:01', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MED COREXV', 'Site Accreditation', 'Y', 'M&E core and cross-vertical product - \r\nExcludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('337', '2011-01-24 08:38:57', 'buckberr', '2017-07-04 05:11:09', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MED SMFMAC', 'Site Accreditation', 'Y', 'M&E Smoke for Mac - \r\nSeparate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('338', '2011-01-24 08:39:45', 'buckberr', '2017-07-04 05:11:15', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC MED XV Plant', 'Site Accreditation', 'Y', 'M&E Plant and Plant Design Suite - \r\nSeparate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('339', '2011-01-24 08:40:36', 'buckberr', '2017-07-04 05:10:44', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC CF CORE', 'Site Accreditation', 'Y', 'Creative Finishing - \r\nall Creative Finishing products');
INSERT INTO `JournalActivities` VALUES ('340', '2011-01-24 08:41:16', 'buckberr', '2017-07-04 05:10:53', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC HRZ CORE', 'Site Accreditation', 'Y', 'Horizontal - \r\nall Horizontal products');
INSERT INTO `JournalActivities` VALUES ('341', '2011-01-24 09:15:23', 'buckberr', '2011-01-24 09:15:23', 'buckberr', 'A', 'Cancelled ATC serial No. MED', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('342', '2011-01-24 09:15:52', 'buckberr', '2011-01-24 09:15:52', 'buckberr', 'A', 'Cancelled ATC serial No. DSG', 'Site Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('343', '2011-02-04 05:35:54', 'morganj', '2011-02-04 05:37:16', 'morganj', 'X', 'EMFY12ATCAGREE', 'Organization Journal Entry', '', 'EMEA FY12 ATC Agreement Active');
INSERT INTO `JournalActivities` VALUES ('344', '2011-02-04 05:37:42', 'morganj', '2017-04-21 09:12:11', 'nbuckberry@engageglobalsolutions.com', 'X', 'EMEA FY12 ATC Agreement Active', 'Organization Journal Entry', '', 'EMEA FY12 ATC Agreement Active');
INSERT INTO `JournalActivities` VALUES ('345', '2011-02-04 05:38:01', 'morganj', '2011-02-04 05:38:01', 'morganj', 'A', 'EMEA FY12 ATC Agreement Active', 'Site Journal Entry', '', 'EMEA FY12 ATC Agreement Active');
INSERT INTO `JournalActivities` VALUES ('346', '2011-02-04 07:31:38', 'buckberr', '2017-07-04 05:10:28', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY12 ATC AEC TOPOBS', 'Site Accreditation', 'Y', 'AEC Topobase -\r\n Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('347', '2011-06-21 05:00:07', '41118', '2011-06-21 05:00:07', '41118', 'A', 'A_Correspondence', 'VAR Site Attribute', '', 'Correspondence note : A Pilot for Asif to see if he can use this tracking feature in PDB');
INSERT INTO `JournalActivities` VALUES ('348', '2011-09-30 10:19:32', 't_barrw', '2011-09-30 10:19:32', 't_barrw', 'A', 'Autodesk HRGED Site Id', 'VAR Site Attribute', 'Y', 'Used to link to the HR database');
INSERT INTO `JournalActivities` VALUES ('349', '2011-09-30 10:20:13', 't_barrw', '2011-09-30 10:20:13', 't_barrw', 'A', 'Autodesk HR Employee Id', 'VAR Contact Attribute', 'Y', 'Used to link to HR contacts');
INSERT INTO `JournalActivities` VALUES ('350', '2011-10-06 13:57:52', 't_barrw', '2011-10-06 13:58:19', 't_barrw', 'A', 'EIDM GUID', 'VAR Contact Attribute', '', 'Link to the EIDM system');
INSERT INTO `JournalActivities` VALUES ('351', '2011-11-07 23:30:45', 'chongl', '2017-04-21 09:11:50', 'nbuckberry@engageglobalsolutions.com', 'X', 'APAC FY13 Activated', 'Organization Journal Entry', 'Y', 'APAC FY13 Activated (new ATC)');
INSERT INTO `JournalActivities` VALUES ('352', '2011-11-07 23:32:14', 'chongl', '2017-04-21 09:11:56', 'nbuckberry@engageglobalsolutions.com', 'X', 'APAC FY13 Renewed', 'Organization Journal Entry', 'Y', 'APAC FY13 Renewed');
INSERT INTO `JournalActivities` VALUES ('353', '2011-11-07 23:32:56', 'chongl', '2017-04-21 09:12:02', 'nbuckberry@engageglobalsolutions.com', 'X', 'APAC FY13 Terminated', 'Organization Journal Entry', 'Y', 'APAC FY13 Terminated');
INSERT INTO `JournalActivities` VALUES ('354', '2011-11-07 23:34:11', 'chongl', '2013-10-30 07:14:21', 'buckberr', 'X', 'FY13 Activated', 'Site Journal Entry', 'Y', 'FY13 Activated');
INSERT INTO `JournalActivities` VALUES ('355', '2011-11-07 23:34:55', 'chongl', '2013-10-30 07:14:37', 'buckberr', 'X', 'FY13 Renewed', 'Site Journal Entry', 'Y', 'FY13 Renewed');
INSERT INTO `JournalActivities` VALUES ('356', '2011-11-07 23:35:20', 'chongl', '2013-10-30 07:15:19', 'buckberr', 'X', 'FY13 Terminated', 'Site Journal Entry', 'Y', 'FY13 Terminated');
INSERT INTO `JournalActivities` VALUES ('357', '2011-12-15 03:27:20', 'buckberr', '2017-07-04 05:12:13', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC AEC COREXV', 'Site Accreditation', 'Y', 'AEC core and cross-vertical product - Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('358', '2011-12-15 03:28:41', 'buckberr', '2017-07-04 05:12:27', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC AEC INFRSTR MODLR', 'Site Accreditation', 'Y', 'AEC Infrastructure Modeler (previously  LandXplorer) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('359', '2011-12-15 03:29:57', 'buckberr', '2017-07-04 05:12:32', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC AEC RSAPRO', 'Site Accreditation', 'Y', 'AEC Robot Analysis Professional - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('360', '2011-12-15 03:31:14', 'buckberr', '2017-07-04 05:12:19', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC AEC INFRSTR MAP SVR', 'Site Accreditation', 'Y', 'AEC Infrastructure Map Server - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('361', '2011-12-15 03:32:27', 'buckberr', '2017-07-04 05:12:38', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC AEC XV Plant', 'Site Accreditation', 'Y', 'AEC Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('362', '2011-12-15 03:33:28', 'buckberr', '2017-07-04 05:12:45', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC CF CORE', 'Site Accreditation', 'Y', 'Creative Finishing - all Creative Finishing products');
INSERT INTO `JournalActivities` VALUES ('363', '2011-12-15 03:34:06', 'buckberr', '2017-07-04 05:12:55', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC HRZ CORE', 'Site Accreditation', 'Y', 'Horizontal - all Horizontal products');
INSERT INTO `JournalActivities` VALUES ('364', '2011-12-15 03:34:47', 'buckberr', '2017-07-04 05:13:02', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC MED COREXV', 'Site Accreditation', 'Y', 'M&E core and cross-vertical product - Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('365', '2011-12-15 03:35:37', 'buckberr', '2011-12-15 03:36:13', 'buckberr', 'X', 'FY13 ATC MED SMFMAC', 'Site Attribute', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('366', '2011-12-15 03:36:38', 'buckberr', '2017-07-04 05:13:08', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC MED SMFMAC', 'Site Accreditation', 'Y', 'M&E Smoke for Mac - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('367', '2011-12-15 03:37:30', 'buckberr', '2017-07-04 05:13:13', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC MED XV Plant', 'Site Accreditation', 'Y', 'M&E Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('368', '2011-12-15 03:38:46', 'buckberr', '2017-07-04 05:13:20', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC MFG ALIASAUTO', 'Site Accreditation', 'Y', 'MFG Alias Automotive - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('369', '2011-12-15 03:39:43', 'buckberr', '2017-07-04 05:13:35', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC MFG COREXV', 'Site Accreditation', 'Y', 'MFG core and cross-vertical product - Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('370', '2011-12-15 03:40:54', 'buckberr', '2017-07-04 05:13:48', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC MFG SIMULATION', 'Site Accreditation', 'Y', 'MFG Simulation (previously Algor) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('371', '2011-12-15 03:42:04', 'buckberr', '2017-07-04 05:13:41', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC MFG MOLD', 'Site Accreditation', 'Y', 'MFG Moldflow - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('372', '2011-12-15 03:42:52', 'buckberr', '2017-07-04 05:13:28', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC MFG ALIASSURFACE', 'Site Accreditation', 'Y', 'MFG Alias Surface - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('373', '2011-12-15 03:44:07', 'buckberr', '2017-07-04 05:13:55', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY13 ATC MFG XV Plant', 'Site Accreditation', 'Y', 'MFG Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('374', '2011-12-22 08:32:22', 'buckberr', '2017-04-21 09:12:39', 'nbuckberry@engageglobalsolutions.com', 'X', 'EMEA FY13 ATC Agreement Active', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('375', '2011-12-22 08:32:57', 'buckberr', '2017-04-21 09:13:00', 'nbuckberry@engageglobalsolutions.com', 'X', 'EMEA FY13 Termination Notice', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('376', '2011-12-22 08:33:23', 'buckberr', '2017-04-21 09:12:47', 'nbuckberry@engageglobalsolutions.com', 'X', 'EMEA FY13 Renewed', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('377', '2011-12-22 08:33:54', 'buckberr', '2017-04-21 09:12:53', 'nbuckberry@engageglobalsolutions.com', 'X', 'EMEA FY13 Terminated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('378', '2012-01-12 07:10:16', '41118', '2012-01-12 07:10:16', '41118', 'A', 'EIDM_Login_Name', 'VAR Contact Attribute', '', 'SSO or EIdM Login username');
INSERT INTO `JournalActivities` VALUES ('379', '2012-01-12 07:12:51', '41118', '2012-01-12 07:12:51', '41118', 'A', 'EIdM_language', 'VAR Contact Attribute', '', 'EIdM language selection by user');
INSERT INTO `JournalActivities` VALUES ('380', '2012-01-12 07:13:47', '41118', '2012-01-12 07:13:47', '41118', 'A', 'EIdM_country', 'VAR Contact Attribute', '', 'Country selected by EIdM user');
INSERT INTO `JournalActivities` VALUES ('381', '2012-02-15 10:03:05', 'buckberr', '2012-02-15 10:04:52', 'buckberr', 'A', 'ATC evals per Workstation count FY13', 'Site Journal Entry', 'Y', 'the FY13 annual number of surveys required per workstation for ATC performance targets as reported in evaluation system performance reports; this number overrides the country default');
INSERT INTO `JournalActivities` VALUES ('382', '2012-05-15 08:42:59', '41118', '2012-05-15 08:45:09', '41118', 'A', 'Saba Reporting Privileges', 'Contact Security Role', 'Y', 'Autodesk Staff Only : set Split Domain = World Privileges and Home Domain = Internal Basic Priveleges in the Internal Domain');
INSERT INTO `JournalActivities` VALUES ('383', '2012-11-06 03:39:07', 'buckberr', '2012-11-06 03:39:07', 'buckberr', 'A', 'General Contact Note', 'Contact Journal Entry', '', 'general notes about non-instructor contacts');
INSERT INTO `JournalActivities` VALUES ('384', '2012-11-22 06:59:10', 'buckberr', '2012-11-22 06:59:38', 'buckberr', 'A', 'ATC evals per Workstation count FY14', 'Site Journal Entry', 'Y', 'the FY14 annual number of surveys required per workstation for ATC performance targets as reported in evaluation system performance reports; this number overrides the country default');
INSERT INTO `JournalActivities` VALUES ('385', '2012-11-23 04:28:02', 'buckberr', '2014-05-23 03:25:48', 'norman.buckberry@autodesk.com', 'A', 'A3P (FY13) and Academic ATC (FY14)', 'Site Attribute', 'Y', 'set this flag if the site is accredited as A3P or, in FY14 an Academic ATC');
INSERT INTO `JournalActivities` VALUES ('386', '2013-01-10 07:56:55', 'neudeckerr', '2017-04-21 09:13:27', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 Renewed', 'Organization Journal Entry', 'Y', 'Global indicator for ATC Renewal');
INSERT INTO `JournalActivities` VALUES ('387', '2013-01-10 07:58:11', 'neudeckerr', '2017-04-21 09:09:21', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 Renewed', 'Site Journal Entry', 'Y', 'Global indicator for ATC Renewal');
INSERT INTO `JournalActivities` VALUES ('388', '2013-02-04 10:39:57', 'neudeckerr', '2017-04-21 09:13:38', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 Terminated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('389', '2013-02-04 10:40:24', 'neudeckerr', '2017-04-21 09:13:11', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 Activated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('390', '2013-02-04 10:40:41', 'neudeckerr', '2017-04-21 09:13:46', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 Termination Notice', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('391', '2013-02-04 10:44:17', 'neudeckerr', '2017-04-21 09:08:58', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 Activated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('392', '2013-02-04 10:44:37', 'neudeckerr', '2017-04-21 09:09:31', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 Terminated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('393', '2013-02-04 10:47:37', 'neudeckerr', '2017-07-04 05:14:07', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC AEC COREXV', 'Site Accreditation', 'Y', 'AEC core and cross-vertical product - Excludes all products requiring separate authorisation.');
INSERT INTO `JournalActivities` VALUES ('394', '2013-02-04 10:49:19', 'neudeckerr', '2017-07-04 05:14:26', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC AEC INFRSTR MAP SVR', 'Site Accreditation', 'Y', 'AEC Infrastructure Map Server - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('395', '2013-02-08 04:38:47', 'neudeckerr', '2017-07-04 05:14:20', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC AEC INFRAWORKS', 'Site Accreditation', 'Y', 'AEC Infraworks (previously Infrastructure Modeler & LandXplorer) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('396', '2013-02-08 04:48:48', 'neudeckerr', '2017-07-04 05:14:32', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC AEC RSAPRO', 'Site Accreditation', 'Y', 'AEC Robot Analysis Professional - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('397', '2013-02-08 04:50:16', 'neudeckerr', '2017-07-04 05:14:38', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC AEC XV Plant', 'Site Accreditation', 'Y', 'AEC Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('398', '2013-02-08 04:51:34', 'neudeckerr', '2017-07-04 05:14:45', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC CF CORE', 'Site Accreditation', 'Y', 'Creative Finishing - all Creative Finishing products');
INSERT INTO `JournalActivities` VALUES ('399', '2013-02-08 04:52:11', 'neudeckerr', '2017-07-04 05:14:53', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC HRZ CORE', 'Site Accreditation', 'Y', 'Horizontal - all Horizontal products');
INSERT INTO `JournalActivities` VALUES ('400', '2013-02-08 04:53:19', 'neudeckerr', '2017-07-04 05:15:03', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MED COREXV', 'Site Accreditation', 'Y', 'M&E core and cross-vertical product - Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('401', '2013-02-08 04:54:21', 'neudeckerr', '2017-07-04 05:15:09', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MED SMFMAC', 'Site Accreditation', 'Y', 'M&E Smoke for Mac - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('402', '2013-02-08 04:55:19', 'neudeckerr', '2017-07-04 05:15:22', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MED XV Plant', 'Site Accreditation', 'Y', 'M&E Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('403', '2013-02-08 04:56:16', 'neudeckerr', '2017-07-04 05:15:35', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MFG ALIASAUTO', 'Site Accreditation', 'Y', 'MFG Alias Automotive - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('404', '2013-02-08 04:57:09', 'neudeckerr', '2017-07-04 05:15:43', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MFG ALIASSURFACE', 'Site Accreditation', 'Y', 'MFG Alias Surface - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('405', '2013-02-08 04:58:27', 'neudeckerr', '2017-07-04 05:15:52', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MFG COREXV', 'Site Accreditation', 'Y', 'MFG core and cross-vertical product. Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('406', '2013-02-08 04:59:31', 'neudeckerr', '2017-07-04 05:15:59', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MFG MOLD', 'Site Accreditation', 'Y', 'MFG Simulation Moldflow - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('407', '2013-02-08 05:00:34', 'neudeckerr', '2017-07-04 05:16:06', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MFG SIMULATION', 'Site Accreditation', 'Y', 'MFG Simulation (previously Algor) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('408', '2013-02-08 05:31:57', 'neudeckerr', '2017-07-04 05:16:18', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MFG XV Plant', 'Site Accreditation', 'Y', 'MFG Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('409', '2013-03-11 09:56:39', 'buckberr', '2013-03-11 09:57:25', 'buckberr', 'A', 'ATC Software Subscription Contract Number', 'Site Journal Entry', '', 'to record subscription contract numbers for ATC sites from FY14 onwards');
INSERT INTO `JournalActivities` VALUES ('410', '2013-04-07 07:42:18', 'neudeckerr', '2017-04-21 09:13:19', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC Agreement Active', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('411', '2013-04-07 07:42:51', 'neudeckerr', '2017-04-21 09:09:11', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC Agreement Active', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('412', '2013-04-10 05:56:57', 'buckberr', '2017-07-04 05:14:13', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC AEC FAB', 'Site Accreditation', 'Y', 'FY14 Fabrication products - specialist authorisation (CADmep, ESTmep, CAMDuct)');
INSERT INTO `JournalActivities` VALUES ('413', '2013-04-10 06:18:10', 'buckberr', '2017-07-04 05:16:12', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MFG VRED', 'Site Accreditation', 'Y', 'VRED, VRED Design and VRED Professional - specialist accreditation');
INSERT INTO `JournalActivities` VALUES ('414', '2013-05-20 04:29:38', 'buckberr', '2013-05-20 04:29:38', 'buckberr', 'A', 'Fabrication product instructor approval request denied', 'Contact Journal Entry', '', 'enter the date the request was denied and the name of the interviewer who denied the application');
INSERT INTO `JournalActivities` VALUES ('415', '2013-10-30 07:16:45', 'buckberr', '2017-04-21 09:09:40', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 Activated', 'Site Journal Entry', 'Y', 'FY15 Activated');
INSERT INTO `JournalActivities` VALUES ('416', '2013-10-30 07:17:32', 'buckberr', '2017-04-21 09:10:00', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 Renewed', 'Site Journal Entry', 'Y', 'FY15 Renewed');
INSERT INTO `JournalActivities` VALUES ('417', '2013-10-30 07:18:12', 'buckberr', '2017-04-21 09:10:09', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 Terminated', 'Site Journal Entry', 'Y', 'FY15 Terminated');
INSERT INTO `JournalActivities` VALUES ('418', '2014-03-03 11:50:37', 'norman.buckberry@autodesk.com', '2017-07-04 05:48:39', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC AEC COREXV', 'Site Accreditation', 'Y', 'AEC core and cross-vertical product - Excludes all products requiring separate authorisation.');
INSERT INTO `JournalActivities` VALUES ('419', '2014-03-03 11:51:31', 'norman.buckberry@autodesk.com', '2017-07-04 05:48:45', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC AEC FAB', 'Site Accreditation', 'Y', 'FY15 Fabrication products - specialist authorisation (CADmep, ESTmep, CAMDuct)');
INSERT INTO `JournalActivities` VALUES ('420', '2014-03-03 11:52:06', 'norman.buckberry@autodesk.com', '2017-07-04 05:48:52', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC AEC INFRAWORKS', 'Site Accreditation', 'Y', 'AEC Infraworks (previously Infrastructure Modeler & LandXplorer) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('421', '2014-03-03 11:52:34', 'norman.buckberry@autodesk.com', '2017-07-04 05:48:59', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC AEC INFRSTR MAP SVR', 'Site Accreditation', 'Y', 'AEC Infrastructure Map Server - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('422', '2014-03-03 11:53:03', 'norman.buckberry@autodesk.com', '2017-07-04 05:49:05', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC AEC RSAPRO', 'Site Accreditation', 'Y', 'AEC Robot Analysis Professional - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('423', '2014-03-03 11:53:29', 'norman.buckberry@autodesk.com', '2017-07-04 05:49:11', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC AEC XV Plant', 'Site Accreditation', 'Y', 'AEC Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('424', '2014-03-03 11:53:55', 'norman.buckberry@autodesk.com', '2017-07-04 05:49:19', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC CF CORE', 'Site Accreditation', 'Y', 'Creative Finishing - all Creative Finishing products');
INSERT INTO `JournalActivities` VALUES ('425', '2014-03-03 11:54:24', 'norman.buckberry@autodesk.com', '2017-07-04 05:49:27', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC HRZ CORE', 'Site Accreditation', 'Y', 'Horizontal - all Horizontal products');
INSERT INTO `JournalActivities` VALUES ('426', '2014-03-03 11:54:50', 'norman.buckberry@autodesk.com', '2017-07-04 05:49:33', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC MED COREXV', 'Site Accreditation', 'Y', 'M&E core and cross-vertical product - Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('427', '2014-03-03 11:55:43', 'norman.buckberry@autodesk.com', '2017-07-04 05:49:40', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC MED SMFMAC', 'Site Accreditation', 'Y', 'M&E Smoke for Mac - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('428', '2014-03-03 11:56:12', 'norman.buckberry@autodesk.com', '2017-07-04 05:15:29', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY14 ATC MED XV Plant', 'Site Accreditation', 'Y', 'M&E Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('429', '2014-03-03 11:56:37', 'norman.buckberry@autodesk.com', '2017-07-04 05:49:47', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC MFG ALIASAUTO', 'Site Accreditation', 'Y', 'MFG Alias Automotive - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('430', '2014-03-03 11:57:04', 'norman.buckberry@autodesk.com', '2017-07-04 05:49:55', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC MFG ALIASSURFACE', 'Site Accreditation', 'Y', 'MFG Alias Surface - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('431', '2014-03-03 11:57:24', 'norman.buckberry@autodesk.com', '2017-07-04 05:50:03', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC MFG COREXV', 'Site Accreditation', 'Y', 'MFG core and cross-vertical product. Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('432', '2014-03-03 11:57:52', 'norman.buckberry@autodesk.com', '2017-07-04 05:50:10', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC MFG MOLD', 'Site Accreditation', 'Y', 'MFG Simulation Moldflow - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('433', '2014-03-03 11:58:15', 'norman.buckberry@autodesk.com', '2017-07-04 05:50:16', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC MFG SIMULATION', 'Site Accreditation', 'Y', 'MFG Simulation (previously Algor) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('434', '2014-03-03 11:58:39', 'norman.buckberry@autodesk.com', '2017-07-04 05:50:28', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC MFG VRED', 'Site Accreditation', 'Y', 'VRED, VRED Design and VRED Professional - specialist accreditation');
INSERT INTO `JournalActivities` VALUES ('435', '2014-03-03 11:59:09', 'norman.buckberry@autodesk.com', '2017-07-04 05:50:36', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC MFG XV Plant', 'Site Accreditation', 'Y', 'MFG Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('436', '2014-03-03 12:04:53', 'norman.buckberry@autodesk.com', '2017-07-04 05:48:31', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC AEC BIM 360 Field Glue', 'Site Accreditation', 'Y', 'AEC BIM 360 Field Glue - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('437', '2014-03-07 11:18:47', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:14:47', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 Renewed', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('438', '2014-03-07 11:19:21', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:14:55', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 Terminated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('439', '2014-03-07 11:19:51', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:14:30', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 Activated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('440', '2014-03-07 11:20:25', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:15:03', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 Termination Notice', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('441', '2014-07-04 04:40:37', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:14:38', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC Agreement Active', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('442', '2014-07-04 04:44:51', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:09:50', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 ATC Agreement Active', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('443', '2014-08-28 03:56:52', 'norman.buckberry@autodesk.com', '2014-08-28 03:56:52', 'norman.buckberry@autodesk.com', 'A', 'ATC quarterly ORG review call', 'Organization Journal Entry', '', 'general activity to record ATC calls and specific notes about the call');
INSERT INTO `JournalActivities` VALUES ('444', '2014-08-28 03:57:37', 'norman.buckberry@autodesk.com', '2014-08-28 03:57:37', 'norman.buckberry@autodesk.com', 'A', 'AAP quarterly ORG review call', 'Organization Journal Entry', '', 'general activity to record AAP calls and specific notes about the call');
INSERT INTO `JournalActivities` VALUES ('445', '2014-08-28 03:58:51', 'norman.buckberry@autodesk.com', '2014-08-28 03:58:51', 'norman.buckberry@autodesk.com', 'A', 'ATC/AAP quarterly ORG review call', 'Organization Journal Entry', '', 'general activity to record ATC/AAP generic calls and specific notes about the call');
INSERT INTO `JournalActivities` VALUES ('446', '2014-08-28 04:00:03', 'norman.buckberry@autodesk.com', '2014-08-28 04:03:38', 'norman.buckberry@autodesk.com', 'A', 'ATC/AAP visit', 'Organization Journal Entry', '', 'general activity to record ATC/AAP visits and specific notes about the visit');
INSERT INTO `JournalActivities` VALUES ('447', '2014-08-28 04:02:18', 'norman.buckberry@autodesk.com', '2014-08-28 04:03:06', 'norman.buckberry@autodesk.com', 'A', 'ATC/AAP ad hoc call', 'Organization Journal Entry', '', 'general activity to record ATC/AAP calls and specific notes about the calls');
INSERT INTO `JournalActivities` VALUES ('448', '2014-08-28 04:04:32', 'norman.buckberry@autodesk.com', '2014-08-28 04:04:32', 'norman.buckberry@autodesk.com', 'A', 'ATC/AAP audit visit', 'Organization Journal Entry', '', 'activity to record ATC/AAP audits and specific notes about the audits');
INSERT INTO `JournalActivities` VALUES ('449', '2014-08-28 04:09:10', 'norman.buckberry@autodesk.com', '2014-08-28 04:09:10', 'norman.buckberry@autodesk.com', 'A', 'ATC/AAP quarterly Site review call', 'Site Journal Entry', '', 'general activity to record ATC site calls and specific notes about the call');
INSERT INTO `JournalActivities` VALUES ('450', '2014-08-28 04:09:39', 'norman.buckberry@autodesk.com', '2014-08-28 04:09:39', 'norman.buckberry@autodesk.com', 'A', 'ATC/AAP visit', 'Site Journal Entry', '', 'general activity to record ATC site visits and specific notes about the visit');
INSERT INTO `JournalActivities` VALUES ('451', '2014-08-28 04:10:02', 'norman.buckberry@autodesk.com', '2014-08-28 04:10:02', 'norman.buckberry@autodesk.com', 'A', 'ATC/AAP ad hoc call', 'Site Journal Entry', '', 'general activity to record ATC/AAP site calls and specific notes about the calls');
INSERT INTO `JournalActivities` VALUES ('452', '2014-08-28 04:10:25', 'norman.buckberry@autodesk.com', '2014-08-28 04:10:25', 'norman.buckberry@autodesk.com', 'A', 'ATC/AAP audit visit', 'Site Journal Entry', '', 'activity to record ATC/AAP site audits and specific notes about the audits');
INSERT INTO `JournalActivities` VALUES ('453', '2014-09-09 13:23:45', 'jeebund@hotmail.com', '2014-09-09 13:23:45', 'jeebund@hotmail.com', 'X', 'FY15 ATC letter of Authorization', 'Site Journal Entry', 'Y', 'FY15 ATC letter of Authorization');
INSERT INTO `JournalActivities` VALUES ('454', '2014-09-09 13:25:14', 'jeebund@hotmail.com', '2014-09-09 13:25:14', 'jeebund@hotmail.com', 'X', 'FY15 AAP letter of Authorization', 'Site Journal Entry', 'Y', 'FY15 AAP letter of Authorization');
INSERT INTO `JournalActivities` VALUES ('455', '2014-11-04 07:02:07', 'robneudecker@engageglobalsolutions.com', '2014-11-04 07:02:07', 'robneudecker@engageglobalsolutions.com', 'A', 'AAP', 'Site Attribute', 'Y', 'Autodesk Academic Partner');
INSERT INTO `JournalActivities` VALUES ('456', '2014-11-04 07:04:11', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:14:22', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY15 AAP/ ATC Agreement Active', 'Organization Journal Entry', 'Y', 'Autodesk Academic Partner and/ or Authorized Training Center');
INSERT INTO `JournalActivities` VALUES ('457', '2015-01-06 06:31:26', 'robneudecker@engageglobalsolutions.com', '2016-05-12 09:43:43', 'robneudecker@engageglobalsolutions.com', 'X', 'FY16 AAP/ ATC Agreement Active', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('458', '2015-01-06 06:32:13', 'robneudecker@engageglobalsolutions.com', '2015-01-06 06:32:13', 'robneudecker@engageglobalsolutions.com', 'A', 'FY16 Activated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('459', '2015-01-06 06:32:38', 'robneudecker@engageglobalsolutions.com', '2015-01-06 06:32:38', 'robneudecker@engageglobalsolutions.com', 'A', 'FY16 ATC Agreement Active', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('460', '2015-01-06 06:33:04', 'robneudecker@engageglobalsolutions.com', '2015-01-06 06:33:04', 'robneudecker@engageglobalsolutions.com', 'A', 'FY16 Renewed', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('461', '2015-01-06 06:33:23', 'robneudecker@engageglobalsolutions.com', '2015-01-06 06:33:23', 'robneudecker@engageglobalsolutions.com', 'A', 'FY16 Terminated', 'Organization Journal Entry', '', null);
INSERT INTO `JournalActivities` VALUES ('462', '2015-01-06 06:33:50', 'robneudecker@engageglobalsolutions.com', '2015-01-06 06:33:50', 'robneudecker@engageglobalsolutions.com', 'A', 'FY16 Termination Notice', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('463', '2015-01-06 06:34:29', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:10:18', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 Activated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('464', '2015-01-06 06:34:50', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:10:27', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC Agreement Active', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('465', '2015-01-06 06:35:10', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:10:35', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 Renewed', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('466', '2015-01-06 06:35:31', 'robneudecker@engageglobalsolutions.com', '2017-04-21 09:10:44', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 Terminated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('467', '2015-02-01 23:39:43', 'aleris@nwhholdings.com', '2015-02-01 23:39:43', 'aleris@nwhholdings.com', 'X', 'FY16 ATC letter of Authorization', 'Site Journal Entry', 'Y', 'FY16 ATC letter of Authorization');
INSERT INTO `JournalActivities` VALUES ('468', '2015-02-05 05:55:52', 'mariya.popova@kazcad.kz', '2015-02-05 05:55:52', 'mariya.popova@kazcad.kz', 'X', 'FY16 AAP letter of Authorization', 'Site Journal Entry', 'Y', 'FY16 AAP letter of Authorization');
INSERT INTO `JournalActivities` VALUES ('469', '2015-05-05 06:37:56', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:52:07', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC AEC INFRAWORKS', 'Site Accreditation', 'Y', 'AEC Infraworks (previously Infrastructure Modeler & LandXplorer) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('470', '2015-05-05 07:02:46', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:51:47', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC AEC BIM 360 Field Glue', 'Site Accreditation', 'Y', 'AEC BIM 360 Field Glue - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('471', '2015-05-05 07:03:24', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:51:53', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC AEC COREXV', 'Site Accreditation', 'Y', 'AEC core and cross-vertical product - Excludes all products requiring separate authorisation.');
INSERT INTO `JournalActivities` VALUES ('472', '2015-05-05 07:04:13', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:51:59', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC AEC FAB', 'Site Accreditation', 'Y', 'FY16 Fabrication products - specialist authorisation (CADmep, ESTmep, CAMDuct)');
INSERT INTO `JournalActivities` VALUES ('473', '2015-05-05 07:04:55', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:52:14', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC AEC INFRSTR MAP SVR', 'Site Accreditation', 'Y', 'AEC Infrastructure Map Server - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('474', '2015-05-05 07:05:29', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:52:21', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC AEC RSAPRO', 'Site Accreditation', 'Y', 'AEC Robot Analysis Professional - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('475', '2015-05-05 07:06:26', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:52:30', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC AEC XV Plant', 'Site Accreditation', 'Y', 'AEC Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('476', '2015-05-05 07:07:15', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:52:37', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC CF CORE', 'Site Accreditation', 'Y', 'Creative Finishing - all Creative Finishing products');
INSERT INTO `JournalActivities` VALUES ('477', '2015-05-05 07:08:01', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:52:45', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC HRZ CORE', 'Site Accreditation', 'Y', 'Horizontal - all Horizontal products');
INSERT INTO `JournalActivities` VALUES ('478', '2015-05-05 07:08:38', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:52:54', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC MED COREXV', 'Site Accreditation', 'Y', 'M&E core and cross-vertical product - Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('479', '2015-05-05 07:09:30', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:53:03', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC MED SMFMAC', 'Site Accreditation', 'Y', 'M&E Smoke for Mac - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('480', '2015-05-05 07:10:05', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:53:11', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC MFG ALIASAUTO', 'Site Accreditation', 'Y', 'MFG Alias Automotive - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('481', '2015-05-05 07:10:47', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:53:19', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC MFG ALIASSURFACE', 'Site Accreditation', 'Y', 'MFG Alias Surface - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('482', '2015-05-05 07:11:31', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:53:27', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC MFG COREXV', 'Site Accreditation', 'Y', 'MFG core and cross-vertical product. Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('483', '2015-05-05 07:12:23', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:53:37', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC MFG MOLD', 'Site Accreditation', 'Y', 'MFG Simulation Moldflow - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('484', '2015-05-05 07:12:56', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:53:44', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC MFG SIMULATION', 'Site Accreditation', 'Y', 'MFG Simulation (previously Algor) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('485', '2015-05-05 07:13:31', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:53:52', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC MFG VRED', 'Site Accreditation', 'Y', 'VRED, VRED Design and VRED Professional - specialist accreditation');
INSERT INTO `JournalActivities` VALUES ('486', '2015-05-05 07:14:09', 'robneudecker@engageglobalsolutions.com', '2017-07-04 05:53:59', 'nbuckberry@engageglobalsolutions.com', 'X', 'FY16 ATC MFG XV Plant', 'Site Accreditation', 'Y', 'MFG Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('487', '2016-01-13 07:53:32', 'robneudecker@engageglobalsolutions.com', '2016-01-13 07:53:32', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 Activated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('488', '2016-01-13 07:53:56', 'robneudecker@engageglobalsolutions.com', '2016-01-13 07:53:56', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 Renewed', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('489', '2016-01-13 07:54:15', 'robneudecker@engageglobalsolutions.com', '2016-01-13 07:54:15', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 Terminated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('490', '2016-01-13 07:54:36', 'robneudecker@engageglobalsolutions.com', '2016-05-12 09:43:28', 'robneudecker@engageglobalsolutions.com', 'X', 'FY17 AAP/ATC Agreement Active', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('491', '2016-01-13 07:55:07', 'robneudecker@engageglobalsolutions.com', '2016-01-13 07:55:07', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 Activated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('492', '2016-01-13 07:55:33', 'robneudecker@engageglobalsolutions.com', '2016-01-13 07:55:33', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 Renewed', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('493', '2016-01-13 07:55:49', 'robneudecker@engageglobalsolutions.com', '2016-01-13 07:55:49', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 Terminated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('494', '2016-01-13 07:56:14', 'robneudecker@engageglobalsolutions.com', '2016-03-18 05:07:54', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 AAP/ ATC Agreement Active', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('495', '2016-02-02 19:56:19', 'mnchai@eatc.co.kr', '2016-02-02 19:56:19', 'mnchai@eatc.co.kr', 'X', 'FY17 AAP letter of Authorization', 'Site Journal Entry', 'Y', 'FY17 AAP letter of Authorization');
INSERT INTO `JournalActivities` VALUES ('496', '2016-02-03 06:33:33', 'robneudecker@engageglobalsolutions.com', '2016-02-03 06:33:33', 'robneudecker@engageglobalsolutions.com', 'X', 'FY17 ATC letter of Authorization', 'Site Journal Entry', 'Y', 'FY17 ATC letter of Authorization');
INSERT INTO `JournalActivities` VALUES ('497', '2016-04-26 09:53:30', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:53:30', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC AEC BIM 360 Field Glue', 'Site Accreditation', 'Y', 'AEC BIM 360 Field Glue - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('498', '2016-04-26 09:54:20', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:54:20', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC AEC COREXV', 'Site Accreditation', 'Y', 'AEC core and cross-vertical product - Excludes all products requiring separate authorisation.');
INSERT INTO `JournalActivities` VALUES ('499', '2016-04-26 09:54:54', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:54:54', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC AEC FAB', 'Site Accreditation', 'Y', 'FY16 Fabrication products - specialist authorisation (CADmep, ESTmep, CAMDuct)');
INSERT INTO `JournalActivities` VALUES ('500', '2016-04-26 09:55:24', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:55:24', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC AEC INFRAWORKS', 'Site Accreditation', 'Y', 'AEC Infraworks (previously Infrastructure Modeler & LandXplorer) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('501', '2016-04-26 09:55:57', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:55:57', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC AEC INFRSTR MAP SVR', 'Site Accreditation', 'Y', 'AEC Infrastructure Map Server - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('502', '2016-04-26 09:56:28', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:56:28', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC AEC RSAPRO', 'Site Accreditation', 'Y', 'AEC Robot Analysis Professional - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('503', '2016-04-26 09:56:59', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:56:59', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC AEC XV Plant', 'Site Accreditation', 'Y', 'AEC Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('504', '2016-04-26 09:57:34', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:57:34', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC CF CORE', 'Site Accreditation', 'Y', 'Creative Finishing - all Creative Finishing products');
INSERT INTO `JournalActivities` VALUES ('505', '2016-04-26 09:58:04', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:58:04', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC HRZ CORE', 'Site Accreditation', 'Y', 'Horizontal - all Horizontal products');
INSERT INTO `JournalActivities` VALUES ('506', '2016-04-26 09:58:38', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:58:38', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MED COREXV', 'Site Accreditation', 'Y', 'M&E core and cross-vertical product - Excludes all products requiring separate authorisation');
INSERT INTO `JournalActivities` VALUES ('507', '2016-04-26 09:59:11', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:59:11', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MED SMFMAC', 'Site Accreditation', 'Y', 'M&E Smoke for Mac - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('508', '2016-04-26 09:59:43', 'robneudecker@engageglobalsolutions.com', '2016-04-26 09:59:43', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MFG ALIASAUTO', 'Site Accreditation', 'Y', 'MFG Alias Automotive - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('509', '2016-04-26 10:05:02', 'robneudecker@engageglobalsolutions.com', '2016-04-26 10:05:02', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MFG ALIASSURFACE', 'Site Accreditation', 'Y', 'MFG Alias Surface - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('510', '2016-04-26 10:05:36', 'robneudecker@engageglobalsolutions.com', '2016-04-26 10:05:36', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MFG COREXV', 'Site Accreditation', 'Y', 'MFG core and cross-vertical product. Excludes all products requiring separate authorisation.');
INSERT INTO `JournalActivities` VALUES ('511', '2016-04-26 10:06:12', 'robneudecker@engageglobalsolutions.com', '2016-04-26 10:06:12', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MFG MOLD', 'Site Accreditation', 'Y', 'MFG Simulation Moldflow - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('512', '2016-04-26 10:06:43', 'robneudecker@engageglobalsolutions.com', '2016-04-26 10:06:43', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MFG SIMULATION', 'Site Accreditation', 'Y', 'MFG Simulation (previously Algor) - Separate specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('513', '2016-04-26 10:07:15', 'robneudecker@engageglobalsolutions.com', '2016-04-26 10:07:15', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MFG VRED', 'Site Accreditation', 'Y', 'VRED, VRED Design and VRED Professional - specialist accreditation');
INSERT INTO `JournalActivities` VALUES ('514', '2016-04-26 10:07:45', 'robneudecker@engageglobalsolutions.com', '2016-04-26 10:07:45', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MFG XV Plant', 'Site Accreditation', 'Y', 'MFG Plant and Plant Design Suite - Separate X-Vertical specialist authorisation');
INSERT INTO `JournalActivities` VALUES ('515', '2016-09-28 10:50:42', 'robneudecker@engageglobalsolutions.com', '2016-09-28 10:55:03', 'robneudecker@engageglobalsolutions.com', 'A', 'Agreement DocuSigned', 'Organization Journal Entry', '', 'Partner Agreement has been signed using DocuSign');
INSERT INTO `JournalActivities` VALUES ('516', '2016-12-08 10:24:11', 'robneudecker@engageglobalsolutions.com', '2016-12-08 10:24:11', 'robneudecker@engageglobalsolutions.com', 'A', 'PED Contract Number', 'Site Journal Entry', 'Y', 'Contract Number for PED Serial Numbers');
INSERT INTO `JournalActivities` VALUES ('517', '2017-01-11 08:31:09', 'robneudecker@engageglobalsolutions.com', '2017-03-15 10:54:04', 'robneudecker@engageglobalsolutions.com', 'A', 'FY18 Activated & Payment Received', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('518', '2017-01-11 08:31:31', 'robneudecker@engageglobalsolutions.com', '2017-03-01 10:17:53', 'robneudecker@engageglobalsolutions.com', 'A', 'FY18 Renewed & Payment Received', 'Organization Journal Entry', 'Y', 'Add the total amount received per ORG. Example: $3,850');
INSERT INTO `JournalActivities` VALUES ('519', '2017-01-11 08:31:50', 'robneudecker@engageglobalsolutions.com', '2017-01-11 08:31:50', 'robneudecker@engageglobalsolutions.com', 'A', 'FY18 Terminated', 'Organization Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('520', '2017-01-11 08:32:41', 'robneudecker@engageglobalsolutions.com', '2017-01-11 08:34:19', 'robneudecker@engageglobalsolutions.com', 'A', 'FY18 Activated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('521', '2017-01-11 08:33:03', 'robneudecker@engageglobalsolutions.com', '2017-01-11 08:34:01', 'robneudecker@engageglobalsolutions.com', 'A', 'FY18 Renewed', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('522', '2017-01-11 08:33:28', 'robneudecker@engageglobalsolutions.com', '2017-01-11 08:33:28', 'robneudecker@engageglobalsolutions.com', 'A', 'FY18 Terminated', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('523', '2017-01-12 04:39:46', 'robneudecker@engageglobalsolutions.com', '2017-01-12 04:39:46', 'robneudecker@engageglobalsolutions.com', 'A', 'AAP Agreement Active', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('524', '2017-01-17 04:12:14', 'robneudecker@engageglobalsolutions.com', '2017-01-17 04:12:14', 'robneudecker@engageglobalsolutions.com', 'A', 'FY18 AAP/ ATC Agreement Active', 'Site Journal Entry', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('525', '2017-02-05 20:07:55', 'aleris@nwhholdings.com', '2017-02-05 20:07:55', 'aleris@nwhholdings.com', 'X', 'FY18 AAP letter of Authorization', 'Site Journal Entry', 'Y', 'FY18 AAP letter of Authorization');
INSERT INTO `JournalActivities` VALUES ('526', '2017-02-16 07:52:30', 'robneudecker@engageglobalsolutions.com', '2017-02-16 07:52:30', 'robneudecker@engageglobalsolutions.com', 'X', 'FY18 ATC letter of Authorization', 'Site Journal Entry', 'Y', 'FY18 ATC letter of Authorization');
INSERT INTO `JournalActivities` VALUES ('527', '2017-03-01 10:11:53', 'robneudecker@engageglobalsolutions.com', '2017-03-01 10:13:02', 'robneudecker@engageglobalsolutions.com', 'X', 'FY18 Renewal Fee Received', 'Organization Journal Entry', 'Y', 'Add the exact amount received for each ORG.\r\nExample: \r\n$3,850');
INSERT INTO `JournalActivities` VALUES ('528', '2017-06-05 06:45:33', 'robneudecker@engageglobalsolutions.com', '2017-06-05 06:46:27', 'robneudecker@engageglobalsolutions.com', 'A', 'FY17 ATC MFG - FUSION 360', 'Site Accreditation', 'Y', null);
INSERT INTO `JournalActivities` VALUES ('529', '2017-08-02 09:53:50', 'robneudecker@engageglobalsolutions.com', '2017-08-02 09:53:50', 'robneudecker@engageglobalsolutions.com', 'A', 'FY18 Re-Activated & Payment Received', 'Organization Journal Entry', 'Y', 'Add the total amount received per ORG e.g.: $3,850');
INSERT INTO `JournalActivities` VALUES ('530', '2017-08-02 09:55:47', 'robneudecker@engageglobalsolutions.com', '2017-08-02 09:55:47', 'robneudecker@engageglobalsolutions.com', 'A', 'FY18 Re-Activated', 'Site Journal Entry', 'Y', 'Use this if the partner was terminated due to non-payment but then made the payment later in the year.');
INSERT INTO `JournalActivities` VALUES ('531', '2017-09-01 09:45:46', 'robneudecker@engageglobalsolutions.com', '2017-09-01 09:45:46', 'robneudecker@engageglobalsolutions.com', 'A', 'ACI Evaluator', 'Contact Attribute', 'Y', 'ACI Evaluator Status');

-- ----------------------------
-- Table structure for Journals
-- ----------------------------
DROP TABLE IF EXISTS `Journals`;
CREATE TABLE `Journals` (
  `JournalId` int(8) NOT NULL AUTO_INCREMENT,
  `ParentId` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `DateAdded` datetime DEFAULT '0000-00-00 00:00:00',
  `AddedBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ActivityId` int(8) DEFAULT '0',
  `ActivityDate` datetime DEFAULT NULL,
  `Notes` text COLLATE utf8_unicode_ci,
  `DateLastAdmin` datetime DEFAULT NULL,
  `LastAdminBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` char(1) COLLATE utf8_unicode_ci DEFAULT 'A',
  `GUID` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`JournalId`),
  KEY `IDX_JOURNALS_Status` (`Status`),
  KEY `IDX_JOURNALS_ActivityId` (`ActivityId`),
  KEY `IDX_JOURNALS_ParentId` (`ParentId`),
  KEY `IDX_JOURNALS_GUID` (`GUID`),
  KEY `IDX_JOURNALS_Notes` (`Notes`(333))
) ENGINE=MyISAM AUTO_INCREMENT=112047 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Subject Codes are controlled externally';

-- ----------------------------
-- Records of Journals
-- ----------------------------
INSERT INTO `Journals` VALUES ('112043', '945219', '0000-00-00 00:00:00', 'Dist_Vinsys_A', '207', '0000-00-00 00:00:00', 'Inventor 2012, Autocad 2012 edit', '0000-00-00 00:00:00', 'neha.gupta@vinsys.in', 'X', '');
INSERT INTO `Journals` VALUES ('112045', 'OE300384', '0000-00-00 00:00:00', 'admin', '20', '0000-00-00 00:00:00', 'test', '0000-00-00 00:00:00', '2018-01-30 0:52:26', 'A', null);
INSERT INTO `Journals` VALUES ('112046', 'OE300384', '0000-00-00 00:00:00', 'admin', '20', '0000-00-00 00:00:00', 'test', '0000-00-00 00:00:00', '2018-01-30 1:19:54', 'A', null);

-- ----------------------------
-- Table structure for Layout_language
-- ----------------------------
DROP TABLE IF EXISTS `Layout_language`;
CREATE TABLE `Layout_language` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_code` varchar(100) DEFAULT NULL,
  `english` varchar(255) DEFAULT NULL,
  `bahasa` varchar(255) DEFAULT NULL,
  `chinese` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of Layout_language
-- ----------------------------
INSERT INTO `Layout_language` VALUES ('1', 'L0112', 'Please Login', 'Silahkan Login', null);
INSERT INTO `Layout_language` VALUES ('2', 'L0113', 'Reset Password', 'Kembalikan Password', null);

-- ----------------------------
-- Table structure for Layout_language_evaluation
-- ----------------------------
DROP TABLE IF EXISTS `Layout_language_evaluation`;
CREATE TABLE `Layout_language_evaluation` (
  `language_id` int(11) NOT NULL AUTO_INCREMENT,
  `language_code` varchar(100) DEFAULT NULL,
  `english` varchar(255) DEFAULT NULL,
  `chinese` varchar(255) DEFAULT NULL,
  `bahasa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`language_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of Layout_language_evaluation
-- ----------------------------
INSERT INTO `Layout_language_evaluation` VALUES ('1', 'L0112', 'Please Login', null, 'Silahkan Login');

-- ----------------------------
-- Table structure for Line_item_summary
-- ----------------------------
DROP TABLE IF EXISTS `Line_item_summary`;
CREATE TABLE `Line_item_summary` (
  `line_item_summary_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Invoice',
  `summary` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `description` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantity` decimal(20,2) DEFAULT NULL,
  `unit_price` decimal(20,2) DEFAULT NULL,
  `vat_rate` decimal(20,2) DEFAULT NULL,
  `vat_amount` decimal(20,2) DEFAULT NULL,
  `total_amount` decimal(20,2) DEFAULT NULL,
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`line_item_summary_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_type`)
) ENGINE=MyISAM AUTO_INCREMENT=123 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Line_item_summary
-- ----------------------------
INSERT INTO `Line_item_summary` VALUES ('120', 'A&P Invoice', '2', 'AS', '1.00', '1.00', '1.00', '0.00', '0.00', '', 'admin', '2018-01-30 17:03:38', 'admin', '2018-01-30 17:03:38');
INSERT INTO `Line_item_summary` VALUES ('2', 'A&P Invoice', '2', 'asdasdasd', '6.00', '6.00', '6.00', '0.00', '0.00', null, null, '0001-01-01 00:00:00', null, '0001-01-01 00:00:00');
INSERT INTO `Line_item_summary` VALUES ('116', 'VAR Invoice', 'zzzz', 'xxxxxx', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:56:55');
INSERT INTO `Line_item_summary` VALUES ('3', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - EC2', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2006-10-11 18:07:58', 'admin', '2008-03-05 04:49:54');
INSERT INTO `Line_item_summary` VALUES ('4', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC2', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2006-10-11 18:08:54', 'admin', '2008-03-05 04:48:58');
INSERT INTO `Line_item_summary` VALUES ('5', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - UK', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2006-10-11 18:09:10', 'admin', '2008-03-05 04:49:29');
INSERT INTO `Line_item_summary` VALUES ('6', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - UK', 'Fees for the Renewal of ATC Software Subscription & ATC Program Membership 2008-2009', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2006-10-11 18:09:25', 'admin', '2008-03-05 04:50:17');
INSERT INTO `Line_item_summary` VALUES ('9', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'Fees for an increase in ATC Software License Entitlements 2007-2008', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2006-10-13 07:56:53', 'admin', '2008-03-05 04:48:45');
INSERT INTO `Line_item_summary` VALUES ('12', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - UK', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-05-22 08:00:14', 'admin', '2008-03-05 04:48:32');
INSERT INTO `Line_item_summary` VALUES ('15', 'ATC Invoice', 'EMIA 2007 (FY08) ATC PROGRAM APPLICATION FOR NEW SITE - ME', 'Fees for ATC Software Subscription & ATC Program Membership 2007-2008', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-05-22 08:06:06', 'admin', '2008-03-05 04:48:19');
INSERT INTO `Line_item_summary` VALUES ('117', 'A&P Invoice', 'new', 'new', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2018-01-13 14:59:57', 'admin', '2018-01-13 14:59:57');
INSERT INTO `Line_item_summary` VALUES ('18', 'ATC Invoice', 'AMER: 2008 (FY09) Renewal', 'ATC Program Renewal Fee in the Program year 2008.', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-04 12:15:54', 'admin', '2007-09-17 11:04:31');
INSERT INTO `Line_item_summary` VALUES ('19', 'ATC Invoice', 'Apac: 2007 (FY08) Renewal ANZ/Australia', 'Program membership Fee for 2007', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-05 04:44:35', 'admin', '2008-01-09 04:14:39');
INSERT INTO `Line_item_summary` VALUES ('20', 'ATC Invoice', 'AMER: 2008 (FY09) ATC Program Fee', 'ATC Program Fee for new ATC Applications in the Program year 2008.', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-17 11:02:23', 'admin', '2007-09-17 11:55:39');
INSERT INTO `Line_item_summary` VALUES ('21', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - EC1', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-25 10:49:57', 'admin', '2008-03-05 04:50:29');
INSERT INTO `Line_item_summary` VALUES ('22', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - EC2', 'Fees for ATC Software Subscription & ATC Program Membership 2008-2009', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-25 10:51:11', 'admin', '2008-03-05 04:50:42');
INSERT INTO `Line_item_summary` VALUES ('23', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - ME', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-25 10:52:10', 'admin', '2008-03-05 04:51:57');
INSERT INTO `Line_item_summary` VALUES ('24', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - UK', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-25 10:52:59', 'admin', '2008-03-05 04:52:08');
INSERT INTO `Line_item_summary` VALUES ('25', 'ATC Invoice', 'EMEA:  2008 (FY09) INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-25 10:53:56', 'admin', '2007-09-26 08:08:51');
INSERT INTO `Line_item_summary` VALUES ('26', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC1', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-25 10:55:45', 'admin', '2008-03-05 04:51:27');
INSERT INTO `Line_item_summary` VALUES ('27', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM INCREASE IN SOFTWARE LICENSE ENTITLEMENTS - EC2', 'Fees for an increase in ATC Software License Entitlements 2008-2009', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-25 10:56:37', 'admin', '2008-03-05 04:51:45');
INSERT INTO `Line_item_summary` VALUES ('28', 'ATC Invoice', 'APAC: FY09  NEW APPLICATION (INDIA)', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-09-26 06:24:15', 'admin', '2008-01-09 04:15:26');
INSERT INTO `Line_item_summary` VALUES ('29', 'ATC Invoice', 'EMIA 2008 (FY09) CREDIT NOTE ANNUAL RENEWAL', 'Credit Notes or refunds', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2007-12-18 11:47:43', 'admin', '2008-03-05 04:52:19');
INSERT INTO `Line_item_summary` VALUES ('30', 'ATC Invoice', 'Apac FY09 ATC PROG New App Annual Fee', 'ATC Program Fee for new ATC Applications', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-01-09 04:10:46', 'admin', '2008-01-15 06:43:10');
INSERT INTO `Line_item_summary` VALUES ('31', 'ATC Invoice', 'AAPAC ATC Program', 'ATC Annual Program Fee', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-01-15 06:45:46', 'admin', '2012-03-21 01:21:36');
INSERT INTO `Line_item_summary` VALUES ('32', 'ATC Invoice', 'AAPAC ATC Certification', 'ATC Certification Fee', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-01-15 06:46:43', 'admin', '2012-03-14 21:29:39');
INSERT INTO `Line_item_summary` VALUES ('33', 'ATC Invoice', 'AAPac AOTG DSL', 'AOTG DSL fee', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-01-15 06:47:43', 'admin', '2012-03-14 21:29:17');
INSERT INTO `Line_item_summary` VALUES ('34', 'ATC Invoice', 'AAPac AOTG Courseware', 'AOTG hard copy Courseware fee', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-01-15 06:48:32', 'admin', '2012-03-14 21:28:55');
INSERT INTO `Line_item_summary` VALUES ('35', 'ATC Invoice', 'EMIA 2008 (FY09) CREDIT NOTE NEW APPLICATION', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-01-18 11:32:38', 'admin', '2008-03-05 04:52:30');
INSERT INTO `Line_item_summary` VALUES ('36', 'ATC Invoice', 'EMIA 2008 (FY09) CREDIT NOTE SLEI', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-01-18 11:33:14', 'admin', '2008-03-05 04:52:42');
INSERT INTO `Line_item_summary` VALUES ('37', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM ANNUAL RENEWAL - SAARC/INDIA', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-04-16 10:21:53', 'admin', '2008-04-16 10:32:22');
INSERT INTO `Line_item_summary` VALUES ('38', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM APPLICATION FOR NEW SITE - SAARC/INDIA', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-04-16 10:22:49', 'admin', '2008-04-16 10:46:53');
INSERT INTO `Line_item_summary` VALUES ('39', 'ATC Invoice', 'EMIA 2008 (FY09) ATC PROGRAM AOTC DSL - SAARC/INDIA', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-04-16 10:23:43', 'admin', '2008-04-16 10:46:35');
INSERT INTO `Line_item_summary` VALUES ('40', 'ATC Invoice', 'AMER: 2009 (FY10) ATC Program Fee LA (12) -  Model 2', 'LA ATC Fee Model 2 (1 Essentials DSL Title)', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-09-26 18:00:07', 'admin', '2008-09-26 18:28:52');
INSERT INTO `Line_item_summary` VALUES ('41', 'ATC Invoice', 'AMER: 2009 (FY10) ATC Program Fee', 'ATC Program Fee for new ATC Applications in the Program year 2009.', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-09-29 12:17:40', 'admin', '2009-01-06 17:15:38');
INSERT INTO `Line_item_summary` VALUES ('42', 'ATC Invoice', 'AMER: 2009 (FY10) Renewal', 'ATC Program Renewal Fee in the Program year 2009.', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-09-29 12:18:48', 'admin', '2008-09-29 12:19:31');
INSERT INTO `Line_item_summary` VALUES ('43', 'ATC Invoice', 'AMER: 2009 (FY10) ATC Program Fee LA (12) SAT-  Model 2', 'LA ATC Fee Model 2 (1 Essentials DSL Title)', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 16:04:32', 'admin', '2008-10-22 18:17:54');
INSERT INTO `Line_item_summary` VALUES ('44', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:09:21', 'admin', '2008-10-02 17:09:57');
INSERT INTO `Line_item_summary` VALUES ('45', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:10:56', 'admin', '2008-10-02 17:11:42');
INSERT INTO `Line_item_summary` VALUES ('46', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:12:26', 'admin', '2008-10-02 17:12:43');
INSERT INTO `Line_item_summary` VALUES ('47', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 1 Industry', 'LA ATC Fee Model 1 PSEB + 1 Industry / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:13:22', 'admin', '2008-10-02 17:13:31');
INSERT INTO `Line_item_summary` VALUES ('48', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:15:24', 'admin', '2008-10-02 17:15:39');
INSERT INTO `Line_item_summary` VALUES ('49', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:16:02', 'admin', '2008-10-02 17:16:23');
INSERT INTO `Line_item_summary` VALUES ('50', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:17:26', 'admin', '2008-10-02 17:17:38');
INSERT INTO `Line_item_summary` VALUES ('51', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 2 Industry', 'LA ATC Fee Model 1 PSEB + 2 Industry / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:18:03', 'admin', '2008-10-02 17:18:15');
INSERT INTO `Line_item_summary` VALUES ('52', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:18:55', 'admin', '2008-10-02 17:19:07');
INSERT INTO `Line_item_summary` VALUES ('53', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:19:30', 'admin', '2008-10-02 17:19:42');
INSERT INTO `Line_item_summary` VALUES ('54', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:20:07', 'admin', '2008-10-02 17:20:21');
INSERT INTO `Line_item_summary` VALUES ('55', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 1 PSEB + 3 Industry', 'LA ATC Fee Model 1 PSEB + 3 Industry / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-02 17:20:49', 'admin', '2008-10-02 17:21:04');
INSERT INTO `Line_item_summary` VALUES ('56', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Essentials Title', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 17:32:34', 'admin', '2008-10-22 18:18:00');
INSERT INTO `Line_item_summary` VALUES ('57', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Industry / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 17:44:45', 'admin', '2008-10-22 18:18:16');
INSERT INTO `Line_item_summary` VALUES ('58', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Essentials', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 17:46:31', 'admin', '2008-10-22 18:17:44');
INSERT INTO `Line_item_summary` VALUES ('59', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2', 'LA ATC Fee Model 2 PSEB + 1 Essentials Title / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 17:56:23', 'admin', '2008-10-22 18:18:25');
INSERT INTO `Line_item_summary` VALUES ('60', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 17:58:17', 'admin', '2008-10-22 18:23:23');
INSERT INTO `Line_item_summary` VALUES ('61', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:06:05', 'admin', '2008-10-22 18:23:49');
INSERT INTO `Line_item_summary` VALUES ('62', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'LA ATC Fee Model 2  + 2 Essentials Title', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:07:04', 'admin', '2008-10-22 18:23:35');
INSERT INTO `Line_item_summary` VALUES ('63', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:07:50', 'admin', '2008-10-22 19:01:14');
INSERT INTO `Line_item_summary` VALUES ('64', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:21:08', 'admin', '2008-10-22 18:23:09');
INSERT INTO `Line_item_summary` VALUES ('65', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:21:33', 'admin', '2008-10-22 18:24:33');
INSERT INTO `Line_item_summary` VALUES ('66', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:26:02', 'admin', '2008-10-22 18:27:05');
INSERT INTO `Line_item_summary` VALUES ('67', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 1', 'LA ATC Fee Model 2 + 1 Essentials Title / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:26:40', 'admin', '2008-10-22 18:27:36');
INSERT INTO `Line_item_summary` VALUES ('68', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (12) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:55:28', 'admin', '2008-10-22 18:56:41');
INSERT INTO `Line_item_summary` VALUES ('69', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:57:21', 'admin', '2008-10-22 18:57:41');
INSERT INTO `Line_item_summary` VALUES ('70', 'ATC Invoice', 'AMER: 2009 Annual Main Site Fee for Latin America (24) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:58:16', 'admin', '2008-10-22 18:58:48');
INSERT INTO `Line_item_summary` VALUES ('71', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (12) - Model 2 / Lev 2', 'LA ATC Fee Model 2 + 2 Essentials Title / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 18:59:15', 'admin', '2008-10-22 19:07:37');
INSERT INTO `Line_item_summary` VALUES ('72', 'ATC Invoice', 'AMER: 2009 Annual SAT Site Fee for Latin America (24) - Model 2 / Lev 3', 'LA ATC Fee Model 2 + 3 Essentials Title / SAT', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-10-22 19:08:45', 'admin', '2008-10-22 19:09:04');
INSERT INTO `Line_item_summary` VALUES ('73', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:28:20', 'admin', '2008-11-13 10:54:44');
INSERT INTO `Line_item_summary` VALUES ('74', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:29:19', 'admin', '2008-11-13 10:50:41');
INSERT INTO `Line_item_summary` VALUES ('75', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:30:13', 'admin', '2008-11-13 10:55:32');
INSERT INTO `Line_item_summary` VALUES ('76', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:30:54', 'admin', '2008-11-13 10:53:56');
INSERT INTO `Line_item_summary` VALUES ('77', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC3 CIS - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:31:29', 'admin', '2008-11-13 10:48:24');
INSERT INTO `Line_item_summary` VALUES ('78', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - SAPAC - NEW SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEES', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:32:07', 'admin', '2008-12-15 09:10:34');
INSERT INTO `Line_item_summary` VALUES ('79', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - MAIN EUROPE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:32:43', 'admin', '2008-11-17 05:24:32');
INSERT INTO `Line_item_summary` VALUES ('80', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - UK & IRE - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:33:13', 'admin', '2008-11-17 05:23:49');
INSERT INTO `Line_item_summary` VALUES ('81', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC1 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:33:39', 'admin', '2008-11-17 05:24:11');
INSERT INTO `Line_item_summary` VALUES ('82', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC2 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:34:09', 'admin', '2008-11-17 05:24:52');
INSERT INTO `Line_item_summary` VALUES ('83', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - EC3 CIS - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:34:34', 'admin', '2008-11-17 05:24:43');
INSERT INTO `Line_item_summary` VALUES ('84', 'ATC Invoice', 'EMIA: 2009 (FY10) ATC PROGRAM - SAPAC - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2008-11-05 06:34:58', 'admin', '2008-12-15 09:10:21');
INSERT INTO `Line_item_summary` VALUES ('85', 'ATC Invoice', 'AMER: 2010 (FY11):  A&P Program Fee', 'Standard 1 user', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-09-29 11:17:48', 'admin', '2009-09-29 11:17:48');
INSERT INTO `Line_item_summary` VALUES ('86', 'A&P Invoice', 'VIRTUAL: A&P Invoices from ADN', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-09-30 09:51:55', 'admin', '2009-09-30 09:51:55');
INSERT INTO `Line_item_summary` VALUES ('87', 'ATC Invoice', 'ATC Discount', 'Prorated Discount', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-10-02 13:41:56', 'admin', '2009-10-02 13:41:56');
INSERT INTO `Line_item_summary` VALUES ('88', 'VAR Invoice', 'VAR Invoice Type Sample', 'Modify as needed', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-10-24 08:45:23', 'admin', '2009-10-24 08:47:03');
INSERT INTO `Line_item_summary` VALUES ('89', 'ATC Invoice', 'AMER: 2010 (FY11) ATC Program Fee', 'ATC Program Fee for new ATC Applications in the Program year 2010.', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-11-11 15:56:18', 'admin', '2009-11-11 15:56:18');
INSERT INTO `Line_item_summary` VALUES ('90', 'ATC Invoice', 'AMER: 2010 (FY11) Renewal', 'ATC Program Renewal Fee in the Program year 2010.', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-11-11 15:57:15', 'admin', '2009-11-11 15:58:06');
INSERT INTO `Line_item_summary` VALUES ('91', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS1 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:46:28', 'admin', '2009-12-08 10:57:31');
INSERT INTO `Line_item_summary` VALUES ('92', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS2 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:47:40', 'admin', '2009-12-08 10:58:09');
INSERT INTO `Line_item_summary` VALUES ('93', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTON FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:48:19', 'admin', '2009-12-08 10:59:08');
INSERT INTO `Line_item_summary` VALUES ('94', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS4 - RENEWAL - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:49:00', 'admin', '2009-12-08 10:58:58');
INSERT INTO `Line_item_summary` VALUES ('95', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS1 - NEW MAIN SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:50:19', 'admin', '2009-12-08 10:57:19');
INSERT INTO `Line_item_summary` VALUES ('96', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS2 - NEW MAIN SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:51:07', 'admin', '2009-12-08 10:57:57');
INSERT INTO `Line_item_summary` VALUES ('97', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - NEW MAIN SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:53:00', 'admin', '2009-12-08 10:58:45');
INSERT INTO `Line_item_summary` VALUES ('98', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS4 - NEW MAIN - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:53:24', 'admin', '2009-12-08 10:59:23');
INSERT INTO `Line_item_summary` VALUES ('99', 'ATC Invoice', 'EMEA ATC PROGRAM - ANNUAL SOFTWARE & SUBSCRIPTION FEE - NEW SITE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:54:20', 'admin', '2009-12-08 11:00:17');
INSERT INTO `Line_item_summary` VALUES ('100', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS2 - NEW ADD SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:54:46', 'admin', '2009-12-08 10:57:45');
INSERT INTO `Line_item_summary` VALUES ('101', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - NEW ADD SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:55:10', 'admin', '2009-12-08 10:58:20');
INSERT INTO `Line_item_summary` VALUES ('102', 'ATC Invoice', 'EMEA: 2010 (FY11) ATC PROGRAM - FS3 - NEW ADD SITE - ANNUAL SOFTWARE & SUBSCRIPTION FEE', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 10:55:32', 'admin', '2009-12-08 10:58:33');
INSERT INTO `Line_item_summary` VALUES ('103', 'ATC Invoice', 'EMEA ATC PROGRAM - ANNUAL SOFTWARE & SUBSCRIPTION FEE - RENEWAL', null, '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2009-12-08 11:00:33', 'admin', '2009-12-08 11:00:52');
INSERT INTO `Line_item_summary` VALUES ('104', 'ATC Invoice', 'APAC: FY17 ATC Program Fee', 'Fees for ATC Software Subscription & ATC Program Membership', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2016-07-21 09:39:07', 'admin', '2016-07-28 23:28:23');
INSERT INTO `Line_item_summary` VALUES ('105', 'ATC Invoice', 'AMER: FY17 ATC Program Fee', 'Fees for ATC Software Subscription & ATC Program Membership', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2016-07-25 04:29:00', 'admin', '2016-07-25 04:29:00');
INSERT INTO `Line_item_summary` VALUES ('106', 'ATC Invoice', 'EMEA: FY17 ATC Program Fee', 'Fees for ATC Software Subscription & ATC Program Membership', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '2016-07-25 04:31:30', 'admin', '2016-07-25 04:31:30');
INSERT INTO `Line_item_summary` VALUES ('108', 'ATC Invoice', 'asd', 'asdasd', '2.00', '3.00', '4.00', '5.00', '6.00', 'Y', 'admin', '0001-01-01 00:00:00', 'admin', '0001-01-01 00:00:00');

-- ----------------------------
-- Table structure for Line_item_summary_category
-- ----------------------------
DROP TABLE IF EXISTS `Line_item_summary_category`;
CREATE TABLE `Line_item_summary_category` (
  `activity_type_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`activity_type_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_type`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Line_item_summary_category
-- ----------------------------
INSERT INTO `Line_item_summary_category` VALUES ('2', 'A&P Invoice Line Item Summaries', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Line_item_summary_category` VALUES ('4', 'VAR Invoice Line Item Summaries', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Line_item_summary_category` VALUES ('3', 'ATC Invoice Line Item Summaries', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');

-- ----------------------------
-- Table structure for Main_contact
-- ----------------------------
DROP TABLE IF EXISTS `Main_contact`;
CREATE TABLE `Main_contact` (
  `ContactIdInt` int(11) NOT NULL AUTO_INCREMENT,
  `ContactId` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `SiebelId` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `SiebelStatus` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `PrimarySiteId` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ContactName` varchar(121) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EnglishContactName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `FirstName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LastName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Salutation` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TitlePosition` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EmailAddress` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EmailAddress2` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EmailAddress3` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EmailAddress4` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DoNotEmail` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Username` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PWDChangeRequired` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PermissionsGroup` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `HireDate` datetime DEFAULT NULL,
  `HowLongWithOrg` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `HowLongInIndustry` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `HowLongAutodeskProducts` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ExperiencedCompetitiveProducts` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `ExperiencedCompetitiveProductsList` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `QuestionnaireDate` date NOT NULL DEFAULT '2007-01-01',
  `PercentPreSales` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `PercentPostSales` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `Dedicated` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `Department` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InternalSales` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `PrimaryRoleId` int(10) NOT NULL,
  `PrimaryIndustryId` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `PrimarySubIndustryId` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `PrimaryRoleExperience` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ATCRole` varchar(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `Telephone1` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Telephone2` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mobile1` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Mobile2` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PrimaryLanguage` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SecondaryLanguage` char(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Timezone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TimeZoneUTC` float NOT NULL,
  `Comments` text COLLATE utf8_unicode_ci,
  `InstructorId` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LoginCount` smallint(5) DEFAULT NULL,
  `LastLogin` datetime DEFAULT NULL,
  `LastIPAddress` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOUserUUID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `AddedBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateLastAdmin` datetime DEFAULT NULL,
  `LastAdminBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Action` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CSOUpdatedOn` datetime DEFAULT NULL,
  `CSOVersion` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Administrator` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `ShowDuplicates` char(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'N',
  `Level` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AdminSystems` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `CompanyName` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResetToken` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ResetTokenExpiration` datetime DEFAULT NULL,
  `Address1` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address2` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Address3` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `City` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `PostalCode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CountryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `StateProvince` varchar(72) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Gender` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `WebsiteUrl` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Bio` varchar(2000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShowInSearch` tinyint(1) DEFAULT '1',
  `ShareEmail` tinyint(1) DEFAULT '1',
  `ShareTelephone` tinyint(1) DEFAULT '1',
  `ShareMobile` tinyint(1) DEFAULT '1',
  `ACIMemberId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ContactIdInt`),
  KEY `IDX_CONTACT_EMAIL` (`EmailAddress`),
  KEY `IDX_CONTACT_UUID` (`CSOUserUUID`),
  KEY `IDX_CONTACTS_ContactName` (`ContactName`),
  KEY `IDX_Contacts_EMAIL2` (`EmailAddress2`),
  KEY `IDX_Contacts_EngName` (`EnglishContactName`),
  KEY `IDX_CONTACTS_FirstName` (`FirstName`),
  KEY `IDX_Contacts_InstructorId` (`InstructorId`),
  KEY `IDX_CONTACTS_LastName` (`LastName`),
  KEY `IDX_Contacts_PrimarySiteId` (`PrimarySiteId`),
  KEY `IDX_CONTACTS_Status` (`Status`),
  KEY `IDX_CONTACTS_UNIQUE_USERNAME` (`Username`),
  KEY `InternalSales` (`InternalSales`),
  KEY `SiebelId` (`SiebelId`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of Main_contact
-- ----------------------------
INSERT INTO `Main_contact` VALUES ('1', '5104506', '1', '1', 'EM303530', 'Mohamed Mohsen Abd El Hamid Ibrahim', 'Mohamed Mohsen Abd El Hamid Ibrahim', 'Mohamed', 'Mohsen Abd El Hamid Ibrahim', '1', 'Instructor', 'Mohamedmohsenae@gmail.com', '1', '1', '1', 'Y', '1', '1', '1', '1', '2018-01-17 03:28:17', '1', '1', '1', '', '', '2007-01-01', '', '', 'Y', '', 'f', '23', 'other', '', '', 'Y', '1', '1', '1', '1', '37', '37', 'tzone000000000000028', '0', '1', '47326', '1', '2018-01-29 04:06:59', null, 'A', null, '2016-06-23 05:12:03', 'nitin.balyan@vinsys.in', '2016-06-23 05:13:15', 'nitin.balyan@vinsys.in', '', '2018-01-17 04:01:22', null, 'N', 'N', null, '', null, null, '2018-01-17 04:07:49', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1');
INSERT INTO `Main_contact` VALUES ('2', 'test', '', '', 'EM300976', 'test test', 'test test', 'test', 'test', '', '', 'test', '', '', '', '', '', '', '', '', '2018-01-20 03:34:22', '', '', '', '', '', '2018-01-20', '', '', '', '', '', '1', '', '', '', '', '', '', '', '', '', '', '', '0', '', '', '1', '2018-01-20 03:34:22', '', '', '', '2018-01-20 03:34:22', '', '2018-01-20 03:34:22', '', '', '2018-01-20 03:34:22', '', '', '', '', '', '', '', '2018-01-20 03:34:22', '', '', '', '', '', '', '', '', '', '', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for Main_organization
-- ----------------------------
DROP TABLE IF EXISTS `Main_organization`;
CREATE TABLE `Main_organization` (
  `OrganizationId` int(11) NOT NULL AUTO_INCREMENT,
  `OrgId` varchar(80) COLLATE utf8_unicode_ci DEFAULT '',
  `ATCOrgId` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OrgStatus_retired` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OrgName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EnglishOrgName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CommercialOrgName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `YearJoined` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `TaxExempt` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VATNumber` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAPSoldTo` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `VARCSN` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATCCSN` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AUPCSN` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OrgWebAddress` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisteredDepartment` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisteredAddress1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisteredAddress2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisteredAddress3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisteredCity` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisteredStateProvince` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisteredPostalCode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `RegisteredCountryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicingDepartment` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicingAddress1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicingAddress2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicingAddress3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicingCity` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicingStateProvince` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicingPostalCode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `InvoicingCountryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractDepartment` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractAddress1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractAddress2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractAddress3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractCity` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractStateProvince` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractPostalCode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ContractCountryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATCDirectorFirstName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATCDirectorLastName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATCDirectorEmailAddress` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATCDirectorTelephone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ATCDirectorFax` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LegalContactFirstName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LegalContactLastName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LegalContactEmailAddress` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LegalContactTelephone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `LegalContactFax` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BillingContactFirstName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BillingContactLastName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BillingContactEmailAddress` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BillingContactTelephone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `BillingContactFax` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AdminNotes` text COLLATE utf8_unicode_ci,
  `Status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `AddedBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateLastAdmin` datetime DEFAULT NULL,
  `LastAdminBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOResellerUUID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOResellerName` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOPartnerManager` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOUpdatedOn` datetime DEFAULT NULL,
  `CSOVersion` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`OrganizationId`),
  KEY `IDX_ORGANIZATIONS_SAP` (`SAPSoldTo`),
  KEY `IDX_ORGANIZATIONS_UUID` (`CSOResellerUUID`),
  KEY `IDX_ORGANIZATIONS_OrgName` (`OrgName`),
  KEY `IDX_ORGANIZATIONS_CountryCode` (`RegisteredCountryCode`),
  KEY `IDX_ORGANIZATIONS_Status` (`Status`),
  KEY `IDX_ORGANIZATIONS_ATCID` (`ATCOrgId`),
  KEY `IDX_ORG_OrgStatus` (`OrgStatus_retired`),
  KEY `IDX_Org_EngName` (`EnglishOrgName`),
  KEY `IDX_Org_CommName` (`CommercialOrgName`),
  KEY `IDX_Org_State` (`RegisteredStateProvince`)
) ENGINE=MyISAM AUTO_INCREMENT=1017 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Main_organization
-- ----------------------------
INSERT INTO `Main_organization` VALUES ('949', 'ORG2788', 'OE4123', 'A', 'CADWARE ENGINEERING SRL', null, '', null, null, null, '5104077932', null, '5104077932', null, null, null, 'STR.LUCRETIU PATRASCANU NR.3', null, null, 'BUCHAREST', null, '30502', 'RO', null, 'STR.LUCRETIU PATRASCANU NR.3', null, null, 'BUCHAREST', null, '30502', 'RO', null, 'STR.LUCRETIU PATRASCANU NR.3', null, null, 'BUCHAREST', null, '30502', 'RO', null, null, 'niculae.ion@cadware-eng.ro', '40770674759', '40213400362', null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2008-11-17 12:35:39', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3891464e-eded-44b8-b9be-e22f0380b239', 'Cadware Engineering SRL', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('950', 'OE300379', null, 'A', 'CADXpert as', null, '', null, null, null, '000', null, '000', null, null, null, null, null, null, null, null, null, 'DK', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2008-12-31 19:27:31', 'SYSTEM', '2008-12-31 19:27:31', 'SYSTEM', '38b42e33-a212-4883-a640-4abcad956681', 'CADXpert as', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('951', 'OE300380', null, 'A', 'Atlas Consulting and Tec Services, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'LY', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2007-05-31 00:56:31', 'SYSTEM', '2009-04-08 09:39:51', 'janine.franz@autodesk.com', '38d2df54-1878-4470-b78f-d43fd2c4e1d6', 'Atlas Consulting and Tec Services, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('952', 'OE300381', 'ORG2515', 'A', 'ARNOLD IT SYSTEMS GMBH & CO. KG', null, '', null, null, null, '5070181441', null, '5070181441', null, null, null, 'HANS-BUNTE-STR. 15', null, null, 'FREIBURG', null, '79108', 'DE', null, 'HANS-BUNTE-STR. 15', null, null, 'FREIBURG', null, '79108', 'DE', null, 'HANS-BUNTE-STR. 15', null, null, 'FREIBURG', null, '79108', 'DE', null, null, 'm.arnold@arnold-it.com', '497615036370', '4976150363790', null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:25', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '392849da-8f1a-4182-bddf-8554cff8db06', 'ARNOLD IT Systems GmbH & Co. KG', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('953', 'OE300382', null, 'A', 'CELOS COMPUTER GMBH', null, '', null, null, null, '5101414314', null, '5101414314', null, null, null, null, null, null, null, null, null, 'DE', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2008-04-04 11:31:33', 'SYSTEM', '2008-04-04 11:31:33', 'SYSTEM', '392cc62c-e5f0-4ec1-9081-7e1fb67db7b9', 'CELOS COMPUTER GMBH', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('954', 'OE300383', null, 'A', 'NATIONAL COMPUTER SERVICES LTD', null, '', null, null, null, '5070001208', null, '5070001208', null, null, null, '4TH RING ROAD', null, null, 'SAFAT', null, '13008', 'KW', null, '4TH RING ROAD', null, null, 'SAFAT', null, '13008', 'KW', null, '4TH RING ROAD', null, null, 'SAFAT', null, '13008', 'KW', null, null, 'ahmed.habashy@ncs-me.com', '9654755127', '9654721237', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:56:19', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3943cfbf-3704-4d65-8187-949da5703bed', 'National Computer Services Kuwait', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('955', 'OE300384', 'ORG1136', 'A', 'NTC MicroCAD, ATC', '', '', '', '', '', '', null, '', null, '', '', '157 Bradford Road', '', '', 'Bradford', '', '', 'GB', '', '157 Bradford Road', '', '', 'Bradford', '', '', 'GB', '', '157 Bradford Road', '', '', 'Bradford', '', '', 'GB', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ' notes', 'D', '2007-05-31 00:56:45', 'SYSTEM', '2009-07-07 08:35:05', 'janm', '397f55d8-6a32-4ed4-9706-567b003c4726', 'NTC MicroCAD, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('956', 'OE300385', null, 'A', 'INFOLINE d.o.o.', null, '', null, null, null, '5070335990', null, '5070335990', null, null, null, null, null, null, null, null, null, 'HR', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:56:21', 'SYSTEM', '2007-05-31 00:56:21', 'SYSTEM', '39895e75-f354-45ee-8a1d-1cc9812efeb3', 'INFOLINE d.o.o.', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('957', 'OE300386', 'ORG1056', 'A', 'Infars Ltd, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'RU', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:37', 'SYSTEM', '2009-04-22 09:04:49', 'marcov', '398b37df-21d7-42fa-b218-bd03afa728dc', 'Infars Ltd, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('958', 'OE300387', null, 'A', 'NTI CADCENTER A/S - VÆRLØSE', null, '', null, null, null, '5070188237', null, '5070188237', null, null, null, null, null, null, null, null, null, 'DK', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2008-04-04 11:31:51', 'SYSTEM', '2009-04-08 10:30:30', 'janine.franz@autodesk.com', '398ee764-8bdc-4c49-8ba6-181b83c73403', 'NTI CADCENTER A/S - VÆRLØSE', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('959', 'OE300388', null, 'A', 'MAN AND MACHINE SWEDEN AB', null, '', null, null, null, '5070000005', null, '5070000005', null, null, null, null, null, null, null, null, null, 'SE', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2008-04-04 11:31:47', 'SYSTEM', '2008-04-04 11:31:47', 'SYSTEM', '3a33f296-35ea-42f0-bc9e-55d8da4ab7f6', 'MAN AND MACHINE SWEDEN AB', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('960', 'OE300389', 'ORG994', 'A', 'Falco SA, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'FR', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:23', 'SYSTEM', '2009-04-24 06:15:50', 'marcov', '3a3a8f9f-9c31-4d34-9d49-46ececc52f3d', 'Falco SA, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('961', 'OE300390', null, 'A', 'COMPUTER AND DESIGN SERVICES LTD', null, '', null, null, null, '5070181897', null, '5070181897', null, null, null, 'ARROWSMITH COURT', null, null, 'BROADSTONE', null, 'BH18 8AX', 'GB', null, 'ARROWSMITH COURT', null, null, 'BROADSTONE', null, 'BH18 8AX', 'GB', null, 'ARROWSMITH COURT', null, null, 'BROADSTONE', null, 'BH18 8AX', 'GB', null, null, 'ianc@cads.co.uk', '1202603031', '1202658549', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-11-08 05:14:42', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3a3d0583-fbf3-4b0d-8248-065d4977a080', 'Computer And Design Services Ltd.', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('962', 'OE300391', null, 'A', 'CAD Studio Hamburg', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'DE', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2007-05-31 00:57:04', 'SYSTEM', '2007-05-31 00:57:04', 'SYSTEM', '3a3e449d-1785-4bf6-b409-8e942060a15f', 'CAD Studio Hamburg', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('963', 'OE300392', 'ORG969', 'A', 'Edicad - Computacao Grafica e Imagem - Lda, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'PT', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:34', 'SYSTEM', '2009-04-24 06:15:30', 'marcov', '3a46bdc0-ff34-4a41-96bb-97da54171c87', 'Edicad - Computacao Grafica e Imagem - Lda, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('964', 'OE300393', 'ORG1123', 'A', 'MCAD SVERIGE AB', null, '', null, null, null, '5070267086', null, '5070267086', null, null, null, 'ANNEDALSVÄGEN 9', null, null, 'LUND', null, '227 64', 'SE', null, 'ANNEDALSVÄGEN 9', null, null, 'LUND', null, '227 64', 'SE', null, 'ANNEDALSVÄGEN 9', null, null, 'LUND', null, '227 64', 'SE', null, null, 'anders.larsson@mcad.se', '4646370700', '4646139550', null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:42', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3a6f8fa6-1779-464a-86a0-62e34bd5a5a9', 'MCAD Sverige AB', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('965', 'OE300394', 'ORG2168', 'A', 'Synthesis Le Site, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'BE', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:21', 'SYSTEM', '2009-04-24 03:54:36', 'fredb', '3ab25b1c-dc4d-4d13-8aac-163835974f29', 'Synthesis Le Site, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('966', 'OE300395', null, 'A', 'AGA Ltd, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'LT', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2007-05-31 00:56:31', 'SYSTEM', '2007-05-31 00:56:31', 'SYSTEM', '3b1e40e7-3030-4e40-871c-57e3f28ce41d', 'AGA Ltd, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('967', 'OE300396', 'ORG1267', 'A', 'AS Usesoft, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'EE', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:22', 'SYSTEM', '2009-04-23 09:36:05', 'fredb', '3b27dab7-e410-420e-bc95-58e04e306fa2', 'AS Usesoft, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('968', 'OE300397', 'ORG1896', 'A', 'ARES Paris, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'FR', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:23', 'SYSTEM', '2009-04-24 06:02:05', 'marcov', '3b3b59a0-e8cc-40d6-872e-fffe0e82afa7', 'ARES Paris, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('969', 'OE300398', null, 'A', 'CARBONE STUDIO', null, '', null, null, null, '5102332975', null, '5102332975', null, null, null, '7 AVENUE DE L\'EUROPE', null, null, 'SEVRES', null, '92310', 'FR', null, '7 AVENUE DE L\'EUROPE', null, null, 'SEVRES', null, '92310', 'FR', null, '7 AVENUE DE L\'EUROPE', null, null, 'SEVRES', null, '92310', 'FR', null, null, 'jean-louis.jarry@picad.fr', '330155602387', '330155601839', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:57:08', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3b445fb5-f0e4-4a43-9dea-cd734220e1d7', 'Carbone Studio / groupe Picad', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('970', 'OE300399', '', 'T', 'CAD-LAN AG', '', '', '', '', '', '5070066010', null, '5070066010', null, '', '', 'REIHERWEG 2', '', '', 'SUHR', '', '5034', 'CH', '', 'REIHERWEG 2', '', '', 'SUHR', '', '5034', 'CH', '', 'REIHERWEG 2', '', '', 'SUHR', '', '5034', 'CH', '', '', 'beni.boos@cadlan.ch', '41628556060', '41628556000', '', '', '', '', '', '', '', '', '', '', ' notes', 'X', '2008-04-04 11:31:32', 'SYSTEM', '2009-07-07 08:16:56', 'janm', '3b752d65-0e76-4d7a-a26b-d4b43816bbab', 'CAD-LAN AG', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('971', 'OE300400', 'OE4120', 'A', 'ROC Zeeland', null, '', null, null, null, '5100086639', null, '5100086639', null, null, null, null, null, null, null, null, null, 'NL', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2008-12-28 17:06:00', 'SYSTEM', '2009-04-24 03:57:36', 'fredb', '3bd32cd8-8186-4664-b000-f1d01fb85afe', 'ROC Zeeland', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('972', 'OE300401', null, 'A', 'MAN AND MACHINE SOFTWARE SP.Z.O.O.', null, '', null, null, null, '5070000708', null, '5070000708', null, null, null, null, null, null, null, null, null, 'PL', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2008-04-04 11:31:47', 'SYSTEM', '2008-04-04 11:31:47', 'SYSTEM', '3be274dd-c7bd-41b7-a837-3c53a901d608', 'MAN AND MACHINE SOFTWARE SP.Z.O.O.', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('973', 'OE300402', null, 'A', 'TECH DATA ITALIA S.R.L.', null, '', null, null, null, '5070000087', null, '5070000087', null, null, null, null, null, null, null, null, null, 'IT', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2008-04-04 11:32:02', 'SYSTEM', '2008-04-04 11:32:02', 'SYSTEM', '3bef2844-71b7-41d8-a30c-4303dfaae57c', 'TECH DATA ITALIA S.R.L.', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('974', 'OE300403', 'ORG895', 'A', 'Cadpool Oy, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'FI', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:22', 'SYSTEM', '2009-04-24 03:19:07', 'fredb', '3c0ca1be-593a-437f-a09b-2278d970fda1', 'Cadpool Oy, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('975', 'OE300404', 'ORG2396', 'A', 'PARA GRAPH, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'FR', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:24', 'SYSTEM', '2009-04-24 09:02:29', 'marcov', '3c2762a0-7f1f-4de9-89ba-e24bad125d99', 'PARA GRAPH, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('976', 'OE300405', null, 'A', 'HIRSCH EDV - SYSTEME', null, '', null, null, null, '5070181557', null, '5070181557', null, null, null, 'STEINERNKREUZ 7', null, null, 'STALLWANG', null, '94375', 'DE', null, 'STEINERNKREUZ 7', null, null, 'STALLWANG', null, '94375', 'DE', null, 'STEINERNKREUZ 7', null, null, 'STALLWANG', null, '94375', 'DE', null, null, 'k.hirsch@hesys.de', '49996694020', '499966940214', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:56:27', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3c39362c-1f09-4e76-b77a-46c0907b362c', 'Hirsch EDV - Systeme', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('977', 'OE300406', null, 'A', 'IMAGEGRAFIX LTD', null, '', null, null, null, '5070196119', null, '5070196119', null, null, null, 'JEBEL ALI FREE ZONE', null, null, 'DUBAI', null, 'Unspecified', 'AE', null, 'JEBEL ALI FREE ZONE', null, null, 'DUBAI', null, 'Unspecified', 'AE', null, 'JEBEL ALI FREE ZONE', null, null, 'DUBAI', null, 'Unspecified', 'AE', null, null, 'imagegfx@emirates.net.ae', '97148819033', '97148819034', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2008-04-04 11:31:41', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3c68a9a5-590d-4bbf-a760-690d4227a180', 'IMAGEGRAFIX LTD', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('978', 'OE300407', null, 'A', 'micad', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'ES', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2007-05-31 00:57:08', 'SYSTEM', '2007-05-31 00:57:08', 'SYSTEM', '3ca93caf-cb56-4310-8603-c68d2eecafb6', 'micad', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('979', 'OE300408', null, 'A', 'ATREE SRL', null, '', null, null, null, '5070253114', null, '5070253114', null, null, null, 'VIA SANTA ROSA 62 / INT.2', null, null, 'ZANE\'', null, '36010', 'IT', null, 'VIA SANTA ROSA 62 / INT.2', null, null, 'ZANE\'', null, '36010', 'IT', null, 'VIA SANTA ROSA 62 / INT.2', null, null, 'ZANE\'', null, '36010', 'IT', null, null, 'ipretto@atree.it', '390445371042', '390445376112', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:57:07', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3cbc78da-1269-490e-a66f-fa69af744eab', 'atree srl', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('980', 'OE300409', 'ORG1205', 'A', 'Salesianos Loyola - Aranjuez, ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'ES', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:41', 'SYSTEM', '2009-04-24 09:02:55', 'marcov', '3cc6f7a0-dde3-4b84-b114-3ec91d6c3f69', 'Salesianos Loyola - Aranjuez, ATC', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('981', 'OE300410', 'ORG2510', 'A', 'Open Systems Technologies (Pvt) Ltd', '', '', '', '', '', '70207103', null, '70207103', null, '', '', '1ST FLOOR, E.H. COORAY BUILDING', '', '', 'COLOMBO 3', '', 'Unspecified', 'LK', '', '1ST FLOOR, E.H. COORAY BUILDING', '', '', 'COLOMBO 3', '', 'Unspecified', 'LK', '', '1ST FLOOR, E.H. COORAY BUILDING', '', '', 'COLOMBO 3', '', 'Unspecified', 'LK', '', '', 'ajith@eopensys.com', '910112372401', '910115338501', '', '', '', '', '', '', '', '', '', '', ' notes', 'D', '2008-04-04 10:34:13', 'SYSTEM', '2009-07-22 09:01:30', 'janm', '3cdbb6a3-6cc2-4376-8a11-de25f327d9a6', 'OPEN SYSTEMS TECHNOLOGIES PTE LTD', 'Karen Ruane', '2008-04-01 14:03:44', '3');
INSERT INTO `Main_organization` VALUES ('982', 'OE300411', 'ORG941', 'A', 'Computer agency o.p.s., ATC', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'CZ', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-07-30 03:38:21', 'SYSTEM', '2009-04-22 10:10:51', 'marcov', '3cf3d967-1c1d-4885-8200-e91a1f35199c', 'Computer agency o.p.s., ATC', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('983', 'OE300412', null, 'A', 'CAD POINT LTD', null, '', null, null, null, '5070267982', null, '5070267982', null, null, null, '44 DUNAV STR., 2 FL.', null, null, 'SOFIA', null, '1000', 'BG', null, '44 DUNAV STR., 2 FL.', null, null, 'SOFIA', null, '1000', 'BG', null, '44 DUNAV STR., 2 FL.', null, null, 'SOFIA', null, '1000', 'BG', null, null, 'j_nikolov@cadpointbg.com', '35929833001', '35929832362', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:56:21', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3d05885a-9a6e-45a3-872b-979741b48c0a', 'CAD Point Ltd.', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('984', 'OE300413', null, 'A', 'Alpha Data processing services L.L.C', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'AE', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:56:19', 'SYSTEM', '2007-05-31 00:56:19', 'SYSTEM', '3d41cf2f-b178-4a7c-bdd7-4e554c436701', 'Alpha Data processing services L.L.C', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('985', 'OE300414', null, 'A', 'ACAD Graph', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'DE', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2007-05-31 00:56:57', 'SYSTEM', '2007-05-31 00:56:57', 'SYSTEM', '3d561614-a88a-4e6c-a4e9-dd0657fd3f30', 'ACAD Graph', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('986', 'OE300415', null, 'A', 'COSMOS BUSINESS SYSTEMS S.A.', null, '', null, null, null, '5070181948', null, '5070181948', null, null, null, '73, VRILISSOU STR.', null, null, 'ATHENS', null, '11475', 'GR', null, '73, VRILISSOU STR.', null, null, 'ATHENS', null, '11475', 'GR', null, '73, VRILISSOU STR.', null, null, 'ATHENS', null, '11475', 'GR', null, null, 'dafnisd@cbs.gr', '2106990891', '2106990820', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2008-04-17 07:21:39', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3dbc0c07-16fb-4248-ace3-7f981623f409', 'COSMOS BUSINESS SYSTEMS S.A.', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('987', 'OE300416', 'ORG2617', 'A', 'CADXPERT AS', null, '', null, null, null, '5101946351', null, '5101946351', null, null, null, 'STRANDKÆRVEJ 40', null, null, 'HORSENS', null, '8700', 'DK', null, 'STRANDKÆRVEJ 40', null, null, 'HORSENS', null, '8700', 'DK', null, 'STRANDKÆRVEJ 40', null, null, 'HORSENS', null, '8700', 'DK', null, null, 'owner@cadx.dk', '4576288100', '4576288109', null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2008-04-04 11:31:32', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3dc98eab-8c3b-40b9-a0ad-7e83951986e4', 'CADXPERT AS', 'Karen Ruane', '2008-04-01 14:03:44', '3');
INSERT INTO `Main_organization` VALUES ('988', 'OE300417', null, 'A', 'EidoData', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'EE', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:56:56', 'SYSTEM', '2007-05-31 00:56:56', 'SYSTEM', '3ddc6934-8d85-499b-aa7e-6aa2285a5b81', 'EidoData', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('989', 'OE300418', null, 'A', 'Planetek Italia', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'IT', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:57:02', 'SYSTEM', '2007-05-31 00:57:02', 'SYSTEM', '3df88d79-c4ec-4cc8-8d67-0778031074af', 'Planetek Italia', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('990', 'OE300419', null, 'A', 'STUDIO C ENGINEERING SRL', null, '', null, null, null, '5103591639', null, '5103591639', null, null, null, 'VIA ARCHIMEDE 10', null, null, 'ARBIZZANO DI NEGRAR', null, '37024', 'IT', null, 'VIA ARCHIMEDE 10', null, null, 'ARBIZZANO DI NEGRAR', null, '37024', 'IT', null, 'VIA ARCHIMEDE 10', null, null, 'ARBIZZANO DI NEGRAR', null, '37024', 'IT', null, null, 'ugolini@studioc.it', '390456020779', '390457514805', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2008-05-13 04:00:56', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3e266ab1-43cc-49b7-bba1-4239ea998cb8', 'Studio C Engineering Srl', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('991', 'OE300420', null, 'A', 'STTEI LDA', null, '', null, null, null, '5070188190', null, '5070188190', null, null, null, 'CAMPO GRANDE, 382 C-1º A', null, null, 'LISBOA', null, '1700-097', 'PT', null, 'CAMPO GRANDE, 382 C-1º A', null, null, 'LISBOA', null, '1700-097', 'PT', null, 'CAMPO GRANDE, 382 C-1º A', null, null, 'LISBOA', null, '1700-097', 'PT', null, null, 'nteixeira@sttei.pt', '351217542480', '351217520819', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2008-04-04 11:32:00', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3e2f6600-2934-45ca-8f10-e20e5c02cc0c', 'STTEI LDA', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('992', 'OE300421', null, 'A', 'EPLAN SOFTWARE & SERVICE GMBH & CO. KG', null, '', null, null, null, '5101533760', null, '5101533760', null, null, null, null, null, null, null, null, null, 'DE', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2008-04-04 11:31:37', 'SYSTEM', '2009-04-06 06:31:51', 'janine.franz@autodesk.com', '3e3de0dc-c7ca-49fe-9437-494d23a700d9', 'EPLAN SOFTWARE & SERVICE GMBH & CO. KG', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('993', 'OE300422', null, 'A', 'IT CENTER', null, '', null, null, null, '5101669448', null, '5101669448', null, null, null, 'NEZAVISIMOSTI AV., 86A', null, null, 'MINSK', null, '220012', 'BY', null, 'NEZAVISIMOSTI AV., 86A', null, null, 'MINSK', null, '220012', 'BY', null, 'NEZAVISIMOSTI AV., 86A', null, null, 'MINSK', null, '220012', 'BY', null, null, 'itcenter@itcenter.by', '375172376873', '375172376873', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2008-04-04 11:31:43', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3e4f6ed7-4852-414a-9571-bff6c4a06a89', 'IT CENTER', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('994', 'OE300423', 'ORG2469', 'A', 'KLG SYSTEL LTD', '', '', '', '', '', '5070334982', null, '5070334982', null, '', '', 'PLOT NO 70 A , SECTOR 34, EHTP', '', '', 'GURGAON', '', '122001', 'IN', '', 'PLOT NO 70 A , SECTOR 34, EHTP', '', '', 'GURGAON', '', '122001', 'IN', '', 'PLOT NO 70 A , SECTOR 34, EHTP', '', '', 'GURGAON', '', '122001', 'IN', '', '', 'klg.ho@klgsystel.com; mukesha@klgsystel.com', '9101244129900', '9101244129999', '', '', '', '', '', '', '', '', '', '', ' notes', 'D', '2008-02-07 08:50:08', 'SYSTEM', '2009-08-12 05:24:42', 'janm', '3e689397-46ff-4097-aba9-e97ca28f7933', 'KLG Systel Ltd.', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('995', 'OE300424', 'ORG2563', 'A', 'Point Ltd', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'RU', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'D', '2007-05-31 00:56:37', 'SYSTEM', '2009-04-23 10:56:47', 'marcov', '3f15e701-1667-457a-98f0-50c79b9a1be7', 'Point Ltd', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('996', 'OE300425', null, 'A', 'TeamCAD', null, '', null, null, null, '5101868371', null, '5101868371', null, null, null, null, null, null, null, null, null, 'TN', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2007-05-31 00:57:06', 'SYSTEM', '2007-05-31 00:57:06', 'SYSTEM', '3f50dadf-cdfd-4348-80ce-62eb660a8dad', 'TeamCAD', 'Karen Ruane', '2008-04-01 14:03:44', '2');
INSERT INTO `Main_organization` VALUES ('997', 'OE300426', null, 'A', 'SCHÜCO SERVICE GMBH', null, '', null, null, null, 'S-CAD +JANIsoft', null, 'S-CAD +JANIsoft', null, null, null, 'KAROLINENSTRASSE 1-15', null, null, 'BIELEFELD', null, '33609', 'DE', null, 'KAROLINENSTRASSE 1-15', null, null, 'BIELEFELD', null, '33609', 'DE', null, 'KAROLINENSTRASSE 1-15', null, null, 'BIELEFELD', null, '33609', 'DE', null, null, '', '521783662', '521783850', null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2007-05-31 00:57:05', 'SYSTEM', '2009-05-28 16:25:27', 'SYSTEM', '3f76fdf2-d23e-40e0-b7e0-cad4be265b3f', 'Schüco International KG', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('998', 'OE300427', null, 'A', 'MIS CO. LTD', null, '', null, null, null, '000001', null, '000001', null, null, null, null, null, null, null, null, null, 'NG', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'A', '2008-04-21 08:55:43', 'SYSTEM', '2008-04-21 08:55:43', 'SYSTEM', '3f7ae53f-96b2-4907-92b3-9cb55e790e05', 'MIS CO. LTD', 'Karen Ruane', '2008-04-01 14:03:44', '1');
INSERT INTO `Main_organization` VALUES ('999', 'OE300428', null, 'A', 'SC Matrix SRL', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'RW', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2007-06-06 10:53:18', 'SYSTEM', '2011-07-20 16:35:32', 't_barrw', '3f99e127-c48d-45b4-b8d9-30a98ade5bc5', 'SC Matrix SRL', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('1000', 'OE300429', null, 'A', 'ENERGY GROUP', null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'IT', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, ' notes', 'X', '2007-05-31 00:57:07', 'SYSTEM', '2007-05-31 00:57:07', 'SYSTEM', '40192cc0-5ac8-4afa-9876-60d3c9e455c7', 'ENERGY GROUP', 'Karen Ruane', '2008-04-01 14:03:44', '0');
INSERT INTO `Main_organization` VALUES ('1002', 'admin', 'admin', 'admin', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test', 'o', 'test@gmail.com', 'admin', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'ID', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'ID', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'ID', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', 'test@gmail.com', '', 'test@gmail.com', 'a', '2018-01-16 18:44:49', 'admin', '2018-01-16 18:44:49', 'admin', 'admin', 'admin', 'admin', '2018-01-16 18:44:49', 'adm');
INSERT INTO `Main_organization` VALUES ('1003', 'admin', 'admin', 'admin', 'test', 'test', 'test', 'test', 'o', 'test', 'admin', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'ID', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'ID', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'ID', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'a', '2018-01-19 22:35:28', 'admin', '2018-01-19 22:35:28', 'admin', 'admin', 'admin', 'admin', '2018-01-19 22:35:28', 'adm');

-- ----------------------------
-- Table structure for Main_organization_contact
-- ----------------------------
DROP TABLE IF EXISTS `Main_organization_contact`;
CREATE TABLE `Main_organization_contact` (
  `activity_type_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`activity_type_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_type`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Main_organization_contact
-- ----------------------------
INSERT INTO `Main_organization_contact` VALUES ('2', 'A&P Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Main_organization_contact` VALUES ('4', 'VAR Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Main_organization_contact` VALUES ('3', 'ATC Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');

-- ----------------------------
-- Table structure for Main_organization_site
-- ----------------------------
DROP TABLE IF EXISTS `Main_organization_site`;
CREATE TABLE `Main_organization_site` (
  `activity_type_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`activity_type_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_type`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Main_organization_site
-- ----------------------------
INSERT INTO `Main_organization_site` VALUES ('2', 'A&P Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Main_organization_site` VALUES ('4', 'VAR Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Main_organization_site` VALUES ('3', 'ATC Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');

-- ----------------------------
-- Table structure for Main_site
-- ----------------------------
DROP TABLE IF EXISTS `Main_site`;
CREATE TABLE `Main_site` (
  `SiteIdInt` int(11) NOT NULL AUTO_INCREMENT,
  `SiteId` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `ATCSiteId` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `OrgId` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteStatus_retired` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiebelSiteName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `EnglishSiteName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CommercialSiteName` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Workstations` int(8) DEFAULT '10',
  `MagellanId` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAPNumber_retired` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SAPShipTo_retired` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteTelephone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteFax` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteEmailAddress` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteWebAddress` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteDepartment` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteAddress1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteAddress2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteAddress3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteCity` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteStateProvince` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteCountryCode` char(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SitePostalCode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MailingDepartment` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MailingAddress1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MailingAddress2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MailingAddress3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MailingCity` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MailingStateProvince` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MailingCountryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MailingPostalCode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShippingDepartment` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShippingAddress1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShippingAddress2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShippingAddress3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShippingCity` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShippingStateProvince` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShippingCountryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ShippingPostalCode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiebelDepartment` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiebelAddress1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiebelAddress2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiebelAddress3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiebelCity` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiebelStateProvince` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiebelCountryCode` varchar(2) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiebelPostalCode` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteAdminFirstName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteAdminLastName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteAdminEmailAddress` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteAdminTelephone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteAdminFax` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteManagerFirstName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteManagerLastName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteManagerEmailAddress` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteManagerTelephone` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SiteManagerFax` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `AdminNotes` text COLLATE utf8_unicode_ci,
  `Status` char(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateAdded` datetime DEFAULT NULL,
  `AddedBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `DateLastAdmin` datetime DEFAULT NULL,
  `LastAdminBy` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOLocationUUID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOLocationName` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOLocationNumber` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOAuthorizationCodes` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOUpdatedOn` datetime DEFAULT NULL,
  `CSOVersion` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `CSOpartner_type` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`SiteIdInt`),
  KEY `IDX_SITES_LOCUUID` (`CSOLocationUUID`),
  KEY `IDX_SITES_SiteName` (`SiteName`),
  KEY `IDX_SITES_OrgId` (`OrgId`),
  KEY `IDX_SITES_CountryCode` (`SiteCountryCode`),
  KEY `IDX_SITES_Status` (`Status`),
  KEY `IDX_SITES_ATCID` (`ATCSiteId`),
  KEY `IDX_Sites_SiteStatus` (`SiteStatus_retired`),
  KEY `IDX_Sites_EngName` (`EnglishSiteName`),
  KEY `IDX_Sites_CommName` (`CommercialSiteName`),
  KEY `IDX_Site_State` (`SiteStateProvince`),
  KEY `SiebelSiteName` (`SiebelSiteName`),
  KEY `IDX_Sites_SiteAddr1` (`SiteAddress1`)
) ENGINE=MyISAM AUTO_INCREMENT=11655 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Main_site
-- ----------------------------
INSERT INTO `Main_site` VALUES ('1001', 'EM300336', 'EM0301', 'ORG2788', 'A', 'Institut Technique Infographie, ATC', '', null, '', '10', null, null, null, '0473240604', null, null, null, null, '55 Rue De Malintrat, ', null, null, 'Clermont Ferrand', 'Puy-de-Dome', 'FR', '63100', null, '55 Rue De Malintrat, ', null, null, 'Clermont Ferrand', 'Puy-de-Dome', 'FR', '63100', null, '55 Rue De Malintrat, ', null, null, 'Clermont Ferrand', 'Puy-de-Dome', 'FR', '63100', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, 'D', '2007-05-31 00:56:24', 'SYSTEM', '2009-05-26 08:31:38', 't_barrw', '34b5bd92-2a32-47f1-a568-a9a0b1ddabc2', 'Institut Technique Infographie, ATC', null, null, '2008-04-04 16:33:31', '2', 'atc');
INSERT INTO `Main_site` VALUES ('1002', 'EM300337', null, 'OE301118', 'A', 'ALDEBRA SPA', '', '', '', '10', '', null, '5070263033', '', '', '', '', '', 'VIA GIOTTO 12', '', '', 'BOLZANO', '', 'IT', '39100', '', 'VIA GIOTTO 12', '', '', 'BOLZANO', '', 'IT', '39100', '', 'VIA GIOTTO 12', '', '', 'BOLZANO', '', 'IT', '39100', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, 'A', '2008-03-18 04:28:15', 'SYSTEM', '2010-07-14 08:02:36', 'oscar.fernandezmendiola@autodesk.com', '34ccf775-8835-4c22-80b7-16180c11cca9', 'ALDEBRA SPA', '1', 'no data', '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1003', 'EM300338', null, 'ORG894', 'A', 'Cadpoint Limited', '', null, '', '10', null, null, null, null, null, null, null, null, '34 Wellington Business Park', null, null, 'Crowthorne', '', 'GB', 'RG456LS', null, '34 Wellington Business Park', null, null, 'Crowthorne', '', 'GB', 'RG456LS', null, '34 Wellington Business Park', null, null, 'Crowthorne', '', 'GB', 'RG456LS', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, 'X', '2007-05-31 00:57:03', 'SYSTEM', '2007-05-31 00:57:03', 'SYSTEM', '3530c574-40f8-4eb0-84bd-060efda140f1', 'Cadpoint Limited', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1645', 'EM300980', null, 'OE301506', 'A', 'Neilsoft, Pune', 'NEILSOFT LIMITED PUNE', '', '', '10', '', null, null, '', '', '', '', '', '10/10+A PRIDE PARMAR GALAXY', '8F SADHU VASWANI CHOWK', '', 'PUNE', '', 'IN', '411001', '', '10/10+A PRIDE PARMAR GALAXY', '8F SADHU VASWANI CHOWK', '', 'PUNE', '', 'IN', '411001', '', '10/10+A PRIDE PARMAR GALAXY', '8F SADHU VASWANI CHOWK', '', 'PUNE', '', 'IN', '411001', '', '10/10+A PRIDE PARMAR GALAXY', '8F SADHU VASWANI CHOWK', '', 'PUNE', '', 'IN', '411001', '', '', '', '', '', '', '', '', '', '', null, 'D', '2008-03-24 21:38:59', 'SYSTEM', '2010-06-17 00:00:09', 'pauline.see@autodesk.com', '97eb7537-c5aa-46a0-88b6-54ca9440237f', 'Neilsoft, Pune', null, null, '2008-04-04 16:33:31', '0', 'res');
INSERT INTO `Main_site` VALUES ('1646', 'EM300981', null, 'OE301766', 'A', 'ZOOFITECH CO.LTD', '', null, '', '10', null, null, null, null, null, null, null, null, 'Khurias Road,Exit 13,Opposite Al Jumaih Car Showroom,', null, null, 'Riyadh', '', 'SA', '', null, 'Khurias Road,Exit 13,Opposite Al Jumaih Car Showroom,', null, null, 'Riyadh', '', 'SA', '', null, 'Khurias Road,Exit 13,Opposite Al Jumaih Car Showroom,', null, null, 'Riyadh', '', 'SA', '', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, 'D', '2007-05-31 00:56:19', 'SYSTEM', '2007-05-31 00:56:19', 'SYSTEM', '9816ab64-1bdd-402b-811e-388531be237d', 'ZOOFITECH CO.LTD', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1647', 'EM300982', 'EM0613', 'OE301301', 'A', 'Centro San Luis, ATC', '', null, '', '10', null, null, null, null, null, null, null, null, 'Licenciado Poza 31, ', null, null, 'Bilbao', '', 'ES', '480911', null, 'Licenciado Poza 31, ', null, null, 'Bilbao', '', 'ES', '480911', null, 'Licenciado Poza 31, ', null, null, 'Bilbao', '', 'ES', '480911', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, 'D', '2007-05-31 00:56:40', 'SYSTEM', '2009-05-05 08:06:08', 'janm', '98178cc7-5234-4f6d-9190-8a976d18609c', 'Centro San Luis, ATC', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1648', 'EM300983', null, 'OE300889', 'A', 'Eprom Cad One Team Varese S.r.l', '', '', '', '10', '', null, null, '', '', '', '', '', 'Via Rossini, 11 A2', '', '', 'Buguggiate', '', 'IT', '21020', '', 'Via Rossini, 11 A2', '', '', 'Buguggiate', '', 'IT', '21020', '', 'Via Rossini, 11 A2', '', '', 'Buguggiate', '', 'IT', '21020', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, 'A', '2008-02-07 08:58:51', 'SYSTEM', '2010-05-24 16:36:11', 'jessica.gray@autodesk.com', '9855d603-aad3-4843-addb-b6b04f56ccee', 'Eprom Cad One Team Varese S.r.l', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1649', 'EM300984', 'EM0158', 'OE301313', 'A', 'CESAE - Aveiro, ATC', '', null, '', '10', null, null, null, null, null, null, null, null, 'Campos da Universidade de Aveiro, Edifcio Cefasi - 1', null, null, 'Aveiro', '', 'PT', '3800-193', null, 'Campos da Universidade de Aveiro, Edifcio Cefasi - 1', null, null, 'Aveiro', '', 'PT', '3800-193', null, 'Campos da Universidade de Aveiro, Edifcio Cefasi - 1', null, null, 'Aveiro', '', 'PT', '3800-193', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, 'D', '2007-05-31 00:56:34', 'SYSTEM', '2009-05-26 08:24:24', 't_barrw', '989927a8-5750-4a3a-b372-ad00d95089be', 'CESAE - Aveiro, ATC', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1650', 'EM300985', null, 'OE300986', 'A', 'DIGITECNO SNC', '', null, '', '10', null, null, '5070000401', null, null, null, null, null, 'VIA ACQUAVIVA, 52', null, null, 'TERAMO', '', 'IT', '64100', null, 'VIA ACQUAVIVA, 52', null, null, 'TERAMO', '', 'IT', '64100', null, 'VIA ACQUAVIVA, 52', null, null, 'TERAMO', '', 'IT', '64100', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, 'X', '2008-04-04 11:31:36', 'SYSTEM', '2008-04-04 11:31:36', 'SYSTEM', '98fc2d07-a7f0-4bd9-9dc6-1e18c0dbaab0', 'DIGITECNO SNC', '1', 'no data', '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1651', 'EM300986', 'EM0753', 'OE300340', 'A', 'Geo-IT', '', '', '', '10', null, null, '', '', '', '', '', '', 'Wilrijkstraat 37', '', '', 'Antwerpen', '', 'BE', '2140', '', 'Wilrijkstraat 37', '', '', 'Antwerpen', '', 'BE', '2140', '', 'Wilrijkstraat 37', '', '', 'Antwerpen', '', 'BE', '2140', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, 'D', '2007-05-31 00:57:06', 'SYSTEM', '2009-05-29 11:22:55', 't_barrw', '993df1f8-8dc0-452d-9751-302c9a541f8a', 'Geo-IT', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1652', 'EM300987', null, 'OE300847', 'A', 'ANTLOG AG', '', '', '', '10', '', null, null, '', '', '', '', '', 'Wuerzenbachstrasse 17', '', '', 'Luzern', '', 'CH', '6006', '', 'Wuerzenbachstrasse 17', '', '', 'Luzern', '', 'CH', '6006', '', 'Wuerzenbachstrasse 17', '', '', 'Luzern', '', 'CH', '6006', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, 'A', '2008-02-08 10:51:28', 'SYSTEM', '2010-08-03 21:57:30', 'Shoichi.Kikuchi@autodesk.com', '995b6a00-af0a-4f46-bcd8-15dbc0bc9891', 'ANTLOG AG', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1653', 'EM300988', 'EM0556', 'OE301636', 'A', 'Industrial Automation Institute, ATC', '', '', '', '10', null, '', '', '88462697578', '', '', '', '', 'Pr. Kirova 201, sek. 9,', '', '', 'Samara', '', 'RU', '443035', '', 'Pr. Kirova 201, sek. 9,', '', '', 'Samara', '', 'RU', '443035', '', 'Pr. Kirova 201, sek. 9,', '', '', 'Samara', '', 'RU', '443035', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, 'D', '2007-05-31 00:56:37', 'SYSTEM', '2009-05-22 17:15:36', 't_barrw', '997b58c6-1018-46da-80fd-7452d5e1c968', 'Industrial Automation Institute, ATC', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1654', 'EM300989', null, 'OE300633', 'A', 'ARXES INFORMATION DESIGN BERLIN GMBH', '', null, '', '10', null, null, '5070181651', '01723607807', null, null, null, null, 'MAXSTR 3A', null, null, 'BERLIN', '', 'DE', '13347', null, 'MAXSTR 3A', null, null, 'BERLIN', '', 'DE', '13347', null, 'MAXSTR 3A', null, null, 'BERLIN', '', 'DE', '13347', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, 'X', '2007-05-31 00:56:25', 'SYSTEM', '2009-03-30 11:57:08', 'janine.franz@autodesk.com', '99bb875c-9b8c-4c29-8c56-a073c0c00e87', 'ARXES INFORMATION DESIGN BERLIN GMBH', '1', 'no data', '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1655', 'EM300990', '', 'OE301623', 'A', 'Prota Computers Inc.', 'PROTA', '', '', '10', '', '', '', '+903124905225', '', 'semih.turkdogan@prota.com.tr', '', '', 'Yildiz Mah. Asariye Cad. Serefli Veli Sok. No: 5/5 Besiktas', '', '', 'Istanbul', '', 'TR', '34349', '', 'Yildiz Mah. Asariye Cad. Serefli Veli Sok. No: 5/5 Besiktas', '', '', 'Istanbul', '', 'TR', '34349', '', 'Yildiz Mah. Asariye Cad. Serefli Veli Sok. No: 5/5 Besiktas', '', '', 'Istanbul', '', 'TR', '34349', '', 'TURAN GÜNES BULVARI', '', '', 'ANKARA', '', 'TR', '6550', 'Semih', 'Türkdoğan', 'semih.turkdogan@prota.com.tr', '903124905225', '903124905452', 'Semih', 'Türkdoğan', 'semih.turkdogan@prota.com.tr', '903124905225', '903124905452', null, 'A', '2007-05-31 00:56:59', 'SYSTEM', '2017-07-29 09:03:04', 'nitin.balyan@vinsys.in', '99e883e7-123c-4a98-83a7-ae687c6837b9', 'Prota Bilgisayar', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1656', 'EM300991', 'EM0691', 'OE300310', 'A', 'Infoling Ltd.', '', '', '', '10', null, '', '', '+73912368495', '', '', '', '', 'BUILD 5, GLADKOVA STR., OFFICE 43.', '', '', 'KRASNOYARSK', '', 'RU', '660016', '', 'BUILD 5, GLADKOVA STR., OFFICE 43.', '', '', 'KRASNOYARSK', '', 'RU', '660016', '', 'BUILD 5, GLADKOVA STR., OFFICE 43.', '', '', 'KRASNOYARSK', '', 'RU', '660016', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, 'D', '2007-05-31 00:56:47', 'SYSTEM', '2009-04-20 06:30:32', 'janm', '99f3007c-bc13-4e33-bb54-968ca1b05af1', 'Infoling Ltd, ATC', null, null, '2008-04-04 16:33:31', '2', 'res,atc');
INSERT INTO `Main_site` VALUES ('1993', 'EM301328', '', 'OE300568', 'A', 'ALLCAD CONSULT LTD', 'ALLCAD CONSULT LTD', '', '', '10', null, null, '5070314989', '23412630810', '', '', '', '', '3/5 boyle str. Onikan. Lagos', '', '', 'LAGOS', '', 'NG', 'Unspecified', '', '3/5 boyle str. Onikan. Lagos', '', '', 'LAGOS', '', 'NG', 'Unspecified', '', '3/5 boyle str. Onikan. Lagos', '', '', 'LAGOS', '', 'NG', 'Unspecified', '', '3/5 BOYLE STREET, ONIKAN', '', '', 'LAGOS', '', 'NG', 'Unspecified', '', '', '', '', '', '', '', '', '', '', null, 'A', '2007-05-31 00:56:56', 'SYSTEM', '2009-06-04 11:02:05', 'janm', 'cf579be3-765c-4e50-b550-2ecbee3526bc', 'ALLCAD CONSULT LTD', '1', 'no data', '2008-04-04 16:33:31', '0', 'res');
INSERT INTO `Main_site` VALUES ('1994', 'EM301329', null, 'OE301185', 'A', 'Daten-Kontor Kft.', 'DATEN-KONTOR LTD.', '', '', '10', '', null, null, '3672552918', '', '', '', '', 'Sz', '', '', 'P', '', 'HU', '7633', '', 'Sz', '', '', 'P', '', 'HU', '7633', '', 'Sz', '', '', 'P', '', 'HU', '7633', '', 'SZÁNTO KOVÁCS J. UTCA 3.', '', '', 'PÉCS', '', 'HU', '7633', '', '', '', '', '', '', '', '', '', '', null, 'A', '2007-05-31 00:57:02', 'SYSTEM', '2010-09-30 08:44:24', 'oscar.fernandezmendiola@autodesk.com', 'cf5c193a-cb24-46af-9076-1e86b8d019c3', 'Daten-Kontor Kft.', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1995', 'EM301330', 'EM0306', 'OE300295', 'A', 'Integrated Business Systems', '', '', '', '10', null, null, '', '', '', '', '', '', '36 Triq ir-Russett', '', '', 'Kappara', '', 'MT', 'SGN4433', '', '36 Triq ir-Russett', '', '', 'Kappara', '', 'MT', 'SGN4433', '', '36 Triq ir-Russett', '', '', 'Kappara', '', 'MT', 'SGN4433', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, 'D', '2007-08-21 10:49:23', 'SYSTEM', '2009-05-27 10:26:28', 't_barrw', 'cf8d7a15-f017-4604-8546-bd727b0a7db5', 'integrated Business Systems, ATC', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1996', 'EM301331', null, 'OE300792', 'A', 'Dataspazio SpA', '', '', '', '10', '', null, null, '', '', '', '', '', 'via Stanislao Cannizzaro 71,', '', '', 'Roma', '', 'IT', '00156', '', 'via Stanislao Cannizzaro 71,', '', '', 'Roma', '', 'IT', '00156', '', 'via Stanislao Cannizzaro 71,', '', '', 'Roma', '', 'IT', '00156', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, 'A', '2007-05-31 00:56:58', 'SYSTEM', '2010-05-21 15:01:23', 'jessica.gray@autodesk.com', 'cfcdf476-aeb0-4642-b2b2-5b6628ab9d90', 'Dataspazio SpA', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1997', 'EM301332', 'EM0352', 'OE300762', 'A', 'Mark Ingegno Srl Hicad-dsc Consultant', '', null, '', '10', null, null, null, '0553987045', null, null, null, null, 'Via Canali 3 ', null, null, 'San Casciano Val di Pesa (Firenze)', '', 'IT', '50026', null, 'Via Canali 3 ', null, null, 'San Casciano Val di Pesa (Firenze)', '', 'IT', '50026', null, 'Via Canali 3 ', null, null, 'San Casciano Val di Pesa (Firenze)', '', 'IT', '50026', '', '', '', '', '', '', '', '', null, null, null, null, null, null, null, null, null, null, null, 'D', '2007-05-31 00:57:07', 'SYSTEM', '2009-05-29 11:30:22', 't_barrw', 'cfd32499-7651-4f4a-ae9c-67d6fb3fe60a', 'Mark Ingegno Srl (hicad-dsc consultant)', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1998', 'EM301333', 'EM0458', 'OE300327', 'A', 'SOFTLINE', '', '', '', '10', null, null, '5070268186', '+74952320023', '', '', '', '', '47A Vavilova Str', '', '', 'Moscow', '', 'RU', '117312', '', '8, GUBKINA STR.', '', '', 'Moscow', '', 'RU', '119991', '', '47A Vavilova Str', '', '', 'Moscow', '', 'RU', '117312', '', '', '', '', '', '', '', '', 'Elena', 'Arzhenykh', 'elenaar@softline.ru', '', '', 'Elena', 'Arzhenykh', 'elenaar@softline.ru', '', '', 'ATC CSN 5104192061', 'D', '2007-05-31 00:56:37', 'SYSTEM', '2009-04-13 07:19:18', 'janm', 'cfe3ff9f-37e0-48bd-8e5d-cc73e3322958', 'SOFTLINE', '1', 'no data', '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('1999', 'EM301334', null, 'OE300035', 'A', 'APLIKOM 2001 sp. z o.o.', 'APLIKOM SP. Z.O.O.', '', '', '10', '', null, null, '48422881600', '', '', '', '', 'Obywatelska 137', '', '', 'Łódź', '', 'PL', '94-104', '', 'Obywatelska 137', '', '', 'Łódź', '', 'PL', '94-104', '', 'Obywatelska 137', '', '', 'Łódź', '', 'PL', '94-104', '', 'OBYWATELSKA 137', '', '', 'ŁÓDŹ', '', 'PL', '94-104', '', '', '', '', '', '', '', '', '', '', null, 'A', '2007-05-31 00:56:49', 'SYSTEM', '2010-09-24 03:44:24', 'anna.kozdranska-gruszka@autodesk.com', 'cff9c62d-300f-49d9-8910-aa1099ddd766', 'APLIKOM 2001 sp. z o.o.', null, null, '2008-04-04 16:33:31', '1', 'res');
INSERT INTO `Main_site` VALUES ('2000', 'EM301335', null, 'OE300483', 'A', 'NETA', '', '', '', '10', '', null, null, '', '', '', '', '', 'Prospect K.Marks 20 korpus 2', '', '', 'Novosibirsk', '', 'RU', '630092', '', 'Prospect K.Marks 20 korpus 2', '', '', 'Novosibirsk', '', 'RU', '630092', '', 'Prospect K.Marks 20 korpus 2', '', '', 'Novosibirsk', '', 'RU', '630092', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', null, 'A', '2007-05-31 00:56:19', 'SYSTEM', '2010-05-26 08:28:08', 'janine.franz@autodesk.com', 'd05b5fab-6d8a-4362-b664-15fce197567d', 'NETA', null, null, '2008-04-04 16:33:31', '1', 'res');

-- ----------------------------
-- Table structure for Main_site_contact
-- ----------------------------
DROP TABLE IF EXISTS `Main_site_contact`;
CREATE TABLE `Main_site_contact` (
  `activity_type_id` int(8) NOT NULL AUTO_INCREMENT,
  `activity_type` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`activity_type_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`activity_type`)
) ENGINE=MyISAM AUTO_INCREMENT=119 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Main_site_contact
-- ----------------------------
INSERT INTO `Main_site_contact` VALUES ('2', 'A&P Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Main_site_contact` VALUES ('4', 'VAR Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Main_site_contact` VALUES ('3', 'ATC Invoice Types', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');

-- ----------------------------
-- Table structure for MarketType
-- ----------------------------
DROP TABLE IF EXISTS `MarketType`;
CREATE TABLE `MarketType` (
  `MarketTypeId` int(11) NOT NULL AUTO_INCREMENT,
  `MarketType` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`MarketTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of MarketType
-- ----------------------------
INSERT INTO `MarketType` VALUES ('1', 'N/A');
INSERT INTO `MarketType` VALUES ('2', 'Emerging Markets');
INSERT INTO `MarketType` VALUES ('3', 'Mature Markets');

-- ----------------------------
-- Table structure for Partner_type
-- ----------------------------
DROP TABLE IF EXISTS `Partner_type`;
CREATE TABLE `Partner_type` (
  `RoleId` int(10) NOT NULL AUTO_INCREMENT,
  `RoleCode` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `RoleName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SiebelName` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `FrameworkName` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `CRBApprovedName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `RoleType` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `RoleSubType` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`RoleId`),
  KEY `IDX_Roles_RoleName` (`RoleName`),
  KEY `IDX_Roles_RoleCode` (`RoleCode`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Partner_type
-- ----------------------------
INSERT INTO `Partner_type` VALUES ('1', 'ATC', 'Authorized Training Center (ATC)', '', '', '', 'Company', '', 'Authorized Training Center (ATC)');
INSERT INTO `Partner_type` VALUES ('2', 'VAR', 'Value Added Reseller (VAR)', '', '', '', 'Company', '', 'Value Added Reseller (VAR)');
INSERT INTO `Partner_type` VALUES ('3', 'Educational', 'Education Sales', '', '', '', 'Company', '', 'Education Sales');
INSERT INTO `Partner_type` VALUES ('4', 'VAD', 'Value Added Distributor (VAD)', '', '', '', 'Company', '', 'Value Added Distributor (VAD)');
INSERT INTO `Partner_type` VALUES ('5', 'Government', 'Government Sales', '', '', '', 'Company', '', 'Government Sales');
INSERT INTO `Partner_type` VALUES ('6', 'Autodesk', 'Autodesk Employee', '', '', '', 'Company', '', 'Autodesk Employee');
INSERT INTO `Partner_type` VALUES ('7', 'AuthorsPublishers', 'Authors and Publishers', '', '', '', 'Company', '', 'Authors and Publishers');
INSERT INTO `Partner_type` VALUES ('8', 'Certification', 'Certification Site', '', '', '', 'Company', '', 'Certification Site');
INSERT INTO `Partner_type` VALUES ('9', 'ADN', 'Autodesk Developer Network (ADN)', '', '', '', 'Company', '', 'Autodesk Developer Network (ADN)');
INSERT INTO `Partner_type` VALUES ('10', 'NRO', 'Retail', '', '', '', 'Company', '', 'Retail');
INSERT INTO `Partner_type` VALUES ('11', 'OwnerManager', 'Owner/Principal', 'Owner/Principal', 'OTHER', 'SALES', 'Contact', '', 'Owner Manager');
INSERT INTO `Partner_type` VALUES ('12', 'SalesManagement', 'Sales Manager', 'Sales Management', 'SALES', 'SALES', 'Contact', '', 'Sales Manager');
INSERT INTO `Partner_type` VALUES ('13', 'SalesRepresentative', 'Sales Representative', 'Sales Representative ', 'SALES', 'SALES', 'Contact', '', 'Sales Representative');
INSERT INTO `Partner_type` VALUES ('15', 'TechnicalPreSales', 'Technical Representative', 'Technical Representative', 'TECHNICAL', 'TECHNICAL', 'Contact', '', 'Technical Pre-Sales');
INSERT INTO `Partner_type` VALUES ('18', 'MarketingManager', 'Marketing', 'Marketing', 'MARKETING', 'OTHER', 'Contact', '', 'Marketing Manager');
INSERT INTO `Partner_type` VALUES ('19', 'AdminOE', 'Admin/Order Entry', 'Admin OE', 'OTHER', 'OTHER', 'Contact', '', 'Admin/Order Entry');
INSERT INTO `Partner_type` VALUES ('23', 'InstructorApplicationEngineer', 'Instructor', 'Training', 'SUPPORT', 'SUPPORT', 'Contact', '', 'Instructor');
INSERT INTO `Partner_type` VALUES ('29', 'Unassigned', 'Unassigned', '', '', 'OTHER', 'Contact', '', 'Unassigned');
INSERT INTO `Partner_type` VALUES ('30', 'Contractor', 'Autodesk Contractor', '', '', '', 'Company', '', 'Autodesk Contractor');
INSERT INTO `Partner_type` VALUES ('31', 'DirectSales', 'Named/Major Account/Direct Sales', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Major/Direct Sales');
INSERT INTO `Partner_type` VALUES ('32', 'VerticalSales', 'Vertical Sales', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Vertical Sales');
INSERT INTO `Partner_type` VALUES ('33', 'ChannelSales', 'Channel/Distribution Sales', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Channel Sales');
INSERT INTO `Partner_type` VALUES ('34', 'InternalAppEngineer', 'Application Engineer', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Application Engineer');
INSERT INTO `Partner_type` VALUES ('35', 'InternalOther', 'Other Sales', '', '', '', 'Contact', 'Internal', 'Other Sales');
INSERT INTO `Partner_type` VALUES ('36', 'SolutionExecutive', 'Solution Executive', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Solution Executive ');
INSERT INTO `Partner_type` VALUES ('37', 'ConsultingSpec', 'Consulting', '', '', '', 'Contact', 'Specialist', 'Consulting');
INSERT INTO `Partner_type` VALUES ('38', 'FactorySales', 'Factory Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Factory Sales');
INSERT INTO `Partner_type` VALUES ('39', 'ProcessPlantDesignSales', 'Process Plant Design Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Process Plant Design Sales');
INSERT INTO `Partner_type` VALUES ('40', 'BESystemsSales', 'Building Engineering - MEP Systems Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Building Engineering - MEP Systems Sales');
INSERT INTO `Partner_type` VALUES ('41', 'BEStructuralSales', 'Building Engineering - Structural Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Building Engineering - Structural Sales');
INSERT INTO `Partner_type` VALUES ('42', 'FactoryTechnical', 'Factory Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Factory Technical');
INSERT INTO `Partner_type` VALUES ('43', 'ProcessPlantDesignTechnical', 'Process Plant Design Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Process Plant Design Technical');
INSERT INTO `Partner_type` VALUES ('44', 'BESystemsTechnical', 'Building Engineering - MEP Systems Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Building Engineering - MEP Systems Technical');
INSERT INTO `Partner_type` VALUES ('45', 'BEStructuralTechnical', 'Building Engineering - Structural Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Building Engineering - Structural Technical');
INSERT INTO `Partner_type` VALUES ('46', 'InsideSales', 'Inside Sales', '', '', '', 'Contact', 'Internal', 'Inside Sales');
INSERT INTO `Partner_type` VALUES ('47', 'InternalSalesManager', 'Internal Sales Manager', '', '', '', 'Contact', 'Internal', 'Internal Sales Manager');
INSERT INTO `Partner_type` VALUES ('48', 'EditorialFinishingSales', 'Editorial Finishing Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Editorial Finishing Sales');
INSERT INTO `Partner_type` VALUES ('49', 'EditorialFinishingTechnical', 'Editorial Finishing Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Editorial Finishing Technical');
INSERT INTO `Partner_type` VALUES ('50', 'SimulationSales', 'Simulation Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Simulation Sales');
INSERT INTO `Partner_type` VALUES ('51', 'SimulationTechnical', 'Simulation Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Simulation Technical');
INSERT INTO `Partner_type` VALUES ('52', 'VCP', 'VCP', '', 'SALES', 'SALES', 'Contact', '', 'VCP');
INSERT INTO `Partner_type` VALUES ('53', 'AITC', 'Autodesk Internal Training Center (AITC)', '', '', '', 'Company', '', 'Autodesk Internal Training Center (AITC)');
INSERT INTO `Partner_type` VALUES ('54', 'RED', 'Regional Education Distributor (RED)', '', '', '', 'Company', '', 'Regional Education Distributor (RED)');
INSERT INTO `Partner_type` VALUES ('55', 'MED', 'Master Education Distributor (MED)', '', '', '', 'Company', '', 'Master Education Distributor (MED)');
INSERT INTO `Partner_type` VALUES ('58', 'AAP', 'Authorized Academic Partner (AAP)', '', '', '', 'Company', '', 'Authorized Academic Partner (AAP)');
INSERT INTO `Partner_type` VALUES ('60', 'MTP', 'Membership Training Partner (MTP)', '', '', '', 'Company', '', 'Membership Training Partner (MTP)');

-- ----------------------------
-- Table structure for Product
-- ----------------------------
DROP TABLE IF EXISTS `Product`;
CREATE TABLE `Product` (
  `product_id` int(8) NOT NULL AUTO_INCREMENT,
  `product_category` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Invoice',
  `product_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `product_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`product_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`product_category`)
) ENGINE=MyISAM AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Product
-- ----------------------------
INSERT INTO `Product` VALUES ('114', 'Geospatial Products', 'MAP3DGEO2007-', 'Map 3D 2007 (Geospatial)', null, null, '0001-01-01 00:00:00', null, '0001-01-01 00:00:00');
INSERT INTO `Product` VALUES ('115', 'Geospatial Products', 'MAP3DGEO2006', 'Map 3D 2006 (Geospatial)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('2', 'Geospatial Products', 'MAP3DGEO2005', 'Map 3D 2005 (Geospatial)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('116', 'Geospatial Products', 'MAP3DGEO2004', 'Map 2004 (Geospatial)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('3', 'Geospatial Products', 'MAPGUIDEGEO2007', 'MapGuide Enterprise 2007 (Geospatial)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('4', 'Geospatial Products', 'MAPGUIDEGEO6.5', 'MapGuide 6.5 (Geospatial)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('5', 'Geospatial Products', 'MAPGUIDEGEO6', 'MapGuide 6 (Geospatial)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('6', 'Geospatial Products', 'TOPOBASE', 'Topobase', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('9', 'Geospatial Products', '3DSMAX8', '3ds Max 8', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('12', 'Geospatial Products', '3DSMAX7', '3ds Max 7', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('15', 'Geospatial Products', '3DSMAX6', '3ds Max 6', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('117', 'Geospatial Products', 'BURN2LINUX', 'Burn 2 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('18', 'Geospatial Products', 'BURN1LINUX', 'Burn 1 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('19', 'Geospatial Products', 'CLEANER6MAC', 'Cleaner 6 Mac OS X', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('20', 'Geospatial Products', 'CLEANERXLWIN', 'Cleaner XL 1 Windows', 'A', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('21', 'Geospatial Products', 'COMBUST4MAC', 'Combustion 4 Mac OS X', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('22', 'Geospatial Products', 'COMBUST4WIN', 'Combustion 4 Windows', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('23', 'Geospatial Products', 'COMBUST3MAC', 'Combustion 3 Mac OS X', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('24', 'Geospatial Products', 'COMBUST3WIN', 'Combustion 3 Windows', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('25', 'Geospatial Products', 'FBX', 'FBX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('26', 'Geospatial Products', 'MAYA7LINUX', 'Maya 7 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('27', 'Geospatial Products', 'MAYA7MAC', 'Maya 7 Mac OS X', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('28', 'Geospatial Products', 'MAYA7WIN', 'Maya 7 Windows', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('29', 'Geospatial Products', 'MOTION7MAC', 'MotionBuilder 7 Mac OS X', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('30', 'Geospatial Products', 'MOTION7WIN', 'MotionBuilder 7 Windows', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('31', 'Geospatial Products', 'TOXIC1LINUX', 'Toxik 1 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('32', 'Geospatial Products', 'TOXIC1WIN', 'Toxik 1 Windows', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('33', 'Geospatial Products', 'VIZ2007MEDIA', 'VIZ 2007 (Media & Entertainment)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('34', 'Geospatial Products', 'VIZ2006MEDIA', 'VIZ 2006 (Media & Entertainment)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('35', 'Geospatial Products', 'VIZ2005MEDIA', 'VIZ 2005 (Media & Entertainment)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('36', 'Geospatial Products', 'WIRETAP2IRIX', 'Wiretap 2 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('37', 'Media & Entertainment Products', 'WIRETAP2LINUX', 'Wiretap 2 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('38', 'Media & Entertainment Products', 'WIRETAP2MAC', 'Wiretap 2 Mac OS X', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('39', 'Media & Entertainment Products', 'WIRETAP2WIN', 'Wiretap 2 Windows', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('40', 'Media & Entertainment Products', 'FIRE7IRIX', 'Discreet Fire 7 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('41', 'Media & Entertainment Products', 'FIRE6IRIX', 'Discreet Fire 6 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('42', 'Media & Entertainment Products', 'FLAME9IRIX', 'Discreet Flame 9 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('43', 'Media & Entertainment Products', 'FLAME9LINUX', 'Discreet Flame 9 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('44', 'Media & Entertainment Products', 'FLAME8IRIX', 'Discreet Flame 8 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('45', 'Media & Entertainment Products', 'FLINT9IRIX', 'Discreet Flint 9 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('46', 'Media & Entertainment Products', 'FLINT9LINUX', 'Discreet Flint 9 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('47', 'Media & Entertainment Products', 'FLINT8IRIX', 'Discreet Flint 8 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('48', 'Media & Entertainment Products', 'FLINT8LINUX', 'Discreet Flint 8 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('49', 'Media & Entertainment Products', 'INFERNO6IRIX', 'Discreet Inferno 6 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('50', 'Media & Entertainment Products', 'INFERNO6LINUX', 'Discreet Inferno 6 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('51', 'Media & Entertainment Products', 'INFERNO5IRIX', 'Discreet Inferno 5 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('52', 'Media & Entertainment Products', 'LUSTRE2LINUX', 'Discreet Lustre 2 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('53', 'Media & Entertainment Products', 'LUSTRE2WIN', 'Discreet Lustre 2 Windows', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('54', 'Media & Entertainment Products', 'LUSTRE1WIN', 'Discreet Lustre 1 Windows', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('55', 'Media & Entertainment Products', 'SMOKE7IRIX', 'Discreet Smoke 7 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('56', 'Media & Entertainment Products', 'SMOKE7LINUX', 'Discreet Smoke 7 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('57', 'Media & Entertainment Products', 'SMOKE6IRIX', 'Discreet Smoke 6 IRIX', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('58', 'Media & Entertainment Products', 'SMOKE6LINUX', 'Discreet Smoke 6 Linux', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('59', 'Media & Entertainment Products', 'ACAD2007', 'AutoCAD 2007', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('60', 'Media & Entertainment Products', 'ACAD2006', 'AutoCAD 2006', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('61', 'Media & Entertainment Products', 'ACAD2005', 'AutoCAD 2005', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('62', 'Media & Entertainment Products', 'ACAD2004', 'AutoCAD 2004', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('63', 'Media & Entertainment Products', 'ARCH2007', 'Architectural Desktop 2007', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('64', 'Media & Entertainment Products', 'ARCH2006', 'Architectural Desktop 2006', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('65', 'Media & Entertainment Products', 'ARCH2005', 'Architectural Desktop 2005', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('66', 'Media & Entertainment Products', 'ARCH2004', 'Architectural Desktop 2004', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('67', 'Media & Entertainment Products', 'BUILD2007', 'Building Systems 2007', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('68', 'Media & Entertainment Products', 'BUILD2006', 'Building Systems 2006', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('69', 'Media & Entertainment Products', 'BUILD2005', 'Building Systems 2005', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('70', 'Media & Entertainment Products', 'BUILD2004', 'Building Systems 2004', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('71', 'Media & Entertainment Products', 'BUZZ', 'Buzzsaw', 'A', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('72', 'Media & Entertainment Products', 'CIVIL2007', 'AutoCAD Civil 3D 2007', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('73', 'Media & Entertainment Products', 'CIVIL2006', 'AutoCAD Civil 3D 2006', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('74', 'Media & Entertainment Products', 'CIVIL2005', 'Civil 3D 2005', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('75', 'Media & Entertainment Products', 'CIVILDESIGN2006', 'Civil Design 2006', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('76', 'Media & Entertainment Products', 'CIVILDESIGN2005', 'Civil Design 2005', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('77', 'Media & Entertainment Products', 'CIVILDESIGN2004', 'Civil Design 2004', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('78', 'Media & Entertainment Products', 'DESIGN', 'Design Review (DWF Composer)', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('79', 'Media & Entertainment Products', 'AUTOSTUDIO13', 'AutoStudio 13', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('80', 'Media & Entertainment Products', 'DESIGNSTUDIO13', 'Design Studio 13', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('81', 'Media & Entertainment Products', 'STUDIO13', 'Studio 13', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('82', 'Media & Entertainment Products', 'SURFACESTUDIO13', 'SurfaceStudio 13', 'X', 'admin', '2018-01-13 00:00:00', 'admin', '2018-01-13 00:00:00');
INSERT INTO `Product` VALUES ('127', 'CAD Products', 'aq', 'aq', '', 'admin', '2018-01-30 17:29:34', 'admin', '2018-01-30 17:29:34');
INSERT INTO `Product` VALUES ('125', 'ACI Certification Products', 'ty', 'ty', '', 'admin', '2018-01-30 17:26:26', 'admin', '2018-01-30 17:26:26');

-- ----------------------------
-- Table structure for Product_category
-- ----------------------------
DROP TABLE IF EXISTS `Product_category`;
CREATE TABLE `Product_category` (
  `product_category_id` int(8) NOT NULL AUTO_INCREMENT,
  `product_category` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`product_category_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`product_category`)
) ENGINE=MyISAM AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Product_category
-- ----------------------------
INSERT INTO `Product_category` VALUES ('2', 'Geospatial Products', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Product_category` VALUES ('4', 'Media & Entertainment Products', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Product_category` VALUES ('3', 'CAD Products', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');
INSERT INTO `Product_category` VALUES ('119', 'ACI Certification Products', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:53:20');

-- ----------------------------
-- Table structure for Ref_countries
-- ----------------------------
DROP TABLE IF EXISTS `Ref_countries`;
CREATE TABLE `Ref_countries` (
  `countries_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `territory_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subregion_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `countries_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `countries_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countries_tel` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `cuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `muid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  PRIMARY KEY (`countries_id`),
  KEY `IDX_Countries_Country` (`countries_name`),
  KEY `IDX_Countries_Geo` (`geo_code`),
  KEY `IDX_Countries_Region` (`region_code`),
  KEY `IDX_Countries_Subregion` (`subregion_code`)
) ENGINE=MyISAM AUTO_INCREMENT=212 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='NOTIFY EXPERTUS WHEN VALUES CHANGE';

-- ----------------------------
-- Records of Ref_countries
-- ----------------------------
INSERT INTO `Ref_countries` VALUES ('1', 'E', 'EE', 'EE', 'MidEast', 'AF', 'Afganistan', '93', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('2', 'E', 'CE', 'CE', 'CEE', 'AL', 'Albania', '355', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('3', 'E', 'SE', 'SE', 'FR', 'DZ', 'Algeria', '213', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('4', 'E', 'SE', 'SE', 'IB', 'AD', 'Andorra', '376', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('5', 'E', 'EE', 'EE', 'Africa', 'AO', 'Angola', '244', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('6', 'A', 'LA', 'LA', 'LAN', 'AI', 'Anguilla', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('7', 'A', 'LA', 'LA', 'LAS', 'AR', 'Argentina', '54', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('8', 'E', 'EE', 'EE', 'CIS', 'AM', 'Armenia', '374', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('9', 'A', 'AZ', 'LA', 'LAN', 'AW', 'Aruba', '297', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('10', 'P', 'CE', 'AZ', 'AU', 'AU', 'Australia', '61', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('11', 'E', 'SA', 'CE', 'DACH', 'AT', 'Austria', '43', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('12', 'E', 'NE', 'EE', 'CIS', 'AZ', 'Azerbaijan', '994', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('13', 'A', 'NE', 'LA', 'LAN', 'BS', 'Bahamas', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('14', 'E', 'LA', 'EE', 'MidEast', 'BH', 'Bahrain', '973', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('15', 'P', 'NE', 'SA', 'SAARC', 'BD', 'Bangladesh', '880', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('16', 'A', 'AS', 'LA', 'LAN', 'BB', 'Barbados', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('17', 'E', 'CA', 'EE', 'CIS', 'BY', 'Belarus', '375', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('18', 'E', 'CN', 'NE', 'BEN', 'BE', 'Belgium', '32', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('19', 'A', 'EE', 'LA', 'LAN', 'BZ', 'Belize', '501', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('20', 'E', 'CN', 'EE', 'Africa', 'BJ', 'Benin', '229', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('21', 'A', 'SA', 'LA', 'LAN', 'BM', 'Bermuda', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('22', 'P', 'SE', 'SA', 'SAARC', 'BT', 'Bhutan', '975', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('23', 'A', 'JP', 'LA', 'LAS', 'BO', 'Bolivia', '591', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('24', 'E', 'LA', 'CE', 'CEE', 'BA', 'Bosnia Herzegovina', '387', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('25', 'E', 'KO', 'EE', 'Africa', 'BW', 'Botswana', '267', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('26', 'E', 'CN', 'NE', 'Nordic', 'BV', 'Bouvet Island', '47', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('27', 'A', 'US', 'LA', 'BR', 'BR', 'Brazil', '55', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('28', 'E', 'EE', 'NE', 'GB', 'IO', 'British Indian Ocean', '246', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('29', 'A', 'CE', 'LA', 'LAN', 'VG', 'British Virgin Islands', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('30', 'P', 'SE', 'AS', 'ASEAN', 'BN', 'Brunei', '673', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('31', 'E', 'SE', 'CE', 'CEE', 'BG', 'Bulgaria', '359', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('32', 'E', 'EE', 'EE', 'Africa', 'BF', 'Burkina Faso', '226', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('33', 'E', 'LA', 'EE', 'Africa', 'BI', 'Burundi', '257', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('34', 'E', 'LA', 'EE', 'Africa', 'CM', 'Cameroon', '237', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('35', 'A', 'EE', 'CA', 'CA', 'CA', 'Canada', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('36', 'E', 'AZ', 'EE', 'Africa', 'CV', 'Cape Verde', '238', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('37', 'A', 'CE', 'LA', 'LAN', 'KY', 'Cayman Islands', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('38', 'E', 'SA', 'EE', 'Africa', 'CF', 'Central African Republic', '236', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('39', 'E', 'NE', 'EE', 'Africa', 'TD', 'Chad', '235', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('40', 'A', 'NE', 'LA', 'LAS', 'CL', 'Chile', '56', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('41', 'G', 'LA', 'CN', 'CN', 'CN', 'China', '86', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('42', 'P', 'NE', 'AZ', 'AU', 'CC', 'Coconut Islands', '61', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('43', 'A', 'AS', 'LA', 'LAN', 'CO', 'Colombia', '57', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('44', 'E', 'CA', 'EE', 'Africa', 'KM', 'Comoros', '269', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('45', 'A', 'CN', 'LA', 'LAN', 'CR', 'Costa Rica', '506', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('46', 'E', 'EE', 'CE', 'CEE', 'HR', 'Croatia', '385', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('47', 'E', 'CN', 'EE', 'MED', 'CY', 'Cyprus', '357', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('48', 'E', 'SA', 'CE', 'CEE', 'CZ', 'Czech Republic', '420', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('49', 'E', 'SE', 'EE', 'Africa', 'CD', 'Dem Rep of Congo', '243', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('50', 'E', 'JP', 'NE', 'Nordic', 'DK', 'Denmark', '45', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('51', 'E', 'LA', 'EE', 'Africa', 'DJ', 'Djibouti', '253', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('52', 'A', 'KO', 'LA', 'LAN', 'DM', 'Dominica', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('53', 'A', 'CN', 'LA', 'LAN', 'EC', 'Ecuador', '593', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('54', 'E', 'US', 'EE', 'MidEast', 'EG', 'Egypt', '20', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('55', 'A', 'EE', 'LA', 'LAN', 'SV', 'El Salvador', '503', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('56', 'E', 'CE', 'EE', 'Africa', 'GQ', 'Equatorial Guinea', '240', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('57', 'E', 'SE', 'EE', 'Africa', 'ER', 'Eritrea', '291', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('58', 'E', 'SE', 'NE', 'Nordic', 'EE', 'Estonia', '372', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('59', 'E', 'EE', 'EE', 'Africa', 'ET', 'Ethiopia', '251', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('60', 'E', 'LA', 'NE', 'Nordic', 'FO', 'Faeroe', '298', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('61', 'E', 'LA', 'NE', 'GB', 'FK', 'Falkland Islands', '500', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('62', 'P', 'EE', 'AZ', 'AU', 'FJ', 'Fiji', '679', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('63', 'E', 'AZ', 'NE', 'Nordic', 'FI', 'Finland', '358', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('64', 'E', 'CE', 'SE', 'FR', 'FR', 'France', '33', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('65', 'A', 'SA', 'LA', 'LAN', 'GF', 'French Guiana', '594', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('66', 'E', 'NE', 'EE', 'Africa', 'TF', 'French Southern Territory', '689', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('67', 'E', 'NE', 'EE', 'Africa', 'GA', 'Gabon', '241', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('68', 'E', 'LA', 'EE', 'Africa', 'GM', 'Gambia', '220', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('69', 'E', 'NE', 'EE', 'CIS', 'GE', 'Georgia', '995', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('70', 'E', 'AS', 'CE', 'DACH', 'DE', 'Germany', '49', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('71', 'E', 'CA', 'EE', 'Africa', 'GH', 'Ghana', '233', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('72', 'E', 'CN', 'SE', 'IB', 'GI', 'Gibraltar', '350', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('73', 'E', 'EE', 'EE', 'MED', 'GR', 'Greece', '30', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('74', 'E', 'CN', 'NE', 'Nordic', 'GL', 'Greenland', '299', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('75', 'A', 'SA', 'LA', 'LAN', 'GD', 'Grenada', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('76', 'E', 'SE', 'SE', 'FR', 'GP', 'Guadeloupe', '590', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('77', 'A', 'JP', 'LA', 'LAN', 'GT', 'Guatemala', '502', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('78', 'E', 'LA', 'EE', 'Africa', 'GN', 'Guinea', '224', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('79', 'E', 'KO', 'EE', 'Africa', 'GW', 'Guinea-Bissau', '245', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('80', 'A', 'CN', 'LA', 'LAN', 'GY', 'Guyana', '592', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('81', 'A', 'US', 'LA', 'LAN', 'HT', 'Haiti', '509', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('82', 'A', 'EE', 'LA', 'LAN', 'HN', 'Honduras', '504', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('83', 'G', 'CE', 'CN', 'HK', 'HK', 'Hong Kong', '852', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('84', 'E', 'SE', 'CE', 'CEE', 'HU', 'Hungary', '36', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('85', 'E', 'SE', 'NE', 'Nordic', 'IS', 'Iceland', '354', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('86', 'P', 'EE', 'SA', 'IN', 'IN', 'India', '91', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('87', 'P', 'LA', 'AS', 'ASEAN', 'ID', 'Indonesia', '62', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('88', 'E', 'LA', 'EE', 'MidEast', 'IQ', 'Iraq', '964', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('89', 'E', 'EE', 'NE', 'GB', 'IE', 'Ireland', '353', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('90', 'E', 'AZ', 'EE', 'MED', 'IL', 'Israel', '972', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('91', 'E', 'CE', 'SE', 'IT', 'IT', 'Italy', '39', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('92', 'E', 'SA', 'EE', 'Africa', 'CI', 'Ivory Coast', '225', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('93', 'A', 'NE', 'LA', 'LAN', 'JM', 'Jamaica', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('94', 'P', 'NE', 'JP', 'JP', 'JP', 'Japan', '81', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('95', 'E', 'LA', 'EE', 'MidEast', 'JO', 'Jordan', '962', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('96', 'E', 'NE', 'EE', 'CIS', 'KZ', 'Kazakhstan', '7', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('97', 'E', 'AS', 'EE', 'Africa', 'KE', 'Kenya', '254', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('98', 'E', 'CA', 'EE', 'MidEast', 'KW', 'Kuwait', '965', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('99', 'E', 'CN', 'EE', 'CIS', 'KG', 'Kyrgyzstan ', '996', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('100', 'E', 'EE', 'NE', 'Nordic', 'LV', 'Latvia', '371', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('101', 'E', 'CN', 'EE', 'MidEast', 'LB', 'Lebanon', '961', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('102', 'E', 'SA', 'EE', 'Africa', 'LS', 'Lesotho', '266', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('103', 'E', 'SE', 'EE', 'Africa', 'LR', 'Liberia', '231', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('104', 'E', 'JP', 'EE', 'MidEast', 'LY', 'Libya', '218', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('105', 'E', 'LA', 'CE', 'DACH', 'LI', 'Liechtenstein', '423', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('106', 'E', 'KO', 'NE', 'Nordic', 'LT', 'Lithuania', '370', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('107', 'E', 'CN', 'NE', 'BEN', 'LU', 'Luxembourg', '352', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('108', 'G', 'US', 'CN', 'CN', 'MO', 'Macao', '853', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('109', 'E', 'EE', 'CE', 'CEE', 'MK', 'Macedonia', '389', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('110', 'E', 'CE', 'EE', 'Africa', 'MG', 'Madagascar', '261', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('111', 'E', 'SE', 'EE', 'Africa', 'MW', 'Malawi', '265', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('112', 'P', 'SE', 'AS', 'ASEAN', 'MY', 'Malaysia', '60', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('113', 'E', 'EE', 'EE', 'Africa', 'ML', 'Mali', '223', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('114', 'E', 'LA', 'EE', 'MED', 'MT', 'Malta', '356', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('115', 'P', 'LA', 'AZ', 'AU', 'MH', 'Marshall Islands', '692', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('116', 'A', 'EE', 'LA', 'LAN', 'MQ', 'Martinique', '596', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('117', 'E', 'AZ', 'EE', 'Africa', 'MR', 'Mauratania', '222', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('118', 'E', 'CE', 'EE', 'Africa', 'MU', 'Mauritius', '230', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('119', 'E', 'SA', 'EE', 'Africa', 'YT', 'Mayotte', '262', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('120', 'A', 'NE', 'LA', 'MX', 'MX', 'Mexico', '52', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('121', 'E', 'NE', 'CE', 'CEE', 'MD', 'Moldova', '373', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('122', 'E', 'LA', 'SE', 'FR', 'MC', 'Monaco', '377', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('123', 'E', 'NE', 'NE', 'GB', 'MS', 'Montserrat', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('124', 'E', 'AS', 'SE', 'FR', 'MA', 'Morocco', '212', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('125', 'E', 'CA', 'EE', 'Africa', 'MZ', 'Mozambique', '258', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('126', 'P', 'CN', 'AS', 'ASEAN', 'MM', 'Myanmar', '95', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('127', 'E', 'EE', 'EE', 'Africa', 'NA', 'Namibia', '264', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('128', 'P', 'CN', 'SA', 'SAARC', 'NP', 'Nepal', '977', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('129', 'E', 'SA', 'NE', 'BEN', 'NL', 'Netherlands', '31', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('130', 'A', 'SE', 'LA', 'LAS', 'AN', 'Netherlands Antilles', '599', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('131', 'E', 'JP', 'SE', 'FR', 'NC', 'New Caledonia', '687', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('132', 'P', 'LA', 'AZ', 'AU', 'NZ', 'New Zealand', '64', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('133', 'A', 'KO', 'LA', 'LAN', 'NI', 'Nicaragua', '505', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('134', 'E', 'CN', 'EE', 'Africa', 'NE', 'Niger', '227', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('135', 'E', 'US', 'EE', 'Africa', 'NG', 'Nigeria', '234', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('136', 'P', 'EE', 'KO', 'KO', 'KO', 'North Korea', '850', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('137', 'E', 'CE', 'NE', 'Nordic', 'NO', 'Norway', '47', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('138', 'E', 'SE', 'EE', 'MidEast', 'OM', 'Oman', '968', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('139', 'P', 'SE', 'SA', 'SAARC', 'PK', 'Pakistan', '92', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('140', 'E', 'EE', 'EE', 'MidEast', 'PS', 'Palestinian Territory', '970', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('141', 'A', 'LA', 'LA', 'LAN', 'PA', 'Panama', '507', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('142', 'A', 'LA', 'LA', 'LAS', 'PY', 'Paraguay', '595', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('143', 'A', 'EE', 'LA', 'LAS', 'PE', 'Peru', '51', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('144', 'P', 'AZ', 'AS', 'ASEAN', 'PH', 'Philippines', '63', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('145', 'E', 'CE', 'CE', 'CEE', 'PL', 'Poland', '48', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('146', 'E', 'SA', 'SE', 'IB', 'PT', 'Portugal', '351', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('147', 'A', 'NE', 'LA', 'LAN', 'PR', 'Puerto Rico', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('148', 'E', 'NE', 'EE', 'MidEast', 'QA', 'Qatar', '974', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('149', 'A', 'LA', 'LA', 'LAN', 'DO', 'Republic Dominica', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('150', 'E', 'NE', 'EE', 'Africa', 'CG', 'Republic of Congo', '242', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('151', 'E', 'AS', 'EE', 'Africa', 'RE', 'Reunion', '262', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('152', 'E', 'CA', 'CE', 'CEE', 'RO', 'Romania', '40', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('153', 'E', 'CN', 'EE', 'CIS', 'RU', 'Russian Federation', '7', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('154', 'E', 'EE', 'EE', 'Africa', 'RW', 'Rwanda', '250', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('155', 'E', 'CN', 'SE', 'IT', 'SM', 'San Marino', '378', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('156', 'E', 'SA', 'EE', 'Africa', 'ST', 'Sao Tome and Principe', '239', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('157', 'E', 'SE', 'EE', 'MidEast', 'SA', 'Saudi Arabia', '966', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('158', 'E', 'JP', 'EE', 'Africa', 'SN', 'Senegal', '221', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('159', 'E', 'LA', 'CE', 'CEE', 'RS', 'Serbia', '381', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('160', 'E', 'KO', 'EE', 'Africa', 'SC', 'Seychelles', '248', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('161', 'E', 'CN', 'EE', 'Africa', 'SL', 'Sierra Leone', '232', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('162', 'P', 'US', 'AS', 'ASEAN', 'SG', 'Singapore', '65', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('163', 'E', 'EE', 'CE', 'CEE', 'SK', 'Slovakia', '421', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('164', 'E', 'CE', 'CE', 'CEE', 'SI', 'Slovenia', '386', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('165', 'E', 'SE', 'EE', 'Africa', 'SO', 'Somalia', '252', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('166', 'E', 'SE', 'EE', 'Africa', 'ZA', 'South Africa', '27', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('167', 'P', 'EE', 'KO', 'KO', 'KR', 'South Korea', '82', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('168', 'E', 'LA', 'SE', 'IB', 'ES', 'Spain', '34', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('169', 'P', 'LA', 'SA', 'SAARC', 'LK', 'Sri Lanka', '94', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('170', 'A', 'EE', 'LA', 'LAN', 'VC', 'St Vincent', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('171', 'E', 'AZ', 'EE', 'Africa', 'SH', 'St. Helena', '290', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('172', 'A', 'CE', 'LA', 'LAN', 'LC', 'St. Lucia', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('173', 'A', 'SA', 'LA', 'LAN', 'SR', 'Surinam', '597', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('174', 'E', 'NE', 'NE', 'Nordic', 'SJ', 'Svalbard', '47', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('175', 'E', 'NE', 'EE', 'Africa', 'SZ', 'Swaziland', '268', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('176', 'E', 'LA', 'NE', 'Nordic', 'SE', 'Sweden', '46', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('177', 'E', 'NE', 'CE', 'DACH', 'CH', 'Switzerland', '41', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('178', 'E', 'AS', 'EE', 'MidEast', 'SY', 'Syria', '963', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('179', 'E', 'CA', 'EE', 'CIS', 'TJ', 'Tadzhikistan', '992', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('180', 'G', 'CN', 'CN', 'TW', 'TW', 'Taiwan', '886', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('181', 'E', 'EE', 'EE', 'Africa', 'TZ', 'Tanzania', '255', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('182', 'P', 'CN', 'AS', 'ASEAN', 'TH', 'Thailand', '66', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('183', 'E', 'SA', 'EE', 'Africa', 'TG', 'Togo', '228', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('184', 'A', 'SE', 'LA', 'LAN', 'TT', 'Trinidad & Tobago', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('185', 'E', 'JP', 'SE', 'FR', 'TN', 'Tunisia', '216', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('186', 'E', 'LA', 'EE', 'MED', 'TR', 'Turkey', '90', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('187', 'E', 'KO', 'EE', 'CIS', 'TM', 'Turkmenistan', '993', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('188', 'A', 'CN', 'LA', 'LAN', 'VI', 'U.S. Virgin Islands', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('189', 'E', 'US', 'EE', 'Africa', 'UG', 'Uganda', '256', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('190', 'E', 'EE', 'EE', 'CIS', 'UA', 'Ukraine', '380', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('191', 'E', 'CE', 'EE', 'MidEast', 'AE', 'United Arab Emirates', '971', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('192', 'E', 'SE', 'NE', 'GB', 'GB', 'United Kingdom', '44', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('193', 'A', 'SE', 'US', 'US', 'US', 'United States', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('194', 'A', 'EE', 'LA', 'LAS', 'UY', 'Uruguay', '598', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('195', 'E', 'LA', 'EE', 'CIS', 'UZ', 'Uzbekistan', '998', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('196', 'E', 'LA', 'SE', 'IT', 'VA', 'Vatican City State', '39', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('197', 'A', 'EE', 'LA', 'LAN', 'VE', 'Venezuela', '58', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('198', 'P', 'AZ', 'AS', 'ASEAN', 'VN', 'Vietnam', '84', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('199', 'E', 'CE', 'SE', 'FR', 'EH', 'Western Sahara', '212', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('200', 'E', 'SA', 'EE', 'MidEast', 'YE', 'Yemen', '967', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('201', 'E', 'NE', 'EE', 'Africa', 'ZM', 'Zambia', '260', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('202', 'E', 'NE', 'EE', 'Africa', 'ZW', 'Zimbabwe', '263', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('203', 'P', 'LA', 'SA', 'SAARC', 'MV', 'Maldives', '960', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('204', 'E', 'NE', 'CE', 'CEE', 'ME', 'Montenegro', '382', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('205', 'A', 'AS', 'US', 'US', 'GU', 'Guam', '1', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('206', 'G', 'CA', 'CN', 'CN', 'MN', 'Mongolia', '976', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');
INSERT INTO `Ref_countries` VALUES ('207', 'E', 'CN', 'CE', 'CEE', 'KS', 'Kosovo', '381', 'admin', '2018-01-16 13:34:01', 'admin', '2018-01-16 13:34:11');

-- ----------------------------
-- Table structure for Ref_geo
-- ----------------------------
DROP TABLE IF EXISTS `Ref_geo`;
CREATE TABLE `Ref_geo` (
  `geo_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `geo_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `muid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  PRIMARY KEY (`geo_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Ref_geo
-- ----------------------------
INSERT INTO `Ref_geo` VALUES ('1', 'AMER', 'AMER', 'admin', '2018-01-16 12:53:55', 'admin', '2018-01-16 12:54:01');
INSERT INTO `Ref_geo` VALUES ('2', 'APAC', 'APAC (excl. GCR)', 'admin', '2018-01-16 12:53:55', 'admin', '2018-01-16 12:54:01');
INSERT INTO `Ref_geo` VALUES ('3', 'EMEA', 'EMEA', 'admin', '2018-01-16 12:53:55', 'admin', '2018-01-16 12:54:01');
INSERT INTO `Ref_geo` VALUES ('4', 'GCR', 'GCR', 'admin', '2018-01-16 12:53:55', 'admin', '2018-01-16 12:54:01');

-- ----------------------------
-- Table structure for Ref_language
-- ----------------------------
DROP TABLE IF EXISTS `Ref_language`;
CREATE TABLE `Ref_language` (
  `LanguageID` int(11) NOT NULL,
  `Name` varchar(25) NOT NULL,
  `EnabledInReports` tinyint(4) NOT NULL,
  `ReportOrder` int(11) DEFAULT NULL,
  `UICulture` varchar(10) NOT NULL,
  `AskInfo` tinyint(4) NOT NULL,
  `PostSurveyEnabled` tinyint(4) NOT NULL,
  `EnabledInSurvey` tinyint(4) NOT NULL,
  PRIMARY KEY (`LanguageID`),
  KEY `IX_tblLanguages` (`UICulture`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of Ref_language
-- ----------------------------
INSERT INTO `Ref_language` VALUES ('1', 'Bulgarian', '1', '1', 'bg-BG', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('2', 'Chinese - Simplified', '1', '2', 'zh-CHS', '1', '1', '1');
INSERT INTO `Ref_language` VALUES ('3', 'Chinese - Traditional', '1', '3', 'zh-CHT', '1', '1', '1');
INSERT INTO `Ref_language` VALUES ('4', 'Czech', '1', '5', 'cs-CZ', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('5', 'Danish', '1', '6', 'da-DK', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('6', 'Dutch', '1', '7', 'nl-NL', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('7', 'English - North America', '1', '10', 'en-US', '1', '1', '1');
INSERT INTO `Ref_language` VALUES ('8', 'Estonian', '1', '13', 'et-EE', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('9', 'Finnish', '1', '14', 'fi-FI', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('10', 'French - Europe', '1', '16', 'fr-FR', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('11', 'German', '1', '17', 'de-DE', '1', '1', '1');
INSERT INTO `Ref_language` VALUES ('12', 'Greek', '1', '18', 'el-GR', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('13', 'Hungarian', '1', '19', 'hu-HU', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('14', 'Icelandic', '1', '20', 'is-IS', '1', '0', '0');
INSERT INTO `Ref_language` VALUES ('15', 'Indonesian', '1', '21', 'id-ID', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('16', 'Italian', '1', '22', 'it-IT', '1', '1', '1');
INSERT INTO `Ref_language` VALUES ('17', 'Japanese', '1', '23', 'ja-JP', '0', '1', '1');
INSERT INTO `Ref_language` VALUES ('18', 'Korean', '1', '24', 'ko-KR', '1', '1', '1');
INSERT INTO `Ref_language` VALUES ('19', 'Latvian', '1', '25', 'lv-LV', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('20', 'Lithuanian', '1', '26', 'lt-LT', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('21', 'Macedonian', '1', '27', 'mk-MK', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('22', 'Norwegian', '1', '28', 'nn-NO', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('23', 'Polish', '1', '29', 'pl-PL', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('24', 'Portuguese - Europe', '1', '31', 'pt-PT', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('25', 'Romanian', '1', '32', 'ro-RO', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('26', 'Russian', '1', '33', 'ru-RU', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('27', 'Croatian', '1', '4', 'hr-HR', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('28', 'Slovenian', '1', '35', 'sl-SI', '1', '0', '0');
INSERT INTO `Ref_language` VALUES ('29', 'Spanish - Europe', '1', '36', 'es-ES', '1', '1', '1');
INSERT INTO `Ref_language` VALUES ('30', 'Swedish', '1', '38', 'sv-SE', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('31', 'Turkish', '1', '40', 'tr-TR', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('32', 'Thai', '1', '39', 'th-TH', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('33', 'Serbian', '1', '34', 'sr-Cyrl-CS', '1', '0', '0');
INSERT INTO `Ref_language` VALUES ('34', 'English - Europe', '1', '9', 'en-GB', '1', '1', '1');
INSERT INTO `Ref_language` VALUES ('35', 'English - UK', '1', '12', 'en-GB', '1', '1', '1');
INSERT INTO `Ref_language` VALUES ('36', 'English - South Africa', '0', '11', 'en-ZA', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('37', 'English - APac', '0', '8', 'en-PH', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('38', 'French - Canada', '0', '15', 'fr-CA', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('39', 'Spanish - Latin America', '0', '37', 'es-MX', '1', '0', '1');
INSERT INTO `Ref_language` VALUES ('40', 'Portuguese - Brazil', '0', '30', 'pt-BR', '1', '0', '1');

-- ----------------------------
-- Table structure for Ref_region
-- ----------------------------
DROP TABLE IF EXISTS `Ref_region`;
CREATE TABLE `Ref_region` (
  `region_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `territory_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `muid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  PRIMARY KEY (`region_id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Ref_region
-- ----------------------------
INSERT INTO `Ref_region` VALUES ('1', 'EMEA', 'EE', 'Emerging Countries', 'MidEast', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('2', 'EMEA', 'CE', 'Central Europe', 'CEE', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('3', 'EMEA', 'SE', 'South Europe', 'FR', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('4', 'EMEA', 'SE', 'South Europe', 'IB', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('5', 'EMEA', 'EE', 'Emerging Countries', 'Africa', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('6', 'AMER', 'LA', 'Latin America', 'LAN', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('7', 'AMER', 'LA', 'Latin America', 'LAS', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('8', 'EMEA', 'EE', 'Emerging Countries', 'CIS', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('9', 'APAC (excl. GCR)', 'AZ', 'ANZ', 'AU', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('10', 'EMEA', 'CE', 'Central Europe', 'DACH', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('11', 'APAC (excl. GCR)', 'SA', 'India and SAARC', 'SAARC', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('12', 'EMEA', 'NE', 'North Europe', 'BEN', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('13', 'EMEA', 'NE', 'North Europe', 'Nordic', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('14', 'AMER', 'LA', 'Latin America', 'BR', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('15', 'EMEA', 'NE', 'North Europe', 'GB', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('16', 'APAC (excl. GCR)', 'AS', 'ASEAN', 'ASEAN', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('17', 'AMER', 'CA', 'Canada', 'CA', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('18', 'GCR', 'CN', 'Greater China', 'CN', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('19', 'EMEA', 'EE', 'Emerging Countries', 'MED', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('20', 'GCR', 'CN', 'Greater China', 'HK', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('21', 'APAC (excl. GCR)', 'SA', 'India and SAARC', 'IN', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('22', 'EMEA', 'SE', 'South Europe', 'IT', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('23', 'APAC (excl. GCR)', 'JP', 'Japan', 'JP', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('24', 'AMER', 'LA', 'Latin America', 'MX', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('25', 'APAC (excl. GCR)', 'KO', 'Korea', 'KO', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('26', 'GCR', 'CN', 'Greater China', 'TW', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');
INSERT INTO `Ref_region` VALUES ('27', 'AMER', 'US', 'US', 'US', 'admin', '2018-01-16 13:11:20', 'admin', '2018-01-16 13:11:29');

-- ----------------------------
-- Table structure for Ref_subcountries
-- ----------------------------
DROP TABLE IF EXISTS `Ref_subcountries`;
CREATE TABLE `Ref_subcountries` (
  `subcountries_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `territory_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subregion_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `countries_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `subcountries_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subcountries_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `muid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  PRIMARY KEY (`subcountries_id`),
  KEY `IDX_Countries_Country` (`subcountries_name`),
  KEY `IDX_Countries_Geo` (`geo_code`),
  KEY `IDX_Countries_Region` (`region_code`),
  KEY `IDX_Countries_Subregion` (`subregion_code`)
) ENGINE=MyISAM AUTO_INCREMENT=213 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='NOTIFY EXPERTUS WHEN VALUES CHANGE';

-- ----------------------------
-- Records of Ref_subcountries
-- ----------------------------
INSERT INTO `Ref_subcountries` VALUES ('1', 'E', 'AMER', 'EE', 'MidEast', 'AF', 'AF', 'Afganistan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('2', 'E', 'AMER', 'CE', 'CEE', 'AL', 'AF', 'Albania', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('3', 'E', 'AMER', 'SE', 'FR', 'DZ', 'AF', 'Algeria', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('4', 'E', 'AMER', 'SE', 'IB', 'AD', 'AF', 'Andorra', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('5', 'E', 'AMER', 'EE', 'Africa', 'AO', 'AF', 'Angola', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('6', 'A', 'AMER', 'LA', 'LAN', 'AI', 'AF', 'Anguilla', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('7', 'A', 'AMER', 'LA', 'LAS', 'AR', 'AF', 'Argentina', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('8', 'E', 'AMER', 'EE', 'CIS', 'AM', 'AF', 'Armenia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('9', 'A', 'AMER', 'LA', 'LAN', 'AW', 'AF', 'Aruba', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('10', 'P', 'AMER', 'AZ', 'AU', 'AU', 'AF', 'Australia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('11', 'E', 'AMER', 'CE', 'DACH', 'AT', 'AF', 'Austria', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('12', 'E', 'AMER', 'EE', 'CIS', 'AZ', 'AF', 'Azerbaijan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('13', 'A', 'AMER', 'LA', 'LAN', 'BS', 'AF', 'Bahamas', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('14', 'E', 'AMER', 'EE', 'MidEast', 'BH', 'AF', 'Bahrain', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('15', 'P', 'AMER', 'SA', 'SAARC', 'BD', 'AF', 'Bangladesh', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('16', 'A', 'AMER', 'LA', 'LAN', 'BB', 'AF', 'Barbados', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('17', 'E', 'AMER', 'EE', 'CIS', 'BY', 'AF', 'Belarus', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('18', 'E', 'AMER', 'NE', 'BEN', 'BE', 'AF', 'Belgium', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('19', 'A', 'AMER', 'LA', 'LAN', 'BZ', 'AF', 'Belize', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('20', 'E', 'AMER', 'EE', 'Africa', 'BJ', 'AF', 'Benin', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('21', 'A', 'AMER', 'LA', 'LAN', 'BM', 'AF', 'Bermuda', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('22', 'P', 'AMER', 'SA', 'SAARC', 'BT', 'AF', 'Bhutan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('23', 'A', 'AMER', 'LA', 'LAS', 'BO', 'AF', 'Bolivia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('24', 'E', 'AMER', 'CE', 'CEE', 'BA', 'AF', 'Bosnia Herzegovina', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('25', 'E', 'AMER', 'EE', 'Africa', 'BW', 'AF', 'Botswana', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('26', 'E', 'AMER', 'NE', 'Nordic', 'BV', 'AF', 'Bouvet Island', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('27', 'A', 'AMER', 'LA', 'BR', 'BR', 'AF', 'Brazil', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('28', 'E', 'AMER', 'NE', 'GB', 'IO', 'AF', 'British Indian Ocean', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('29', 'A', 'AMER', 'LA', 'LAN', 'VG', 'AF', 'British Virgin Islands', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('30', 'P', 'AMER', 'AS', 'ASEAN', 'BN', 'AF', 'Brunei', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('31', 'E', 'AMER', 'CE', 'CEE', 'BG', 'AF', 'Bulgaria', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('32', 'E', 'AMER', 'EE', 'Africa', 'BF', 'AF', 'Burkina Faso', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('33', 'E', 'AMER', 'EE', 'Africa', 'BI', 'AF', 'Burundi', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('34', 'E', 'AMER', 'EE', 'Africa', 'CM', 'AF', 'Cameroon', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('35', 'A', 'AMER', 'CA', 'CA', 'CA', 'AF', 'Canada', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('36', 'E', 'AMER', 'EE', 'Africa', 'CV', 'AF', 'Cape Verde', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('37', 'A', 'AMER', 'LA', 'LAN', 'KY', 'AF', 'Cayman Islands', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('38', 'E', 'AMER', 'EE', 'Africa', 'CF', 'AF', 'Central African Republic', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('39', 'E', 'AMER', 'EE', 'Africa', 'TD', 'AF', 'Chad', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('40', 'A', 'AMER', 'LA', 'LAS', 'CL', 'AF', 'Chile', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('41', 'G', 'AMER', 'CN', 'CN', 'CN', 'AF', 'China', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('42', 'P', 'AMER', 'AZ', 'AU', 'CC', 'AF', 'Coconut Islands', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('43', 'A', 'AMER', 'LA', 'LAN', 'CO', 'AF', 'Colombia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('44', 'E', 'AMER', 'EE', 'Africa', 'KM', 'AF', 'Comoros', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('45', 'A', 'AMER', 'LA', 'LAN', 'CR', 'AF', 'Costa Rica', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('46', 'E', 'AMER', 'CE', 'CEE', 'HR', 'AF', 'Croatia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('47', 'E', 'AMER', 'EE', 'MED', 'CY', 'AF', 'Cyprus', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('48', 'E', 'AMER', 'CE', 'CEE', 'CZ', 'AF', 'Czech Republic', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('49', 'E', 'AMER', 'EE', 'Africa', 'CD', 'AF', 'Dem Rep of Congo', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('50', 'E', 'AMER', 'NE', 'Nordic', 'DK', 'AF', 'Denmark', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('51', 'E', 'AMER', 'EE', 'Africa', 'DJ', 'AF', 'Djibouti', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('52', 'A', 'AMER', 'LA', 'LAN', 'DM', 'AF', 'Dominica', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('53', 'A', 'AMER', 'LA', 'LAN', 'EC', 'AF', 'Ecuador', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('54', 'E', 'AMER', 'EE', 'MidEast', 'EG', 'AF', 'Egypt', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('55', 'A', 'AMER', 'LA', 'LAN', 'SV', 'AF', 'El Salvador', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('56', 'E', 'AMER', 'EE', 'Africa', 'GQ', 'AF', 'Equatorial Guinea', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('57', 'E', 'AMER', 'EE', 'Africa', 'ER', 'AF', 'Eritrea', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('58', 'E', 'AMER', 'NE', 'Nordic', 'EE', 'AF', 'Estonia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('59', 'E', 'AMER', 'EE', 'Africa', 'ET', 'AF', 'Ethiopia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('60', 'E', 'AMER', 'NE', 'Nordic', 'FO', 'AF', 'Faeroe', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('61', 'E', 'AMER', 'NE', 'GB', 'FK', 'AF', 'Falkland Islands', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('62', 'P', 'AMER', 'AZ', 'AU', 'FJ', 'AF', 'Fiji', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('63', 'E', 'AMER', 'NE', 'Nordic', 'FI', 'AF', 'Finland', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('64', 'E', 'AMER', 'SE', 'FR', 'FR', 'AF', 'France', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('65', 'A', 'AMER', 'LA', 'LAN', 'GF', 'AF', 'French Guiana', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('66', 'E', 'AMER', 'EE', 'Africa', 'TF', 'AF', 'French Southern Territory', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('67', 'E', 'AMER', 'EE', 'Africa', 'GA', 'AF', 'Gabon', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('68', 'E', 'AMER', 'EE', 'Africa', 'GM', 'AF', 'Gambia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('69', 'E', 'AMER', 'EE', 'CIS', 'GE', 'AF', 'Georgia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('70', 'E', 'AMER', 'CE', 'DACH', 'DE', 'AF', 'Germany', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('71', 'E', 'AMER', 'EE', 'Africa', 'GH', 'AF', 'Ghana', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('72', 'E', 'AMER', 'SE', 'IB', 'GI', 'AF', 'Gibraltar', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('73', 'E', 'AMER', 'EE', 'MED', 'GR', 'AF', 'Greece', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('74', 'E', 'AMER', 'NE', 'Nordic', 'GL', 'AF', 'Greenland', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('75', 'A', 'AMER', 'LA', 'LAN', 'GD', 'AF', 'Grenada', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('76', 'E', 'AMER', 'SE', 'FR', 'GP', 'AF', 'Guadeloupe', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('77', 'A', 'AMER', 'LA', 'LAN', 'GT', 'AF', 'Guatemala', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('78', 'E', 'AMER', 'EE', 'Africa', 'GN', 'AF', 'Guinea', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('79', 'E', 'AMER', 'EE', 'Africa', 'GW', 'AF', 'Guinea-Bissau', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('80', 'A', 'AMER', 'LA', 'LAN', 'GY', 'AF', 'Guyana', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('81', 'A', 'AMER', 'LA', 'LAN', 'HT', 'AF', 'Haiti', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('82', 'A', 'AMER', 'LA', 'LAN', 'HN', 'AF', 'Honduras', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('83', 'G', 'AMER', 'CN', 'HK', 'HK', 'AF', 'Hong Kong', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('84', 'E', 'AMER', 'CE', 'CEE', 'HU', 'AF', 'Hungary', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('85', 'E', 'AMER', 'NE', 'Nordic', 'IS', 'AF', 'Iceland', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('86', 'P', 'AMER', 'SA', 'IN', 'IN', 'AF', 'India', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('87', 'P', 'AMER', 'AS', 'ASEAN', 'ID', 'AF', 'Indonesia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('88', 'E', 'AMER', 'EE', 'MidEast', 'IQ', 'AF', 'Iraq', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('89', 'E', 'AMER', 'NE', 'GB', 'IE', 'AF', 'Ireland', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('90', 'E', 'AMER', 'EE', 'MED', 'IL', 'AF', 'Israel', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('91', 'E', 'AMER', 'SE', 'IT', 'IT', 'AF', 'Italy', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('92', 'E', 'AMER', 'EE', 'Africa', 'CI', 'AF', 'Ivory Coast', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('93', 'A', 'AMER', 'LA', 'LAN', 'JM', 'AF', 'Jamaica', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('94', 'P', 'AMER', 'JP', 'JP', 'JP', 'AF', 'Japan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('95', 'E', 'AMER', 'EE', 'MidEast', 'JO', 'AF', 'Jordan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('96', 'E', 'AMER', 'EE', 'CIS', 'KZ', 'AF', 'Kazakhstan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('97', 'E', 'AMER', 'EE', 'Africa', 'KE', 'AF', 'Kenya', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('98', 'E', 'AMER', 'EE', 'MidEast', 'KW', 'AF', 'Kuwait', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('99', 'E', 'AMER', 'EE', 'CIS', 'KG', 'AF', 'Kyrgyzstan ', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('100', 'E', 'AMER', 'NE', 'Nordic', 'LV', 'AF', 'Latvia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('101', 'E', 'AMER', 'EE', 'MidEast', 'LB', 'AF', 'Lebanon', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('102', 'E', 'AMER', 'EE', 'Africa', 'LS', 'AF', 'Lesotho', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('103', 'E', 'AMER', 'EE', 'Africa', 'LR', 'AF', 'Liberia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('104', 'E', 'AMER', 'EE', 'MidEast', 'LY', 'AF', 'Libya', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('105', 'E', 'AMER', 'CE', 'DACH', 'LI', 'AF', 'Liechtenstein', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('106', 'E', 'AMER', 'NE', 'Nordic', 'LT', 'AF', 'Lithuania', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('107', 'E', 'AMER', 'NE', 'BEN', 'LU', 'AF', 'Luxembourg', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('108', 'G', 'AMER', 'CN', 'CN', 'MO', 'AF', 'Macao', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('109', 'E', 'AMER', 'CE', 'CEE', 'MK', 'AF', 'Macedonia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('110', 'E', 'AMER', 'EE', 'Africa', 'MG', 'AF', 'Madagascar', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('111', 'E', 'AMER', 'EE', 'Africa', 'MW', 'AF', 'Malawi', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('112', 'P', 'AMER', 'AS', 'ASEAN', 'MY', 'AF', 'Malaysia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('113', 'E', 'AMER', 'EE', 'Africa', 'ML', 'AF', 'Mali', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('114', 'E', 'AMER', 'EE', 'MED', 'MT', 'AF', 'Malta', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('115', 'P', 'AMER', 'AZ', 'AU', 'MH', 'AF', 'Marshall Islands', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('116', 'A', 'AMER', 'LA', 'LAN', 'MQ', 'AF', 'Martinique', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('117', 'E', 'AMER', 'EE', 'Africa', 'MR', 'AF', 'Mauratania', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('118', 'E', 'AMER', 'EE', 'Africa', 'MU', 'AF', 'Mauritius', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('119', 'E', 'AMER', 'EE', 'Africa', 'YT', 'AF', 'Mayotte', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('120', 'A', 'AMER', 'LA', 'MX', 'MX', 'AF', 'Mexico', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('121', 'E', 'AMER', 'CE', 'CEE', 'MD', 'AF', 'Moldova', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('122', 'E', 'AMER', 'SE', 'FR', 'MC', 'AF', 'Monaco', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('123', 'E', 'AMER', 'NE', 'GB', 'MS', 'AF', 'Montserrat', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('124', 'E', 'AMER', 'SE', 'FR', 'MA', 'AF', 'Morocco', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('125', 'E', 'AMER', 'EE', 'Africa', 'MZ', 'AF', 'Mozambique', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('126', 'P', 'AMER', 'AS', 'ASEAN', 'MM', 'AF', 'Myanmar', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('127', 'E', 'AMER', 'EE', 'Africa', 'NA', 'AF', 'Namibia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('128', 'P', 'AMER', 'SA', 'SAARC', 'NP', 'AF', 'Nepal', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('129', 'E', 'AMER', 'NE', 'BEN', 'NL', 'AF', 'Netherlands', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('130', 'A', 'AMER', 'LA', 'LAS', 'AN', 'AF', 'Netherlands Antilles', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('131', 'E', 'AMER', 'SE', 'FR', 'NC', 'AF', 'New Caledonia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('132', 'P', 'AMER', 'AZ', 'AU', 'NZ', 'AF', 'New Zealand', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('133', 'A', 'AMER', 'LA', 'LAN', 'NI', 'AF', 'Nicaragua', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('134', 'E', 'AMER', 'EE', 'Africa', 'NE', 'AF', 'Niger', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('135', 'E', 'AMER', 'EE', 'Africa', 'NG', 'AF', 'Nigeria', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('136', 'P', 'AMER', 'KO', 'KO', 'KO', 'AF', 'North Korea', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('137', 'E', 'AMER', 'NE', 'Nordic', 'NO', 'AF', 'Norway', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('138', 'E', 'AMER', 'EE', 'MidEast', 'OM', 'AF', 'Oman', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('139', 'P', 'AMER', 'SA', 'SAARC', 'PK', 'AF', 'Pakistan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('140', 'E', 'AMER', 'EE', 'MidEast', 'PS', 'AF', 'Palestinian Territory', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('141', 'A', 'AMER', 'LA', 'LAN', 'PA', 'AF', 'Panama', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('142', 'A', 'AMER', 'LA', 'LAS', 'PY', 'AF', 'Paraguay', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('143', 'A', 'AMER', 'LA', 'LAS', 'PE', 'AF', 'Peru', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('144', 'P', 'AMER', 'AS', 'ASEAN', 'PH', 'AF', 'Philippines', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('145', 'E', 'AMER', 'CE', 'CEE', 'PL', 'AF', 'Poland', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('146', 'E', 'AMER', 'SE', 'IB', 'PT', 'AF', 'Portugal', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('147', 'A', 'AMER', 'LA', 'LAN', 'PR', 'AF', 'Puerto Rico', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('148', 'E', 'AMER', 'EE', 'MidEast', 'QA', 'AF', 'Qatar', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('149', 'A', 'AMER', 'LA', 'LAN', 'DO', 'AF', 'Republic Dominica', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('150', 'E', 'AMER', 'EE', 'Africa', 'CG', 'AF', 'Republic of Congo', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('151', 'E', 'AMER', 'EE', 'Africa', 'RE', 'AF', 'Reunion', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('152', 'E', 'AMER', 'CE', 'CEE', 'RO', 'AF', 'Romania', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('153', 'E', 'AMER', 'EE', 'CIS', 'RU', 'AF', 'Russian Federation', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('154', 'E', 'AMER', 'EE', 'Africa', 'RW', 'AF', 'Rwanda', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('155', 'E', 'AMER', 'SE', 'IT', 'SM', 'AF', 'San Marino', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('156', 'E', 'AMER', 'EE', 'Africa', 'ST', 'AF', 'Sao Tome and Principe', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('157', 'E', 'AMER', 'EE', 'MidEast', 'SA', 'AF', 'Saudi Arabia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('158', 'E', 'AMER', 'EE', 'Africa', 'SN', 'AF', 'Senegal', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('159', 'E', 'AMER', 'CE', 'CEE', 'RS', 'AF', 'Serbia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('160', 'E', 'AMER', 'EE', 'Africa', 'SC', 'AF', 'Seychelles', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('161', 'E', 'AMER', 'EE', 'Africa', 'SL', 'AF', 'Sierra Leone', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('162', 'P', 'AMER', 'AS', 'ASEAN', 'SG', 'AF', 'Singapore', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('163', 'E', 'AMER', 'CE', 'CEE', 'SK', 'AF', 'Slovakia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('164', 'E', 'AMER', 'CE', 'CEE', 'SI', 'AF', 'Slovenia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('165', 'E', 'AMER', 'EE', 'Africa', 'SO', 'AF', 'Somalia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('166', 'E', 'AMER', 'EE', 'Africa', 'ZA', 'AF', 'South Africa', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('167', 'P', 'AMER', 'KO', 'KO', 'KR', 'AF', 'South Korea', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('168', 'E', 'AMER', 'SE', 'IB', 'ES', 'AF', 'Spain', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('169', 'P', 'AMER', 'SA', 'SAARC', 'LK', 'AF', 'Sri Lanka', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('170', 'A', 'AMER', 'LA', 'LAN', 'VC', 'AF', 'St Vincent', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('171', 'E', 'AMER', 'EE', 'Africa', 'SH', 'AF', 'St. Helena', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('172', 'A', 'AMER', 'LA', 'LAN', 'LC', 'AF', 'St. Lucia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('173', 'A', 'AMER', 'LA', 'LAN', 'SR', 'AF', 'Surinam', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('174', 'E', 'AMER', 'NE', 'Nordic', 'SJ', 'AF', 'Svalbard', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('175', 'E', 'AMER', 'EE', 'Africa', 'SZ', 'AF', 'Swaziland', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('176', 'E', 'AMER', 'NE', 'Nordic', 'SE', 'AF', 'Sweden', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('177', 'E', 'AMER', 'CE', 'DACH', 'CH', 'AF', 'Switzerland', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('178', 'E', 'AMER', 'EE', 'MidEast', 'SY', 'AF', 'Syria', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('179', 'E', 'AMER', 'EE', 'CIS', 'TJ', 'AF', 'Tadzhikistan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('180', 'G', 'AMER', 'CN', 'TW', 'TW', 'AF', 'Taiwan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('181', 'E', 'AMER', 'EE', 'Africa', 'TZ', 'AF', 'Tanzania', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('182', 'P', 'AMER', 'AS', 'ASEAN', 'TH', 'AF', 'Thailand', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('183', 'E', 'AMER', 'EE', 'Africa', 'TG', 'AF', 'Togo', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('184', 'A', 'AMER', 'LA', 'LAN', 'TT', 'AF', 'Trinidad & Tobago', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('185', 'E', 'AMER', 'SE', 'FR', 'TN', 'AF', 'Tunisia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('186', 'E', 'AMER', 'EE', 'MED', 'TR', 'AF', 'Turkey', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('187', 'E', 'AMER', 'EE', 'CIS', 'TM', 'AF', 'Turkmenistan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('188', 'A', 'AMER', 'LA', 'LAN', 'VI', 'AF', 'U.S. Virgin Islands', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('189', 'E', 'AMER', 'EE', 'Africa', 'UG', 'AF', 'Uganda', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('190', 'E', 'AMER', 'EE', 'CIS', 'UA', 'AF', 'Ukraine', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('191', 'E', 'AMER', 'EE', 'MidEast', 'AE', 'AF', 'United Arab Emirates', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('192', 'E', 'AMER', 'NE', 'GB', 'GB', 'AF', 'United Kingdom', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('193', 'A', 'AMER', 'US', 'US', 'US', 'AF', 'United States', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('194', 'A', 'AMER', 'LA', 'LAS', 'UY', 'AF', 'Uruguay', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('195', 'E', 'AMER', 'EE', 'CIS', 'UZ', 'AF', 'Uzbekistan', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('196', 'E', 'AMER', 'SE', 'IT', 'VA', 'AF', 'Vatican City State', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('197', 'A', 'AMER', 'LA', 'LAN', 'VE', 'AF', 'Venezuela', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('198', 'P', 'AMER', 'AS', 'ASEAN', 'VN', 'AF', 'Vietnam', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('199', 'E', 'AMER', 'SE', 'FR', 'EH', 'AF', 'Western Sahara', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('200', 'E', 'AMER', 'EE', 'MidEast', 'YE', 'AF', 'Yemen', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('201', 'E', 'AMER', 'EE', 'Africa', 'ZM', 'AF', 'Zambia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('202', 'E', 'AMER', 'EE', 'Africa', 'ZW', 'AF', 'Zimbabwe', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('203', 'P', 'AMER', 'SA', 'SAARC', 'MV', 'AF', 'Maldives', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('204', 'E', 'AMER', 'CE', 'CEE', 'ME', 'AF', 'Montenegro', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('205', 'A', 'AMER', 'US', 'US', 'GU', 'AF', 'Guam', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('206', 'G', 'AMER', 'CN', 'CN', 'MN', 'AF', 'Mongolia', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');
INSERT INTO `Ref_subcountries` VALUES ('207', 'E', 'AMER', 'CE', 'CEE', 'KS', 'AF', 'Kosovo', 'admin', '2018-01-16 13:36:09', 'admin', '2018-01-16 13:36:14');

-- ----------------------------
-- Table structure for Ref_subregion
-- ----------------------------
DROP TABLE IF EXISTS `Ref_subregion`;
CREATE TABLE `Ref_subregion` (
  `subregion_id` int(11) NOT NULL AUTO_INCREMENT,
  `geo_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `territory_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subregion_code` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subregion_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `muid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  PRIMARY KEY (`subregion_id`)
) ENGINE=MyISAM AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Ref_subregion
-- ----------------------------
INSERT INTO `Ref_subregion` VALUES ('1', 'EMEA', 'EE', 'Emerging Countries', 'ME', 'MidEast', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('2', 'EMEA', 'CE', 'Central Europe', 'CEE', 'CEE', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('3', 'EMEA', 'SE', 'South Europe', 'FR', 'FR', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('4', 'EMEA', 'SE', 'South Europe', 'IB', 'IB', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('5', 'EMEA', 'EE', 'Emerging Countries', 'Af', 'Africa', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('6', 'AMER', 'LA', 'Latin America', 'LAN', 'LAN', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('7', 'AMER', 'LA', 'Latin America', 'LAS', 'LAS', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('8', 'EMEA', 'EE', 'Emerging Countries', 'CIS', 'CIS', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('9', 'APAC (excl. GCR)', 'AZ', 'ANZ', 'AU', 'AU', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('10', 'EMEA', 'CE', 'Central Europe', 'DACH', 'DACH', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('11', 'APAC (excl. GCR)', 'SA', 'India and SAARC', 'SAARC', 'SAARC', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('12', 'EMEA', 'NE', 'North Europe', 'BEN', 'BEN', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('13', 'EMEA', 'NE', 'North Europe', 'Nor', 'Nordic', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('14', 'AMER', 'LA', 'Latin America', 'BR', 'BR', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('15', 'EMEA', 'NE', 'North Europe', 'GB', 'GB', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('16', 'APAC (excl. GCR)', 'AS', 'ASEAN', 'ASEAN', 'ASEAN', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('17', 'AMER', 'CA', 'Canada', 'CA', 'CA', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('18', 'GCR', 'CN', 'Greater China', 'CN', 'CN', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('19', 'EMEA', 'EE', 'Emerging Countries', 'MED', 'MED', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('20', 'GCR', 'CN', 'Greater China', 'HK', 'HK', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('21', 'APAC (excl. GCR)', 'SA', 'India and SAARC', 'IN', 'IN', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('22', 'EMEA', 'SE', 'South Europe', 'IT', 'IT', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('23', 'APAC (excl. GCR)', 'JP', 'Japan', 'JP', 'JP', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('24', 'AMER', 'LA', 'Latin America', 'MX', 'MX', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('25', 'APAC (excl. GCR)', 'KO', 'Korea', 'KO', 'KO', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('26', 'GCR', 'CN', 'Greater China', 'TW', 'TW', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');
INSERT INTO `Ref_subregion` VALUES ('27', 'AMER', 'US', 'US', 'US', 'US', 'admin', '2018-01-16 13:20:25', 'admin', '2018-01-16 13:20:33');

-- ----------------------------
-- Table structure for Ref_territory
-- ----------------------------
DROP TABLE IF EXISTS `Ref_territory`;
CREATE TABLE `Ref_territory` (
  `territory_id` int(11) NOT NULL AUTO_INCREMENT,
  `territory_code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `territory_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cuid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `muid` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  PRIMARY KEY (`territory_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Ref_territory
-- ----------------------------
INSERT INTO `Ref_territory` VALUES ('1', 'AMER', 'AMER', 'admin', '2018-01-16 13:03:42', 'admin', '2018-01-16 13:03:52');
INSERT INTO `Ref_territory` VALUES ('2', 'APAC', 'APAC', 'admin', '2018-01-16 13:03:42', 'admin', '2018-01-16 13:03:52');
INSERT INTO `Ref_territory` VALUES ('3', 'EMEA', 'EMEA', 'admin', '2018-01-16 13:03:42', 'admin', '2018-01-16 13:03:52');
INSERT INTO `Ref_territory` VALUES ('4', 'GCR', 'GCR', 'admin', '2018-01-16 13:03:42', 'admin', '2018-01-16 13:03:52');

-- ----------------------------
-- Table structure for Ref_timezone
-- ----------------------------
DROP TABLE IF EXISTS `Ref_timezone`;
CREATE TABLE `Ref_timezone` (
  `ZoneName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `Country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `RuleName` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `GmtOffset` int(10) NOT NULL,
  `Format` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `StandardName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DaylightName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `DisplayName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SupportsDaylightSavingTime` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `BaseUtcOffset` int(10) NOT NULL,
  PRIMARY KEY (`ZoneName`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Ref_timezone
-- ----------------------------
INSERT INTO `Ref_timezone` VALUES ('America/Eirunepe', 'Eirunepe', '-', '-18000', 'ACT', 'SA Pacific Standard Time', 'SA Pacific Daylight Time', '(GMT-05:00) Bogota, Lima, Quito, Rio Branco', 'FALSE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Rio_Branco', 'Rio_Branco', '-', '-18000', 'ACT', 'SA Pacific Standard Time', 'SA Pacific Daylight Time', '(GMT-05:00) Bogota, Lima, Quito, Rio Branco', 'FALSE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Kabul', 'Kabul', '-', '16200', 'AFT', 'Afghanistan Standard Time', 'Afghanistan Daylight Time', '(GMT+04:30) Kabul', 'FALSE', '16200');
INSERT INTO `Ref_timezone` VALUES ('America/Anchorage', 'Anchorage', 'US', '-32400', 'AKST', 'Alaskan Standard Time', 'Alaskan Daylight Time', '(GMT-09:00) Alaska', 'TRUE', '-32400');
INSERT INTO `Ref_timezone` VALUES ('America/Juneau', 'Juneau', 'US', '-32400', 'AKST', 'Alaskan Standard Time', 'Alaskan Daylight Time', '(GMT-09:00) Alaska', 'TRUE', '-32400');
INSERT INTO `Ref_timezone` VALUES ('America/Nome', 'Nome', 'US', '-32400', 'AKST', 'Alaskan Standard Time', 'Alaskan Daylight Time', '(GMT-09:00) Alaska', 'TRUE', '-32400');
INSERT INTO `Ref_timezone` VALUES ('America/Yakutat', 'Yakutat', 'US', '-32400', 'AKST', 'Alaskan Standard Time', 'Alaskan Daylight Time', '(GMT-09:00) Alaska', 'TRUE', '-32400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Almaty', 'Almaty', '-', '21600', 'ALMT', 'Central Asia Standard Time', 'Central Asia Daylight Time', '(GMT+06:00) Astana, Dhaka', 'FALSE', '21600');
INSERT INTO `Ref_timezone` VALUES ('America/Campo_Grande', 'Campo_Grande', 'Brazil', '-14400', 'AMST', 'SA Western Standard Time', 'SA Western Daylight Time', '(GMT-04:00) La Paz', 'FALSE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Cuiaba', 'Cuiaba', 'Brazil', '-14400', 'AMST', 'SA Western Standard Time', 'SA Western Daylight Time', '(GMT-04:00) La Paz', 'FALSE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Yerevan', 'Yerevan', 'RussiaAsia', '14400', 'AMST', 'Caucasus Standard Time', 'Caucasus Daylight Time', '(GMT+04:00) Yerevan', 'TRUE', '14400');
INSERT INTO `Ref_timezone` VALUES ('America/Boa_Vista', 'Boa_Vista', '-', '-14400', 'AMT', 'Central Brazilian Standard Time', 'Central Brazilian Daylight Time', '(GMT-04:00) Manaus', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Manaus', 'Manaus', '-', '-14400', 'AMT', 'Central Brazilian Standard Time', 'Central Brazilian Daylight Time', '(GMT-04:00) Manaus', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Porto_Velho', 'Porto_Velho', '-', '-14400', 'AMT', 'Central Brazilian Standard Time', 'Central Brazilian Daylight Time', '(GMT-04:00) Manaus', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Anadyr', 'Anadyr', 'Russia', '43200', 'ANAST', 'New Zealand Standard Time', 'New Zealand Daylight Time', '(GMT+12:00) Auckland, Wellington', 'TRUE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Aqtau', 'Aqtau', '-', '18000', 'AQTT', 'West Asia Standard Time', 'West Asia Daylight Time', '(GMT+05:00) Tashkent', 'FALSE', '18000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Aqtobe', 'Aqtobe', '-', '18000', 'AQTT', 'West Asia Standard Time', 'West Asia Daylight Time', '(GMT+05:00) Tashkent', 'FALSE', '18000');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/Buenos_Aires', 'Buenos_Aires', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/Catamarca', 'Catamarca', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/Cordoba', 'Cordoba', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/Jujuy', 'Jujuy', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/La_Rioja', 'La_Rioja', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/Mendoza', 'Mendoza', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/Rio_Gallegos', 'Rio_Gallegos', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/San_Juan', 'San_Juan', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/Tucuman', 'Tucuman', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/Ushuaia', 'Ushuaia', 'Arg', '-10800', 'ARST', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Argentina/San_Luis', 'San_Luis', '-', '-10800', 'ART', 'Argentina Standard Time', 'Argentina Daylight Time', '(GMT-03:00) Buenos Aires', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Anguilla', 'Anguilla', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Antigua', 'Antigua', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Aruba', 'Aruba', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Barbados', 'Barbados', 'Barb', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Blanc-Sablon', 'Blanc-Sablon', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Curacao', 'Curacao', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Dominica', 'Dominica', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Glace_Bay', 'Glace_Bay', 'Canada', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Goose_Bay', 'Goose_Bay', 'StJohns', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Grenada', 'Grenada', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Guadeloupe', 'Guadeloupe', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Halifax', 'Halifax', 'Canada', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Martinique', 'Martinique', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Moncton', 'Moncton', 'Canada', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Montserrat', 'Montserrat', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Port_of_Spain', 'Port_of_Spain', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Puerto_Rico', 'Puerto_Rico', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Santo_Domingo', 'Santo_Domingo', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/St_Kitts', 'St_Kitts', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/St_Lucia', 'St_Lucia', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/St_Thomas', 'St_Thomas', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/St_Vincent', 'St_Vincent', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Thule', 'Thule', 'Thule', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Tortola', 'Tortola', '-', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Aden', 'Aden', '-', '10800', 'AST', 'Arabic Standard Time', 'Arabic Daylight Time', '(GMT+03:00) Baghdad', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Baghdad', 'Baghdad', 'Iraq', '10800', 'AST', 'Arabic Standard Time', 'Arabic Daylight Time', '(GMT+03:00) Baghdad', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Bahrain', 'Bahrain', '-', '10800', 'AST', 'Arabic Standard Time', 'Arabic Daylight Time', '(GMT+03:00) Baghdad', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Kuwait', 'Kuwait', '-', '10800', 'AST', 'Arabic Standard Time', 'Arabic Daylight Time', '(GMT+03:00) Baghdad', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Qatar', 'Qatar', '-', '10800', 'AST', 'Arabic Standard Time', 'Arabic Daylight Time', '(GMT+03:00) Baghdad', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Riyadh', 'Riyadh', '-', '10800', 'AST', 'Arabic Standard Time', 'Arabic Daylight Time', '(GMT+03:00) Baghdad', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/Bermuda', 'Bermuda', 'US', '-14400', 'AST', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/Azores', 'Azores', 'EU', '-3600', 'AZOST', 'Azores Standard Time', 'Azores Daylight Time', '(GMT-01:00) Azores', 'TRUE', '-3600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Baku', 'Baku', 'Azer', '14400', 'AZST', 'Azerbaijan Standard Time', 'Azerbaijan Daylight Time', '(GMT+04:00) Baku', 'TRUE', '14400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Dhaka', 'Dhaka', '-', '21600', 'BDT', 'N. Central Asia Standard Time', 'N. Central Asia Daylight Time', '(GMT+06:00) Almaty, Novosibirsk', 'TRUE', '21600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Brunei', 'Brunei', '-', '28800', 'BNT', 'North Asia East Standard Time', 'North Asia East Daylight Time', '(GMT+08:00) Irkutsk, Ulaan Bataar', 'TRUE', '28800');
INSERT INTO `Ref_timezone` VALUES ('America/La_Paz', 'La_Paz', '-', '-14400', 'BOT', 'SA Western Standard Time', 'SA Western Daylight Time', '(GMT-04:00) La Paz', 'FALSE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Sao_Paulo', 'Sao_Paulo', 'Brazil', '-10800', 'BRST', 'E. South America Standard Time', 'E. South America Daylight Time', '(GMT-03:00) Brasilia', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Araguaina', 'Araguaina', '-', '-10800', 'BRT', 'E. South America Standard Time', 'E. South America Daylight Time', '(GMT-03:00) Brasilia', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Bahia', 'Bahia', '-', '-10800', 'BRT', 'E. South America Standard Time', 'E. South America Daylight Time', '(GMT-03:00) Brasilia', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Belem', 'Belem', '-', '-10800', 'BRT', 'E. South America Standard Time', 'E. South America Daylight Time', '(GMT-03:00) Brasilia', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Fortaleza', 'Fortaleza', '-', '-10800', 'BRT', 'E. South America Standard Time', 'E. South America Daylight Time', '(GMT-03:00) Brasilia', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Maceio', 'Maceio', '-', '-10800', 'BRT', 'E. South America Standard Time', 'E. South America Daylight Time', '(GMT-03:00) Brasilia', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('America/Recife', 'Recife', '-', '-10800', 'BRT', 'E. South America Standard Time', 'E. South America Daylight Time', '(GMT-03:00) Brasilia', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Thimphu', 'Thimphu', '-', '21600', 'BTT', 'N. Central Asia Standard Time', 'N. Central Asia Daylight Time', '(GMT+06:00) Almaty, Novosibirsk', 'TRUE', '21600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Blantyre', 'Blantyre', '-', '7200', 'CAT', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Bujumbura', 'Bujumbura', '-', '7200', 'CAT', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Gaborone', 'Gaborone', '-', '7200', 'CAT', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Harare', 'Harare', '-', '7200', 'CAT', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Kigali', 'Kigali', '-', '7200', 'CAT', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Lubumbashi', 'Lubumbashi', '-', '7200', 'CAT', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Lusaka', 'Lusaka', '-', '7200', 'CAT', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Maputo', 'Maputo', '-', '7200', 'CAT', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Indian/Cocos', 'Cocos', '-', '23400', 'CCT', 'Myanmar Standard Time', 'Myanmar Daylight Time', '(GMT+06:30) Yangon (Rangoon)', 'FALSE', '23400');
INSERT INTO `Ref_timezone` VALUES ('Africa/Ceuta', 'Ceuta', 'EU', '3600', 'CEST', 'Central Europe Standard Time', 'Central Europe Daylight Time', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Tunis', 'Tunis', 'Tunisia', '3600', 'CEST', 'Central Europe Standard Time', 'Central Europe Daylight Time', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('CET', 'CET', 'C-Eur', '3600', 'CEST', 'Central Europe Standard Time', 'Central Europe Daylight Time', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Amsterdam', 'Amsterdam', 'EU', '3600', 'CEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Andorra', 'Andorra', 'EU', '3600', 'CEST', 'Central Europe Standard Time', 'Central Europe Daylight Time', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Belgrade', 'Belgrade', 'EU', '3600', 'CEST', 'Central Europe Standard Time', 'Central Europe Daylight Time', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Berlin', 'Berlin', 'EU', '3600', 'CEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Brussels', 'Brussels', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Budapest', 'Budapest', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Copenhagen', 'Copenhagen', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Gibraltar', 'Gibraltar', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Luxembourg', 'Luxembourg', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Madrid', 'Madrid', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Malta', 'Malta', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Monaco', 'Monaco', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Oslo', 'Oslo', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Paris', 'Paris', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Prague', 'Prague', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Rome', 'Rome', 'EU', '3600', 'CEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Stockholm', 'Stockholm', 'EU', '3600', 'CEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Tirane', 'Tirane', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Vaduz', 'Vaduz', 'EU', '3600', 'CEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Vienna', 'Vienna', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Warsaw', 'Warsaw', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Zurich', 'Zurich', 'EU', '3600', 'CEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Algiers', 'Algiers', '-', '3600', 'CET', 'Central Europe Standard Time', 'Central Europe Daylight Time', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Chatham', 'Chatham', 'Chatham', '45900', 'CHAST', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Choibalsan', 'Choibalsan', 'Mongol', '32400', 'CHOST', 'Yakutsk Standard Time', 'Yakutsk Daylight Time', '(GMT+09:00) Yakutsk', 'TRUE', '32400');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Guam', 'Guam', '-', '36000', 'ChST', 'West Pacific Standard Time', 'West Pacific Daylight Time', '(GMT+10:00) Guam, Port Moresby', 'FALSE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Saipan', 'Saipan', '-', '36000', 'ChST', 'West Pacific Standard Time', 'West Pacific Daylight Time', '(GMT+10:00) Guam, Port Moresby', 'FALSE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Makassar', 'Makassar', '-', '28800', 'CIT', 'Malay Peninsula Standard Time', 'Malay Peninsula Daylight Time', '(GMT+08:00) Kuala Lumpur, Singapore', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Rarotonga', 'Rarotonga', 'Cook', '-36000', 'CKST', 'Hawaiian Standard Time', 'Hawaiian Daylight Time', '(GMT-10:00) Hawaii', 'FALSE', '-36000');
INSERT INTO `Ref_timezone` VALUES ('America/Santiago', 'Santiago', 'Chile', '-14400', 'CLST', 'SA Western Standard Time', 'SA Western Daylight Time', '(GMT-04:00) La Paz', 'FALSE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('Antarctica/Palmer', 'Palmer', 'ChileAQ', '-14400', 'CLST', 'SA Western Standard Time', 'SA Western Daylight Time', '(GMT-04:00) La Paz', 'FALSE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Bogota', 'Bogota', 'CO', '-18000', 'COST', 'SA Pacific Standard Time', 'SA Pacific Daylight Time', '(GMT-05:00) Bogota, Lima, Quito, Rio Branco', 'FALSE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Belize', 'Belize', 'Belize', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Cancun', 'Cancun', 'Mexico', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Chicago', 'Chicago', 'US', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Costa_Rica', 'Costa_Rica', 'CR', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/El_Salvador', 'El_Salvador', 'Salv', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Guatemala', 'Guatemala', 'Guat', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Havana', 'Havana', 'Cuba', '-18000', 'CST', 'SA Pacific Standard Time', 'SA Pacific Daylight Time', '(GMT-05:00) Bogota, Lima, Quito, Rio Branco', 'FALSE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Indiana/Knox', 'Knox', 'US', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Indiana/Tell_City', 'Tell_City', 'US', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Managua', 'Managua', 'Nic', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Menominee', 'Menominee', 'US', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Merida', 'Merida', 'Mexico', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Mexico_City', 'Mexico_City', 'Mexico', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Monterrey', 'Monterrey', 'Mexico', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/North_Dakota/Center', 'Center', 'US', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/North_Dakota/New_Salem', 'New_Salem', 'US', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Rainy_River', 'Rainy_River', 'Canada', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Rankin_Inlet', 'Rankin_Inlet', 'Canada', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Regina', 'Regina', '-', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Swift_Current', 'Swift_Current', '-', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Tegucigalpa', 'Tegucigalpa', 'Hond', '-21600', 'CST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('America/Winnipeg', 'Winnipeg', 'Canada', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Chongqing', 'Chongqing', 'PRC', '28800', 'CST', 'China Standard Time', 'China Daylight Time', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Harbin', 'Harbin', 'PRC', '28800', 'CST', 'China Standard Time', 'China Daylight Time', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Kashgar', 'Kashgar', 'PRC', '28800', 'CST', 'China Standard Time', 'China Daylight Time', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Macau', 'Macau', 'PRC', '28800', 'CST', 'China Standard Time', 'China Daylight Time', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Shanghai', 'Shanghai', 'PRC', '28800', 'CST', 'China Standard Time', 'China Daylight Time', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Taipei', 'Taipei', 'Taiwan', '28800', 'CST', 'China Standard Time', 'China Daylight Time', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Urumqi', 'Urumqi', 'PRC', '28800', 'CST', 'China Standard Time', 'China Daylight Time', '(GMT+08:00) Beijing, Chongqing, Hong Kong, Urumqi', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Australia/Adelaide', 'Adelaide', 'AS', '34200', 'CST', 'Cen. Australia Standard Time', 'Cen. Australia Daylight Time', '(GMT+09:30) Adelaide', 'TRUE', '34200');
INSERT INTO `Ref_timezone` VALUES ('Australia/Broken_Hill', 'Broken_Hill', 'AS', '34200', 'CST', 'Cen. Australia Standard Time', 'Cen. Australia Daylight Time', '(GMT+09:30) Adelaide', 'TRUE', '34200');
INSERT INTO `Ref_timezone` VALUES ('Australia/Darwin', 'Darwin', 'Aus', '34200', 'CST', 'AUS Central Standard Time', 'AUS Central Daylight Time', '(GMT+09:30) Darwin', 'FALSE', '34200');
INSERT INTO `Ref_timezone` VALUES ('CST6CDT', 'CST6CDT', 'US', '-21600', 'CST', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/Cape_Verde', 'Cape_Verde', '-', '-3600', 'CVT', 'Cape Verde Standard Time', 'Cape Verde Daylight Time', '(GMT-01:00) Cape Verde Is.', 'FALSE', '-3600');
INSERT INTO `Ref_timezone` VALUES ('Australia/Eucla', 'Eucla', 'AW', '31500', 'CWST', 'Cen. Australia Standard Time', 'Cen. Australia Daylight Time', '(GMT+09:30) Adelaide', 'TRUE', '34200');
INSERT INTO `Ref_timezone` VALUES ('Indian/Christmas', 'Christmas', '-', '25200', 'CXT', 'SE Asia Standard Time', 'SE Asia Daylight Time', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 'FALSE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Antarctica/Davis', 'Davis', '-', '25200', 'DAVT', 'SE Asia Standard Time', 'SE Asia Daylight Time', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 'FALSE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Antarctica/DumontDUrville', 'DumontDUrville', '-', '36000', 'DDUT', 'E. Australia Standard Time', 'E. Australia Daylight Time', '(GMT+10:00) Brisbane', 'FALSE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Easter', 'Easter', 'Chile', '-21600', 'EASST', 'Central Standard Time (Mexico)', 'Central Daylight Time (Mexico)', '(GMT-06:00) Guadalajara, Mexico City, Monterrey', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Addis_Ababa', 'Addis_Ababa', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Africa/Asmara', 'Asmara', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Africa/Dar_es_Salaam', 'Dar_es_Salaam', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Africa/Djibouti', 'Djibouti', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Africa/Kampala', 'Kampala', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Africa/Khartoum', 'Khartoum', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Africa/Mogadishu', 'Mogadishu', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Africa/Nairobi', 'Nairobi', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Indian/Antananarivo', 'Antananarivo', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Indian/Comoro', 'Comoro', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Indian/Mayotte', 'Mayotte', '-', '10800', 'EAT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('America/Guayaquil', 'Guayaquil', '-', '-18000', 'ECT', 'SA Pacific Standard Time', 'SA Pacific Daylight Time', '(GMT-05:00) Bogota, Lima, Quito, Rio Branco', 'FALSE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('Africa/Cairo', 'Cairo', 'Egypt', '7200', 'EEST', 'Egypt Standard Time', 'Egypt Daylight Time', '(GMT+02:00) Cairo', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Amman', 'Amman', 'Jordan', '7200', 'EEST', 'Jordan Standard Time', 'Jordan Daylight Time', '(GMT+02:00) Amman', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Beirut', 'Beirut', 'Lebanon', '7200', 'EEST', 'Middle East Standard Time', 'Middle East Daylight Time', '(GMT+02:00) Beirut', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Damascus', 'Damascus', 'Syria', '7200', 'EEST', 'Middle East Standard Time', 'Middle East Daylight Time', '(GMT+02:00) Beirut', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Gaza', 'Gaza', 'Palestine', '7200', 'EEST', 'Middle East Standard Time', 'Middle East Daylight Time', '(GMT+02:00) Beirut', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Nicosia', 'Nicosia', 'EUAsia', '7200', 'EEST', 'Middle East Standard Time', 'Middle East Daylight Time', '(GMT+02:00) Beirut', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('EET', 'EET', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Athens', 'Athens', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Bucharest', 'Bucharest', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Chisinau', 'Chisinau', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Helsinki', 'Helsinki', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Istanbul', 'Istanbul', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Kaliningrad', 'Kaliningrad', 'Russia', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Kiev', 'Kiev', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Minsk', 'Minsk', 'Russia', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Riga', 'Riga', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Simferopol', 'Simferopol', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Sofia', 'Sofia', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Tallinn', 'Tallinn', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Uzhgorod', 'Uzhgorod', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Vilnius', 'Vilnius', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Europe/Zaporozhye', 'Zaporozhye', 'EU', '7200', 'EEST', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Tripoli', 'Tripoli', '-', '7200', 'EET', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('America/Scoresbysund', 'Scoresbysund', 'EU', '-3600', 'EGST', 'Azores Standard Time', 'Azores Daylight Time', '(GMT-01:00) Azores', 'TRUE', '-3600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Jayapura', 'Jayapura', '-', '32400', 'EIT', 'Korea Standard Time', 'Korea Daylight Time', '(GMT+09:00) Seoul', 'FALSE', '32400');
INSERT INTO `Ref_timezone` VALUES ('America/Atikokan', 'Atikokan', '-', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Cayman', 'Cayman', '-', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Detroit', 'Detroit', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Grand_Turk', 'Grand_Turk', 'TC', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Indiana/Indianapolis', 'Indianapolis', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Indiana/Marengo', 'Marengo', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Indiana/Petersburg', 'Petersburg', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Indiana/Vevay', 'Vevay', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Indiana/Vincennes', 'Vincennes', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Indiana/Winamac', 'Winamac', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Iqaluit', 'Iqaluit', 'Canada', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Jamaica', 'Jamaica', '-', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Kentucky/Louisville', 'Louisville', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Kentucky/Monticello', 'Monticello', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Montreal', 'Montreal', 'Canada', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Nassau', 'Nassau', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/New_York', 'New_York', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Nipigon', 'Nipigon', 'Canada', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Panama', 'Panama', '-', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Pangnirtung', 'Pangnirtung', 'Canada', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Port-au-Prince', 'Port-au-Prince', 'Haiti', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Resolute', 'Resolute', '-', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Thunder_Bay', 'Thunder_Bay', 'Canada', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('America/Toronto', 'Toronto', 'Canada', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('Australia/Brisbane', 'Brisbane', 'AQ', '36000', 'EST', 'E. Australia Standard Time', 'E. Australia Daylight Time', '(GMT+10:00) Brisbane', 'FALSE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Australia/Currie', 'Currie', 'AT', '36000', 'EST', 'Tasmania Standard Time', 'Tasmania Daylight Time', '(GMT+10:00) Hobart', 'TRUE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Australia/Hobart', 'Hobart', 'AT', '36000', 'EST', 'Tasmania Standard Time', 'Tasmania Daylight Time', '(GMT+10:00) Hobart', 'TRUE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Australia/Lindeman', 'Lindeman', 'Holiday', '36000', 'EST', 'AUS Eastern Standard Time', 'AUS Eastern Daylight Time', '(GMT+10:00) Canberra, Melbourne, Sydney', 'TRUE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Australia/Melbourne', 'Melbourne', 'AV', '36000', 'EST', 'AUS Eastern Standard Time', 'AUS Eastern Daylight Time', '(GMT+10:00) Canberra, Melbourne, Sydney', 'TRUE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Australia/Sydney', 'Sydney', 'AN', '36000', 'EST', 'AUS Eastern Standard Time', 'AUS Eastern Daylight Time', '(GMT+10:00) Canberra, Melbourne, Sydney', 'TRUE', '36000');
INSERT INTO `Ref_timezone` VALUES ('EST', 'EST', '-', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('EST5EDT', 'EST5EDT', 'US', '-18000', 'EST', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Fiji', 'Fiji', 'Fiji', '43200', 'FJST', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/Stanley', 'Stanley', 'Falk', '-14400', 'FKST', 'Pacific SA Standard Time', 'Pacific SA Daylight Time', '(GMT-04:00) Santiago', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Noronha', 'Noronha', '-', '-7200', 'FNT', 'Mid-Atlantic Standard Time', 'Mid-Atlantic Daylight Time', '(GMT-02:00) Mid-Atlantic', 'TRUE', '-7200');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Galapagos', 'Galapagos', '-', '-21600', 'GALT', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Gambier', 'Gambier', '-', '-32400', 'GAMT', 'Alaskan Standard Time', 'Alaskan Daylight Time', '(GMT-09:00) Alaska', 'TRUE', '-32400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Tbilisi', 'Tbilisi', '-', '14400', 'GET', 'Arabian Standard Time', 'Arabian Daylight Time', '(GMT+04:00) Abu Dhabi, Muscat', 'FALSE', '14400');
INSERT INTO `Ref_timezone` VALUES ('America/Cayenne', 'Cayenne', '-', '-10800', 'GFT', 'SA Eastern Standard Time', 'SA Eastern Daylight Time', '(GMT-03:00) Georgetown', 'FALSE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Tarawa', 'Tarawa', '-', '43200', 'GILT', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Abidjan', 'Abidjan', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Bamako', 'Bamako', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Banjul', 'Banjul', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Bissau', 'Bissau', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Conakry', 'Conakry', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Dakar', 'Dakar', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Lome', 'Lome', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Monrovia', 'Monrovia', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Nouakchott', 'Nouakchott', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Ouagadougou', 'Ouagadougou', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Sao_Tome', 'Sao_Tome', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('America/Danmarkshavn', 'Danmarkshavn', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/Reykjavik', 'Reykjavik', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/St_Helena', 'St_Helena', '-', '0', 'GMT', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT', 'GMT', '-', '0', 'GMT', 'GMT Standard Time', 'GMT Daylight Time', '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London', 'TRUE', '0');
INSERT INTO `Ref_timezone` VALUES ('Europe/London', 'London', 'EU', '0', 'GMT/BST', 'GMT Standard Time', 'GMT Daylight Time', '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London', 'TRUE', '0');
INSERT INTO `Ref_timezone` VALUES ('Europe/Dublin', 'Dublin', 'EU', '0', 'GMT/IST', 'GMT Standard Time', 'GMT Daylight Time', '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London', 'TRUE', '0');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+1', 'EtcGMT+1', '-', '-3600', 'GMT+1', 'Azores Standard Time', 'Azores Daylight Time', '(GMT-01:00) Azores', 'TRUE', '-3600');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+10', 'EtcGMT+10', '-', '-36000', 'GMT+10', 'Hawaiian Standard Time', 'Hawaiian Daylight Time', '(GMT-10:00) Hawaii', 'FALSE', '-36000');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+11', 'EtcGMT+11', '-', '-39600', 'GMT+11', 'Samoa Standard Time', 'Samoa Daylight Time', '(GMT-11:00) Midway Island, Samoa', 'FALSE', '-39600');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+12', 'EtcGMT+12', '-', '-43200', 'GMT+12', 'Dateline Standard Time', 'Dateline Standard Time', '(GMT-12:00) International Date Line West', 'FALSE', '-43200');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+2', 'EtcGMT+2', '-', '-7200', 'GMT+2', 'Mid-Atlantic Standard Time', 'Mid-Atlantic Daylight Time', '(GMT-02:00) Mid-Atlantic', 'TRUE', '-7200');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+3', 'EtcGMT+3', '-', '-10800', 'GMT+3', 'E. South America Standard Time', 'E. South America Daylight Time', '(GMT-03:00) Brasilia', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+4', 'EtcGMT+4', '-', '-14400', 'GMT+4', 'Atlantic Standard Time', 'Atlantic Daylight Time', '(GMT-04:00) Atlantic Time (Canada)', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+5', 'EtcGMT+5', '-', '-18000', 'GMT+5', 'Eastern Standard Time', 'Eastern Daylight Time', '(GMT-05:00) Eastern Time (US & Canada)', 'TRUE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+6', 'EtcGMT+6', '-', '-21600', 'GMT+6', 'Central Standard Time', 'Central Daylight Time', '(GMT-06:00) Central Time (US & Canada)', 'TRUE', '-21600');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+7', 'EtcGMT+7', '-', '-25200', 'GMT+7', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+8', 'EtcGMT+8', '-', '-28800', 'GMT+8', 'Pacific Standard Time', 'Pacific Daylight Time', '(GMT-08:00) Pacific Time (US & Canada)', 'TRUE', '-28800');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT+9', 'EtcGMT+9', '-', '-32400', 'GMT+9', 'Alaskan Standard Time', 'Alaskan Daylight Time', '(GMT-09:00) Alaska', 'TRUE', '-32400');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-1', 'EtcGMT-1', '-', '3600', 'GMT-1', 'Central Europe Standard Time', 'Central Europe Daylight Time', '(GMT+01:00) Belgrade, Bratislava, Budapest, Ljubljana, Prague', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-10', 'EtcGMT-10', '-', '36000', 'GMT-10', 'AUS Eastern Standard Time', 'AUS Eastern Daylight Time', '(GMT+10:00) Canberra, Melbourne, Sydney', 'TRUE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-11', 'EtcGMT-11', '-', '39600', 'GMT-11', 'Central Pacific Standard Time', 'Central Pacific Daylight Time', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 'FALSE', '39600');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-12', 'EtcGMT-12', '-', '43200', 'GMT-12', 'New Zealand Standard Time', 'New Zealand Daylight Time', '(GMT+12:00) Auckland, Wellington', 'TRUE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-13', 'EtcGMT-13', '-', '46800', 'GMT-13', 'Tonga Standard Time', 'Tonga Daylight Time', '(GMT+13:00) Nuku\'alofa', 'FALSE', '46800');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-14', 'EtcGMT-14', '-', '50400', 'GMT-14', 'Tonga Standard Time', 'Tonga Daylight Time', '(GMT+13:00) Nuku\'alofa', 'FALSE', '46800');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-2', 'EtcGMT-2', '-', '7200', 'GMT-2', 'E. Europe Standard Time', 'E. Europe Daylight Time', '(GMT+02:00) Minsk', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-3', 'EtcGMT-3', '-', '10800', 'GMT-3', 'Russian Standard Time', 'Russian Daylight Time', '(GMT+03:00) Moscow, St. Petersburg, Volgograd', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-4', 'EtcGMT-4', '-', '14400', 'GMT-4', 'Azerbaijan Standard Time', 'Azerbaijan Daylight Time', '(GMT+04:00) Baku', 'TRUE', '14400');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-5', 'EtcGMT-5', '-', '18000', 'GMT-5', 'Ekaterinburg Standard Time', 'Ekaterinburg Daylight Time', '(GMT+05:00) Ekaterinburg', 'TRUE', '18000');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-6', 'EtcGMT-6', '-', '21600', 'GMT-6', 'N. Central Asia Standard Time', 'N. Central Asia Daylight Time', '(GMT+06:00) Almaty, Novosibirsk', 'TRUE', '21600');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-7', 'EtcGMT-7', '-', '25200', 'GMT-7', 'North Asia Standard Time', 'North Asia Daylight Time', '(GMT+07:00) Krasnoyarsk', 'TRUE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-8', 'EtcGMT-8', '-', '28800', 'GMT-8', 'W. Australia Standard Time', 'W. Australia Daylight Time', '(GMT+08:00) Perth', 'TRUE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Etc/GMT-9', 'EtcGMT-9', '-', '32400', 'GMT-9', 'Yakutsk Standard Time', 'Yakutsk Daylight Time', '(GMT+09:00) Yakutsk', 'TRUE', '32400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Dubai', 'Dubai', '-', '14400', 'GST', 'Arabian Standard Time', 'Arabian Daylight Time', '(GMT+04:00) Abu Dhabi, Muscat', 'FALSE', '14400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Muscat', 'Muscat', '-', '14400', 'GST', 'Arabian Standard Time', 'Arabian Daylight Time', '(GMT+04:00) Abu Dhabi, Muscat', 'FALSE', '14400');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/South_Georgia', 'South_Georgia', '-', '-7200', 'GST', 'Mid-Atlantic Standard Time', 'Mid-Atlantic Daylight Time', '(GMT-02:00) Mid-Atlantic', 'TRUE', '-7200');
INSERT INTO `Ref_timezone` VALUES ('America/Guyana', 'Guyana', '-', '-14400', 'GYT', 'SA Western Standard Time', 'SA Western Daylight Time', '(GMT-04:00) La Paz', 'FALSE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('America/Adak', 'Adak', 'US', '-36000', 'HAST', 'Hawaiian Standard Time', 'Hawaiian Daylight Time', '(GMT-10:00) Hawaii', 'FALSE', '-36000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Hong_Kong', 'Hong_Kong', 'HK', '28800', 'HKST', 'Malay Peninsula Standard Time', 'Malay Peninsula Daylight Time', '(GMT+08:00) Kuala Lumpur, Singapore', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Hovd', 'Hovd', 'Mongol', '25200', 'HOVST', 'SE Asia Standard Time', 'SE Asia Daylight Time', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 'FALSE', '25200');
INSERT INTO `Ref_timezone` VALUES ('HST', 'HST', '-', '-36000', 'HST', 'Hawaiian Standard Time', 'Hawaiian Daylight Time', '(GMT-10:00) Hawaii', 'FALSE', '-36000');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Honolulu', 'Honolulu', '-', '-36000', 'HST', 'Hawaiian Standard Time', 'Hawaiian Daylight Time', '(GMT-10:00) Hawaii', 'FALSE', '-36000');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Johnston', 'Johnston', '-', '-36000', 'HST', 'Hawaiian Standard Time', 'Hawaiian Daylight Time', '(GMT-10:00) Hawaii', 'FALSE', '-36000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Bangkok', 'Bangkok', '-', '25200', 'ICT', 'SE Asia Standard Time', 'SE Asia Daylight Time', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 'FALSE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Ho_Chi_Minh', 'Ho_Chi_Minh', '-', '25200', 'ICT', 'SE Asia Standard Time', 'SE Asia Daylight Time', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 'FALSE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Phnom_Penh', 'Phnom_Penh', '-', '25200', 'ICT', 'SE Asia Standard Time', 'SE Asia Daylight Time', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 'FALSE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Vientiane', 'Vientiane', '-', '25200', 'ICT', 'SE Asia Standard Time', 'SE Asia Daylight Time', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 'FALSE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Indian/Chagos', 'Chagos', '-', '21600', 'IOT', 'Central Asia Standard Time', 'Central Asia Daylight Time', '(GMT+06:00) Astana, Dhaka', 'FALSE', '21600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Irkutsk', 'Irkutsk', 'Russia', '28800', 'IRKST', 'North Asia East Standard Time', 'North Asia East Daylight Time', '(GMT+08:00) Irkutsk, Ulaan Bataar', 'TRUE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Tehran', 'Tehran', 'Iran', '12600', 'IRST', 'Iran Standard Time', 'Iran Daylight Time', '(GMT+03:30) Tehran', 'TRUE', '12600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Colombo', 'Colombo', '-', '19800', 'IST', 'Sri Lanka Standard Time', 'Sri Lanka Daylight Time', '(GMT+05:30) Sri Jayawardenepura', 'FALSE', '19800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Jerusalem', 'Jerusalem', 'Zion', '7200', 'IST', 'Middle East Standard Time', 'Middle East Daylight Time', '(GMT+02:00) Beirut', 'TRUE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Kolkata', 'Kolkata', '-', '19800', 'IST', 'India Standard Time', 'India Daylight Time', '(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi', 'FALSE', '19800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Tokyo', 'Tokyo', 'Japan', '32400', 'JST', 'Tokyo Standard Time', 'Tokyo Daylight Time', '(GMT+09:00) Osaka, Sapporo, Tokyo', 'FALSE', '32400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Bishkek', 'Bishkek', '-', '21600', 'KGT', 'Central Asia Standard Time', 'Central Asia Daylight Time', '(GMT+06:00) Astana, Dhaka', 'FALSE', '21600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Kosrae', 'Kosrae', '-', '39600', 'KOST', 'Central Pacific Standard Time', 'Central Pacific Daylight Time', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 'FALSE', '39600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Krasnoyarsk', 'Krasnoyarsk', 'Russia', '25200', 'KRAST', 'North Asia Standard Time', 'North Asia Daylight Time', '(GMT+07:00) Krasnoyarsk', 'TRUE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Pyongyang', 'Pyongyang', '-', '32400', 'KST', 'Korea Standard Time', 'Korea Daylight Time', '(GMT+09:00) Seoul', 'FALSE', '32400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Seoul', 'Seoul', 'ROK', '32400', 'KST', 'Korea Standard Time', 'Korea Daylight Time', '(GMT+09:00) Seoul', 'FALSE', '32400');
INSERT INTO `Ref_timezone` VALUES ('Australia/Lord_Howe', 'Lord_Howe', 'LH', '37800', 'LHST', 'Central Pacific Standard Time', 'Central Pacific Daylight Time', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 'FALSE', '39600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Kiritimati', 'Kiritimati', '-', '50400', 'LINT', 'Tonga Standard Time', 'Tonga Daylight Time', '(GMT+13:00) Nuku\'alofa', 'FALSE', '46800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Magadan', 'Magadan', 'Russia', '39600', 'MAGST', 'Central Pacific Standard Time', 'Central Pacific Daylight Time', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 'FALSE', '39600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Marquesas', 'Marquesas', '-', '-30600', 'MART', 'Pacific Standard Time (Mexico)', 'Pacific Daylight Time (Mexico)', '(GMT-08:00) Tijuana, Baja California', 'TRUE', '-28800');
INSERT INTO `Ref_timezone` VALUES ('Antarctica/Mawson', 'Mawson', '-', '21600', 'MAWT', 'Central Asia Standard Time', 'Central Asia Daylight Time', '(GMT+06:00) Astana, Dhaka', 'FALSE', '21600');
INSERT INTO `Ref_timezone` VALUES ('MET', 'MET', 'C-Eur', '3600', 'MEST', 'Central European Standard Time', 'Central European Daylight Time', '(GMT+01:00) Sarajevo, Skopje, Warsaw, Zagreb', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Kwajalein', 'Kwajalein', '-', '43200', 'MHT', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Majuro', 'Majuro', '-', '43200', 'MHT', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Rangoon', 'Rangoon', '-', '23400', 'MMT', 'Myanmar Standard Time', 'Myanmar Daylight Time', '(GMT+06:30) Yangon (Rangoon)', 'FALSE', '23400');
INSERT INTO `Ref_timezone` VALUES ('Europe/Moscow', 'Moscow', 'Russia', '10800', 'MSK/MSD', 'Russian Standard Time', 'Russian Daylight Time', '(GMT+03:00) Moscow, St. Petersburg, Volgograd', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('America/Boise', 'Boise', 'US', '-25200', 'MST', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Cambridge_Bay', 'Cambridge_Bay', 'Canada', '-25200', 'MST', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Chihuahua', 'Chihuahua', 'Mexico', '-25200', 'MST', 'Mountain Standard Time (Mexico)', 'Mountain Daylight Time (Mexico)', '(GMT-07:00) Chihuahua, La Paz, Mazatlan', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Dawson_Creek', 'Dawson_Creek', '-', '-25200', 'MST', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Denver', 'Denver', 'US', '-25200', 'MST', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Edmonton', 'Edmonton', 'Canada', '-25200', 'MST', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Hermosillo', 'Hermosillo', '-', '-25200', 'MST', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Inuvik', 'Inuvik', 'Canada', '-25200', 'MST', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Mazatlan', 'Mazatlan', 'Mexico', '-25200', 'MST', 'Mountain Standard Time (Mexico)', 'Mountain Daylight Time (Mexico)', '(GMT-07:00) Chihuahua, La Paz, Mazatlan', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Phoenix', 'Phoenix', '-', '-25200', 'MST', 'US Mountain Standard Time', 'US Mountain Daylight Time', '(GMT-07:00) Arizona', 'FALSE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('America/Yellowknife', 'Yellowknife', 'Canada', '-25200', 'MST', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('MST', 'MST', '-', '-25200', 'MST', 'Mountain Standard Time', 'Mountain Daylight Time', '(GMT-07:00) Mountain Time (US & Canada)', 'TRUE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('MST7MDT', 'MST7MDT', 'US', '-25200', 'MST', 'US Mountain Standard Time', 'US Mountain Daylight Time', '(GMT-07:00) Arizona', 'FALSE', '-25200');
INSERT INTO `Ref_timezone` VALUES ('Indian/Mauritius', 'Mauritius', '-', '14400', 'MUT', 'Azerbaijan Standard Time', 'Azerbaijan Daylight Time', '(GMT+04:00) Baku', 'TRUE', '14400');
INSERT INTO `Ref_timezone` VALUES ('Indian/Maldives', 'Maldives', '-', '18000', 'MVT', 'West Asia Standard Time', 'West Asia Daylight Time', '(GMT+05:00) Tashkent', 'FALSE', '18000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Kuala_Lumpur', 'Kuala_Lumpur', '-', '28800', 'MYT', 'Malay Peninsula Standard Time', 'Malay Peninsula Daylight Time', '(GMT+08:00) Kuala Lumpur, Singapore', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Kuching', 'Kuching', '-', '28800', 'MYT', 'Malay Peninsula Standard Time', 'Malay Peninsula Daylight Time', '(GMT+08:00) Kuala Lumpur, Singapore', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Noumea', 'Noumea', 'NC', '39600', 'NCST', 'Central Pacific Standard Time', 'Central Pacific Daylight Time', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 'FALSE', '39600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Norfolk', 'Norfolk', '-', '41400', 'NFT', 'Central Pacific Standard Time', 'Central Pacific Daylight Time', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 'FALSE', '39600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Novosibirsk', 'Novosibirsk', 'Russia', '21600', 'NOVST', 'N. Central Asia Standard Time', 'N. Central Asia Daylight Time', '(GMT+06:00) Almaty, Novosibirsk', 'TRUE', '21600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Katmandu', 'Katmandu', '-', '20700', 'NPT', 'Nepal Standard Time', 'Nepal Daylight Time', '(GMT+05:45) Kathmandu', 'FALSE', '20700');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Nauru', 'Nauru', '-', '43200', 'NRT', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('America/St_Johns', 'St_Johns', 'StJohns', '-9000', 'NST', 'Newfoundland Standard Time', 'Newfoundland Daylight Time', '(GMT-03:30) Newfoundland', 'TRUE', '-12600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Niue', 'Niue', '-', '-39600', 'NUT', 'Samoa Standard Time', 'Samoa Daylight Time', '(GMT-11:00) Midway Island, Samoa', 'FALSE', '-39600');
INSERT INTO `Ref_timezone` VALUES ('Antarctica/McMurdo', 'McMurdo', 'NZAQ', '43200', 'NZST', 'New Zealand Standard Time', 'New Zealand Daylight Time', '(GMT+12:00) Auckland, Wellington', 'TRUE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Auckland', 'Auckland', 'NZ', '43200', 'NZST', 'New Zealand Standard Time', 'New Zealand Daylight Time', '(GMT+12:00) Auckland, Wellington', 'TRUE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Omsk', 'Omsk', 'Russia', '21600', 'OMSST', 'N. Central Asia Standard Time', 'N. Central Asia Daylight Time', '(GMT+06:00) Almaty, Novosibirsk', 'TRUE', '21600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Oral', 'Oral', '-', '18000', 'ORAT', 'Ekaterinburg Standard Time', 'Ekaterinburg Daylight Time', '(GMT+05:00) Ekaterinburg', 'TRUE', '18000');
INSERT INTO `Ref_timezone` VALUES ('America/Lima', 'Lima', 'Peru', '-18000', 'PEST', 'SA Pacific Standard Time', 'SA Pacific Daylight Time', '(GMT-05:00) Bogota, Lima, Quito, Rio Branco', 'FALSE', '-18000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Kamchatka', 'Kamchatka', 'Russia', '43200', 'PETST', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Port_Moresby', 'Port_Moresby', '-', '36000', 'PGT', 'West Pacific Standard Time', 'West Pacific Daylight Time', '(GMT+10:00) Guam, Port Moresby', 'FALSE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Enderbury', 'Enderbury', '-', '46800', 'PHOT', 'Tonga Standard Time', 'Tonga Daylight Time', '(GMT+13:00) Nuku\'alofa', 'FALSE', '46800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Manila', 'Manila', 'Phil', '28800', 'PHST', 'Malay Peninsula Standard Time', 'Malay Peninsula Daylight Time', '(GMT+08:00) Kuala Lumpur, Singapore', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Karachi', 'Karachi', 'Pakistan', '18000', 'PKST', 'West Asia Standard Time', 'West Asia Daylight Time', '(GMT+05:00) Tashkent', 'FALSE', '18000');
INSERT INTO `Ref_timezone` VALUES ('America/Miquelon', 'Miquelon', 'Canada', '-10800', 'PMST', 'Greenland Standard Time', 'Greenland Daylight Time', '(GMT-03:00) Greenland', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Ponape', 'Ponape', '-', '39600', 'PONT', 'Central Pacific Standard Time', 'Central Pacific Daylight Time', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 'FALSE', '39600');
INSERT INTO `Ref_timezone` VALUES ('America/Dawson', 'Dawson', 'Canada', '-28800', 'PST', 'Pacific Standard Time', 'Pacific Daylight Time', '(GMT-08:00) Pacific Time (US & Canada)', 'TRUE', '-28800');
INSERT INTO `Ref_timezone` VALUES ('America/Los_Angeles', 'Los_Angeles', 'US', '-28800', 'PST', 'Pacific Standard Time', 'Pacific Daylight Time', '(GMT-08:00) Pacific Time (US & Canada)', 'TRUE', '-28800');
INSERT INTO `Ref_timezone` VALUES ('America/Tijuana', 'Tijuana', 'Mexico', '-28800', 'PST', 'Pacific Standard Time (Mexico)', 'Pacific Daylight Time (Mexico)', '(GMT-08:00) Tijuana, Baja California', 'TRUE', '-28800');
INSERT INTO `Ref_timezone` VALUES ('America/Vancouver', 'Vancouver', 'Canada', '-28800', 'PST', 'Pacific Standard Time', 'Pacific Daylight Time', '(GMT-08:00) Pacific Time (US & Canada)', 'TRUE', '-28800');
INSERT INTO `Ref_timezone` VALUES ('America/Whitehorse', 'Whitehorse', 'Canada', '-28800', 'PST', 'Pacific Standard Time', 'Pacific Daylight Time', '(GMT-08:00) Pacific Time (US & Canada)', 'TRUE', '-28800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Pitcairn', 'Pitcairn', '-', '-28800', 'PST', 'Pacific Standard Time', 'Pacific Daylight Time', '(GMT-08:00) Pacific Time (US & Canada)', 'TRUE', '-28800');
INSERT INTO `Ref_timezone` VALUES ('PST8PDT', 'PST8PDT', 'US', '-28800', 'PST', 'Pacific Standard Time', 'Pacific Daylight Time', '(GMT-08:00) Pacific Time (US & Canada)', 'TRUE', '-28800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Palau', 'Palau', '-', '32400', 'PWT', 'Tokyo Standard Time', 'Tokyo Daylight Time', '(GMT+09:00) Osaka, Sapporo, Tokyo', 'FALSE', '32400');
INSERT INTO `Ref_timezone` VALUES ('America/Asuncion', 'Asuncion', 'Para', '-14400', 'PYST', 'Central Brazilian Standard Time', 'Central Brazilian Daylight Time', '(GMT-04:00) Manaus', 'TRUE', '-14400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Qyzylorda', 'Qyzylorda', '-', '21600', 'QYZT', 'N. Central Asia Standard Time', 'N. Central Asia Daylight Time', '(GMT+06:00) Almaty, Novosibirsk', 'TRUE', '21600');
INSERT INTO `Ref_timezone` VALUES ('Indian/Reunion', 'Reunion', '-', '14400', 'RET', 'Arabian Standard Time', 'Arabian Daylight Time', '(GMT+04:00) Abu Dhabi, Muscat', 'FALSE', '14400');
INSERT INTO `Ref_timezone` VALUES ('Antarctica/Rothera', 'Rothera', '-', '-10800', 'ROTT', 'SA Eastern Standard Time', 'SA Eastern Daylight Time', '(GMT-03:00) Georgetown', 'FALSE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('Africa/Accra', 'Accra', 'Ghana', '0', 'S', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/Freetown', 'Freetown', 'SL', '0', 'S', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Asia/Sakhalin', 'Sakhalin', 'Russia', '36000', 'SAKST', 'Vladivostok Standard Time', 'Vladivostok Daylight Time', '(GMT+10:00) Vladivostok', 'TRUE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Europe/Samara', 'Samara', 'Russia', '14400', 'SAMST', 'Caucasus Standard Time', 'Caucasus Daylight Time', '(GMT+04:00) Yerevan', 'TRUE', '14400');
INSERT INTO `Ref_timezone` VALUES ('Africa/Johannesburg', 'Johannesburg', 'SA', '7200', 'SAST', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Maseru', 'Maseru', '-', '7200', 'SAST', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Mbabane', 'Mbabane', '-', '7200', 'SAST', 'South Africa Standard Time', 'South Africa Daylight Time', '(GMT+02:00) Harare, Pretoria', 'FALSE', '7200');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Guadalcanal', 'Guadalcanal', '-', '39600', 'SBT', 'Central Pacific Standard Time', 'Central Pacific Daylight Time', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 'FALSE', '39600');
INSERT INTO `Ref_timezone` VALUES ('Indian/Mahe', 'Mahe', '-', '14400', 'SCT', 'Arabian Standard Time', 'Arabian Daylight Time', '(GMT+04:00) Abu Dhabi, Muscat', 'FALSE', '14400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Singapore', 'Singapore', '-', '28800', 'SGT', 'Malay Peninsula Standard Time', 'Malay Peninsula Daylight Time', '(GMT+08:00) Kuala Lumpur, Singapore', 'FALSE', '28800');
INSERT INTO `Ref_timezone` VALUES ('America/Paramaribo', 'Paramaribo', '-', '-10800', 'SRT', 'SA Eastern Standard Time', 'SA Eastern Daylight Time', '(GMT-03:00) Georgetown', 'FALSE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Midway', 'Midway', '-', '-39600', 'SST', 'Samoa Standard Time', 'Samoa Daylight Time', '(GMT-11:00) Midway Island, Samoa', 'FALSE', '-39600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Pago_Pago', 'Pago_Pago', '-', '-39600', 'SST', 'Samoa Standard Time', 'Samoa Daylight Time', '(GMT-11:00) Midway Island, Samoa', 'FALSE', '-39600');
INSERT INTO `Ref_timezone` VALUES ('Antarctica/Syowa', 'Syowa', '-', '10800', 'SYOT', 'E. Africa Standard Time', 'E. Africa Daylight Time', '(GMT+03:00) Nairobi', 'FALSE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Tahiti', 'Tahiti', '-', '-36000', 'TAHT', 'Hawaiian Standard Time', 'Hawaiian Daylight Time', '(GMT-10:00) Hawaii', 'FALSE', '-36000');
INSERT INTO `Ref_timezone` VALUES ('Indian/Kerguelen', 'Kerguelen', '-', '18000', 'TFT', 'West Asia Standard Time', 'West Asia Daylight Time', '(GMT+05:00) Tashkent', 'FALSE', '18000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Dushanbe', 'Dushanbe', '-', '18000', 'TJT', 'West Asia Standard Time', 'West Asia Daylight Time', '(GMT+05:00) Tashkent', 'FALSE', '18000');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Fakaofo', 'Fakaofo', '-', '-36000', 'TKT', 'Hawaiian Standard Time', 'Hawaiian Daylight Time', '(GMT-10:00) Hawaii', 'FALSE', '-36000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Dili', 'Dili', '-', '32400', 'TLT', 'Korea Standard Time', 'Korea Daylight Time', '(GMT+09:00) Seoul', 'FALSE', '32400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Ashgabat', 'Ashgabat', '-', '18000', 'TMT', 'West Asia Standard Time', 'West Asia Daylight Time', '(GMT+05:00) Tashkent', 'FALSE', '18000');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Tongatapu', 'Tongatapu', 'Tonga', '46800', 'TOST', 'Tonga Standard Time', 'Tonga Daylight Time', '(GMT+13:00) Nuku\'alofa', 'FALSE', '46800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Truk', 'Truk', '-', '36000', 'TRUT', 'West Pacific Standard Time', 'West Pacific Daylight Time', '(GMT+10:00) Guam, Port Moresby', 'FALSE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Funafuti', 'Funafuti', '-', '43200', 'TVT', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Etc/UCT', 'UCT', '-', '0', 'UCT', 'GMT Standard Time', 'GMT Daylight Time', '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London', 'TRUE', '0');
INSERT INTO `Ref_timezone` VALUES ('Asia/Ulaanbaatar', 'Ulaanbaatar', 'Mongol', '28800', 'ULAST', 'North Asia East Standard Time', 'North Asia East Daylight Time', '(GMT+08:00) Irkutsk, Ulaan Bataar', 'TRUE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Etc/UTC', 'UTC', '-', '0', 'UTC', 'GMT Standard Time', 'GMT Daylight Time', '(GMT) Greenwich Mean Time : Dublin, Edinburgh, Lisbon, London', 'TRUE', '0');
INSERT INTO `Ref_timezone` VALUES ('America/Montevideo', 'Montevideo', 'Uruguay', '-10800', 'UYST', 'Montevideo Standard Time', 'Montevideo Daylight Time', '(GMT-03:00) Montevideo', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Samarkand', 'Samarkand', '-', '18000', 'UZT', 'West Asia Standard Time', 'West Asia Daylight Time', '(GMT+05:00) Tashkent', 'FALSE', '18000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Tashkent', 'Tashkent', '-', '18000', 'UZT', 'West Asia Standard Time', 'West Asia Daylight Time', '(GMT+05:00) Tashkent', 'FALSE', '18000');
INSERT INTO `Ref_timezone` VALUES ('America/Caracas', 'Caracas', '-', '-12600', 'VET', 'Newfoundland Standard Time', 'Newfoundland Standard Time', '(GMT-03:30) Newfoundland', 'Newfo', '0');
INSERT INTO `Ref_timezone` VALUES ('Asia/Vladivostok', 'Vladivostok', 'Russia', '36000', 'VLAST', 'Vladivostok Standard Time', 'Vladivostok Daylight Time', '(GMT+10:00) Vladivostok', 'TRUE', '36000');
INSERT INTO `Ref_timezone` VALUES ('Europe/Volgograd', 'Volgograd', 'Russia', '10800', 'VOLST', 'Russian Standard Time', 'Russian Standard Time', '(GMT+03:00) Moscow, St. Petersburg, Volgograd', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Antarctica/Vostok', 'Vostok', '-', '21600', 'VOST', 'Central Asia Standard Time', 'Central Asia Daylight Time', '(GMT+06:00) Astana, Dhaka', 'FALSE', '21600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Efate', 'Efate', 'Vanuatu', '39600', 'VUST', 'Central Pacific Standard Time', 'Central Pacific Daylight Time', '(GMT+11:00) Magadan, Solomon Is., New Caledonia', 'FALSE', '39600');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Wake', 'Wake', '-', '43200', 'WAKT', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('Africa/Windhoek', 'Windhoek', 'Namibia', '3600', 'WAST', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Bangui', 'Bangui', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Brazzaville', 'Brazzaville', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Douala', 'Douala', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Kinshasa', 'Kinshasa', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Lagos', 'Lagos', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Libreville', 'Libreville', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Luanda', 'Luanda', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Malabo', 'Malabo', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Ndjamena', 'Ndjamena', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Niamey', 'Niamey', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Porto-Novo', 'Porto-Novo', '-', '3600', 'WAT', 'W. Central Africa Standard Time', 'W. Central Africa Daylight Time', '(GMT+01:00) West Central Africa', 'FALSE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/Canary', 'Canary', 'EU', '3600', 'WEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/Faroe', 'Faroe', 'EU', '3600', 'WEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Atlantic/Madeira', 'Madeira', 'EU', '3600', 'WEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Europe/Lisbon', 'Lisbon', 'EU', '3600', 'WEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('WET', 'WET', 'EU', '3600', 'WEST', 'W. Europe Standard Time', 'W. Europe Daylight Time', '(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna', 'TRUE', '3600');
INSERT INTO `Ref_timezone` VALUES ('Africa/Casablanca', 'Casablanca', '-', '0', 'WET', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Africa/El_Aaiun', 'El_Aaiun', '-', '0', 'WET', 'Greenwich Standard Time', 'Greenwich Daylight Time', '(GMT) Monrovia, Reykjavik', 'FALSE', '0');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Wallis', 'Wallis', '-', '43200', 'WFT', 'Fiji Standard Time', 'Fiji Daylight Time', '(GMT+12:00) Fiji, Kamchatka, Marshall Is.', 'FALSE', '43200');
INSERT INTO `Ref_timezone` VALUES ('America/Godthab', 'Godthab', 'EU', '-10800', 'WGST', 'Greenland Standard Time', 'Greenland Daylight Time', '(GMT-03:00) Greenland', 'TRUE', '-10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Jakarta', 'Jakarta', '-', '25200', 'WIT', 'SE Asia Standard Time', 'SE Asia Daylight Time', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 'FALSE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Asia/Pontianak', 'Pontianak', '-', '25200', 'WIT', 'SE Asia Standard Time', 'SE Asia Daylight Time', '(GMT+07:00) Bangkok, Hanoi, Jakarta', 'FALSE', '25200');
INSERT INTO `Ref_timezone` VALUES ('Antarctica/Casey', 'Casey', '-', '28800', 'WST', 'W. Australia Standard Time', 'W. Australia Daylight Time', '(GMT+08:00) Perth', 'TRUE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Australia/Perth', 'Perth', 'AW', '28800', 'WST', 'W. Australia Standard Time', 'W. Australia Daylight Time', '(GMT+08:00) Perth', 'TRUE', '28800');
INSERT INTO `Ref_timezone` VALUES ('Pacific/Apia', 'Apia', '-', '-39600', 'WST', 'Samoa Standard Time', 'Samoa Daylight Time', '(GMT-11:00) Midway Island, Samoa', 'FALSE', '-39600');
INSERT INTO `Ref_timezone` VALUES ('Asia/Yakutsk', 'Yakutsk', 'Russia', '32400', 'YAKST', 'Yakutsk Standard Time', 'Yakutsk Daylight Time', '(GMT+09:00) Yakutsk', 'TRUE', '32400');
INSERT INTO `Ref_timezone` VALUES ('Asia/Yekaterinburg', 'Yekaterinburg', 'Russia', '18000', 'YEKST', 'Ekaterinburg Standard Time', 'Ekaterinburg Daylight Time', '(GMT+05:00) Ekaterinburg', 'TRUE', '18000');
INSERT INTO `Ref_timezone` VALUES ('Asia/Riyadh87', 'AsiaRiyadh87', '-', '11220', 'zzz', 'Arabic Standard Time', 'Arabic Daylight Time', '(GMT+03:00) Baghdad', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Riyadh88', 'AsiaRiyadh88', '-', '11220', 'zzz', 'Arabic Standard Time', 'Arabic Daylight Time', '(GMT+03:00) Baghdad', 'TRUE', '10800');
INSERT INTO `Ref_timezone` VALUES ('Asia/Riyadh89', 'AsiaRiyadh89', '-', '11220', 'zzz', 'Arabic Standard Time', 'Arabic Daylight Time', '(GMT+03:00) Baghdad', 'TRUE', '10800');

-- ----------------------------
-- Table structure for RoleParams
-- ----------------------------
DROP TABLE IF EXISTS `RoleParams`;
CREATE TABLE `RoleParams` (
  `RoleParamId` bigint(20) NOT NULL AUTO_INCREMENT,
  `RoleCode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `ParamName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `ParamValue` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `Status` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`RoleParamId`),
  KEY `IDX_RoleParams_RoleCode` (`RoleCode`),
  KEY `IDX_RoleParams_Status` (`Status`),
  KEY `IDX_RoleParams_ParamName` (`ParamName`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of RoleParams
-- ----------------------------
INSERT INTO `RoleParams` VALUES ('1', 'ATC', 'SubType', 'Educational', 'A');
INSERT INTO `RoleParams` VALUES ('2', 'ATC', 'SubType', 'TrainingOrg', 'A');
INSERT INTO `RoleParams` VALUES ('3', 'ATC', 'SubType', 'Reseller', 'A');
INSERT INTO `RoleParams` VALUES ('4', 'ATC', 'SubType', 'Other', 'A');
INSERT INTO `RoleParams` VALUES ('6', 'VAR', 'SubType', 'iVAR', 'A');
INSERT INTO `RoleParams` VALUES ('7', 'VAR', 'SubType', 'dVAR', 'A');
INSERT INTO `RoleParams` VALUES ('8', 'ATC', 'SubType', 'Corporate', 'A');
INSERT INTO `RoleParams` VALUES ('9', 'ATC', 'SubType', 'test', 'X');
INSERT INTO `RoleParams` VALUES ('10', 'AAP', 'SubType', 'Educational', 'A');
INSERT INTO `RoleParams` VALUES ('11', 'AAP', 'SubType', 'TrainingOrg', 'A');
INSERT INTO `RoleParams` VALUES ('12', 'AAP', 'SubType', 'Reseller', 'A');
INSERT INTO `RoleParams` VALUES ('13', 'AAP', 'SubType', 'Other', 'A');
INSERT INTO `RoleParams` VALUES ('14', 'AAP', 'SubType', 'Corporate', 'A');
INSERT INTO `RoleParams` VALUES ('15', 'MTP', 'SubType', 'Educational', 'A');
INSERT INTO `RoleParams` VALUES ('16', 'MTP', 'SubType', 'TrainingOrg', 'A');
INSERT INTO `RoleParams` VALUES ('17', 'MTP', 'SubType', 'Reseller', 'A');
INSERT INTO `RoleParams` VALUES ('18', 'MTP', 'SubType', 'Other', 'A');
INSERT INTO `RoleParams` VALUES ('19', 'MTP', 'SubType', 'Corporate', 'A');
INSERT INTO `RoleParams` VALUES ('20', 'MTP', 'SubType', 'Unions', 'A');
INSERT INTO `RoleParams` VALUES ('21', 'MTP', 'SubType', 'Industry associations', 'A');
INSERT INTO `RoleParams` VALUES ('22', 'MTP', 'SubType', 'Professional associations', 'A');

-- ----------------------------
-- Table structure for Roles
-- ----------------------------
DROP TABLE IF EXISTS `Roles`;
CREATE TABLE `Roles` (
  `RoleId` int(10) NOT NULL AUTO_INCREMENT,
  `RoleCode` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `RoleName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SiebelName` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `FrameworkName` varchar(100) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `CRBApprovedName` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `RoleType` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `RoleSubType` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `Description` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`RoleId`),
  KEY `IDX_Roles_RoleName` (`RoleName`),
  KEY `IDX_Roles_RoleCode` (`RoleCode`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Roles
-- ----------------------------
INSERT INTO `Roles` VALUES ('1', 'ATC', 'Authorized Training Center (ATC)', '', '', '', 'Company', '', 'Authorized Training Center (ATC)');
INSERT INTO `Roles` VALUES ('2', 'VAR', 'Value Added Reseller (VAR)', '', '', '', 'Company', '', 'Value Added Reseller (VAR)');
INSERT INTO `Roles` VALUES ('3', 'Educational', 'Education Sales', '', '', '', 'Company', '', 'Education Sales');
INSERT INTO `Roles` VALUES ('4', 'VAD', 'Value Added Distributor (VAD)', '', '', '', 'Company', '', 'Value Added Distributor (VAD)');
INSERT INTO `Roles` VALUES ('5', 'Government', 'Government Sales', '', '', '', 'Company', '', 'Government Sales');
INSERT INTO `Roles` VALUES ('6', 'Autodesk', 'Autodesk Employee', '', '', '', 'Company', '', 'Autodesk Employee');
INSERT INTO `Roles` VALUES ('7', 'AuthorsPublishers', 'Authors and Publishers', '', '', '', 'Company', '', 'Authors and Publishers');
INSERT INTO `Roles` VALUES ('8', 'Certification', 'Certification Site', '', '', '', 'Company', '', 'Certification Site');
INSERT INTO `Roles` VALUES ('9', 'ADN', 'Autodesk Developer Network (ADN)', '', '', '', 'Company', '', 'Autodesk Developer Network (ADN)');
INSERT INTO `Roles` VALUES ('10', 'NRO', 'Retail', '', '', '', 'Company', '', 'Retail');
INSERT INTO `Roles` VALUES ('11', 'OwnerManager', 'Owner/Principal', 'Owner/Principal', 'OTHER', 'SALES', 'Contact', '', 'Owner Manager');
INSERT INTO `Roles` VALUES ('12', 'SalesManagement', 'Sales Manager', 'Sales Management', 'SALES', 'SALES', 'Contact', '', 'Sales Manager');
INSERT INTO `Roles` VALUES ('13', 'SalesRepresentative', 'Sales Representative', 'Sales Representative ', 'SALES', 'SALES', 'Contact', '', 'Sales Representative');
INSERT INTO `Roles` VALUES ('15', 'TechnicalPreSales', 'Technical Representative', 'Technical Representative', 'TECHNICAL', 'TECHNICAL', 'Contact', '', 'Technical Pre-Sales');
INSERT INTO `Roles` VALUES ('18', 'MarketingManager', 'Marketing', 'Marketing', 'MARKETING', 'OTHER', 'Contact', '', 'Marketing Manager');
INSERT INTO `Roles` VALUES ('19', 'AdminOE', 'Admin/Order Entry', 'Admin OE', 'OTHER', 'OTHER', 'Contact', '', 'Admin/Order Entry');
INSERT INTO `Roles` VALUES ('23', 'InstructorApplicationEngineer', 'Instructor', 'Training', 'SUPPORT', 'SUPPORT', 'Contact', '', 'Instructor');
INSERT INTO `Roles` VALUES ('29', 'Unassigned', 'Unassigned', '', '', 'OTHER', 'Contact', '', 'Unassigned');
INSERT INTO `Roles` VALUES ('30', 'Contractor', 'Autodesk Contractor', '', '', '', 'Company', '', 'Autodesk Contractor');
INSERT INTO `Roles` VALUES ('31', 'DirectSales', 'Named/Major Account/Direct Sales', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Major/Direct Sales');
INSERT INTO `Roles` VALUES ('32', 'VerticalSales', 'Vertical Sales', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Vertical Sales');
INSERT INTO `Roles` VALUES ('33', 'ChannelSales', 'Channel/Distribution Sales', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Channel Sales');
INSERT INTO `Roles` VALUES ('34', 'InternalAppEngineer', 'Application Engineer', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Application Engineer');
INSERT INTO `Roles` VALUES ('35', 'InternalOther', 'Other Sales', '', '', '', 'Contact', 'Internal', 'Other Sales');
INSERT INTO `Roles` VALUES ('36', 'SolutionExecutive', 'Solution Executive', '', '', '', 'Contact', 'Internal', 'Autodesk Internal - Solution Executive ');
INSERT INTO `Roles` VALUES ('37', 'ConsultingSpec', 'Consulting', '', '', '', 'Contact', 'Specialist', 'Consulting');
INSERT INTO `Roles` VALUES ('38', 'FactorySales', 'Factory Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Factory Sales');
INSERT INTO `Roles` VALUES ('39', 'ProcessPlantDesignSales', 'Process Plant Design Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Process Plant Design Sales');
INSERT INTO `Roles` VALUES ('40', 'BESystemsSales', 'Building Engineering - MEP Systems Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Building Engineering - MEP Systems Sales');
INSERT INTO `Roles` VALUES ('41', 'BEStructuralSales', 'Building Engineering - Structural Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Building Engineering - Structural Sales');
INSERT INTO `Roles` VALUES ('42', 'FactoryTechnical', 'Factory Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Factory Technical');
INSERT INTO `Roles` VALUES ('43', 'ProcessPlantDesignTechnical', 'Process Plant Design Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Process Plant Design Technical');
INSERT INTO `Roles` VALUES ('44', 'BESystemsTechnical', 'Building Engineering - MEP Systems Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Building Engineering - MEP Systems Technical');
INSERT INTO `Roles` VALUES ('45', 'BEStructuralTechnical', 'Building Engineering - Structural Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Building Engineering - Structural Technical');
INSERT INTO `Roles` VALUES ('46', 'InsideSales', 'Inside Sales', '', '', '', 'Contact', 'Internal', 'Inside Sales');
INSERT INTO `Roles` VALUES ('47', 'InternalSalesManager', 'Internal Sales Manager', '', '', '', 'Contact', 'Internal', 'Internal Sales Manager');
INSERT INTO `Roles` VALUES ('48', 'EditorialFinishingSales', 'Editorial Finishing Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Editorial Finishing Sales');
INSERT INTO `Roles` VALUES ('49', 'EditorialFinishingTechnical', 'Editorial Finishing Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Editorial Finishing Technical');
INSERT INTO `Roles` VALUES ('50', 'SimulationSales', 'Simulation Sales', '', 'SALES', '', 'Contact', 'Specialist', 'Simulation Sales');
INSERT INTO `Roles` VALUES ('51', 'SimulationTechnical', 'Simulation Technical', '', 'TECHNICAL', '', 'Contact', 'Specialist', 'Simulation Technical');
INSERT INTO `Roles` VALUES ('52', 'VCP', 'VCP', '', 'SALES', 'SALES', 'Contact', '', 'VCP');
INSERT INTO `Roles` VALUES ('53', 'AITC', 'Autodesk Internal Training Center (AITC)', '', '', '', 'Company', '', 'Autodesk Internal Training Center (AITC)');
INSERT INTO `Roles` VALUES ('54', 'RED', 'Regional Education Distributor (RED)', '', '', '', 'Company', '', 'Regional Education Distributor (RED)');
INSERT INTO `Roles` VALUES ('55', 'MED', 'Master Education Distributor (MED)', '', '', '', 'Company', '', 'Master Education Distributor (MED)');
INSERT INTO `Roles` VALUES ('58', 'AAP', 'Authorized Academic Partner (AAP)', '', '', '', 'Company', '', 'Authorized Academic Partner (AAP)');
INSERT INTO `Roles` VALUES ('60', 'MTP', 'Membership Training Partner (MTP)', '', '', '', 'Company', '', 'Membership Training Partner (MTP)');

-- ----------------------------
-- Table structure for Site_service
-- ----------------------------
DROP TABLE IF EXISTS `Site_service`;
CREATE TABLE `Site_service` (
  `site_service_id` int(8) NOT NULL AUTO_INCREMENT,
  `site_service_code` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `site_service_name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Invoice',
  `status` char(1) COLLATE utf8_unicode_ci DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`site_service_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Site_service
-- ----------------------------
INSERT INTO `Site_service` VALUES ('114', 'FDEP', 'Faculty Deployment', 'Faculty Deployment', 'Y', 'admin', '0001-01-01 00:00:00', 'admin', '2018-01-13 14:59:18');
INSERT INTO `Site_service` VALUES ('115', 'FDEV', 'Faculty Development', 'Faculty Development', 'Y', null, '2018-01-13 00:00:00', null, '2018-01-13 00:00:00');
INSERT INTO `Site_service` VALUES ('2', 'CDEV', 'Content Development-', 'Content Development', null, null, '0001-01-01 00:00:00', null, '0001-01-01 00:00:00');
INSERT INTO `Site_service` VALUES ('116', 'SDEV', 'Student Development', 'Student Development', 'Y', 'admin', '2018-01-13 14:53:20', 'admin', '2018-01-13 14:56:55');
INSERT INTO `Site_service` VALUES ('3', 'SIMP', 'Software Implementation', 'Software Implementation', 'Y', 'ATCDB000', '2006-10-11 18:07:58', 'morganj', '2008-03-05 04:49:54');
INSERT INTO `Site_service` VALUES ('4', 'CERT', 'Certification', 'Certification', 'Y', 'ATCDB000', '2006-10-11 18:08:54', 'morganj', '2008-03-05 04:48:58');
INSERT INTO `Site_service` VALUES ('5', 'TINT', 'Technology Integration', 'Technology Integration', 'Y', 'ATCDB000', '2006-10-11 18:09:10', 'morganj', '2008-03-05 04:49:29');
INSERT INTO `Site_service` VALUES ('6', 'FUSION T&C', 'Fusion 360 Training / Consulting', 'Fusion 360', 'Y', 'ATCDB000', '2006-10-11 18:09:25', 'morganj', '2008-03-05 04:50:17');

-- ----------------------------
-- Table structure for User
-- ----------------------------
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `user_id` int(4) NOT NULL AUTO_INCREMENT,
  `user_level_id` varchar(5) NOT NULL,
  `email` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `password_plain` varchar(100) DEFAULT NULL,
  `profile_picture` varchar(200) DEFAULT NULL,
  `cuid` smallint(4) DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `muid` smallint(4) DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`,`user_level_id`)
) ENGINE=MyISAM AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of User
-- ----------------------------
INSERT INTO `User` VALUES ('1', 'ADM', 'admin@gmail.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'Users_2017.09.25-16.43.57-.png', '1', '2016-07-26 02:29:27', '1', '2017-09-25 16:44:19');
INSERT INTO `User` VALUES ('21', 'USER', 'user@gmail.com', 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'user', 'Users_2016.07.26-04.02.22-.png', '1', '2016-07-26 04:02:22', '1', '2017-09-20 14:38:18');

-- ----------------------------
-- Table structure for User_level
-- ----------------------------
DROP TABLE IF EXISTS `User_level`;
CREATE TABLE `User_level` (
  `user_level_id` varchar(5) NOT NULL,
  `name` varchar(32) NOT NULL,
  `eim` varchar(5) DEFAULT '00000',
  `epdb` varchar(5) DEFAULT NULL,
  `eva` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`user_level_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of User_level
-- ----------------------------
INSERT INTO `User_level` VALUES ('ADM', 'Super Admin', '11111', '11111', '11111');
INSERT INTO `User_level` VALUES ('USER', 'User', '11111', '11111', '11111');

-- ----------------------------
-- Table structure for Variable
-- ----------------------------
DROP TABLE IF EXISTS `Variable`;
CREATE TABLE `Variable` (
  `variable_id` int(20) NOT NULL AUTO_INCREMENT,
  `variable_category` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `variable_key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `variable_value` text COLLATE utf8_unicode_ci NOT NULL,
  `status` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT NULL,
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT NULL,
  PRIMARY KEY (`variable_id`)
) ENGINE=MyISAM AUTO_INCREMENT=453 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Variable
-- ----------------------------
INSERT INTO `Variable` VALUES ('1', 'SiteStatus', 'A', 'Active', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('2', 'SiteStatus', 'P', 'Pending', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('3', 'SiteStatus', 'S', 'Suspended', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('4', 'SiteStatus', 'T', 'Terminated', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('5', 'SiteStatus', 'R', 'Rejected', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('6', 'Permissions', '1', 'Guest', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('7', 'Permissions', '2', 'Read Only', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('8', 'Permissions', '3', 'Read/Write', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('9', 'Permissions', '4', 'Full Access', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('10', 'Permissions', '5', 'System Admin', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('11', 'BusinessFocus', 'DSG', 'DSG', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('12', 'BusinessFocus', '3DS_MAX', 'MED', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('13', 'BusinessFocus', 'Apac_PTD', 'APAC Platform Technology (PTD)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('14', 'BusinessFocus', 'Apac_BSD', 'APAC Building Solutions (BSD)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('15', 'BusinessFocus', 'Apac_MSD', 'APAC Manufacturing Solutions (MSD)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('16', 'BusinessFocus', 'Apac_MED', 'APAC MED', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('17', 'BusinessFocus', 'Apac_ISD', 'APAC Infrastructure Solutions (ISD)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('18', 'BusinessFocus', 'Apac_Maya', 'APAC Maya', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('19', 'BusinessFocus', 'ATC-APAC-PSEB', 'ATC-APAC-PSEB', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('20', 'BusinessFocus', 'ATC-APAC-AEC', 'ATC-APAC-AEC', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('21', 'BusinessFocus', 'ATC-APAC-MSD-ENG', 'ATC-APAC-MSD-ENG', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('22', 'BusinessFocus', 'ATC-APAC-MSD-ID', 'ATC-APAC-MSD-ID', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('23', 'BusinessFocus', 'ATC-APAC-MED-VC', 'ATC-APAC-MED-VC', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('24', 'BusinessFocus', 'ATC-APAC-MED-DE', 'ATC-APAC-MED-DE', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('25', 'CompanyTypes', 'Reseller', 'Reseller', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('26', 'CompanyTypes', 'Educational', 'Educational', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('27', 'CompanyTypes', 'TrainingOrg', 'Training Organization', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('28', 'CompanyTypes', 'Other', 'Other', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('29', 'Salutation', 'Mr', 'Mr', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('30', 'Salutation', 'Miss', 'Miss', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('31', 'Salutation', 'Mrs', 'Mrs', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('32', 'Salutation', 'Ms', 'Ms', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('33', 'Salutation', 'Dr', 'Dr', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('34', 'Salutation', 'Other', 'Other', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('35', 'Languages', 'ZH', 'Chinese', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('36', 'Languages', 'EN', 'English', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('37', 'Languages', 'FR', 'French', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('38', 'Languages', 'DE', 'German', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('39', 'Languages', 'IT', 'Italian', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('40', 'Languages', 'JA', 'Japanese', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('41', 'Languages', 'KO', 'Korean', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('42', 'Languages', 'RU', 'Russian', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('43', 'Languages', 'ES', 'Spanish', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('44', 'Languages', 'PO', 'Portuguese', 'x', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('45', 'UILanguages', 'EN', 'English', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('46', 'UILanguages', 'DE', 'German', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('47', 'UILanguages', 'FR', 'French', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('48', 'UILanguages', 'IT', 'Italian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('49', 'UILanguages', 'ES', 'Spanish', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('50', 'UILanguages', 'JA', 'Japanese', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('51', 'UILanguages', 'KO', 'Korean', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('52', 'UILanguages', 'PT-BR', 'Portuguese-Brazilian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('53', 'UILanguages', 'ZH-HANS', 'Simplified Chinese', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('54', 'UILanguages', 'RU', 'Russian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('55', 'UILanguages', 'KEYS', 'Show Variables', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('56', 'Currencies', 'AUD', 'Australian Dollar-', '', null, '0001-01-01 00:00:00', null, '0001-01-01 00:00:00');
INSERT INTO `Variable` VALUES ('57', 'Currencies', 'EUR', 'Euro', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('58', 'Currencies', 'GBP', 'British Pound (UK)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('59', 'Currencies', 'JPY', 'Japanese Yen', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('60', 'Currencies', 'NZD', 'New Zealand Dollar', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('61', 'Currencies', 'RMB', 'Chinese Yuan', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('62', 'Currencies', 'USD', 'US Dollar', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('63', 'Currencies', 'WON', 'Korean Won', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('64', 'QualificationTypes', 'Trained', 'Trained', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('65', 'QualificationTypes', 'Tested', 'Tested', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('66', 'QualificationTypes', 'Evidence', 'Provided Evidence of Experience', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('67', 'QualificationTypes', 'Other', 'Other', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('68', 'SiteAttachmentTypes', 'AppDoc', 'Application Document', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('69', 'SiteAttachmentTypes', 'Certificate', 'Certificate Image', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('70', 'SiteAttachmentTypes', 'CV', 'Curriculum Vitae', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('71', 'SiteAttachmentTypes', 'Sample', 'Sample Material', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('72', 'SiteAttachmentTypes', 'Other', 'Other', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('73', 'ContractStatuses', 'A', 'Active', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('74', 'ContractStatuses', 'I', 'Inactive', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('75', 'ContractStatuses', 'S', 'Suspended', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('76', 'PaymentMethods', '1', 'Wire Transfer', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('77', 'PaymentMethods', '2', 'CC', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('78', 'PaymentMethods', '3', 'Check/Cheque', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('79', 'PaymentMethods', '4', 'Net 30 (C004)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('80', 'PaymentMethods', '5', 'Prepaid Check (C014)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('81', 'PaymentMethods', '6', 'American Express (C020)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('82', 'PaymentMethods', '7', 'Discover (C021)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('83', 'PaymentMethods', '8', 'MasterCard (C022)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('84', 'PaymentMethods', '9', 'Visa (C023)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('85', 'JournalActivityTypes', '6', 'Organization Attribute', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('86', 'JournalActivityTypes', '5', 'Organization Journal Entry', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('87', 'JournalActivityTypes', '7', 'Site Accreditation', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('88', 'JournalActivityTypes', '3', 'Site Attribute', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('89', 'JournalActivityTypes', '1', 'Site Journal Entry', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('90', 'JournalActivityTypes', '2', 'Site Event', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('91', 'JournalActivityTypes', '9', 'Site Migration Anomalies', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('92', 'JournalActivityTypes', '8', 'Contact Attribute', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('93', 'JournalActivityTypes', '4', 'Contact Journal Entry', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('94', 'JournalActivityTypes', '10', 'Contact Migration Anomalies', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('95', 'JournalActivityTypes', '11', 'VAR Site Attribute', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('96', 'JournalActivityTypes', '12', 'VAR Contact Attribute', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('97', 'JournalActivityTypes', '13', 'VAR Organization Attribute', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('98', 'JournalActivityTypes', '0', 'Contact Security Role', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('99', 'JournalActivityTypes', '14', 'VAR Contact Journal Entry', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('100', 'BusinessFocus', 'ATC-LA-MSD', 'ATC-LA-MSD', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('101', 'BusinessFocus', 'ATC-LA-PSEB', 'ATC-LA-PSEB', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('102', 'BusinessFocus', 'ATC-LA-AEC', 'ATC-LA-AEC', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('103', 'SiteStatus', 'B', 'Submitted', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('104', 'SiteStatus', 'O', 'Opportunity', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('105', 'SiteStatus', 'V', 'Approved', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('106', 'JournalActivityTypes', '15', 'Contact Certification', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('107', 'SiteStatus', 'F', 'Fulfillment Center (ATC)', 'X', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('108', 'Live Examinations', 'CRS-EN-10-O-3712', 'AEC Building Channel Certification Exam for Sales Representatives', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('109', 'Live Examinations', 'JP-CRS-JP-10-O-4173', 'オートデスク・チャネル認定試験　AEC-Building（サポート）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('110', 'Live Examinations', 'CRS-FR-10-O-4171', 'Examen de Certification d\'Autodesk pour les représentant support à la clientèle AEC-Building', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('111', 'Live Examinations', 'CRS-ES-10-O-4175', 'Examen de certificación de canal de AEC-Construcción de Autodesk para soporte de product', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('112', 'Live Examinations', 'SE-CRS-IT-10-O-4172', 'Esame di certificazione per il canale AEC Building Autodesk per il supporto del prodotto', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('113', 'Live Examinations', 'CE-CRS-DE-10-O-4170', 'Autodesk AEC-Building Zertifizierungsprüfung für den Produkt Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('114', 'Live Examinations', 'CH-CRS-CH-10-O-4169', 'Autodesk AEC-Building Channel Certification 产品支持测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('115', 'Live Examinations', 'KO-CRS-KO-10-O-4174', 'Autodesk AEC-Building Channel Certification 시험 (Product support)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('116', 'Live Examinations', 'CRS-EN-10-O-3731', 'AEC Building Channel Certification Exam for Product Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('117', 'Live Examinations', 'JP-CRS-JP-10-O-4161', 'オートデスク・チャネル認定試験　AEC-Building（プリセールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('118', 'Live Examinations', 'CRS-FR-10-O-4159', 'Examen de Certification d\'Autodesk pour les représentant technical-commerciaux AEC-Building', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('119', 'Live Examinations', 'CRS-ES-10-O-4163', 'Examen de certificación de canal de AEC-Construcción de Autodesk para técnicos de preventa.', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('120', 'Live Examinations', 'SE-CRS-IT-10-O-4160', 'Esame di certificazione per il canale AEC Building Autodesk per tecnici di prevendita', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('121', 'Live Examinations', 'CH-CRS-CH-10-O-4157', 'Autodesk AEC-Building Channel Certification 售前技术测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('122', 'Live Examinations', 'KO-CRS-KO-10-O-4162', 'Autodesk AEC-Building Channel Certification 시험 (Pre-Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('123', 'Live Examinations', 'CE-CRS-DE-10-O-4158', 'Autodesk AEC Building Zertifizierungsprüfung für Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('124', 'Live Examinations', 'CRS-EN-10-O-3710', 'AEC Building Channel Certification Exam for Pre-Sales Technical', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('125', 'Live Examinations', 'CE-CRS-DE-10-O-4177', 'Autodesk AEC Building Zertifizierungsprüfung für Vertriebsmitarbeiter von Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('126', 'Live Examinations', 'KO-CRS-KO-10-O-4181', 'Autodesk AEC-Building Channel Certification 시험 (Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('127', 'Live Examinations', 'CH-CRS-CH-10-O-4176', 'Autodesk AEC-Building Channel Certification 销售代表测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('128', 'Live Examinations', 'SE-CRS-IT-10-O-4179', 'Esame di certificazione per il canale AEC Building Autodesk per i venditori', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('129', 'Live Examinations', 'CRS-ES-10-O-4182', 'Examen de certificación de canal de AEC-Construcción de Autodesk para Representantes de Ventas', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('130', 'Live Examinations', 'CRS-FR-10-O-4178', 'Examen de Certification d\'Autodesk pour les représentant de vente AEC-Building', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('131', 'Live Examinations', 'JP-CRS-JP-10-O-4180', 'オートデスク・チャネル認定試験　AEC-Building（セールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('132', 'Live Examinations', 'CRS-EN-10-O-3714', 'AEC Civil Channel Certification Exam for Pre-Sales Technical', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('133', 'Live Examinations', 'KO-CRS-KO-10-O-4189', 'Autodesk AEC-Civil Channel Certification 시험 (Pre-Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('134', 'Live Examinations', 'CH-CRS-CH-10-O-4184', 'Autodesk AEC-Civil Channel Certification 售前技术测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('135', 'Live Examinations', 'CE-CRS-DE-10-O-4185', 'Autodesk AEC-Civil Zertifikationsprüfung für Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('136', 'Live Examinations', 'SE-CRS-IT-10-O-4187', 'Esame di certificazione per il canale AEC Civil Autodesk per tecnici di prevendita', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('137', 'Live Examinations', 'CRS-ES-10-O-4190', 'Examen de certificación de canal de AEC-Civil de Autodesk para técnicos de preventa', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('138', 'Live Examinations', 'CRS-FR-10-O-4186', 'Examen de Certification d\'Autodesk pour les représentant technical-commerciaux AEC-Civil', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('139', 'Live Examinations', 'JP-CRS-JP-10-O-4188', 'オートデスク・チャネル認定試験　AEC-Civil（プリセールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('140', 'Live Examinations', 'CRS-EN-10-O-3729', 'AEC Civil Channel Certification Exam for Product Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('141', 'Live Examinations', 'KO-CRS-KO-10-O-4196', 'Autodesk AEC-Civil Channel Certification 시험 (Product support)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('142', 'Live Examinations', 'CH-CRS-CH-10-O-4191', 'Autodesk AEC-Civil Channel Certification 产品支持测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('143', 'Live Examinations', 'CE-CRS-DE-10-O-4192', 'Autodesk AEC-Civil Zertifizierungsprüfung für den Produktsupport', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('144', 'Live Examinations', 'SE-CRS-IT-10-O-4194', 'Esame di certificazione per il canale AEC Civil Autodesk per il supporto del prodotto', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('145', 'Live Examinations', 'CRS-ES-10-O-4197', 'Examen de certificación de canal de AEC-Civil de Autodesk para soporte de producto', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('146', 'Live Examinations', 'CRS-FR-10-O-4193', 'Examen de Certification d\'Autodesk pour les représentant support à la clientèle AEC-Civil', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('147', 'Live Examinations', 'JP-CRS-JP-10-O-4195', 'オートデスク・チャネル認定試験　AEC-Civil（サポート）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('148', 'Live Examinations', 'CRS-EN-10-O-3716', 'AEC Civil Channel Certification Exam for Sales Representatives', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('149', 'Live Examinations', 'CH-CRS-CH-10-O-4198', 'Autodesk AEC-Civil Channel Certification Exam 销售代表测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('150', 'Live Examinations', 'KO-CRS-KO-10-O-4203', 'Autodesk AEC-Civil Channel Certification 시험 (Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('151', 'Live Examinations', 'CE-CRS-DE-10-O-4199', 'Autodesk AEC-Civil Zertifizierungsprüfung für Vertriebsmitarbeiter von Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('152', 'Live Examinations', 'SE-CRS-IT-10-O-4201', 'Esame di certificazione per il canale AEC Civil Autodesk per i venditori', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('153', 'Live Examinations', 'CRS-ES-10-O-4204', 'Examen de certificación de canal de AEC-Civil de Autodesk para Representantes de Ventas', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('154', 'Live Examinations', 'CRS-FR-10-O-4200', 'Examen de Certification d\'Autodesk pour les représentant de vente AEC-Civil', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('155', 'Live Examinations', 'JP-CRS-JP-10-O-4202', 'オートデスク・チャネル認定試験　AEC-Civil（セールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('156', 'Live Examinations', 'KO-CRS-KO-10-O-4251', 'Autodesk Horizontal Design Channel Certification 시험 (Pre-Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('157', 'Live Examinations', 'CH-CRS-CH-10-O-4246', 'Autodesk Horizontal Design Channel Certification 售前技术测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('158', 'Live Examinations', 'CE-CRS-DE-10-O-4247', 'Autodesk Horizontal Design Zertifizierungsprüfung für Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('159', 'Live Examinations', 'CRS-FR-10-O-4248', 'Examen Conception horizontale Autodesk du programme Channel Certification pour les technico-commerciaux', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('160', 'Live Examinations', 'CRS-ES-10-O-4252', 'Examen de certificación de canal de diseño horizontal de Autodesk para técnicos de preventa', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('161', 'Live Examinations', 'CRS-EN-10-O-3775', 'Horizontal Design Channel Certification Exam for Pre-Sales Technical', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('162', 'Live Examinations', 'SE-CRS-IT-10-O-4249', 'Progettazione orizzontale Autodesk - Esame di certificazione canale per tecnici di prevendita', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('163', 'Live Examinations', 'JP-CRS-JP-10-O-4250', 'オートデスク・チャネル認定試験　Horizontal Design（プリセールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('164', 'Live Examinations', 'KO-CRS-KO-10-O-4259', 'Autodesk Horizontal Design Channel Certification 시험 (Product support)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('165', 'Live Examinations', 'CH-CRS-CH-10-O-4254', 'Autodesk Horizontal Design Channel Certification 产品支持测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('166', 'Live Examinations', 'CE-CRS-DE-10-O-4255', 'Autodesk Horizontal Design Zertifizierungsprüfung für für den Produkt-Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('167', 'Live Examinations', 'CRS-FR-10-O-4256', 'Examen Conception horizontale du programme Channel Certification pour les techniciens d\'assistance produit', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('168', 'Live Examinations', 'CRS-ES-10-O-4260', 'Examen de certificación de canal de diseño horizontal para soporte de producto', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('169', 'Live Examinations', 'CRS-EN-10-O-3776', 'Horizontal Design Channel Certification Exam for Product Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('170', 'Live Examinations', 'SE-CRS-IT-10-O-4257', 'Progettazione orizzontale - Esame di certificazione canale per il supporto del prodotto', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('171', 'Live Examinations', 'JP-CRS-JP-10-O-4258', 'オートデスク・チャネル認定試験　Horizontal Design（サポート）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('172', 'Live Examinations', 'KO-CRS-KO-10-O-4266', 'Autodesk Horizontal Design Channel Certification 시험 (Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('173', 'Live Examinations', 'CH-CRS-CH-10-O-4261', 'Autodesk Horizontal Design Channel Certification 销售代表测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('174', 'Live Examinations', 'CE-CRS-DE-10-O-4262', 'Autodesk Horizontal Design Zertifizierungsprüfung für Vertriebsmitarbeiter bei Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('175', 'Live Examinations', 'CRS-FR-10-O-4263', 'Examen Conception horizontale Autodesk du programme Channel Certification pour les commerciaux', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('176', 'Live Examinations', 'CRS-ES-10-O-4267', 'Examen de certificación de canal de diseño horizontal de Autodesk para Representantes de Ventas', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('177', 'Live Examinations', 'CRS-EN-10-O-3774', 'Horizontal Design Channel Certification Exam for Sales Representatives', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('178', 'Live Examinations', 'SE-CRS-IT-10-O-4264', 'Progettazione orizzontale Autodesk - Esame di certificazione canale per i venditori', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('179', 'Live Examinations', 'JP-CRS-JP-10-O-4265', 'オートデスク・チャネル認定試験　Horizontal Design（セールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('180', 'Live Examinations', 'CRS-CH-10-O-4060', 'Autodesk Manufacturing Channel Certification - 售前技术测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('181', 'Live Examinations', 'CRS-KO-10-O-4066', 'Autodesk Manufacturing Channel Certification 시험 (Pre-Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('182', 'Live Examinations', 'CRS-DE-10-O-4062', 'Autodesk Maschinenbau Zertifizierungsprüfung für Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('183', 'Live Examinations', 'CRS-IT-10-O-4064', 'Esame di certificazione per il Canale Manifatturiero Autodesk per tecnici di Prevendita', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('184', 'Live Examinations', 'CRS-FR-10-O-4063', 'Examen Autodesk Manufacturing : programme de certification du Channel pour les technico-commerciaux.', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('185', 'Live Examinations', 'CRS-ES-10-O-4067', 'Examen de certificación de canal de manufactura Autodesk para técnicos de preventa', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('186', 'Live Examinations', 'CRS-EN-10-O-3428', 'Manufacturing Channel Certification Exam for Pre-Sales Technical', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('187', 'Live Examinations', 'CRS-JP-10-O-4065', 'オートデスク・チャネル認定試験　製造（プリセールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('188', 'Live Examinations', 'CRS-KO-10-O-4050', 'Autodesk Manufacturing Channel Certification 시험 (Product support)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('189', 'Live Examinations', 'CRS-CH-10-O-4044', 'Autodesk Manufacturing Channel Certification 产品支持测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('190', 'Live Examinations', 'CRS-DE-10-O-4046', 'Autodesk Maschinenbau Zertifizierungsprüfung für für den Produkt-Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('191', 'Live Examinations', 'CRS-IT-10-O-4048', 'Esame di certificazione per il Canale Manifatturiero Autodesk per il supporto del prodotto', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('192', 'Live Examinations', 'CRS-ES-10-O-4051', 'Examen de certificación de canal de manufactura Autodesk para soporte de producto', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('193', 'Live Examinations', 'CRS-FR-10-O-4047', 'Examen Fabrication Autodesk du programme Channel Certification pour les techniciens d\'assistance', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('194', 'Live Examinations', 'CRS-EN-10-O-3427', 'Manufacturing Channel Certification Exam for Product Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('195', 'Live Examinations', 'CRS-JP-10-O-4049', 'オートデスク・チャネル認定試験　製造（サポート）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('196', 'Live Examinations', 'CRS-KO-10-O-4058', 'Autodesk Manufacturing Channel Certification 시험 (Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('197', 'Live Examinations', 'CRS-CH-10-O-4052', 'Autodesk Manufacturing Channel Certification 销售代表测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('198', 'Live Examinations', 'CRS-DE-10-O-4054', 'Autodesk Maschinenbau -Zertifizierungsprüfung für Vertriebsmitarbeiter von Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('199', 'Live Examinations', 'CRS-IT-10-O-4056', 'Esame di certificazione per il Canale Manifatturiero Autodesk per i venditori', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('200', 'Live Examinations', 'CRS-FR-10-O-4055', 'Examen Autodesk Manufacturing : programme de certification du Channel pour les commerciaux', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('201', 'Live Examinations', 'CRS-ES-10-O-4059', 'Examen de certificación de canal de manufactura Autodesk para Representantes de Ventas', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('202', 'Live Examinations', 'CRS-EN-10-O-3418', 'Manufacturing Channel Certification Exam for Sales Representatives', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('203', 'Live Examinations', 'CRS-JP-10-O-4057', 'オートデスク・チャネル認定試験　製造（セールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('204', 'Live Examinations', 'KO-CRS-KO-10-O-4211', 'Autodesk M&E Channel Certification 시험 (Pre-Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('205', 'Live Examinations', 'CH-CRS-CH-10-O-4206', 'Autodesk M&E Channel Certification 售前技术测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('206', 'Live Examinations', 'CE-CRS-DE-10-O-4207', 'Autodesk M&E Zertifizierungsprüfung Technische Vertriebsmitarbeiter bei Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('207', 'Live Examinations', 'SE-CRS-IT-10-O-4209', 'Esame di certificazione per il canale M&E Autodesk per tecnici di prevendita', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('208', 'Live Examinations', 'CRS-ES-10-O-4212', 'Examen de certificación de canal de M&E de Autodesk para técnicos de preventa', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('209', 'Live Examinations', 'CRS-FR-10-O-4208', 'Examen de Certification d\'Autodesk pour les représentant technical-commerciaux, MandE', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('210', 'Live Examinations', 'CRS-EN-10-O-3718', 'Media and Entertainment Channel Certification Exam for Pre-Sales Technical', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('211', 'Live Examinations', 'JP-CRS-JP-10-O-4210', 'オートデスク・チャネル認定試験　M&E（プリセールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('212', 'Live Examinations', 'CH-CRS-CH-10-O-4213', 'AEC Media and Entertainment Channel Certification Exam for Product Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('213', 'Live Examinations', 'JP-CRS-JP-10-O-4217', 'AEC Media and Entertainment Channel Certification Exam for Product Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('214', 'Live Examinations', 'KO-CRS-KO-10-O-4218', 'AEC Media and Entertainment Channel Certification Exam for Product Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('215', 'Live Examinations', 'CE-CRS-DE-10-O-4214', 'Autodesk M&E Zertifizierungsprüfung für den Produkt Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('216', 'Live Examinations', 'SE-CRS-IT-10-O-4216', 'Esame di certificazione per il canale M&E Autodesk per il supporto del prodotto', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('217', 'Live Examinations', 'CRS-ES-10-O-4219', 'Examen de certificación de canal de M&E de Autodesk para soporte de producto.', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('218', 'Live Examinations', 'CRS-FR-10-O-4215', 'Examen de Certification d\'Autodesk pour les représentant support à la clientele, MandE', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('219', 'Live Examinations', 'CRS-EN-10-O-3727', 'Media and Entertainment Channel Certification Exam for Product Support', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('220', 'Live Examinations', 'KO-CRS-KO-10-O-4225', 'Autodesk M&E Channel Certification 시험 (Sales)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('221', 'Live Examinations', 'CH-CRS-CH-10-O-4220', 'Autodesk M&E Channel Certification 销售代表测试', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('222', 'Live Examinations', 'CE-CRS-DE-10-O-4221', 'Autodesk M&E Zertifizierungsprüfung für Vertriebsmitarbeiter von Vertriebspartnern', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('223', 'Live Examinations', 'SE-CRS-IT-10-O-4223', 'Esame di certificazione per il canale M&E Autodesk per i venditori', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('224', 'Live Examinations', 'CRS-ES-10-O-4226', 'Examen de certificación de canal de MyE de Autodesk para Representantes de Ventas', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('225', 'Live Examinations', 'CRS-FR-10-O-4222', 'Examen de Certification d\'Autodesk pour les représentant de vente, MandE', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('226', 'Live Examinations', 'CRS-EN-10-O-3720', 'Media and Entertainment Channel Certification Exam for Sales Representatives', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('227', 'Live Examinations', 'JP-CRS-JP-10-O-4224', 'オートデスク・チャネル認定試験　M&E（セールス）', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('228', 'BusinessFocus', 'ATCWWAEC', 'ATC-WW-AEC (FY12)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('229', 'BusinessFocus', 'ATCWWMFG', 'ATC-WW-MFG (FY12)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('230', 'BusinessFocus', 'ATCWWME', 'ATC-WW-MED (FY12)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('231', 'BusinessFocus', 'ATCWWHRZ', 'ATC-WW-HRZ (FY12)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('232', 'BusinessFocus', 'ATCWWSFM2011', 'ATC-WW-SFM-2011 (FY12)SmkM2011', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('233', 'BusinessFocus', 'ATCWWSFM2012', 'ATC-WW-SFM-2012 (FY12) SmkM2012', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('234', 'Live Examinations', 'CH-CRS-CH-12-O-5134', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('235', 'Live Examinations', 'CE-CRS-DE-12-O-5135', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('236', 'Live Examinations', 'CRS-FR-12-O-5136', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('237', 'Live Examinations', 'SE-CRS-IT-12-O-5137', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('238', 'Live Examinations', 'JP-CRS-JP-12-O-5138', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('239', 'Live Examinations', 'KO-CRS-KO-12-O-5139', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('240', 'Live Examinations', 'CRS-ES-12-O-5140', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('241', 'Live Examinations', 'CRS-PB-12-O-5141', 'Autodesk AEC Building Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('242', 'Live Examinations', 'EC-CRS-RU-12-O-5142', 'Проектирование зданий: сертификационный экзамен для партнеров (менеджеры по продажам)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('243', 'Live Examinations', 'CH-CRS-CH-12-O-5143', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('244', 'Live Examinations', 'CE-CRS-DE-12-O-5144', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('245', 'Live Examinations', 'CRS-FR-12-O-5145', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('246', 'Live Examinations', 'SE-CRS-IT-12-O-5146', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('247', 'Live Examinations', 'JP-CRS-JP-12-O-5147', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('248', 'Live Examinations', 'KO-CRS-KO-12-O-5148', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('249', 'Live Examinations', 'CRS-ES-12-O-5149', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('250', 'Live Examinations', 'CRS-PB-12-O-5150', 'Autodesk AEC Building Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('251', 'Live Examinations', 'EC-CRS-RU-12-O-5151', 'Проектирование зданий: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('252', 'Live Examinations', 'CH-CRS-CH-12-O-5152', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('253', 'Live Examinations', 'CE-CRS-DE-12-O-5153', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('254', 'Live Examinations', 'CRS-FR-12-O-5154', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('255', 'Live Examinations', 'SE-CRS-IT-12-O-5155', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('256', 'Live Examinations', 'JP-CRS-JP-12-O-5156', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('257', 'Live Examinations', 'kO-CRS-KO-12-O-5157', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('258', 'Live Examinations', 'CRS-ES-12-O-5158', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('259', 'Live Examinations', 'CRS-PB-12-O-5159', 'Autodesk AEC Building Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('260', 'Live Examinations', 'EC-CRS-RU-12-O-5160', 'Проектирование зданий: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('261', 'Live Examinations', 'CH-CRS-CH-12-O-5161', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('262', 'Live Examinations', 'CE-CRS-DE-12-O-5162', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('263', 'Live Examinations', 'CRS-FR-12-O-5163', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('264', 'Live Examinations', 'SE-CRS-IT-12-O-5164', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('265', 'Live Examinations', 'JP-CRS-JP-12-O-5165', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('266', 'Live Examinations', 'KO-CRS-KO-12-O-5166', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('267', 'Live Examinations', 'CRS-ES-12-O-5167', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('268', 'Live Examinations', 'CRS-PB-12-O-5168', 'Autodesk AEC Infrastructure Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('269', 'Live Examinations', 'EC-CRS-RU-12-O-5169', 'Проектирование инфраструктуры: сертификационный экзамен для партнеров (специалисты по продажам)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('270', 'Live Examinations', 'CH-CRS-CH-12-O-5170', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('271', 'Live Examinations', 'CE-CRS-DE-12-O-5171', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('272', 'Live Examinations', 'CRS-FR-12-O-5172', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('273', 'Live Examinations', 'SE-CRS-IT-12-O-5173', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('274', 'Live Examinations', 'JP-CRS-JP-12-O-5174', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('275', 'Live Examinations', 'KO-CRS-KO-12-O-5175', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('276', 'Live Examinations', 'CRS-ES-12-O-5176', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('277', 'Live Examinations', 'CRS-PB-12-O-5177', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('278', 'Live Examinations', 'EC-CRS-RU-12-O-5178', 'Проектирование инфраструктуры: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('279', 'Live Examinations', 'CH-CRS-CH-12-O-5179', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('280', 'Live Examinations', 'CE-CRS-DE-12-O-5180', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('281', 'Live Examinations', 'CRS-FR-12-O-5181', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('282', 'Live Examinations', 'SE-CRS-IT-12-O-5182', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('283', 'Live Examinations', 'JP-CRS-JP-12-O-5183', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('284', 'Live Examinations', 'KO-CRS-KO-12-O-5184', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('285', 'Live Examinations', 'CRS-ES-12-O-5185', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('286', 'Live Examinations', 'CRS-PB-12-O-5186', 'Autodesk AEC Infrastructure Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('287', 'Live Examinations', 'EC-CRS-RU-12-O-5187', 'Проектирование инфраструктуры: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('288', 'Live Examinations', 'CH-CRS-CH-12-O-5188', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('289', 'Live Examinations', 'CE-CRS-DE-12-O-5189', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('290', 'Live Examinations', 'CRS-FR-12-O-5190', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('291', 'Live Examinations', 'SE-CRS-IT-12-O-5191', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('292', 'Live Examinations', 'JP-CRS-JP-12-O-5192', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('293', 'Live Examinations', 'KO-CRS-KO-12-O-5193', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('294', 'Live Examinations', 'CRS-ES-12-O-5194', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('295', 'Live Examinations', 'CRS-PB-12-O-5195', 'Autodesk Manufacturing Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('296', 'Live Examinations', 'EC-CRS-RU-12-O-5196', 'Промышленное проектирование: сертификационный экзамен для партнеров (менеджеры по продажам)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('297', 'Live Examinations', 'CH-CRS-CH-12-O-5197', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('298', 'Live Examinations', 'CE-CRS-DE-12-O-5198', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('299', 'Live Examinations', 'CRS-FR-12-O-5199', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('300', 'Live Examinations', 'SE-CRS-IT-12-O-5200', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('301', 'Live Examinations', 'JP-CRS-JP-12-O-5201', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('302', 'Live Examinations', 'KO-CRS-KO-12-O-5202', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('303', 'Live Examinations', 'CRS-ES-12-O-5203', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('304', 'Live Examinations', 'CRS-PB-12-O-5204', 'Autodesk Manufacturing Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('305', 'Live Examinations', 'EC-CRS-RU-12-O-5205', 'Промышленное проектирование: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('306', 'Live Examinations', 'CH-CRS-CH-12-O-5206', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('307', 'Live Examinations', 'CE-CRS-DE-12-O-5207', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('308', 'Live Examinations', 'CRS-FR-12-O-5208', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('309', 'Live Examinations', 'SE-CRS-IT-12-O-5209', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('310', 'Live Examinations', 'JP-CRS-JP-12-O-5210', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('311', 'Live Examinations', 'KO-CRS-KO-12-O-5211', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('312', 'Live Examinations', 'CRS-ES-12-O-5212', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('313', 'Live Examinations', 'CRS-PB-12-O-5213', 'Autodesk Manufacturing Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('314', 'Live Examinations', 'EC-CRS-RU-12-O-5214', 'Промышленное проектирование: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('315', 'Live Examinations', 'CH-CRS-CH-12-O-5215', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('316', 'Live Examinations', 'CE-CRS-DE-12-O-5216', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('317', 'Live Examinations', 'CRS-FR-12-O-5217', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('318', 'Live Examinations', 'SE-CRS-IT-12-O-5218', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('319', 'Live Examinations', 'JP-CRS-JP-12-O-5219', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('320', 'Live Examinations', 'KO-CRS-KO-12-O-5220', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('321', 'Live Examinations', 'CRS-ES-12-O-5221', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('322', 'Live Examinations', 'CRS-PB-12-O-5222', 'Autodesk Media and Entertainment Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('323', 'Live Examinations', 'EC-CRS-RU-12-O-5223', 'Графика и анимация: сертификационный экзамен для партнеров (специалисты по продажам)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('324', 'Live Examinations', 'CH-CRS-CH-12-O-5224', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('325', 'Live Examinations', 'CE-CRS-DE-12-O-5225', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('326', 'Live Examinations', 'CRS-FR-12-O-5226', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('327', 'Live Examinations', 'SE-CRS-IT-12-O-5227', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('328', 'Live Examinations', 'JP-CRS-JP-12-O-5228', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('329', 'Live Examinations', 'KO-CRS-KO-12-O-5229', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('330', 'Live Examinations', 'CRS-ES-12-O-5230', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('331', 'Live Examinations', 'CRS-PB-12-O-5231', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('332', 'Live Examinations', 'EC-CRS-RU-12-O-5232', 'Графика и анимация: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('333', 'Live Examinations', 'CH-CRS-CH-12-O-5233', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('334', 'Live Examinations', 'CE-CRS-DE-12-O-5234', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('335', 'Live Examinations', 'CRS-FR-12-O-5235', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('336', 'Live Examinations', 'SE-CRS-IT-12-O-5236', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('337', 'Live Examinations', 'JP-CRS-JP-12-O-5237', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('338', 'Live Examinations', 'KO-CRS-KO-12-O-5238', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('339', 'Live Examinations', 'CRS-ES-12-O-5239', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('340', 'Live Examinations', 'CRS-PB-12-O-5240', 'Autodesk Media and Entertainment Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('341', 'Live Examinations', 'EC-CRS-RU-12-O-5241', 'Графика и анимация: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('342', 'Live Examinations', 'CH-CRS-CH-12-O-5242', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('343', 'Live Examinations', 'CE-CRS-DE-12-O-5243', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('344', 'Live Examinations', 'CRS-FR-12-O-5244', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('345', 'Live Examinations', 'SE-CRS-IT-12-O-5245', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('346', 'Live Examinations', 'JP-CRS-JP-12-O-5246', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('347', 'Live Examinations', 'KO-CRS-KO-12-O-5247', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('348', 'Live Examinations', 'CRS-ES-12-O-5248', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('349', 'Live Examinations', 'CRS-PB-12-O-5249', 'Autodesk Horizontal Design Channel Certification Exam for Sales Representatives for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('350', 'Live Examinations', 'EC-CRS-RU-12-O-5250', 'Горизонтальная авторизация: сертификационный экзамен для партнеров (специалисты по продажам)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('351', 'Live Examinations', 'CH-CRS-CH-12-O-5251', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('352', 'Live Examinations', 'CE-CRS-DE-12-O-5252', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('353', 'Live Examinations', 'CRS-FR-12-O-5253', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('354', 'Live Examinations', 'SE-CRS-IT-12-O-5254', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('355', 'Live Examinations', 'JP-CRS-JP-12-O-5255', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('356', 'Live Examinations', 'KO-CRS-KO-12-O-5256', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('357', 'Live Examinations', 'CRS-ES-12-O-5257', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('358', 'Live Examinations', 'CRS-PB-12-O-5258', 'Autodesk Horizontal Design Channel Certification Exam for Technical Post-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('359', 'Live Examinations', 'EC-CRS-RU-12-O-5259', 'Горизонтальная авторизация: сертификационный экзамен для партнеров (послепродажное обслуживание)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('360', 'Live Examinations', 'CH-CRS-CH-12-O-5260', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (简体中文)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('361', 'Live Examinations', 'CE-CRS-DE-12-O-5261', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Deutsch)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('362', 'Live Examinations', 'CRS-FR-12-O-5262', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Français)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('363', 'Live Examinations', 'SE-CRS-IT-12-O-5263', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Italiano)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('364', 'Live Examinations', 'JP-CRS-JP-12-O-5264', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (日本語)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('365', 'Live Examinations', 'KO-CRS-KO-12-O-5265', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (한국어)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('366', 'Live Examinations', 'CRS-ES-12-O-5266', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Español)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('367', 'Live Examinations', 'CRS-PB-12-O-5267', 'Autodesk Horizontal Design Channel Certification Exam for Technical Pre-Sales for 2012 (Português)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('368', 'Live Examinations', 'EC-CRS-RU-12-O-5268', 'Горизонтальная авторизация: сертификационный экзамен для партнеров (предпродажная подготовка)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('369', 'BusinessFocus', 'ATCWWAEC13', 'ATC-WW-AEC (FY13)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('370', 'BusinessFocus', 'ATCWWHRZ13', 'ATC-WW-HRZ (FY13)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('371', 'BusinessFocus', 'ATCWWME13', 'ATC-WW-MED (FY13)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('372', 'BusinessFocus', 'ATCWWMFG13', 'ATC-WW-MFG (FY13)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('373', 'BusinessFocus', 'ATCWWSFM201111FY13', 'ATC-WW-SFM-2011 (FY13)SmkM2011', 'X', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('374', 'BusinessFocus', 'ATCWWSFM2011FY13', 'ATC-WW-SFM-2011 (FY13)SmkM2011', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('375', 'BusinessFocus', 'ATCWWSFM2012FY13', 'ATC-WW-SFM-2012 (FY13) SmkM2012', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('376', 'BusinessFocus', 'ATCWWSFM2013FY13', 'ATC-WW-SFM-2013 (FY13) SmkM2013', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('377', 'CompanyTypes', 'Corporate', 'Corporate - internal only', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('378', 'CompanyTypes', 'test', 'test', 'X', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('379', 'BusinessFocus', 'ATCWWAEC14', 'ATC-WW-AEC (FY14)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('380', 'BusinessFocus', 'ATCWWHRZ14', 'ATC-WW-HRZ (FY14)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('381', 'BusinessFocus', 'ATCWWMED14', 'ATC-WW-MED (FY14)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('382', 'BusinessFocus', 'ATCWWMFG14', 'ATC-WW-MFG (FY14)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('383', 'QualificationTypes', 'ADSKAPPRV', 'Autodesk approved', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('384', 'Languages', '1', 'Bulgarian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('385', 'Languages', '10', 'French - Europe', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('386', 'Languages', '11', 'German', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('387', 'Languages', '12', 'Greek', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('388', 'Languages', '13', 'Hungarian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('389', 'Languages', '14', 'Icelandic', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('390', 'Languages', '15', 'Indonesian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('391', 'Languages', '16', 'Italian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('392', 'Languages', '17', 'Japanese', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('393', 'Languages', '18', 'Korean', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('394', 'Languages', '19', 'Latvian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('395', 'Languages', '2', 'Chinese - Simplified', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('396', 'Languages', '20', 'Lithuanian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('397', 'Languages', '21', 'Macedonian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('398', 'Languages', '22', 'Norwegian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('399', 'Languages', '23', 'Polish', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('400', 'Languages', '24', 'Portuguese - Europe', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('401', 'Languages', '25', 'Romanian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('402', 'Languages', '26', 'Russian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('403', 'Languages', '27', 'Croatian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('404', 'Languages', '28', 'Slovenian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('405', 'Languages', '29', 'Spanish - Europe', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('406', 'Languages', '3', 'Chinese - Traditional', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('407', 'Languages', '30', 'Swedish', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('408', 'Languages', '31', 'Turkish', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('409', 'Languages', '32', 'Thai', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('410', 'Languages', '33', 'Serbian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('411', 'Languages', '34', 'English - Europe', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('412', 'Languages', '35', 'English - UK', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('413', 'Languages', '36', 'English - South Africa', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('414', 'Languages', '37', 'English - APac', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('415', 'Languages', '38', 'French - Canada', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('416', 'Languages', '39', 'Spanish - Latin America', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('417', 'Languages', '4', 'Czech', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('418', 'Languages', '40', 'Portuguese - Brazil', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('419', 'Languages', '5', 'Danish', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('420', 'Languages', '6', 'Dutch', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('421', 'Languages', '7', 'English - North America', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('422', 'Languages', '8', 'Estonian', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('423', 'Languages', '9', 'Finnish', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('424', 'AcademicTargets', 'AT', 'Students trained', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('425', 'AcademicPrograms', 'AP', 'Panorama participation', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('426', 'AcademicProjects', 'P', 'student coursework project', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('427', 'FYIndicator', '2015', 'FY15 (2014)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('428', 'FYIndicator', '2016', 'FY16 (2015)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('429', 'AcademicPrograms', 'FSAE', 'Formula SAE', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('430', 'AcademicPrograms', 'F1S', 'Formula 1 in Schools', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('431', 'AcademicPrograms', 'ABPA', 'Autodesk Building Performance Analysis (BPA) Certificate', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('432', 'AcademicPrograms', 'DSTEAM', 'Digital STEAM Workshop', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('433', 'AcademicPrograms', 'VEX', 'VEX Robotics', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('434', 'AcademicPrograms', 'WSK', 'WorldSkills', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('435', 'AcademicTargets', 'ET', 'Educators trained', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('436', 'AcademicTargets', 'MoU', 'Memoranda of Understanding with institutions', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('437', 'FYIndicator', '2017', 'FY17 (2016)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('438', 'FYIndicator', '2018', 'FY18 (2017)', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('439', 'Currencies', 'SGD', 'Singapore Dollar', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('440', 'Currencies', 'INR', 'Indian Rupee', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('441', 'Currencies', 'CAD', 'Canadian Dollar', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('442', 'Currencies', 'BRL', 'Brazilian Real', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('443', 'Currencies', 'MXN', 'Mexican Peso', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('444', 'Currencies', 'SEK', 'Swedish Krona', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('445', 'Currencies', 'NOK', 'Norwegian Krone', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('446', 'Currencies', 'ZAR', 'South African Rand', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('447', 'Currencies', 'AED', 'Emirati Dirham', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('448', 'Currencies', 'HKD', 'Hong Kong Dollar', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('449', 'Currencies', 'TRY', 'Turkish Lira', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');
INSERT INTO `Variable` VALUES ('450', 'Currencies', 'RUB', 'Russian Ruble', 'A', 'admin', '2018-01-13 17:47:23', 'admin', '2018-01-13 17:47:23');

-- ----------------------------
-- Table structure for Variable_category
-- ----------------------------
DROP TABLE IF EXISTS `Variable_category`;
CREATE TABLE `Variable_category` (
  `variable_category_id` int(8) NOT NULL AUTO_INCREMENT,
  `variable_category` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `cuid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cdate` datetime DEFAULT '0000-00-00 00:00:00',
  `muid` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mdate` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`variable_category_id`),
  KEY `IDX_INVOICETYPE_ActivityType` (`variable_category`)
) ENGINE=MyISAM AUTO_INCREMENT=134 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of Variable_category
-- ----------------------------
INSERT INTO `Variable_category` VALUES ('2', 'SiteStatus', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('1', 'Permissions', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('3', 'BusinessFocus', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('119', 'CompanyTypes', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('120', 'Salutation', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('121', 'Languages', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('122', 'UILanguages', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('123', 'Currencies', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('124', 'QualificationTypes', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('125', 'SiteAttachmentTypes', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('126', 'ContractStatuses', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('127', 'PaymentMethods', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('128', 'JournalActivityTypes', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('129', 'Live Examinations', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('130', 'AcademicTargets', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('131', 'AcademicPrograms', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('132', 'AcademicProjects', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
INSERT INTO `Variable_category` VALUES ('133', 'FYIndicator', 'admin', '2006-10-11 17:51:18', 'admin', '2006-10-11 17:51:18');
SET FOREIGN_KEY_CHECKS=1;
