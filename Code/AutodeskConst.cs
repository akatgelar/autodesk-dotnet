﻿namespace autodesk.Code
{
    public class AutodeskConst
    {
        public const string NAN = "NAN";
        public class StatusConts
        {
            public const string A = "A";
            public const string Active = "Active";
            public const string Pending = "Pending";
            public const string D = "D";
            public const string Delete = "Delete";
            public const string I = "I";
            public const string InActive = "InActive";
            public const string P = "P";
            public const string X = "X";
            public const string Paid = "Paid";
            public const string C = "C";
            public const string Cancelled = "Cancelled";
            public const string T = "T";
            public const string Terminated = "Terminated";
            public const string S = "S";
            /// <summary>
            /// Track Email Reponse - Bounce 
            /// </summary>
            public const string B = "B";//bounce
            /// <summary>
            /// Track Email Reponse - Block 
            /// </summary>
            public const string L = "L";//block
            /// <summary>
            /// Track Email Reponse - Invalid 
            /// </summary>
            public const string V = "V";//invalid



        }



        public class FileExtension
        {
            public const string JPG = "jpg";
            public const string JPEG = "jpeg";
            public const string PNG = "png";
            public const string DOC = "doc";
            public const string DOCX = "docx";
            public const string XLS = "xls";
            public const string XLSX = "xlsx";
            public const string PDF = "pdf";
            public const string TIFF = "tiff";

        }

        public class SKUType
        {
            public const int Master = 0;
            public const int Pricing = 1;
        }

        public class DiscountName
        {
            public const string Discount = "Discount";
            public const string ProRata = "Pro-Rata";
        }
        public class AmountType
        {
            public const string Percentage = "Percentage";
            public const string Exactly = "Exactly";

        }

        public class InvoiceStatus
        {
            public const string Pending = "Pending";
            public const string Paid = "Paid";
            public const string Invalid = "Invalid";
            public const string Cancelled = "Cancelled";

        }
        public class Currency
        {
            public const string USD = "USD";
        }

        public class EmailType
        {
            public const string ApprovedDiscount = "ApprovedDiscount";
            public const string RejectedDiscount = "RejectedDiscount";
            public const string ApplyDiscount = "ApplyDiscount";
            public const string MailSent = "Mail Sent";
        }
        public class DiscountAction
        {
            public const string Rejected = "Rejected";
            public const string Created = "Created";
            public const string Approved = "Approved";

        }
        public class ResponseMessage
        {
            public const string Success = "Success";
            public const string Fail = "Failed";
            public const string NotFound = "Not found";
            public const string Error = "Error";

            public const string OTPInvalid = "Invalid";
            public const string SAUnauthorized = "Unauthorized";
        }
        public class AgreementsAction
        {
            public const string Acknowledged = "Acknowledged";
            public const string Rejected = "Rejected";
            public const string Approved = "Approved";

        }
        public class AgreementsStatus
        {
            /// <summary>
            /// Org has acknowledged agreement
            /// </summary>
            public const string Pending = "Pending";
            public const string PA = "PApproval";
            public const string PendingApproval = "Pending Approval";

            public const string PendingReview = "Pending Review";
            /// <summary>
            /// Org has yet to acknowledge
            /// </summary>
            public const string Review = "Review";
            public const string PS = "PSignature";
            public const string PendingSignature = "Pending Signature";


            /// <summary>
            /// Super admin has approved agreement
            /// </summary>
            public const string Approved = "Approved";
            public const string Rejected = "Rejected";


        }
        public static class EmailInfo
        {
            public static readonly string DefaultEmail = AppConfig.Config["EmailSender:Email"];
            public static readonly string DefaultPassword = AppConfig.Config["EmailSender:Password"];
            public static readonly string DefaultSmtpServer = AppConfig.Config["EmailSender:SmtpServer"];
            public static readonly string DefaultPort = AppConfig.Config["EmailSender:Port"];
            public static readonly string DefaultTitle = AppConfig.Config["EmailSender:Title"];

        }

        public class UserLevelIdEnum
        {
            public const string ORGANIZATION = "ORGANIZATION";
            public const string SUPERADMIN = "SUPERADMIN";
            public const string STUDENT = "STUDENT";
        }

        public class LogDescription
        {
            public const string Insert = "Insert data to {0}</br>{1}";
            public const string Delete = "Delete data of {0}</br>{1}";
            public const string Update = "Update data of {0}</br>{1}";
        }

        public class InstructorDeletionStatus
        {
            public const string Approved = "Approved";
            public const string Pending = "Pending";
        }


        public class AuditInfoEnum
        {
            public const string AcademicProject = "AcademicProject";
            public const string AcademicTargetProgram = "AcademicTargetProgram";
            public const string AdministrativeUsers_old = "AdministrativeUsers_old";
            public const string AgreementMaster = "AgreementMaster";
            public const string AgreementRemark = "AgreementRemark";
            public const string Applications = "Applications";
            public const string ApplicationsProducts = "ApplicationsProducts";
            public const string Attachments = "Attachments";
            public const string Authorizations = "Authorizations";
            public const string Certificate = "Certificate";
            public const string CertificateBackground = "CertificateBackground";
            public const string ContactCountry = "ContactCountry";
            public const string ContactIds = "ContactIds";
            public const string ContactNotes = "ContactNotes";
            public const string Contacts_All = "Contacts_All";
            public const string Contacts_old = "Contacts_old";
            public const string ContactServiceProviderRoles = "ContactServiceProviderRoles";
            public const string ContactsOrganizations = "ContactsOrganizations";
            public const string Courses = "Courses";
            public const string CourseSoftware = "CourseSoftware";
            public const string CourseStudent = "CourseStudent";
            public const string CourseTeachingLevel = "CourseTeachingLevel";
            public const string CourseTrainingFormat = "CourseTrainingFormat";
            public const string CourseTrainingMaterial = "CourseTrainingMaterial";
            public const string CurrencyConversion = "CurrencyConversion";
            public const string Dictionary = "Dictionary";
            public const string Duplicates = "Duplicates";
            public const string eCompanyCountryMap = "eCompanyCountryMap";
            public const string EmailBody = "EmailBody";
            public const string EmailBodyType = "EmailBodyType";
            public const string EvaluationAnswer = "EvaluationAnswer";
            public const string EvaluationQuestion = "EvaluationQuestion";
            public const string EvaluationReport = "EvaluationReport";
            public const string Family = "Family";
            public const string GeoCountry = "GeoCountry";
            public const string Glossary = "Glossary";
            public const string History = "History";
            public const string Industries = "Industries";
            public const string IndustryProducts = "IndustryProducts";
            public const string IndustryRoles = "IndustryRoles";
            public const string InstructorRequest = "InstructorRequest";
            public const string IntegrityOk = "IntegrityOk";
            public const string Invoice_type = "Invoice_type";
            public const string InvoiceDiscount = "InvoiceDiscount";
            public const string InvoicePricingSKU = "InvoicePricingSKU";
            public const string Invoices = "Invoices";
            public const string InvoiceSKU = "InvoiceSKU";
            public const string InvoicesSites = "InvoicesSites";
            public const string JournalActivities = "JournalActivities";
            public const string Journals = "Journals";
            public const string LanguagePack = "LanguagePack";
            public const string Layout_language = "Layout_language";
            public const string Layout_language_evaluation = "Layout_language_evaluation";
            public const string LineItemSummaries = "LineItemSummaries";
            public const string LMSCapacity = "LMSCapacity";
            public const string LMSCertificationActivities = "LMSCertificationActivities";
            public const string LMSCertificationHistory = "LMSCertificationHistory";
            public const string LMSCertificationHistory_FY11 = "LMSCertificationHistory_FY11";
            public const string LMSContactRegistrationCounts = "LMSContactRegistrationCounts";
            public const string LMSCourseAgeFeed = "LMSCourseAgeFeed";
            public const string LMSLanguageEquivalentCourses = "LMSLanguageEquivalentCourses";
            public const string LMSRegistrations = "LMSRegistrations";
            public const string LMSSpecializationHistory = "LMSSpecializationHistory";
            public const string LMSSpecializations = "LMSSpecializations";
            public const string LMSTranscripts = "LMSTranscripts";
            public const string LoginHistory = "LoginHistory";
            public const string LoginHistoryCSO = "LoginHistoryCSO";
            public const string LookUp = "LookUp";
            public const string Main_organization = "Main_organization";
            public const string Main_site = "Main_site";
            public const string Main_site_stations = "Main_site_stations";
            public const string MarketType = "MarketType";
            public const string ODS_ContactHistory = "ODS_ContactHistory";
            public const string OrgIds = "OrgIds";
            public const string OrgSiteAttachment = "OrgSiteAttachment";
            public const string Partners = "Partners";
            public const string ProductFamilies = "ProductFamilies";
            public const string Products = "Products";
            public const string ProductsOriginal = "ProductsOriginal";
            public const string ProductVersions = "ProductVersions";
            public const string Qualifications = "Qualifications";
            public const string Ref_countries = "Ref_countries";
            public const string Ref_geo = "Ref_geo";
            public const string Ref_language = "Ref_language";
            public const string Ref_region = "Ref_region";
            public const string Ref_subregion = "Ref_subregion";
            public const string Ref_territory = "Ref_territory";
            public const string RoleCertificate = "RoleCertificate";
            public const string RoleCertificateType = "RoleCertificateType";
            public const string RoleParams = "RoleParams";
            public const string Roles = "Roles";
            public const string Rooms = "Rooms";
            public const string Saba_Certifications = "Saba_Certifications";
            public const string Saba_Companies = "Saba_Companies";
            public const string Saba_CompanyProfiles = "Saba_CompanyProfiles";
            public const string Saba_ContactData = "Saba_ContactData";
            public const string Saba_ContactLogins = "Saba_ContactLogins";
            public const string Saba_Domains = "Saba_Domains";
            public const string Saba_Jobtypes = "Saba_Jobtypes";
            public const string SabaComponentIds = "SabaComponentIds";
            public const string SerialNumbers = "SerialNumbers";
            public const string ServiceProviderRoles = "ServiceProviderRoles";
            public const string SiebelContactCSNLinks = "SiebelContactCSNLinks";
            public const string SiebelContactData = "SiebelContactData";
            public const string SiebelContactEIDMLinks = "SiebelContactEIDMLinks";
            public const string SiebelContacts = "SiebelContacts";
            public const string SiebelSiteContactLinks = "SiebelSiteContactLinks";
            public const string SiebelSiteContracts = "SiebelSiteContracts";
            public const string SiebelSites = "SiebelSites";
            public const string Site_service = "Site_service";
            public const string SiteContactLinks = "SiteContactLinks";
            public const string SiteCountryDistributor = "SiteCountryDistributor";
            public const string SiteCountryDistributorSKU = "SiteCountryDistributorSKU";
            public const string SiteIds = "SiteIds";
            public const string SiteRoleParams = "SiteRoleParams";
            public const string SiteRoles = "SiteRoles";
            public const string SiteServices = "SiteServices";
            public const string States = "States";
            public const string tblPostSurveyMatchQuestions = "tblPostSurveyMatchQuestions";
            public const string tblStudents = "tblStudents";
            public const string tblTranslatedLanguages = "tblTranslatedLanguages";
            public const string TerritoryCountry = "TerritoryCountry";
            public const string TimezoneDetail = "TimezoneDetail";
            public const string Timezones = "Timezones";
            public const string tmpDashboardAAP1 = "tmpDashboardAAP1";
            public const string tmpDashboardAAP2 = "tmpDashboardAAP2";
            public const string tmpDashboardAAP3 = "tmpDashboardAAP3";
            public const string tmpDashboardATC1 = "tmpDashboardATC1";
            public const string tmpDashboardATC2 = "tmpDashboardATC2";
            public const string tmpDashboardATC3 = "tmpDashboardATC3";
            public const string tmpDashboardMeetAnnual = "tmpDashboardMeetAnnual";
            public const string tmpDashboardMonthCoursesDelivered = "tmpDashboardMonthCoursesDelivered";
            public const string tmpDashboardSitesParticipated1 = "tmpDashboardSitesParticipated1";
            public const string tmpDashboardSitesParticipated2 = "tmpDashboardSitesParticipated2";
            public const string tmpDashboardTopInstructors = "tmpDashboardTopInstructors";
            public const string tmpDashboardTotalActiveInstructors = "tmpDashboardTotalActiveInstructors";
            public const string tmpDashboardTotalActivePartner = "tmpDashboardTotalActivePartner";
            public const string tmpDashboardTotalCourses = "tmpDashboardTotalCourses";
            public const string tmpDashboardTotalNewPartner = "tmpDashboardTotalNewPartner";
            public const string tmpDashboardTotalPerformanceSite = "tmpDashboardTotalPerformanceSite";
            public const string tmpDashboardTotalSurveyTaken = "tmpDashboardTotalSurveyTaken";
            public const string tmpEvaSurveyTaken = "tmpEvaSurveyTaken";
            public const string tmpSitesCOE = "tmpSitesCOE";
            public const string TypeOfTrainings = "TypeOfTrainings";
            public const string UserLevel = "UserLevel";
            public const string UserToken = "UserToken";
            public const string versioncontrol = "versioncontrol";

        }

        public static class DataManagement
        {
         public const string DataManagementKey = "DataMagement";
        }
    }
}
