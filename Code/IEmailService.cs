﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace autodesk.Code
{
    public interface IEmailService
    {
        bool SendMail(string email, string subject, string message, string cc = null, List<SendGrid.Helpers.Mail.Attachment> attachments = null);
        Task<bool> SendMailAsync(string email, string subject, string message, string cc = null, List<SendGrid.Helpers.Mail.Attachment> attachments = null);
    }
}
