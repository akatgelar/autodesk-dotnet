﻿using autodesk.Code.Models.Dto;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace autodesk.Code
{
    public interface IAgreementService
    {

        AgreementMasterListDto GetAgreementMasters(AgreementSearchDto searchInput);
        Task<List<AgreementRemarkDto>> GetAgreementRemarks(int agreementMasterId);
        Task<List<FYDropdownListDto>> GetListFY();
        Task<List<Tuple<int, string>>> GetListPartnerType();
        Task<List<TerritoryDropdownListDto>> GetListTerritory();
        Task<List<CountryDropdownListDto>> GetListCountry(int territoryId);
        Task<List<OrgDropdownListDto>> GetListOrg(int territoryId, string countryCode);
        Task<int> ExecuteAgreement(AgreementAddDto agreementAddDto);
        Task<OrganizationInfoDto> GetOrganizationInfo(int agreementMasterId);
        Task<AgreementDetailDto> GetAgreementDetail(int agreementMasterId);
        Task<bool> RejectAgreement(AgreementRejectDto agreementRejectDto);
        Task<bool> ApproveAgreement(AgreementApproveDto agreementApproveDto);
        Task GenerateAgreementMaster(string FY = null);

        Task<string> GetOrgName(string orgId, string userId);

    }
}
