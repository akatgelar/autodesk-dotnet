﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.TokenGenerator
{
    public interface ITokenGenerator
    {
         string GetEncryptedToken(string UserLevel,decimal AppVersion,int UserId);
         string GetDecryptedTokenValue(string token);
    }
}
