﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace autodesk.Code.TokenGenerator
{
    public class TokenGenerator:ITokenGenerator
    {
        private readonly string saultValue="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrestuvwxyz";
        public string GetEncryptedToken(string UserLevel,decimal AppVersion,int UserId)
        {
            string token = "";
            string secondObject = "";
            string thirdObject = "";
            Random random = new Random();
            var dateTime = DateTime.Now;
            var dateTimeOffset = new DateTimeOffset(dateTime);
            var t = dateTimeOffset.ToUnixTimeSeconds();
            var firstObject = dateTimeOffset.ToUnixTimeSeconds() * DateTime.Now.Month;
            var byteArr = GetBytes(UserLevel);
            foreach (var val in byteArr)
            {
                int rd = random.Next(0, saultValue.Length);
                var sault = saultValue[rd];
                secondObject = secondObject + val + sault;
            }

            for (int i = 1; i < 5; i++)
            {
                var no = random.Next(0, 6);
                no += i;
                thirdObject = thirdObject +saultValue[random.Next(0, no)];
            }

            string firstValue = firstObject.ToString()+saultValue[random.Next(0, 37)] + UserId;
            thirdObject += AppVersion;
            token = firstValue + "-" + secondObject + "-" + thirdObject;
            return token;

        }

        public string GetDecryptedTokenValue(string token)
        {
            var decriptedToken = String.Empty;
            if (!string.IsNullOrEmpty(token))
            {
                List<string> splitedValue = token.Split("-").ToList();
                var firstArr= Regex.Split(splitedValue.First().ToString(), @"[a-zA-Z]").ToList().Where(x => x != "")
                    .Select(x => Convert.ToInt64(x)).ToArray();
                var timeSpam = Convert.ToInt64(firstArr.AsEnumerable().First()) / DateTime.Now.Month;
                var firstObject = DateTimeOffset.FromUnixTimeSeconds(timeSpam).DateTime.ToLocalTime();
                var byteArr = Regex.Split(splitedValue[1].ToString(), @"[a-zA-Z]").ToList().Where(x => x != "")
                    .Select(x => Convert.ToByte(x)).ToArray();
                var secondObject = GetString(byteArr);
                var intArr= Regex.Split(splitedValue.Last(), @"[a-zA-Z]").ToList().Where(x => x != "")
                    .Select(x => Convert.ToDecimal(x)).ToArray();
                var thirdObject = intArr.AsEnumerable().LastOrDefault();
                decriptedToken = firstObject.ToString("dd-MM-yyyy") + "," + secondObject + "," + thirdObject;
;

            }

            return decriptedToken;
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
    }
}
