﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code
{
    public interface IEmailSender
    {
        // Task SendEmailAsync();
        Task<Boolean> SendEmailAsync(String email, String subject, String message);
    }
}
