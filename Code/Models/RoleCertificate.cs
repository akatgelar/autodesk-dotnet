﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class RoleCertificate
    {
        public long RoleCertificateId { get; set; }
        public string RoleCode { get; set; }
        public string CertificateName { get; set; }
        public string CertificateType { get; set; }
        public string Status { get; set; }
    }
}
