﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class ContactsOld
    {
        public string ContactId { get; set; }
        public string SiebelId { get; set; }
        public string SiebelStatus { get; set; }
        public string PrimarySiteId { get; set; }
        public string ContactName { get; set; }
        public string EnglishContactName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string TitlePosition { get; set; }
        public string EmailAddress { get; set; }
        public string EmailAddress2 { get; set; }
        public string EmailAddress3 { get; set; }
        public string EmailAddress4 { get; set; }
        public string DoNotEmail { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PwdchangeRequired { get; set; }
        public string PermissionsGroup { get; set; }
        public DateTime? HireDate { get; set; }
        public string HowLongWithOrg { get; set; }
        public string HowLongInIndustry { get; set; }
        public string HowLongAutodeskProducts { get; set; }
        public string ExperiencedCompetitiveProducts { get; set; }
        public string ExperiencedCompetitiveProductsList { get; set; }
        public DateTime QuestionnaireDate { get; set; }
        public string PercentPreSales { get; set; }
        public string PercentPostSales { get; set; }
        public string Dedicated { get; set; }
        public string Department { get; set; }
        public string InternalSales { get; set; }
        public int PrimaryRoleId { get; set; }
        public string PrimaryIndustryId { get; set; }
        public string PrimarySubIndustryId { get; set; }
        public string PrimaryRoleExperience { get; set; }
        public string Atcrole { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Mobile1 { get; set; }
        public string Mobile2 { get; set; }
        public string PrimaryLanguage { get; set; }
        public string SecondaryLanguage { get; set; }
        public string Timezone { get; set; }
        public float TimeZoneUtc { get; set; }
        public string Comments { get; set; }
        public string InstructorId { get; set; }
        public short? LoginCount { get; set; }
        public DateTime? LastLogin { get; set; }
        public string LastIpaddress { get; set; }
        public string Status { get; set; }
        public string CsouserUuid { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string Action { get; set; }
        public DateTime? CsoupdatedOn { get; set; }
        public string Csoversion { get; set; }
    }
}
