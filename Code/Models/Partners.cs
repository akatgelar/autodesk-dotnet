﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Partners
    {
        public string PartnerCode { get; set; }
        public short WantAuthnRequestSigned { get; set; }
        public short SignSamlresponse { get; set; }
        public short EncryptAssertion { get; set; }
        public string AssertionConsumerServiceUrl { get; set; }
        public string SingleLogoutServiceUrl { get; set; }
        public string CertificateFilePath { get; set; }
        public string Url { get; set; }
        public string Name { get; set; }
    }
}
