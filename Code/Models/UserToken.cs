﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class UserToken
    {
        public int Id { get; set; }
        public string Token { get; set; }
        public int UserId { get; set; }
        public DateTime? Datetime { get; set; }
    }
}
