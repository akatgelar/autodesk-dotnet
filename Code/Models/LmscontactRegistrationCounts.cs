﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class LmscontactRegistrationCounts
    {
        public string ContactId { get; set; }
        public string Progress { get; set; }
        public long CourseCount { get; set; }
    }
}
