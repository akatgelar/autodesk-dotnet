﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class LoginHistory
    {
        public long DbId { get; set; }
        public string ContactId { get; set; }
        public string Action { get; set; }
        public string Application { get; set; }
        public DateTime Date { get; set; }
    }
}
