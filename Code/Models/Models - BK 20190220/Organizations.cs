﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class MainOrganization
    {
        public int OrganizationId { get; set; }
        public string OrgId { get; set; }
        public string AtcorgId { get; set; }
        public string OrgStatusRetired { get; set; }
        public string OrgName { get; set; }
        public string EnglishOrgName { get; set; }
        public string CommercialOrgName { get; set; }
        public string YearJoined { get; set; }
        public string TaxExempt { get; set; }
        public string Vatnumber { get; set; }
        public string SapsoldTo { get; set; }
        public string Varcsn { get; set; }
        public string Atccsn { get; set; }
        public string Aupcsn { get; set; }
        public string OrgWebAddress { get; set; }
        public string RegisteredDepartment { get; set; }
        public string RegisteredAddress1 { get; set; }
        public string RegisteredAddress2 { get; set; }
        public string RegisteredAddress3 { get; set; }
        public string RegisteredCity { get; set; }
        public string RegisteredStateProvince { get; set; }
        public string RegisteredPostalCode { get; set; }
        public string RegisteredCountryCode { get; set; }
        public string InvoicingDepartment { get; set; }
        public string InvoicingAddress1 { get; set; }
        public string InvoicingAddress2 { get; set; }
        public string InvoicingAddress3 { get; set; }
        public string InvoicingCity { get; set; }
        public string InvoicingStateProvince { get; set; }
        public string InvoicingPostalCode { get; set; }
        public string InvoicingCountryCode { get; set; }
        public string ContractDepartment { get; set; }
        public string ContractAddress1 { get; set; }
        public string ContractAddress2 { get; set; }
        public string ContractAddress3 { get; set; }
        public string ContractCity { get; set; }
        public string ContractStateProvince { get; set; }
        public string ContractPostalCode { get; set; }
        public string ContractCountryCode { get; set; }
        public string AtcdirectorFirstName { get; set; }
        public string AtcdirectorLastName { get; set; }
        public string AtcdirectorEmailAddress { get; set; }
        public string AtcdirectorTelephone { get; set; }
        public string AtcdirectorFax { get; set; }
        public string LegalContactFirstName { get; set; }
        public string LegalContactLastName { get; set; }
        public string LegalContactEmailAddress { get; set; }
        public string LegalContactTelephone { get; set; }
        public string LegalContactFax { get; set; }
        public string BillingContactFirstName { get; set; }
        public string BillingContactLastName { get; set; }
        public string BillingContactEmailAddress { get; set; }
        public string BillingContactTelephone { get; set; }
        public string BillingContactFax { get; set; }
        public string AdminNotes { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string CsoresellerUuid { get; set; }
        public string CsoresellerName { get; set; }
        public string CsopartnerManager { get; set; }
        public DateTime? CsoupdatedOn { get; set; }
        public string Csoversion { get; set; }
        public string PrimarySiteId { get; set; }
    }
}
