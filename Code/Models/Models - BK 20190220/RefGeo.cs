﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class RefGeo
    {
        public int GeoId { get; set; }
        public string GeoCode { get; set; }
        public string GeoName { get; set; }
        public string Cuid { get; set; }
        public DateTime? Cdate { get; set; }
        public string Muid { get; set; }
        public DateTime? Mdate { get; set; }
    }
}
