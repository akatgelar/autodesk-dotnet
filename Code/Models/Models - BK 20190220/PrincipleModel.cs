﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public class PrincipleModel
    {
        public string Geo { get; set; }
        public string Territory { get; set; }
        public string Country { get; set; }
        public string Org { get; set; }
        public string Site { get; set; }
        public Reports Reports { get; set; }
        }

    public class Epdb
    {
        public string OrgReport { get; set; }
        public string SiteReport { get; set; }
        public string ContactReport { get; set; }
        public string OrgJournalEntryReport { get; set; }
        public string SiteJournalEntryReport { get; set; }
        public string InvoiceReport { get; set; }
        public string QualificationContactReport { get; set; }
        public string SiteAttributeReport { get; set; }
        public string ContactAttributeReport { get; set; }
        public string ATCAAPAccReport { get; set; }
        public string DBIntReport { get; set; }
        public string HistoryReport { get; set; }
    }

    public class Eva
    {
        public string OrgReport { get; set; }
        public string PerformanceOverviewReport { get; set; }
        public string InstructorPFMReport { get; set; }
        public string EvalResponseReport { get; set; }
        public string StudentSearchReport { get; set; }
        public string DownloadSiteEval { get; set; }
        public string ChannelPFMReport { get; set; }
        public string ProductTrained { get; set; }
        public string ProductTrainedtatistics { get; set; }
        public string SubmissionLanguage { get; set; }
        public string Audit { get; set; }
    }

    public class Reports
    {
        public Epdb epdb { get; set; }
        public Eva eva { get; set; }
    }
}
