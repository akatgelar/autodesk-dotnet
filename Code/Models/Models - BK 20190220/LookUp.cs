﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public class LookUp
    {
        public string ControlName { get; set; }
        public int DataValue { get; set; }
        public string Description { get; set; }
    }
}
