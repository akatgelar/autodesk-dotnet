﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class JournalActivities
    {
        public int ActivityId { get; set; }
        public DateTime DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string Status { get; set; }
        public string ActivityName { get; set; }
        public string ActivityType { get; set; }
        public string IsUnique { get; set; }
        public string Description { get; set; }
    }
}
