﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public class InstructorRequest
    {
        public int Id { get; set; }
        public string ContactId { get; set; }
        public string RequesterId { get; set; }
        public string DistributorId { get; set; }
       // public string SiteId { get; set; }
        public string ReasonType { get; set; }
        public string RequestedSiteId { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
    }
}
