﻿using autodesk.Code.ViewModel;
using Microsoft.EntityFrameworkCore;

namespace autodesk.Code.Models
{
    public class MySQLContext : DbContext
    {
        public DbSet<OrgSiteAttachment> OrgSiteAttachment { get; set; }
        //public DbSet<LanguagePack> LanguagePack { get; set; }
        public DbSet<MasterSKU> MasterSKU { get; set; }
        public DbSet<InvoicePricingSku> InvoicePricingSKU { get; set; }
        public DbSet<InvoiceSku> InvoiceSku { get; set; }
        public DbSet<Courses> Courses { get; set; }
        public DbSet<Roles> Roles { get; set; }
        public DbSet<RefTerritory> RefTerritory { get; set; }
        public DbSet<RefCountries> RefCountries { get; set; }
        public DbSet<Invoices> Invoices { get; set; }
        public DbSet<MainSite> MainSite { get; set; }
        public DbSet<InvoiceDiscount> InvoiceDiscount { get; set; }
        public DbSet<MainOrganization> MainOrganization { get; set; }
        public virtual DbSet<EmailBody> EmailBody { get; set; }
        public virtual DbSet<EmailBodyType> EmailBodyType { get; set; }
        public virtual DbSet<ContactsAll> ContactsAll { get; set; }
        public virtual DbSet<Dictionary> Dictionary { get; set; }
        public virtual DbSet<SiteRoles> SiteRoles { get; set; }
        public virtual DbSet<RefGeo> RefGeo { get; set; }
        public virtual DbSet<RefRegion> RefRegion { get; set; }
        public virtual DbSet<RefSubregion> RefSubregion { get; set; }
        public virtual DbSet<Journals> Journals { get; set; }
        public virtual DbSet<JournalActivities> JournalActivities { get; set; }
        public virtual DbSet<LookUp> LookUp { get; set; }
        public virtual DbSet<MarketType> MarketType { get; set; }
        public virtual DbSet<SiteContactLinks> SiteContactLinks { get; set; }
        public virtual DbSet<AgreementMaster> AgreementMaster { get; set; }
        public virtual DbSet<AgreementRemark> AgreementRemark { get; set; }
        public virtual  DbSet<InstructorRequest> InstructorRequest { get; set; }
        public virtual DbSet<LanguagePack> LanguagePack { get; set; }
       public virtual DbSet<VersionControlModel> VersionControl { get; set; }
        public MySQLContext()
        {

        }
        public MySQLContext(DbContextOptions<MySQLContext> options)
            : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {

                optionsBuilder.UseMySQL(AppConfig.Config["ConnectionStrings:MySQLConnection"]);
            }
        }
        //for server 42.61.88.131
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrgSiteAttachment>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.ToTable("OrgSiteAttachment");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnType("blob");

                entity.Property(e => e.OrgId)
                    .IsRequired()
                    .HasColumnName("OrgID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .IsRequired(false)
                    .HasColumnName("SiteID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);
                entity.Property(e => e.DocType)
                    .IsRequired(false)
                    .HasMaxLength(15)
                    .IsUnicode(false);
                entity.Property(e => e.Name)
                   .IsRequired(true)
                   .HasMaxLength(250)
                   .IsUnicode(false);
            });
            modelBuilder.Entity<LookUp>(entity =>
            {
                entity.HasKey(e => e.ControlName);
                entity.ToTable("LookUp");
                entity.Property(e => e.ControlName)
                    .HasColumnName("ControlName");
                entity.Property(e => e.DataValue)
                    .HasColumnName("DataValue")
                    .HasColumnType("int(11)");
                entity.Property(e => e.Description)
                    .HasColumnName("Description");
            });
            modelBuilder.Entity<VersionControlModel>(entity =>
            {
                entity.ToTable("VersionControl");
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Version).HasColumnType("DECIMAL(5,2)");
                entity.Property(e => e.DateTimes).HasColumnType("DATETIME");
            });


            modelBuilder.Entity<InstructorRequest>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.ToTable("InstructorRequest");
                entity.Property(e => e.Id).HasMaxLength(45).HasColumnName("Id").IsUnicode(false).IsRequired();
                entity.Property(e => e.ContactId).HasMaxLength(20).HasColumnName("ContactId").IsUnicode(false).IsRequired();
                entity.Property(e => e.RequesterId).HasMaxLength(20).HasColumnName("RequesterId").IsUnicode(false).IsRequired();
                entity.Property(e => e.DistributorId).HasMaxLength(20).HasColumnName("DistributorId").IsUnicode(false).IsRequired();
                // entity.Property(e => e.SiteId).HasMaxLength(15).HasColumnName("SiteId").IsUnicode(false).IsRequired();
                entity.Property(e => e.ReasonType).HasMaxLength(45).HasColumnName("ReasonType").IsUnicode(false).IsRequired();
                entity.Property(e => e.RequestedSiteId).HasMaxLength(255).HasColumnName("RequestedSiteId").IsUnicode(false);
                entity.Property(e => e.Reason).HasMaxLength(255).HasColumnName("Reason").IsUnicode(false).IsRequired();
                entity.Property(e => e.Status).HasMaxLength(25).HasColumnName("Status").IsUnicode(false).IsRequired();

            });
            modelBuilder.Entity<LanguagePack>(entity =>
            {
                entity.HasKey(e => e.LanguagePackId);
                entity.ToTable("LanguagePack");

                entity.Property(e => e.LanguageCountry)
                    .HasColumnName("LanguageCountry")
                    .IsUnicode(false)
                    ;

                entity.Property(e => e.LanguageJson)
                    .HasColumnName("LanguageJson")
                    ;
                entity.Property(e => e.Status)
                    .HasColumnName("Status")
                    ;

                entity.Property(e => e.CreatedBy)
                    .HasColumnName("CreatedBy")
                    ;

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("CreatedDate");


            });

            modelBuilder.Entity<MasterSKU>(entity =>
            {
                entity.HasKey(e => e.SiteCountryDistributorSKUId);
                entity.ToTable("SiteCountryDistributorSKU");

                entity.Property(e => e.SiteCountryDistributorSKUId)
                    .HasColumnName("SiteCountryDistributorSKUId")
                    .HasColumnType("int(8)");

                entity.Property(e => e.SiteId)
                    .IsRequired(false)
                    .HasColumnName("SiteID")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCode)
                    .IsRequired(false)
                    .IsUnicode(false);

                entity.Property(e => e.EdistributorType)
                    .HasColumnName("EDistributorType")
                    .IsRequired(false)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Skudescription)
                .HasColumnName("SKUDescription")
                   .IsRequired(false)
                   .HasMaxLength(255)
                   .IsUnicode(false);

                entity.Property(e => e.Price)
               .HasColumnName("Price")
                  .IsRequired(false)
                  .HasMaxLength(12)
                  .IsUnicode(false);

                entity.Property(e => e.License)
               .HasColumnName("License")
                  .IsRequired(false)
                  .HasMaxLength(100)
                  .IsUnicode(false);

                entity.Property(e => e.Currency)
               .HasColumnName("Currency")
                  .IsRequired(false)
                  .HasMaxLength(12)
                  .IsUnicode(false);

                entity.Property(e => e.Status)
               .HasColumnName("Status")
                  .IsRequired(true)
                  .HasMaxLength(1)
                  .IsUnicode(false);

                entity.Property(e => e.PartnerType)
               .HasColumnName("PartnerType")
                  .IsRequired(false)
                  .HasMaxLength(11)
                  .IsUnicode(false);

                entity.Property(e => e.FYIndicatorKey)
               .HasColumnName("FYIndicatorKey")
                  .IsRequired(false)
                  .HasMaxLength(50)
                  .IsUnicode(false);

                entity.Property(e => e.SKUName)
            .HasColumnName("SKUName")
               .IsRequired(false)
               .HasMaxLength(50)
               .IsUnicode(false);
                entity.Property(e => e.IsForPrimarySite)
          .HasColumnName("IsForPrimarySite")
          .HasColumnType("bit(1)")
             .IsRequired(false)
             .IsUnicode(false);
            });

            modelBuilder.Entity<InvoicePricingSku>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.ToTable("InvoicePricingSKU");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SiteCountryDistributorSkuid)
                    .IsRequired(true)
                    .HasColumnName("SiteCountryDistributorSKUId")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Territory)
                .HasColumnName("Territory")
                    .IsRequired(true)
                     .HasMaxLength(50)
                    .IsUnicode(false);


                entity.Property(e => e.Region)
                    .HasColumnName("Region")
                    .IsRequired(false)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                .HasColumnName("Country")
                   .IsRequired(false)
                   .HasMaxLength(50)
                   .IsUnicode(false);

                entity.Property(e => e.Price)
               .HasColumnName("Price")
                  .IsRequired(true)
                  .HasMaxLength(12)
                  .IsUnicode(false);

                entity.Property(e => e.Distributor)
               .HasColumnName("Distributor")
                  .IsRequired(true)
                  .HasMaxLength(150)
                  .IsUnicode(false);

                entity.Property(e => e.Status)
               .HasColumnName("Status")
                  .IsRequired(true)
                  .HasMaxLength(1)
                  .IsUnicode(false);

                entity.Property(e => e.Description)
              .HasColumnName("Description")
                 .IsRequired(false)
                 .HasMaxLength(500)
                 .IsUnicode(false);

                entity.Property(e => e.SiteId)
              .HasColumnName("SiteId")
                 .IsRequired(false)
                 .HasMaxLength(12)
                 .IsUnicode(false);

              
            });

            modelBuilder.Entity<InvoiceSku>(entity =>
            {
                entity.HasKey(e => e.InvoiceSkuid);
                entity.ToTable("InvoiceSKU");

                entity.Property(e => e.InvoiceSkuid)
                    .HasColumnName("InvoiceSKUId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SiteCountryDistributorSkuid)
                    .IsRequired(false)
                    .HasColumnName("SiteCountryDistributorSKUId")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.PricingSKUId)
                .HasColumnName("PricingSKUId")
                    .IsRequired(false)
                     .HasMaxLength(50)
                    .IsUnicode(false);


                entity.Property(e => e.Qty)
                    .HasColumnName("QTY")
                    .IsRequired(false)
                    .HasColumnType("int(10)")
                    .IsUnicode(false);

                entity.Property(e => e.TotalAmmount)
                .HasColumnName("TotalAmmount")
                   .IsRequired(false)
                   .HasColumnType("float")
                   .IsUnicode(false);

                entity.Property(e => e.InvoiceId)
               .HasColumnName("InvoiceId")
                  .IsRequired(false)
                  .HasMaxLength(50)
                  .IsUnicode(false);
                entity.Property(e => e.SiteId)
               .HasColumnName("SiteId")
                  .IsRequired(false)
                  .HasMaxLength(15)
                  .IsUnicode(false);

            });

            modelBuilder.Entity<Courses>(entity =>
            {
                entity.HasKey(e => new { e.CourseId, e.ApplicationId });

                entity.ToTable("Courses");

                entity.HasIndex(e => e.CompletionDate)
                    .HasName("IDX_CompletionDate");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_ContactId");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CourseId");

                entity.HasIndex(e => e.FyindicatorKey)
                    .HasName("IDX_FYIndicatorKey");

                entity.HasIndex(e => e.Name)
                    .HasName("IDX_CourseTitle");

                entity.HasIndex(e => e.PartnerType)
                    .HasName("IDX_PartnerType");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SiteId");

                entity.HasIndex(e => e.StartDate)
                    .HasName("IDX_StartDate");

                entity.Property(e => e.CourseId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ApplicationId)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Atccomputer)
                    .HasColumnName("ATCComputer")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Atcfacility)
                    .HasColumnName("ATCFacility")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CertificateId).HasColumnType("int(11)");

                entity.Property(e => e.CertificateNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.CourseCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EvaluationQuestionId).HasColumnType("int(11)");

                entity.Property(e => e.EventType2)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.EventTypeKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FyindicatorKey)
                    .HasColumnName("FYIndicatorKey")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoursTraining)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HoursTrainingOther)
                    .HasColumnType("tinyint(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Institution)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InstitutionLogo).HasColumnType("mediumblob");

                entity.Property(e => e.LocationName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LogoName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LogoType)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerType)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectTypeKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TrainingTypeKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("Roles");

                entity.HasIndex(e => e.RoleCode)
                    .HasName("IDX_Roles_RoleCode");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IDX_Roles_RoleId");

                entity.HasIndex(e => e.RoleName)
                    .HasName("IDX_Roles_RoleName");

                entity.Property(e => e.RoleId).HasColumnType("int(10)");

                entity.Property(e => e.CrbapprovedName)
                    .IsRequired()
                    .HasColumnName("CRBApprovedName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.FrameworkName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RoleCode)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RoleSubType)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RoleType)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RefTerritory>(entity =>
            {
                entity.HasKey(e => e.TerritoryId);

                entity.ToTable("Ref_territory");

                entity.HasIndex(e => e.TerritoryId)
                    .HasName("INDEX_territory_id");

                entity.HasIndex(e => e.TerritoryName)
                    .HasName("INDEX_territory_name");

                entity.Property(e => e.TerritoryId).HasColumnType("int(11)");

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryName)
                    .IsRequired()
                    .HasColumnName("Territory_Name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<RefCountries>(entity =>
            {
                entity.HasKey(e => new { e.CountriesId, e.CountriesCode });

                entity.ToTable("Ref_countries");

                entity.HasIndex(e => e.CountriesCode)
                    .HasName("IDX_Countries_Code");

                entity.HasIndex(e => e.CountriesName)
                    .HasName("IDX_Countries_Country");

                entity.HasIndex(e => e.GeoCode)
                    .HasName("IDX_Countries_Geo");

                entity.HasIndex(e => e.RegionCode)
                    .HasName("IDX_Countries_Region");

                entity.HasIndex(e => e.SubregionCode)
                    .HasName("IDX_Countries_Subregion");

                entity.Property(e => e.CountriesId)
                    .HasColumnName("countries_id")
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CountriesCode)
                    .HasColumnName("countries_code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.CountriesName)
                    .HasColumnName("countries_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CountriesTel)
                    .IsRequired()
                    .HasColumnName("countries_tel")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Embargoed)
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GeoCode)
                    .HasColumnName("geo_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GeoName)
                    .IsRequired()
                    .HasColumnName("geo_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MarketTypeId)
                    .HasColumnType("int(2)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MultiplierOverride).HasColumnType("int(255)");

                entity.Property(e => e.RegionCode)
                    .HasColumnName("region_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RegionName)
                    .IsRequired()
                    .HasColumnName("region_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SubregionCode)
                    .IsRequired()
                    .HasColumnName("subregion_code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SubregionName)
                    .IsRequired()
                    .HasColumnName("subregion_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryId).HasColumnType("int(11)");
            });
            modelBuilder.Entity<Invoices>(entity =>
            {
                entity.HasKey(e => e.InvoiceId);

                entity.ToTable("Invoices");

                entity.HasIndex(e => e.OrgId)
                    .HasName("IDX_INVOICES_OrgId");

                entity.Property(e => e.InvoiceId).HasColumnType("int(8)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AmountOutstanding).HasColumnType("decimal(20,2)");

                entity.Property(e => e.CcexpDate)
                    .HasColumnName("CCExpDate")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CcexpDate2)
                    .HasColumnName("CCExpDate2")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Ccname)
                    .HasColumnName("CCName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ccname2)
                    .HasColumnName("CCName2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Comments).IsUnicode(false);

                entity.Property(e => e.ContractEndDate).HasColumnType("date");

                entity.Property(e => e.ContractId)
                    .HasColumnName("ContractID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ContractStartDate).HasColumnType("date");

                entity.Property(e => e.ContractStatus)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.DistributorFee).HasColumnType("decimal(20,0)");

                entity.Property(e => e.DocuSign)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FinancialYear)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.InvoiceDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNumber)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicedCurrency)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.L1Description)
                    .HasColumnName("L1_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L1Quantity)
                    .HasColumnName("L1_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L1Summary)
                    .HasColumnName("L1_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L1TotalAmount)
                    .HasColumnName("L1_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L1UnitPrice)
                    .HasColumnName("L1_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L1Vatamount)
                    .HasColumnName("L1_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L1Vatrate)
                    .HasColumnName("L1_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L2Description)
                    .HasColumnName("L2_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L2Quantity)
                    .HasColumnName("L2_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L2Summary)
                    .HasColumnName("L2_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L2TotalAmount)
                    .HasColumnName("L2_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L2UnitPrice)
                    .HasColumnName("L2_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L2Vatamount)
                    .HasColumnName("L2_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L2Vatrate)
                    .HasColumnName("L2_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L3Description)
                    .HasColumnName("L3_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L3Quantity)
                    .HasColumnName("L3_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L3Summary)
                    .HasColumnName("L3_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L3TotalAmount)
                    .HasColumnName("L3_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L3UnitPrice)
                    .HasColumnName("L3_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L3Vatamount)
                    .HasColumnName("L3_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L3Vatrate)
                    .HasColumnName("L3_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L4Description)
                    .HasColumnName("L4_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L4Quantity)
                    .HasColumnName("L4_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L4Summary)
                    .HasColumnName("L4_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L4TotalAmount)
                    .HasColumnName("L4_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L4UnitPrice)
                    .HasColumnName("L4_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L4Vatamount)
                    .HasColumnName("L4_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L4Vatrate)
                    .HasColumnName("L4_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L5Description)
                    .HasColumnName("L5_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L5Quantity)
                    .HasColumnName("L5_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L5Summary)
                    .HasColumnName("L5_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L5TotalAmount)
                    .HasColumnName("L5_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L5UnitPrice)
                    .HasColumnName("L5_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L5Vatamount)
                    .HasColumnName("L5_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L5Vatrate)
                    .HasColumnName("L5_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L6Description)
                    .HasColumnName("L6_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L6Quantity)
                    .HasColumnName("L6_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L6Summary)
                    .HasColumnName("L6_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L6TotalAmount)
                    .HasColumnName("L6_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L6UnitPrice)
                    .HasColumnName("L6_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L6Vatamount)
                    .HasColumnName("L6_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L6Vatrate)
                    .HasColumnName("L6_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L7Description)
                    .HasColumnName("L7_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L7Quantity)
                    .HasColumnName("L7_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L7Summary)
                    .HasColumnName("L7_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L7TotalAmount)
                    .HasColumnName("L7_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L7UnitPrice)
                    .HasColumnName("L7_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L7Vatamount)
                    .HasColumnName("L7_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L7Vatrate)
                    .HasColumnName("L7_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L8Description)
                    .HasColumnName("L8_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L8Quantity)
                    .HasColumnName("L8_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L8Summary)
                    .HasColumnName("L8_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L8TotalAmount)
                    .HasColumnName("L8_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L8UnitPrice)
                    .HasColumnName("L8_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L8Vatamount)
                    .HasColumnName("L8_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L8Vatrate)
                    .HasColumnName("L8_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfLicenses).HasColumnType("int(10)");

                entity.Property(e => e.OldContractId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OrgId)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentAmount)
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.PaymentAmount2).HasColumnType("decimal(20,2)");

                entity.Property(e => e.PaymentCurrency)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentDate).HasColumnType("date");

                entity.Property(e => e.PaymentDate2).HasColumnType("date");

                entity.Property(e => e.PaymentMethod)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentMethod2)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentMonth).HasColumnType("int(2)");

                entity.Property(e => e.PaymentMonth2).HasColumnType("int(2)");

                entity.Property(e => e.PaymentReference)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentReference2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Poid)
                    .HasColumnName("POID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PoreceivedDate)
                    .HasColumnName("POReceivedDate")
                    .HasColumnType("date");

                entity.Property(e => e.SiteCountryDistributorSkuid)
                    .HasColumnName("SiteCountryDistributorSKUId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteIdList)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TotalInvoicedAmount)
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.TotalLineAmount).HasColumnType("decimal(20,2)");

                entity.Property(e => e.TotalPaymentAmount).HasColumnType("decimal(20,2)");

                entity.Property(e => e.TotalVatamount)
                    .HasColumnName("TotalVATAmount")
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("0.00");
            });
            modelBuilder.Entity<MainSite>(entity =>
            {
                entity.HasKey(e => new { e.SiteIdInt, e.SiteId });

                entity.ToTable("Main_site");

                entity.HasIndex(e => e.AtcsiteId)
                    .HasName("IDX_SITES_ATCID");

                entity.HasIndex(e => e.CommercialSiteName)
                    .HasName("IDX_Sites_CommName");

                entity.HasIndex(e => e.CsolocationUuid)
                    .HasName("IDX_SITES_LOCUUID");

                entity.HasIndex(e => e.EnglishSiteName)
                    .HasName("IDX_Sites_EngName");

                entity.HasIndex(e => e.OrgId)
                    .HasName("IDX_SITES_OrgId");

                entity.HasIndex(e => e.SiebelSiteName)
                    .HasName("SiebelSiteName");

                entity.HasIndex(e => e.SiteAddress1)
                    .HasName("IDX_Sites_SiteAddr1");

                entity.HasIndex(e => e.SiteCountryCode)
                    .HasName("IDX_SITES_CountryCode");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SiteId");

                entity.HasIndex(e => e.SiteName)
                    .HasName("IDX_SITES_SiteName");

                entity.HasIndex(e => e.SiteStateProvince)
                    .HasName("IDX_Site_State");

                entity.HasIndex(e => e.SiteStatusRetired)
                    .HasName("IDX_Sites_SiteStatus");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_SITES_Status");

                entity.Property(e => e.SiteIdInt)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AdminNotes).IsUnicode(false);

                entity.Property(e => e.AtcsiteId)
                    .HasColumnName("ATCSiteId")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CommercialSiteName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CsoauthorizationCodes)
                    .HasColumnName("CSOAuthorizationCodes")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CsolocationName)
                    .HasColumnName("CSOLocationName")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CsolocationNumber)
                    .HasColumnName("CSOLocationNumber")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.CsolocationUuid)
                    .HasColumnName("CSOLocationUUID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CsopartnerType)
                    .HasColumnName("CSOpartner_type")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

                entity.Property(e => e.Csoversion)
                    .HasColumnName("CSOVersion")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.EnglishSiteName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MagellanId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MailingAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MailingAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MailingAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MailingCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.MailingCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MailingDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.MailingPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MailingStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.OrgId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SapnumberRetired)
                    .HasColumnName("SAPNumber_retired")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.SapshipToRetired)
                    .HasColumnName("SAPShipTo_retired")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelSiteName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminFax)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminFirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminLastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SiteCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.SiteCountryCode).HasColumnType("char(2)");

                entity.Property(e => e.SiteDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.SiteEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteFax)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerFax)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerFirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerLastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SiteName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SitePostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SiteStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiteStatusRetired)
                    .HasColumnName("SiteStatus_retired")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SiteTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SiteWebAddress)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.Workstations)
                    .HasColumnType("int(8)")
                    .HasDefaultValueSql("10");
            });
            modelBuilder.Entity<InvoiceDiscount>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.ToTable("InvoiceDiscount");

                entity.Property(e => e.Id)
                    .HasColumnName("Id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AmountType)
                    .IsRequired(true)
                    .HasMaxLength(15)
                     .IsUnicode(false);

                entity.Property(e => e.Amount)
                    .IsRequired()
                      .HasColumnType("float")
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                   .IsRequired(true)
                   .HasMaxLength(15)
                   .IsUnicode(false);

                entity.Property(e => e.InvoiceId)
                   .IsRequired(true)
                   .HasColumnType("int(8)")
                   .IsUnicode(false);
            });
            modelBuilder.Entity<MainOrganization>(entity =>
            {
                entity.HasKey(e => new { e.OrganizationId, e.OrgId });

                entity.ToTable("Main_organization");

                entity.HasIndex(e => e.AtcorgId)
                    .HasName("IDX_ORGANIZATIONS_ATCID");

                entity.HasIndex(e => e.CommercialOrgName)
                    .HasName("IDX_Org_CommName");

                entity.HasIndex(e => e.CsoresellerUuid)
                    .HasName("IDX_ORGANIZATIONS_UUID");

                entity.HasIndex(e => e.EnglishOrgName)
                    .HasName("IDX_Org_EngName");

                entity.HasIndex(e => e.OrgId)
                    .HasName("OrgId");

                entity.HasIndex(e => e.OrgName)
                    .HasName("IDX_ORGANIZATIONS_OrgName");

                entity.HasIndex(e => e.OrgStatusRetired)
                    .HasName("IDX_ORG_OrgStatus");

                entity.HasIndex(e => e.RegisteredCountryCode)
                    .HasName("IDX_ORGANIZATIONS_CountryCode");

                entity.HasIndex(e => e.RegisteredStateProvince)
                    .HasName("IDX_Org_State");

                entity.HasIndex(e => e.SapsoldTo)
                    .HasName("IDX_ORGANIZATIONS_SAP");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_ORGANIZATIONS_Status");

                entity.Property(e => e.OrganizationId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.OrgId)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AdminNotes).IsUnicode(false);

                entity.Property(e => e.Atccsn)
                    .HasColumnName("ATCCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorEmailAddress)
                    .HasColumnName("ATCDirectorEmailAddress")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorFax)
                    .HasColumnName("ATCDirectorFax")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorFirstName)
                    .HasColumnName("ATCDirectorFirstName")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorLastName)
                    .HasColumnName("ATCDirectorLastName")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorTelephone)
                    .HasColumnName("ATCDirectorTelephone")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AtcorgId)
                    .HasColumnName("ATCOrgId")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Aupcsn)
                    .HasColumnName("AUPCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactFax)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactFirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactLastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CommercialOrgName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ContractAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContractAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContractAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ContractDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.ContractPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ContractStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CsopartnerManager)
                    .HasColumnName("CSOPartnerManager")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CsoresellerName)
                    .HasColumnName("CSOResellerName")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CsoresellerUuid)
                    .HasColumnName("CSOResellerUUID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

                entity.Property(e => e.Csoversion)
                    .HasColumnName("CSOVersion")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.EnglishOrgName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactFax)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactFirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactLastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OrgName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OrgStatusRetired)
                    .HasColumnName("OrgStatus_retired")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrgWebAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.PrimarySiteId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SapsoldTo)
                    .HasColumnName("SAPSoldTo")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TaxExempt).HasColumnType("char(1)");

                entity.Property(e => e.Varcsn)
                    .HasColumnName("VARCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Vatnumber)
                    .HasColumnName("VATNumber")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.YearJoined)
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<EmailBody>(entity =>
            {
                entity.HasKey(e => e.BodyId);

                entity.ToTable("EmailBody");

                entity.Property(e => e.BodyId)
                    .HasColumnName("body_id")
                    .HasColumnType("int(2)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BodyEmail)
                    .HasColumnName("body_email")
                    .IsUnicode(false);

                entity.Property(e => e.BodyType)
                    .HasColumnName("body_type")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Key)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmailBodyType>(entity =>
            {
                entity.ToTable("EmailBodyType");

                entity.Property(e => e.EmailBodyTypeId).HasColumnType("int(11)");

                entity.Property(e => e.EmailBodyTypeName)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<ContactsAll>(entity =>
            {
                entity.HasKey(e => e.ContactIdInt);

                entity.ToTable("Contacts_All");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_ContactId");

                entity.HasIndex(e => e.ContactName)
                    .HasName("IDX_CONTACTS_ContactName");

                entity.HasIndex(e => e.CsouserUuid)
                    .HasName("IDX_CONTACT_UUID");

                entity.HasIndex(e => e.EmailAddress)
                    .HasName("IDX_CONTACT_EMAIL");

                entity.HasIndex(e => e.EmailAddress2)
                    .HasName("IDX_Contacts_EMAIL2");

                entity.HasIndex(e => e.EnglishContactName)
                    .HasName("IDX_Contacts_EngName");

                entity.HasIndex(e => e.FirstName)
                    .HasName("IDX_CONTACTS_FirstName");

                entity.HasIndex(e => e.InstructorId)
                    .HasName("IDX_Contacts_InstructorId");

                entity.HasIndex(e => e.InternalSales)
                    .HasName("InternalSales");

                entity.HasIndex(e => e.LastName)
                    .HasName("IDX_CONTACTS_LastName");

                entity.HasIndex(e => e.PrimarySiteId)
                    .HasName("IDX_Contacts_PrimarySiteId");

                entity.HasIndex(e => e.SiebelId)
                    .HasName("SiebelId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_CONTACTS_Status");

                entity.HasIndex(e => e.Username)
                    .HasName("IDX_CONTACTS_UNIQUE_USERNAME");

                entity.Property(e => e.ContactIdInt).HasColumnType("int(11)");

                entity.Property(e => e.AcimemberId)
                    .HasColumnName("ACIMemberId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Action)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Address3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AdminSystems)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Administrator)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("N");

                entity.Property(e => e.Atcrole)
                    .HasColumnName("ATCRole")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("N");

                entity.Property(e => e.Bio)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Comments).IsUnicode(false);

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactName)
                    .HasMaxLength(121)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

                entity.Property(e => e.CsouserUuid)
                    .HasColumnName("CSOUserUUID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Csoversion)
                    .HasColumnName("CSOVersion")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Dedicated).HasColumnType("char(1)");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DoNotEmail).HasColumnType("char(1)");

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress2)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress3)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress4)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EnglishContactName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExperiencedCompetitiveProducts).HasColumnType("char(1)");

                entity.Property(e => e.ExperiencedCompetitiveProductsList)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongAutodeskProducts)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongInIndustry)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongWithOrg)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.InstructorId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InternalSales)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastIpaddress)
                    .HasColumnName("LastIPAddress")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Level).HasColumnType("char(1)");

                entity.Property(e => e.LoginCount).HasColumnType("smallint(5)");

                entity.Property(e => e.Mobile1)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Mobile2)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.MobileCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordPlain)
                    .HasColumnName("password_plain")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PercentPostSales)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PercentPreSales)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PermissionsGroup)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryIndustryId)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryLanguage).HasColumnType("char(10)");

                entity.Property(e => e.PrimaryRoleExperience)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryRoleId).HasColumnType("int(10)");

                entity.Property(e => e.PrimarySiteId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrimarySubIndustryId)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ProfilePicture)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PwdchangeRequired)
                    .HasColumnName("PWDChangeRequired")
                    .HasColumnType("char(1)");

                entity.Property(e => e.QuestionnaireDate).HasColumnType("date");

                entity.Property(e => e.ResetPasswordCode).IsUnicode(false);

                entity.Property(e => e.ResetToken)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Salutation)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SecondaryLanguage).HasColumnType("char(10)");

                entity.Property(e => e.ShareEmail)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ShareMobile)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ShareTelephone)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ShowDuplicates)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("N");

                entity.Property(e => e.ShowInSearch)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.SiebelId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelStatus)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StateProvince)
                    .HasMaxLength(72)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.StatusEmailSend)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Telephone1)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone2)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.TelephoneCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TimeZoneUtc).HasColumnName("TimeZoneUTC");

                entity.Property(e => e.Timezone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TitlePosition)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.UserLevelId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.WebsiteUrl)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<Dictionary>(entity =>
            {
                entity.HasKey(e => new { e.Key, e.Parent });

                entity.ToTable("Dictionary");

                entity.Property(e => e.Key)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Parent)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.KeyValue)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");

            });
            modelBuilder.Entity<SiteRoles>(entity =>
            {
                entity.HasKey(e => e.SiteRoleId);

                entity.ToTable("SiteRoles");

                entity.HasIndex(e => e.Csn)
                    .HasName("IDX_SR_CSN");

                entity.HasIndex(e => e.PartyId)
                    .HasName("IDX_SR_PartyID");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IDX_SiteRoles_RoleId");

                entity.HasIndex(e => e.RoleParamId)
                    .HasName("IDX_RoleParamId");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SR_SiteId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_SR_Status");

                entity.Property(e => e.SiteRoleId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCsn)
                    .IsRequired()
                    .HasColumnName("ContractCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Csn)
                    .IsRequired()
                    .HasColumnName("CSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateDeleted).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExternalId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItsId)
                    .HasColumnName("ITS_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NewParentCsn)
                    .HasColumnName("NewParent_CSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ParentCsn)
                    .IsRequired()
                    .HasColumnName("ParentCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PartyId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnType("int(10)");

                entity.Property(e => e.RoleParamId).HasColumnType("int(10)");

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.UparentCsn)
                    .HasColumnName("UParent_CSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<MarketType>(entity =>
            {
                entity.ToTable("MarketType");

                entity.Property(e => e.MarketTypeId).HasColumnType("int(11)");

                entity.Property(e => e.MarketType1)
                    .HasColumnName("MarketType")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<RefGeo>(entity =>
            {
                entity.HasKey(e => e.GeoId);

                entity.ToTable("Ref_geo");

                entity.HasIndex(e => e.GeoCode)
                    .HasName("IDX_GeoCode");

                entity.Property(e => e.GeoId)
                    .HasColumnName("geo_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GeoCode)
                    .IsRequired()
                    .HasColumnName("geo_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GeoName)
                    .IsRequired()
                    .HasColumnName("geo_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<RefRegion>(entity =>
            {
                entity.HasKey(e => e.RegionId);

                entity.ToTable("Ref_region");

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GeoCode)
                    .HasColumnName("geo_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegionCode)
                    .HasColumnName("region_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RegionName)
                    .HasColumnName("region_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RefSubregion>(entity =>
            {
                entity.HasKey(e => e.SubregionId);

                entity.ToTable("Ref_subregion");

                entity.Property(e => e.SubregionId)
                    .HasColumnName("subregion_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GeoCode)
                    .HasColumnName("geo_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GeoName)
                    .IsRequired()
                    .HasColumnName("geo_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegionCode)
                    .HasColumnName("region_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RegionName)
                    .IsRequired()
                    .HasColumnName("region_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SubregionCode)
                    .IsRequired()
                    .HasColumnName("subregion_code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SubregionName)
                    .IsRequired()
                    .HasColumnName("subregion_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });
            modelBuilder.Entity<Journals>(entity =>
            {
                entity.HasKey(e => e.JournalId);

                entity.ToTable("Journals");

                entity.HasIndex(e => e.ActivityId)
                    .HasName("IDX_JOURNALS_ActivityId");

                entity.HasIndex(e => e.Guid)
                    .HasName("IDX_JOURNALS_GUID");

                entity.HasIndex(e => e.Notes)
                    .HasName("IDX_JOURNALS_Notes");

                entity.HasIndex(e => e.ParentId)
                    .HasName("IDX_JOURNALS_ParentId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_JOURNALS_Status");

                entity.Property(e => e.JournalId).HasColumnType("int(8)");

                entity.Property(e => e.ActivityId)
                    .HasColumnType("int(8)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes).HasColumnType("text");

                entity.Property(e => e.ParentId)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Status)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("A");
            });
            modelBuilder.Entity<JournalActivities>(entity =>
            {
                entity.HasKey(e => e.ActivityId);

                entity.ToTable("JournalActivities");

                entity.HasIndex(e => e.ActivityType)
                    .HasName("IDX_JOURNALACTIVITIES_ActivityType");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_JOURNALACTIVITIES_Status");

                entity.Property(e => e.ActivityId).HasColumnType("int(8)");

                entity.Property(e => e.ActivityName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ActivityType)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IsUnique).HasColumnType("char(1)");

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });
            modelBuilder.Entity<SiteContactLinks>(entity =>
            {
                entity.HasKey(e => e.SiteContactLinkId);

                entity.ToTable("SiteContactLinks");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_SCL_ContactId");

                entity.HasIndex(e => e.DateLastAdmin)
                    .HasName("IDX_SCL_DateLA");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SiteId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_SCL_Status");

                entity.HasIndex(e => new { e.SiteId, e.ContactId })
                    .HasName("IDX_MAKE_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.SiteContactLinkId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateDeleted).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");
            });
            modelBuilder.Entity<AgreementMaster>(entity =>
            {
                entity.ToTable("AgreementMaster");
                entity.HasKey(e => e.AgreementMasterId);
                entity.Property(e => e.AgreementMasterId)
                    .HasColumnName("AgreementMasterID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CheckTc)
                    .HasColumnName("CheckTC")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.FyindicatorKey)
                    .HasColumnName("FYIndicatorKey")
                    .HasColumnType("varchar(11)");

                entity.Property(e => e.LegalJobTitle)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.LegalName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OrgId)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerType)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SubmissionPerson)
                   .IsRequired(true)
                   .HasMaxLength(45)
                   .IsUnicode(false);
                entity.Property(e => e.FirstName)
                .HasColumnName("FirstName")
                    .IsRequired(true)
                    .HasMaxLength(45)
                    .IsUnicode(false);
                entity.Property(e => e.LastName)
                .HasColumnName("LastName")
                    .IsRequired(true)
                    .HasMaxLength(45)
                    .IsUnicode(false);

            });

            modelBuilder.Entity<AgreementRemark>(entity =>
            {
                entity.ToTable("AgreementRemark");
                entity.HasKey(e => e.AgreementRemarkId);
                entity.Property(e => e.AgreementRemarkId)
                    .HasColumnType("int(11)")
                    ;

                entity.Property(e => e.AgreementMasterId).HasColumnType("int(11)");

                entity.Property(e => e.RecordBy)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Remark)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.Action)
                .HasMaxLength(45)
                .IsUnicode(false);
            });
        }


        //for server autodesk.ligarian.com

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<OrgSiteAttachment>(entity =>
        //    {
        //        entity.HasKey(e => e.Id);
        //        entity.ToTable("OrgSiteAttachment", "autodesk_demo");

        //        entity.Property(e => e.Id)
        //            .HasColumnName("ID")
        //            .HasColumnType("int(11)");

        //        entity.Property(e => e.Content)
        //            .IsRequired()
        //            .HasColumnType("blob");

        //        entity.Property(e => e.OrgId)
        //            .IsRequired()
        //            .HasColumnName("OrgID")
        //            .HasMaxLength(15)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteId)
        //            .IsRequired(false)
        //            .HasColumnName("SiteID")
        //            .HasMaxLength(15)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Type)
        //            .IsRequired()
        //            .HasMaxLength(15)
        //            .IsUnicode(false);
        //        entity.Property(e => e.DocType)
        //            .IsRequired(false)
        //            .HasMaxLength(15)
        //            .IsUnicode(false);
        //        entity.Property(e => e.Name)
        //           .IsRequired(true)
        //           .HasMaxLength(250)
        //           .IsUnicode(false);
        //    });

        //    modelBuilder.Entity<MasterSKU>(entity =>
        //    {
        //        entity.HasKey(e => e.SiteCountryDistributorSKUId);
        //        entity.ToTable("SitecountryDistributorSKU", "autodesk_demo");

        //        entity.Property(e => e.SiteCountryDistributorSKUId)
        //            .HasColumnName("SiteCountryDistributorSKUId")
        //            .HasColumnType("int(8)");

        //        entity.Property(e => e.SiteID)
        //            .IsRequired(false)
        //            .HasColumnName("SiteID")
        //            .HasMaxLength(12)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CountryCode)
        //            .IsRequired(false)
        //            .IsUnicode(false);

        //        entity.Property(e => e.EDistributorType)
        //            .HasColumnName("EDistributorType")
        //            .IsRequired(false)
        //            .HasMaxLength(3)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SKUDescription)
        //        .HasColumnName("SKUDescription")
        //           .IsRequired(false)
        //           .HasMaxLength(255)
        //           .IsUnicode(false);

        //        entity.Property(e => e.Price)
        //       .HasColumnName("Price")
        //          .IsRequired(false)
        //          .HasMaxLength(12)
        //          .IsUnicode(false);

        //        entity.Property(e => e.License)
        //       .HasColumnName("License")
        //          .IsRequired(false)
        //          .HasMaxLength(100)
        //          .IsUnicode(false);

        //        entity.Property(e => e.Currency)
        //       .HasColumnName("Currency")
        //          .IsRequired(false)
        //          .HasMaxLength(12)
        //          .IsUnicode(false);

        //        entity.Property(e => e.Status)
        //       .HasColumnName("Status")
        //          .IsRequired(true)
        //          .HasMaxLength(1)
        //          .IsUnicode(false);

        //        entity.Property(e => e.PartnerType)
        //       .HasColumnName("PartnerType")
        //          .IsRequired(false)
        //          .HasMaxLength(11)
        //          .IsUnicode(false);

        //        entity.Property(e => e.FYIndicatorKey)
        //       .HasColumnName("FYIndicatorKey")
        //          .IsRequired(false)
        //          .HasMaxLength(50)
        //          .IsUnicode(false);

        //        entity.Property(e => e.SKUName)
        //    .HasColumnName("SKUName")
        //       .IsRequired(false)
        //       .HasMaxLength(50)
        //       .IsUnicode(false);

        //    });

        //    modelBuilder.Entity<InvoicePricingSKU>(entity =>
        //    {
        //        entity.HasKey(e => e.Id);
        //        entity.ToTable("InvoicePricingSKU", "autodesk_demo");

        //        entity.Property(e => e.Id)
        //            .HasColumnName("Id")
        //            .HasColumnType("int(11)");

        //        entity.Property(e => e.SiteCountryDistributorSKUId)
        //            .IsRequired(true)
        //            .HasColumnName("SiteCountryDistributorSKUId")
        //            .HasMaxLength(12)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Territory)
        //        .HasColumnName("Territory")
        //            .IsRequired(true)
        //             .HasMaxLength(50)
        //            .IsUnicode(false);


        //        entity.Property(e => e.Region)
        //            .HasColumnName("Region")
        //            .IsRequired(false)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Country)
        //        .HasColumnName("Country")
        //           .IsRequired(false)
        //           .HasMaxLength(50)
        //           .IsUnicode(false);

        //        entity.Property(e => e.Price)
        //       .HasColumnName("Price")
        //          .IsRequired(true)
        //          .HasMaxLength(12)
        //          .IsUnicode(false);

        //        entity.Property(e => e.Distributor)
        //       .HasColumnName("Distributor")
        //          .IsRequired(true)
        //          .HasMaxLength(150)
        //          .IsUnicode(false);

        //        entity.Property(e => e.Status)
        //       .HasColumnName("Status")
        //          .IsRequired(true)
        //          .HasMaxLength(1)
        //          .IsUnicode(false);

        //        entity.Property(e => e.Description)
        //      .HasColumnName("Description")
        //         .IsRequired(false)
        //         .HasMaxLength(500)
        //         .IsUnicode(false);

        //        entity.Property(e => e.SiteId)
        //      .HasColumnName("SiteId")
        //         .IsRequired(false)
        //         .HasMaxLength(12)
        //         .IsUnicode(false);
        //    });

        //    modelBuilder.Entity<InvoiceSKU>(entity =>
        //    {
        //        entity.HasKey(e => e.InvoiceSKUId);
        //        entity.ToTable("InvoiceSKU", "autodesk_demo");

        //        entity.Property(e => e.InvoiceSKUId)
        //            .HasColumnName("InvoiceSKUId")
        //            .HasColumnType("int(10)");

        //        entity.Property(e => e.SiteCountryDistributorSKUId)
        //            .IsRequired(false)
        //            .HasColumnName("SiteCountryDistributorSKUId")
        //            .HasMaxLength(12)
        //            .IsUnicode(false);

        //        entity.Property(e => e.PricingSKUId)
        //        .HasColumnName("PricingSKUId")
        //            .IsRequired(false)
        //             .HasMaxLength(50)
        //            .IsUnicode(false);


        //        entity.Property(e => e.QTY)
        //            .HasColumnName("QTY")
        //            .IsRequired(false)
        //            .HasColumnType("int(10)")
        //            .IsUnicode(false);

        //        entity.Property(e => e.TotalAmmount)
        //        .HasColumnName("TotalAmmount")
        //           .IsRequired(false)
        //           .HasColumnType("float")
        //           .IsUnicode(false);

        //        entity.Property(e => e.InvoiceId)
        //       .HasColumnName("InvoiceId")
        //          .IsRequired(false)
        //          .HasMaxLength(50)
        //          .IsUnicode(false);
        //entity.Property(e => e.SiteId)
        //       .HasColumnName("SiteId")
        //          .IsRequired(false)
        //          .HasMaxLength(15)
        //          .IsUnicode(false);
        //    });

        //    modelBuilder.Entity<Courses>(entity =>
        //    {
        //        entity.HasKey(e => new { e.CourseId, e.ApplicationId });

        //        entity.ToTable("Courses", "autodesk_demo");

        //        entity.HasIndex(e => e.CompletionDate)
        //            .HasName("IDX_CompletionDate");

        //        entity.HasIndex(e => e.ContactId)
        //            .HasName("IDX_ContactId");

        //        entity.HasIndex(e => e.CourseId)
        //            .HasName("IDX_CourseId");

        //        entity.HasIndex(e => e.FyindicatorKey)
        //            .HasName("IDX_FYIndicatorKey");

        //        entity.HasIndex(e => e.Name)
        //            .HasName("IDX_CourseTitle");

        //        entity.HasIndex(e => e.PartnerType)
        //            .HasName("IDX_PartnerType");

        //        entity.HasIndex(e => e.SiteId)
        //            .HasName("IDX_SiteId");

        //        entity.HasIndex(e => e.StartDate)
        //            .HasName("IDX_StartDate");

        //        entity.Property(e => e.CourseId)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ApplicationId)
        //            .HasColumnType("int(11)")
        //            .HasDefaultValueSql("0");

        //        entity.Property(e => e.Atccomputer)
        //            .HasColumnName("ATCComputer")
        //            .HasColumnType("bit(1)");

        //        entity.Property(e => e.Atcfacility)
        //            .HasColumnName("ATCFacility")
        //            .HasColumnType("bit(1)");

        //        entity.Property(e => e.CertificateId).HasColumnType("int(11)");

        //        entity.Property(e => e.CertificateNumber)
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContactId)
        //            .HasMaxLength(11)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CourseCode)
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.EvaluationQuestionId).HasColumnType("int(11)");

        //        entity.Property(e => e.EventType2)
        //            .HasMaxLength(2)
        //            .IsUnicode(false);

        //        entity.Property(e => e.EventTypeKey)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.FyindicatorKey)
        //            .HasColumnName("FYIndicatorKey")
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.HoursTraining)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.HoursTrainingOther)
        //            .HasColumnType("tinyint(4)")
        //            .HasDefaultValueSql("0");

        //        entity.Property(e => e.Institution)
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InstitutionLogo).HasColumnType("mediumblob");

        //        entity.Property(e => e.LocationName)
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LogoName)
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LogoType)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Name)
        //            .HasMaxLength(150)
        //            .IsUnicode(false);

        //        entity.Property(e => e.PartnerType)
        //            .HasMaxLength(11)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ProjectTypeKey)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteId)
        //            .HasMaxLength(15)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Status).HasColumnType("char(1)");

        //        entity.Property(e => e.TrainingTypeKey)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);
        //    });

        //    modelBuilder.Entity<Roles>(entity =>
        //    {
        //        entity.HasKey(e => e.RoleId);

        //        entity.ToTable("Roles", "autodesk_demo");

        //        entity.HasIndex(e => e.RoleCode)
        //            .HasName("IDX_Roles_RoleCode");

        //        entity.HasIndex(e => e.RoleId)
        //            .HasName("IDX_Roles_RoleId");

        //        entity.HasIndex(e => e.RoleName)
        //            .HasName("IDX_Roles_RoleName");

        //        entity.Property(e => e.RoleId).HasColumnType("int(10)");

        //        entity.Property(e => e.CrbapprovedName)
        //            .IsRequired()
        //            .HasColumnName("CRBApprovedName")
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Description).IsUnicode(false);

        //        entity.Property(e => e.FrameworkName)
        //            .IsRequired()
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RoleCode)
        //            .IsRequired()
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RoleName)
        //            .IsRequired()
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RoleSubType)
        //            .IsRequired()
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RoleType)
        //            .IsRequired()
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelName)
        //            .IsRequired()
        //            .HasMaxLength(100)
        //            .IsUnicode(false);
        //    });

        //    modelBuilder.Entity<RefTerritory>(entity =>
        //    {
        //        entity.HasKey(e => e.TerritoryId);

        //        entity.ToTable("Ref_Territory", "autodesk_demo");

        //        entity.HasIndex(e => e.TerritoryId)
        //            .HasName("INDEX_territory_id");

        //        entity.HasIndex(e => e.TerritoryName)
        //            .HasName("INDEX_territory_name");

        //        entity.Property(e => e.TerritoryId).HasColumnType("int(11)");

        //        entity.Property(e => e.Cdate).HasColumnName("cdate");

        //        entity.Property(e => e.Cuid)
        //            .HasColumnName("cuid")
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Mdate).HasColumnName("mdate");

        //        entity.Property(e => e.Muid)
        //            .HasColumnName("muid")
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.TerritoryName)
        //            .IsRequired()
        //            .HasColumnName("Territory_Name")
        //            .HasMaxLength(255)
        //            .IsUnicode(false);
        //    });
        //    modelBuilder.Entity<RefCountries>(entity =>
        //    {
        //        entity.HasKey(e => new { e.CountriesId, e.CountriesCode });

        //        entity.ToTable("Ref_Countries", "autodesk_demo");

        //        entity.HasIndex(e => e.CountriesCode)
        //            .HasName("IDX_Countries_Code");

        //        entity.HasIndex(e => e.CountriesName)
        //            .HasName("IDX_Countries_Country");

        //        entity.HasIndex(e => e.GeoCode)
        //            .HasName("IDX_Countries_Geo");

        //        entity.HasIndex(e => e.RegionCode)
        //            .HasName("IDX_Countries_Region");

        //        entity.HasIndex(e => e.SubregionCode)
        //            .HasName("IDX_Countries_Subregion");

        //        entity.Property(e => e.CountriesId)
        //            .HasColumnName("countries_id")
        //            .HasColumnType("int(11)")
        //            .ValueGeneratedOnAdd();

        //        entity.Property(e => e.CountriesCode)
        //            .HasColumnName("countries_code")
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Cdate).HasColumnName("cdate");

        //        entity.Property(e => e.CountriesName)
        //            .HasColumnName("countries_name")
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CountriesTel)
        //            .IsRequired()
        //            .HasColumnName("countries_tel")
        //            .HasMaxLength(5)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Cuid)
        //            .HasColumnName("cuid")
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Embargoed)
        //            .HasColumnType("tinyint(1)")
        //            .HasDefaultValueSql("0");

        //        entity.Property(e => e.GeoCode)
        //            .HasColumnName("geo_code")
        //            .HasMaxLength(10)
        //            .IsUnicode(false);

        //        entity.Property(e => e.GeoName)
        //            .IsRequired()
        //            .HasColumnName("geo_name")
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MarketTypeId)
        //            .HasColumnType("int(2)")
        //            .HasDefaultValueSql("1");

        //        entity.Property(e => e.Mdate).HasColumnName("mdate");

        //        entity.Property(e => e.Muid)
        //            .HasColumnName("muid")
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MultiplierOverride).HasColumnType("int(255)");

        //        entity.Property(e => e.RegionCode)
        //            .HasColumnName("region_code")
        //            .HasMaxLength(10)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RegionName)
        //            .IsRequired()
        //            .HasColumnName("region_name")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SubregionCode)
        //            .IsRequired()
        //            .HasColumnName("subregion_code")
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SubregionName)
        //            .IsRequired()
        //            .HasColumnName("subregion_name")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.TerritoryId).HasColumnType("int(11)");
        //    });
        //    modelBuilder.Entity<Invoices>(entity =>
        //    {
        //        entity.HasKey(e => e.InvoiceId);

        //        entity.ToTable("Invoices", "autodesk_demo");

        //        entity.HasIndex(e => e.OrgId)
        //            .HasName("IDX_INVOICES_OrgId");

        //        entity.Property(e => e.InvoiceId).HasColumnType("int(8)");

        //        entity.Property(e => e.AddedBy)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AmountOutstanding).HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.CcexpDate)
        //            .HasColumnName("CCExpDate")
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CcexpDate2)
        //            .HasColumnName("CCExpDate2")
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Ccname)
        //            .HasColumnName("CCName")
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Ccname2)
        //            .HasColumnName("CCName2")
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Comments).IsUnicode(false);

        //        entity.Property(e => e.ContractEndDate).HasColumnType("date");

        //        entity.Property(e => e.ContractId)
        //            .HasColumnName("ContractID")
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContractStartDate).HasColumnType("date");

        //        entity.Property(e => e.ContractStatus)
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.DateAdded).HasDefaultValueSql("0000-00-00 00:00:00");

        //        entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("0000-00-00 00:00:00");

        //        entity.Property(e => e.DistributorFee).HasColumnType("decimal(20,0)");

        //        entity.Property(e => e.DocuSign)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.FinancialYear)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoiceDate).HasColumnType("date");

        //        entity.Property(e => e.InvoiceDescription)
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoiceNumber)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoiceType)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoicedCurrency)
        //            .HasMaxLength(10)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L1Description)
        //            .HasColumnName("L1_Description")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L1Quantity)
        //            .HasColumnName("L1_Quantity")
        //            .HasColumnType("int(3)");

        //        entity.Property(e => e.L1Summary)
        //            .HasColumnName("L1_Summary")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L1TotalAmount)
        //            .HasColumnName("L1_TotalAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L1UnitPrice)
        //            .HasColumnName("L1_UnitPrice")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L1Vatamount)
        //            .HasColumnName("L1_VATAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L1Vatrate)
        //            .HasColumnName("L1_VATRate")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L2Description)
        //            .HasColumnName("L2_Description")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L2Quantity)
        //            .HasColumnName("L2_Quantity")
        //            .HasColumnType("int(3)");

        //        entity.Property(e => e.L2Summary)
        //            .HasColumnName("L2_Summary")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L2TotalAmount)
        //            .HasColumnName("L2_TotalAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L2UnitPrice)
        //            .HasColumnName("L2_UnitPrice")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L2Vatamount)
        //            .HasColumnName("L2_VATAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L2Vatrate)
        //            .HasColumnName("L2_VATRate")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L3Description)
        //            .HasColumnName("L3_Description")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L3Quantity)
        //            .HasColumnName("L3_Quantity")
        //            .HasColumnType("int(3)");

        //        entity.Property(e => e.L3Summary)
        //            .HasColumnName("L3_Summary")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L3TotalAmount)
        //            .HasColumnName("L3_TotalAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L3UnitPrice)
        //            .HasColumnName("L3_UnitPrice")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L3Vatamount)
        //            .HasColumnName("L3_VATAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L3Vatrate)
        //            .HasColumnName("L3_VATRate")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L4Description)
        //            .HasColumnName("L4_Description")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L4Quantity)
        //            .HasColumnName("L4_Quantity")
        //            .HasColumnType("int(3)");

        //        entity.Property(e => e.L4Summary)
        //            .HasColumnName("L4_Summary")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L4TotalAmount)
        //            .HasColumnName("L4_TotalAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L4UnitPrice)
        //            .HasColumnName("L4_UnitPrice")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L4Vatamount)
        //            .HasColumnName("L4_VATAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L4Vatrate)
        //            .HasColumnName("L4_VATRate")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L5Description)
        //            .HasColumnName("L5_Description")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L5Quantity)
        //            .HasColumnName("L5_Quantity")
        //            .HasColumnType("int(3)");

        //        entity.Property(e => e.L5Summary)
        //            .HasColumnName("L5_Summary")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L5TotalAmount)
        //            .HasColumnName("L5_TotalAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L5UnitPrice)
        //            .HasColumnName("L5_UnitPrice")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L5Vatamount)
        //            .HasColumnName("L5_VATAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L5Vatrate)
        //            .HasColumnName("L5_VATRate")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L6Description)
        //            .HasColumnName("L6_Description")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L6Quantity)
        //            .HasColumnName("L6_Quantity")
        //            .HasColumnType("int(3)");

        //        entity.Property(e => e.L6Summary)
        //            .HasColumnName("L6_Summary")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L6TotalAmount)
        //            .HasColumnName("L6_TotalAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L6UnitPrice)
        //            .HasColumnName("L6_UnitPrice")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L6Vatamount)
        //            .HasColumnName("L6_VATAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L6Vatrate)
        //            .HasColumnName("L6_VATRate")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L7Description)
        //            .HasColumnName("L7_Description")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L7Quantity)
        //            .HasColumnName("L7_Quantity")
        //            .HasColumnType("int(3)");

        //        entity.Property(e => e.L7Summary)
        //            .HasColumnName("L7_Summary")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L7TotalAmount)
        //            .HasColumnName("L7_TotalAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L7UnitPrice)
        //            .HasColumnName("L7_UnitPrice")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L7Vatamount)
        //            .HasColumnName("L7_VATAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L7Vatrate)
        //            .HasColumnName("L7_VATRate")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L8Description)
        //            .HasColumnName("L8_Description")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L8Quantity)
        //            .HasColumnName("L8_Quantity")
        //            .HasColumnType("int(3)");

        //        entity.Property(e => e.L8Summary)
        //            .HasColumnName("L8_Summary")
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.L8TotalAmount)
        //            .HasColumnName("L8_TotalAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L8UnitPrice)
        //            .HasColumnName("L8_UnitPrice")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L8Vatamount)
        //            .HasColumnName("L8_VATAmount")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.L8Vatrate)
        //            .HasColumnName("L8_VATRate")
        //            .HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.LastAdminBy)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.NumberOfLicenses).HasColumnType("int(10)");

        //        entity.Property(e => e.OldContractId)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.OrgId)
        //            .IsRequired()
        //            .HasMaxLength(15)
        //            .IsUnicode(false);

        //        entity.Property(e => e.PaymentAmount)
        //            .HasColumnType("decimal(20,2)")
        //            .HasDefaultValueSql("0.00");

        //        entity.Property(e => e.PaymentAmount2).HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.PaymentCurrency)
        //            .HasMaxLength(10)
        //            .IsUnicode(false);

        //        entity.Property(e => e.PaymentDate).HasColumnType("date");

        //        entity.Property(e => e.PaymentDate2).HasColumnType("date");

        //        entity.Property(e => e.PaymentMethod)
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.PaymentMethod2)
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.PaymentMonth).HasColumnType("int(2)");

        //        entity.Property(e => e.PaymentMonth2).HasColumnType("int(2)");

        //        entity.Property(e => e.PaymentReference)
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.PaymentReference2)
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Poid)
        //            .HasColumnName("POID")
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.PoreceivedDate)
        //            .HasColumnName("POReceivedDate")
        //            .HasColumnType("date");

        //        entity.Property(e => e.SiteCountryDistributorSkuid)
        //            .HasColumnName("SiteCountryDistributorSKUId")
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteIdList)
        //            .HasMaxLength(400)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Status).HasColumnType("char(1)");

        //        entity.Property(e => e.TotalInvoicedAmount)
        //            .HasColumnType("decimal(20,2)")
        //            .HasDefaultValueSql("0.00");

        //        entity.Property(e => e.TotalLineAmount).HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.TotalPaymentAmount).HasColumnType("decimal(20,2)");

        //        entity.Property(e => e.TotalVatamount)
        //            .HasColumnName("TotalVATAmount")
        //            .HasColumnType("decimal(20,2)")
        //            .HasDefaultValueSql("0.00");
        //    });
        //    modelBuilder.Entity<MainSite>(entity =>
        //    {
        //        entity.HasKey(e => new { e.SiteIdInt, e.SiteId });

        //        entity.ToTable("Main_Site", "autodesk_demo");

        //        entity.HasIndex(e => e.AtcsiteId)
        //            .HasName("IDX_SITES_ATCID");

        //        entity.HasIndex(e => e.CommercialSiteName)
        //            .HasName("IDX_Sites_CommName");

        //        entity.HasIndex(e => e.CsolocationUuid)
        //            .HasName("IDX_SITES_LOCUUID");

        //        entity.HasIndex(e => e.EnglishSiteName)
        //            .HasName("IDX_Sites_EngName");

        //        entity.HasIndex(e => e.OrgId)
        //            .HasName("IDX_SITES_OrgId");

        //        entity.HasIndex(e => e.SiebelSiteName)
        //            .HasName("SiebelSiteName");

        //        entity.HasIndex(e => e.SiteAddress1)
        //            .HasName("IDX_Sites_SiteAddr1");

        //        entity.HasIndex(e => e.SiteCountryCode)
        //            .HasName("IDX_SITES_CountryCode");

        //        entity.HasIndex(e => e.SiteId)
        //            .HasName("IDX_SiteId");

        //        entity.HasIndex(e => e.SiteName)
        //            .HasName("IDX_SITES_SiteName");

        //        entity.HasIndex(e => e.SiteStateProvince)
        //            .HasName("IDX_Site_State");

        //        entity.HasIndex(e => e.SiteStatusRetired)
        //            .HasName("IDX_Sites_SiteStatus");

        //        entity.HasIndex(e => e.Status)
        //            .HasName("IDX_SITES_Status");

        //        entity.Property(e => e.SiteIdInt)
        //            .HasColumnType("int(11)")
        //            .ValueGeneratedOnAdd();

        //        entity.Property(e => e.SiteId)
        //            .HasMaxLength(15)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AddedBy)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AdminNotes).IsUnicode(false);

        //        entity.Property(e => e.AtcsiteId)
        //            .HasColumnName("ATCSiteId")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CommercialSiteName)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsoauthorizationCodes)
        //            .HasColumnName("CSOAuthorizationCodes")
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsolocationName)
        //            .HasColumnName("CSOLocationName")
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsolocationNumber)
        //            .HasColumnName("CSOLocationNumber")
        //            .HasMaxLength(8)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsolocationUuid)
        //            .HasColumnName("CSOLocationUUID")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsopartnerType)
        //            .HasColumnName("CSOpartner_type")
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

        //        entity.Property(e => e.Csoversion)
        //            .HasColumnName("CSOVersion")
        //            .HasMaxLength(3)
        //            .IsUnicode(false);

        //        entity.Property(e => e.EnglishSiteName)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LastAdminBy)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MagellanId)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MailingAddress1)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MailingAddress2)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MailingAddress3)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MailingCity)
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MailingCountryCode)
        //            .HasMaxLength(2)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MailingDepartment)
        //            .HasMaxLength(180)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MailingPostalCode)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.MailingStateProvince)
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.OrgId)
        //            .HasMaxLength(15)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SapnumberRetired)
        //            .HasColumnName("SAPNumber_retired")
        //            .HasMaxLength(120)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SapshipToRetired)
        //            .HasColumnName("SAPShipTo_retired")
        //            .HasMaxLength(120)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ShippingAddress1)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ShippingAddress2)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ShippingAddress3)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ShippingCity)
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ShippingCountryCode)
        //            .HasMaxLength(2)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ShippingDepartment)
        //            .HasMaxLength(180)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ShippingPostalCode)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ShippingStateProvince)
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelAddress1)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelAddress2)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelAddress3)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelCity)
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelCountryCode)
        //            .HasMaxLength(2)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelDepartment)
        //            .HasMaxLength(180)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelPostalCode)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelSiteName)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiebelStateProvince)
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteAddress1)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteAddress2)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteAddress3)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteAdminEmailAddress)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteAdminFax)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteAdminFirstName)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteAdminLastName)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteAdminTelephone)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteCity)
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteCountryCode).HasColumnType("char(2)");

        //        entity.Property(e => e.SiteDepartment)
        //            .HasMaxLength(180)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteEmailAddress)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteFax)
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteManagerEmailAddress)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteManagerFax)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteManagerFirstName)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteManagerLastName)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteManagerTelephone)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteName)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SitePostalCode)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteStateProvince)
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteStatusRetired)
        //            .HasColumnName("SiteStatus_retired")
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteTelephone)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SiteWebAddress)
        //            .HasMaxLength(200)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Status).HasColumnType("char(1)");

        //        entity.Property(e => e.Workstations)
        //            .HasColumnType("int(8)")
        //            .HasDefaultValueSql("10");
        //    });
        //    modelBuilder.Entity<InvoiceDiscount>(entity =>
        //    {
        //        entity.HasKey(e => e.Id);
        //        entity.ToTable("InvoiceDiscount", "autodesk_demo");

        //        entity.Property(e => e.Id)
        //            .HasColumnName("Id")
        //            .HasColumnType("int(11)");

        //        entity.Property(e => e.AmountType)
        //            .IsRequired(true)
        //            .HasMaxLength(15)
        //             .IsUnicode(false);

        //        entity.Property(e => e.Amount)
        //            .IsRequired()
        //              .HasColumnType("float")
        //            .IsUnicode(false);

        //        entity.Property(e => e.Name)
        //           .IsRequired(true)
        //           .HasMaxLength(15)
        //           .IsUnicode(false);

        //        entity.Property(e => e.InvoiceId)
        //           .IsRequired(true)
        //           .HasColumnType("int(8)")
        //           .IsUnicode(false);
        //    });
        //    modelBuilder.Entity<MainOrganization>(entity =>
        //    {
        //        entity.HasKey(e => new { e.OrganizationId, e.OrgId });

        //        entity.ToTable("Main_Organization", "autodesk_demo");

        //        entity.HasIndex(e => e.AtcorgId)
        //            .HasName("IDX_ORGANIZATIONS_ATCID");

        //        entity.HasIndex(e => e.CommercialOrgName)
        //            .HasName("IDX_Org_CommName");

        //        entity.HasIndex(e => e.CsoresellerUuid)
        //            .HasName("IDX_ORGANIZATIONS_UUID");

        //        entity.HasIndex(e => e.EnglishOrgName)
        //            .HasName("IDX_Org_EngName");

        //        entity.HasIndex(e => e.OrgId)
        //            .HasName("OrgId");

        //        entity.HasIndex(e => e.OrgName)
        //            .HasName("IDX_ORGANIZATIONS_OrgName");

        //        entity.HasIndex(e => e.OrgStatusRetired)
        //            .HasName("IDX_ORG_OrgStatus");

        //        entity.HasIndex(e => e.RegisteredCountryCode)
        //            .HasName("IDX_ORGANIZATIONS_CountryCode");

        //        entity.HasIndex(e => e.RegisteredStateProvince)
        //            .HasName("IDX_Org_State");

        //        entity.HasIndex(e => e.SapsoldTo)
        //            .HasName("IDX_ORGANIZATIONS_SAP");

        //        entity.HasIndex(e => e.Status)
        //            .HasName("IDX_ORGANIZATIONS_Status");

        //        entity.Property(e => e.OrganizationId)
        //            .HasColumnType("int(11)")
        //            .ValueGeneratedOnAdd();

        //        entity.Property(e => e.OrgId)
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AddedBy)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AdminNotes).IsUnicode(false);

        //        entity.Property(e => e.Atccsn)
        //            .HasColumnName("ATCCSN")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AtcdirectorEmailAddress)
        //            .HasColumnName("ATCDirectorEmailAddress")
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AtcdirectorFax)
        //            .HasColumnName("ATCDirectorFax")
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AtcdirectorFirstName)
        //            .HasColumnName("ATCDirectorFirstName")
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AtcdirectorLastName)
        //            .HasColumnName("ATCDirectorLastName")
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AtcdirectorTelephone)
        //            .HasColumnName("ATCDirectorTelephone")
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.AtcorgId)
        //            .HasColumnName("ATCOrgId")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Aupcsn)
        //            .HasColumnName("AUPCSN")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.BillingContactEmailAddress)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.BillingContactFax)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.BillingContactFirstName)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.BillingContactLastName)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.BillingContactTelephone)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CommercialOrgName)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContractAddress1)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContractAddress2)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContractAddress3)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContractCity)
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContractCountryCode)
        //            .HasMaxLength(2)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContractDepartment)
        //            .HasMaxLength(180)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContractPostalCode)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.ContractStateProvince)
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsopartnerManager)
        //            .HasColumnName("CSOPartnerManager")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsoresellerName)
        //            .HasColumnName("CSOResellerName")
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsoresellerUuid)
        //            .HasColumnName("CSOResellerUUID")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

        //        entity.Property(e => e.Csoversion)
        //            .HasColumnName("CSOVersion")
        //            .HasMaxLength(3)
        //            .IsUnicode(false);

        //        entity.Property(e => e.EnglishOrgName)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoicingAddress1)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoicingAddress2)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoicingAddress3)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoicingCity)
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoicingCountryCode)
        //            .HasMaxLength(2)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoicingDepartment)
        //            .HasMaxLength(180)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoicingPostalCode)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.InvoicingStateProvince)
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LastAdminBy)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LegalContactEmailAddress)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LegalContactFax)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LegalContactFirstName)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LegalContactLastName)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LegalContactTelephone)
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.OrgName)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.OrgStatusRetired)
        //            .HasColumnName("OrgStatus_retired")
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.OrgWebAddress)
        //            .HasMaxLength(60)
        //            .IsUnicode(false);

        //        entity.Property(e => e.PrimarySiteId)
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RegisteredAddress1)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RegisteredAddress2)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RegisteredAddress3)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RegisteredCity)
        //            .HasMaxLength(80)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RegisteredCountryCode)
        //            .HasMaxLength(2)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RegisteredDepartment)
        //            .HasMaxLength(180)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RegisteredPostalCode)
        //            .HasMaxLength(30)
        //            .IsUnicode(false);

        //        entity.Property(e => e.RegisteredStateProvince)
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.SapsoldTo)
        //            .HasColumnName("SAPSoldTo")
        //            .HasMaxLength(120)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Status).HasColumnType("char(1)");

        //        entity.Property(e => e.TaxExempt).HasColumnType("char(1)");

        //        entity.Property(e => e.Varcsn)
        //            .HasColumnName("VARCSN")
        //            .HasMaxLength(40)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Vatnumber)
        //            .HasColumnName("VATNumber")
        //            .HasMaxLength(20)
        //            .IsUnicode(false);

        //        entity.Property(e => e.YearJoined)
        //            .HasMaxLength(4)
        //            .IsUnicode(false);
        //    });
        //    modelBuilder.Entity<EmailBody>(entity =>
        //    {
        //        entity.HasKey(e => e.BodyId);

        //        entity.ToTable("EmailBody", "autodesk_demo");

        //        entity.Property(e => e.BodyId)
        //            .HasColumnName("body_id")
        //            .HasColumnType("int(2)");

        //        entity.Property(e => e.AddedBy)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.BodyEmail)
        //            .HasColumnName("body_email")
        //            .IsUnicode(false);

        //        entity.Property(e => e.BodyType)
        //            .HasColumnName("body_type")
        //            .HasMaxLength(100)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Key)
        //            .HasMaxLength(3)
        //            .IsUnicode(false);

        //        entity.Property(e => e.LastAdminBy)
        //            .HasMaxLength(50)
        //            .IsUnicode(false);

        //        entity.Property(e => e.Subject)
        //            .HasColumnName("subject")
        //            .HasMaxLength(100)
        //            .IsUnicode(false);
        //    });

        //    modelBuilder.Entity<EmailBodyType>(entity =>
        //    {
        //        entity.ToTable("EmailBodyType", "autodesk_demo");

        //        entity.Property(e => e.EmailBodyTypeId).HasColumnType("int(11)");

        //        entity.Property(e => e.EmailBodyTypeName)
        //            .HasMaxLength(255)
        //            .IsUnicode(false);
        //    });
        //    modelBuilder.Entity<ContactsAll>(entity =>
        //        {
        //            entity.HasKey(e => e.ContactIdInt);

        //            entity.ToTable("Contacts_All", "autodesk_demo");

        //            entity.HasIndex(e => e.ContactId)
        //                .HasName("IDX_ContactId");

        //    entity.HasIndex(e => e.ContactName)
        //                .HasName("IDX_CONTACTS_ContactName");

        //    entity.HasIndex(e => e.CsouserUuid)
        //                .HasName("IDX_CONTACT_UUID");

        //    entity.HasIndex(e => e.EmailAddress)
        //                .HasName("IDX_CONTACT_EMAIL");

        //    entity.HasIndex(e => e.EmailAddress2)
        //                .HasName("IDX_Contacts_EMAIL2");

        //    entity.HasIndex(e => e.EnglishContactName)
        //                .HasName("IDX_Contacts_EngName");

        //    entity.HasIndex(e => e.FirstName)
        //                .HasName("IDX_CONTACTS_FirstName");

        //    entity.HasIndex(e => e.InstructorId)
        //                .HasName("IDX_Contacts_InstructorId");

        //    entity.HasIndex(e => e.InternalSales)
        //                .HasName("InternalSales");

        //    entity.HasIndex(e => e.LastName)
        //                .HasName("IDX_CONTACTS_LastName");

        //    entity.HasIndex(e => e.PrimarySiteId)
        //                .HasName("IDX_Contacts_PrimarySiteId");

        //    entity.HasIndex(e => e.SiebelId)
        //                .HasName("SiebelId");

        //    entity.HasIndex(e => e.Status)
        //                .HasName("IDX_CONTACTS_Status");

        //    entity.HasIndex(e => e.Username)
        //                .HasName("IDX_CONTACTS_UNIQUE_USERNAME");

        //    entity.Property(e => e.ContactIdInt).HasColumnType("int(11)");

        //    entity.Property(e => e.AcimemberId)
        //                .HasColumnName("ACIMemberId")
        //                .HasColumnType("int(11)");

        //    entity.Property(e => e.Action)
        //                .HasMaxLength(20)
        //                .IsUnicode(false);

        //    entity.Property(e => e.AddedBy)
        //                .HasMaxLength(50)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Address1)
        //                .HasMaxLength(200)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Address2)
        //                .HasMaxLength(200)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Address3)
        //                .HasMaxLength(200)
        //                .IsUnicode(false);

        //    entity.Property(e => e.AdminSystems)
        //                .HasMaxLength(20)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Administrator)
        //                .HasColumnType("char(1)")
        //                .HasDefaultValueSql("N");

        //    entity.Property(e => e.Atcrole)
        //                .HasColumnName("ATCRole")
        //                .HasMaxLength(1)
        //                .IsUnicode(false)
        //                .HasDefaultValueSql("N");

        //    entity.Property(e => e.Bio)
        //                .HasMaxLength(2000)
        //                .IsUnicode(false);

        //    entity.Property(e => e.City)
        //                .HasMaxLength(100)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Comments).IsUnicode(false);

        //    entity.Property(e => e.CompanyName)
        //                .HasMaxLength(50)
        //                .IsUnicode(false);

        //    entity.Property(e => e.ContactId)
        //                .IsRequired()
        //                .HasMaxLength(20)
        //                .IsUnicode(false);

        //    entity.Property(e => e.ContactName)
        //                .HasMaxLength(121)
        //                .IsUnicode(false);

        //    entity.Property(e => e.CountryCode)
        //                .HasMaxLength(2)
        //                .IsUnicode(false);

        //    entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

        //    entity.Property(e => e.CsouserUuid)
        //                .HasColumnName("CSOUserUUID")
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Csoversion)
        //                .HasColumnName("CSOVersion")
        //                .HasMaxLength(3)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Dedicated).HasColumnType("char(1)");

        //    entity.Property(e => e.Department)
        //                .HasMaxLength(255)
        //                .IsUnicode(false);

        //    entity.Property(e => e.DoNotEmail).HasColumnType("char(1)");

        //    entity.Property(e => e.EmailAddress)
        //                .HasMaxLength(60)
        //                .IsUnicode(false);

        //    entity.Property(e => e.EmailAddress2)
        //                .HasMaxLength(60)
        //                .IsUnicode(false);

        //    entity.Property(e => e.EmailAddress3)
        //                .HasMaxLength(60)
        //                .IsUnicode(false);

        //    entity.Property(e => e.EmailAddress4)
        //                .HasMaxLength(60)
        //                .IsUnicode(false);

        //    entity.Property(e => e.EnglishContactName)
        //                .HasMaxLength(100)
        //                .IsUnicode(false);

        //    entity.Property(e => e.ExperiencedCompetitiveProducts).HasColumnType("char(1)");

        //    entity.Property(e => e.ExperiencedCompetitiveProductsList)
        //                .HasMaxLength(255)
        //                .IsUnicode(false);

        //    entity.Property(e => e.FirstName)
        //                .HasMaxLength(60)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Gender)
        //                .HasMaxLength(1)
        //                .IsUnicode(false);

        //    entity.Property(e => e.HowLongAutodeskProducts)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.HowLongInIndustry)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.HowLongWithOrg)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.InstructorId)
        //                .HasMaxLength(20)
        //                .IsUnicode(false);

        //    entity.Property(e => e.InternalSales)
        //                .HasMaxLength(1)
        //                .IsUnicode(false);

        //    entity.Property(e => e.LastAdminBy)
        //                .HasMaxLength(50)
        //                .IsUnicode(false);

        //    entity.Property(e => e.LastIpaddress)
        //                .HasColumnName("LastIPAddress")
        //                .HasMaxLength(15)
        //                .IsUnicode(false);

        //    entity.Property(e => e.LastName)
        //                .HasMaxLength(60)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Level).HasColumnType("char(1)");

        //    entity.Property(e => e.LoginCount).HasColumnType("smallint(5)");

        //    entity.Property(e => e.Mobile1)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Mobile2)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.MobileCode)
        //                .HasMaxLength(10)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Password)
        //                .HasColumnName("password")
        //                .HasMaxLength(2000)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PasswordPlain)
        //                .HasColumnName("password_plain")
        //                .HasMaxLength(255)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PercentPostSales)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PercentPreSales)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PermissionsGroup)
        //                .HasMaxLength(30)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PostalCode)
        //                .HasMaxLength(30)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PrimaryIndustryId)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PrimaryLanguage).HasColumnType("char(10)");

        //    entity.Property(e => e.PrimaryRoleExperience)
        //                .HasMaxLength(20)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PrimaryRoleId).HasColumnType("int(10)");

        //    entity.Property(e => e.PrimarySiteId)
        //                .HasMaxLength(10)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PrimarySubIndustryId)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.ProfilePicture)
        //                .HasMaxLength(255)
        //                .IsUnicode(false);

        //    entity.Property(e => e.PwdchangeRequired)
        //                .HasColumnName("PWDChangeRequired")
        //                .HasColumnType("char(1)");

        //    entity.Property(e => e.QuestionnaireDate).HasColumnType("date");

        //    entity.Property(e => e.ResetPasswordCode).IsUnicode(false);

        //    entity.Property(e => e.ResetToken)
        //                .HasMaxLength(32)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Salutation)
        //                .HasMaxLength(10)
        //                .IsUnicode(false);

        //    entity.Property(e => e.SecondaryLanguage).HasColumnType("char(10)");

        //    entity.Property(e => e.ShareEmail)
        //                .HasColumnType("char(1)")
        //                .HasDefaultValueSql("1");

        //    entity.Property(e => e.ShareMobile)
        //                .HasColumnType("char(1)")
        //                .HasDefaultValueSql("1");

        //    entity.Property(e => e.ShareTelephone)
        //                .HasColumnType("char(1)")
        //                .HasDefaultValueSql("1");

        //    entity.Property(e => e.ShowDuplicates)
        //                .HasColumnType("char(1)")
        //                .HasDefaultValueSql("N");

        //    entity.Property(e => e.ShowInSearch)
        //                .HasColumnType("char(1)")
        //                .HasDefaultValueSql("1");

        //    entity.Property(e => e.SiebelId)
        //                .HasMaxLength(20)
        //                .IsUnicode(false);

        //    entity.Property(e => e.SiebelStatus)
        //                .IsRequired()
        //                .HasMaxLength(20)
        //                .IsUnicode(false);

        //    entity.Property(e => e.StateProvince)
        //                .HasMaxLength(72)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Status).HasColumnType("char(1)");

        //    entity.Property(e => e.StatusEmailSend)
        //                .HasColumnType("int(11)")
        //                .HasDefaultValueSql("0");

        //    entity.Property(e => e.Telephone1)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Telephone2)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.TelephoneCode)
        //                .HasMaxLength(10)
        //                .IsUnicode(false);

        //    entity.Property(e => e.TimeZoneUtc).HasColumnName("TimeZoneUTC");

        //    entity.Property(e => e.Timezone)
        //                .HasMaxLength(100)
        //                .IsUnicode(false);

        //    entity.Property(e => e.TitlePosition)
        //                .HasMaxLength(40)
        //                .IsUnicode(false);

        //    entity.Property(e => e.UserLevelId)
        //                .HasMaxLength(255)
        //                .IsUnicode(false);

        //    entity.Property(e => e.Username)
        //                .HasMaxLength(60)
        //                .IsUnicode(false);

        //    entity.Property(e => e.WebsiteUrl)
        //                .HasMaxLength(200)
        //                .IsUnicode(false);
        //});
        //        modelBuilder.Entity<Dictionary>(entity =>
        //            {
        //                entity.HasKey(e => new { e.Key, e.Parent
        //    });

        //                entity.ToTable("Dictionary", "autodesk_demo");

        //                entity.Property(e => e.Key)
        //                    .HasMaxLength(50)
        //                    .IsUnicode(false);

        //    entity.Property(e => e.Parent)
        //                    .HasMaxLength(50)
        //                    .IsUnicode(false);

        //    entity.Property(e => e.KeyValue)
        //                    .IsRequired()
        //                    .IsUnicode(false);

        //    entity.Property(e => e.Status)
        //                    .IsRequired()
        //                    .HasColumnType("char(1)");
        //});


        //}
    }


}

