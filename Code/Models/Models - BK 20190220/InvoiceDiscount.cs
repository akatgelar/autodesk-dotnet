﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public class InvoiceDiscount
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public string AmountType { get; set; }
        public int InvoiceId { get; set; }
    }
}
