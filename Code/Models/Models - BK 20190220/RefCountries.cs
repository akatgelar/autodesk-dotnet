﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class RefCountries
    {
        public int CountriesId { get; set; }
        public string CountriesCode { get; set; }
        public string CountriesName { get; set; }
        public string GeoCode { get; set; }
        public string RegionCode { get; set; }
        public string SubregionCode { get; set; }
        public int? TerritoryId { get; set; }
        public string GeoName { get; set; }
        public string RegionName { get; set; }
        public string SubregionName { get; set; }
        public string CountriesTel { get; set; }
        public int? MarketTypeId { get; set; }
        public byte Embargoed { get; set; }
        public string Cuid { get; set; }
        public DateTime? Cdate { get; set; }
        public string Muid { get; set; }
        public DateTime? Mdate { get; set; }
        public int? MultiplierOverride { get; set; }
    }
}
