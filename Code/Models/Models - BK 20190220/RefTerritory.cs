﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class RefTerritory
    {
        public int TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public string Cuid { get; set; }
        public DateTime? Cdate { get; set; }
        public string Muid { get; set; }
        public DateTime? Mdate { get; set; }
    }
}
