﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class RefRegion
    {
        public int RegionId { get; set; }
        public string GeoCode { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string Cuid { get; set; }
        public DateTime? Cdate { get; set; }
        public string Muid { get; set; }
        public DateTime? Mdate { get; set; }
        public DateTime? TimeOfSynchro { get; set; }
    }
}
