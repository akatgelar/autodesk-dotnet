﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class EmailBody
    {
        public int BodyId { get; set; }
        public string BodyType { get; set; }
        public string BodyEmail { get; set; }
        public string Key { get; set; }
        public string Subject { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateAdded { get; set; }
        public string LastAdminBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
    }
}
