﻿namespace autodesk.Code.Models
{
    public partial class OrgSiteAttachment
    {
        public int Id { get; set; }
        public string SiteId { get; set; }
        public string OrgId { get; set; }
        public string Type { get; set; }
        public byte[] Content { get; set; }
        public string DocType { get; set; }
        public string Name { get; set; }
        public string Metadata { get; set; }
    }
}