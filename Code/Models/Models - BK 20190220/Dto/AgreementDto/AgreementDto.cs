﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models.Dto
{

    public class AgreementMasterDto
    {
        public int AgreementMasterId { get; set; }
        public string FyindicatorKey { get; set; }
        public string OrgId { get; set; }
        public string OrgName { get; set; }
        public string PartnerType { get; set; }
        public string Status { get; set; }
        public string LegalJobTitle { get; set; }
        public string LegalName { get; set; }
        public bool? CheckTc { get; set; }
        public int? TerritoryId { get; set; }
        public string CountryCode { get; set; }
        public string  CountryName { get; set; }
        public string TerritoryName { get; set; }
        public string ApproveBy { get; set; }
        public DateTime? RecordTime { get; set; }
    }
    public class AgreementDto
    {
        public int AgreementMasterId { get; set; }
        public string FyindicatorKey { get; set; }
        public string OrgId { get; set; }
        public string OrgName { get; set; }
        public string PartnerType { get; set; }
        public string Status { get; set; }
        public string TerritoryName { get; set; }
        public string ApproveBy { get; set; }
        public DateTime? RecordTime { get; set; }
    }
    public class AgreementMasterListDto
    {
        public List<AgreementDto> AgreementMasters { get; set; }
        public int TotalCount { get; set; } = 0;
    }
}
