﻿
using System.Collections.Generic;

namespace autodesk.Code.Models.Dto
{
    public class AgreementSearchDto
    {
        public string FY { get; set; }
        public string Organization { get; set; }
        //public string TerritoryId { get; set; }
        public string OrgId { get; set; }
        public string PartnerType { get; set; }
        //public string Country { get; set; }
        public string OrgName { get; set; }
        public string UserId { get; set; }
        //public string PageNumber { get; set; } = "1";
        //public string PageSize { get; set; } = "10";
        public List<string> Geo { get; set; }
        public List<string> TerritoryIds { get; set; }
        public List<string> PartnerTypeStatus { get; set; }
        //public string RegisteredStateProvince { get; set; }
        public List<string> RegisteredCountryCode { get; set; }
    }
}
//{
//  "Organization": "",
//  "OrgId": "",
//  "OrgName": "",
//  "RegisteredStateProvince": "",
//  "RegisteredCountryCode": "",
//  "PartnerType": "",
//  "PartnerTypeStatus": "'A','V','X','I','O','P','R','B','S','T'",
//  "Geo": "'A','P','E','G','ty'",
//  "Territory": "'16','18','19','20','21','22','23','24','25','26','27','28','30','31','32','33','35','36','37'"
//}