﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto
{
    public class OrgDropdownListDto
    {
        public string OrgId { get; set; }
        public string OrgName { get; set; }
    }

    public class TerritoryDropdownListDto
    {
        public int Id { get; set; }
        public string TerritoryName { get; set; }
    }
    public class FYDropdownListDto
    {
        public string KeyValue { get; set; }
        public string FYName { get; set; }
        public string Key { get; set; }
    }

    public class CountryDropdownListDto
    {
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
    }
}
