﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto
{
    public class AgreementApproveDto
    {
        public int  AgreementMasterId { get; set; }
        public string UserId { get; set; }

        public string ExportFile { get; set; }
    }
}
