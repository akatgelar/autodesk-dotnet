﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace autodesk.Code.Models.Dto.InvoiceDto
{
    public class AddInvoiceDto
    {
        [Required]
        public string OrgId { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDescription { get; set; }
        public string Territory { get; set; }
        public string Country { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string Summary { get; set; }

        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public string Status { get; set; }

        public string FinancialYear { get; set; }

        public List<ListPricingSKUDto> PricingSKUs { get; set; }

        public DiscountDto Discounts { get; set; }

    }
}
