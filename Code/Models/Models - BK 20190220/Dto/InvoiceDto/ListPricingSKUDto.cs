﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto.InvoiceDto
{
    public class ListPricingSKUDto
    {
        public int Id { get; set; }
        public string SKUName { get; set; }
        public string Distributor { get; set; }
        public string Description { get; set; }
        public int QTY { get; set; }
        public string License { get; set; }
        public string Price { get; set; }
        public string SiteId { get; set; }

        public string CountryCode { get; set; }
        public string Region { get; set; }
        public bool? IsForPrimarySite { get; set; }


    }
}
