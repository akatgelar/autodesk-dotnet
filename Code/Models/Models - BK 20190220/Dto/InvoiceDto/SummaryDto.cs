﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto.InvoiceDto
{
    public class SummaryDto
    {
        public string Distributor { get; set; }
        public int? QTY { get; set; }
        public double? TotalAmount { get; set; }
    }
}
