﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto.InvoiceDto
{
    public class ReportDto
    {
        public string OrgId { get; set; }
        public string OrgName { get; set; }
        public string SiteId { get; set; }
        public string SiteName { get; set; }
        public string SKUName { get; set; }
        public string SiteStatus { get; set; }
        public string TerritoryName { get; set; }
        //public string Country { get; set; }
        public string PartnerType { get; set; }
        public double Discount { get; set; }
        public string InvoiceStatus { get; set; }
        //public int? Quantity { get; set; }
        //public int SKUPriceBand { get; set; }
        //public int SKUSRP { get; set; }
        //public double SKUPriceInclDiscount { get; set; }
        //public double? SiteTotal { get; set; }
        //public double? OrgTotal { get; set; }
        //public double? Total { get; set; }
        public string FYIndicatorKey { get; set; }
        public int InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDescription { get; set; }
        public string CountriesName { get; set; }
        public int QTY { get; set; }
        public double TotalAmmount { get; set; }
        public string SKUDescription { get; set; }
        public string PriceBand { get; set; }
        //public string FinancialYear { get; set; }
        public int TerritoryId { get; set; }
        public string CountriesCode { get; set; }
        public int RoleId { get; set; }
        public int MarketTypeId { get; set; }
        public string GeoCode { get; set; }
        public string RegionCode { get; set; }
        public string SubregionCode { get; set; }
        public string DiscountType { get; set; }
        public int InvoiceSKUId { get; set; }

    }
}
