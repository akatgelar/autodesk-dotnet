﻿namespace autodesk.Code.Models.Dto.InvoiceDto
{
    public class InvoiceReportInputDto
    {
        public string fieldOrg { get; set; }
        public string anotherField { get; set; }
        public string invoicesField { get; set; }
        public string filterByInvoicesNumber { get; set; }
        public string filterByPOReceivedDate { get; set; }
        public string filterByPartnerType { get; set; }
        public string filterByPartnerTypeStatus { get; set; }
        public string filterByMarketType { get; set; }
        public string filterByGeo { get; set; }
        public string filterByRegion { get; set; }
        public string filterBySubRegion { get; set; }
        public string filterByTerritory { get; set; }
        public string filterByCountries { get; set; }
        public string FinancialYear { get; set; }
        public string OrgId { get; set; }
        public string Role { get; set; }
    }
}
