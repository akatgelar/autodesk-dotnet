﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public class Journals
    {
        public int JournalId { get;set;}
        public string ParentId { get; set; }
        public DateTime DateAdded { get; set; }
        public string AddedBy { get; set; }
        public int ActivityId { get; set; }
        public DateTime ActivityDate { get; set; }
        public string Notes { get; set; }
        public DateTime DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public char Status { get; set; }
        public string Guid { get; set; }
    }
}
