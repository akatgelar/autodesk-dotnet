﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class RefSubregion
    {
        public int SubregionId { get; set; }
        public string GeoCode { get; set; }
        public string GeoName { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public string SubregionCode { get; set; }
        public string SubregionName { get; set; }
        public string Cuid { get; set; }
        public DateTime? Cdate { get; set; }
        public string Muid { get; set; }
        public DateTime? Mdate { get; set; }
    }
}
