﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class InvoicesSites
    {
        public int InvoicesSitesId { get; set; }
        public int InvoiceId { get; set; }
        public string SiteId { get; set; }
        public string Status { get; set; }
    }
}
