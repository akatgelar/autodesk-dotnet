﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class LineItemSummaries
    {
        public int LineItemSummaryId { get; set; }
        public string ActivityType { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        public int? Quantity { get; set; }
        public decimal? UnitPrice { get; set; }
        public decimal? Vatrate { get; set; }
        public decimal? Vatamount { get; set; }
        public decimal? TotalAmount { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateAdded { get; set; }
        public string LastAdminBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string Status { get; set; }
    }
}
