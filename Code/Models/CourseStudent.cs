﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class CourseStudent
    {
        public int CourseStudentId { get; set; }
        public string CourseId { get; set; }
        public int StudentId { get; set; }
        public int? CourseTaken { get; set; }
        public int? SurveyTaken { get; set; }
        public string Status { get; set; }
    }
}
