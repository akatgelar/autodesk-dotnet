﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public partial class InvoicePricingSKU
    {
        public int Id { get; set; }
        public int? SiteCountryDistributorSKUId { get; set; }
        public string Territory { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public string Distributor { get; set; }
        public string Price { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
        public string SiteId { get; set; }
    }
}
