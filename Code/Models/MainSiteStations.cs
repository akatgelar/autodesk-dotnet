﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class MainSiteStations
    {
        public int StationInt { get; set; }
        public string SiteId { get; set; }
        public int Year { get; set; }
        public int StationsQ1 { get; set; }
        public int StationsQ2 { get; set; }
        public int StationsQ3 { get; set; }
        public int StationsQ4 { get; set; }
    }
}
