﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class AdministrativeUsersOld
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Status { get; set; }
        public string Level { get; set; }
        public string AdminSystems { get; set; }
        public short? TimeOut { get; set; }
        public DateTime? DateLastChange { get; set; }
        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public short? LoginCount { get; set; }
        public DateTime? LastLogin { get; set; }
        public string LastIpaddress { get; set; }
        public string ShowDuplicates { get; set; }
    }
}
