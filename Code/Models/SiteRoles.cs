﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SiteRoles
    {
        public int SiteRoleId { get; set; }
        public int? RoleId { get; set; }
        public string SiteId { get; set; }
        public string PartyId { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public DateTime? DateDeleted { get; set; }
        public string DeletedBy { get; set; }
        public string Csn { get; set; }
        public string ContractCsn { get; set; }
        public string ParentCsn { get; set; }
        public string ItsId { get; set; }
        public string NewParentCsn { get; set; }
        public string UparentCsn { get; set; }
        public string ExternalId { get; set; }
        public int? RoleParamId { get; set; }
        public DateTime? AccreditationDate { get; set; }
    }
}
