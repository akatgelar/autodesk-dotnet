﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public partial class AgreementRemark
    {
        public int AgreementRemarkId { get; set; }
        public int AgreementMasterId { get; set; }
        public string Remark { get; set; }
        public string RecordBy { get; set; }
        public DateTime RecordDateTime { get; set; }
        public string Action { get; set; }
    }
}
