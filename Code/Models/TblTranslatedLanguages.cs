﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class TblTranslatedLanguages
    {
        public int FromLanguageId { get; set; }
        public int ToLanguageId { get; set; }
        public string Text { get; set; }
    }
}
