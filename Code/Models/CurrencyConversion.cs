﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class CurrencyConversion
    {
        public int CurrencyConversionId { get; set; }
        public string Year { get; set; }
        public string Currency { get; set; }
        public string ValuePerUsd { get; set; }
        public string Status { get; set; }
    }
}
