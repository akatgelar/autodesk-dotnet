﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public class LanguageJsonDisplayModel
    {
        public string DisplayName { get; set; }
        public string LabelName { get; set; }
        public IList<SubModule> SubModuleses { get; set; }
    }

    public class SubModule
    {
        public string DisplayName { get; set; }
        public string LabelName { get; set; }
        public IList<NestedSubModule> NestedSubModules { get; set; }
    }

    public class NestedSubModule
    {
        public string DisplayName { get; set; }
        public string LabelName { get; set; }
    }
}
