﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class CourseSoftware
    {
        public int CourseSoftwareId { get; set; }
        public string CourseId { get; set; }
        public string ProductId { get; set; }
        public int? ProductVersionsId { get; set; }
        public string ProductsecondaryId { get; set; }
        public int? ProductVersionsSecondaryId { get; set; }
        public string Status { get; set; }
    }
}
