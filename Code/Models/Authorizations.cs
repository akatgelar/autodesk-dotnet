﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Authorizations
    {
        public int QualificationId { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string ContactId { get; set; }
        public string JobRole { get; set; }
        public DateTime? AuthorizationDate { get; set; }
        public string Industry { get; set; }
        public string Comments { get; set; }
    }
}
