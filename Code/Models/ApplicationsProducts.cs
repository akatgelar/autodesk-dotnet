﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class ApplicationsProducts
    {
        public int ApplicationId { get; set; }
        public string ProductId { get; set; }
    }
}
