﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SiebelContactEidmlinks
    {
        public string SiebelId { get; set; }
        public string ContactId { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
    }
}
