﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Applications
    {
        public int ApplicationId { get; set; }
        public string ContactId { get; set; }
        public string PrincipalProductId { get; set; }
        public int? NumberOfTeachingYears { get; set; }
        public int? TypeOfTrainingId { get; set; }
        public DateTime? SubmissionDate { get; set; }
        public int? CertificateId { get; set; }
        public DateTime? CertificateDate { get; set; }
        public string Status { get; set; }
        public DateTime? RejectedDate { get; set; }
        public string RejectedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
        public int? HowAciProgramWasFound { get; set; }
        public string ChangesReason { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? AcicertificationHistoryId { get; set; }
    }
}
