﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Duplicates
    {
        public string DuplicateType { get; set; }
        public string MasterId { get; set; }
        public string DuplicateId { get; set; }
    }
}
