﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class RoleParams
    {
        public long RoleParamId { get; set; }
        public string RoleCode { get; set; }
        public string ParamName { get; set; }
        public string ParamValue { get; set; }
        public string Status { get; set; }
    }
}
