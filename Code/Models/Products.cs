﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Products
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Year { get; set; }
        public string Status { get; set; }
    }
}
