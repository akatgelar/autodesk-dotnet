﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class CertificateBackground
    {
        public int CertificateBackgroundId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string FileLocation { get; set; }
        public string Status { get; set; }
    }
}
