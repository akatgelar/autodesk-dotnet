﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public partial class EvaluationReportFilter
    {
        public int Id { get; set; }
        public string FilterString { get; set; }
        public string Submiter { get; set; }
        public string ReportName { get; set; }
        public DateTime SumitedDate { get; set; }
        public string Status { get; set; }
        public DateTime LastUpdated { get; set; }
        public int PartnerType { get; set; }
    }
}
