﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SiteServices
    {
        public int SiteServiceId { get; set; }
        public string SiteId { get; set; }
        public int ServiceId { get; set; }
        public DateTime? SiteServiceDate { get; set; }
        public DateTime DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string Status { get; set; }
        public string Description { get; set; }
    }
}
