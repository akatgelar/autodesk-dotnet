﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Attachments
    {
        public int AttachmentId { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string ParentId { get; set; }
        public string AttachmentType { get; set; }
        public string Description { get; set; }
        public string Filename { get; set; }
        public int? Filesize { get; set; }
        public string Csn { get; set; }
    }
}
