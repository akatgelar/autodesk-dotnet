﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public partial class AgreementMaster
    {
        public int AgreementMasterId { get; set; }
        public string FyindicatorKey { get; set; }
        public string OrgId { get; set; }
        public string PartnerType { get; set; }
        public string Status { get; set; }
        public string LegalJobTitle { get; set; }
        public string LegalName { get; set; }
        public bool? CheckTc { get; set; }
        public string SubmissionPerson { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
