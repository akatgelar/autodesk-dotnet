﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Timezones
    {
        public string ZoneName { get; set; }
        public string Country { get; set; }
        public string RuleName { get; set; }
        public int GmtOffset { get; set; }
        public string Format { get; set; }
        public string StandardName { get; set; }
        public string DaylightName { get; set; }
        public string DisplayName { get; set; }
        public string SupportsDaylightSavingTime { get; set; }
        public int BaseUtcOffset { get; set; }
    }
}
