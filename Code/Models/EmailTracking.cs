﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public class EmailTracking
    {
        public int Id { get; set; }
        public string SendTo { get; set; }
        public string CC { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
        public string Result { get; set; }
        public DateTime? SendTime { get; set; }
        public DateTime? SendgridResponseTime { get; set; }

    }
}
