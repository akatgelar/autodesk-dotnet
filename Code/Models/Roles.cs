﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Roles
    {
        public int RoleId { get; set; }
        public string RoleCode { get; set; }
        public string RoleName { get; set; }
        public string SiebelName { get; set; }
        public string FrameworkName { get; set; }
        public string CrbapprovedName { get; set; }
        public string RoleType { get; set; }
        public string RoleSubType { get; set; }
        public string Description { get; set; }
    }
}
