﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class CourseTrainingMaterial
    {
        public int CourseTrainingMaterialId { get; set; }
        public string CourseId { get; set; }
        public int Autodesk { get; set; }
        public int Aap { get; set; }
        public int Atc { get; set; }
        public int Independent { get; set; }
        public int IndependentOnline { get; set; }
        public int Atconline { get; set; }
        public int Other { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
    }
}
