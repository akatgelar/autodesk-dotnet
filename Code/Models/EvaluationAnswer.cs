﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class EvaluationAnswer
    {
        public int EvaluationAnswerId { get; set; }
        public string EvaluationQuestionCode { get; set; }
        public string StudentEvaluationId { get; set; }
        public string CourseId { get; set; }
        public string CountryCode { get; set; }
        public int? StudentId { get; set; }
        public string EvaluationAnswerJson { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? Quarter { get; set; }
    }
}
