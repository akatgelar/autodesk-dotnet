﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public partial class Dictionary
    {
        public string Key { get; set; }
        public string Parent { get; set; }
        public string KeyValue { get; set; }
        public string Status { get; set; }
    }

}
