﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class ProductFamilies
    {
        public long ProductfamilyId { get; set; }
        public int ProductId { get; set; }
        public string FamilyId { get; set; }
        public string Year { get; set; }
        public string Status { get; set; }
    }
}
