﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace autodesk.Code.Models
{
    public partial class Autodesk_NewContext : DbContext
    {
        public Autodesk_NewContext()
        {
        }

        public Autodesk_NewContext(DbContextOptions<Autodesk_NewContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AcademicProject> AcademicProject { get; set; }
        public virtual DbSet<AcademicTargetProgram> AcademicTargetProgram { get; set; }
        public virtual DbSet<AdministrativeUsersOld> AdministrativeUsersOld { get; set; }
        public virtual DbSet<AgreementMaster> AgreementMaster { get; set; }
        public virtual DbSet<AgreementRemark> AgreementRemark { get; set; }
        public virtual DbSet<Applications> Applications { get; set; }
        public virtual DbSet<ApplicationsProducts> ApplicationsProducts { get; set; }
        public virtual DbSet<Attachments> Attachments { get; set; }
        public virtual DbSet<Authorizations> Authorizations { get; set; }
        public virtual DbSet<Certificate> Certificate { get; set; }
        public virtual DbSet<CertificateBackground> CertificateBackground { get; set; }
        public virtual DbSet<ContactNotes> ContactNotes { get; set; }
        public virtual DbSet<ContactsAll> ContactsAll { get; set; }
        public virtual DbSet<ContactsOld> ContactsOld { get; set; }
        public virtual DbSet<CourseSoftware> CourseSoftware { get; set; }
        public virtual DbSet<CourseStudent> CourseStudent { get; set; }
        public virtual DbSet<CourseTeachingLevel> CourseTeachingLevel { get; set; }
        public virtual DbSet<CourseTrainingFormat> CourseTrainingFormat { get; set; }
        public virtual DbSet<CourseTrainingMaterial> CourseTrainingMaterial { get; set; }
        public virtual DbSet<Courses> Courses { get; set; }
        public virtual DbSet<CurrencyConversion> CurrencyConversion { get; set; }
        public virtual DbSet<Dictionary> Dictionary { get; set; }
        public virtual DbSet<Duplicates> Duplicates { get; set; }
        public virtual DbSet<EmailBody> EmailBody { get; set; }
        public virtual DbSet<EmailBodyType> EmailBodyType { get; set; }
        public virtual DbSet<EvaluationAnswer> EvaluationAnswer { get; set; }
        public virtual DbSet<EvaluationQuestion> EvaluationQuestion { get; set; }
        public virtual DbSet<EvaluationReport> EvaluationReport { get; set; }
        public virtual DbSet<Glossary> Glossary { get; set; }
        public virtual DbSet<IndustryRoles> IndustryRoles { get; set; }
        public virtual DbSet<InstructorRequest> InstructorRequest { get; set; }
        public virtual DbSet<IntegrityOk> IntegrityOk { get; set; }
        public virtual DbSet<InvoiceDiscount> InvoiceDiscount { get; set; }
        public virtual DbSet<InvoicePricingSKU> InvoicePricingSku { get; set; }
        public virtual DbSet<InvoiceSku> InvoiceSku { get; set; }
        public virtual DbSet<InvoiceType> InvoiceType { get; set; }
        public virtual DbSet<Invoices> Invoices { get; set; }
        public virtual DbSet<InvoicesSites> InvoicesSites { get; set; }
        public virtual DbSet<JournalActivities> JournalActivities { get; set; }
        public virtual DbSet<Journals> Journals { get; set; }
        public virtual DbSet<LanguagePack> LanguagePack { get; set; }
        public virtual DbSet<LayoutLanguage> LayoutLanguage { get; set; }
        public virtual DbSet<LayoutLanguageEvaluation> LayoutLanguageEvaluation { get; set; }
        public virtual DbSet<LineItemSummaries> LineItemSummaries { get; set; }
        public virtual DbSet<LmscertificationHistory> LmscertificationHistory { get; set; }
        public virtual DbSet<LmscertificationHistoryFy11> LmscertificationHistoryFy11 { get; set; }
        public virtual DbSet<LmscontactRegistrationCounts> LmscontactRegistrationCounts { get; set; }
        public virtual DbSet<LmslanguageEquivalentCourses> LmslanguageEquivalentCourses { get; set; }
        public virtual DbSet<LmsspecializationHistory> LmsspecializationHistory { get; set; }
        public virtual DbSet<LoginHistory> LoginHistory { get; set; }
        public virtual DbSet<LookUp> LookUp { get; set; }
        public virtual DbSet<MainOrganization> MainOrganization { get; set; }
        public virtual DbSet<MainSite> MainSite { get; set; }
        public virtual DbSet<MainSiteStations> MainSiteStations { get; set; }
        public virtual DbSet<MarketType> MarketType { get; set; }
        public virtual DbSet<OrgSiteAttachment> OrgSiteAttachment { get; set; }
        public virtual DbSet<Partners> Partners { get; set; }
        public virtual DbSet<ProductFamilies> ProductFamilies { get; set; }
        public virtual DbSet<ProductVersions> ProductVersions { get; set; }
        public virtual DbSet<Products> Products { get; set; }
        public virtual DbSet<ProductsOriginal> ProductsOriginal { get; set; }
        public virtual DbSet<Qualifications> Qualifications { get; set; }
        public virtual DbSet<RefCountries> RefCountries { get; set; }
        public virtual DbSet<RefGeo> RefGeo { get; set; }
        public virtual DbSet<RefLanguage> RefLanguage { get; set; }
        public virtual DbSet<RefRegion> RefRegion { get; set; }
        public virtual DbSet<RefSubregion> RefSubregion { get; set; }
        public virtual DbSet<RefTerritory> RefTerritory { get; set; }
        public virtual DbSet<RoleCertificate> RoleCertificate { get; set; }
        public virtual DbSet<RoleCertificateType> RoleCertificateType { get; set; }
        public virtual DbSet<RoleParams> RoleParams { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Rooms> Rooms { get; set; }
        public virtual DbSet<SabaCompanies> SabaCompanies { get; set; }
        public virtual DbSet<SabaCompanyProfiles> SabaCompanyProfiles { get; set; }
        public virtual DbSet<SabaDomains> SabaDomains { get; set; }
        public virtual DbSet<SerialNumbers> SerialNumbers { get; set; }
        public virtual DbSet<ServiceProviderRoles> ServiceProviderRoles { get; set; }
        public virtual DbSet<SiebelContactCsnlinks> SiebelContactCsnlinks { get; set; }
        public virtual DbSet<SiebelContactEidmlinks> SiebelContactEidmlinks { get; set; }
        public virtual DbSet<SiebelSiteContactLinks> SiebelSiteContactLinks { get; set; }
        public virtual DbSet<SiteContactLinks> SiteContactLinks { get; set; }
        public virtual DbSet<SiteCountryDistributorSKU> SiteCountryDistributorSku { get; set; }
        public virtual DbSet<SiteRoleParams> SiteRoleParams { get; set; }
        public virtual DbSet<SiteRoles> SiteRoles { get; set; }
        public virtual DbSet<SiteService> SiteService { get; set; }
        public virtual DbSet<SiteServices> SiteServices { get; set; }
        public virtual DbSet<States> States { get; set; }
        public virtual DbSet<TblPostSurveyMatchQuestions> TblPostSurveyMatchQuestions { get; set; }
        public virtual DbSet<TblStudents> TblStudents { get; set; }
        public virtual DbSet<TblTranslatedLanguages> TblTranslatedLanguages { get; set; }
        public virtual DbSet<Timezones> Timezones { get; set; }
        public virtual DbSet<TmpDashboardMeetAnnual> TmpDashboardMeetAnnual { get; set; }
        public virtual DbSet<TmpSitesCoe> TmpSitesCoe { get; set; }
        public virtual DbSet<TypeOfTrainings> TypeOfTrainings { get; set; }
        public virtual DbSet<UserLevel> UserLevel { get; set; }
        public virtual DbSet<UserToken> UserToken { get; set; }
        public virtual DbSet<Versioncontrol> Versioncontrol { get; set; }

        // Unable to generate entity type for table 'Autodesk_New.ContactCountry'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.ContactIds'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.ContactServiceProviderRoles'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.ContactsOrganizations'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.eCompanyCountryMap'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.Family'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.GeoCountry'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.History'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.Industries'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.IndustryProducts'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.LMSCapacity'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.LMSCertificationActivities'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.LMSCourseAgeFeed'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.LMSRegistrations'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.LMSSpecializations'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.LMSTranscripts'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.LoginHistoryCSO'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.ODS_ContactHistory'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.OrgIds'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.SabaComponentIds'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.Saba_Certifications'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.Saba_ContactData'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.Saba_ContactLogins'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.Saba_Jobtypes'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.SiebelContactData'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.SiebelContacts'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.SiebelSiteContracts'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.SiebelSites'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.SiteCountryDistributor'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.SiteIds'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.TerritoryCountry'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.TimezoneDetail'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardAAP1'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardAAP2'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardAAP3'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardATC1'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardATC2'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardATC3'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardMonthCoursesDelivered'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardSitesParticipated1'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardSitesParticipated2'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardTopInstructors'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardTotalActiveInstructors'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardTotalActivePartner'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardTotalCourses'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardTotalNewPartner'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardTotalPerformanceSite'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpDashboardTotalSurveyTaken'. Please see the warning messages.
        // Unable to generate entity type for table 'Autodesk_New.tmpEvaSurveyTaken'. Please see the warning messages.

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySQL(@"server=42.61.88.131;port=3306;userid=Autodesk;password=Passw0rd456;database=Autodesk_New");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<AcademicProject>(entity =>
            {
                entity.HasKey(e => e.ProjectId);

                entity.ToTable("AcademicProject", "Autodesk_New");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_ATP_SiteId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_ATP_Status");

                entity.Property(e => e.ProjectId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasColumnType("char(2)");

                entity.Property(e => e.Institution)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductVersion)
                    .HasColumnName("productVersion")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TargetEducator).HasColumnType("smallint(5)");

                entity.Property(e => e.TargetStudent).HasColumnType("smallint(5)");

                entity.Property(e => e.Usage)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AcademicTargetProgram>(entity =>
            {
                entity.HasKey(e => e.AcdemicId);

                entity.ToTable("AcademicTargetProgram", "Autodesk_New");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_ATP_SiteId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_ATP_Status");

                entity.Property(e => e.AcdemicId).HasColumnType("int(10)");

                entity.Property(e => e.AcademicType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Csn)
                    .HasColumnName("CSN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Fyindicator)
                    .HasColumnName("FYIndicator")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TargetEducator).HasColumnType("smallint(5)");

                entity.Property(e => e.TargetInstitution).HasColumnType("smallint(5)");

                entity.Property(e => e.TargetStudent).HasColumnType("smallint(5)");

                entity.Property(e => e.Usage)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AdministrativeUsersOld>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.ToTable("AdministrativeUsers_old", "Autodesk_New");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_AU_Status");

                entity.Property(e => e.UserId)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AdminSystems)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastIpaddress)
                    .HasColumnName("LastIPAddress")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Level).HasColumnType("char(1)");

                entity.Property(e => e.LoginCount).HasColumnType("smallint(6)");

                entity.Property(e => e.Password)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ShowDuplicates)
                    .IsRequired()
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("N");

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TimeOut).HasColumnType("smallint(6)");

                entity.Property(e => e.UserEmail)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AgreementMaster>(entity =>
            {
                entity.ToTable("AgreementMaster", "Autodesk_New");

                entity.Property(e => e.AgreementMasterId)
                    .HasColumnName("AgreementMasterID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.CheckTc)
                    .HasColumnName("CheckTC")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.FyindicatorKey)
                    .IsRequired()
                    .HasColumnName("FYIndicatorKey")
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.LegalJobTitle)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.LegalName)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.OrgId)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerType)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SubmissionPerson)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<AgreementRemark>(entity =>
            {
                entity.ToTable("AgreementRemark", "Autodesk_New");

                entity.Property(e => e.AgreementRemarkId).HasColumnType("int(11)");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.AgreementMasterId).HasColumnType("int(11)");

                entity.Property(e => e.RecordBy)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.Remark)
                    .HasMaxLength(1000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Applications>(entity =>
            {
                entity.HasKey(e => e.ApplicationId);

                entity.ToTable("Applications", "Autodesk_New");

                entity.Property(e => e.ApplicationId).HasColumnType("int(11)");

                entity.Property(e => e.AcicertificationHistoryId)
                    .HasColumnName("ACICertificationHistoryId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ApprovedBy)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CertificateId).HasColumnType("int(11)");

                entity.Property(e => e.ChangesReason)
                    .HasMaxLength(1000)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.HowAciProgramWasFound).HasColumnType("int(11)");

                entity.Property(e => e.ModifiedBy)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfTeachingYears).HasColumnType("int(11)");

                entity.Property(e => e.PrincipalProductId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RejectedBy)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TypeOfTrainingId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<ApplicationsProducts>(entity =>
            {
                entity.HasKey(e => new { e.ApplicationId, e.ProductId });

                entity.ToTable("ApplicationsProducts", "Autodesk_New");

                entity.Property(e => e.ApplicationId).HasColumnType("int(11)");

                entity.Property(e => e.ProductId)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Attachments>(entity =>
            {
                entity.HasKey(e => e.AttachmentId);

                entity.ToTable("Attachments", "Autodesk_New");

                entity.HasIndex(e => e.ParentId)
                    .HasName("IDX_Attachments_ParentID");

                entity.Property(e => e.AttachmentId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AttachmentType)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Csn)
                    .IsRequired()
                    .HasColumnName("CSN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Filename)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Filesize).HasColumnType("int(10)");

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParentId)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");
            });

            modelBuilder.Entity<Authorizations>(entity =>
            {
                entity.HasKey(e => e.QualificationId);

                entity.ToTable("Authorizations", "Autodesk_New");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_QUALIFICATIONS_ContactId");

                entity.HasIndex(e => e.JobRole)
                    .HasName("IDX_QUALIFICATIONS_AutodeskProduct");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_QUALIFICATIONS_Status");

                entity.Property(e => e.QualificationId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AuthorizationDate).HasColumnType("date");

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Industry)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.JobRole)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");
            });

            modelBuilder.Entity<Certificate>(entity =>
            {
                entity.ToTable("Certificate", "Autodesk_New");

                entity.Property(e => e.CertificateId).HasColumnType("int(8)");

                entity.Property(e => e.CertificateBackgroundId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CertificateTypeId).HasColumnType("int(1)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Htmldesign)
                    .IsRequired()
                    .HasColumnName("HTMLDesign")
                    .HasColumnType("longtext");

                entity.Property(e => e.LanguageCertificate)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerTypeId).HasColumnType("int(2)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.Year)
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CertificateBackground>(entity =>
            {
                entity.ToTable("CertificateBackground", "Autodesk_New");

                entity.Property(e => e.CertificateBackgroundId).HasColumnType("int(8)");

                entity.Property(e => e.Code)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FileLocation)
                    .IsRequired()
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<ContactNotes>(entity =>
            {
                entity.HasKey(e => e.ContactNoteId);

                entity.ToTable("ContactNotes", "Autodesk_New");

                entity.Property(e => e.ContactNoteId).HasColumnType("int(11)");

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Content)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.EnteredBy)
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ContactsAll>(entity =>
            {
                entity.HasKey(e => e.ContactIdInt);

                entity.ToTable("Contacts_All", "Autodesk_New");

                entity.HasIndex(e => e.ContactId)
                    .HasName("ContactId");

                entity.HasIndex(e => e.ContactName)
                    .HasName("IDX_CONTACTS_ContactName");

                entity.HasIndex(e => e.CsouserUuid)
                    .HasName("IDX_CONTACT_UUID");

                entity.HasIndex(e => e.EmailAddress)
                    .HasName("IDX_CONTACT_EMAIL");

                entity.HasIndex(e => e.EmailAddress2)
                    .HasName("IDX_Contacts_EMAIL2");

                entity.HasIndex(e => e.EnglishContactName)
                    .HasName("IDX_Contacts_EngName");

                entity.HasIndex(e => e.FirstName)
                    .HasName("IDX_CONTACTS_FirstName");

                entity.HasIndex(e => e.InstructorId)
                    .HasName("IDX_Contacts_InstructorId");

                entity.HasIndex(e => e.InternalSales)
                    .HasName("InternalSales");

                entity.HasIndex(e => e.LastName).HasName("IDX_CONTACTS_LastName");

                entity.HasIndex(e => e.PrimarySiteId)
                    .HasName("IDX_Contacts_PrimarySiteId");

                entity.HasIndex(e => e.SiebelId)
                    .HasName("SiebelId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_CONTACTS_Status");

                entity.HasIndex(e => e.Username)
                    .HasName("IDX_CONTACTS_UNIQUE_USERNAME");

                entity.Property(e => e.ContactIdInt).HasColumnType("int(11)");

                //entity.Property(e => e.AccountStatus).HasColumnType("char(1)");

                entity.Property(e => e.AcimemberId)
                    .HasColumnName("ACIMemberId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Action)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Address1)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Address3)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.AdminSystems)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Administrator)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("N");

                entity.Property(e => e.Atcrole)
                    .HasColumnName("ATCRole")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("N");

                entity.Property(e => e.Bio)
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactName)
                    .HasMaxLength(121)
                    .IsUnicode(false);

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

                entity.Property(e => e.CsouserUuid)
                    .HasColumnName("CSOUserUUID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Csoversion)
                    .HasColumnName("CSOVersion")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Dedicated).HasColumnType("char(1)");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Designation)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DoNotEmail).HasColumnType("char(1)");

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress2)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress3)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress4)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EnglishContactName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExperiencedCompetitiveProducts).HasColumnType("char(1)");

                entity.Property(e => e.ExperiencedCompetitiveProductsList)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongAutodeskProducts)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongInIndustry)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongWithOrg)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.InstructorId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InternalSales)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastIpaddress)
                    .HasColumnName("LastIPAddress")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Level).HasColumnType("char(1)");

                entity.Property(e => e.LoginCount).HasColumnType("smallint(5)");

                entity.Property(e => e.Mobile1)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Mobile2)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.MobileCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordPlain)
                    .HasColumnName("password_plain")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PercentPostSales)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PercentPreSales)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PermissionsGroup)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryIndustryId)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryLanguage).HasColumnType("char(10)");

                entity.Property(e => e.PrimaryRoleExperience)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryRoleId).HasColumnType("int(10)");

                entity.Property(e => e.PrimarySiteId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrimarySubIndustryId)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ProfilePicture)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PwdchangeRequired)
                    .HasColumnName("PWDChangeRequired")
                    .HasColumnType("char(1)");

                entity.Property(e => e.QuestionnaireDate).HasColumnType("date");

                entity.Property(e => e.ResetToken)
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Salutation)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SecondaryLanguage).HasColumnType("char(10)");

                entity.Property(e => e.ShareEmail)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ShareMobile)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ShareTelephone)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.ShowDuplicates)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("N");

                entity.Property(e => e.ShowInSearch)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.SiebelId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelStatus)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.StateProvince)
                    .HasMaxLength(72)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.StatusEmailSend)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Telephone1)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone2)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.TelephoneCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TimeZoneUtc).HasColumnName("TimeZoneUTC");

                entity.Property(e => e.Timezone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TitlePosition)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.UserLevelId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.WebsiteUrl)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ContactsOld>(entity =>
            {
                entity.HasKey(e => e.ContactId);

                entity.ToTable("Contacts_old", "Autodesk_New");

                entity.HasIndex(e => e.ContactName)
                    .HasName("IDX_CONTACTS_ContactName");

                entity.HasIndex(e => e.CsouserUuid)
                    .HasName("IDX_CONTACT_UUID");

                entity.HasIndex(e => e.EmailAddress)
                    .HasName("IDX_CONTACT_EMAIL");

                entity.HasIndex(e => e.EmailAddress2)
                    .HasName("IDX_Contacts_EMAIL2");

                entity.HasIndex(e => e.EnglishContactName)
                    .HasName("IDX_Contacts_EngName");

                entity.HasIndex(e => e.FirstName)
                    .HasName("IDX_CONTACTS_FirstName");

                entity.HasIndex(e => e.InstructorId)
                    .HasName("IDX_Contacts_InstructorId");

                entity.HasIndex(e => e.InternalSales)
                    .HasName("InternalSales");

                entity.HasIndex(e => e.LastName)
                    .HasName("IDX_CONTACTS_LastName");

                entity.HasIndex(e => e.PrimarySiteId)
                    .HasName("IDX_Contacts_PrimarySiteId");

                entity.HasIndex(e => e.SiebelId)
                    .HasName("SiebelId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_CONTACTS_Status");

                entity.HasIndex(e => e.Username)
                    .HasName("IDX_CONTACTS_UNIQUE_USERNAME");

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Atcrole)
                    .IsRequired()
                    .HasColumnName("ATCRole")
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("N");

                entity.Property(e => e.ContactName)
                    .HasMaxLength(121)
                    .IsUnicode(false);

                entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

                entity.Property(e => e.CsouserUuid)
                    .HasColumnName("CSOUserUUID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Csoversion)
                    .HasColumnName("CSOVersion")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.Dedicated)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.Department)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DoNotEmail).HasColumnType("char(1)");

                entity.Property(e => e.EmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress2)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress3)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EmailAddress4)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.EnglishContactName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExperiencedCompetitiveProducts)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.ExperiencedCompetitiveProductsList)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongAutodeskProducts)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongInIndustry)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.HowLongWithOrg)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.InstructorId)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.InternalSales)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastIpaddress)
                    .HasColumnName("LastIPAddress")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LoginCount).HasColumnType("smallint(5)");

                entity.Property(e => e.Mobile1)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Mobile2)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.PercentPostSales)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PercentPreSales)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PermissionsGroup)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryIndustryId)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryLanguage).HasColumnType("char(10)");

                entity.Property(e => e.PrimaryRoleExperience)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryRoleId).HasColumnType("int(10)");

                entity.Property(e => e.PrimarySiteId)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PrimarySubIndustryId)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PwdchangeRequired)
                    .HasColumnName("PWDChangeRequired")
                    .HasColumnType("char(1)");

                entity.Property(e => e.QuestionnaireDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("2007-01-01");

                entity.Property(e => e.Salutation)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SecondaryLanguage).HasColumnType("char(10)");

                entity.Property(e => e.SiebelId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelStatus)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.Telephone1)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Telephone2)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.TimeZoneUtc).HasColumnName("TimeZoneUTC");

                entity.Property(e => e.Timezone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.TitlePosition)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Username)
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CourseSoftware>(entity =>
            {
                entity.ToTable("CourseSoftware", "Autodesk_New");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CourseId");

                entity.HasIndex(e => e.ProductId)
                    .HasName("IDX_ProductId");

                entity.HasIndex(e => e.ProductVersionsId)
                    .HasName("IDX_CS_PVId");

                entity.Property(e => e.CourseSoftwareId).HasColumnType("int(8)");

                entity.Property(e => e.CourseId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductVersionsId).HasColumnType("int(2)");

                entity.Property(e => e.ProductVersionsSecondaryId).HasColumnType("int(2)");

                entity.Property(e => e.ProductsecondaryId)
                    .HasColumnName("productsecondaryId")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<CourseStudent>(entity =>
            {
                entity.ToTable("CourseStudent", "Autodesk_New");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CourseId");

                entity.HasIndex(e => e.StudentId)
                    .HasName("IDX_StudentID");

                entity.Property(e => e.CourseStudentId).HasColumnType("int(8)");

                entity.Property(e => e.CourseId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CourseTaken).HasColumnType("int(2)");

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.StudentId)
                    .HasColumnName("StudentID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SurveyTaken).HasColumnType("int(2)");
            });

            modelBuilder.Entity<CourseTeachingLevel>(entity =>
            {
                entity.ToTable("CourseTeachingLevel", "Autodesk_New");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CourseId");

                entity.Property(e => e.CourseTeachingLevelId).HasColumnType("int(8)");

                entity.Property(e => e.Advanced).HasColumnType("int(2)");

                entity.Property(e => e.Comment)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CourseId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Customized).HasColumnType("int(2)");

                entity.Property(e => e.Essentials).HasColumnType("int(2)");

                entity.Property(e => e.Intermediate).HasColumnType("int(2)");

                entity.Property(e => e.Other).HasColumnType("int(2)");

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.Update).HasColumnType("int(2)");
            });

            modelBuilder.Entity<CourseTrainingFormat>(entity =>
            {
                entity.ToTable("CourseTrainingFormat", "Autodesk_New");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CourseId");

                entity.Property(e => e.CourseTrainingFormatId).HasColumnType("int(8)");

                entity.Property(e => e.CourseId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InstructorLed).HasColumnType("int(2)");

                entity.Property(e => e.Online).HasColumnType("int(2)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<CourseTrainingMaterial>(entity =>
            {
                entity.ToTable("CourseTrainingMaterial", "Autodesk_New");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CourseId");

                entity.Property(e => e.CourseTrainingMaterialId).HasColumnType("int(8)");

                entity.Property(e => e.Aap)
                    .HasColumnName("AAP")
                    .HasColumnType("int(2)");

                entity.Property(e => e.Atc)
                    .HasColumnName("ATC")
                    .HasColumnType("int(2)");

                entity.Property(e => e.Atconline)
                    .HasColumnName("ATCOnline")
                    .HasColumnType("int(2)");

                entity.Property(e => e.Autodesk).HasColumnType("int(2)");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CourseId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Independent).HasColumnType("int(2)");

                entity.Property(e => e.IndependentOnline).HasColumnType("int(2)");

                entity.Property(e => e.Other).HasColumnType("int(2)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<Courses>(entity =>
            {
                entity.HasKey(e => new { e.CourseId, e.ApplicationId });

                entity.ToTable("Courses", "Autodesk_New");

                entity.HasIndex(e => e.CompletionDate)
                    .HasName("IDX_CompletionDate");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_ContactId");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CourseId");

                entity.HasIndex(e => e.FyindicatorKey)
                    .HasName("IDX_FYIndicatorKey");

                entity.HasIndex(e => e.Name)
                    .HasName("IDX_CourseTitle");

                entity.HasIndex(e => e.PartnerType)
                    .HasName("IDX_PartnerType");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SiteId");

                entity.HasIndex(e => e.StartDate)
                    .HasName("IDX_StartDate");

                entity.Property(e => e.CourseId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ApplicationId)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Atccomputer)
                    .HasColumnName("ATCComputer")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.Atcfacility)
                    .HasColumnName("ATCFacility")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.CertificateId).HasColumnType("int(11)");

                entity.Property(e => e.CertificateNumber)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.CourseCode)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EvaluationQuestionId).HasColumnType("int(11)");

                entity.Property(e => e.EventType2)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.EventTypeKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FyindicatorKey)
                    .HasColumnName("FYIndicatorKey")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.HoursTraining)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HoursTrainingOther).HasColumnType("tinyint(4)");

                entity.Property(e => e.Institution)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InstitutionLogo)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LocationName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LogoName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LogoType)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerType)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.ProjectTypeKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TrainingTypeKey)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<CurrencyConversion>(entity =>
            {
                entity.ToTable("CurrencyConversion", "Autodesk_New");

                entity.Property(e => e.CurrencyConversionId).HasColumnType("int(8)");

                entity.Property(e => e.Currency)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.ValuePerUsd)
                    .HasColumnName("ValuePerUSD")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Dictionary>(entity =>
            {
                entity.HasKey(e => new { e.Key, e.Parent });

                entity.ToTable("Dictionary", "Autodesk_New");

                entity.Property(e => e.Key)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Parent)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.KeyValue).IsRequired();

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<Duplicates>(entity =>
            {
                entity.HasKey(e => new { e.DuplicateType, e.MasterId, e.DuplicateId });

                entity.ToTable("Duplicates", "Autodesk_New");

                entity.HasIndex(e => e.DuplicateId)
                    .HasName("IDX_DUPES_Duplicate");

                entity.HasIndex(e => e.DuplicateType)
                    .HasName("IDX_DUPES_Type");

                entity.HasIndex(e => e.MasterId)
                    .HasName("IDX_DUPES_Master");

                entity.Property(e => e.DuplicateType)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MasterId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DuplicateId)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmailBody>(entity =>
            {
                entity.HasKey(e => e.BodyId);

                entity.ToTable("EmailBody", "Autodesk_New");

                entity.Property(e => e.BodyId)
                    .HasColumnName("body_id")
                    .HasColumnType("int(2)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BodyEmail).HasColumnName("body_email");

                entity.Property(e => e.BodyType)
                    .HasColumnName("body_type")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Key)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Subject)
                    .HasColumnName("subject")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EmailBodyType>(entity =>
            {
                entity.ToTable("EmailBodyType", "Autodesk_New");

                entity.Property(e => e.EmailBodyTypeId).HasColumnType("int(11)");

                entity.Property(e => e.EmailBodyTypeName)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EvaluationAnswer>(entity =>
            {
                entity.ToTable("EvaluationAnswer", "Autodesk_New");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CourseId");

                entity.HasIndex(e => e.CreatedDate)
                    .HasName("IDX_CreatedDate");

                entity.HasIndex(e => e.EvaluationAnswerId)
                    .HasName("IDX_EvaluationAnswerId");

                entity.HasIndex(e => e.EvaluationQuestionCode)
                    .HasName("IDX_EvaAnsQCode");

                entity.HasIndex(e => e.StudentEvaluationId)
                    .HasName("IDX_StudentEvaluationID");

                entity.HasIndex(e => e.StudentId)
                    .HasName("IDX_StudentID");

                entity.Property(e => e.EvaluationAnswerId).HasColumnType("int(11)");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CourseId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EvaluationAnswerJson).HasColumnType("longtext");

                entity.Property(e => e.EvaluationQuestionCode)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Quarter).HasColumnType("int(1)");

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StudentEvaluationId)
                    .HasColumnName("StudentEvaluationID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.StudentId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<EvaluationQuestion>(entity =>
            {
                entity.ToTable("EvaluationQuestion", "Autodesk_New");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_EvaCourseId");

                entity.HasIndex(e => e.EvaluationQuestionCode)
                    .HasName("IDX_EvaQCode");

                entity.HasIndex(e => e.EvaluationQuestionId)
                    .HasName("IDX_EvaQId");

                entity.Property(e => e.EvaluationQuestionId).HasColumnType("int(11)");

                entity.Property(e => e.CertificateTypeId).HasColumnType("int(2)");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CourseId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EvaluationQuestionCode)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EvaluationQuestionJson).HasColumnType("longtext");

                entity.Property(e => e.EvaluationQuestionTemplate).HasColumnType("longtext");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("LanguageID")
                    .HasColumnType("int(2)");

                entity.Property(e => e.PartnerType)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EvaluationReport>(entity =>
            {
                entity.HasKey(e => new { e.EvaluationReportId, e.SiteId });

                entity.ToTable("EvaluationReport", "Autodesk_New");

                entity.HasIndex(e => e.CountryCode)
                    .HasName("IDX_CountryCode");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_CourseId");

                entity.HasIndex(e => e.EvaluationReportId)
                    .HasName("IDX_EvalId");

                entity.HasIndex(e => e.FyindicatorKey)
                    .HasName("IDX_FYIndicatorKey");

                entity.HasIndex(e => e.GeoCode)
                    .HasName("IDX_GeoCode");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IDX_RoleId");

                entity.HasIndex(e => e.ScoreOe)
                    .HasName("IDX_ScoreOE");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SiteId");

                entity.HasIndex(e => e.StudentId)
                    .HasName("IDX_StudentID");

                entity.Property(e => e.EvaluationReportId)
                    .HasColumnType("int(8)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CertificateType).HasColumnType("int(10)");

                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CourseId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.FyindicatorKey)
                    .HasColumnName("FYIndicatorKey")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.GeoCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Month).HasColumnType("int(2)");

                entity.Property(e => e.NbAnswerCcm)
                    .HasColumnName("NbAnswerCCM")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NbAnswerFr)
                    .HasColumnName("NbAnswerFR")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NbAnswerIq)
                    .HasColumnName("NbAnswerIQ")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NbAnswerOe)
                    .HasColumnName("NbAnswerOE")
                    .HasColumnType("int(11)");

                entity.Property(e => e.NbAnswerOp)
                    .HasColumnName("NbAnswerOP")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Quarter).HasColumnType("int(1)");

                entity.Property(e => e.RoleId).HasColumnType("int(10)");

                entity.Property(e => e.ScoreCcm)
                    .HasColumnName("ScoreCCM")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ScoreFr)
                    .HasColumnName("ScoreFR")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ScoreIq)
                    .HasColumnName("ScoreIQ")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ScoreOe)
                    .HasColumnName("ScoreOE")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ScoreOp)
                    .HasColumnName("ScoreOP")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.StudentId)
                    .IsRequired()
                    .HasColumnName("StudentID")
                    .HasMaxLength(11)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Glossary>(entity =>
            {
                entity.ToTable("Glossary", "Autodesk_New");

                entity.HasIndex(e => e.GlossaryTerm)
                    .HasName("IDX_INVOICETYPE_ActivityType");

                entity.Property(e => e.GlossaryId)
                    .HasColumnName("glossary_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Cdate)
                    .HasColumnName("cdate")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.GlossaryTerm)
                    .IsRequired()
                    .HasColumnName("glossary_term")
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasDefaultValueSql("Invoice");

                entity.Property(e => e.Mdate)
                    .HasColumnName("mdate")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<IndustryRoles>(entity =>
            {
                entity.HasKey(e => new { e.IndustryCode, e.RoleCode });

                entity.ToTable("IndustryRoles", "Autodesk_New");

                entity.Property(e => e.IndustryCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InstructorRequest>(entity =>
            {
                entity.ToTable("InstructorRequest", "Autodesk_New");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ContactId)
                    .HasColumnName("ContactID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DistributorId)
                    .HasColumnName("DistributorID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Reason)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ReasonType)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.RequestedSiteId)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.RequesterId)
                    .HasColumnName("RequesterID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(25)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<IntegrityOk>(entity =>
            {
                entity.HasKey(e => new { e.IntegrityCheck, e.MasterItemId, e.OtherItemId });

                entity.ToTable("IntegrityOk", "Autodesk_New");

                entity.HasIndex(e => e.IntegrityCheck)
                    .HasName("IDX_DUPES_Type");

                entity.HasIndex(e => e.MasterItemId)
                    .HasName("IDX_DUPES_Master");

                entity.HasIndex(e => e.OtherItemId)
                    .HasName("IDX_DUPES_Duplicate");

                entity.Property(e => e.IntegrityCheck)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MasterItemId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OtherItemId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes)
                    .IsRequired()
                    .HasMaxLength(5000)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoiceDiscount>(entity =>
            {
                entity.ToTable("InvoiceDiscount", "Autodesk_New");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.AmountType)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceId).HasColumnType("int(8)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoicePricingSKU>(entity =>
            {
                entity.ToTable("InvoicePricingSKU", "Autodesk_New");

                entity.HasIndex(e => e.SiteCountryDistributorSKUId)
                    .HasName("SiteCountryDistributorSKUId_idx");

                entity.HasIndex(e => e.SiteId)
                    .HasName("Pricing_SIte_ForeignKey_idx");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Country)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Distributor)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Price)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Region)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.SiteCountryDistributorSKUId)
                    .HasColumnName("SiteCountryDistributorSKUId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Territory)
                    .HasMaxLength(45)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoiceSku>(entity =>
            {
                entity.ToTable("InvoiceSKU", "Autodesk_New");

                entity.Property(e => e.InvoiceSkuid)
                    .HasColumnName("InvoiceSKUId")
                    .HasColumnType("int(10)");

                entity.Property(e => e.InvoiceId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.PricingSKUId)
                    .HasColumnName("PricingSKUId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Qty)
                    .HasColumnName("QTY")
                    .HasColumnType("int(10)");

                entity.Property(e => e.SiteCountryDistributorSkuid)
                    .HasColumnName("SiteCountryDistributorSKUId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<InvoiceType>(entity =>
            {
                entity.ToTable("Invoice_type", "Autodesk_New");

                entity.HasIndex(e => e.ActivityType)
                    .HasName("IDX_INVOICETYPE_ActivityType");

                entity.Property(e => e.InvoiceTypeId)
                    .HasColumnName("invoice_type_id")
                    .HasColumnType("int(8)");

                entity.Property(e => e.ActivityType)
                    .IsRequired()
                    .HasColumnName("activity_type")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("Invoice");

                entity.Property(e => e.Cdate)
                    .HasColumnName("cdate")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.ContractEndDate)
                    .HasColumnName("contract_end_date")
                    .HasColumnType("date");

                entity.Property(e => e.ContractStartDate)
                    .HasColumnName("contract_start_date")
                    .HasColumnType("date");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceType1)
                    .IsRequired()
                    .HasColumnName("invoice_type")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicedAmount)
                    .HasColumnName("invoiced_amount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.InvoicedCurrency)
                    .HasColumnName("invoiced_currency")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mdate)
                    .HasColumnName("mdate")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<Invoices>(entity =>
            {
                entity.HasKey(e => e.InvoiceId);

                entity.ToTable("Invoices", "Autodesk_New");

                entity.HasIndex(e => e.OrgId)
                    .HasName("IDX_INVOICES_OrgId");

                entity.Property(e => e.InvoiceId).HasColumnType("int(8)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AmountOutstanding).HasColumnType("decimal(20,2)");

                entity.Property(e => e.CcexpDate)
                    .HasColumnName("CCExpDate")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.CcexpDate2)
                    .HasColumnName("CCExpDate2")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Ccname)
                    .HasColumnName("CCName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Ccname2)
                    .HasColumnName("CCName2")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ContractEndDate).HasColumnType("date");

                entity.Property(e => e.ContractId)
                    .HasColumnName("ContractID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ContractStartDate).HasColumnType("date");

                entity.Property(e => e.ContractStatus)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.DistributorFee).HasColumnType("decimal(20,0)");

                entity.Property(e => e.DocuSign)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FinancialYear)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceDate).HasColumnType("date");

                entity.Property(e => e.InvoiceDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceNumber)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.InvoiceType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicedCurrency)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.L1Description)
                    .HasColumnName("L1_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L1Quantity)
                    .HasColumnName("L1_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L1Summary)
                    .HasColumnName("L1_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L1TotalAmount)
                    .HasColumnName("L1_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L1UnitPrice)
                    .HasColumnName("L1_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L1Vatamount)
                    .HasColumnName("L1_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L1Vatrate)
                    .HasColumnName("L1_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L2Description)
                    .HasColumnName("L2_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L2Quantity)
                    .HasColumnName("L2_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L2Summary)
                    .HasColumnName("L2_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L2TotalAmount)
                    .HasColumnName("L2_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L2UnitPrice)
                    .HasColumnName("L2_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L2Vatamount)
                    .HasColumnName("L2_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L2Vatrate)
                    .HasColumnName("L2_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L3Description)
                    .HasColumnName("L3_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L3Quantity)
                    .HasColumnName("L3_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L3Summary)
                    .HasColumnName("L3_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L3TotalAmount)
                    .HasColumnName("L3_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L3UnitPrice)
                    .HasColumnName("L3_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L3Vatamount)
                    .HasColumnName("L3_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L3Vatrate)
                    .HasColumnName("L3_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L4Description)
                    .HasColumnName("L4_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L4Quantity)
                    .HasColumnName("L4_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L4Summary)
                    .HasColumnName("L4_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L4TotalAmount)
                    .HasColumnName("L4_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L4UnitPrice)
                    .HasColumnName("L4_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L4Vatamount)
                    .HasColumnName("L4_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L4Vatrate)
                    .HasColumnName("L4_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L5Description)
                    .HasColumnName("L5_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L5Quantity)
                    .HasColumnName("L5_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L5Summary)
                    .HasColumnName("L5_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L5TotalAmount)
                    .HasColumnName("L5_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L5UnitPrice)
                    .HasColumnName("L5_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L5Vatamount)
                    .HasColumnName("L5_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L5Vatrate)
                    .HasColumnName("L5_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L6Description)
                    .HasColumnName("L6_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L6Quantity)
                    .HasColumnName("L6_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L6Summary)
                    .HasColumnName("L6_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L6TotalAmount)
                    .HasColumnName("L6_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L6UnitPrice)
                    .HasColumnName("L6_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L6Vatamount)
                    .HasColumnName("L6_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L6Vatrate)
                    .HasColumnName("L6_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L7Description)
                    .HasColumnName("L7_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L7Quantity)
                    .HasColumnName("L7_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L7Summary)
                    .HasColumnName("L7_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L7TotalAmount)
                    .HasColumnName("L7_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L7UnitPrice)
                    .HasColumnName("L7_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L7Vatamount)
                    .HasColumnName("L7_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L7Vatrate)
                    .HasColumnName("L7_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L8Description)
                    .HasColumnName("L8_Description")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L8Quantity)
                    .HasColumnName("L8_Quantity")
                    .HasColumnType("int(3)");

                entity.Property(e => e.L8Summary)
                    .HasColumnName("L8_Summary")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.L8TotalAmount)
                    .HasColumnName("L8_TotalAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L8UnitPrice)
                    .HasColumnName("L8_UnitPrice")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L8Vatamount)
                    .HasColumnName("L8_VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.L8Vatrate)
                    .HasColumnName("L8_VATRate")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfLicenses).HasColumnType("int(10)");

                entity.Property(e => e.OldContractId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.OrgId)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentAmount)
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.PaymentAmount2).HasColumnType("decimal(20,2)");

                entity.Property(e => e.PaymentCurrency)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentDate).HasColumnType("date");

                entity.Property(e => e.PaymentDate2).HasColumnType("date");

                entity.Property(e => e.PaymentMethod)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentMethod2)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentMonth).HasColumnType("int(2)");

                entity.Property(e => e.PaymentMonth2).HasColumnType("int(2)");

                entity.Property(e => e.PaymentReference)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PaymentReference2)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Poid)
                    .HasColumnName("POID")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.PoreceivedDate)
                    .HasColumnName("POReceivedDate")
                    .HasColumnType("date");

                entity.Property(e => e.SiteCountryDistributorSkuid)
                    .HasColumnName("SiteCountryDistributorSKUId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteIdList)
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TotalInvoicedAmount)
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("0.00");

                entity.Property(e => e.TotalLineAmount).HasColumnType("decimal(20,2)");

                entity.Property(e => e.TotalPaymentAmount).HasColumnType("decimal(20,2)");

                entity.Property(e => e.TotalVatamount)
                    .HasColumnName("TotalVATAmount")
                    .HasColumnType("decimal(20,2)")
                    .HasDefaultValueSql("0.00");
            });

            modelBuilder.Entity<InvoicesSites>(entity =>
            {
                entity.ToTable("InvoicesSites", "Autodesk_New");

                entity.Property(e => e.InvoicesSitesId).HasColumnType("int(8)");

                entity.Property(e => e.InvoiceId).HasColumnType("int(8)");

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<JournalActivities>(entity =>
            {
                entity.HasKey(e => e.ActivityId);

                entity.ToTable("JournalActivities", "Autodesk_New");

                entity.HasIndex(e => e.ActivityType)
                    .HasName("IDX_JOURNALACTIVITIES_ActivityType");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_JOURNALACTIVITIES_Status");

                entity.Property(e => e.ActivityId).HasColumnType("int(8)");

                entity.Property(e => e.ActivityName)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ActivityType)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IsUnique).HasColumnType("char(1)");

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<Journals>(entity =>
            {
                entity.HasKey(e => e.JournalId);

                entity.ToTable("Journals", "Autodesk_New");

                entity.HasIndex(e => e.ActivityId)
                    .HasName("IDX_JOURNALS_ActivityId");

                entity.HasIndex(e => e.Guid)
                    .HasName("IDX_JOURNALS_GUID");

                entity.HasIndex(e => e.Notes)
                    .HasName("IDX_JOURNALS_Notes");

                entity.HasIndex(e => e.ParentId)
                    .HasName("IDX_JOURNALS_ParentId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_JOURNALS_Status");

                entity.Property(e => e.JournalId).HasColumnType("int(8)");

                entity.Property(e => e.ActivityId)
                    .HasColumnType("int(8)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Guid)
                    .HasColumnName("GUID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Notes).HasColumnType("text");

                entity.Property(e => e.ParentId)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Status)
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("A");
            });

            modelBuilder.Entity<LanguagePack>(entity =>
            {
                entity.ToTable("LanguagePack", "Autodesk_New");

                entity.Property(e => e.LanguagePackId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageCountry)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageJson).HasColumnType("longtext");

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LayoutLanguage>(entity =>
            {
                entity.HasKey(e => e.LanguageId);

                entity.ToTable("Layout_language", "Autodesk_New");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("language_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Bahasa)
                    .HasColumnName("bahasa")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Chinese)
                    .HasColumnName("chinese")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.English)
                    .HasColumnName("english")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageCode)
                    .HasColumnName("language_code")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LayoutLanguageEvaluation>(entity =>
            {
                entity.HasKey(e => e.LanguageId);

                entity.ToTable("Layout_language_evaluation", "Autodesk_New");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("language_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Bahasa)
                    .HasColumnName("bahasa")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Chinese)
                    .HasColumnName("chinese")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.English)
                    .HasColumnName("english")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageCode)
                    .HasColumnName("language_code")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LineItemSummaries>(entity =>
            {
                entity.HasKey(e => e.LineItemSummaryId);

                entity.ToTable("LineItemSummaries", "Autodesk_New");

                entity.Property(e => e.LineItemSummaryId).HasColumnType("int(8)");

                entity.Property(e => e.ActivityType)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .HasDefaultValueSql("ATC Invoice");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasColumnType("mediumtext");

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Quantity).HasColumnType("int(6)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.Summary)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.TotalAmount).HasColumnType("decimal(20,2)");

                entity.Property(e => e.UnitPrice).HasColumnType("decimal(20,2)");

                entity.Property(e => e.Vatamount)
                    .HasColumnName("VATAmount")
                    .HasColumnType("decimal(20,2)");

                entity.Property(e => e.Vatrate)
                    .HasColumnName("VATRate")
                    .HasColumnType("decimal(20,2)");
            });

            modelBuilder.Entity<LmscertificationHistory>(entity =>
            {
                entity.HasKey(e => new { e.ContactId, e.Industry, e.RoleName });

                entity.ToTable("LMSCertificationHistory", "Autodesk_New");

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Industry)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.History)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.PdbdateChanged)
                    .HasColumnName("PDBDateChanged")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PdbdateCreated).HasColumnName("PDBDateCreated");

                entity.Property(e => e.RoleCode)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LmscertificationHistoryFy11>(entity =>
            {
                entity.HasKey(e => new { e.ContactId, e.Industry, e.RoleName });

                entity.ToTable("LMSCertificationHistory_FY11", "Autodesk_New");

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Industry)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.History)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.PdbdateChanged)
                    .HasColumnName("PDBDateChanged")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PdbdateCreated).HasColumnName("PDBDateCreated");
            });

            modelBuilder.Entity<LmscontactRegistrationCounts>(entity =>
            {
                entity.HasKey(e => new { e.ContactId, e.Progress });

                entity.ToTable("LMSContactRegistrationCounts", "Autodesk_New");

                entity.Property(e => e.ContactId)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Progress)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CourseCount)
                    .HasColumnType("bigint(21)")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<LmslanguageEquivalentCourses>(entity =>
            {
                entity.HasKey(e => e.EquivalentCode);

                entity.ToTable("LMSLanguageEquivalentCourses", "Autodesk_New");

                entity.HasIndex(e => e.Guid)
                    .HasName("IDX_LMSLEC_GUID");

                entity.HasIndex(e => new { e.LeparentCode, e.Langugage })
                    .HasName("LEParentCode");

                entity.Property(e => e.EquivalentCode)
                    .HasMaxLength(40)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.EquivalentTitle)
                    .IsRequired()
                    .HasMaxLength(400)
                    .IsUnicode(false);

                entity.Property(e => e.Guid)
                    .IsRequired()
                    .HasColumnName("GUID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Langugage)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.LeparentCode)
                    .IsRequired()
                    .HasColumnName("LEParentCode")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.LeparentTitle)
                    .IsRequired()
                    .HasColumnName("LEParentTitle")
                    .HasMaxLength(400)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LmsspecializationHistory>(entity =>
            {
                entity.HasKey(e => new { e.ContactId, e.Industry, e.RoleName });

                entity.ToTable("LMSSpecializationHistory", "Autodesk_New");

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Industry)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.History)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.PdbdateChanged).HasColumnName("PDBDateChanged");

                entity.Property(e => e.PdbdateCreated).HasColumnName("PDBDateCreated");
            });

            modelBuilder.Entity<LoginHistory>(entity =>
            {
                entity.HasKey(e => e.DbId);

                entity.ToTable("LoginHistory", "Autodesk_New");

                entity.HasIndex(e => e.Action)
                    .HasName("IDX_LOGINH_Action");

                entity.HasIndex(e => e.Application)
                    .HasName("IDX_LOGINH_App");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_LOGINH_ContactId");

                entity.Property(e => e.DbId)
                    .HasColumnName("dbId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Application)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LookUp>(entity =>
            {
                entity.HasKey(e => e.ControlName);

                entity.ToTable("LookUp", "Autodesk_New");

                entity.Property(e => e.ControlName)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DataValue).HasColumnType("int(11)");

                entity.Property(e => e.Description)
                    .HasMaxLength(300)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MainOrganization>(entity =>
            {
                entity.HasKey(e => new { e.OrganizationId, e.OrgId });

                entity.ToTable("Main_organization", "Autodesk_New");

                entity.HasIndex(e => e.AtcorgId)
                    .HasName("IDX_ORGANIZATIONS_ATCID");

                entity.HasIndex(e => e.CommercialOrgName)
                    .HasName("IDX_Org_CommName");

                entity.HasIndex(e => e.CsoresellerUuid)
                    .HasName("IDX_ORGANIZATIONS_UUID");

                entity.HasIndex(e => e.EnglishOrgName)
                    .HasName("IDX_Org_EngName");

                entity.HasIndex(e => e.OrgId)
                    .HasName("OrgId");

                entity.HasIndex(e => e.OrgName)
                    .HasName("IDX_ORGANIZATIONS_OrgName");

                entity.HasIndex(e => e.OrgStatusRetired)
                    .HasName("IDX_ORG_OrgStatus");

                entity.HasIndex(e => e.RegisteredCountryCode)
                    .HasName("IDX_ORGANIZATIONS_CountryCode");

                entity.HasIndex(e => e.RegisteredStateProvince)
                    .HasName("IDX_Org_State");

                entity.HasIndex(e => e.SapsoldTo)
                    .HasName("IDX_ORGANIZATIONS_SAP");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_ORGANIZATIONS_Status");

                entity.Property(e => e.OrganizationId)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.OrgId)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Atccsn)
                    .HasColumnName("ATCCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorEmailAddress)
                    .HasColumnName("ATCDirectorEmailAddress")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorFax)
                    .HasColumnName("ATCDirectorFax")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorFirstName)
                    .HasColumnName("ATCDirectorFirstName")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorLastName)
                    .HasColumnName("ATCDirectorLastName")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.AtcdirectorTelephone)
                    .HasColumnName("ATCDirectorTelephone")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.AtcorgId)
                    .HasColumnName("ATCOrgId")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Aupcsn)
                    .HasColumnName("AUPCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactFax)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactFirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactLastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CommercialOrgName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ContractAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContractAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContractAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ContractDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.ContractPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ContractStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CsopartnerManager)
                    .HasColumnName("CSOPartnerManager")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CsoresellerName)
                    .HasColumnName("CSOResellerName")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CsoresellerUuid)
                    .HasColumnName("CSOResellerUUID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

                entity.Property(e => e.Csoversion)
                    .HasColumnName("CSOVersion")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.EnglishOrgName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.InvoicingStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactFax)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactFirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactLastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.LegalContactTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.OrgName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OrgStatusRetired)
                    .HasColumnName("OrgStatus_retired")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.OrgWebAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.PrimarySiteId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.RegisteredStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SapsoldTo)
                    .HasColumnName("SAPSoldTo")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.TaxExempt).HasColumnType("char(1)");

                entity.Property(e => e.Varcsn)
                    .HasColumnName("VARCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Vatnumber)
                    .HasColumnName("VATNumber")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.YearJoined)
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MainSite>(entity =>
            {
                entity.HasKey(e => new { e.SiteIdInt, e.SiteId });

                entity.ToTable("Main_site", "Autodesk_New");

                entity.HasIndex(e => e.AtcsiteId)
                    .HasName("IDX_SITES_ATCID");

                entity.HasIndex(e => e.CommercialSiteName)
                    .HasName("IDX_Sites_CommName");

                entity.HasIndex(e => e.CsolocationUuid)
                    .HasName("IDX_SITES_LOCUUID");

                entity.HasIndex(e => e.EnglishSiteName)
                    .HasName("IDX_Sites_EngName");

                entity.HasIndex(e => e.OrgId)
                    .HasName("IDX_SITES_OrgId");

                entity.HasIndex(e => e.SiebelSiteName)
                    .HasName("SiebelSiteName");

                entity.HasIndex(e => e.SiteAddress1)
                    .HasName("IDX_Sites_SiteAddr1");

                entity.HasIndex(e => e.SiteCountryCode)
                    .HasName("IDX_SITES_CountryCode");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SiteId");

                entity.HasIndex(e => e.SiteName)
                    .HasName("IDX_SITES_SiteName");

                entity.HasIndex(e => e.SiteStateProvince)
                    .HasName("IDX_Site_State");

                entity.HasIndex(e => e.SiteStatusRetired)
                    .HasName("IDX_Sites_SiteStatus");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_SITES_Status");

                entity.Property(e => e.SiteIdInt)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AtcsiteId)
                    .HasColumnName("ATCSiteId")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CommercialSiteName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CsoauthorizationCodes)
                    .HasColumnName("CSOAuthorizationCodes")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CsolocationName)
                    .HasColumnName("CSOLocationName")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CsolocationNumber)
                    .HasColumnName("CSOLocationNumber")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.CsolocationUuid)
                    .HasColumnName("CSOLocationUUID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.CsopartnerType)
                    .HasColumnName("CSOpartner_type")
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.CsoupdatedOn).HasColumnName("CSOUpdatedOn");

                entity.Property(e => e.Csoversion)
                    .HasColumnName("CSOVersion")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.EnglishSiteName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MagellanId)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MailingAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MailingAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MailingAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MailingCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.MailingCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.MailingDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.MailingPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.MailingStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.OrgId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SapnumberRetired)
                    .HasColumnName("SAPNumber_retired")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.SapshipToRetired)
                    .HasColumnName("SAPShipTo_retired")
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.ShippingStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelCountryCode)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelPostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelSiteName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAddress1)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAddress2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAddress3)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminFax)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminFirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminLastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteAdminTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SiteCity)
                    .HasMaxLength(80)
                    .IsUnicode(false);

                entity.Property(e => e.SiteCountryCode).HasColumnType("char(2)");

                entity.Property(e => e.SiteDepartment)
                    .HasMaxLength(180)
                    .IsUnicode(false);

                entity.Property(e => e.SiteEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteFax)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerEmailAddress)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerFax)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerFirstName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerLastName)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SiteManagerTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SiteName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SitePostalCode)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.SiteStateProvince)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiteStatusRetired)
                    .HasColumnName("SiteStatus_retired")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SiteTelephone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SiteWebAddress)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.Workstations)
                    .HasColumnType("int(8)")
                    .HasDefaultValueSql("10");
            });

            modelBuilder.Entity<MainSiteStations>(entity =>
            {
                entity.HasKey(e => e.StationInt);

                entity.ToTable("Main_site_stations", "Autodesk_New");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SiteId");

                entity.HasIndex(e => e.StationInt)
                    .HasName("IDX_StationInt");

                entity.HasIndex(e => e.Year)
                    .HasName("IDX_Year");

                entity.Property(e => e.StationInt).HasColumnType("int(11)");

                entity.Property(e => e.SiteId)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.StationsQ1).HasColumnType("int(11)");

                entity.Property(e => e.StationsQ2).HasColumnType("int(11)");

                entity.Property(e => e.StationsQ3).HasColumnType("int(11)");

                entity.Property(e => e.StationsQ4).HasColumnType("int(11)");

                entity.Property(e => e.Year).HasColumnType("int(11)");
            });

            modelBuilder.Entity<MarketType>(entity =>
            {
                entity.ToTable("MarketType", "Autodesk_New");

                entity.Property(e => e.MarketTypeId).HasColumnType("int(11)");

                entity.Property(e => e.MarketType1)
                    .HasColumnName("MarketType")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<OrgSiteAttachment>(entity =>
            {
                entity.ToTable("OrgSiteAttachment", "Autodesk_New");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Content)
                    .IsRequired()
                    .HasColumnType("mediumblob");

                entity.Property(e => e.DocType)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Metadata)
                    .HasMaxLength(45)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.OrgId)
                    .HasColumnName("OrgID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasColumnName("SiteID")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Partners>(entity =>
            {
                entity.HasKey(e => e.PartnerCode);

                entity.ToTable("Partners", "Autodesk_New");

                entity.Property(e => e.PartnerCode)
                    .HasMaxLength(200)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AssertionConsumerServiceUrl)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.CertificateFilePath)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.EncryptAssertion).HasColumnType("bit(1)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SignSamlresponse)
                    .HasColumnName("SignSAMLResponse")
                    .HasColumnType("bit(1)");

                entity.Property(e => e.SingleLogoutServiceUrl)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Url)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.WantAuthnRequestSigned).HasColumnType("bit(1)");
            });

            modelBuilder.Entity<ProductFamilies>(entity =>
            {
                entity.HasKey(e => e.ProductfamilyId);

                entity.ToTable("ProductFamilies", "Autodesk_New");

                entity.Property(e => e.ProductfamilyId)
                    .HasColumnName("productfamilyId")
                    .HasColumnType("bigint(20)");

                entity.Property(e => e.FamilyId)
                    .IsRequired()
                    .HasColumnName("familyId")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductVersions>(entity =>
            {
                entity.ToTable("ProductVersions", "Autodesk_New");

                entity.HasIndex(e => e.ProductId)
                    .HasName("IDX_PrdId");

                entity.HasIndex(e => e.ProductVersionsId)
                    .HasName("IDX_PVId");

                entity.Property(e => e.ProductVersionsId).HasColumnType("int(11)");

                entity.Property(e => e.DisplayOrder).HasColumnType("int(11)");

                entity.Property(e => e.Os)
                    .IsRequired()
                    .HasColumnName("OS")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.Version)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Products>(entity =>
            {
                entity.HasKey(e => e.ProductId);

                entity.ToTable("Products", "Autodesk_New");

                entity.HasIndex(e => e.ProductId)
                    .HasName("IDX_ProdID");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ProductCode)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasColumnName("productName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("A");

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProductsOriginal>(entity =>
            {
                entity.HasKey(e => e.ProductId);

                entity.ToTable("ProductsOriginal", "Autodesk_New");

                entity.HasIndex(e => e.FamilyId)
                    .HasName("familyId");

                entity.Property(e => e.ProductId)
                    .HasColumnName("productId")
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.FamilyId)
                    .IsRequired()
                    .HasColumnName("familyId")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.ProductName)
                    .IsRequired()
                    .HasColumnName("productName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)")
                    .HasDefaultValueSql("A");
            });

            modelBuilder.Entity<Qualifications>(entity =>
            {
                entity.HasKey(e => e.QualificationId);

                entity.ToTable("Qualifications", "Autodesk_New");

                entity.HasIndex(e => e.AutodeskProduct)
                    .HasName("IDX_QUALIFICATIONS_AutodeskProduct");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_QUALIFICATIONS_ContactId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_QUALIFICATIONS_Status");

                entity.Property(e => e.QualificationId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AutodeskProduct)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.AutodeskProductVersion)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Guid)
                    .IsRequired()
                    .HasColumnName("GUID")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.QualificationDate).HasColumnType("date");

                entity.Property(e => e.QualificationType)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");
            });

            modelBuilder.Entity<RefCountries>(entity =>
            {
                entity.HasKey(e => new { e.CountriesId, e.CountriesCode });

                entity.ToTable("Ref_countries", "Autodesk_New");

                entity.HasIndex(e => e.CountriesCode)
                    .HasName("IDX_Countries_Code");

                entity.HasIndex(e => e.CountriesName)
                    .HasName("IDX_Countries_Country");

                entity.HasIndex(e => e.GeoCode)
                    .HasName("IDX_Countries_Geo");

                entity.HasIndex(e => e.RegionCode)
                    .HasName("IDX_Countries_Region");

                entity.HasIndex(e => e.SubregionCode)
                    .HasName("IDX_Countries_Subregion");

                entity.HasIndex(e => e.TerritoryId)
                    .HasName("IDX_TerritoryId");

                entity.Property(e => e.CountriesId)
                    .HasColumnName("countries_id")
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.CountriesCode)
                    .HasColumnName("countries_code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.CountriesName)
                    .HasColumnName("countries_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CountriesTel)
                    .IsRequired()
                    .HasColumnName("countries_tel")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Embargoed)
                    .HasColumnType("tinyint(1)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GeoCode)
                    .HasColumnName("geo_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GeoName)
                    .IsRequired()
                    .HasColumnName("geo_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.MarketTypeId)
                    .HasColumnType("int(2)")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MultiplierOverride).HasColumnType("int(255)");

                entity.Property(e => e.RegionCode)
                    .HasColumnName("region_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RegionName)
                    .IsRequired()
                    .HasColumnName("region_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SubregionCode)
                    .IsRequired()
                    .HasColumnName("subregion_code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SubregionName)
                    .IsRequired()
                    .HasColumnName("subregion_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<RefGeo>(entity =>
            {
                entity.HasKey(e => e.GeoId);

                entity.ToTable("Ref_geo", "Autodesk_New");

                entity.Property(e => e.GeoId)
                    .HasColumnName("geo_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GeoCode)
                    .IsRequired()
                    .HasColumnName("geo_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GeoName)
                    .IsRequired()
                    .HasColumnName("geo_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RefLanguage>(entity =>
            {
                entity.HasKey(e => e.LanguageId);

                entity.ToTable("Ref_language", "Autodesk_New");

                entity.HasIndex(e => e.Uiculture)
                    .HasName("IX_tblLanguages");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("LanguageID")
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.AskInfo).HasColumnType("tinyint(4)");

                entity.Property(e => e.EnabledInReports).HasColumnType("tinyint(4)");

                entity.Property(e => e.EnabledInSurvey).HasColumnType("tinyint(4)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false);

                entity.Property(e => e.PostSurveyEnabled).HasColumnType("tinyint(4)");

                entity.Property(e => e.ReportOrder).HasColumnType("int(11)");

                entity.Property(e => e.Uiculture)
                    .IsRequired()
                    .HasColumnName("UICulture")
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RefRegion>(entity =>
            {
                entity.HasKey(e => e.RegionId);

                entity.ToTable("Ref_region", "Autodesk_New");

                entity.Property(e => e.RegionId)
                    .HasColumnName("region_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GeoCode)
                    .HasColumnName("geo_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegionCode)
                    .HasColumnName("region_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RegionName)
                    .HasColumnName("region_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RefSubregion>(entity =>
            {
                entity.HasKey(e => e.SubregionId);

                entity.ToTable("Ref_subregion", "Autodesk_New");

                entity.Property(e => e.SubregionId)
                    .HasColumnName("subregion_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GeoCode)
                    .HasColumnName("geo_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.GeoName)
                    .IsRequired()
                    .HasColumnName("geo_name")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RegionCode)
                    .HasColumnName("region_code")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RegionName)
                    .IsRequired()
                    .HasColumnName("region_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SubregionCode)
                    .IsRequired()
                    .HasColumnName("subregion_code")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.SubregionName)
                    .IsRequired()
                    .HasColumnName("subregion_name")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RefTerritory>(entity =>
            {
                entity.HasKey(e => e.TerritoryId);

                entity.ToTable("Ref_territory", "Autodesk_New");

                entity.HasIndex(e => e.TerritoryId)
                    .HasName("INDEX_territory_id");

                entity.HasIndex(e => e.TerritoryName)
                    .HasName("INDEX_territory_name");

                entity.Property(e => e.TerritoryId).HasColumnType("int(11)");

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Mdate).HasColumnName("mdate");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TerritoryName)
                    .IsRequired()
                    .HasColumnName("Territory_Name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoleCertificate>(entity =>
            {
                entity.ToTable("RoleCertificate", "Autodesk_New");

                entity.HasIndex(e => e.CertificateName)
                    .HasName("IDX_RoleParams_ParamName");

                entity.HasIndex(e => e.RoleCode)
                    .HasName("IDX_RoleParams_RoleCode");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_RoleParams_Status");

                entity.Property(e => e.RoleCertificateId).HasColumnType("bigint(20)");

                entity.Property(e => e.CertificateName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CertificateType)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.RoleCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoleCertificateType>(entity =>
            {
                entity.ToTable("RoleCertificateType", "Autodesk_New");

                entity.Property(e => e.RoleCertificateTypeId).HasColumnType("int(11)");

                entity.Property(e => e.RoleCertificateType1)
                    .HasColumnName("RoleCertificateType")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoleParams>(entity =>
            {
                entity.HasKey(e => e.RoleParamId);

                entity.ToTable("RoleParams", "Autodesk_New");

                entity.HasIndex(e => e.ParamName)
                    .HasName("IDX_RoleParams_ParamName");

                entity.HasIndex(e => e.RoleCode)
                    .HasName("IDX_RoleParams_RoleCode");

                entity.HasIndex(e => e.RoleParamId)
                    .HasName("IDX_RoleParamId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_RoleParams_Status");

                entity.Property(e => e.RoleParamId).HasColumnType("bigint(20)");

                entity.Property(e => e.ParamName)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.ParamValue)
                    .IsRequired()
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.RoleCode)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.HasKey(e => e.RoleId);

                entity.ToTable("Roles", "Autodesk_New");

                entity.HasIndex(e => e.RoleCode)
                    .HasName("IDX_Roles_RoleCode");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IDX_Roles_RoleId");

                entity.HasIndex(e => e.RoleName)
                    .HasName("IDX_Roles_RoleName");

                entity.Property(e => e.RoleId).HasColumnType("int(10)");

                entity.Property(e => e.CrbapprovedName)
                    .IsRequired()
                    .HasColumnName("CRBApprovedName")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FrameworkName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RoleCode)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RoleSubType)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RoleType)
                    .IsRequired()
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.SiebelName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Rooms>(entity =>
            {
                entity.HasKey(e => e.RoomId);

                entity.ToTable("Rooms", "Autodesk_New");

                entity.Property(e => e.RoomId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.FacilityId)
                    .HasColumnType("int(10)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfSeats)
                    .HasColumnType("int(4)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.RoomName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<SabaCompanies>(entity =>
            {
                entity.ToTable("Saba_Companies", "Autodesk_New");

                entity.HasIndex(e => e.Custom0)
                    .HasName("IDX_SBCMP_CUSTOM0");

                entity.HasIndex(e => e.Custom3)
                    .HasName("IDX_SBCMP_CUSTOM3");

                entity.HasIndex(e => e.Custom4)
                    .HasName("IDX_SBCMP_CUSTOM4");

                entity.HasIndex(e => e.Custom5)
                    .HasName("IDX_SBCMP_CUSTOM5");

                entity.HasIndex(e => e.ParentId)
                    .HasName("IDX_SBCMP_PARENT_ID");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AaId)
                    .IsRequired()
                    .HasColumnName("AA_ID")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.BusinessTyp)
                    .IsRequired()
                    .HasColumnName("BUSINESS_TYP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CiName)
                    .IsRequired()
                    .HasColumnName("CI_NAME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CiName2)
                    .IsRequired()
                    .HasColumnName("CI_NAME2")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.CompanyType)
                    .IsRequired()
                    .HasColumnName("COMPANY_TYPE")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .IsRequired()
                    .HasColumnName("CONTACT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CostCenter)
                    .IsRequired()
                    .HasColumnName("COST_CENTER")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn)
                    .IsRequired()
                    .HasColumnName("CREATED_ON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom0)
                    .IsRequired()
                    .HasColumnName("CUSTOM0")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom1)
                    .IsRequired()
                    .HasColumnName("CUSTOM1")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Custom2)
                    .IsRequired()
                    .HasColumnName("CUSTOM2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom3)
                    .IsRequired()
                    .HasColumnName("CUSTOM3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom4)
                    .IsRequired()
                    .HasColumnName("CUSTOM4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom5)
                    .IsRequired()
                    .HasColumnName("CUSTOM5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom6)
                    .IsRequired()
                    .HasColumnName("CUSTOM6")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom7)
                    .IsRequired()
                    .HasColumnName("CUSTOM7")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom8)
                    .IsRequired()
                    .HasColumnName("CUSTOM8")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom9)
                    .IsRequired()
                    .HasColumnName("CUSTOM9")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("EMAIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fax)
                    .IsRequired()
                    .HasColumnName("FAX")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Flags)
                    .IsRequired()
                    .HasColumnName("FLAGS")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LevelNo)
                    .IsRequired()
                    .HasColumnName("LEVEL_NO")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.MaxDiscount)
                    .IsRequired()
                    .HasColumnName("MAX_DISCOUNT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Name2)
                    .IsRequired()
                    .HasColumnName("NAME2")
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ParentId)
                    .IsRequired()
                    .HasColumnName("PARENT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone1)
                    .IsRequired()
                    .HasColumnName("PHONE1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Phone2)
                    .IsRequired()
                    .HasColumnName("PHONE2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SalesrepId)
                    .IsRequired()
                    .HasColumnName("SALESREP_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SecKey)
                    .IsRequired()
                    .HasColumnName("SEC_KEY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Split)
                    .IsRequired()
                    .HasColumnName("SPLIT")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StoreId)
                    .IsRequired()
                    .HasColumnName("STORE_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeStamp)
                    .IsRequired()
                    .HasColumnName("TIME_STAMP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("UPDATED_BY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn)
                    .IsRequired()
                    .HasColumnName("UPDATED_ON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WebServer)
                    .IsRequired()
                    .HasColumnName("WEB_SERVER")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SabaCompanyProfiles>(entity =>
            {
                entity.ToTable("Saba_CompanyProfiles", "Autodesk_New");

                entity.HasIndex(e => e.Country)
                    .HasName("IDX_SBCMPP_COUNTRY");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Addr1)
                    .IsRequired()
                    .HasColumnName("ADDR1")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Addr2)
                    .IsRequired()
                    .HasColumnName("ADDR2")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.BillingAddr1)
                    .IsRequired()
                    .HasColumnName("BILLING_ADDR1")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.BillingAddr2)
                    .IsRequired()
                    .HasColumnName("BILLING_ADDR2")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.BillingCity)
                    .IsRequired()
                    .HasColumnName("BILLING_CITY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillingContactId)
                    .IsRequired()
                    .HasColumnName("BILLING_CONTACT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillingCountry)
                    .IsRequired()
                    .HasColumnName("BILLING_COUNTRY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillingEmail)
                    .IsRequired()
                    .HasColumnName("BILLING_EMAIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillingFax)
                    .IsRequired()
                    .HasColumnName("BILLING_FAX")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillingPhone)
                    .IsRequired()
                    .HasColumnName("BILLING_PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillingState)
                    .IsRequired()
                    .HasColumnName("BILLING_STATE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.BillingZip)
                    .IsRequired()
                    .HasColumnName("BILLING_ZIP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("CITY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasColumnName("COUNTRY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CurrencyId)
                    .IsRequired()
                    .HasColumnName("CURRENCY_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom0)
                    .IsRequired()
                    .HasColumnName("CUSTOM0")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Custom1)
                    .IsRequired()
                    .HasColumnName("CUSTOM1")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Custom2)
                    .IsRequired()
                    .HasColumnName("CUSTOM2")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Custom3)
                    .IsRequired()
                    .HasColumnName("CUSTOM3")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Custom4)
                    .IsRequired()
                    .HasColumnName("CUSTOM4")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ShortName)
                    .IsRequired()
                    .HasColumnName("SHORT_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasColumnName("STATE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StoreId)
                    .IsRequired()
                    .HasColumnName("STORE_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeStamp)
                    .IsRequired()
                    .HasColumnName("TIME_STAMP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingAddr1)
                    .IsRequired()
                    .HasColumnName("TRAINING_ADDR1")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingAddr2)
                    .IsRequired()
                    .HasColumnName("TRAINING_ADDR2")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingCity)
                    .IsRequired()
                    .HasColumnName("TRAINING_CITY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingContactId)
                    .IsRequired()
                    .HasColumnName("TRAINING_CONTACT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingCountry)
                    .IsRequired()
                    .HasColumnName("TRAINING_COUNTRY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingEmail)
                    .IsRequired()
                    .HasColumnName("TRAINING_EMAIL")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingFax)
                    .IsRequired()
                    .HasColumnName("TRAINING_FAX")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingPhone)
                    .IsRequired()
                    .HasColumnName("TRAINING_PHONE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingState)
                    .IsRequired()
                    .HasColumnName("TRAINING_STATE")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TrainingZip)
                    .IsRequired()
                    .HasColumnName("TRAINING_ZIP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.WebSiteUrl)
                    .IsRequired()
                    .HasColumnName("WEB_SITE_URL")
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Zip)
                    .IsRequired()
                    .HasColumnName("ZIP")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SabaDomains>(entity =>
            {
                entity.ToTable("Saba_Domains", "Autodesk_New");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CiName)
                    .IsRequired()
                    .HasColumnName("CI_NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("CREATED_BY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedId)
                    .IsRequired()
                    .HasColumnName("CREATED_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn)
                    .IsRequired()
                    .HasColumnName("CREATED_ON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom0)
                    .IsRequired()
                    .HasColumnName("CUSTOM0")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom1)
                    .IsRequired()
                    .HasColumnName("CUSTOM1")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom2)
                    .IsRequired()
                    .HasColumnName("CUSTOM2")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom3)
                    .IsRequired()
                    .HasColumnName("CUSTOM3")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom4)
                    .IsRequired()
                    .HasColumnName("CUSTOM4")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom5)
                    .IsRequired()
                    .HasColumnName("CUSTOM5")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom6)
                    .IsRequired()
                    .HasColumnName("CUSTOM6")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom7)
                    .IsRequired()
                    .HasColumnName("CUSTOM7")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom8)
                    .IsRequired()
                    .HasColumnName("CUSTOM8")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Custom9)
                    .IsRequired()
                    .HasColumnName("CUSTOM9")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("DESCRIPTION")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DesktopId)
                    .IsRequired()
                    .HasColumnName("DESKTOP_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("NAME")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ParentId)
                    .IsRequired()
                    .HasColumnName("PARENT_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StoreId)
                    .IsRequired()
                    .HasColumnName("STORE_ID")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TimeStamp)
                    .IsRequired()
                    .HasColumnName("TIME_STAMP")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("UPDATED_BY")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn)
                    .IsRequired()
                    .HasColumnName("UPDATED_ON")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Worldlet)
                    .IsRequired()
                    .HasColumnName("WORLDLET")
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SerialNumbers>(entity =>
            {
                entity.HasKey(e => e.SerialNumberId);

                entity.ToTable("SerialNumbers", "Autodesk_New");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SN_SiteId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_SN_Status");

                entity.Property(e => e.SerialNumberId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Csn)
                    .IsRequired()
                    .HasColumnName("CSN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumberOfLicenses).HasColumnType("smallint(5)");

                entity.Property(e => e.SerialNumber)
                    .HasMaxLength(120)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.Usage)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ServiceProviderRoles>(entity =>
            {
                entity.HasKey(e => e.RoleCode);

                entity.ToTable("ServiceProviderRoles", "Autodesk_New");

                entity.Property(e => e.RoleCode)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.PartnerCode)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleDescription)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SiebelContactCsnlinks>(entity =>
            {
                entity.HasKey(e => new { e.SiebelId, e.ContactId });

                entity.ToTable("SiebelContactCSNLinks", "Autodesk_New");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_SCCL_ContactId");

                entity.HasIndex(e => e.SiebelId)
                    .HasName("IDX_SCCL_SiebelId");

                entity.Property(e => e.SiebelId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SiebelContactEidmlinks>(entity =>
            {
                entity.HasKey(e => new { e.SiebelId, e.ContactId });

                entity.ToTable("SiebelContactEIDMLinks", "Autodesk_New");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_SCCL_ContactId");

                entity.HasIndex(e => e.SiebelId)
                    .HasName("IDX_SCCL_SiebelId");

                entity.Property(e => e.SiebelId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SiebelSiteContactLinks>(entity =>
            {
                entity.HasKey(e => new { e.ContactCsn, e.AccountCsn });

                entity.ToTable("SiebelSiteContactLinks", "Autodesk_New");

                entity.Property(e => e.ContactCsn)
                    .HasColumnName("CONTACT_CSN")
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.AccountCsn)
                    .HasColumnName("ACCOUNT_CSN")
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SiteContactLinks>(entity =>
            {
                entity.HasKey(e => e.SiteContactLinkId);

                entity.ToTable("SiteContactLinks", "Autodesk_New");

                entity.HasIndex(e => e.ContactId)
                    .HasName("IDX_SCL_ContactId");

                entity.HasIndex(e => e.DateLastAdmin)
                    .HasName("IDX_SCL_DateLA");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SCL_SiteID");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_SCL_Status");

                entity.HasIndex(e => new { e.SiteId, e.ContactId })
                    .HasName("IDX_MAKE_UNIQUE")
                    .IsUnique();

                entity.Property(e => e.SiteContactLinkId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContactId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateDeleted).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");
            });

            modelBuilder.Entity<SiteCountryDistributorSKU>(entity =>
            {
                entity.ToTable("SiteCountryDistributorSKU", "Autodesk_New");

                entity.Property(e => e.SiteCountryDistributorSKUId)
                    .HasColumnName("SiteCountryDistributorSKUId")
                    .HasColumnType("int(8)");

                entity.Property(e => e.CountryCode).IsRequired();

                entity.Property(e => e.Currency)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.EdistributorType)
                    .IsRequired()
                    .HasColumnName("EDistributorType")
                    .HasColumnType("char(3)");

                entity.Property(e => e.FYIndicatorKey)
                    .HasColumnName("FYIndicatorKey")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IsForPrimarySite).HasColumnType("bit(1)");

                entity.Property(e => e.License)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerType)
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Price)
                    .IsRequired()
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.SiteId)
                    .IsRequired()
                    .HasColumnName("SiteID")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.Skudescription)
                    .IsRequired()
                    .HasColumnName("SKUDescription")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SKUName)
                    .HasColumnName("SKUName")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<SiteRoleParams>(entity =>
            {
                entity.HasKey(e => new { e.SiteId, e.RoleId, e.ParamName });

                entity.ToTable("SiteRoleParams", "Autodesk_New");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IDX_SiteRoleParams_RoleId");

                entity.HasIndex(e => e.RoleParamId)
                    .HasName("IDX_SiteRoleParams_RoleParamId");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SiteRoleParams_SiteId");

                entity.Property(e => e.SiteId)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnType("int(10)");

                entity.Property(e => e.ParamName)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RoleParamId).HasColumnType("bigint(20)");
            });

            modelBuilder.Entity<SiteRoles>(entity =>
            {
                entity.HasKey(e => e.SiteRoleId);

                entity.ToTable("SiteRoles", "Autodesk_New");

                entity.HasIndex(e => e.Csn)
                    .HasName("IDX_SR_CSN");

                entity.HasIndex(e => e.PartyId)
                    .HasName("IDX_SR_PartyID");

                entity.HasIndex(e => e.RoleId)
                    .HasName("IDX_SiteRoles_RoleId");

                entity.HasIndex(e => e.RoleParamId)
                    .HasName("IDX_RoleParamId");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SR_SiteId");

                entity.HasIndex(e => e.Status)
                    .HasName("IDX_SR_Status");

                entity.Property(e => e.SiteRoleId).HasColumnType("int(10)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ContractCsn)
                    .IsRequired()
                    .HasColumnName("ContractCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.Csn)
                    .IsRequired()
                    .HasColumnName("CSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateDeleted).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("2007-01-01 00:00:00");

                entity.Property(e => e.DeletedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ExternalId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ItsId)
                    .HasColumnName("ITS_ID")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NewParentCsn)
                    .HasColumnName("NewParent_CSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.ParentCsn)
                    .IsRequired()
                    .HasColumnName("ParentCSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PartyId)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.RoleId).HasColumnType("int(10)");

                entity.Property(e => e.RoleParamId).HasColumnType("int(10)");

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Status).HasColumnType("char(1)");

                entity.Property(e => e.UparentCsn)
                    .HasColumnName("UParent_CSN")
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<SiteService>(entity =>
            {
                entity.ToTable("Site_service", "Autodesk_New");

                entity.HasIndex(e => e.Description)
                    .HasName("IDX_INVOICETYPE_ActivityType");

                entity.Property(e => e.SiteServiceId)
                    .HasColumnName("site_service_id")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Cdate)
                    .HasColumnName("cdate")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Cuid)
                    .HasColumnName("cuid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasMaxLength(30)
                    .IsUnicode(false)
                    .HasDefaultValueSql("Invoice");

                entity.Property(e => e.Mdate)
                    .HasColumnName("mdate")
                    .HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Muid)
                    .HasColumnName("muid")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SiteServiceCode)
                    .IsRequired()
                    .HasColumnName("site_service_code")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.SiteServiceName)
                    .HasColumnName("site_service_name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasColumnName("status")
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<SiteServices>(entity =>
            {
                entity.HasKey(e => e.SiteServiceId);

                entity.ToTable("SiteServices", "Autodesk_New");

                entity.Property(e => e.SiteServiceId).HasColumnType("int(8)");

                entity.Property(e => e.AddedBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DateAdded).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.DateLastAdmin).HasDefaultValueSql("0000-00-00 00:00:00");

                entity.Property(e => e.Description)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LastAdminBy)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ServiceId)
                    .HasColumnName("ServiceID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SiteId)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");
            });

            modelBuilder.Entity<States>(entity =>
            {
                entity.HasKey(e => e.StateIdint);

                entity.ToTable("States", "Autodesk_New");

                entity.HasIndex(e => e.CountryCode)
                    .HasName("IDX_States_CountryCode");

                entity.HasIndex(e => e.StateCode)
                    .HasName("IDX_StateCode");

                entity.HasIndex(e => e.StateId)
                    .HasName("IDX_StateID");

                entity.HasIndex(e => e.SubCountry)
                    .HasName("IDX_States_SubCountry");

                entity.Property(e => e.StateIdint)
                    .HasColumnName("StateIDInt")
                    .HasColumnType("int(8)");

                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.StateCode)
                    .IsRequired()
                    .HasMaxLength(72)
                    .IsUnicode(false);

                entity.Property(e => e.StateId)
                    .HasColumnName("StateID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StateName)
                    .HasMaxLength(72)
                    .IsUnicode(false);

                entity.Property(e => e.SubCountry)
                    .IsRequired()
                    .HasMaxLength(72)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblPostSurveyMatchQuestions>(entity =>
            {
                entity.HasKey(e => e.PostSurveyMatchQuestions);

                entity.ToTable("tblPostSurveyMatchQuestions", "Autodesk_New");

                entity.HasIndex(e => new { e.PostSurveyId, e.PostQuestionId })
                    .HasName("PostSurveyID");

                entity.HasIndex(e => new { e.SurveyId, e.QuestionId })
                    .HasName("SurveyID");

                entity.Property(e => e.PostSurveyMatchQuestions)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever();

                entity.Property(e => e.DeviationType)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.PostQuestionId)
                    .HasColumnName("PostQuestionID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PostSurveyId)
                    .HasColumnName("PostSurveyID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.PostSurveyName)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.QuestionId)
                    .HasColumnName("QuestionID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SurveyId)
                    .HasColumnName("SurveyID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.SurveyName)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TblStudents>(entity =>
            {
                entity.HasKey(e => e.StudentId);

                entity.ToTable("tblStudents", "Autodesk_New");

                entity.HasIndex(e => e.CountryId)
                    .HasName("IDX_CountryID");

                entity.HasIndex(e => e.Email);

                entity.HasIndex(e => e.ExternalId);

                entity.HasIndex(e => e.LanguageId)
                    .HasName("LanguageID");

                entity.HasIndex(e => e.StateId)
                    .HasName("StateID");

                entity.HasIndex(e => e.StudentId)
                    .HasName("IDX_StudentID");

                entity.Property(e => e.StudentId)
                    .HasColumnName("StudentID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Address)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.City)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Company)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CountryId)
                    .HasColumnName("CountryID")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Email2)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ExternalId)
                    .HasColumnName("ExternalID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Firstname)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageId)
                    .HasColumnName("LanguageID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.LastIpaddress)
                    .HasColumnName("LastIPAddress")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Lastname)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LoginCount).HasColumnType("smallint(5)");

                entity.Property(e => e.Mobile)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MobileCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PasswordPlain)
                    .HasColumnName("Password_plain")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Phone)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PostalCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryIndustryId)
                    .HasMaxLength(40)
                    .IsUnicode(false);

                entity.Property(e => e.PrimaryLanguage).HasColumnType("char(10)");

                entity.Property(e => e.PwdchangeRequired)
                    .HasColumnName("PWDChangeRequired")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.ResetPasswordCode).HasColumnType("mediumtext");

                entity.Property(e => e.Salutation)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.SecondaryLanguage).HasColumnType("char(10)");

                entity.Property(e => e.StateId)
                    .HasColumnName("StateID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StateProvince)
                    .HasMaxLength(72)
                    .IsUnicode(false);

                entity.Property(e => e.Status)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.StatusLevel)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.TelephoneCode)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.TermAndCondition).HasColumnType("smallint(5)");
            });

            modelBuilder.Entity<TblTranslatedLanguages>(entity =>
            {
                entity.HasKey(e => new { e.FromLanguageId, e.ToLanguageId });

                entity.ToTable("tblTranslatedLanguages", "Autodesk_New");

                entity.HasIndex(e => e.FromLanguageId)
                    .HasName("IDX_FromLangId");

                entity.HasIndex(e => e.ToLanguageId)
                    .HasName("ToLanguageID");

                entity.Property(e => e.FromLanguageId)
                    .HasColumnName("FromLanguageID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.ToLanguageId)
                    .HasColumnName("ToLanguageID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Text).IsRequired();
            });

            modelBuilder.Entity<Timezones>(entity =>
            {
                entity.HasKey(e => e.ZoneName);

                entity.ToTable("Timezones", "Autodesk_New");

                entity.Property(e => e.ZoneName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BaseUtcOffset).HasColumnType("int(10)");

                entity.Property(e => e.Country)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DaylightName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DisplayName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Format)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.GmtOffset).HasColumnType("int(10)");

                entity.Property(e => e.RuleName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StandardName)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.SupportsDaylightSavingTime)
                    .IsRequired()
                    .HasMaxLength(5)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TmpDashboardMeetAnnual>(entity =>
            {
                entity.HasKey(e => e.OrgId);

                entity.ToTable("tmpDashboardMeetAnnual", "Autodesk_New");

                entity.Property(e => e.OrgId)
                    .HasMaxLength(88)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.BackgroundColor)
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.Label)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Meetornotmeet)
                    .HasColumnName("MEETORNOTMEET")
                    .HasColumnType("int(11)");

                entity.Property(e => e.TerritoryName)
                    .HasColumnName("Territory_Name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TmpSitesCoe>(entity =>
            {
                entity.HasKey(e => e.SiteId);

                entity.ToTable("tmpSitesCOE", "Autodesk_New");

                entity.HasIndex(e => e.SiteId)
                    .HasName("IDX_SiteId");

                entity.Property(e => e.SiteId)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Coe).HasColumnName("COE");
            });

            modelBuilder.Entity<TypeOfTrainings>(entity =>
            {
                entity.HasKey(e => e.TypeOfTrainingId);

                entity.ToTable("TypeOfTrainings", "Autodesk_New");

                entity.Property(e => e.TypeOfTrainingId)
                    .HasColumnType("int(11)")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TypeOfTraining)
                    .HasMaxLength(200)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserLevel>(entity =>
            {
                entity.ToTable("UserLevel", "Autodesk_New");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Admin)
                    .HasColumnName("admin")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminActivities)
                    .HasColumnName("admin/activities")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminActivitiesActivitiesAdd)
                    .HasColumnName("admin/activities/activities-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminActivitiesActivitiesDelete)
                    .HasColumnName("admin/activities/activities-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminActivitiesActivitiesDetail)
                    .HasColumnName("admin/activities/activities-detail")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminActivitiesActivitiesList)
                    .HasColumnName("admin/activities/activities-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminActivitiesActivitiesUpdate)
                    .HasColumnName("admin/activities/activities-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountry)
                    .HasColumnName("admin/country")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminCountryCountriesAdd)
                    .HasColumnName("admin/country/countries-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryCountriesDelete)
                    .HasColumnName("admin/country/countries-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryCountriesList)
                    .HasColumnName("admin/country/countries-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryCountriesUpdate)
                    .HasColumnName("admin/country/countries-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryCountryList)
                    .HasColumnName("admin/country/country-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryGeoAdd)
                    .HasColumnName("admin/country/geo-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryGeoDelete)
                    .HasColumnName("admin/country/geo-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryGeoList)
                    .HasColumnName("admin/country/geo-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryGeoUpdate)
                    .HasColumnName("admin/country/geo-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryRegionAdd)
                    .HasColumnName("admin/country/region-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryRegionDelete)
                    .HasColumnName("admin/country/region-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryRegionList)
                    .HasColumnName("admin/country/region-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryRegionUpdate)
                    .HasColumnName("admin/country/region-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountrySubRegionAdd)
                    .HasColumnName("admin/country/sub-region-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountrySubRegionDelete)
                    .HasColumnName("admin/country/sub-region-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountrySubRegionList)
                    .HasColumnName("admin/country/sub-region-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountrySubRegionUpdate)
                    .HasColumnName("admin/country/sub-region-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryTerritoryAdd)
                    .HasColumnName("admin/country/territory-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryTerritoryDelete)
                    .HasColumnName("admin/country/territory-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryTerritoryList)
                    .HasColumnName("admin/country/territory-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCountryTerritoryUpdate)
                    .HasColumnName("admin/country/territory-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCurrency)
                    .HasColumnName("admin/currency")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCurrencyAdd)
                    .HasColumnName("admin/currency-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCurrencyConversionAdd)
                    .HasColumnName("admin/currency-conversion-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCurrencyConversionDelete)
                    .HasColumnName("admin/currency-conversion-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCurrencyConversionEdit)
                    .HasColumnName("admin/currency-conversion-edit")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminCurrencyEdit)
                    .HasColumnName("admin/currency-edit")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributor)
                    .HasColumnName("admin/distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminDistributorAddContactDistributor)
                    .HasColumnName("admin/distributor/add-contact-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributorAddDistributor)
                    .HasColumnName("admin/distributor/add-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributorAddMasterDistributor)
                    .HasColumnName("admin/distributor/add-master-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributorDeleteContactDistributor)
                    .HasColumnName("admin/distributor/delete-contact-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributorDeleteMasterDistributor)
                    .HasColumnName("admin/distributor/delete-master-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributorDeleteRegionalDistributor)
                    .HasColumnName("admin/distributor/delete-regional-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributorListDistributor)
                    .HasColumnName("admin/distributor/list-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributorSetContactDistributor)
                    .HasColumnName("admin/distributor/set-contact-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributorSetMasterDistributor)
                    .HasColumnName("admin/distributor/set-master-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminDistributorSetRegionalDistributor)
                    .HasColumnName("admin/distributor/set-regional-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminEmail)
                    .HasColumnName("admin/email")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminEmailAddEditEmail)
                    .HasColumnName("admin/email/add-edit-email")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminEmailDeleteEmail)
                    .HasColumnName("admin/email/delete-email")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminEmailListEmailBody)
                    .HasColumnName("admin/email/list-email-body")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminGlossary)
                    .HasColumnName("admin/glossary")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminGlossaryGlossary)
                    .HasColumnName("admin/glossary/glossary")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminGlossaryGlossaryAdd)
                    .HasColumnName("admin/glossary/glossary-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminGlossaryGlossaryDelete)
                    .HasColumnName("admin/glossary/glossary-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminGlossaryGlossaryList)
                    .HasColumnName("admin/glossary/glossary-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminGlossaryGlossaryUpdate)
                    .HasColumnName("admin/glossary/glossary-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminInvoiceType)
                    .HasColumnName("admin/invoice-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminInvoiceTypeInvoiceTypeAdd)
                    .HasColumnName("admin/invoice-type/invoice-type-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminInvoiceTypeInvoiceTypeList)
                    .HasColumnName("admin/invoice-type/invoice-type-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminInvoiceTypeInvoiceTypeUpdate)
                    .HasColumnName("admin/invoice-type/invoice-type-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminLanguage)
                    .HasColumnName("admin/language")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminLanguageAddLanguage)
                    .HasColumnName("admin/language/add-language")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminLanguageListLanguage)
                    .HasColumnName("admin/language/list-language")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminLanguageSetupLanguage)
                    .HasColumnName("admin/language/setup-language")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminLineItemSummaries)
                    .HasColumnName("admin/line-item-summaries")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminLineItemSummariesLineItemSummariesAdd)
                    .HasColumnName("admin/line-item-summaries/line-item-summaries-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminLineItemSummariesLineItemSummariesList)
                    .HasColumnName("admin/line-item-summaries/line-item-summaries-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminLineItemSummariesLineItemSummariesUpdate)
                    .HasColumnName("admin/line-item-summaries/line-item-summaries-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminMarketType)
                    .HasColumnName("admin/market-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminMarketTypeMarketTypeAdd)
                    .HasColumnName("admin/market-type/market-type-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminMarketTypeMarketTypeList)
                    .HasColumnName("admin/market-type/market-type-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminMarketTypeMarketTypeUpdate)
                    .HasColumnName("admin/market-type/market-type-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminPartnerType)
                    .HasColumnName("admin/partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminPartnerTypeAddPartnerType)
                    .HasColumnName("admin/partner-type/add-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminPartnerTypeAddSubPartnerType)
                    .HasColumnName("admin/partner-type/add-sub-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminPartnerTypeDeletePartnerType)
                    .HasColumnName("admin/partner-type/delete-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminPartnerTypeDeleteSubPartnerType)
                    .HasColumnName("admin/partner-type/delete-sub-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminPartnerTypeListPartnerType)
                    .HasColumnName("admin/partner-type/list-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminPartnerTypeListSubPartnerType)
                    .HasColumnName("admin/partner-type/list-sub-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminPartnerTypeUpdatePartnerType)
                    .HasColumnName("admin/partner-type/update-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminPartnerTypeUpdateSubPartnerType)
                    .HasColumnName("admin/partner-type/update-sub-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminProducts)
                    .HasColumnName("admin/products")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminProductsAddCategory)
                    .HasColumnName("admin/products/add-category")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminProductsDeleteCategory)
                    .HasColumnName("admin/products/delete-category")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminProductsListCategory)
                    .HasColumnName("admin/products/list-category")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminProductsProductsAdd)
                    .HasColumnName("admin/products/products-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminProductsProductsCategoryUpdate)
                    .HasColumnName("admin/products/products-category-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminProductsProductsDelete)
                    .HasColumnName("admin/products/products-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminProductsProductsList)
                    .HasColumnName("admin/products/products-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminProductsProductsUpdate)
                    .HasColumnName("admin/products/products-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminSetupLanguage)
                    .HasColumnName("admin/setup-language")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminSiteServices)
                    .HasColumnName("admin/site-services")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminSiteServicesSiteServicesAdd)
                    .HasColumnName("admin/site-services/site-services-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminSiteServicesSiteServicesList)
                    .HasColumnName("admin/site-services/site-services-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminSiteServicesSiteServicesUpdate)
                    .HasColumnName("admin/site-services/site-services-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminSku)
                    .HasColumnName("admin/sku")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminSkuAddSku)
                    .HasColumnName("admin/sku/add-sku")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminSkuDeleteSku)
                    .HasColumnName("admin/sku/delete/sku")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminSkuListSku)
                    .HasColumnName("admin/sku/list-sku")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminSkuUpdateSku)
                    .HasColumnName("admin/sku/update-sku")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminTerritory)
                    .HasColumnName("admin/territory")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminTerritoryTerritoryAdd)
                    .HasColumnName("admin/territory/territory-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminTerritoryTerritoryList)
                    .HasColumnName("admin/territory/territory-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminTerritoryTerritoryUpdate)
                    .HasColumnName("admin/territory/territory-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminVariables)
                    .HasColumnName("admin/variables")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.AdminVariablesVariablesAdd)
                    .HasColumnName("admin/variables/variables-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminVariablesVariablesDelete)
                    .HasColumnName("admin/variables/variables-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminVariablesVariablesList)
                    .HasColumnName("admin/variables/variables-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AdminVariablesVariablesUpdate)
                    .HasColumnName("admin/variables/variables-update")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Agreement)
                    .HasColumnName("agreement")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AgreementApprove)
                    .HasColumnName("agreement/approve")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.AgreementExecute)
                    .HasColumnName("agreement/execute")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Certificate)
                    .HasColumnName("certificate")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.CertificateCertificateAddBackground)
                    .HasColumnName("certificate/certificate-add-background")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CertificateCertificateAddDesign)
                    .HasColumnName("certificate/certificate-add-design")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CertificateCertificateDeleteBackground)
                    .HasColumnName("certificate/certificate-delete-background")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CertificateCertificateDeleteDesign)
                    .HasColumnName("certificate/certificate-delete-design")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CertificateCertificateEditDesign)
                    .HasColumnName("certificate/certificate-edit-design")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CertificateCertificateListBackground)
                    .HasColumnName("certificate/certificate-list-background")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CertificateCertificateListDesign)
                    .HasColumnName("certificate/certificate-list-design")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CertificateCertificateViewBackground)
                    .HasColumnName("certificate/certificate-view-background")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CertificateCertificateViewDesign)
                    .HasColumnName("certificate/certificate-view-design")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ChangeProfile)
                    .HasColumnName("change-profile")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Course)
                    .HasColumnName("course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.CourseAddCourse)
                    .HasColumnName("course/add-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CourseCourseAdd)
                    .HasColumnName("course/course-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CourseCourseCopy)
                    .HasColumnName("course/course-copy")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CourseCourseDelete)
                    .HasColumnName("course/course-delete")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.CourseCourseList)
                    .HasColumnName("course/course-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CourseCourseUpdate)
                    .HasColumnName("course/" +
                                   "course-update" +
                                   "")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CourseEvaluationStudent)
                    .HasColumnName("course/evaluation-student")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.CourseSearchCourses)
                    .HasColumnName("course/search-courses")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Dashboard)
                    .HasColumnName("dashboard")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.DashboardAdmin)
                    .HasColumnName("dashboard-admin")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.DashboardDistributor)
                    .IsRequired()
                    .HasColumnName("dashboard-distributor")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.DashboardOrganization)
                    .HasColumnName("dashboard-organization")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.DashboardSite)
                    .HasColumnName("dashboard-site")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.DashboardStudent)
                    .HasColumnName("dashboard-student")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.DashboardTrainer)
                    .HasColumnName("dashboard-trainer")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Evaluation)
                    .HasColumnName("evaluation")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.EvaluationAddEditForm)
                    .HasColumnName("evaluation/add-edit-form")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.EvaluationAddEditTemplatedata)
                    .HasColumnName("evaluation/add-edit-templatedata")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.EvaluationDeleteTemplatedata)
                    .HasColumnName("evaluation/delete-templatedata")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.EvaluationListOfTheForm)
                    .HasColumnName("evaluation/list-of-the-form")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.EvaluationSurveyAnswerAdd)
                    .HasColumnName("evaluation/survey-answer-add")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.EvaluationSurveyAnswerList)
                    .HasColumnName("evaluation/survey-answer-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.EvaluationSurveyTemplateCopy)
                    .HasColumnName("evaluation/survey-template-copy")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.EvaluationSurveyTemplateDetail)
                    .HasColumnName("evaluation/survey-template-detail")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.EvaluationSurveyTemplateList)
                    .HasColumnName("evaluation/survey-template-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.EvaluationUpdateTemplatedata)
                    .HasColumnName("evaluation/update-templatedata")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Manage)
                    .HasColumnName("manage")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.ManageAci)
                    .HasColumnName("manage/aci")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContact)
                    .HasColumnName("manage/contact")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.ManageContactAddContact)
                    .HasColumnName("manage/contact/add-contact")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContactAddContactJournal)
                    .HasColumnName("manage/contact/add-contact-journal")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContactAddContactQualifications)
                    .HasColumnName("manage/contact/add-contact-qualifications")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContactAddNewContact)
                    .HasColumnName("manage/contact/add-new-contact")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContactDetailContact)
                    .HasColumnName("manage/contact/detail-contact")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContactEditContact)
                    .HasColumnName("manage/contact/edit-contact")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContactEditContactJournal)
                    .HasColumnName("manage/contact/edit-contact-journal")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContactEditContactQualifications)
                    .HasColumnName("manage/contact/edit-contact-qualifications")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContactSearchContact)
                    .HasColumnName("manage/contact/search-contact")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageContactSiteAffiliations)
                    .HasColumnName("manage/contact/site-affiliations")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganization)
                    .HasColumnName("manage/organization")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationAddAcademicProject)
                    .HasColumnName("manage/organization/add-academic-project")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationAddInvoice)
                    .HasColumnName("manage/organization/add-invoice")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationAddOrganization)
                    .HasColumnName("manage/organization/add-organization")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationAddOrgjournalentries)
                    .HasColumnName("manage/organization/add-orgjournalentries")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationAddPartnerType)
                    .HasColumnName("manage/organization/add-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationCertifications)
                    .HasColumnName("manage/organization/certifications")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationDetailOrganization)
                    .HasColumnName("manage/organization/detail-organization")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationEditOrganization)
                    .HasColumnName("manage/organization/edit-organization")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationEditOrgattachments)
                    .HasColumnName("manage/organization/edit-orgattachments")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationEditOrgattributes)
                    .HasColumnName("manage/organization/edit-orgattributes")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationEditOrgjournalentries)
                    .HasColumnName("manage/organization/edit-orgjournalentries")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationLearningHistory)
                    .HasColumnName("manage/organization/learning-history")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationPdbHistory)
                    .HasColumnName("manage/organization/pdb-history")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationSearchOrganization)
                    .HasColumnName("manage/organization/search-organization")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationSpecializations)
                    .HasColumnName("manage/organization/specializations")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationUpdateInvoice)
                    .HasColumnName("manage/organization/update-invoice")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageOrganizationUpdatePartnerType)
                    .HasColumnName("manage/organization/update-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSearchGeneral)
                    .HasColumnName("manage/search-general")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSite)
                    .HasColumnName("manage/site")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.ManageSiteAddAcademicProject)
                    .HasColumnName("manage/site/add-academic-project")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteAddPartnerType)
                    .HasColumnName("manage/site/add-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteAddSite)
                    .HasColumnName("manage/site/add-site")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteAddSiteAcademicPrograms)
                    .HasColumnName("manage/site/add-site-academic-programs")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteAddSiteAccreditation)
                    .HasColumnName("manage/site/add-site-accreditation")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteAddSiteJournal)
                    .HasColumnName("manage/site/add-site-journal")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteDetailSite)
                    .HasColumnName("manage/site/detail-site")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteEditSite)
                    .HasColumnName("manage/site/edit-site")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteEditSiteAcademicPrograms)
                    .HasColumnName("manage/site/edit-site-academic-programs")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteSearchSiebel)
                    .HasColumnName("manage/site/search-siebel")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteSearchSite)
                    .HasColumnName("manage/site/search-site")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteUpdateAcademicProject)
                    .HasColumnName("manage/site/update-academic-project")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteUpdatePartnerType)
                    .HasColumnName("manage/site/update-partner-type")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteUpdateSiteAccreditation)
                    .HasColumnName("manage/site/update-site-accreditation")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ManageSiteUpdateSiteJournal)
                    .HasColumnName("manage/site/update-site-journal")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Profile)
                    .HasColumnName("profile")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Report)
                    .HasColumnName("report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.ReportAtcAapAccreditationCountsReport)
                    .HasColumnName("report/ATC-AAP-accreditation-counts-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportBetaReportBuilder)
                    .HasColumnName("report/beta-report-builder")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportContactAttributesReport)
                    .HasColumnName("report/contact-attributes-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportContactReport)
                    .HasColumnName("report/contact-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportDatabaseIntegrityReport)
                    .HasColumnName("report/database-integrity-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEmailListGenerator)
                    .HasColumnName("report/email-list-generator")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEva)
                    .HasColumnName("report-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.ReportEvaAudit)
                    .HasColumnName("report-eva/audit")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaChannelPerformanceEva)
                    .HasColumnName("report-eva/channel-performance-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaComparisonReport)
                    .HasColumnName("report-eva/comparison-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaDownloadSiteEva)
                    .HasColumnName("report-eva/download-site-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaEvaluationEdition)
                    .HasColumnName("report-eva/evaluation-edition")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaEvaluationResponses)
                    .HasColumnName("report-eva/evaluation-responses")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaInstructorCourse)
                    .HasColumnName("report-eva/instructor-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaInstructorPerformanceEva)
                    .HasColumnName("report-eva/instructor-performance-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaInstructorStudent)
                    .HasColumnName("report-eva/instructor-student")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaOrganizationReportEva)
                    .HasColumnName("report-eva/organization-report-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaOtherAnswer)
                    .HasColumnName("report-eva/other-answer")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaPerformanceOverviewEva)
                    .HasColumnName("report-eva/performance-overview-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaPostEvaluationResponses)
                    .HasColumnName("report-eva/post-evaluation-responses")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaProductTrainedEva)
                    .HasColumnName("report-eva/product-trained-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaProductTrainedStatistic)
                    .HasColumnName("report-eva/product-trained-statistic")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaSiteProfile)
                    .HasColumnName("report-eva/site-profile")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaStudentCourse)
                    .HasColumnName("report-eva/student-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaStudentSearchEva)
                    .HasColumnName("report-eva/student-search-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportEvaSubmissionsByLanguageEva)
                    .HasColumnName("report-eva/submissions-by-language-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportHistoryReport)
                    .HasColumnName("report/history-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportInvoiceReport)
                    .HasColumnName("report/invoice-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportOrganizationJournalEntryReport)
                    .HasColumnName("report/organization-journal-entry-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportOrganizationReport)
                    .HasColumnName("report/organization-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportQualificationsByContactReport)
                    .HasColumnName("report/qualifications-by-contact-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportShowSecurityRoles)
                    .HasColumnName("report/show-security-roles")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportSiteAccreditations)
                    .HasColumnName("report/site-accreditations")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportSiteAttributesReport)
                    .HasColumnName("report/site-attributes-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportSiteJournalEntryReport)
                    .HasColumnName("report/site-journal-entry-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.ReportSitesReport)
                    .HasColumnName("report/sites-report")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Role)
                    .HasColumnName("role")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.RoleAddRole)
                    .HasColumnName("role/add-role")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.RoleListAccess)
                    .HasColumnName("role/list-access")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Student)
                    .HasColumnName("student")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.StudentAllCourse)
                    .HasColumnName("student/all-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentAnswerEvaluationForm)
                    .HasColumnName("student/answer-evaluation-form")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentDetailCourse)
                    .HasColumnName("student/detail-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentDownloadCertificate)
                    .HasColumnName("student/download-certificate")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentHistoryCourse)
                    .HasColumnName("student/history-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentMyCourse)
                    .HasColumnName("student/my-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentNextCourse)
                    .HasColumnName("student/next-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentSearchCourse)
                    .HasColumnName("student/search-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentSurveyCourse)
                    .HasColumnName("student/survey-course")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.Studentinfo)
                    .HasColumnName("studentinfo")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.StudentinfoConfirmFinalStudent)
                    .HasColumnName("studentinfo/confirm-final-student")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentinfoSearchStudent)
                    .HasColumnName("studentinfo/search-student")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentinfoStudentList)
                    .HasColumnName("studentinfo/student-list")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentinfoTerminateStudent)
                    .HasColumnName("studentinfo/terminate-student")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.StudentinfoUpdateStudentInformation)
                    .HasColumnName("studentinfo/update-student-information")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.TemplatePreview)
                    .HasColumnName("template-preview")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("11111");

                entity.Property(e => e.TemplatePreviewCertificatePreviewEva)
                    .HasColumnName("template-preview/certificate-preview-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.TemplatePreviewSurveyPreviewEva)
                    .HasColumnName("template-preview/survey-preview-eva")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasDefaultValueSql("00000");

                entity.Property(e => e.UserLevelId)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserToken>(entity =>
            {
                entity.ToTable("UserToken", "Autodesk_New");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Token).IsRequired();

                entity.Property(e => e.UserId).HasColumnType("int(11)");
            });

            modelBuilder.Entity<Versioncontrol>(entity =>
            {
                entity.ToTable("versioncontrol", "Autodesk_New");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.Version).HasColumnType("decimal(5,2)");
            });
        }
    }
}
