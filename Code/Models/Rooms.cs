﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Rooms
    {
        public int RoomId { get; set; }
        public string SiteId { get; set; }
        public DateTime DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string Status { get; set; }
        public string RoomName { get; set; }
        public int NumberOfSeats { get; set; }
        public string Comments { get; set; }
        public int? FacilityId { get; set; }
    }
}
