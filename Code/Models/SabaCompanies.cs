﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SabaCompanies
    {
        public string Id { get; set; }
        public string TimeStamp { get; set; }
        public string Custom0 { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public string Custom6 { get; set; }
        public string Custom7 { get; set; }
        public string Custom8 { get; set; }
        public string Custom9 { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public string CiName { get; set; }
        public string CompanyType { get; set; }
        public string Name { get; set; }
        public string AaId { get; set; }
        public string BusinessTyp { get; set; }
        public string CiName2 { get; set; }
        public string ContactId { get; set; }
        public string CostCenter { get; set; }
        public string Email { get; set; }
        public string Fax { get; set; }
        public string MaxDiscount { get; set; }
        public string Name2 { get; set; }
        public string ParentId { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Split { get; set; }
        public string WebServer { get; set; }
        public string SalesrepId { get; set; }
        public string SecKey { get; set; }
        public string Description { get; set; }
        public string Flags { get; set; }
        public string LevelNo { get; set; }
        public string StoreId { get; set; }
    }
}
