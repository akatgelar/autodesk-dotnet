﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class AcademicTargetProgram
    {
        public int AcdemicId { get; set; }
        public string AcademicType { get; set; }
        public string SiteId { get; set; }
        public string Usage { get; set; }
        public string Fyindicator { get; set; }
        public short? TargetEducator { get; set; }
        public short? TargetStudent { get; set; }
        public short? TargetInstitution { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string Csn { get; set; }
    }
}
