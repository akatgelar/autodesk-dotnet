﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Versioncontrol
    {
        public int Id { get; set; }
        public decimal Version { get; set; }
        public DateTime? DateTimes { get; set; }
    }
}
