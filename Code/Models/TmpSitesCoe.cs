﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class TmpSitesCoe
    {
        public string SiteId { get; set; }
        public float? Coe { get; set; }
    }
}
