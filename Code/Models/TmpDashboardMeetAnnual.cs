﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class TmpDashboardMeetAnnual
    {
        public string OrgId { get; set; }
        public string Label { get; set; }
        public string TerritoryName { get; set; }
        public int? Meetornotmeet { get; set; }
        public string BackgroundColor { get; set; }
        public DateTime? LastUpdated { get; set; }
    }
}
