﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public class History
    {
        public DateTime Date { get; set; }
        public string Id { get; set; }
        public string Description { get; set; }
        public string Admin { get; set; }
    }
}
