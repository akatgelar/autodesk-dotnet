﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class ProductVersions
    {
        public int ProductVersionsId { get; set; }
        public int ProductId { get; set; }
        public string Version { get; set; }
        public string Os { get; set; }
        public int DisplayOrder { get; set; }
        public string Status { get; set; }
    }
}
