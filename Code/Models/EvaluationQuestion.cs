﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class EvaluationQuestion
    {
        public int EvaluationQuestionId { get; set; }
        public string EvaluationQuestionCode { get; set; }
        public string CourseId { get; set; }
        public string CountryCode { get; set; }
        public string PartnerType { get; set; }
        public int? CertificateTypeId { get; set; }
        public string Year { get; set; }
        public string EvaluationQuestionTemplate { get; set; }
        public string EvaluationQuestionJson { get; set; }
        public int? LanguageId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
