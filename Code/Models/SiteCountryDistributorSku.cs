﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SiteCountryDistributorSKU
    {
        public int SiteCountryDistributorSKUId { get; set; }
        public string SiteId { get; set; }
        public string CountryCode { get; set; }
        public string EdistributorType { get; set; }
        public string Skudescription { get; set; }
        public string Price { get; set; }
        public string License { get; set; }
        public string Currency { get; set; }
        public string Status { get; set; }
        public string FYIndicatorKey { get; set; }
        public string SKUName { get; set; }
        public string PartnerType { get; set; }
        public bool? IsForPrimarySite { get; set; }
    }
}
