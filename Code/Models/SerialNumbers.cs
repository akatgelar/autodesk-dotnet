﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SerialNumbers
    {
        public int SerialNumberId { get; set; }
        public string SerialNumber { get; set; }
        public string SiteId { get; set; }
        public string Usage { get; set; }
        public short? NumberOfLicenses { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string Csn { get; set; }
    }
}
