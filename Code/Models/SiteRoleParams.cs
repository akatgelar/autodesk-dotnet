﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SiteRoleParams
    {
        public string SiteId { get; set; }
        public int RoleId { get; set; }
        public string ParamName { get; set; }
        public long RoleParamId { get; set; }
    }
}
