﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class InstructorRequest
    {
        public int Id { get; set; }
        public string ContactId { get; set; }
        public string RequesterId { get; set; }
        public string DistributorId { get; set; }
        public string RequestedSiteId { get; set; }
        public string ReasonType { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
    }
}
