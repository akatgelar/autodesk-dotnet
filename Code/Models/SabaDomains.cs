﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SabaDomains
    {
        public string Id { get; set; }
        public string TimeStamp { get; set; }
        public string Custom0 { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public string Custom6 { get; set; }
        public string Custom7 { get; set; }
        public string Custom8 { get; set; }
        public string Custom9 { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public string UpdatedOn { get; set; }
        public string CiName { get; set; }
        public string Name { get; set; }
        public string CreatedId { get; set; }
        public string Worldlet { get; set; }
        public string Description { get; set; }
        public string ParentId { get; set; }
        public string DesktopId { get; set; }
        public string StoreId { get; set; }
    }
}
