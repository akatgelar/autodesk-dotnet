﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class AcademicProject
    {
        public int ProjectId { get; set; }
        public string SiteId { get; set; }
        public string Usage { get; set; }
        public DateTime? CompletionDate { get; set; }
        public short? TargetEducator { get; set; }
        public short? TargetStudent { get; set; }
        public string ProductId { get; set; }
        public string ProductVersion { get; set; }
        public string Institution { get; set; }
        public string CountryCode { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
    }
}
