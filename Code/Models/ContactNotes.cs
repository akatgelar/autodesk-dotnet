﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class ContactNotes
    {
        public int ContactNoteId { get; set; }
        public string ContactId { get; set; }
        public string Content { get; set; }
        public DateTime? DateEntered { get; set; }
        public string EnteredBy { get; set; }
    }
}
