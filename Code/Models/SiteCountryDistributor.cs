﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public class SiteCountryDistributor
    {
        [Key]
        public Guid Guid { get; set; }
        public string SiteID { get; set; }
        public string CountryCode { get; set; }
        public string EDistributorType { get; set; }
        public int PrimaryD { get; set; }
    }
}
