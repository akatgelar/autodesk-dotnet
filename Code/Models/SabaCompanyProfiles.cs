﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SabaCompanyProfiles
    {
        public string Id { get; set; }
        public string TimeStamp { get; set; }
        public string Name { get; set; }
        public string CurrencyId { get; set; }
        public string ShortName { get; set; }
        public string Addr1 { get; set; }
        public string Addr2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string WebSiteUrl { get; set; }
        public string BillingAddr1 { get; set; }
        public string BillingAddr2 { get; set; }
        public string BillingCity { get; set; }
        public string BillingState { get; set; }
        public string BillingZip { get; set; }
        public string BillingCountry { get; set; }
        public string BillingPhone { get; set; }
        public string BillingContactId { get; set; }
        public string BillingFax { get; set; }
        public string BillingEmail { get; set; }
        public string TrainingAddr1 { get; set; }
        public string TrainingAddr2 { get; set; }
        public string TrainingCity { get; set; }
        public string TrainingState { get; set; }
        public string TrainingZip { get; set; }
        public string TrainingCountry { get; set; }
        public string TrainingPhone { get; set; }
        public string TrainingContactId { get; set; }
        public string TrainingFax { get; set; }
        public string TrainingEmail { get; set; }
        public string Custom0 { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string StoreId { get; set; }
    }
}
