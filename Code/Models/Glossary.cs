﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Glossary
    {
        public int GlossaryId { get; set; }
        public string GlossaryTerm { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
        public string Cuid { get; set; }
        public DateTime? Cdate { get; set; }
        public string Muid { get; set; }
        public DateTime? Mdate { get; set; }
    }
}
