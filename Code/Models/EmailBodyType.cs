﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class EmailBodyType
    {
        public int EmailBodyTypeId { get; set; }
        public string EmailBodyTypeName { get; set; }
    }
}
