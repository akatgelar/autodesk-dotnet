﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Qualifications
    {
        public int QualificationId { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string ContactId { get; set; }
        public string AutodeskProductVersion { get; set; }
        public string AutodeskProduct { get; set; }
        public DateTime? QualificationDate { get; set; }
        public string QualificationType { get; set; }
        public string Comments { get; set; }
        public string Guid { get; set; }
    }
}
