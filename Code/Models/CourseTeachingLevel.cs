﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class CourseTeachingLevel
    {
        public int CourseTeachingLevelId { get; set; }
        public string CourseId { get; set; }
        public int? Update { get; set; }
        public int? Essentials { get; set; }
        public int? Intermediate { get; set; }
        public int? Advanced { get; set; }
        public int? Customized { get; set; }
        public int? Other { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
    }
}
