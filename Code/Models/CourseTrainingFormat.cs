﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class CourseTrainingFormat
    {
        public int CourseTrainingFormatId { get; set; }
        public string CourseId { get; set; }
        public int InstructorLed { get; set; }
        public int Online { get; set; }
        public string Status { get; set; }
    }
}
