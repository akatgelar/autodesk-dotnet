﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class LmsspecializationHistory
    {
        public string ContactId { get; set; }
        public string Industry { get; set; }
        public string RoleName { get; set; }
        public DateTime SpecializationStatusDate { get; set; }
        public string History { get; set; }
        public DateTime PdbdateCreated { get; set; }
        public DateTime PdbdateChanged { get; set; }
    }
}
