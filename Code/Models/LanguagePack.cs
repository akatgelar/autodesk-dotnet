﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using Microsoft.WindowsAzure.Storage.Blob.Protocol;

namespace autodesk.Code.Models
{
    public class LanguagePack
    {
        [Key]
        public int LanguagePackId { get; set; }
        public string LanguageCountry { get; set; }
        public string LanguageJson { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }

}
}
