﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class MainSite
    {
        public int SiteIdInt { get; set; }
        public string SiteId { get; set; }
        public string AtcsiteId { get; set; }
        public string OrgId { get; set; }
        public string SiteStatusRetired { get; set; }
        public string SiteName { get; set; }
        public string SiebelSiteName { get; set; }
        public string EnglishSiteName { get; set; }
        public string CommercialSiteName { get; set; }
        public int? Workstations { get; set; }
        public string MagellanId { get; set; }
        public string SapnumberRetired { get; set; }
        public string SapshipToRetired { get; set; }
        public string SiteTelephone { get; set; }
        public string SiteFax { get; set; }
        public string SiteEmailAddress { get; set; }
        public string SiteWebAddress { get; set; }
        public string SiteDepartment { get; set; }
        public string SiteAddress1 { get; set; }
        public string SiteAddress2 { get; set; }
        public string SiteAddress3 { get; set; }
        public string SiteCity { get; set; }
        public string SiteStateProvince { get; set; }
        public string SiteCountryCode { get; set; }
        public string SitePostalCode { get; set; }
        public string MailingDepartment { get; set; }
        public string MailingAddress1 { get; set; }
        public string MailingAddress2 { get; set; }
        public string MailingAddress3 { get; set; }
        public string MailingCity { get; set; }
        public string MailingStateProvince { get; set; }
        public string MailingCountryCode { get; set; }
        public string MailingPostalCode { get; set; }
        public string ShippingDepartment { get; set; }
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingAddress3 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingStateProvince { get; set; }
        public string ShippingCountryCode { get; set; }
        public string ShippingPostalCode { get; set; }
        public string SiebelDepartment { get; set; }
        public string SiebelAddress1 { get; set; }
        public string SiebelAddress2 { get; set; }
        public string SiebelAddress3 { get; set; }
        public string SiebelCity { get; set; }
        public string SiebelStateProvince { get; set; }
        public string SiebelCountryCode { get; set; }
        public string SiebelPostalCode { get; set; }
        public string SiteAdminFirstName { get; set; }
        public string SiteAdminLastName { get; set; }
        public string SiteAdminEmailAddress { get; set; }
        public string SiteAdminTelephone { get; set; }
        public string SiteAdminFax { get; set; }
        public string SiteManagerFirstName { get; set; }
        public string SiteManagerLastName { get; set; }
        public string SiteManagerEmailAddress { get; set; }
        public string SiteManagerTelephone { get; set; }
        public string SiteManagerFax { get; set; }
        public string AdminNotes { get; set; }
        public string Status { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string CsolocationUuid { get; set; }
        public string CsolocationName { get; set; }
        public string CsolocationNumber { get; set; }
        public string CsoauthorizationCodes { get; set; }
        public DateTime? CsoupdatedOn { get; set; }
        public string Csoversion { get; set; }
        public string CsopartnerType { get; set; }
    }
}
