﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class RefLanguage
    {
        public int LanguageId { get; set; }
        public string Name { get; set; }
        public byte EnabledInReports { get; set; }
        public int? ReportOrder { get; set; }
        public string Uiculture { get; set; }
        public byte AskInfo { get; set; }
        public byte PostSurveyEnabled { get; set; }
        public byte EnabledInSurvey { get; set; }
    }
}
