﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class ServiceProviderRoles
    {
        public string RoleCode { get; set; }
        public string PartnerCode { get; set; }
        public string RoleDescription { get; set; }
    }
}
