﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class Courses
    {
        public string CourseId { get; set; }
        public string SiteId { get; set; }
        public string CourseCode { get; set; }
        public string ContactId { get; set; }
        public int ApplicationId { get; set; }
        public string Name { get; set; }
        public string CertificateNumber { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EnrollmentExpireDate { get; set; }
        public string HoursTraining { get; set; }
        public byte? HoursTrainingOther { get; set; }
        public string LocationName { get; set; }
        public string FyindicatorKey { get; set; }
        public string TrainingTypeKey { get; set; }
        public string ProjectTypeKey { get; set; }
        public string EventTypeKey { get; set; }
        public string EventType2 { get; set; }
        public short? Atcfacility { get; set; }
        public short? Atccomputer { get; set; }
        public string Institution { get; set; }
        public byte[] InstitutionLogo { get; set; }
        public string LogoName { get; set; }
        public string LogoType { get; set; }
        public int? EvaluationQuestionId { get; set; }
        public int? CertificateId { get; set; }
        public string Status { get; set; }
        public string PartnerType { get; set; }
    }
}
