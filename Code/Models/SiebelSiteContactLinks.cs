﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class SiebelSiteContactLinks
    {
        public string ContactCsn { get; set; }
        public string AccountCsn { get; set; }
    }
}
