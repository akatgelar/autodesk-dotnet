﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class IntegrityOk
    {
        public string IntegrityCheck { get; set; }
        public string MasterItemId { get; set; }
        public string OtherItemId { get; set; }
        public string Notes { get; set; }
        public DateTime DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
    }
}
