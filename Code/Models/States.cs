﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class States
    {
        public int StateIdint { get; set; }
        public int StateId { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }
        public string SubCountry { get; set; }
        public string CountryCode { get; set; }
    }
}
