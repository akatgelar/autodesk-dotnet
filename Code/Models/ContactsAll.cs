﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace autodesk.Code.Models
{
    public partial class ContactsAll
    {
        [Key]
        public int ContactIdInt { get; set; }
        public string ContactId { get; set; }
        public string SiebelId { get; set; }
        public string SiebelStatus { get; set; }
        public string PrimarySiteId { get; set; }
        public string ContactName { get; set; }
        public string EnglishContactName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Salutation { get; set; }
        public string TitlePosition { get; set; }
        public string EmailAddress { get; set; }
        public string EmailAddress2 { get; set; }
        public string EmailAddress3 { get; set; }
        public string EmailAddress4 { get; set; }
        public string DoNotEmail { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string PasswordPlain { get; set; }
        public string PwdchangeRequired { get; set; }
        public string PermissionsGroup { get; set; }
        public DateTime? HireDate { get; set; }
        public string HowLongWithOrg { get; set; }
        public string HowLongInIndustry { get; set; }
        public string HowLongAutodeskProducts { get; set; }
        public string ExperiencedCompetitiveProducts { get; set; }
        public string ExperiencedCompetitiveProductsList { get; set; }
        public DateTime? QuestionnaireDate { get; set; }
        public string PercentPreSales { get; set; }
        public string PercentPostSales { get; set; }
        public string Dedicated { get; set; }
        public string Department { get; set; }
        public string InternalSales { get; set; }
        public int? PrimaryRoleId { get; set; }
        public string PrimaryIndustryId { get; set; }
        public string PrimarySubIndustryId { get; set; }
        public string PrimaryRoleExperience { get; set; }
        public string Atcrole { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Mobile1 { get; set; }
        public string Mobile2 { get; set; }
        public string PrimaryLanguage { get; set; }
        public string SecondaryLanguage { get; set; }
        public string Timezone { get; set; }
        public float? TimeZoneUtc { get; set; }
        public string Comments { get; set; }
        public string InstructorId { get; set; }
        public short? LoginCount { get; set; }
        public DateTime? LastLogin { get; set; }
        public string LastIpaddress { get; set; }
        public string Status { get; set; }
        public string CsouserUuid { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string Action { get; set; }
        public DateTime? CsoupdatedOn { get; set; }
        public string Csoversion { get; set; }
        public string Administrator { get; set; }
        public string ShowDuplicates { get; set; }
        public string Level { get; set; }
        public string AdminSystems { get; set; }
        public string CompanyName { get; set; }
        public string ResetToken { get; set; }
        public DateTime? ResetTokenExpiration { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string CountryCode { get; set; }
        public string StateProvince { get; set; }
        public string Gender { get; set; }
        public string Designation { get; set; }
        public string WebsiteUrl { get; set; }
        public string Bio { get; set; }
        public string ShowInSearch { get; set; }
        public string ShareEmail { get; set; }
        public string ShareTelephone { get; set; }
        public string ShareMobile { get; set; }
        public int? AcimemberId { get; set; }
        public string MobileCode { get; set; }
        public string TelephoneCode { get; set; }
        public string ProfilePicture { get; set; }
        public string UserLevelId { get; set; }
        public string ResetPasswordCode { get; set; }
        public DateTime? ResetPasswordDate { get; set; }
        public DateTime? DateBirth { get; set; }
        public int? StatusEmailSend { get; set; }
        public string TerminationReason { get; set; }
    }

}
