﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class LayoutLanguage
    {
        public int LanguageId { get; set; }
        public string LanguageCode { get; set; }
        public string English { get; set; }
        public string Bahasa { get; set; }
        public string Chinese { get; set; }
    }
}
