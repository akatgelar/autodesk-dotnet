﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace autodesk.Code.Models
{
    public partial class Invoices
    {
        public int InvoiceId { get; set; }
        public string OrgId { get; set; }
        public string InvoiceNumber { get; set; }
        public decimal? DistributorFee { get; set; }
        public string DocuSign { get; set; }
        public string ContractId { get; set; }
        public string InvoiceType { get; set; }
        public string InvoiceDescription { get; set; }
        public string ContractStatus { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public DateTime? PoreceivedDate { get; set; }
        public string Poid { get; set; }
        public decimal? TotalInvoicedAmount { get; set; }
        public decimal? TotalLineAmount { get; set; }
        public string InvoicedCurrency { get; set; }
        public decimal? TotalVatamount { get; set; }
        public decimal? TotalPaymentAmount { get; set; }
        public decimal? AmountOutstanding { get; set; }
        public decimal? PaymentAmount { get; set; }
        public DateTime? PaymentDate { get; set; }
        public int? PaymentMonth { get; set; }
        public string PaymentCurrency { get; set; }
        public string PaymentMethod { get; set; }
        public string PaymentReference { get; set; }
        public string CcexpDate { get; set; }
        public string Ccname { get; set; }
        public decimal? PaymentAmount2 { get; set; }
        public DateTime? PaymentDate2 { get; set; }
        public int? PaymentMonth2 { get; set; }
        public string PaymentMethod2 { get; set; }
        public string PaymentReference2 { get; set; }
        public string CcexpDate2 { get; set; }
        public string Ccname2 { get; set; }
        public string SiteIdList { get; set; }
        public string L1Summary { get; set; }
        public string L1Description { get; set; }
        public int? L1Quantity { get; set; }
        public decimal? L1UnitPrice { get; set; }
        public decimal? L1TotalAmount { get; set; }
        public decimal? L1Vatrate { get; set; }
        public decimal? L1Vatamount { get; set; }
        public string L2Summary { get; set; }
        public string L2Description { get; set; }
        public int? L2Quantity { get; set; }
        public decimal? L2UnitPrice { get; set; }
        public decimal? L2TotalAmount { get; set; }
        public decimal? L2Vatrate { get; set; }
        public decimal? L2Vatamount { get; set; }
        public string L3Summary { get; set; }
        public string L3Description { get; set; }
        public int? L3Quantity { get; set; }
        public decimal? L3UnitPrice { get; set; }
        public decimal? L3TotalAmount { get; set; }
        public decimal? L3Vatrate { get; set; }
        public decimal? L3Vatamount { get; set; }
        public string L4Summary { get; set; }
        public string L4Description { get; set; }
        public int? L4Quantity { get; set; }
        public decimal? L4UnitPrice { get; set; }
        public decimal? L4TotalAmount { get; set; }
        public decimal? L4Vatrate { get; set; }
        public decimal? L4Vatamount { get; set; }
        public string L5Summary { get; set; }
        public string L5Description { get; set; }
        public int? L5Quantity { get; set; }
        public decimal? L5UnitPrice { get; set; }
        public decimal? L5TotalAmount { get; set; }
        public decimal? L5Vatrate { get; set; }
        public decimal? L5Vatamount { get; set; }
        public string L6Summary { get; set; }
        public string L6Description { get; set; }
        public int? L6Quantity { get; set; }
        public decimal? L6UnitPrice { get; set; }
        public decimal? L6TotalAmount { get; set; }
        public decimal? L6Vatrate { get; set; }
        public decimal? L6Vatamount { get; set; }
        public string L7Summary { get; set; }
        public string L7Description { get; set; }
        public int? L7Quantity { get; set; }
        public decimal? L7UnitPrice { get; set; }
        public decimal? L7TotalAmount { get; set; }
        public decimal? L7Vatrate { get; set; }
        public decimal? L7Vatamount { get; set; }
        public string L8Summary { get; set; }
        public string L8Description { get; set; }
        public int? L8Quantity { get; set; }
        public decimal? L8UnitPrice { get; set; }
        public decimal? L8TotalAmount { get; set; }
        public decimal? L8Vatrate { get; set; }
        public decimal? L8Vatamount { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public string OldContractId { get; set; }
        public string Comments { get; set; }
        public DateTime? DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        [Column("Status")]
        public string Status { get; set; }
        public string SiteCountryDistributorSkuid { get; set; }
        public int? NumberOfLicenses { get; set; }
        public string FinancialYear { get; set; }
        public string LastUpdateBy { get; set; }
        public DateTime? DateLastUpdate { get; set; }
        public string HasMainSKU { get; set; }

        public string RejectReason { get; set; }
    }
}
