﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class RoleCertificateType
    {
        public int RoleCertificateTypeId { get; set; }
        public string RoleCertificateType1 { get; set; }
    }
}
