﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class LmslanguageEquivalentCourses
    {
        public string LeparentCode { get; set; }
        public string LeparentTitle { get; set; }
        public string EquivalentCode { get; set; }
        public string EquivalentTitle { get; set; }
        public string Guid { get; set; }
        public string Langugage { get; set; }
    }
}
