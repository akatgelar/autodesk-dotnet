﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class TypeOfTrainings
    {
        public int TypeOfTrainingId { get; set; }
        public string TypeOfTraining { get; set; }
    }
}
