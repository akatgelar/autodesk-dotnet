﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class ProductsOriginal
    {
        public string ProductId { get; set; }
        public string FamilyId { get; set; }
        public string ProductName { get; set; }
        public string Status { get; set; }
    }
}
