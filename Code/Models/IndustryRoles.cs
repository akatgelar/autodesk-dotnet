﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class IndustryRoles
    {
        public string IndustryCode { get; set; }
        public string RoleCode { get; set; }
    }
}
