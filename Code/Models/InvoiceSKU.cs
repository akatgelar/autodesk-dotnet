﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models
{
    public partial class InvoiceSku
    {
        public int InvoiceSkuid { get; set; }
        public int InvoiceId { get; set; }
        public double? TotalAmmount { get; set; }
        [Column("QTY")]
        public int? Qty { get; set; }
        public string SiteCountryDistributorSkuid { get; set; }
        public int PricingSKUId { get; set; }
        public string SiteId { get; set; }
    }
}
