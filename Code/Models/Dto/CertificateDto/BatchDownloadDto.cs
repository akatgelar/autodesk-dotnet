﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto.CertificateDto
{
    public class BatchDownloadDto
    {
        public int PartnerTypeId { get; set; }
        public int CertificateTypeId { get; set; }
        public string Year { get; set; }
        public string LanguageCertificate { get; set; }
        public IEnumerable<string> StudentIds { get; set; }
        public string CourseId { get; set; }
    }
}
