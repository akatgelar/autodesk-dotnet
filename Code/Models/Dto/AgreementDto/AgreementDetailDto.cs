﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto
{
    public class AgreementDetailDto
    {
        public int AgreementMasterId { get; set; }  
        public string FY { get; set; }
        public string OrgId { get; set; }
        public string OrgName { get; set; }
        public string Status { get; set; }
        public string LegalName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string PartnerType { get; set; }
        public bool? CheckTC { get; set; }
        public string SubmissionPerson { get; set; }
        public string Address { get; set; }
        public string ExecuteBy { get; set; }
        public List<AgreementRemarkDto> RemarkHistories { get; set; }
        
    }
}
