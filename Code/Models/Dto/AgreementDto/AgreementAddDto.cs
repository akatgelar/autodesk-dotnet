﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto
{
    public class AgreementAddDto
    {
        public string OrgId { get; set; }
        public string JobTitle { get; set; }
        //public string LegalName { get; set; }
        public bool? CheckTC { get; set; }
        public string  UserId { get; set; }
        public int AgreementMasterId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
