﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto
{
    public class OrganizationInfoDto
    {
        public string OrgName { get; set; }
        public string EnglishOrgName { get; set; }
        public string RegisteredAddress { get; set; }
    }
}
