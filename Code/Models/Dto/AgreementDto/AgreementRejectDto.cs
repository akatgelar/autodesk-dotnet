﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto
{
    public class AgreementRejectDto
    {
        public int AgreementMasterId { get; set; }
        public string Remark { get; set; }
        public string UserId { get; set; }
    }
}
