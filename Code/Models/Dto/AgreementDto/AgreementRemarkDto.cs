﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace autodesk.Code.Models.Dto
{
    
    public class AgreementRemarkDto
    {
        public int AgreementRemarkId { get; set; }
        public int AgreementMasterId { get; set; }
        public string Remark { get; set; }
        public string Action { get; set; }
        public string RecordBy { get; set; }
        public DateTime RecordDateTime { get; set; }
    }
}
