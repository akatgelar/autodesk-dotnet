﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto
{
    public class SearchDto
    {
    }
    public class GeneralSearchDto
    {
        //public string id_org { get; set; }
        //public string id_site { get; set; }
        //public string id_contact { get; set; }
        //public string name_org { get; set; }
        //public string name_site { get; set; }
        //public string name_contact { get; set; }
        //public string address_org { get; set; }
        //public string address_site { get; set; }
        //public string email_org { get; set; }
        //public string email_site { get; set; }
        //public string email_contact { get; set; }
        //public string notes_org { get; set; }
        //public string notes_site { get; set; }
        //public string notes_contact { get; set; }
        //public string CSN_org { get; set; }
        //public string CSN_site { get; set; }
        //public string contact_org { get; set; }
        //public string contact_site { get; set; }
        //public string invoices_org { get; set; }

        public string searchText { get; set; }
        public string showDataBy { get; set; }

    }

    public class OrgData
    {
        public string address_org { get; set; }

        public string contact_org { get; set; }
        public string CSN_org { get; set; }
        public string email_org { get; set; }
        public string notes_org { get; set; }
        public string name_org { get; set; }
        public string id_org { get; set; }
        public string invoices_org { get; set; }

        public bool IsNull()
        {
            if (string.IsNullOrEmpty(address_org) && string.IsNullOrEmpty(contact_org) && string.IsNullOrEmpty(CSN_org) && string.IsNullOrEmpty(email_org) && string.IsNullOrEmpty(notes_org) && string.IsNullOrEmpty(name_org) && string.IsNullOrEmpty(id_org) && string.IsNullOrEmpty(invoices_org))
            {
                return true;
            }
            return false;
        }
    }
    public class SiteData
    {
        public string address_site { get; set; }
        public string contact_site { get; set; }
        public string CSN_site { get; set; }
        public string email_site { get; set; }
        public string notes_site { get; set; }
        public string name_site { get; set; }
        public string id_site { get; set; }
        public bool IsNull()
        {
            if (string.IsNullOrEmpty(address_site) && string.IsNullOrEmpty(contact_site) && string.IsNullOrEmpty(CSN_site) && string.IsNullOrEmpty(email_site) && string.IsNullOrEmpty(notes_site) && string.IsNullOrEmpty(name_site) && string.IsNullOrEmpty(id_site))
            {
                return true;
            }
            return false;
        }
    }

    public class ContactData
    {
        public string email_contact { get; set; }
        public string id_contact { get; set; }
        public string name_contact { get; set; }
        public string notes_contact { get; set; }

        public bool IsNull()
        {
            if (string.IsNullOrEmpty(email_contact) && string.IsNullOrEmpty(notes_contact) && string.IsNullOrEmpty(name_contact) && string.IsNullOrEmpty(id_contact))
            {
                return true;
            }
            return false;
        }
    }
    public class GeneralSearchOrgDto
    {
        public string OrgId { get; set; }
        public int OrganizationId { get; set; }
        public string OrgName { get; set; }
        public string EnglishOrgName { get; set; }
        public string AdminNotes { get; set; }
        public string AtcdirectorEmailAddress { get; set; }
        public string BillingContactEmailAddress { get; set; }
        public string LegalContactEmailAddress { get; set; }
        public string RegisteredAddress1 { get; set; }
        public string RegisteredAddress2 { get; set; }
        public string RegisteredAddress3 { get; set; }

        public string RegisteredCountryCode { get; set; }

        public string RegisteredStateProvince { get; set; }
        public string CountriesCode { get; set; }

        public string GeoName { get; set; }

        public string CountriesName { get; set; }
        public string OrgStatus { get; set; }
        public string BillingContactFirstName { get; set; }
        public string BillingContactLastName { get; set; }
        public string LegalContactFirstName { get; set; }
        public string LegalContactLastName { get; set; }
        public string InvoiceNumber { get; set; }
        public string PartnerType { get; set; }
        public string PartnerTypeStatus { get; set; }
        public string Csn { get; set; }
        string _status => OrgStatus;
        public string Status
        {
            get
            {

                switch (_status)
                {
                    case "A":
                        return "Active";
                    case "I":
                        return "InActive";
                    case "D":
                        return "Deleted";
                    case "X":
                        return "Deleted";
                    case "S":
                        return "Suspended";
                    case "T":
                        return "Terminated";
                    default:
                        return string.Empty;
                }
            }
        }


    }

    public class GeneralSearchOrgResultDto : GeneralSearchOrgDto
    {
        public List<string> FoundInformation { get; set; }
    }

    public class GeneralSearchSiteDto
    {
        public string OrgId { get; set; }
        public int OrganizationId { get; set; }
        public int SiteIdInt { get; set; }
        public string SiteId { get; set; }
        public string SiteName { get; set; }
        public string SiteAddress1 { get; set; }
        public string SiteAddress2 { get; set; }
        public string SiteAddress3 { get; set; }
        public string SiteEmailAddress { get; set; }
        public string SiteAdminEmailAddress { get; set; }
        public string SiteManagerEmailAddress { get; set; }
        public string PartnerType { get; set; }
        public string PartnerTypeStatus { get; set; }
        public string SiteStatus { get; set; }
        public string Csn { get; set; }
        public string AdminNotes { get; set; }
        public string GeoName { get; set; }
        public string CountriesName { get; set; }
        public string SiteAdminFirstName { get; set; }
        public string SiteAdminLastName { get; set; }
        public string SiteManagerFirstName { get; set; }
        public string SiteManagerLastName { get; set; }

    }

    public class GeneralSearchSiteResultDto : GeneralSearchSiteDto
    {
        public List<string> FoundInformation { get; set; }
    }
    public class GeneralSearchContactDto
    {
        public string ContactId { get; set; }
        public int ContactIdInt { get; set; }
        public string ContactName { get; set; }
        public string EmailAddress { get; set; }
        public string CountriesName { get; set; }
        public string CountriesCode { get; set; }
        public int OrganizationId { get; set; }
        public string SiteId { get; set; }
        public string Status { get; set; }
        public string InstructorId { get; set; }

    }
    public class GeneralSearchContactResultDto : GeneralSearchContactDto
    {
        public List<string> FoundInformation { get; set; }
    }
    public class GeneralSearchResult
    {
        public List<GeneralSearchOrgResultDto> Orgs { get; set; }
        public List<GeneralSearchSiteResultDto> Sites { get; set; }
        public List<GeneralSearchContactResultDto> Contacts { get; set; }

    }
}
