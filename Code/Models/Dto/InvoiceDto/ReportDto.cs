﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto.InvoiceDto
{
    public class ReportDto
    {
        public string OrgId { get; set; }
        public string OrgName { get; set; }
        public int SiteIdInt { get; set; }
        public int OrgIdInt { get; set; }
        public string SiteId { get; set; }
        public string SiteName { get; set; }
        public string SKUName { get; set; }
        public string SiteStatus { get; set; }
        public string TerritoryName { get; set; }
        //public string Country { get; set; }
        public string PartnerType { get; set; }
        public string SitePartnerType { get; set; }
        public double? Discount { get; set; }
        public string InvoiceStatus { get; set; }
        //public int? Quantity { get; set; }
        //public int SKUPriceBand { get; set; }
        //public int SKUSRP { get; set; }
        //public double SKUPriceInclDiscount { get; set; }
        //public double? SiteTotal { get; set; }
        //public double? OrgTotal { get; set; }
        //public double? Total { get; set; }
        public string FYIndicator { get; set; }
        public int? InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDescription { get; set; }
        public string CountriesName { get; set; }
        public int? QTY { get; set; }
        public double? TotalAmmount { get; set; }
        public string SKUDescription { get; set; }
        public string PriceBand { get; set; }
        //public string FinancialYear { get; set; }
        public int TerritoryId { get; set; }
        public string CountriesCode { get; set; }
        public int RoleId { get; set; }
        public int MarketTypeId { get; set; }
        public string GeoCode { get; set; }
        public string RegionCode { get; set; }
        public string SubregionCode { get; set; }
        public string DiscountType { get; set; }
        public int? InvoiceSKUId { get; set; }
        public string LastAdminBy { get; set; }
        public DateTime? PaymentReceivedDate { get; set; }
        public string Description { get; set; }
        public string Approver { get; set; }
        public string AmountType { get; set; }
        public DateTime? InvoiceCreatedDate { get; set; }
        public DateTime? InvoiceRejectedDate { get; set; }
        public DateTime? InvoiceApproverDate { get; set; }
        public string InvoiceCreatedBy { get; set; }
        public string InvoiceRejectedBy { get; set; }
        public double? TotalDiscountAmmount { get; set; }
        public double? NumberOfLicenses { get; set; }
        public string SiteTerritory { get; set; }
        public string InvoiceSKUFYIndicatorKey { get; set; }
        public string InvoiceLicense { get; set; }
        public string InvoiceProrateYN { get; set; }
        public string RejectReason { get; set; }
        public string Comments { get; set; }
        public string OrgStatus { get; set; }
    }

    public class ReportOrgDto
    {
        public string OrgId { get; set; }
        public string OrgName { get; set; }
        public string Territory_Name { get; set; }
        public double? Discount { get; set; }
        public string InvoiceStatus { get; set; }
        public string FYIndicatorKey { get; set; }
        public int? InvoiceId { get; set; }
        public string InvoiceNumber { get; set; }
        public string InvoiceDescription { get; set; }
        public string countries_name { get; set; }
        public double? TotalAmount { get; set; }
        public string CountriesCode { get; set; }
        public string DiscountType { get; set; }
        public string ApprovedBy { get; set; }
        public string AmountType { get; set; }
        public DateTime? InvoiceCreatedDate { get; set; }
        public DateTime? PaymentReceiveDate { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string InvoiceCreatedBy { get; set; }
        public string RejectedBy { get; set; }
        public double? TotalInvoiceAmmount { get; set; }
        public double? NumberOfLicenses { get; set; }
        public string RejectReason { get; set; }
        public int? QTY { get; set; }
        public string License { get; set; }
        public double? Price { get; set; }
        public string Comments { get; set; }
        public string OrgStatus { get; set; }
    }

    public class ReportListDto
    {
        public List<ReportDto> Reports { get; set; }
        public List<ReportOrgDto> ReportsOrg { get; set; }
        public List<OrgTotal> OrgTotals { get; set; }
        public double ReportTotalAmount { get; set; }
    }
    public class OrgTotal
    {
        public string OrgId { get; set; }
        public double Total { get; set; }
        public double TotalLicense { get; set; }
        public double? TotalWithDiscount { get; set; }
        public List<SiteTotal> SiteTotal { get; set; }
    }

    public class SiteTotal
    {
        public double Total { get; set; }
        public string SiteId { get; set; }
    }

    public class DiscountTotal
    {
        public int InvoiceId { get; set; }
        public double Amount { get; set; }
    }
}
