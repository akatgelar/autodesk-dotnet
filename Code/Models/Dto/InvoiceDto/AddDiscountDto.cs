﻿namespace autodesk.Code.Models.Dto.InvoiceDto
{
    public class DiscountDto
    {
        public AddDiscountDto Discount { get; set; }
        public AddDiscountDto ProRata { get; set; }
    }
    public class ApplyDiscountDto
    {
        public UpdateDiscountDto Discount { get; set; }
        public UpdateDiscountDto ProRata { get; set; }
    }

    public class AddDiscountDto
    {
        public string Name { get; set; }
        public double? Amount { get; set; }
        public string AmountType { get; set; }
        public int? InvoiceId { get; set; }
    }

    public class UpdateDiscountDto
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public double? Amount { get; set; }
        public string AmountType { get; set; }
        public int? InvoiceId { get; set; }
    }
}
