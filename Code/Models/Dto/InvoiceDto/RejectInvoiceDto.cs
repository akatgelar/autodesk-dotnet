﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto.InvoiceDto
{
    public class RejectInvoiceDto
    {
        public string Email { get; set; }
        public int InvoiceId { get; set; }
        public string RejectReason { get; set; }
    }
}
