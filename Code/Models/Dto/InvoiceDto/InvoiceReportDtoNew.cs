﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto.InvoiceDto
{
    public class InvoiceReportDtoNew
    {
        public int OrgIdInt { get; set; }
        public int SiteIdInt { get; set; }
       
        public string OrgId { get; set; }
        public string SiteOrgId { get; set; }
        public string InvoiceOrgId { get; set; }
        public string OrgStatus { get; set; }
        public string OrgName { get; set; }
        public string SiteId { get; set; }
        public string TerritoryName { get; set; }
        public string CountriesName { get; set; }
        public string AmountType { get; set; }
        public double? Discount { get; set; }
        public string InvoiceNumber { get; set; }
        public string FYIndicatorKey { get; set; }
        public string RejectReason { get; set; }
        public DateTime? PaymentReceiveDate { get; set; }
        public DateTime? InvoiceCreatedDate { get; set; }
        public string InvoiceStatus { get; set; }
        public double? TotalInvoiceAmmount { get; set; }
        public double? NumberOfLicenses { get; set; }
        public string InvoiceCreatedBy { get; set; }
        public int? ContactIdInt { get; set; }
        public string SKUDescription { get; set; }
        public string ApprovedBy { get; set; }
        public string RejectedBy { get; set; }
        public string Comments { get; set; }
        public DateTime? DateLastAdmin { get; set; }
        public string Status { get; set; }
        public string InvoiceId { get; set; }
        public int? InvoiceSKUId { get; set; }
        public DateTime? InvoiceApprovedDate { get; set; }
        public DateTime? InvoiceRejectedDate { get; set; }
        public string SiteStatus { get; set; }
        public string PartnerType { get; set; }
        public int RoleId { get; set; }
        public double? OrgTotalDiscount { get; set; }
        public string SiteName { get; set; }
        public string SiteCountry { get; set; }
        public string SiteTerritory { get; set; }
        public string SKUName { get; set; }
        public string SKUFY { get; set; }
        public int? SKUQTY { get; set; }
        public string ProratedYN { get; set; }
        public string PriceBand { get; set; }
        public string InvoiceDesciption { get; set; }
        public int? QTYPerSetup { get; set; }


    }

    public class ReportDataDto
    {
        public object WebInfo { get; set; }
        public object ExcelOrgInfo { get; set; }
        public object ExcelSiteInfo { get; set; }
        
    }
}
