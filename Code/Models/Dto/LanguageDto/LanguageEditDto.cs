﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.Models.Dto.LanguageDto
{
    public class LanguageEditDto
    {
        public string Value { get; set; }
        public string EditValue { get; set; }
        public string Cuid { get; set; }
        public string UserId { get; set; }
    }
}
