﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class EvaluationReport
    {
        public int EvaluationReportId { get; set; }
        public string CourseId { get; set; }
        public string StudentId { get; set; }
        public int? NbAnswerCcm { get; set; }
        public int? ScoreCcm { get; set; }
        public int? NbAnswerFr { get; set; }
        public int? ScoreFr { get; set; }
        public int? NbAnswerIq { get; set; }
        public int? ScoreIq { get; set; }
        public int? NbAnswerOe { get; set; }
        public int? ScoreOe { get; set; }
        public int? NbAnswerOp { get; set; }
        public int? ScoreOp { get; set; }
        public string FyindicatorKey { get; set; }
        public string SiteId { get; set; }
        public string CountryCode { get; set; }
        public string GeoCode { get; set; }
        public int? RoleId { get; set; }
        public int? CertificateType { get; set; }
        public int? Month { get; set; }
        public string Status { get; set; }
        public int? Quarter { get; set; }
        public DateTime? DateSubmitted { get; set; }
        public DateTime? ActualDateSubmitted { get; set; }
    }
}
