﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class TblPostSurveyMatchQuestions
    {
        public int PostSurveyMatchQuestions { get; set; }
        public int? SurveyId { get; set; }
        public int? PostSurveyId { get; set; }
        public string PostSurveyName { get; set; }
        public string SurveyName { get; set; }
        public int? QuestionId { get; set; }
        public int? PostQuestionId { get; set; }
        public string DeviationType { get; set; }
    }
}
