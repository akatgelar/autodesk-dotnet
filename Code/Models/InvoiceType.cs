﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.Models
{
    public partial class InvoiceType
    {
        public int InvoiceTypeId { get; set; }
        public string ActivityType { get; set; }
        public string InvoiceType1 { get; set; }
        public string Description { get; set; }
        public decimal? InvoicedAmount { get; set; }
        public string InvoicedCurrency { get; set; }
        public DateTime? ContractStartDate { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public string Status { get; set; }
        public string Cuid { get; set; }
        public DateTime? Cdate { get; set; }
        public string Muid { get; set; }
        public DateTime? Mdate { get; set; }
    }
}
