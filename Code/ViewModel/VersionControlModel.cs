﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ViewModel
{
    public class VersionControlModel
    {
        public int Id { get; set; }
        public decimal Version { get; set; }
        public DateTime? DateTimes { get; set; }
    }
}
