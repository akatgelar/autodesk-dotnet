﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ViewModel
{
    public class ActivitiesViewMode
    {

        public int ActivityId { get; set; }
        public DateTime DateAdded { get; set; }
        public string AddedBy { get; set; }
        public DateTime DateLastAdmin { get; set; }
        public string LastAdminBy { get; set; }
        public string Status { get; set; }
        public string ActivityName { get; set; }
        public string ActivityType { get; set; }
        [DefaultValue(" ")]
        public string IsUnique { get; set; }
        [DefaultValue(" ")]
        public string Description { get; set; }





    }
}
