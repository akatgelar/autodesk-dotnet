﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ViewModel
{
    public class ProductDropdownViewModel
    {
        public List<PrimaryProduct> primaryProducts { get; set; }
        public List<SecondaryProduct> SecondaryProducts { get; set; }
    }

    public class PrimaryProduct
    {
        public int Id { get; set; }
        public string itemName { get; set; }
    }

    public class SecondaryProduct
    {
        public int Id { get; set; }
        public string itemName { get; set; }

    }
}
