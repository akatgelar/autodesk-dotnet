﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ViewModel
{
    public class BrowserVersionModel
    {
        public string Name { get; set; }
        public int Version { get; set; }
    }
}
