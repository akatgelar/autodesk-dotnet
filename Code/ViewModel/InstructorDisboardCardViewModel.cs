﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ViewModel
{
    public class InstructorDisboardCardViewModel
    {
        public CourseEndThisWeekViewModel CourseEndThisWeekViewModels { get; set; }
        public EvalEndThisWeekViewModel EvalEndThisWeekViewModels { get; set; }
        public StudentEnrolThisWeekViewModel StudentEnrolThisWeekViewModels { get; set; }
    }

    public class CourseEndThisWeekViewModel
    {
        public int Count { get; set; }
        public List<CourseEnd> DataList { get; set; }
    }

    public class EvalEndThisWeekViewModel
    {
        public int Count { get; set; }
        public List<EvaleEnd> DataList { get; set; }
    }

    public class StudentEnrolThisWeekViewModel
    {
        public int Count { get; set; }
        public List<StudentEnrol> DataList { get; set;}
    }

    public class CourseEnd
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string SDate { get; set; }
        public string EDate { get; set; }
    }
    public class EvaleEnd
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string SDate { get; set; }
        public string EDate { get; set; }
    }
    public class StudentEnrol
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CourseName { get; set; }
        public string SDate { get; set; }
        public string EDate { get; set; }
    }
}
