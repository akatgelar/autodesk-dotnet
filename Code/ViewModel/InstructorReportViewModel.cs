﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ViewModel
{
    public class InstructorReportViewModel
    {
        public string ContactID { get; set; }
        public string InstructorID { get; set; }
        public string Name { get; set; }
        public List<SiteViewModel> SiteId { get; set; }
        public string CurrentUserId { get; set; }
        public string RequestedSiteId { get; set; }
        public string SelectedRespondType { get; set; }
        public string Reason { get; set; }
        public string Status { get; set; }
    }

    
}
