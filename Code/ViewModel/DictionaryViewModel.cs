﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ViewModel
{
    public class DictionaryViewModel
    {
        public string Key { get; set; }
        public string Name { get; set; }
    }
}
