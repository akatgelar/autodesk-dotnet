﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ViewModel
{
    public class RequesterViewModel
    {
        public string ContactId { get; set; }
        public string Name { get; set; }
        public string SiteName { get; set; }
        public string UserLevel { get; set; }
        public List<RequestedInstructorList> RequestedInstructorLists { get; set; }
    }
}
