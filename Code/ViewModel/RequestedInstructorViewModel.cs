﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ViewModel
{
    public class RequestedInstructorViewModel
    {
        //public SiteViewModel SiteViewModel { get; set; }
        public List<RequesterViewModel> RequesterLists { get; set; }
    }

    public class RequestedInstructorList
    {
        public int RequestID { get; set; }
        public string ContactId { get; set; }
        public string ContactName { get; set; }
        public string RequestedSiteId { get; set; }
        public string ReasonType { get; set; }
        public string Reason { get; set; }
        public RequesterViewModel Requester { get; set; }

    }

    
}
