using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Linq;
using autodesk.Code.BackUpDbContext;
using autodesk.Code.CommonServices;
using autodesk.Code.Models;
using autodesk.Code.ViewModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using Certificate = autodesk.Code.Models.Certificate;
using EvaluationQuestion = autodesk.Code.Models.EvaluationQuestion;
using LanguagePack = autodesk.Code.Models.LanguagePack;

namespace autodesk.Code.DataServices
{
 using System.Text.RegularExpressions;

 using ObjectsComparer;

 public class DataServices:IDataServices
    {
       private MySqlDb oDb;//= new MySqlDb("autodesk", true);
       private DataSet ds = new DataSet();
       private MySqlCommand sqlCommand = new MySqlCommand();
       private readonly MySQLContext db;
       private readonly backupdbContext backupdbContext;
       private Comparer objectComparer;
       private readonly ICommonServices commonService;


  public DataServices(MySqlDb sqlDb,MySQLContext _db)
        {
            oDb = sqlDb;
            db = _db;
            backupdbContext = new backupdbContext();
            this.objectComparer = new Comparer(new ComparisonSettings { UseDefaultIfMemberNotExist = true });
            this.commonService = new CommonServices.CommonServices();
        }

        public List<T> GetProductPrimaryorSecondary<T>(string name)
        {
            List<T> res = new List<T>();
            if (!String.IsNullOrEmpty(name))
            {
                if (name.ToLower().Equals("primary"))
                {

                    var cmd = "select p.productId, p.productName FROM Products p " +
                    "INNER JOIN ProductVersions pv ON p.productId = pv.productId INNER JOIN ProductFamilies pf ON p.productId = pf.productId INNER JOIN Family f ON pf.familyId = f.familyId WHERE p.`Status` = 'A' AND pv.`Status` = 'A' GROUP BY p.productId ORDER BY p.productName ASC";
                    sqlCommand = new MySqlCommand(cmd.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    if (ds.Tables.Count > 0)
                    {
                        var primaryProducts = ds.Tables[0].Select().AsEnumerable().Select(row => new PrimaryProduct
                            { Id = Convert.ToInt32(row["productId"].ToString()), itemName = row["productName"].ToString() }).Cast<T>().ToList();
                        res = primaryProducts.Cast<T>().ToList();
                    }
                    else
                    {
                        res = null;
                    }

                }
                else
                {
                    var cmd = "select p.productId, p.productName FROM Products p " +
                              "INNER JOIN ProductVersions pv ON p.productId = pv.productId INNER JOIN ProductFamilies pf ON p.productId = pf.productId INNER JOIN Family f ON pf.familyId = f.familyId WHERE p.`Status` = 'A' AND pv.`Status` = 'A' GROUP BY p.productId ORDER BY p.productName ASC";
                    sqlCommand = new MySqlCommand(cmd.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    if (ds.Tables.Count > 0)
                    {
                        var productList = ds.Tables[0].Select().AsEnumerable().Select(row => new SecondaryProduct
                            {
                                Id = Convert.ToInt32(row["productId"].ToString()),
                                itemName = row["productName"].ToString()
                            })
                            .Cast<T>().ToList();
                        res = productList;
                    }
                    else
                    {
                        res = null;
                    }

                }
            }
            else
            {
                res = null;
            }

            return res;
        }

        //public DataSet EscapeSpecialChar(DataSet ds)
        //{
        //    int n;
        //    int x;
        //    int y;

        //    for (n = 0; n < ds.Tables.Count; n++)
        //    {
        //        DataTable temp = ds.Tables[n];
        //        for (x = 0; x < temp.Rows.Count; x++)
        //        {
        //            DataRow dr = temp.Rows[x];
        //            for (y = 0; y < temp.Columns.Count; y++)
        //            {
        //                DataColumn dc = temp.Columns[y];

        //                if (dr[y].ToString().Contains(@"\"))
        //                {

        //                    dr[y] = dr[y].ToString().Replace(@"\", "\\");
        //                }

        //                if (dr[y].ToString().Contains("\""))
        //                {
        //                    dr[y] = dr[y].ToString().Replace("\"", "\\\"");
        //                }

        //                if (dr[y].ToString().Contains("\\\\"))
        //                {
        //                    dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
        //                }

        //                if (dr[y].ToString().Contains("\\\\\""))
        //                {
        //                    dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
        //                }

        //                if (dr[y].ToString().Contains("\t"))
        //                {
        //                    dr[y] = dr[y].ToString().Replace("\t", "\\t");
        //                }

        //                if (dr[y].ToString().Contains("\n"))
        //                {
        //                    dr[y] = dr[y].ToString().Replace("\n", "\\n");
        //                }

        //                if (dr[y].ToString().Contains("\r"))
        //                {
        //                    dr[y] = dr[y].ToString().Replace("\r", "\\r");
        //                }

        //                if (dr[y].ToString().Contains("\b"))
        //                {
        //                    dr[y] = dr[y].ToString().Replace("\b", "\\b");
        //                }

        //                if (dr[y].ToString().Contains("\f"))
        //                {
        //                    dr[y] = dr[y].ToString().Replace("\f", "\\f");
        //                }

        //                if (dr[y].ToString().Contains("/"))
        //                {
        //                    dr[y] = dr[y].ToString().Replace("/", "\\/");
        //                }
        //            }
        //        }
        //    }

        //    return ds;
        //}
        //public IEnumerable<Dictionary<string, object>> ToDictionary(DataTable table)
        //{
        //    string[] columns = table.Columns.Cast<DataColumn>().Select(c => c.ColumnName).ToArray();
        //    IEnumerable<Dictionary<string, object>> result = table.Rows.Cast<DataRow>().Select(dr => columns.ToDictionary(c => c, c => dr[c]));
        //    return result;

        //}

        public class NullToEmptyStringResolver : Newtonsoft.Json.Serialization.DefaultContractResolver
        {
            protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
            {
                return type.GetProperties()
                    .Select(p => {
                        var jp = base.CreateProperty(p, memberSerialization);
                        jp.ValueProvider = new NullToEmptyStringValueProvider(p);
                        return jp;
                    }).ToList();
            }
            
        }

        public class NullToEmptyStringValueProvider : IValueProvider
        {
            PropertyInfo _MemberInfo;
            
            public NullToEmptyStringValueProvider(PropertyInfo memberInfo)
            {
                _MemberInfo = memberInfo;
            }

            public object GetValue(object target)
            {
                object result = _MemberInfo.GetValue(target);
                if (result == null)
                {
                    result = "";
                }
                return result;

            }

            public void SetValue(object target, object value)
            {
                _MemberInfo.SetValue(target, value);
            }
        }

        public List<T> DataTableToList<T>(DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.Select().AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }
  
        public T DataTableType<T>(DataTable table) where T : class, new()
        {
         try
         {
          T model = new T();
          var allCols = table.Columns
           .Cast<DataColumn>()
           .Select(col => col.ColumnName)
           .ToArray();

          foreach (var row in table.Select().AsEnumerable())
          {       

           foreach (var prop in model.GetType().GetProperties())
           {
            try
            {
             PropertyInfo propertyInfo = model.GetType().GetProperty(prop.Name);
             if (allCols.Any(c => c.Equals(prop.Name)) && row[prop.Name] != null)
             {
              propertyInfo.SetValue(model, Convert.ChangeType(row[prop.Name], Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType), null);
             }

            }
            catch
            {
             break;
            }
           }
          }

          return model;
         }
         catch
         {
          return null;
         }
        }

        public List<dynamic> ToDynamic(DataTable dt)
        {
            var dynamicDt = new List<dynamic>();
            int i = 0;
            foreach (DataRow row in dt.Rows)
            {
                dynamic dyn = new ExpandoObject();
                dynamicDt.Add(dyn);
                foreach (DataColumn column in dt.Columns)
                {
                    var dic = (IDictionary<string, object>)dyn;
                    var isNull = row[column];
                    var type = isNull.GetType();

                    if (type!=typeof(DBNull) && type!=null)
                    {
                        dic[column.ColumnName] = isNull;
                    }
                    else
                    {
                        isNull = "";
                        dic[column.ColumnName] = isNull;
                    }
                }

                i++;
            }
            return dynamicDt;
        }

        public DataTable DynamicToDataTable(List<dynamic> list)
        {
            if (list == null || list.Count == 0)
            {
                return null;
            }

            //build columns
            var props = (IDictionary<string, object>)list[0];
            var t = new DataTable();
            foreach (var prop in props)
            {
                t.Columns.Add(new DataColumn(prop.Key, typeof(string)));
            }
            //add rows
            foreach (var row in list)
            {
                var data = t.NewRow();
                foreach (var prop in (IDictionary<string, object>)row)
                {
                    if (data.Table.Columns.Contains(prop.Key))
                        data[prop.Key] = prop.Value;
                }
                t.Rows.Add(data);
            }
            return t;
        }

        public async Task<RefTerritory> GetERefTerritoryById(int Tid)
        {
            var territory = await Task.Run(() => db.RefTerritory.FirstOrDefault(c=>c.TerritoryId==Tid));

            return territory;

        }

        public async Task<int> GetLookBrowserLimitation(string browserName)
        {
           int browserVersion = 0;
            if (!string.IsNullOrEmpty(browserName))
            {       
                
               var lookUp = await Task.Run(()=> db.LookUp.SingleOrDefault(x => x.ControlName.Equals(browserName)));
                if (lookUp!=null)
                {
                    return lookUp.DataValue;
                }
            }

            return browserVersion;
        }

        public List<ColumnViewModel> GetColumns(Type type)
        {

            var listColumn = type.GetProperties()
                .Select(property => new ColumnViewModel { Field = property.Name, FieName = property.Name }).ToList();
            return listColumn;
        }


        public async Task<VersionControlModel> GetVersion()
        {
            VersionControlModel vcModel = new VersionControlModel();
            vcModel = await db.Versioncontrol.Select(x => new VersionControlModel { Version = x.Version, DateTimes = x.DateTimes }).SingleOrDefaultAsync();

            #region old Code

            //var cmd = "SELECT * FROM versioncontrol";
            //sqlCommand = new MySqlCommand(cmd.ToString());
            //ds = oDb.getDataSetFromSP(sqlCommand);
            //if (ds.Tables.Count > 0)
            //{
            //    vcModel = ds.Tables[0].Select().AsEnumerable().Select(row => new VersionControlModel
            //    {
            //        Version = Convert.ToDecimal(row["Version"].ToString()),
            //        DateTimes = Convert.ToDateTime(row["DateTimes"].ToString())

            //    }).Cast<VersionControlModel>().FirstOrDefault();


            //}

            #endregion


            return vcModel;

        }


        /*
               public bool IsAuthenicate(int UserId,string token)
                {
                    bool isAuthenicated = false;
                    TokenGenerator.TokenGenerator tokenGenerator=new TokenGenerator.TokenGenerator();
                    if (UserId > 0)
                    {
                        var cmd = "SELECT * FROM UserToken WHERE UserId =" + UserId;
                        sqlCommand = new MySqlCommand(cmd.ToString());
                        ds = oDb.getDataSetFromSP(sqlCommand);
                        if (ds.Tables.Count > 0)
                        {
                            List<dynamic> tokenModel = ToDynamic(ds.Tables[0]);
                            var decryptedToken = tokenGenerator.GetDecryptedTokenValue(token);
                            isAuthenicated = tokenModel.Select(x => x.Token).FirstOrDefault() == decryptedToken ? true : false;
                        }
                    }


                    return isAuthenicated;
                }
        */
        public List<dynamic> EvalanswerReformater(DataTable EvalList, string TypeName, DataTable EvalQuestionList)
        {
            var listObject = ToDynamic(EvalList);
            //added by Soe on 28
            //var listObject1 = ToDynamic(EvalQuestionList);

            string key = "";
            bool isAAP = false;
            Dictionary<string, string> qTxtDictionary = new Dictionary<string, string>();
            List<dynamic> formattedList= new List<dynamic>();

            var jasonValues = listObject.Select(x=>new {x.EvaluationAnswerJson, x.EvalID} ).ToList();

            //added by Soe on 28
            // var jasonValues1 = listObject1.Select(x => new { x.EvaluationQuestionJson }).ToList();

            dynamic objQuestion = JsonConvert.DeserializeObject(EvalQuestionList.Rows[0]["EvaluationQuestionJson"].ToString());
            IEnumerable<JToken> questionName = objQuestion.SelectTokens("$..elements[0].elements[0].name");
            IEnumerable<JToken> questionTitle = objQuestion.SelectTokens("$..elements[0].elements[0].title");
            var qName = questionName.AsJEnumerable().Select(x => x).ToList();
            var qTitle = questionTitle.AsJEnumerable().Select(x => x).ToList();

            qTxtDictionary = questionName.Zip(qTitle, (k, v) => new { Key = k, Value = v })
                .ToDictionary(x => x.Key.ToString(), x => x.Value.ToString());

            EvalList.Columns.Remove("EvaluationAnswerJson");
            EvalList.Columns.Remove("EvaluationQuestionCode");

            formattedList = ToDynamic(EvalList);
 
            ICommonServices commonServices = new CommonServices.CommonServices();
            if (TypeName.Equals("ATC"))
            {
                key = "Q";
            }
            else
            {
                key = "QUESTION";
                isAAP = true;
            }

            try
            {
                 foreach (var evalJason in jasonValues)
            {
                
                if (commonServices.IsJasonValid(evalJason.EvaluationAnswerJson) && evalJason.EvaluationAnswerJson != null)
                {                   
                    var obj = JsonConvert.DeserializeObject<dynamic>(evalJason.EvaluationAnswerJson);
                    if (obj!=null)
                    {
                        int k = 0;
                        ExpandoObject objExpandoObject = formattedList.AsEnumerable().FirstOrDefault(x=>x.EvalID.Equals(evalJason.EvalID));
                    
                      
                        foreach (JProperty jObj in obj)
                        {
                            //if (k != arrQuestion.Count - 1) k += 1;
                            //else if (k == arrQuestion.Count) k -= 1;

                            Type t = jObj.Value.GetType();

                            string objName = jObj.Name.ToUpper();
                            if (TypeName.Equals("ATC") && objName.Equals("QUESTION1"))
                            {
                                objName = "Q4" + "." + qTxtDictionary.FirstOrDefault(x => x.Key.Equals("question1")).Value;
                            }
                            else
                            {
                                objName = objName + "." + qTxtDictionary.FirstOrDefault(x => x.Key.Equals(objName.ToLower())).Value;
                            }

                            if (t != typeof(JObject) && t != typeof(JArray))
                            {
                                if (objName.StartsWith("C") && objName.Length>=5)
                                {                              
                                    objName = "COMMENT";
                                }
                                objExpandoObject.TryAdd(objName, jObj.Value.Value<string>());
                            }
                            else if (t == typeof(JObject))
                            {
                                string concatObj=null;
                                int i = 0;
                                int o = 0;
                                foreach (var jObjValue in JObject.Parse(jObj.Value.ToString()))
                                {
                                    if (!String.IsNullOrEmpty(jObjValue.Value.Value<string>()))
                                    {
                                        if (isAAP == false)
                                        {
                                            if (jObj.Name.ToUpper().Equals(key + "5") || jObj.Name.ToUpper().Equals(key + "6") || jObj.Name.ToUpper().Equals(key + "7") || jObj.Name.ToUpper().Equals(key + "12"))
                                            {
                                              
                                                string name = jObj.Name.ToUpper()+"."+jObjValue.Key.ToUpper();

                                                if (jObj.Name.ToUpper().Equals("Q4"))
                                                {
                                                    name = name+"."+ qTxtDictionary.FirstOrDefault(x => x.Key.Equals("question1")).Value;
                                                }
                                                else
                                                {
                                                    name = name + "." + qTxtDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Name.ToLower())).Value;
                                                }
                                                // name = name + " - " + arrQuestion[k].ToString();
                                                switch (jObjValue.Value.Value<string>())
                                                {

                                                    case "100":
                                                        objExpandoObject.TryAdd(name, 5);
                                                        break;
                                                    case "75":
                                                        objExpandoObject.TryAdd(name, 4);
                                                        break;
                                                    case "50":
                                                        objExpandoObject.TryAdd(name, 3);
                                                        break;
                                                    case "25":
                                                        objExpandoObject.TryAdd(name, 2);
                                                        break;
                                                    case "NULL":
                                                        objExpandoObject.TryAdd(name, 1);
                                                        break;
                                                    default:
                                                        objExpandoObject.TryAdd(name, 1);
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                if (i == 0)
                                                {
                                                    concatObj = jObjValue.Value.Value<string>();
                                                }
                                                else
                                                {
                                                    concatObj = concatObj + ", " + jObjValue.Value.Value<string>();
                                                }
                                            }
                                          
                                        }
                                        else
                                        {
                                            if (jObj.Name.ToUpper().Equals(key + "5") || jObj.Name.ToUpper().Equals(key + "6") || jObj.Name.ToUpper().Equals(key + "7") || jObj.Name.ToUpper().Equals(key + "11"))
                                            {
                                            
                                                string name = jObj.Name.ToUpper() + "_" + jObjValue.Key.ToUpper().ToString();
                                                //name = name + " - " + arrQuestion[k].ToString();
                                                name = name + "." + qTxtDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Name.ToLower())).Value;

                                                switch (jObjValue.Value.Value<string>())
                                                {

                                                    case "100":
                                                        objExpandoObject.TryAdd(name, 5);
                                                        break;
                                                    case "75":
                                                        objExpandoObject.TryAdd(name, 4);
                                                        break;
                                                    case "50":
                                                        objExpandoObject.TryAdd(name, 3);
                                                        break;
                                                    case "25":
                                                        objExpandoObject.TryAdd(name, 2);
                                                        break;
                                                    case "NULL":
                                                        objExpandoObject.TryAdd(name, 1);
                                                        break;
                                                    default:
                                                        objExpandoObject.TryAdd(name, 1);
                                                        break;
                                                }

                                            }
                                            else
                                            {
                                                if (i == 0)
                                                {
                                                    concatObj = jObjValue.Value.Value<string>();
                                                }
                                                else
                                                {
                                                    concatObj = concatObj + ", " + jObjValue.Value.Value<string>();
                                                }
                                            }

                                        }

                                        i++;
                                    }
                                  

                                }

                                if (!String.IsNullOrEmpty(concatObj))
                                {
                                    objExpandoObject.TryAdd(objName, concatObj);
                                }
                               
                            }
                            else if(t == typeof(JArray))
                            {
                                string concatObj = null;
                                int i = 0;
                                foreach (var jObjValue in jObj.Value)
                                {
                                    if (!String.IsNullOrEmpty(jObjValue.Value<string>()))
                                    {
                                        if (i == 0)
                                        {
                                            concatObj = jObjValue.Value<string>();
                                        }
                                        else
                                        {
                                            concatObj = concatObj + ", " + jObjValue.Value<string>();
                                        }
                                        i++;
                                    }

                                    
                                }
                                objExpandoObject.TryAdd(objName, concatObj);

                            }
                            else
                            {
                                objExpandoObject.TryAdd(jObj.Name.ToUpper(), jObj.Value);
                            }
                           

                        }

                        formattedList.Remove(objExpandoObject);
                        formattedList.Add(objExpandoObject);
                    }
                }
                else
                {
                    System.Dynamic.ExpandoObject tempExpandoObject = formattedList.AsEnumerable().FirstOrDefault(x => x.EvalID.Equals(evalJason.EvalID));
                    formattedList.Remove(tempExpandoObject);
                    continue;
                }
            }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           


            return formattedList;
        }

        public List<dynamic> EvalquestionReformater(DataTable EvalList, string TypeName)
        {
            var listObject = ToDynamic(EvalList);
            string key = "";
            bool isAAP = false;
            Dictionary<string, string> qTxtDictionary = new Dictionary<string, string>();
            List<dynamic> formattedList = new List<dynamic>();
            dynamic objQuestion = JsonConvert.DeserializeObject(EvalList.Rows[0]["EvaluationQuestionJson"].ToString());
           
            IEnumerable<JToken> questionName = objQuestion.SelectTokens("$..elements[0].elements[0].name");
            IEnumerable<JToken> questionTitle = objQuestion.SelectTokens("$..elements[0].elements[0].title");
          
            var qName = questionName.AsJEnumerable().Select(x => x).ToList();
            var qTitle = questionTitle.AsJEnumerable().Select(x => x).ToList();

            qTxtDictionary = questionName.Zip(questionTitle, (k, v) => new { Key = k, Value = v })
                .ToDictionary(x => x.Key.ToString(), x => x.Value.ToString());

            //create a temporary dictionary
            ExpandoObject objExpandoObject = new ExpandoObject();
            Dictionary<string, string> temp = new Dictionary<string, string>();
            for (int i = 1; i < qTxtDictionary.Count; i++)
            {
                var item = qTxtDictionary.ElementAt(i);
                string itemKey = item.Key.ToString();
                if (itemKey.Contains("question"))
                {
                    if (i > 0)
                    {
                        itemKey = "q" + i;
                    }
                }
                //itemKey = itemKey.Replace("question", "q");
                //if (temp.ContainsKey(itemKey))
                //{
                //    if (i > 0)
                //    {
                //        itemKey = "q" + i;
                //    }
                //}
                objExpandoObject.TryAdd(itemKey, item.Value);
            }
            //foreach (KeyValuePair<string, string> pair in qTxtDictionary)
            //{
            //    string k = pair.Key.ToString();
            //    k=k.Replace("question", "q");
            //    temp.Add(k, pair.Value);
            //}
            qTxtDictionary = temp;
            List<KeyValuePair<string, string>> lst1 = qTxtDictionary.ToList();
            formattedList.Add(objExpandoObject);
            return formattedList;

        }
        public DataTable ListToDatatable(List<dynamic> tmpList)
        {
            if (tmpList.Count == 0 || tmpList == null) return null;
            var tmp = (IDictionary<string, object>)tmpList[0];
            for (int i = 0; i < tmpList.Count; i++)
            {
                tmp = (IDictionary<string, object>)tmpList[i];
                if (tmp.Count > 2)
                {
                    break;
                }
            }
            var tmpdt = new DataTable();
            foreach (var i in tmp)
            {
                tmpdt.Columns.Add(new DataColumn(i.Key, typeof(string)));
            }
            int rIndex = 0;
            Boolean tmpIndex = false;
            foreach (var row in tmpList)
            {
                var tmpData = tmpdt.NewRow();
                foreach (var j in (IDictionary<string, object>)row)
                {
                    if (tmpData.Table.Columns.Contains(j.Key))
                        tmpData[j.Key] = j.Value;
                }
                tmpdt.Rows.Add(tmpData);

                if (tmpdt.Columns.Contains("PID"))
                {
                    if (string.IsNullOrEmpty(tmpdt.Rows[rIndex]["PID"].ToString()))
                    {
                        tmpdt.Rows[rIndex].Delete();
                        tmpIndex = true;
                    }

                }
                if (tmpIndex != true) rIndex += 1;
            }

            if (tmpdt.Columns.Contains("EvalID")) tmpdt.Columns.Remove("EvalID");
            return tmpdt;
        }

        public List<dynamic> EvalanswerReformatertwo(DataTable EvalList,DataTable EvalQuestionList)
        {
            var listObject = ToDynamic(EvalList);
            Dictionary<string, string> qTxtDictionary = new Dictionary<string, string>();
            List<dynamic> formattedList = new List<dynamic>();
            Dictionary<string, string> choiceDictionary = new Dictionary<string, string>();
            var jasonValues = listObject.Select(x => new { x.EvaluationAnswerJson, x.EvalID }).ToList();
            EvalList.Columns.Remove("EvaluationAnswerJson");
            EvalList.Columns.Remove("EvaluationQuestionCode");
            formattedList = ToDynamic(EvalList);
            ICommonServices commonServices = new CommonServices.CommonServices();
            dynamic objQuestion = JsonConvert.DeserializeObject(EvalQuestionList.Rows[0]["EvaluationQuestionJson"].ToString());
            IEnumerable<JToken> questionChoices = objQuestion.SelectTokens("$..elements[0].elements[0].choices");
            var qChoiceValue = questionChoices.AsJEnumerable().Select(x => x).Distinct().ToList();

            foreach (var item in qChoiceValue)
            {
                foreach (var i in item)
                {
                    string tempValue = "";
                    string tempText = "";
                    if (i is JObject)
                    {
                        JObject json = JObject.FromObject(i);
                        foreach (JProperty property in json.Properties())
                        {
                            if (property.Name.Equals("value"))
                            {
                                if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                                {
                                    tempValue = property.Value.ToString();
                                }
                            }
                            if (property.Name.Equals("text"))
                            {
                                if ((property.Value.ToString() != "") && (property.Value.ToString() != null))
                                {
                                    tempText = property.Value.ToString();
                                }
                            }
                        }
                        if (!string.IsNullOrEmpty(tempValue.ToString()) && !choiceDictionary.ContainsKey(tempValue.ToString()))
                        {
                            choiceDictionary.Add(tempValue.ToString(), tempText.ToString());
                        }
                    }
                }
            }
            //string name="";
            foreach (var evalJason in jasonValues)
            {

                if (commonServices.IsJasonValid(evalJason.EvaluationAnswerJson) && evalJason.EvaluationAnswerJson != null)
                {
                    var obj = JsonConvert.DeserializeObject<dynamic>(evalJason.EvaluationAnswerJson);
                    if (obj != null)
                    {
                        int k = 0;
                        ExpandoObject objExpandoObject = formattedList.AsEnumerable().FirstOrDefault(x => x.EvalID.Equals(evalJason.EvalID));
                        objExpandoObject.TryAdd("PID", 1);
                        int tmpIndex = 1;
                        foreach (JProperty jObj in obj)
                        {
                            string name = jObj.Name;
                            Type t = jObj.Value.GetType();
                            
                            if (name.Length >= 3 && name.Contains("question"))
                            {
                                name = name.Replace("question", "q");
                                name = name.Substring(0, 1) + tmpIndex;
                            }
                            if (t != typeof(JObject) && t != typeof(JArray))
                            {
                                if (name.Length >= 3 && name.ToUpper().StartsWith("C"))
                                {
                                    name = "comment";
                                    objExpandoObject.TryAdd(name, jObj.Value.Value<string>());
                                }
                                else
                                {
                                    //objExpandoObject.TryAdd(name, jObj.Value.Value<string>());
                                    objExpandoObject.TryAdd(name, choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Value.Value<string>())).Value);
                                }
                            }
                            
                            else if (t == typeof(JObject))
                            {
                                string concatObj = null;
                                int i = 0;
                                int o = 0;
                                foreach (var jObjValue in JObject.Parse(jObj.Value.ToString()))
                                {
                                    if (name.Equals("q5") || name.Equals("q6") || name.Equals("q7") || name.Equals("q12"))
                                    {
                                        string aliasQ = jObjValue.Key.ToString();
                                        string temp = aliasQ;

                                        if (name.Equals("q5"))
                                        {
                                            switch (aliasQ)
                                            {
                                                case "CCM":
                                                    temp = "Courseware";
                                                    break;
                                                case "FR_1":
                                                    temp = "Computer equipment";
                                                    break;
                                                case "FR_2":
                                                    temp = "Training facility";
                                                    break;
                                                case "IQ":
                                                    temp = "Instructor";
                                                    break;
                                                case "OE":
                                                    temp = "Overall experience";
                                                    break;
                                                default:
                                                    temp = aliasQ;
                                                    break;
                                            }
                                            switch (jObjValue.Value.Value<string>())
                                            {
                                                case "100":
                                                    objExpandoObject.TryAdd(name, "Very Satisfied");
                                                    break;
                                                case "75":
                                                    objExpandoObject.TryAdd(name, "Satisfied");
                                                    break;
                                                case "50":
                                                    objExpandoObject.TryAdd(name, "Neutral");
                                                    break;
                                                case "25":
                                                    objExpandoObject.TryAdd(name, "Dissatisfied");
                                                    break;
                                                case "NULL":
                                                    objExpandoObject.TryAdd(name, "Very dissatisfied");
                                                    break;
                                                default:
                                                    objExpandoObject.TryAdd(name, "Very dissatisfied");
                                                    break;
                                            }
                                        }
                                        else if (name.Equals("q6"))
                                        {
                                            switch (aliasQ)
                                            {
                                                case "CCM":
                                                    temp = "This course";
                                                    break;
                                                case "IQ":
                                                    temp = "This instructor";
                                                    break;
                                                case "FR":
                                                    temp = "The training facility";
                                                    break;
                                                default:
                                                    temp = aliasQ;
                                                    break;
                                            }
                                            switch (jObjValue.Value.Value<string>())
                                            {
                                                case "100":
                                                    objExpandoObject.TryAdd(name, "Extremely likely");
                                                    break;
                                                case "75":
                                                    objExpandoObject.TryAdd(name, "Very likely");
                                                    break;
                                                case "50":
                                                    objExpandoObject.TryAdd(name, "Likely");
                                                    break;
                                                case "25":
                                                    objExpandoObject.TryAdd(name, "Somewhat likely");
                                                    break;
                                                case "NULL":
                                                    objExpandoObject.TryAdd(name, "Not at all likely");
                                                    break;
                                                default:
                                                    objExpandoObject.TryAdd(name, "Not at all likely");
                                                    break;
                                            }
                                        }
                                        else if (name.Equals("q7"))
                                        {
                                            switch (jObjValue.Value.Value<string>())
                                            {
                                                case "100":
                                                    objExpandoObject.TryAdd(name, "Extremely likely");
                                                    break;
                                                case "75":
                                                    objExpandoObject.TryAdd(name, "Very likely");
                                                    break;
                                                case "50":
                                                    objExpandoObject.TryAdd(name, "Somewhat likely");
                                                    break;
                                                case "25":
                                                    objExpandoObject.TryAdd(name, "A little likely");
                                                    break;
                                                case "NULL":
                                                    objExpandoObject.TryAdd(name, "Not at all likely");
                                                    break;
                                                default:
                                                    objExpandoObject.TryAdd(name, "Not at all likely");
                                                    break;
                                            }
                                        }
                                        else if (name.Equals("q12"))
                                        {
                                            switch (aliasQ)
                                            {
                                                case "R1":
                                                    temp = "I learned new knowledge and skills";
                                                    break;
                                                case "R2":
                                                    temp = "I will be able to apply the new skills I learned";
                                                    break;
                                                case "R3":
                                                    temp = "The new skills I learned will improve my performance";
                                                    break;
                                                case "R4":
                                                    temp = "I'm more likely to recommend Autodesk products as a result of this course";
                                                    break;
                                                default:
                                                    temp = aliasQ;
                                                    break;
                                            }
                                            switch (jObjValue.Value.Value<string>())
                                            {
                                                case "100":
                                                    objExpandoObject.TryAdd(name, "Strongly agree");
                                                    break;
                                                case "75":
                                                    objExpandoObject.TryAdd(name, "Agree");
                                                    break;
                                                case "50":
                                                    objExpandoObject.TryAdd(name, "Neutral");
                                                    break;
                                                case "25":
                                                    objExpandoObject.TryAdd(name, "Disagree");
                                                    break;
                                                case "NULL":
                                                    objExpandoObject.TryAdd(name, "Strongly disagree");
                                                    break;
                                                default:
                                                    objExpandoObject.TryAdd(name, "Strongly disagree");
                                                    break;
                                            }
                                        }
                                    }
                                    //if (i == 0)
                                    //{
                                    //    concatObj = jObjValue.Value.Value<string>();
                                    //}
                                    else
                                    {
                                        if (i == 0)
                                        {
                                            concatObj = choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObjValue.Value.Value<string>())).Value;
                                        }
                                        else
                                        {
                                            concatObj = concatObj + ", " + choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObjValue.Value.Value<string>())).Value;
                                        }
                                    }



                                }
                                if (!String.IsNullOrEmpty(concatObj))
                                {
                                    objExpandoObject.TryAdd(name, concatObj);
                                }
                            }
                            else if (t == typeof(JArray))
                            {
                                string concatObj = null;
                                int i = 0;
                                foreach (var jObjValue in jObj.Value)
                                {
                                    if (!String.IsNullOrEmpty(jObjValue.Value<string>()))
                                    {
                                        if (i == 0)
                                        {
                                            concatObj = jObjValue.Value<string>();
                                        }
                                        else
                                        {
                                            if (!concatObj.Contains(jObjValue.Value<string>()))
                                            {
                                                concatObj = concatObj + "," + jObjValue.Value<string>();
                                            }
                                        }
                                        i++;
                                    }

                                    else
                                    {
                                        objExpandoObject.TryAdd(jObj.Name, choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Value)).Value);
                                    }
                                }
                                string[] tmp = null;
                                if (!string.IsNullOrEmpty(concatObj))
                                {
                                    tmp = concatObj.Split(',');
                                    var sort = from s in tmp orderby s select s;
                                    concatObj = null;
                                    foreach (string value in sort)
                                    {
                                        concatObj += value;
                                        concatObj += ",";
                                    }
                                    concatObj = concatObj.TrimEnd(',');
                                    objExpandoObject.TryAdd(name, concatObj);
                                }
                              
                            }
                            else
                            {
                                objExpandoObject.TryAdd(name.ToUpper(), choiceDictionary.FirstOrDefault(x => x.Key.Equals(jObj.Value)).Value);
                            }
                            tmpIndex++;
                        }
                        formattedList.Remove(objExpandoObject);
                        formattedList.Add(objExpandoObject);
                    }
                }
                else
                {
                    formattedList.Remove(jasonValues.IndexOf(evalJason));
                    continue;
                }
            }
           
            return formattedList;
        }

        public List<DictionaryViewModel> GetListFromDictionaryByParent(string Parent)
        {
            List<DictionaryViewModel> model =new List<DictionaryViewModel>();
            if (!string.IsNullOrEmpty(Parent))
            {
                model = db.Dictionary.Where(x => x.Parent.Equals(Parent)).Select(x=> new DictionaryViewModel
                {
                    Key = x.Key,
                    Name = x.KeyValue
                }).OrderBy(x => x.Key).ToList();
            }

            return model;
        }

        public ContactsAll GetDistributor(string CountryCode)
        {
         ContactsAll contactModel = new ContactsAll();

         //ContactCountry need to have a Primary Key to generate as model, so I used query instead of linq
         if (!string.IsNullOrEmpty(CountryCode))
         {
          string query =
           "SELECT c.ContactIdInt, c.ContactId, c.InstructorId, c.PrimaryRoleId, c.ContactName, c.EmailAddress, c.UserLevelId" +
           " FROM Contacts_All c, ContactCountry cc, Roles r " +
           " WHERE " +
           " c.ContactId = cc.ContactId and " +
           " c.PrimaryRoleId = r.RoleId AND " +
           " c.Status <> 'X' AND " +
           " c.Status <> 'D' AND " +
           " cc.CountryCode = '" + CountryCode + "' AND " +
           "c.UserLevelId LIKE '%DISTRIBUTOR%'" +
           " GROUP BY c.ContactId ORDER BY c.ContactName";
          sqlCommand = new MySqlCommand(query);
          var distributorDS = oDb.getDataSetFromSP(sqlCommand);
          if (distributorDS.Tables.Count > 0)
          {
           contactModel = this.DataTableType<ContactsAll>(distributorDS.Tables[0]);
          }
         }

         return contactModel;
        }

        public bool ContactDeletion(string ContactId)
        {
         bool isSucceed = false;
         if (!string.IsNullOrEmpty(ContactId))
         {
          try
          {
           var contact = db.ContactsAll.FirstOrDefault(x => x.ContactId.Equals(ContactId));
           contact.Status = "X";
           db.SaveChanges();
           isSucceed = true;
          }
          catch (Exception e)
          {
           isSucceed = false;
          }
         }

         return isSucceed;
        }

        public bool RemoveContactRole(string ContactId)
        {
         bool isSucceed = false;
         if (!string.IsNullOrEmpty(ContactId))
         {
          try
          {
           var contact = db.ContactsAll.FirstOrDefault(x => x.ContactId.Equals(ContactId));
           contact.UserLevelId = string.Empty;
           contact.PrimaryRoleId = null;
           db.SaveChanges();
           isSucceed = true;
          }
          catch (Exception e)
          {
           isSucceed = false;
          }
         }
         return isSucceed;
        }

        public bool UnAffiliatedFromSite(string contactId, List<string> SiteId)
        {
         bool isSucceed = false;

         if (!string.IsNullOrEmpty(contactId) && SiteId.Count > 0)
         {
          try
          {
           foreach (var site in SiteId)
           {
            var contactModel = db.ContactsAll.FirstOrDefault(x => x.ContactId.Equals(contactId));
            if (contactModel.PrimarySiteId.Equals(site))
            {
             contactModel.PrimarySiteId = String.Empty;
             
            }
            var model = db.SiteContactLinks.FirstOrDefault(x => x.ContactId.Equals(contactId) && x.SiteId.Equals(site));
            model.Status = "X";
            db.Update(contactModel);
            db.Update(model);
            db.SaveChanges();
            isSucceed = true;
           }
          }
          catch (Exception e)
          {
           isSucceed = false;
          }
         }

         return isSucceed;
        }

        public bool AddAffiliationToSites(string contact, List<string> SiteId, string currentUserID)
        {
         var isSucceed = false;
         if (!string.IsNullOrEmpty(contact) && SiteId.Count > 0 && !string.IsNullOrEmpty(currentUserID))
         {
          var isAffiliated = db.SiteContactLinks.Count(x => SiteId.Contains(x.SiteId) && x.ContactId.Equals(contact));
          if (isAffiliated < 1)
          {
           List<SiteContactLinks> contactLinkList = new List<SiteContactLinks>();
           foreach (var sID in SiteId)
           {
            SiteContactLinks newSiteContactLinks = new SiteContactLinks
            {
             ContactId = contact,
             SiteId = sID,
             AddedBy = currentUserID,
             DateAdded = DateTime.Now,
             DateLastAdmin = DateTime.Now,
             LastAdminBy = currentUserID,
             Status = "A"
            };
            contactLinkList.Add(newSiteContactLinks);          
           }
           
           if(contactLinkList.Count > 0)
            db.SiteContactLinks.UpdateRange(contactLinkList);
           db.SaveChanges();
          }

         }

         return isSucceed;
        }

        /*  public List<T> GetThreeNestedStageJsonLabel<T>(string JsonString)
          {
              //JsonString ="{\"firstObj\": \"one\",\"SecondObj\": {\"Sec1\": {\"One\": 1},\"third\": {\"t1\": 12,\"t2\": {\"t21\": 21,\"t22\": 22}}}}";
              List<T> resultList = new List<T>();
              if (!String.IsNullOrEmpty(JsonString))
              {
                  var obj= JObject.Parse(JsonString);
                  resultList = obj.Properties().Select(x=> new LanguageJsonDisplayModel
                  {
                      LabelName = x.Name,
                      DisplayName = x.Name.Replace("_"," "),
                      SubModuleses = x.Value.HasValues? x.Value.Children().OfType<JProperty>().Select(sec=>new SubModule
                      {
                          LabelName = sec.Name,
                          DisplayName = sec.Name.Replace("_"," "),
                          NestedSubModules = sec.Value.HasValues ? sec.Value.Children().OfType<JProperty>().Select(thd => new NestedSubModule
                          {
                              LabelName = thd.Name,
                              DisplayName = thd.Name.Replace("_"," ")
                          }).ToList():null
                      }).ToList():null
                  }).OfType<T>().ToList();


              }

              return resultList;
          }*/

        public bool EnumeratePropertyDifferences<T>(T obj1, T obj2)
        {
           PropertyInfo[] properties = typeof(T).GetProperties();
           bool isChanges = false;
           //List<string> changes = new List<string>();
           
           foreach (PropertyInfo pi in properties)
           {
            object originalValue = typeof(T).GetProperty(pi.Name).GetValue(obj1, null);
            object changesValue = typeof(T).GetProperty(pi.Name).GetValue(obj2, null);
            //var id = new object();

            //if (typeof(T).FullName.Contains("Certificate"))
            //{
            //  id = typeof(T).GetProperty("CertificateId").GetValue(obj1, null);
              
            //}
            //else if (typeof(T).FullName.Contains("EvaluationQuestion"))
            //{
            //  id = typeof(T).GetProperty("EvaluationQuestionId").GetValue(obj1, null);
            //}
            //else if (typeof(T).FullName.Contains("LanguagePack"))
            //{
            //  id = typeof(T).GetProperty("LanguagePackId").GetValue(obj1, null);
            //}

            if (originalValue != changesValue && (originalValue == null || !originalValue.Equals(changesValue)))
            {
             //var changedValues = new {ID = id, OriginalValue = originalValue, ChangedValue = changesValue};
             //changes.Add(JsonConvert.SerializeObject(changesValue, Formatting.Indented));
             isChanges = true;
             break;
            }
           }


           return isChanges;
        }

        public dynamic CertificateChanges()
        {
         List<Certificate> newData = new List<Certificate>();
         List<dynamic> dynamicModel = new List<dynamic>();
         var listCertificateLive = this.db.Certificate.OrderByDescending(x => x.CertificateId).ToList();
         var listCertificateBackUp = this.backupdbContext.Certificate.OrderByDescending(x => x.CertificateId).ToList();
         
         var isContainNew = listCertificateLive.Any(x => !listCertificateBackUp.Any(b => b.CertificateId.Equals(x.CertificateId)));

         if (isContainNew)
         {
          newData = listCertificateLive.Where(x => !listCertificateBackUp.Any(c => c.CertificateId.Equals(x.CertificateId))).ToList();

          if (newData.Count > 0)
          {
           var list = newData.Select(x => new
                                           {
                                            ID = x.CertificateId,
                                            KeyCode = x.Name + " - " + x.Year ,
                                            ChangesValue = new List<string>{$"Code: {x.Code},Language: {x.LanguageCertificate},Course Title: {x.TitleCourseTitle},Partner: {x.TitlePartner}"},
                                            OriginalValue = new List<string>{"Newly Added Record"},
                                            Status = "New"
                                           }).ToList();
           dynamicModel.AddRange(list);
          }
         }
         
         listCertificateLive = db.Certificate
          .Where(x => listCertificateBackUp.Any(c => c.CertificateId.Equals(x.CertificateId)))
          .OrderByDescending(x => x.CertificateId).ToList();

         foreach (var liveData in listCertificateLive)
         {
          var changes = new List<string>();
          var original = new List<string>();

          var backUpData = listCertificateBackUp.Where(x => x.CertificateId.Equals(liveData.CertificateId))
           .Select(x => new Certificate
           {
            Name = x.Name,
            Code = x.Code,
            Status = x.Status,
            Year = x.Year,
            CertificateTypeId = x.CertificateTypeId,
            CertificateBackgroundId = x.CertificateBackgroundId,
            CertificateId = x.CertificateId,
            Htmldesign = x.Htmldesign,
            LanguageCertificate = x.LanguageCertificate,
            PartnerTypeId = x.PartnerTypeId,
            TitleCertificateNo = x.TitleCertificateNo,
            TitleCourseTitle = x.TitleCourseTitle,
            TitleDate = x.TitleDate,
            TitleDuration = x.TitleDuration,
            TitleEvent = x.TitleEvent,
            TitleHeaderDescription = x.TitleHeaderDescription,
            TitleInstitution = x.TitleInstitution,
            TitleInstructor = x.TitleInstructor,
            TitleLocation = x.TitleLocation,
            TitlePartner = x.TitlePartner,
            TitleProduct = x.TitleProduct,
            TitleStudentName = x.TitleStudentName,
            TitleTextFooter = x.TitleTextFooter,
            TitleTextHeader = x.TitleTextHeader

           }).SingleOrDefault();
          var isEqual = objectComparer.Compare(liveData, backUpData, out var differences);
          if (!isEqual)
          {
           foreach (var diffValue in differences)
           {
            var changesValue = string.Empty;
            var originalValue = string.Empty;

            if (diffValue.MemberPath.Contains("Htmldesign"))
            {
             changesValue =  diffValue.MemberPath.Split('.').Last() + ": " + "HTML Design is Changed";
             originalValue = diffValue.MemberPath.Split('.').Last() + ": " + "HTML Design is Changed";
            }
            else
            {
             changesValue =  diffValue.MemberPath.Split('.').Last() + ": " + diffValue.Value1;
             originalValue = diffValue.MemberPath.Split('.').Last() + ": " + diffValue.Value2;
            }
           

            changes.Add(changesValue);
            original.Add(originalValue);
           }        
          }

          if (changes.Count > 0 && original.Count > 0)
          {
           dynamic changesObject = new
           {
            ID = liveData.CertificateId,
            KeyCode = liveData.Name + " - " + liveData.Year,
            ChangesValue = changes,
            OriginalValue = original,
            Status = "changes"
           };
           dynamicModel.Add(changesObject);
          }
         }
        
         #region old Code

         
         //if (!isEqual)
         //{
         // var keysList = differences.Select(x => Regex.Match(x.MemberPath, @"(\w*?\[\d\])").Groups[1].Value).Distinct().ToList();
         // dynamic changesObject = string.Empty;
         // foreach (var keyValue in keysList)
         // {
         //    string changedValues = string.Empty;
         //    string originalValue = string.Empty;
         //    string keyCode = string.Empty;
         //    int i = 0;

         //     var valueList = differences.Where(x => x.MemberPath.Equals(keyValue))
         //                      .Select(x => new
         //                                    {
         //                                     ID = listCertificateLive[Convert.ToInt32(Regex.Match(x.MemberPath, @"\[(\d)\]").Groups[1].Value)].CertificateId,
         //                                      KeyCode = listCertificateLive[Convert.ToInt32(Regex.Match(x.MemberPath, @"\[(\d)\]").Groups[1].Value)].Name,
         //                                      ChangesValue = $"{Regex.Match(x.MemberPath, @"\]\.(\w+)").Groups[1].Value}: {x.Value1}",
         //                                      OriginalValue = x.Value2
         //                                    }).ToList();
         //     foreach (var valueObject in valueList)
         //     {
         //      keyCode = valueObject.KeyCode;
         //      if (i == 0)
         //      {
         //         changedValues = valueObject.ChangesValue;
         //         originalValue = valueObject.OriginalValue;
         //      }
         //      else
         //      {
         //       changedValues += ","  + valueObject.ChangesValue;
         //       originalValue += "," + valueObject.OriginalValue;               
         //      } 

         //       i++;
         //     }

         //     changesObject = new
         //                      {
         //                       KeyCode = keyCode,
         //                       ChangesValue = changedValues,
         //                       OriginalValue = originalValue
         //                      };
         //     dynamicModel.Add(changesObject);
         // }
         //}

             //foreach (var liveValue in listCertificateLive)
          //{
          // var backUpValue = listCertificateBackUp.Where(x => x.CertificateId.Equals(liveValue.CertificateId)).Select(x => x).Cast<Certificate>()
          //  .SingleOrDefault();
          // if (backUpValue != null)
          // {
          //  var isChanges = this.EnumeratePropertyDifferences<Certificate>(liveValue, backUpValue);
          //  if (isChanges)
          //  {
          //   var changes = new {LiveCertificate = liveValue, BackUpCertificate = backUpValue};
          //   dynamicModel.Add(changes);
          //  }
          // }
          // else
          // {
          //  var changes = new {LiveValue = liveValue, BackUpValue = string.Empty};
          //  dynamicModel.Add(changes);
          //  //certificateChangesValues.Add($"Property ID: {liveValue.CertificateId} | Name: {liveValue.Name}  | PartnerTypeID: {liveValue.PartnerTypeId} is newly Added");
          // }


          // }

          // certificateChangesValues = JsonConvert.SerializeObject(dynamicModel, Formatting.Indented);
                

         #endregion

         return dynamicModel;
       }

       public dynamic LanguageChanges()
       {
        List<LanguagePack> newData = new List<LanguagePack>();
        var listLanguagePacLive = this.db.LanguagePack.OrderByDescending(x => x.LanguagePackId).ToList();
        var listLanguagePacBackUp = this.backupdbContext.LanguagePack.OrderByDescending(x => x.LanguagePackId).ToList();
        List<dynamic> dynamicModel = new List<dynamic>();

        var isContainNew = listLanguagePacLive.Any(x => !listLanguagePacBackUp.Any(u => u.LanguagePackId.Equals(x.LanguagePackId)));

        if (isContainNew)
        {
         newData = listLanguagePacLive.Where(x => !listLanguagePacBackUp.All(c => c.LanguagePackId.Equals(x.LanguagePackId))).ToList();

         if (newData.Count > 0)
         {
          var list = newData.Select(x => new
                                          {
                                           ID = x.LanguagePackId,
                                           KeyCode = x.LanguageCountry,
                                           ChangesValue = new List<string>{$"Country: {x.LanguageCountry},Json: {x.LanguageJson},Status: {x.Status}"},
                                           OriginalValue = new List<string>{"Newly Added Record"},
                                           Status = "New"
                                          }).ToList();
          dynamicModel.AddRange(list);
         }
        }

        listLanguagePacLive = db.LanguagePack
          .Where(x => listLanguagePacBackUp.Any(c => c.LanguagePackId.Equals(x.LanguagePackId)))
          .OrderByDescending(x => x.LanguagePackId).ToList();

         foreach (var liveData in listLanguagePacLive)
         {
          var changes = new List<string>();
          var original = new List<string>();
          var backData = listLanguagePacBackUp.Where(x => x.LanguagePackId.Equals(liveData.LanguagePackId))
           .Select(x => new LanguagePack
           {
              Status = x.Status,
              LanguagePackId =  x.LanguagePackId,
              CreatedDate = x.CreatedDate,
              CreatedBy = x.CreatedBy,
              LanguageCountry = x.LanguageCountry,
              LanguageJson = x.LanguageJson

           }).SingleOrDefault();
          var isEqual = objectComparer.Compare(liveData, backData, out var differences);
          if (!isEqual)
          {
           var enumerable = differences as Difference[] ?? differences.ToArray();

           if (enumerable.Any(x => x.MemberPath.Contains("LanguageJson")))
           {
            var jsonData = enumerable.Where(x => x.MemberPath.Contains("LanguageJson")).Select(x => x)
             .SingleOrDefault();

            bool isData1Valid = this.commonService.IsJasonValid(jsonData.Value1);
            bool isData2Valid = this.commonService.IsJasonValid(jsonData.Value2);

            if (isData1Valid && isData2Valid)
            {
             var liveJsonData = JsonConvert.DeserializeObject<ExpandoObject>(jsonData.Value1);
             var backJsonData = JsonConvert.DeserializeObject<ExpandoObject>(jsonData.Value2);

             var jsonVaid = objectComparer.Compare(liveJsonData, backJsonData, out var jsonDifferences);

             if (!jsonVaid)
             {
              foreach (var diffValue in jsonDifferences)
              {
               var jsonChangesValue = "Language-" + diffValue.MemberPath.Split('.').Last().ToUpper() + ":" +
                                      diffValue.Value1;
               var jsonOriginalValue = "Language-" + diffValue.MemberPath.Split('.').Last().ToUpper() + ":" +
                                       diffValue.Value2;

               changes.Add(jsonChangesValue);
               original.Add(jsonOriginalValue);
              }

             }
            }
            else
            {
             dynamic invalidObject = new
             {
              ID = liveData.LanguagePackId,
              KeyCode = liveData.LanguageCountry + " - " + liveData.Status,
              ChangesValue = isData2Valid ? new List<string>() : new List<string>{"Detected Invalid Json"} ,
              OriginalValue = isData1Valid ? new List<string>() : new List<string>{"Detected Invalid Json"},
              Status = "Invalid"
             };
             dynamicModel.Add(invalidObject);
            }
           }

           var changesData = enumerable.Where(x => !x.MemberPath.Contains("LanguageJson")).Select(x => x)
              .ToList();
             foreach (var diffValue in changesData)
             {
              var changesValue = diffValue.MemberPath.Split('.').Last() + ": " + diffValue.Value1;
              var originalValue = diffValue.MemberPath.Split('.').Last() + ": " + diffValue.Value2;

              changes.Add(changesValue);
              original.Add(originalValue);
             }
            
           }
          

           if (changes.Count > 0 && original.Count > 0)
             {
              dynamic changesObject = new
              {
               ID = liveData.LanguagePackId,
               KeyCode = liveData.LanguageCountry + " - " + liveData.Status,
               ChangesValue = changes,
               OriginalValue = original,
               Status = "changes"
              };
              dynamicModel.Add(changesObject);
             }
        }
           
          return dynamicModel;
       }
  
        public dynamic EvalQuestionChanges()
        {
         var listEvalQuestionLive = db.EvaluationQuestion.OrderByDescending(x => x.EvaluationQuestionId).ToList();
         var listEvalQuestionBackUp =
          backupdbContext.EvaluationQuestion.OrderByDescending(x => x.EvaluationQuestionId).ToList();
         var dynamicModel = new List<dynamic>();

         var isContainNew = listEvalQuestionLive.Any(x =>
          listEvalQuestionBackUp.Any(u => u.EvaluationQuestionId != x.EvaluationQuestionId));

         if (isContainNew)
         {
          var newData = listEvalQuestionLive
           .Where(x => listEvalQuestionBackUp.All(c => c.EvaluationQuestionId != x.EvaluationQuestionId)).ToList();

          if (newData.Count > 0)
          {
           var list = newData.Select(x => new
           {
            ID = x.EvaluationQuestionId,
            KeyCode = x.CourseId,
            ChangesValue = new List<string>{$"Partner Type: {x.PartnerType},Json: {x.EvaluationQuestionJson},Year: {x.Year}"},
            OriginalValue = new List<string>{"Newly Added Record"},
            Status = "New"
           }).ToList();
           dynamicModel.AddRange(list);
          }
         }

         listEvalQuestionLive = db.EvaluationQuestion
          .Where(x => listEvalQuestionBackUp.Any(c => c.EvaluationQuestionId.Equals(x.EvaluationQuestionId)))
          .OrderByDescending(x => x.EvaluationQuestionId).ToList();

         foreach (var liveData in listEvalQuestionLive)
         {
          var changes = new List<string>();
          var original = new List<string>();
          var backData = listEvalQuestionBackUp.Where(x => x.EvaluationQuestionId.Equals(liveData.EvaluationQuestionId))
           .Select(x => new EvaluationQuestion
           {
            CountryCode = x.CountryCode,
            CourseId = x.CourseId,
            CertificateTypeId = x.CertificateTypeId,
            CreatedBy = x.CreatedBy,
            CreatedDate = x.CreatedDate,
            EvaluationQuestionCode = x.EvaluationQuestionCode,
            EvaluationQuestionId = x.EvaluationQuestionId,
            EvaluationQuestionJson = x.EvaluationQuestionJson,
            EvaluationQuestionTemplate = x.EvaluationQuestionTemplate,
            LanguageId = x.LanguageId,
            PartnerType = x.PartnerType,
            Year = x.Year
           }).SingleOrDefault();
          var isEqual = objectComparer.Compare(liveData, backData, out var differences);
          if (!isEqual)
          {
           var enumerable = differences as Difference[] ?? differences.ToArray();
           if (enumerable.Any(x => x.MemberPath.Contains("EvaluationQuestionJson")))
           {
            var jsonData = enumerable.Where(x => x.MemberPath.Contains("EvaluationQuestionJson")).Select(x => x)
             .SingleOrDefault();

            var isData1Valid = this.commonService.IsJasonValid(jsonData.Value1);
            var isData2Valid = this.commonService.IsJasonValid(jsonData.Value2);

            if (isData1Valid && isData2Valid)
            {
             var liveJsonData = JsonConvert.DeserializeObject<ExpandoObject>(jsonData.Value1);
             var backJsonData = JsonConvert.DeserializeObject<ExpandoObject>(jsonData.Value2);

             var jsonVaid = objectComparer.Compare(liveJsonData, backJsonData, out var jsonDifferences);

             if (!jsonVaid)
             {
              foreach (var diffValue in jsonDifferences)
              {
               var jsonChangesValue = "EvaluationQuestion-" + diffValue.MemberPath.Split('.').Last().ToUpper() + ": " + diffValue.Value1;
               var jsonOriginalValue = diffValue.MemberPath.Split('.').Last().ToUpper() + ": " + diffValue.Value2;

               changes.Add(jsonChangesValue);
               original.Add(jsonOriginalValue);
              }
             }
            }
            else
            {
             dynamic changesObject = new
             {
              ID = liveData.EvaluationQuestionId,
              KeyCode = liveData.CourseId + " - " + liveData.Year,
              ChangesValue = isData2Valid ? new List<string>() : new List<string>{"Detected Invalid Json"} ,
              OriginalValue = isData1Valid ? new List<string>() : new List<string>{"Detected Invalid Json"},
              Status = "Invalid"
             };
             dynamicModel.Add(changesObject);
            }
           }

           var changesData = enumerable.Where(x => !x.MemberPath.Contains("EvaluationQuestionJson")).Select(x => x)
            .ToList();

           foreach (var diffValue in changesData)
           {
            var changesValue = diffValue.MemberPath.Split('.').Last() + ": " + diffValue.Value1;
            var originalValue = diffValue.MemberPath.Split('.').Last() + ": " + diffValue.Value2;

            changes.Add(changesValue);
            original.Add(originalValue);
           }
          }

          if (changes.Count > 0 && original.Count > 0)
          {
           dynamic changesObject = new
           {
            ID = liveData.EvaluationQuestionId,
            KeyCode = liveData.CourseId + " - " + liveData.Year,
            ChangesValue = changes,
            OriginalValue = original,
            Status = "changes"
           };
           dynamicModel.Add(changesObject);
          }
         }

         return dynamicModel;
        }

    }
}
