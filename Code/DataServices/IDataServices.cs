﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using autodesk.Code.Models;
using autodesk.Code.ViewModel;

namespace autodesk.Code.DataServices
{
    public interface IDataServices
    {
        List<T> GetProductPrimaryorSecondary<T>(string name);

        List<T> DataTableToList<T>(DataTable table) where T : class, new();
        T DataTableType<T>(DataTable table) where T : class, new();
        List<dynamic> ToDynamic(DataTable dt);

        DataTable DynamicToDataTable(List<dynamic> list);
 
        Task<RefTerritory> GetERefTerritoryById(int Tid);
        Task<int> GetLookBrowserLimitation(string desName);
        Task<VersionControlModel> GetVersion();
        List<ColumnViewModel> GetColumns(Type type);

        // bool IsAuthenicate(int UserId,string token);

        List<dynamic> EvalanswerReformater(DataTable EvalList, string TypeName, DataTable EvalQuestionList);
        List<dynamic> EvalanswerReformatertwo(DataTable EvalList, DataTable EvalQuestionList);
        List<dynamic> EvalquestionReformater(DataTable EvalList, string TypeName);

        List<DictionaryViewModel> GetListFromDictionaryByParent(string Parent);
        ContactsAll GetDistributor(string CountryCode);
        bool ContactDeletion(string ContactId);
        bool RemoveContactRole(string ContactId);
        bool UnAffiliatedFromSite(string contactId, List<string> SiteId);
        bool AddAffiliationToSites(string contact, List<string> SiteId,string currentUserID);

        bool EnumeratePropertyDifferences<T>(T obj1, T obj2);

        dynamic CertificateChanges();

        dynamic LanguageChanges();

        dynamic EvalQuestionChanges();
        DataTable ListToDatatable(List<dynamic> list);
        //List<T> GetThreeNestedStageJsonLabel<T>(string JsonString);
        //DataSet EscapeSpecialChar(DataSet ds);
        //IEnumerable<Dictionary<string, object>> ToDictionary(DataTable table);
    }
}
