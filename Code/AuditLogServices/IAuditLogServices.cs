﻿using autodesk.Code.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.AuditLogServices
{
    public interface IAuditLogServices
    {
        string formatAuditLogDes(string cmdText);
        void LogHistory(History history);
    }
}
