﻿using autodesk.Code.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.AuditLogServices
{
    public class AuditLogServices:IAuditLogServices
    {
        private string res;
        private readonly MySQLContext _mySQLcontext;
        public AuditLogServices(MySQLContext context)
        {
            _mySQLcontext = context;
        }
        public string formatAuditLogDes(string cmdText)
        {
            if (!String.IsNullOrEmpty(cmdText))
            {
                var isCrud = cmdText.Trim().Substring(0, 6);
                if (isCrud.ToLower().Equals("insert"))
                {
                    var tbName = getBetween(cmdText.ToLower(), "into", "Values");
                    if (cmdText.ToLower().Contains("organization"))
                    {

                    }

                    if (cmdText.ToLower().Contains("site"))
                    {

                    }
                }
                else if(isCrud.ToLower().Equals("delete"))
                {
                    
                }
                else if(isCrud.ToLower().Equals("update"))
                {
                    
                }
            }
            return res;
        }

        protected static string getBetween(string strSource, string strStart, string strEnd)
        {
            int Start, End;
            if (strSource.Contains(strStart) && strSource.Contains(strEnd))
            {
                Start = strSource.IndexOf(strStart, 0) + strStart.Length;
                End = strSource.IndexOf(strEnd, Start);
                return strSource.Substring(Start, End - Start);
            }
            else
            {
                return "";
            }
        }
        public void LogHistory(History history)
        {
            if (history.Date != null && !string.IsNullOrEmpty(history.Id) && !string.IsNullOrEmpty(history.Description))
            {
                using (var db= new MySQLContext())
                {
                    db.Histories.Add(history);
                    db.SaveChanges();
                }
                
            }

        }
    }
}
