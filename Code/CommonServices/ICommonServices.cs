﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using autodesk.Code.Models;
using Microsoft.Extensions.Configuration;

namespace autodesk.Code.CommonServices
{
    public interface ICommonServices
    {
        bool IsJasonValid(string jason);
        string EncrypttoMd5(string text);
        string CreateToken(ContactsAll loginUser, string JwtKey, string Version);
        string HtmlDecoder(string encodedString);
        string HtmlEncoder(string text);
     
    }
}
