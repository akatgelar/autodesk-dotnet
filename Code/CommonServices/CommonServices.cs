﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using autodesk.Code.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace autodesk.Code.CommonServices
{
    using System.Drawing.Text;
    using System.Web;

    using Microsoft.AspNetCore.Server.Kestrel.Core.Internal.Infrastructure;

    public class CommonServices : ICommonServices
    {
        public bool IsJasonValid(string jason)
        {
            jason = jason.Trim();
            if ((jason.StartsWith("{") && jason.EndsWith("}")) || //For object
                (jason.StartsWith("[") && jason.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(jason);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    Console.WriteLine(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public string EncrypttoMd5(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            //compute hash from the bytes of text  
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

            //get hash result after compute it  
            byte[] result = md5.Hash;

            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits  
                //for each byte  
                strBuilder.Append(result[i].ToString("x2"));
            }
            return strBuilder.ToString();
        }

        public string CreateToken(ContactsAll loginUser, string JwtKey, string Version)
        {

            var claims = new[] {
                new Claim(JwtRegisteredClaimNames.Email, loginUser.EmailAddress),
                new Claim(JwtRegisteredClaimNames.Typ, loginUser.UserLevelId),
                new Claim(JwtRegisteredClaimNames.Sid, Version),
                new Claim(JwtRegisteredClaimNames.NameId,loginUser.ContactId)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtKey));
            var credential = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var iisUser = AppConfig.Config["Jwt:Issuer"];
            var audience = AppConfig.Config["Auth0:Audience"];

            var token = new JwtSecurityToken(iisUser,
                audience,
                claims,
                expires: DateTime.Now.AddDays(60),
                signingCredentials: credential);

            return new JwtSecurityTokenHandler().WriteToken(token);

        }
      
        public string HtmlDecoder(string encodedString)
        {
            if (!String.IsNullOrEmpty(encodedString))
            {
                return HttpUtility.HtmlDecode(encodedString);
            }

            return null;
        }

        public string HtmlEncoder(string text)
        {
            //string encodeString = null;
            string encodeString = text;
            if (!String.IsNullOrEmpty(text))
            {
                for (int i = text.Length - 1; i >= 0; i--)
                {
                    if (text[i].Equals('\'') || text[i].Equals('\"') || text[i].Equals('\n') || text[i].Equals('\\') || text[i].Equals('{') || text[i].Equals('}') || text[i].Equals('/'))
                    {
                        var encodechar = "&#" + Convert.ToUInt16(text[i]) + ";";
                        encodeString = encodeString.Substring(0, i) + encodechar + encodeString.Substring(i + 1);
                        //encodeString = text.Substring(0, i) + HttpUtility.HtmlEncode(text[i]) + text.Substring(i + 1);
                    }

                }
            }
            return encodeString;


        }

      
    }
}
