﻿using autodesk.Code.Models;
using autodesk.Code.Models.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static autodesk.Code.AutodeskConst;

namespace autodesk.Code
{
    public class AgreementService : IAgreementService
    {

        private readonly MySQLContext _mySqlContext;
        private readonly IEmailService _emailService;
        public AgreementService(MySQLContext mySqlContext, IEmailService emailService)
        {

            _mySqlContext = mySqlContext;
            _emailService = emailService;
        }


        public AgreementMasterListDto GetAgreementMasters(AgreementSearchDto searchInput)
        {
            try
            {
                if (!string.IsNullOrEmpty(searchInput.UserId))
                {
                    var contact = _mySqlContext.ContactsAll.FirstOrDefault(c => c.ContactId == searchInput.UserId);

                    if (contact != null)
                    {
                        //create query
                        var orgQuery = _mySqlContext.MainOrganization.AsQueryable();
                        //if (!string.IsNullOrEmpty(searchInput.OrgId))
                        //{
                        //    var orgId = searchInput.OrgId.Trim();
                        //    orgQuery = orgQuery.Where(oq => oq.OrgId.Contains(orgId) );
                        //}
                        //if (!string.IsNullOrEmpty(searchInput.OrgName))
                        //{
                        //    var orgName = searchInput.OrgName.Trim();

                        //    orgQuery = orgQuery.Where(oq => oq.OrgName.Contains(orgName));
                        //}

                        var partnerQuery = from s in _mySqlContext.MainSite
                                           join sr in _mySqlContext.SiteRoles on s.SiteId equals sr.SiteId
                                           where (sr.Status == AutodeskConst.StatusConts.A || sr.Status == AutodeskConst.StatusConts.P)
                                           select new
                                           {
                                               s.OrgId,
                                               sr.RoleId,
                                               sr.Status
                                           };

                        var geoQuery = from ctry in _mySqlContext.RefCountries
                                       join terr in _mySqlContext.RefTerritory on ctry.TerritoryId equals terr.TerritoryId
                                       select new
                                       {
                                           ctry.CountriesId,
                                           ctry.CountriesName,
                                           ctry.CountriesCode,
                                           ctry.TerritoryId,
                                           terr.TerritoryName,
                                           ctry.GeoCode
                                       };

                        var query = from org in orgQuery
                                    join partner in partnerQuery on org.OrgId equals partner.OrgId
                                    join geo in geoQuery on org.RegisteredCountryCode equals geo.CountriesCode
                                    select new
                                    {
                                        org.OrgId,
                                        org.OrgName,
                                        org.RegisteredCountryCode,
                                        org.RegisteredStateProvince,
                                        partner.RoleId,
                                        partner.Status,
                                        geo.GeoCode,
                                        geo.TerritoryId,
                                        geo.TerritoryName
                                    };

                        // add filter


                        if (searchInput.PartnerTypeStatus != null && searchInput.PartnerTypeStatus.Any())
                        {
                            query = query.Where(q => searchInput.PartnerTypeStatus.Contains(q.Status));
                        }

                        if (searchInput.Geo != null && searchInput.Geo.Any())
                        {
                            query = query.Where(q => searchInput.Geo.Contains(q.GeoCode));
                        }

                        if (searchInput.RegisteredCountryCode != null && searchInput.RegisteredCountryCode.Any())
                        {
                            query = query.Where(q => searchInput.RegisteredCountryCode.Contains(q.RegisteredCountryCode));
                        }

                        //if (!string.IsNullOrEmpty(searchInput.RegisteredStateProvince))
                        //{
                        //    query = query.Where(oq => oq.RegisteredStateProvince.ToLower().Contains(searchInput.RegisteredStateProvince.ToLower()));
                        //}

                        // check UserLevelId
                        if (contact.UserLevelId!= UserLevelIdEnum.SUPERADMIN)
                        {
                            var userLevelQuery = (from site in _mySqlContext.MainSite
                                                  join scl in _mySqlContext.SiteContactLinks on site.SiteId equals scl.SiteId
                                                  join ca in _mySqlContext.ContactsAll on scl.ContactId equals ca.ContactId
                                                  where ca.ContactId == contact.ContactId
                                                  select site.OrgId).Distinct().ToList();

                            query = query.Where(x => userLevelQuery.Contains(x.OrgId));
                        }

                        // Get return data
                        var result = (from data in (from agrM in _mySqlContext.AgreementMaster
                                                    join org in query on agrM.OrgId equals org.OrgId
                                                    join r in _mySqlContext.Roles on org.RoleId equals r.RoleId
                                                    join agrR in _mySqlContext.AgreementRemark on agrM.AgreementMasterId equals agrR.AgreementMasterId into agrRemark
                                                    from remark in agrRemark.DefaultIfEmpty()
                                                    where (!string.IsNullOrEmpty(searchInput.FY) ? searchInput.FY == agrM.FyindicatorKey : true)
                                                    && (!string.IsNullOrEmpty(searchInput.PartnerType) ? (searchInput.PartnerType == r.RoleId.ToString() && r.RoleCode == agrM.PartnerType) : true)
                                                    && (!string.IsNullOrEmpty(searchInput.OrgId) ? agrM.OrgId.ToLower().Contains(searchInput.OrgId.Trim().ToLower()) : true)
                                                    && (!string.IsNullOrEmpty(searchInput.OrgName) ? org.OrgName.ToLower().Contains(searchInput.OrgName.Trim().ToLower()) : true)
                                                    select new AgreementDto
                                                    {
                                                        AgreementMasterId = agrM.AgreementMasterId,
                                                        FyindicatorKey = agrM.FyindicatorKey,
                                                        OrgId = agrM.OrgId,
                                                        OrgName = org.OrgName,
                                                        PartnerType = agrM.PartnerType,
                                                        Status = agrM.Status==AgreementsStatus.Review?AgreementsStatus.PendingSignature: agrM.Status == AgreementsStatus.Pending? AgreementsStatus.PendingApproval:agrM.Status,
                                                        TerritoryName = org.TerritoryName,
                                                        ApproveBy = remark != null ? remark.RecordBy : null,
                                                        RecordTime = remark != null ? remark.RecordDateTime : (DateTime?)null
                                                    })
                                      group data by new { data.OrgId, data.PartnerType } into g
                                      select g.OrderByDescending(d => d.RecordTime).First()).ToList();

                        //if (!string.IsNullOrEmpty(searchInput.PartnerType))
                        //{
                        //    result = result.Where(q => searchInput.PartnerType == q.RoleId.ToString()).ToList();
                        //}

                        return (new AgreementMasterListDto { AgreementMasters = result, TotalCount = result.Count() });
                    }
                }

                return null;
            }
            catch
            {
                return null;
            }
        }
        private string GetApproveBy(AgreementRemark remark)
        {
            if (remark != null)
            {
                return remark.RecordBy;
            }
            return string.Empty;

        }
        private bool CheckTerritoryId(string territoryInputId, int territoryEntityId)
        {
            return !string.IsNullOrEmpty(territoryInputId) ? int.Parse(territoryInputId) == territoryEntityId : true;
        }

        public async Task<List<AgreementRemarkDto>> GetAgreementRemarks(int agreementMasterId)
        {
            var entity = await Task.Run(() => _mySqlContext.AgreementRemark.Where(a => a.AgreementMasterId == agreementMasterId).ToList());
            var result = entity.Select(a => new AgreementRemarkDto
            {
                Action = a.Action,
                AgreementMasterId = a.AgreementMasterId,
                AgreementRemarkId = a.AgreementRemarkId,
                RecordBy = a.RecordBy,
                RecordDateTime = a.RecordDateTime,
                Remark = a.Remark
            }).ToList();
            return result;
        }

        public async Task<List<CountryDropdownListDto>> GetListCountry(int territoryId)
        {
            var query = await Task.Run(() => _mySqlContext.RefCountries.Where(c => c.TerritoryId == territoryId).Select(c => new CountryDropdownListDto { RegionCode = c.RegionCode, RegionName = c.RegionName }).Distinct().ToList());
            return query;
        }

        public async Task<List<FYDropdownListDto>> GetListFY()
        {
            var query = await Task.Run(() => _mySqlContext.Dictionary.Where(c => c.Parent == "FYIndicator" && c.Status == AutodeskConst.StatusConts.A).Select(c => new FYDropdownListDto { FYName = c.KeyValue, KeyValue = c.KeyValue, Key = c.Key }).OrderBy(x => x.Key).Distinct().ToList());
            return query;
        }

        public async Task<List<OrgDropdownListDto>> GetListOrg(int territoryId, string countryCode)
        {
            var query = await Task.Run(() => (from o in _mySqlContext.MainOrganization
                                              join ctry in _mySqlContext.RefCountries on o.RegisteredCountryCode equals ctry.CountriesCode
                                              join ter in _mySqlContext.RefTerritory on ctry.TerritoryId equals ter.TerritoryId
                                              where ter.TerritoryId == territoryId && (!string.IsNullOrEmpty(countryCode) ? ctry.CountriesCode == countryCode : true)
                                              select new OrgDropdownListDto { OrgId = o.OrgId, OrgName = o.OrgName }
                ).Distinct().ToList());

            return query;
        }

        public async Task<List<Tuple<int, string>>> GetListPartnerType()
        {
            var res = await Task.Run(() => _mySqlContext.Roles.Where(c => c.RoleType == "Company").Select(c => new { c.RoleId, c.RoleName, c.RoleCode }).OrderBy(x => x.RoleCode).ToList());
            return res.Select(x => new Tuple<int, string>(x.RoleId, x.RoleName)).Distinct().ToList();
        }

        public async Task<List<TerritoryDropdownListDto>> GetListTerritory()
        {
            var query = await Task.Run(() => _mySqlContext.RefTerritory.Select(c => new TerritoryDropdownListDto { Id = c.TerritoryId, TerritoryName = c.TerritoryName }).Distinct().ToList());
            return query;

        }

        public async Task<int> ExecuteAgreement(AgreementAddDto agreementAddDto)
        {
            try
            {

                var listFy = await GetListFY();
                var currentTime = DateTime.Now;
                var _1stFeb = new DateTime(currentTime.Year, 2, 1);

                var fy = listFy.FirstOrDefault(x => x.Key == DateTime.Now.Year.ToString());
                if (currentTime < _1stFeb)
                {
                    fy = listFy.FirstOrDefault(x => x.Key == (DateTime.Now.Year - 1).ToString());
                }
                //var listPartnerType = (from o in _clickThruDbContext.MainOrganization
                //                       join s in _clickThruDbContext.MainSite on o.OrgId equals s.OrgId
                //                       join sr in _clickThruDbContext.SiteRoles on s.SiteId equals sr.SiteId
                //                       join r in _clickThruDbContext.Roles on sr.RoleId equals r.RoleId
                //                       where r.RoleType == "Company" && o.OrgId == agreementAddDto.OrgId
                //                       select r.RoleCode
                //                   ).Distinct().ToList();
                //var partnerType = string.Join(",", listPartnerType);
                var contact = _mySqlContext.ContactsAll.FirstOrDefault(c => c.ContactId == agreementAddDto.UserId);
                var _agreement = _mySqlContext.AgreementMaster.FirstOrDefault(a => a.AgreementMasterId == agreementAddDto.AgreementMasterId);
                if (_agreement != null)
                {
                    if (_agreement.Status != AutodeskConst.AgreementsStatus.Review)
                    {
                        return 0;
                    }

                    _agreement.FyindicatorKey = fy.KeyValue;
                    _agreement.PartnerType = _agreement.PartnerType;
                    _agreement.LegalJobTitle = agreementAddDto.JobTitle;
                    _agreement.FirstName = agreementAddDto.FirstName;
                    _agreement.LastName = agreementAddDto.LastName;
                    _agreement.LegalName = string.Join(' ', agreementAddDto.FirstName, agreementAddDto.LastName);
                    _agreement.CheckTc = agreementAddDto.CheckTC;
                    _agreement.OrgId = agreementAddDto.OrgId;
                    _agreement.Status = AutodeskConst.AgreementsStatus.Pending;
                    _agreement.SubmissionPerson = contact.EmailAddress;

                    _mySqlContext.AgreementMaster.Update(_agreement);
                    var res = await _mySqlContext.SaveChangesAsync();


                    var agreeRemark = new AgreementRemark
                    {
                        AgreementMasterId = _agreement.AgreementMasterId,
                        RecordDateTime = DateTime.Now,
                        Action = AutodeskConst.AgreementsAction.Acknowledged,
                        RecordBy = contact.ContactName,
                        Remark = string.Empty
                    };
                    await _mySqlContext.AgreementRemark.AddAsync(agreeRemark);
                    await _mySqlContext.SaveChangesAsync();

                    if (res > 0)
                    {
                        return res;
                    }
                }

                return 0;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public async Task<OrganizationInfoDto> GetOrganizationInfo(int agreementMasterId)
        {
            var query = await Task.Run(() => (from o in _mySqlContext.MainOrganization
                                              join agrM in _mySqlContext.AgreementMaster on o.OrgId equals agrM.OrgId
                                              let regAdd1 = o.RegisteredAddress1
                                              let regAdd2 = o.RegisteredAddress2
                                              let regAdd3 = o.RegisteredAddress3
                                              where agrM.AgreementMasterId == agreementMasterId
                                              select new OrganizationInfoDto
                                              {
                                                  OrgName = o.OrgName,
                                                  EnglishOrgName = o.EnglishOrgName,
                                                  RegisteredAddress = !string.IsNullOrEmpty(regAdd1) ? regAdd1 : !string.IsNullOrEmpty(regAdd2) ? regAdd2 : !string.IsNullOrEmpty(regAdd3) ? regAdd3 : string.Empty
                                              }
                          ).FirstOrDefault());
            if (query != null)
            {
                return query;
            }
            return null;
        }

        public async Task<AgreementDetailDto> GetAgreementDetail(int agreementMasterId)
        {

            var checkAgreementMaster = _mySqlContext.AgreementMaster.FirstOrDefault(a => a.AgreementMasterId == agreementMasterId);
            if (checkAgreementMaster != null)
            {

                var orgInfo = await Task.Run(() => (from o in _mySqlContext.MainOrganization
                                                    join agrM in _mySqlContext.AgreementMaster on o.OrgId equals agrM.OrgId
                                                    join ctry in _mySqlContext.RefCountries on o.RegisteredCountryCode equals ctry.CountriesCode
                                                    let regAdd1 = o.RegisteredAddress1
                                                    let regAdd2 = o.RegisteredAddress2
                                                    let regAdd3 = o.RegisteredAddress3
                                                    where agrM.AgreementMasterId == agreementMasterId
                                                    select new
                                                    {
                                                        OrgName = o.OrgName,
                                                        EnglishOrgName = o.EnglishOrgName,
                                                        RegisteredAddress = string.Join("", regAdd1, regAdd2, regAdd3),
                                                        Country = ctry.CountriesName,
                                                        PostalCode = o.RegisteredPostalCode,
                                                        Region = ctry.RegionName,
                                                        City = o.RegisteredCity,
                                                    }
                             ).FirstOrDefault());


                var address = string.Empty;
                if (orgInfo != null)
                {
                    address = string.Join(" ", orgInfo.RegisteredAddress, orgInfo.City, orgInfo.Region, orgInfo.PostalCode, orgInfo.Country);
                }
                var query = await Task.Run(() => (from agrM in _mySqlContext.AgreementMaster
                                                  join org in _mySqlContext.MainOrganization.Where(x => x.OrgId == checkAgreementMaster.OrgId) on agrM.OrgId equals org.OrgId
                                                  where agrM.AgreementMasterId == agreementMasterId
                                                  select new AgreementDetailDto
                                                  {
                                                      AgreementMasterId = agrM.AgreementMasterId,
                                                      FY = agrM.FyindicatorKey,
                                                      OrgId = agrM.OrgId,
                                                      OrgName = org.OrgName,
                                                      CheckTC = agrM.CheckTc,
                                                      Title = agrM.LegalJobTitle,
                                                      LegalName = agrM.LegalName,
                                                      FirstName = agrM.FirstName,
                                                      LastName = agrM.LastName,
                                                      PartnerType = agrM.PartnerType,
                                                      Status = agrM.Status,
                                                      SubmissionPerson = agrM.SubmissionPerson,
                                                      Address = address,
                                                      ExecuteBy = string.Join(' ', agrM.FirstName, agrM.LastName),
                                                      RemarkHistories = _mySqlContext.AgreementRemark.Where(r => r.AgreementMasterId == agrM.AgreementMasterId).Select(agrR => new AgreementRemarkDto
                                                      {
                                                          Action = agrR.Action,
                                                          AgreementMasterId = agrM.AgreementMasterId,
                                                          AgreementRemarkId = agrR.AgreementRemarkId,
                                                          RecordBy = agrR.RecordBy,
                                                          RecordDateTime = agrR.RecordDateTime,
                                                          Remark = agrR.Remark

                                                      }).ToList()
                                                  }
                             ).FirstOrDefault());
                if (query != null)
                {
                    return query;
                }
            }
            return null;
        }

        public async Task<bool> RejectAgreement(AgreementRejectDto agreementRejectDto)
        {
            try
            {
                if (agreementRejectDto != null && agreementRejectDto.AgreementMasterId != 0)
                {
                    var agreementEnt = _mySqlContext.AgreementMaster.FirstOrDefault(a => a.AgreementMasterId == agreementRejectDto.AgreementMasterId);
                    if (agreementEnt != null)
                    {
                        agreementEnt.Status = AutodeskConst.AgreementsStatus.Rejected;
                        _mySqlContext.AgreementMaster.Update(agreementEnt);
                        _mySqlContext.SaveChanges();

                        var contact = _mySqlContext.ContactsAll.FirstOrDefault(c => c.ContactId == agreementRejectDto.UserId);
                        if (contact != null)
                        {
                            var remarkEnt = new AgreementRemark
                            {
                                Action = AutodeskConst.AgreementsAction.Rejected,
                                AgreementMasterId = agreementEnt.AgreementMasterId,
                                RecordBy = contact.ContactName,
                                RecordDateTime = DateTime.Now,
                                Remark = agreementRejectDto.Remark
                            };
                            await _mySqlContext.AgreementRemark.AddAsync(remarkEnt);
                            await _mySqlContext.SaveChangesAsync();


                            var emailBodyType = _mySqlContext.EmailBodyType.Where(t => t.EmailBodyTypeName.Contains("Agreement Reject"));
                            if (emailBodyType != null && emailBodyType.FirstOrDefault() != null)
                            {
                                var entity = emailBodyType.FirstOrDefault();
                                var emailBody = _mySqlContext.EmailBody.FirstOrDefault(b => b.BodyType == entity.EmailBodyTypeId.ToString());
                                if (emailBody != null)
                                {
                                    var bodyContent = emailBody.BodyEmail;
                                    bodyContent = bodyContent.Replace("[ReceiverName]", agreementEnt.SubmissionPerson);
                                    bodyContent = bodyContent.Replace("[DateRejected]", remarkEnt.RecordDateTime.ToShortDateString());
                                    bodyContent = bodyContent.Replace("[FY]", agreementEnt.FyindicatorKey);
                                    bodyContent = bodyContent.Replace("[OrgId]", agreementEnt.OrgId);
                                    bodyContent = bodyContent.Replace("[Approver]", contact.ContactName);
                                    bodyContent = bodyContent.Replace("[AgreementUrl]", string.Format("<a href='" + AppConfig.Config["EmailSender:AgreementUrl"] + "' target='_blank' >here</a>", agreementEnt.AgreementMasterId));

                                    await _emailService.SendMailAsync(agreementEnt.SubmissionPerson, emailBody.Subject, bodyContent);
                                }

                            }


                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> ApproveAgreement(AgreementApproveDto agreementApproveDto)
        {
            try
            {
                var agreementEnt = _mySqlContext.AgreementMaster.FirstOrDefault(a => a.AgreementMasterId == agreementApproveDto.AgreementMasterId);
                if (agreementEnt != null)
                {
                    agreementEnt.Status = AutodeskConst.AgreementsStatus.Approved;
                    _mySqlContext.AgreementMaster.Update(agreementEnt);
                    _mySqlContext.SaveChanges();

                    var contact = _mySqlContext.ContactsAll.FirstOrDefault(c => c.ContactId == agreementApproveDto.UserId);
                    if (contact != null)
                    {
                        var remark = new AgreementRemark
                        {
                            Action = AutodeskConst.AgreementsAction.Approved,
                            AgreementMasterId = agreementEnt.AgreementMasterId,
                            RecordBy = contact.ContactName,
                            RecordDateTime = DateTime.Now,
                        };
                        _mySqlContext.AgreementRemark.Add(remark);
                        _mySqlContext.SaveChanges();

                        //save agreement content to db
                        var data = Encoding.ASCII.GetBytes(agreementApproveDto.ExportFile);
                        var orgSiteAtt = new OrgSiteAttachment
                        {
                            Content = data,
                            DocType = "pdf",
                            Name = "AgreementApprove_" + remark.AgreementMasterId + "_" + remark.RecordDateTime.ToString("yyyyMMddhhmmss"),
                            OrgId = agreementEnt.OrgId,
                            Type = "pdf",

                        };
                        await _mySqlContext.OrgSiteAttachment.AddAsync(orgSiteAtt);
                        await _mySqlContext.SaveChangesAsync();
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task GenerateAgreementMaster(string fyIndicatorKey = null)
        {
            try
            {
                //if (_clickThruDbContext.AgreementMaster.Count() != 0)
                //{
                //    return;
                //}
                var listFy = await Task.Run(() => _mySqlContext.Dictionary.Where(c => c.Parent == "FYIndicator" && c.Status == AutodeskConst.StatusConts.A).Select(c => new FYDropdownListDto { FYName = c.KeyValue, KeyValue = c.KeyValue, Key = c.Key }).Distinct().ToList());
                if (!string.IsNullOrEmpty(fyIndicatorKey) && _mySqlContext.AgreementMaster.Where(x => x.FyindicatorKey == fyIndicatorKey)?.Count() == 0)
                {
                    listFy = new List<FYDropdownListDto> { new FYDropdownListDto { FYName = fyIndicatorKey, KeyValue = fyIndicatorKey, Key = fyIndicatorKey } };
                }

                //var _query = (from org in _clickThruDbContext.MainOrganization.Where(o => (o.Status == AutodeskConst.StatusConts.A || o.Status == AutodeskConst.StatusConts.P))
                //              join s in _clickThruDbContext.MainSite on org.OrgId equals s.OrgId
                //              join sr in _clickThruDbContext.SiteRoles on s.SiteId equals sr.SiteId
                //              join r in _clickThruDbContext.Roles on sr.RoleId equals r.RoleId
                //              where (r.RoleCode == "ATC" || r.RoleCode == "AAP")
                //              select new
                //              {
                //                  org.OrgId,
                //                  PartnerType = r.RoleCode
                //              } into list
                //              group list by list.OrgId
                //          ).Select(x => new
                //          {
                //              OrgId = x.Key,
                //              PartnerType = string.Join(",", x.Select(s => s.PartnerType).Distinct())
                //          });
                var _joinQuery = (
                                  from org in _mySqlContext.MainOrganization
                                  join s in _mySqlContext.MainSite on org.OrgId equals s.OrgId
                                  join sr in _mySqlContext.SiteRoles on s.SiteId equals sr.SiteId
                                  join r in _mySqlContext.Roles on sr.RoleId equals r.RoleId
                                  join ctry in _mySqlContext.RefCountries on org.RegisteredCountryCode equals ctry.CountriesCode
                                  join terr in _mySqlContext.RefTerritory on ctry.TerritoryId equals terr.TerritoryId
                                  where (sr.Status == AutodeskConst.StatusConts.A || sr.Status == AutodeskConst.StatusConts.P)
                                  where (r.RoleCode == "ATC" || r.RoleCode == "AAP")
                                  select new
                                  {
                                      FyindicatorKey = string.Empty,
                                      OrgId = org.OrgId,
                                      CheckTc = true,
                                      FirstName = string.Empty,
                                      LastName = string.Empty,
                                      Status = AutodeskConst.AgreementsStatus.Review,
                                      PartnerType = r.RoleCode,
                                      SubmissionPerson = string.Empty,

                                  }
                                  ).Distinct().ToList();
                var listAgrM = new List<AgreementMaster>();
                string sql = "INSERT INTO AgreementMaster (FYIndicatorKey,OrgId,PartnerType,Status,CheckTC,SubmissionPerson,FirstName,LastName) VALUES ";
                foreach (var item in listFy)
                {
                    var _list = _joinQuery.Select(a => new AgreementMaster
                    {
                        FyindicatorKey = item.KeyValue,
                        OrgId = a.OrgId,
                        CheckTc = true,
                        FirstName = string.Empty,
                        LastName = string.Empty,
                        Status = AutodeskConst.AgreementsStatus.Review,
                        PartnerType = a.PartnerType,
                        SubmissionPerson = string.Empty,
                    }).ToList();
                    listAgrM.AddRange(_list);
                }

                foreach (var item in listAgrM)
                {

                    sql += "('" + item.FyindicatorKey + "','" + item.OrgId + "','" + item.PartnerType + "','" + item.Status + "','" + 1 + "','" + item.SubmissionPerson + "','" + item.FirstName + "','" + item.LastName + "'),";

                    //await _clickThruDbContext.AgreementMaster.AddAsync(item);
                    //await _clickThruDbContext.SaveChangesAsync();
                }
                sql += ";";
                _mySqlContext.Database.ExecuteSqlCommand(sql);


            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<string> GetOrgName(string orgId, string userId)
        {
            var query = (from o in _mySqlContext.MainOrganization
                         join s in _mySqlContext.MainSite on o.OrgId equals s.OrgId
                         join scl in _mySqlContext.SiteContactLinks on s.SiteId equals scl.SiteId
                         join c in _mySqlContext.ContactsAll on scl.ContactId equals c.ContactId
                         where s.OrgId == orgId && c.ContactId == userId
                         select o.OrgName
                         ).Distinct();


            var name = await query.FirstOrDefaultAsync();
            if (!string.IsNullOrEmpty(name))
            {
                return name;
            }
            return string.Empty;
        }
    }
}
