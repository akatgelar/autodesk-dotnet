﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.ExcelExportService
{
    public interface IExcelExportServices
    {
        // Task<byte[]> DataTableToExcel(DataTable dt, string sheetName);
        Task<List<dynamic>> DataTableToExcel(List<dynamic> dynList, string TypeName);
        //Task<DataTable> DataTableToExcel_AAP(DataTable dt, string sheetName);
    }

    
}
