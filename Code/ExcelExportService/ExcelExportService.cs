﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace autodesk.Code.ExcelExportService
{
    public class ExcelExportService:IExcelExportServices
    {
        private static byte[] fileResult;
        
       public async Task<List<dynamic>> DataTableToExcel(List<dynamic> dynList, string TypeName)
        {
            List<dynamic> resList = new List<dynamic>();
            
            string key = "";
            bool isAAP = false;
            if (dynList.Count > 0)
            {
                #region Commented Old  Code on 12 Nov

                /*
                foreach (DataRow row in dt.Rows)
                {

                    string d = row["Q2_1"].ToString();
                    switch (row["Q2_1"].ToString())
                    {
                        case "100":
                            row["Q2_1"] = "5";
                            break;
                        case "75":
                            row["Q2_1"] = "4";
                            break;
                        case "50":
                            row["Q2_1"] = "3";
                            break;
                        case "2":
                            row["Q2_1"] = "2";
                            break;
                        case "NULL":
                            row["Q2_1"] = "1";
                            break;
                        default:
                            row["Q2_1"] = "1";
                            break;
                    }

                    switch (row["Q2_2"].ToString())
                    {
                        case "100":
                            row["Q2_2"] = "5";
                            break;
                        case "75":
                            row["Q2_2"] = "4";
                            break;
                        case "50":
                            row["Q2_2"] = "3";
                            break;
                        case "2":
                            row["Q2_2"] = "2";
                            break;
                        case "NULL":
                            row["Q2_2"] = "1";
                            break;
                        default:
                            row["Q2_2"] = "1";
                            break;
                    }

                    switch (row["Q2_3"].ToString())
                    {
                        case "100":
                            row["Q2_3"] = "5";
                            break;
                        case "75":
                            row["Q2_3"] = "4";
                            break;
                        case "50":
                            row["Q2_3"] = "3";
                            break;
                        case "2":
                            row["Q2_3"] = "2";
                            break;
                        case "NULL":
                            row["Q2_3"] = "1";
                            break;
                        default:
                            row["Q2_3"] = "1";
                            break;
                    }
                    switch (row["Q2_5"].ToString())
                    {
                        case "100":
                            row["Q2_5"] = "5";
                            break;
                        case "75":
                            row["Q2_5"] = "4";
                            break;
                        case "50":
                            row["Q2_5"] = "3";
                            break;
                        case "2":
                            row["Q2_5"] = "2";
                            break;
                        case "NULL":
                            row["Q2_5"] = "1";
                            break;
                        default:
                            row["Q2_5"] = "1";
                            break;
                    }
                    switch (row["Q2_6"].ToString())
                    {
                        case "100":
                            row["Q2_6"] = "5";
                            break;
                        case "75":
                            row["Q2_6"] = "4";
                            break;
                        case "50":
                            row["Q2_6"] = "3";
                            break;
                        case "2":
                            row["Q2_6"] = "2";
                            break;
                        case "NULL":
                            row["Q2_6"] = "1";
                            break;
                        default:
                            row["Q2_6"] = "1";
                            break;
                    }
                    switch (row["Q3_1"].ToString())
                    {
                        case "100":
                            row["Q3_1"] = "5";
                            break;
                        case "75":
                            row["Q3_1"] = "4";
                            break;
                        case "50":
                            row["Q3_1"] = "3";
                            break;
                        case "2":
                            row["Q3_1"] = "2";
                            break;
                        case "NULL":
                            row["Q3_1"] = "1";
                            break;
                        default:
                            row["Q3_1"] = "1";
                            break;
                    }
                    switch (row["Q3_2"].ToString())
                    {
                        case "100":
                            row["Q3_2"] = "5";
                            break;
                        case "75":
                            row["Q3_2"] = "4";
                            break;
                        case "50":
                            row["Q3_2"] = "3";
                            break;
                        case "2":
                            row["Q3_2"] = "2";
                            break;
                        case "NULL":
                            row["Q3_2"] = "1";
                            break;
                        default:
                            row["Q3_2"] = "1";
                            break;
                    }
                    switch (row["Q3_3"].ToString())
                    {
                        case "100":
                            row["Q3_3"] = "5";
                            break;
                        case "75":
                            row["Q3_3"] = "4";
                            break;
                        case "50":
                            row["Q3_3"] = "3";
                            break;
                        case "2":
                            row["Q3_3"] = "2";
                            break;
                        case "NULL":
                            row["Q3_3"] = "1";
                            break;
                        default:
                            row["Q3_3"] = "1";
                            break;
                    }
                    switch (row["Q4"].ToString())
                    {
                        case "100":
                            row["Q4"] = "5";
                            break;
                        case "75":
                            row["Q4"] = "4";
                            break;
                        case "50":
                            row["Q4"] = "3";
                            break;
                        case "2":
                            row["Q4"] = "2";
                            break;
                        case "NULL":
                            row["Q4"] = "1";
                            break;
                        default:
                            row["Q4"] = "1";
                            break;
                    }


                   
                    string q8 = row["Q5"].ToString();
                    if (!string.IsNullOrEmpty(q8))
                    {
                        if (q8.Length > 4)
                        {
                            q8 = q8.Substring(2, q8.Length - 4);

                            string[] q8Arr = q8.Split(',');
                            row["Q5"] = null;
                            foreach (string item in q8Arr)
                            {
                                string q8Data = item.ToString();

                                q8Data = q8Data.Replace('\"', ' ');
                                q8Data = q8Data.Trim(' ');
                                if (!string.IsNullOrEmpty(q8Data) && q8Data != " " && q8Data != "' '")
                                {
                                    row["Q5"] += q8Data.Trim('"');
                                    row["Q5"] += ",";
                                }

                            }
                            if (row["Q5"].ToString().Length > 0) { 
                            row["Q5"] = row["Q5"].ToString().Remove(row["Q5"].ToString().Length - 1);
                            }
                            else
                            {
                                row["Q5"] = "";
                            }
                        }
                        else
                        {
                            row["Q5"] = " ";
                        }


                        switch (row["Q9_2"].ToString())
                        {
                            case "100":
                                row["Q9_2"] = "5";
                                break;
                            case "75":
                                row["Q9_2"] = "4";
                                break;
                            case "50":
                                row["Q9_2"] = "3";
                                break;
                            case "2":
                                row["Q9_2"] = "2";
                                break;
                            case "NULL":
                                row["Q9_2"] = "1";
                                break;
                            default:
                                row["Q9_2"] = "1";
                                break;
                        }

                        switch (row["Q9_3"].ToString())
                        {
                            case "100":
                                row["Q9_3"] = "5";
                                break;
                            case "75":
                                row["Q9_3"] = "4";
                                break;
                            case "50":
                                row["Q9_3"] = "3";
                                break;
                            case "2":
                                row["Q9_3"] = "2";
                                break;
                            case "NULL":
                                row["Q9_3"] = "1";
                                break;
                            default:
                                row["Q9_3"] = "1";
                                break;
                        }

                        switch (row["Q9_4"].ToString())
                        {
                            case "100":
                                row["Q9_4"] = "5";
                                break;
                            case "75":
                                row["Q9_4"] = "4";
                                break;
                            case "50":
                                row["Q9_4"] = "3";
                                break;
                            case "2":
                                row["Q9_4"] = "2";
                                break;
                            case "NULL":
                                row["Q9_4"] = "1";
                                break;
                            default:
                                row["Q9_4"] = "1";
                                break;
                        }



                    }
                    else
                    {
                        row["Q5"] = " ";
                    }
                }
                */

                #endregion

                #region Changed to convert in Dataservice 


                /*
                if (TypeName.Equals("ATC"))
                {
                    key = "Q";
                }
                else
                {
                    key = "QUESTION";
                    isAAP = true;
                }
               
               var expando = dynList.Select(o => ((IDictionary<string, object>)o).ToDictionary(nvp => nvp.Key, nvp => nvp.Value));
                int idx = 0;
                foreach (Dictionary<string, object> objValue in expando)
                {
                    //var idx = dynList.IndexOf(objValue);
                    ExpandoObject expandoObj = dynList[idx];
                    if (objValue.ContainsKey(key+"5") || objValue.ContainsKey(key+"6")|| objValue.ContainsKey(key+"12") || objValue.ContainsKey(key + "11")) {
                        foreach (var values in objValue)
                        {
                            if (isAAP == false)
                            {
                                if (values.Key.Equals(key + "5") || values.Key.Equals(key + "6") || values.Key.Equals(key + "12"))
                                {
                                    List<string> objValues = values.Value.ToString().Split(',').ToList();
                                    ((IDictionary<String, Object>)expandoObj).Remove(values.Key);

                                    int i = 0;
                                    foreach (var listValue in objValues)
                                    {

                                        int s = i + 1;
                                        string name = values.Key.ToString() + "_" + s;
                                        switch (listValue)
                                        {

                                            case "100":
                                                expandoObj.TryAdd(name, 5);
                                                break;
                                            case "75":
                                                expandoObj.TryAdd(name, 4);
                                                break;
                                            case "50":
                                                expandoObj.TryAdd(name, 3);
                                                break;
                                            case "2":
                                                expandoObj.TryAdd(name, 2);
                                                break;
                                            case "NULL":
                                                expandoObj.TryAdd(name, 1);
                                                break;
                                            default:
                                                expandoObj.TryAdd(name, 1);
                                                break;
                                        }

                                        i++;
                                    }

                                }
                            }
                            else
                            {
                                if (values.Key.Equals(key + "5") || values.Key.Equals(key + "6") || values.Key.Equals(key + "11"))
                                {
                                    List<string> objValues = values.Value.ToString().Split(',').ToList();
                                    ((IDictionary<String, Object>)expandoObj).Remove(values.Key);

                                    int i = 0;
                                    foreach (var listValue in objValues)
                                    {

                                        int s = i + 1;
                                        string name = values.Key.ToString() + "_" + s;
                                        switch (listValue)
                                        {

                                            case "100":
                                                expandoObj.TryAdd(name, 5);
                                                break;
                                            case "75":
                                                expandoObj.TryAdd(name, 4);
                                                break;
                                            case "50":
                                                expandoObj.TryAdd(name, 3);
                                                break;
                                            case "2":
                                                expandoObj.TryAdd(name, 2);
                                                break;
                                            case "NULL":
                                                expandoObj.TryAdd(name, 1);
                                                break;
                                            default:
                                                expandoObj.TryAdd(name, 1);
                                                break;
                                        }

                                        i++;
                                    }

                                }
                            }
                           
                        }
                        resList.Add(expandoObj);
                       
                    }

                    idx++;
                }
            }
        */


                #endregion
            }

            return resList;
        }


        /*
         public async Task<DataTable> DataTableToExcel_AAP(DataTable dt, string sheetName)
        {
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {

                    string d = row["Q2_1"].ToString();
                    switch (row["Q2_1"].ToString())
                    {
                        case "100":
                            row["Q2_1"] = "5";
                            break;
                        case "75":
                            row["Q2_1"] = "4";
                            break;
                        case "50":
                            row["Q2_1"] = "3";
                            break;
                        case "2":
                            row["Q2_1"] = "2";
                            break;
                        case "NULL":
                            row["Q2_1"] = "1";
                            break;
                        default:
                            row["Q2_1"] = "1";
                            break;
                    }

                    switch (row["Q2_2"].ToString())
                    {
                        case "100":
                            row["Q2_2"] = "5";
                            break;
                        case "75":
                            row["Q2_2"] = "4";
                            break;
                        case "50":
                            row["Q2_2"] = "3";
                            break;
                        case "2":
                            row["Q2_2"] = "2";
                            break;
                        case "NULL":
                            row["Q2_2"] = "1";
                            break;
                        default:
                            row["Q2_2"] = "1";
                            break;
                    }

                    switch (row["Q2_3"].ToString())
                    {
                        case "100":
                            row["Q2_3"] = "5";
                            break;
                        case "75":
                            row["Q2_3"] = "4";
                            break;
                        case "50":
                            row["Q2_3"] = "3";
                            break;
                        case "2":
                            row["Q2_3"] = "2";
                            break;
                        case "NULL":
                            row["Q2_3"] = "1";
                            break;
                        default:
                            row["Q2_3"] = "1";
                            break;
                    }
                    switch (row["Q2_5"].ToString())
                    {
                        case "100":
                            row["Q2_5"] = "5";
                            break;
                        case "75":
                            row["Q2_5"] = "4";
                            break;
                        case "50":
                            row["Q2_5"] = "3";
                            break;
                        case "2":
                            row["Q2_5"] = "2";
                            break;
                        case "NULL":
                            row["Q2_5"] = "1";
                            break;
                        default:
                            row["Q2_5"] = "1";
                            break;
                    }
                    switch (row["Q2_6"].ToString())
                    {
                        case "100":
                            row["Q2_6"] = "5";
                            break;
                        case "75":
                            row["Q2_6"] = "4";
                            break;
                        case "50":
                            row["Q2_6"] = "3";
                            break;
                        case "2":
                            row["Q2_6"] = "2";
                            break;
                        case "NULL":
                            row["Q2_6"] = "1";
                            break;
                        default:
                            row["Q2_6"] = "1";
                            break;
                    }
                    switch (row["Q3_1"].ToString())
                    {
                        case "100":
                            row["Q3_1"] = "5";
                            break;
                        case "75":
                            row["Q3_1"] = "4";
                            break;
                        case "50":
                            row["Q3_1"] = "3";
                            break;
                        case "2":
                            row["Q3_1"] = "2";
                            break;
                        case "NULL":
                            row["Q3_1"] = "1";
                            break;
                        default:
                            row["Q3_1"] = "1";
                            break;
                    }
                    switch (row["Q3_2"].ToString())
                    {
                        case "100":
                            row["Q3_2"] = "5";
                            break;
                        case "75":
                            row["Q3_2"] = "4";
                            break;
                        case "50":
                            row["Q3_2"] = "3";
                            break;
                        case "2":
                            row["Q3_2"] = "2";
                            break;
                        case "NULL":
                            row["Q3_2"] = "1";
                            break;
                        default:
                            row["Q3_2"] = "1";
                            break;
                    }
                    switch (row["Q3_3"].ToString())
                    {
                        case "100":
                            row["Q3_3"] = "5";
                            break;
                        case "75":
                            row["Q3_3"] = "4";
                            break;
                        case "50":
                            row["Q3_3"] = "3";
                            break;
                        case "2":
                            row["Q3_3"] = "2";
                            break;
                        case "NULL":
                            row["Q3_3"] = "1";
                            break;
                        default:
                            row["Q3_3"] = "1";
                            break;
                    }
                    switch (row["Q4"].ToString())
                    {
                        case "100":
                            row["Q4"] = "5";
                            break;
                        case "75":
                            row["Q4"] = "4";
                            break;
                        case "50":
                            row["Q4"] = "3";
                            break;
                        case "2":
                            row["Q4"] = "2";
                            break;
                        case "NULL":
                            row["Q4"] = "1";
                            break;
                        default:
                            row["Q4"] = "1";
                            break;
                    }



                    string q8 = row["Q5"].ToString();
                    if (!string.IsNullOrEmpty(q8))
                    {
                        if (q8.Length > 4)
                        {
                            q8 = q8.Substring(2, q8.Length - 4);

                            string[] q8Arr = q8.Split(',');
                            row["Q5"] = null;
                            foreach (string item in q8Arr)
                            {
                                string q8Data = item.ToString();

                                q8Data = q8Data.Replace('\"', ' ');
                                q8Data = q8Data.Trim(' ');
                                if (!string.IsNullOrEmpty(q8Data) && q8Data != " " && q8Data != "' '")
                                {
                                    row["Q5"] += q8Data.Trim('"');
                                    row["Q5"] += ",";
                                }

                            }
                            if (row["Q5"].ToString().Length > 0)
                            {
                                row["Q5"] = row["Q5"].ToString().Remove(row["Q5"].ToString().Length - 1);
                            }
                            else
                            {
                                row["Q5"] = "";
                            }
                        }
                        else
                        {
                            row["Q5"] = " ";
                        }


                        switch (row["Q8_1"].ToString())
                        {
                            case "100":
                                row["Q8_1"] = "5";
                                break;
                            case "75":
                                row["Q8_1"] = "4";
                                break;
                            case "50":
                                row["Q8_1"] = "3";
                                break;
                            case "2":
                                row["Q8_1"] = "2";
                                break;
                            case "NULL":
                                row["Q8_1"] = "1";
                                break;
                            default:
                                row["Q8_1"] = "1";
                                break;
                        }

                        switch (row["Q8_2"].ToString())
                        {
                            case "100":
                                row["Q8_2"] = "5";
                                break;
                            case "75":
                                row["Q8_2"] = "4";
                                break;
                            case "50":
                                row["Q8_2"] = "3";
                                break;
                            case "2":
                                row["Q8_2"] = "2";
                                break;
                            case "NULL":
                                row["Q8_2"] = "1";
                                break;
                            default:
                                row["Q8_2"] = "1";
                                break;
                        }
                        
                        switch (row["Q8_3"].ToString())
                        {
                            case "100":
                                row["Q8_3"] = "5";
                                break;
                            case "75":
                                row["Q8_3"] = "4";
                                break;
                            case "50":
                                row["Q8_3"] = "3";
                                break;
                            case "2":
                                row["Q8_3"] = "2";
                                break;
                            case "NULL":
                                row["Q8_3"] = "1";
                                break;
                            default:
                                row["Q8_3"] = "1";
                                break;
                        }
                        switch (row["Q8_4"].ToString())
                        {
                            case "100":
                                row["Q8_4"] = "5";
                                break;
                            case "75":
                                row["Q8_4"] = "4";
                                break;
                            case "50":
                                row["Q8_4"] = "3";
                                break;
                            case "2":
                                row["Q8_4"] = "2";
                                break;
                            case "NULL":
                                row["Q8_4"] = "1";
                                break;
                            default:
                                row["Q8_4"] = "1";
                                break;
                        }


                    }
                    else
                    {
                        row["Q5"] = " ";
                    }
                }

            }

            return dt;
        }

        */

        #region need to apply this logic for some questions like q5,q12
        /*
         selectedScore(score) {
        var choosen: number = 0;
        switch (score) {
            case 100:
                choosen = 5;
                break;
            case 75:
                choosen = 4;
                break;
            case 50:
                choosen = 3;
                break;
            case 25:
                choosen = 2;
                break;
            default:
                choosen = 1;
                break;
        }
        return choosen;
    }

    question8(val) {
        var choosen: string = "";
        switch (val) {
            case "1":
                choosen = "Architecture, Engineering & Construction";
                break;
            case "2":
                choosen = "Automotive & Transportation";
                break;
            case "3":
                choosen = "Government";
                break;
            case "4":
                choosen = "Infrastructure and Civil Engineering";
                break;
            case "5":
                choosen = "Manufacturing";
                break;
            case "6":
                choosen = "Media & Entertainment";
                break;
            case "7":
                choosen = "Utilities & Telecommunications";
                break;

            default:
                choosen = "Other";
                break;
        }
        return choosen;
    }
         
         */
        #endregion
    }
}
