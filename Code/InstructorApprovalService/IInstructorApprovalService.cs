﻿using autodesk.Code.InstructorApprovalService.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.InstructorApprovalService
{
    public interface IInstructorApprovalService
    {
        Task<List<StudentDto>> GetStudents();
    }
}
