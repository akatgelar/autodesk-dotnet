﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.BackUpDbContext
{
    public partial class LookUpTable
    {
        public int Id { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string IsNeed { get; set; }
    }
}
