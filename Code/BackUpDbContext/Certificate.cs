﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.BackUpDbContext
{
    public partial class Certificate
    {
        public int CertificateId { get; set; }
        public int? PartnerTypeId { get; set; }
        public int? CertificateTypeId { get; set; }
        public string CertificateBackgroundId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
        public string Htmldesign { get; set; }
        public string Status { get; set; }
        public string TitleTextHeader { get; set; }
        public string TitleHeaderDescription { get; set; }
        public string TitleTextFooter { get; set; }
        public string TitleCertificateNo { get; set; }
        public string TitleStudentName { get; set; }
        public string TitleEvent { get; set; }
        public string TitleCourseTitle { get; set; }
        public string TitleProduct { get; set; }
        public string TitleInstructor { get; set; }
        public string TitleDate { get; set; }
        public string TitleDuration { get; set; }
        public string TitlePartner { get; set; }
        public string TitleInstitution { get; set; }
        public string TitleLocation { get; set; }
        public string LanguageCertificate { get; set; }
    }
}
