﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.BackUpDbContext
{
    public partial class LanguagePack
    {
        public int LanguagePackId { get; set; }
        public string LanguageCountry { get; set; }
        public string LanguageJson { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
