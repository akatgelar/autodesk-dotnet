﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace autodesk.Code.BackUpDbContext
{
    public partial class backupdbContext : DbContext
    {
        public backupdbContext()
        {
        }

        public backupdbContext(DbContextOptions<backupdbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Certificate> Certificate { get; set; }
        public virtual DbSet<EvaluationQuestion> EvaluationQuestion { get; set; }
        public virtual DbSet<LanguagePack> LanguagePack { get; set; }
        public virtual DbSet<LookUpTable> LookUpTable { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
         if (!optionsBuilder.IsConfigured)
         {
          optionsBuilder.UseMySQL(AppConfig.Config["ConnectionStrings:MySQLBackUpConnection"]);
         }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<Certificate>(entity =>
            {
                entity.ToTable("Certificate");

                entity.Property(e => e.CertificateId).HasColumnType("int(8)");

                entity.Property(e => e.CertificateBackgroundId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.CertificateTypeId).HasColumnType("int(1)");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(11)
                    .IsUnicode(false);

                entity.Property(e => e.Htmldesign)
                    .IsRequired()
                    .HasColumnName("HTMLDesign")
                    .HasColumnType("longtext");

                entity.Property(e => e.LanguageCertificate)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.PartnerTypeId).HasColumnType("int(2)");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasColumnType("char(1)");

                entity.Property(e => e.TitleCertificateNo).IsUnicode(false);

                entity.Property(e => e.TitleCourseTitle).IsUnicode(false);

                entity.Property(e => e.TitleDate).IsUnicode(false);

                entity.Property(e => e.TitleDuration).IsUnicode(false);

                entity.Property(e => e.TitleEvent).IsUnicode(false);

                entity.Property(e => e.TitleHeaderDescription).IsUnicode(false);

                entity.Property(e => e.TitleInstitution).IsUnicode(false);

                entity.Property(e => e.TitleInstructor).IsUnicode(false);

                entity.Property(e => e.TitleLocation).IsUnicode(false);

                entity.Property(e => e.TitlePartner).IsUnicode(false);

                entity.Property(e => e.TitleProduct).IsUnicode(false);

                entity.Property(e => e.TitleStudentName).IsUnicode(false);

                entity.Property(e => e.TitleTextFooter).IsUnicode(false);

                entity.Property(e => e.TitleTextHeader).IsUnicode(false);

                entity.Property(e => e.Year)
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EvaluationQuestion>(entity =>
            {
                entity.ToTable("EvaluationQuestion");

                entity.HasIndex(e => e.CourseId)
                    .HasName("IDX_EvaCourseId");

                entity.HasIndex(e => e.EvaluationQuestionCode)
                    .HasName("IDX_EvaQCode");

                entity.HasIndex(e => e.EvaluationQuestionId)
                    .HasName("IDX_EvaQId");

                entity.Property(e => e.EvaluationQuestionId).HasColumnType("int(11)");

                entity.Property(e => e.CertificateTypeId).HasColumnType("int(2)");

                entity.Property(e => e.CountryCode)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CourseId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EvaluationQuestionCode)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EvaluationQuestionJson).HasColumnType("longtext");

                entity.Property(e => e.EvaluationQuestionTemplate).HasColumnType("longtext");

                entity.Property(e => e.LanguageId)
                    .HasColumnName("LanguageID")
                    .HasColumnType("int(2)");

                entity.Property(e => e.PartnerType)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Year)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LanguagePack>(entity =>
            {
                entity.ToTable("LanguagePack");

                entity.Property(e => e.LanguagePackId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageCountry)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LanguageJson).HasColumnType("longtext");

                entity.Property(e => e.Status)
                    .HasMaxLength(255)
                    .IsUnicode(false);
                entity.Property(e => e.CreatedDate)               
                 .IsUnicode(false);
            });

            modelBuilder.Entity<LookUpTable>(entity =>
            {
                entity.ToTable("LookUpTable");

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IsNeed)
                 .IsRequired()
                 .HasMaxLength(5)
                 .IsUnicode(false);
            });
        }
    }
}
