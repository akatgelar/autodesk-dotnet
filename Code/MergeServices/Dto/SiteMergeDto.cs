﻿using System.Collections.Generic;

namespace autodesk.Code.MergeServices.Dto
{
    public class SiteMergeDto
    {
        [ForDisplay(true)]
        public string SiteId { get; set; }
        [ForDisplay(true)]
        public string OrgId { get; set; }

        //SiteName
        public string SiteName { get; set; }

        //EnglishSiteName
        public string EnglishSiteName { get; set; }

        [ForDisplay(true)]
        public string CommercialSiteName { get; set; }
        [ForDisplay(true)]
        public string SiebelSiteName { get; set; }

        //SiteWebAddress
        public string SiteWebAddress { get; set; }

        //Workstations
        public int? Workstations { get; set; }

        [ForDisplay(true)]
        public string MagellanId { get; set; }

        //SiteTelephone
        public string SiteTelephone { get; set; }

        [ForDisplay(true)]
        public string SiteFax { get; set; }

        //SiteEmailAddress
        public string SiteEmailAddress { get; set; }

        //SiteAdmin
        public string SiteAdminFirstName { get; set; }
        public string SiteAdminLastName { get; set; }
        public string SiteAdminEmailAddress { get; set; }
        public string SiteAdminTelephone { get; set; }
        public string SiteAdminFax { get; set; }

        //SiteManager
        public string SiteManagerFirstName { get; set; }
        public string SiteManagerLastName { get; set; }
        public string SiteManagerEmailAddress { get; set; }
        public string SiteManagerTelephone { get; set; }
        public string SiteManagerFax { get; set; }

        //SiteAddress
        public string SiteDepartment { get; set; }
        public string SiteAddress1 { get; set; }
        public string SiteAddress2 { get; set; }
        public string SiteAddress3 { get; set; }
        public string SiteCity { get; set; }
        public string SiteStateProvince { get; set; }
        public string SiteCountryCode { get; set; }
        public string SitePostalCode { get; set; }

        //MailingAddress
        public string MailingDepartment { get; set; }
        public string MailingAddress1 { get; set; }
        public string MailingAddress2 { get; set; }
        public string MailingAddress3 { get; set; }
        public string MailingCity { get; set; }
        public string MailingStateProvince { get; set; }
        public string MailingCountryCode { get; set; }
        public string MailingPostalCode { get; set; }

        //ShippingAddress
        public string ShippingDepartment { get; set; }
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingAddress3 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingStateProvince { get; set; }
        public string ShippingCountryCode { get; set; }
        public string ShippingPostalCode { get; set; }

        //PartnerTypes
        public List<PartnerType> PartnerTypes { get; set; }

        //Contacts
        public List<Contact> Contacts { get; set; }

        //JournalEntries
        public List<JournalEntry> JournalEntries { get; set; }

        //Attachments
        public List<Attachment> Attachments { get; set; }


        //Attributes
        public string SiteStatusRetired { get; set; }

        public string Status { get; set; }

    }
    public class PartnerType
    {
        public string Type { get; set; }
        public string Status { get; set; }
        public string CSN { get; set; }
        public int SiteRoleId { get; set; }
    }
    public class Contact
    {
        public string ContactId { get; set; }
        public string ContactName { get; set; }
    }
    public class Attachment {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
