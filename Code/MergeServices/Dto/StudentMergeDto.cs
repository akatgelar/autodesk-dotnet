﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.MergeServices.Dto
{
    public class StudentMergeDto
    {
        [ForDisplay(true)]
        public int StudentID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Salutation { get; set; }
        public string PrimaryLanguage { get; set; }
        public string SecondaryLanguage { get; set; }
        public string Gender { get; set; }
        public string Company { get; set; }
        public string PrimaryIndustryId { get; set; }
        public string Address { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string CountryID { get; set; }
        public int? StateID { get; set; }
        public string StateProvince { get; set; }
        public string PostalCode { get; set; }
        public string Status { get; set; }
        public string TelephoneCode { get; set; }
        public string Phone { get; set; }
        public string MobileCode { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Password_plain { get; set; }
        public int LanguageID { get; set; }
        public int? ExternalID { get; set; }
        public string PWDChangeRequired { get; set; }
        public short? LoginCount { get; set; }
        public DateTime? LastLogin { get; set; }
        public string LastIPAddress { get; set; }
        public string ResetPasswordCode { get; set; }
        public DateTime? ResetPasswordDate { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateRequestTerminated { get; set; }
        public DateTime? DateBirth { get; set; }
        public string StatusLevel { get; set; }
        public DateTime? ExpetationGradDate { get; set; }
        public string Email2 { get; set; }
        public short? TermAndCondition { get; set; }

        public List<JournalEntry> JournalEntries { get; set; }


    }
}
