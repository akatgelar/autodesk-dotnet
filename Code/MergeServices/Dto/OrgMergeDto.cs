﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.MergeServices.Dto
{
    public class OrgMergeDto
    {
        //OrgId
        [ForDisplay(true)]
        public string OrgId { get; set; }

        //OrgName
        public string OrgName { get; set; }
        public string EnglishOrgName { get; set; }
        [ForDisplay(true)]
        public string CommercialOrgName { get; set; }

        //OrgWebAddress
        public string OrgWebAddress { get; set; }
        [ForDisplay(true)]
        public string YearJoined { get; set; }
        [ForDisplay(true)]
        public string TaxExempt { get; set; }
        [ForDisplay(true)]
        public string Vatnumber { get; set; }
        [ForDisplay(true)]
        public string SapsoldTo { get; set; }

        //AtcDirector
        public string AtcdirectorFirstName { get; set; }
        public string AtcdirectorLastName { get; set; }
        public string AtcdirectorEmailAddress { get; set; }
        public string AtcdirectorTelephone { get; set; }
        public string AtcdirectorFax { get; set; }

        //LegalContact
        public string LegalContactFirstName { get; set; }
        public string LegalContactLastName { get; set; }
        public string LegalContactEmailAddress { get; set; }
        public string LegalContactTelephone { get; set; }
        public string LegalContactFax { get; set; }

        //BillingContact
        public string BillingContactFirstName { get; set; }
        public string BillingContactLastName { get; set; }
        public string BillingContactEmailAddress { get; set; }
        public string BillingContactTelephone { get; set; }
        public string BillingContactFax { get; set; }

        //RegisteredAddress
        public string RegisteredDepartment { get; set; }
        public string RegisteredAddress1 { get; set; }
        public string RegisteredAddress2 { get; set; }
        public string RegisteredAddress3 { get; set; }
        public string RegisteredCity { get; set; }
        public string RegisteredStateProvince { get; set; }
        public string RegisteredPostalCode { get; set; }
        public string RegisteredCountryCode { get; set; }

        //InvoicingAddress
        public string InvoicingDepartment { get; set; }
        public string InvoicingAddress1 { get; set; }
        public string InvoicingAddress2 { get; set; }
        public string InvoicingAddress3 { get; set; }
        public string InvoicingCity { get; set; }
        public string InvoicingStateProvince { get; set; }
        public string InvoicingPostalCode { get; set; }
        public string InvoicingCountryCode { get; set; }

        //ContactAddress
        public string ContractDepartment { get; set; }
        public string ContractAddress1 { get; set; }
        public string ContractAddress2 { get; set; }
        public string ContractAddress3 { get; set; }
        public string ContractCity { get; set; }
        public string ContractStateProvince { get; set; }
        public string ContractPostalCode { get; set; }
        public string ContractCountryCode { get; set; }

        //Sites
        public List<Site> Sites { get; set; }

        //JournalEntries
        public List<JournalEntry> JournalEntries { get; set; }

        //Attributes,...
        [ForDisplay(true)]
        public string Status { get; set; }
        [ForDisplay(true)]
        public string OrgStatusRetired { get; set; }
    }

    //AtcDirector
    public class AtcDirector
    {
        public string AtcdirectorFirstName { get; set; }
        public string AtcdirectorLastName { get; set; }
        public string AtcdirectorEmailAddress { get; set; }
        public string AtcdirectorTelephone { get; set; }
        public string AtcdirectorFax { get; set; }
    }

    //LegalContact
    public class LegalContact
    {
        public string LegalContactFirstName { get; set; }
        public string LegalContactLastName { get; set; }
        public string LegalContactEmailAddress { get; set; }
        public string LegalContactTelephone { get; set; }
        public string LegalContactFax { get; set; }
    }

    //BillingContact
    public class BillingContact
    {
        public string BillingContactFirstName { get; set; }
        public string BillingContactLastName { get; set; }
        public string BillingContactEmailAddress { get; set; }
        public string BillingContactTelephone { get; set; }
        public string BillingContactFax { get; set; }
    }

    //RegisteredAddress
    public class RegisteredAddress
    {
        public string RegisteredDepartment { get; set; }
        public string RegisteredAddress1 { get; set; }
        public string RegisteredAddress2 { get; set; }
        public string RegisteredAddress3 { get; set; }
        public string RegisteredCity { get; set; }
        public string RegisteredStateProvince { get; set; }
        public string RegisteredPostalCode { get; set; }
        public string RegisteredCountryCode { get; set; }
    }

    //InvoicingAddress
    public class InvoicingAddress
    {
        public string InvoicingDepartment { get; set; }
        public string InvoicingAddress1 { get; set; }
        public string InvoicingAddress2 { get; set; }
        public string InvoicingAddress3 { get; set; }
        public string InvoicingCity { get; set; }
        public string InvoicingStateProvince { get; set; }
        public string InvoicingPostalCode { get; set; }
        public string InvoicingCountryCode { get; set; }
    }

    //ContactAddress
    public class ContactAddress
    {
        public string ContractDepartment { get; set; }
        public string ContractAddress1 { get; set; }
        public string ContractAddress2 { get; set; }
        public string ContractAddress3 { get; set; }
        public string ContractCity { get; set; }
        public string ContractStateProvince { get; set; }
        public string ContractPostalCode { get; set; }
        public string ContractCountryCode { get; set; }
    }

    //Sites
    public class Site
    {
        public string SiteId { get; set; }
        public string SiteName { get; set; }
        public string PartnerType { get; set; }
    }

    //JournalEntries
    public class JournalEntry
    {
        public int JournalId { get; set; }
        public string ParentId { get; set; }
        public string EntryType { get; set; }
        public string NoteValue { get; set; }
    }
}
