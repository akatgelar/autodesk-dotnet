﻿using autodesk.Code.MergeServices.Dto;
using autodesk.Code.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace autodesk.Code.MergeServices
{
    public class MergeServices : IMergeServices
    {
        private readonly MySQLContext _mySQLContext;
        public MergeServices(MySQLContext mySQLContext)
        {
            _mySQLContext = mySQLContext;
        }

        public ContactMergeDto GetContact(string userId)
        {
            var contact = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactId == userId);
            if (contact != null)
            {
                var listSite = (from site in _mySQLContext.MainSite
                                join siteLink in _mySQLContext.SiteContactLinks on site.SiteId equals siteLink.SiteId

                                where siteLink.ContactId == userId
                                select new Site
                                {
                                    SiteId = site.SiteId,
                                    SiteName = site.SiteName,

                                });
                var listJournal = (from journal in _mySQLContext.Journals
                                   join journalAct in _mySQLContext.JournalActivities on journal.ActivityId equals journalAct.ActivityId
                                   where journal.ParentId == userId
                                   select new JournalEntry
                                   {
                                       JournalId = journal.JournalId,
                                       ParentId = userId,
                                       EntryType = journalAct.ActivityName,
                                       NoteValue = journal.Notes
                                   }
                                 );
                return new ContactMergeDto
                {
                    ContactId = contact.ContactId,
                    SiebelId = contact.SiebelId,
                    SiebelStatus = contact.SiebelStatus,
                    PrimarySiteId = contact.PrimarySiteId,
                    ContactName = contact.ContactName,
                    EnglishContactName = contact.EnglishContactName,
                    FirstName = contact.FirstName,
                    LastName = contact.LastName,
                    Salutation = contact.Salutation,
                    TitlePosition = contact.TitlePosition,
                    EmailAddress = contact.EmailAddress,
                    EmailAddress2 = contact.EmailAddress2,
                    EmailAddress3 = contact.EmailAddress3,
                    EmailAddress4 = contact.EmailAddress4,
                    DoNotEmail = contact.DoNotEmail,
                    Username = contact.Username,
                    Password = contact.Password,
                    PasswordPlain = contact.PasswordPlain,
                    PwdchangeRequired = contact.PwdchangeRequired,
                    PermissionsGroup = contact.PermissionsGroup,
                    HireDate = contact.HireDate,
                    HowLongWithOrg = contact.HowLongWithOrg,
                    HowLongInIndustry = contact.HowLongInIndustry,
                    HowLongAutodeskProducts = contact.HowLongAutodeskProducts,
                    ExperiencedCompetitiveProducts = contact.ExperiencedCompetitiveProducts,
                    ExperiencedCompetitiveProductsList = contact.ExperiencedCompetitiveProductsList,
                    QuestionnaireDate = contact.QuestionnaireDate,
                    PercentPreSales = contact.PercentPreSales,
                    PercentPostSales = contact.PercentPostSales,
                    Dedicated = contact.Dedicated,
                    Department = contact.Department,
                    InternalSales = contact.InternalSales,
                    PrimaryRoleId = contact.PrimaryRoleId,
                    PrimaryIndustryId = contact.PrimaryIndustryId,
                    PrimarySubIndustryId = contact.PrimarySubIndustryId,
                    PrimaryRoleExperience = contact.PrimaryRoleExperience,
                    Atcrole = contact.Atcrole,
                    Telephone1 = contact.Telephone1,
                    Telephone2 = contact.Telephone2,
                    Mobile1 = contact.Mobile1,
                    Mobile2 = contact.Mobile2,
                    PrimaryLanguage = contact.PrimaryLanguage,
                    SecondaryLanguage = contact.SecondaryLanguage,
                    Timezone = contact.Timezone,
                    TimeZoneUtc = contact.TimeZoneUtc,
                    Comments = contact.Comments,
                    InstructorId = contact.InstructorId,
                    LoginCount = contact.LoginCount,
                    LastLogin = contact.LastLogin,
                    LastIpaddress = contact.LastIpaddress,
                    Status = contact.Status,
                    CsouserUuid = contact.CsouserUuid,
                    DateAdded = contact.DateAdded,
                    AddedBy = contact.AddedBy,
                    DateLastAdmin = contact.DateLastAdmin,
                    LastAdminBy = contact.LastAdminBy,
                    Action = contact.Action,
                    CsoupdatedOn = contact.CsoupdatedOn,
                    Csoversion = contact.Csoversion,
                    Administrator = contact.Administrator,
                    ShowDuplicates = contact.ShowDuplicates,
                    Level = contact.Level,
                    AdminSystems = contact.AdminSystems,
                    CompanyName = contact.CompanyName,
                    ResetToken = contact.ResetToken,
                    ResetTokenExpiration = contact.ResetTokenExpiration,
                    Address1 = contact.Address1,
                    Address2 = contact.Address2,
                    Address3 = contact.Address3,
                    City = contact.City,
                    PostalCode = contact.PostalCode,
                    CountryCode = contact.CountryCode,
                    StateProvince = contact.StateProvince,
                    Gender = contact.Gender,
                    Designation = contact.Designation,
                    WebsiteUrl = contact.WebsiteUrl,
                    Bio = contact.Bio,
                    ShowInSearch = contact.ShowInSearch,
                    ShareEmail = contact.ShareEmail,
                    ShareTelephone = contact.ShareTelephone,
                    ShareMobile = contact.ShareMobile,
                    AcimemberId = contact.AcimemberId,
                    MobileCode = contact.MobileCode,
                    TelephoneCode = contact.TelephoneCode,
                    ProfilePicture = contact.ProfilePicture,
                    UserLevelId = contact.UserLevelId,
                    ResetPasswordCode = contact.ResetPasswordCode,
                    ResetPasswordDate = contact.ResetPasswordDate,
                    DateBirth = contact.DateBirth,
                    StatusEmailSend = contact.StatusEmailSend,
                    AdditionSites = listSite.ToList(),
                    JournalEntries = listJournal.ToList()

                };


            }
            return null;
        }

        public OrgMergeDto GetOrg(string orgId)
        {
            var org = _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == orgId&& o.Status!=AutodeskConst.StatusConts.X && o.OrgStatusRetired!=AutodeskConst.StatusConts.A);
            if (org != null)
            {
                var listSite = (from site in _mySQLContext.MainSite
                                join siteRole in _mySQLContext.SiteRoles on site.SiteId equals siteRole.SiteId
                                join role in _mySQLContext.Roles on siteRole.RoleId equals role.RoleId
                                where site.OrgId == orgId && (siteRole.Status != AutodeskConst.StatusConts.X && siteRole.Status != AutodeskConst.StatusConts.T)
                                && (site.Status != AutodeskConst.StatusConts.X && site.Status != AutodeskConst.StatusConts.D)
                                select new
                                {
                                    SiteId = site.SiteId,
                                    SiteName = site.SiteName,
                                    PartnerType = role.RoleName,
                                } into list
                                group list by new { list.SiteId, list.SiteName }
                                ).Select(x => new Site { SiteId = x.Key.SiteId, SiteName = x.Key.SiteName, PartnerType = string.Join(',', x.Select(p => p.PartnerType)) });
                var listJournal = (from journal in _mySQLContext.Journals
                                   join journalAct in _mySQLContext.JournalActivities on journal.ActivityId equals journalAct.ActivityId
                                   where journal.ParentId == orgId
                                   select new JournalEntry
                                   {
                                       JournalId = journal.JournalId,
                                       ParentId = orgId,
                                       EntryType = journalAct.ActivityName,
                                       NoteValue = journal.Notes
                                   }
                                   );
                return new OrgMergeDto
                {
                    OrgId = org.OrgId,

                    OrgName = org.OrgName,
                    EnglishOrgName = org.EnglishOrgName,
                    CommercialOrgName = org.CommercialOrgName,

                    OrgWebAddress = org.OrgWebAddress,
                    YearJoined = org.YearJoined,
                    TaxExempt = org.TaxExempt,
                    Vatnumber = org.Vatnumber,
                    SapsoldTo = org.SapsoldTo,

                    AtcdirectorEmailAddress = org.AtcdirectorEmailAddress,
                    AtcdirectorFax = org.AtcdirectorFax,
                    AtcdirectorFirstName = org.AtcdirectorFirstName,
                    AtcdirectorLastName = org.AtcdirectorLastName,
                    AtcdirectorTelephone = org.AtcdirectorTelephone
                    ,
                    LegalContactEmailAddress = org.LegalContactEmailAddress,
                    LegalContactFax = org.LegalContactFax,
                    LegalContactFirstName = org.LegalContactFirstName,
                    LegalContactLastName = org.LegalContactLastName,
                    LegalContactTelephone = org.LegalContactTelephone
                   ,
                    BillingContactEmailAddress = org.BillingContactEmailAddress,
                    BillingContactFax = org.BillingContactFax,
                    BillingContactFirstName = org.BillingContactFirstName,
                    BillingContactLastName = org.BillingContactLastName,
                    BillingContactTelephone = org.BillingContactTelephone
                    ,
                    RegisteredAddress1 = org.RegisteredAddress1,
                    RegisteredAddress2 = org.RegisteredAddress2,
                    RegisteredAddress3 = org.RegisteredAddress3,
                    RegisteredCity = org.RegisteredCity,
                    RegisteredCountryCode = org.RegisteredCountryCode,
                    RegisteredDepartment = org.RegisteredDepartment,
                    RegisteredPostalCode = org.RegisteredPostalCode,
                    RegisteredStateProvince = org.RegisteredStateProvince
                    ,
                    InvoicingAddress1 = org.InvoicingAddress1,
                    InvoicingAddress2 = org.InvoicingAddress2,
                    InvoicingAddress3 = org.InvoicingAddress3,
                    InvoicingCity = org.InvoicingCity,
                    InvoicingCountryCode = org.InvoicingCountryCode,
                    InvoicingDepartment = org.InvoicingDepartment,
                    InvoicingPostalCode = org.InvoicingPostalCode,
                    InvoicingStateProvince = org.InvoicingStateProvince
                    ,
                    ContractAddress1 = org.ContractAddress1,
                    ContractAddress2 = org.ContractAddress2,
                    ContractAddress3 = org.ContractAddress3,
                    ContractCity = org.ContractCity,
                    ContractCountryCode = org.ContractCountryCode,
                    ContractDepartment = org.ContractDepartment,
                    ContractPostalCode = org.ContractPostalCode
                    ,
                    Sites = listSite.ToList(),
                    JournalEntries = listJournal.ToList(),
                    Status = org.Status,
                    OrgStatusRetired = org.OrgStatusRetired
                };
            }
            return null;
        }

        public SiteMergeDto GetSite(string siteId)
        {
            var site = _mySQLContext.MainSite.FirstOrDefault(s => s.SiteId == siteId && s.Status != AutodeskConst.StatusConts.X && s.SiteStatusRetired != AutodeskConst.StatusConts.A);
            if (site != null)
            {
                var listParnertType = (from s in _mySQLContext.MainSite
                                       join sr in _mySQLContext.SiteRoles on s.SiteId equals sr.SiteId
                                       join r in _mySQLContext.Roles on sr.RoleId equals r.RoleId
                                       where s.SiteId == siteId
                                       select new PartnerType
                                       {
                                           SiteRoleId = sr.SiteRoleId,
                                           CSN = sr.Csn,
                                           Status = sr.Status,
                                           Type = r.RoleName

                                       });
                var listContact = (from c in _mySQLContext.ContactsAll
                                   join scl in _mySQLContext.SiteContactLinks on c.ContactId equals scl.ContactId
                                   join s in _mySQLContext.MainSite on scl.SiteId equals s.SiteId
                                   where s.SiteId == siteId
                                   select new Contact
                                   {
                                       ContactId = c.ContactId,
                                       ContactName = c.ContactName
                                   });
                var listJournal = (from journal in _mySQLContext.Journals
                                   join journalAct in _mySQLContext.JournalActivities on journal.ActivityId equals journalAct.ActivityId
                                   where journal.ParentId == siteId
                                   select new JournalEntry
                                   {
                                       JournalId = journal.JournalId,
                                       ParentId = siteId,
                                       EntryType = journalAct.ActivityName,
                                       NoteValue = journal.Notes
                                   });
                var listAttachment = (from att in _mySQLContext.OrgSiteAttachment
                                          //join s in _mySQLContext.MainSite on att.SiteId equals s.SiteId
                                      where att.SiteId == siteId
                                      select new Attachment
                                      {
                                          Id = att.Id,
                                          Name = att.Name
                                      });

                return new SiteMergeDto
                {

                    SiteId = site.SiteId,

                    OrgId = site.OrgId,

                    //SiteName
                    SiteName = site.SiteName,

                    //EnglishSiteName
                    EnglishSiteName = site.EnglishSiteName,


                    CommercialSiteName = site.CommercialSiteName,

                    SiebelSiteName = site.SiebelSiteName,

                    //SiteWebAddress
                    SiteWebAddress = site.SiteWebAddress,

                    //Workstations
                    Workstations = site.Workstations,


                    MagellanId = site.MagellanId,

                    //SiteTelephone
                    SiteTelephone = site.SiteTelephone,


                    SiteFax = site.SiteFax,

                    //SiteEmailAddress
                    SiteEmailAddress = site.SiteEmailAddress,

                    //SiteAdmin
                    SiteAdminFirstName = site.SiteAdminFirstName,
                    SiteAdminLastName = site.SiteAdminLastName,
                    SiteAdminEmailAddress = site.SiteAdminEmailAddress,
                    SiteAdminTelephone = site.SiteAdminTelephone,
                    SiteAdminFax = site.SiteAdminFax,

                    //SiteManager
                    SiteManagerFirstName = site.SiteManagerFirstName,
                    SiteManagerLastName = site.SiteManagerLastName,
                    SiteManagerEmailAddress = site.SiteManagerEmailAddress,
                    SiteManagerTelephone = site.SiteManagerTelephone,
                    SiteManagerFax = site.SiteManagerFax,

                    //SiteAddress
                    SiteDepartment = site.SiteDepartment,
                    SiteAddress1 = site.SiteAddress1,
                    SiteAddress2 = site.SiteAddress2,
                    SiteAddress3 = site.SiteAddress3,
                    SiteCity = site.SiteCity,
                    SiteStateProvince = site.SiteStateProvince,
                    SiteCountryCode = site.SiteCountryCode,
                    SitePostalCode = site.SitePostalCode,

                    //MailingAddress
                    MailingDepartment = site.MailingDepartment,
                    MailingAddress1 = site.MailingAddress1,
                    MailingAddress2 = site.MailingAddress2,
                    MailingAddress3 = site.MailingAddress3,
                    MailingCity = site.MailingCity,
                    MailingStateProvince = site.MailingStateProvince,
                    MailingCountryCode = site.MailingCountryCode,
                    MailingPostalCode = site.MailingPostalCode,

                    //ShippingAddress
                    ShippingDepartment = site.ShippingDepartment,
                    ShippingAddress1 = site.ShippingAddress1,
                    ShippingAddress2 = site.ShippingAddress2,
                    ShippingAddress3 = site.ShippingAddress3,
                    ShippingCity = site.ShippingCity,
                    ShippingStateProvince = site.ShippingStateProvince,
                    ShippingCountryCode = site.ShippingCountryCode,
                    ShippingPostalCode = site.ShippingPostalCode,

                    //PartnerTypes
                    PartnerTypes = listParnertType.ToList(),

                    //Contacts
                    Contacts = listContact.ToList(),

                    //JournalEntries
                    JournalEntries = listJournal.ToList(),

                    //Attachments
                    Attachments = listAttachment.ToList(),

                    Status = site.Status,
                    SiteStatusRetired = site.SiteStatusRetired
                };

            }
            return null;
        }

        public StudentMergeDto GetStudent(int studentId)
        {
            var student = _mySQLContext.TblStudents.FirstOrDefault(c => c.StudentId == studentId);
            if (student != null)
            {
                var listJournal = (from journal in _mySQLContext.Journals
                                   join journalAct in _mySQLContext.JournalActivities on journal.ActivityId equals journalAct.ActivityId
                                   where journal.ParentId == studentId.ToString()
                                   select new JournalEntry
                                   {
                                       JournalId = journal.JournalId,
                                       ParentId = studentId.ToString(),
                                       EntryType = journalAct.ActivityName,
                                       NoteValue = journal.Notes
                                   }
                                 );
                return new StudentMergeDto
                {
                    
                    Firstname = student.Firstname,
                    Lastname = student.Lastname,
                    Salutation = student.Salutation,
                    Email = student.Email,
                    Email2 = student.Email2,
                    Password = student.Password,
                    Password_plain = student.PasswordPlain,
                    PWDChangeRequired = student.PwdchangeRequired,
                    
                    PrimaryIndustryId = student.PrimaryIndustryId,
                    Phone = student.Phone,
                    Mobile = student.Mobile,
                    PrimaryLanguage = student.PrimaryLanguage,
                    SecondaryLanguage = student.SecondaryLanguage,
                    
                    LoginCount = student.LoginCount,
                    LastLogin = student.LastLogin,
                    LastIPAddress = student.LastIpaddress,
                    Status = student.Status,
                    DateAdded = student.DateAdded,
                   
                    StatusLevel = student.StatusLevel,
                   
                    Address = student.Address,
                    Address2 = student.Address2,
                    
                    City = student.City,
                    PostalCode = student.PostalCode,
                    CountryID = student.CountryId,
                    StateProvince = student.StateProvince,
                    Gender = student.Gender,
                    MobileCode = student.MobileCode,
                    TelephoneCode = student.TelephoneCode,
                    ResetPasswordCode = student.ResetPasswordCode,
                    
                    DateBirth = student.DateBirth,
                    Company= student.Company,
                    CreatedDate=student.CreatedDate,
                    DateRequestTerminated=student.DateRequestTerminated,
                    ExpetationGradDate= student.ExpetationGradDate,
                    ExternalID=student.ExternalId,
                    LanguageID=student.LanguageId,
                    ResetPasswordDate=student.ResetPasswordDate,
                    StateID=student.StateId,
                    StudentID=student.StudentId,
                    TermAndCondition= student.TermAndCondition,
                    JournalEntries= listJournal.ToList()
                    
                };


            }
            return null;
        }

        public bool MarkContactDuplicate(string contactId)
        {
            var contact = _mySQLContext.ContactsAll.FirstOrDefault(c => c.ContactId == contactId);
            if (contact != null)
            {
                contact.Status = AutodeskConst.StatusConts.X;
                _mySQLContext.ContactsAll.Update(contact);
                _mySQLContext.SaveChanges();
                return true;
            }
            return false;
        }

        public bool MarkOrgDuplicate(string orgId)
        {
            var org = _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == orgId);
            if (org != null)
            {
                org.OrgStatusRetired = AutodeskConst.StatusConts.A;
                org.Status = AutodeskConst.StatusConts.X;
                _mySQLContext.MainOrganization.Update(org);
                _mySQLContext.SaveChanges();
                return true;
            }
            return false;
        }

        public bool MarkSiteDuplicate(string siteId)
        {
            var site = _mySQLContext.MainSite.FirstOrDefault(o => o.SiteId == siteId);
            if (site != null)
            {
                site.SiteStatusRetired = AutodeskConst.StatusConts.A;
                site.Status = AutodeskConst.StatusConts.X;
                _mySQLContext.MainSite.Update(site);
                _mySQLContext.SaveChanges();
                return true;
            }
            return false;
        }

        public bool MarkStudentDuplicate(int studentId)
        {
            var student = _mySQLContext.TblStudents.FirstOrDefault(c => c.StudentId == studentId);
            if (student != null)
            {
                student.Status = AutodeskConst.StatusConts.X;
                _mySQLContext.TblStudents.Update(student);
                _mySQLContext.SaveChanges();
                return true;
            }
            return false;
        }

        public bool MergeContact(ContactMergeDto contactMergeDto, string newContactId)
        {
            try
            {
                var newContact = _mySQLContext.ContactsAll.FirstOrDefault(o => o.ContactId == newContactId);
                //var retiredContact = _mySQLContext.ContactsAll.FirstOrDefault(o => o.ContactId == contactMergeDto.ContactId);
                if (newContact != null && newContact.ContactId != contactMergeDto.ContactId)
                {
                    var orgType = newContact.GetType();
                    var orgMergeType = contactMergeDto.GetType();
                    List<PropertyInfo> newOrgProps = new List<PropertyInfo>(orgType.GetProperties());
                    List<PropertyInfo> mergeProps = new List<PropertyInfo>(orgMergeType.GetProperties());
                    mergeProps = mergeProps.Where(x => x.GetValue(contactMergeDto, null) != null && (x.GetCustomAttribute(typeof(ForDisplayAttribute)) as ForDisplayAttribute) == null).ToList();
                    foreach (var mergeItem in mergeProps)
                    {
                        var val = mergeItem.GetValue(contactMergeDto, null);
                        if (val != null)
                        {
                            if (mergeItem.Name == "AdditionSites")
                            {
                                if (contactMergeDto.AdditionSites?.Count > 0)
                                {
                                    var siteIds = contactMergeDto.AdditionSites.Select(s => s.SiteId).ToList();
                                    var listSite = _mySQLContext.SiteContactLinks.Where(x => siteIds.Contains(x.SiteId) && x.ContactId == contactMergeDto.ContactId).ToList();
                                    _mySQLContext.SiteContactLinks.RemoveRange(listSite);
                                    _mySQLContext.SaveChanges();

                                    foreach (var item in siteIds)
                                    {
                                        var exist = _mySQLContext.SiteContactLinks.FirstOrDefault(x => x.SiteId == item && x.ContactId == newContactId);
                                        if (exist==null)
                                        {
                                            var newSiteLink = new SiteContactLinks
                                            {
                                                ContactId = newContactId,
                                                SiteId = item,
                                                Status = AutodeskConst.StatusConts.A,
                                                AddedBy = "SYSTEM",
                                                DateAdded = DateTime.Now,

                                            };
                                            _mySQLContext.SiteContactLinks.Add(newSiteLink);
                                        }

                                    }
                                    _mySQLContext.SaveChanges();

                                }


                            }
                            else if (mergeItem.Name == "JournalEntries")
                            {
                                if (contactMergeDto.JournalEntries?.Count > 0)
                                {
                                    var journalIds = contactMergeDto.JournalEntries.Select(s => s.JournalId).ToList();
                                    var listJournal = _mySQLContext.Journals.Where(x => journalIds.Contains(x.JournalId)).ToList();
                                    listJournal.ForEach(x => x.ParentId = newContactId);
                                    _mySQLContext.Journals.UpdateRange(listJournal);
                                    _mySQLContext.SaveChanges();
                                }

                            }
                            else
                            {
                                foreach (var newItem in newOrgProps)
                                {
                                    if (mergeItem.Name == newItem.Name)
                                    {
                                        newItem.SetValue(newContact, val);
                                        break;
                                    }

                                }
                            }

                        }
                    }

                    _mySQLContext.ContactsAll.Update(newContact);
                    _mySQLContext.SaveChanges();

                    return true;

                }
                return false;
            }
            catch (System.Exception ex)
            {

                return false;
            }
        }

        public bool MergeOrg(OrgMergeDto orgMergeDto, string newOrgId)
        {
            try
            {
                var newOrg = _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == newOrgId);
                var retiredOrg = _mySQLContext.MainOrganization.FirstOrDefault(o => o.OrgId == orgMergeDto.OrgId);
                if (newOrg != null && newOrg.OrgId != orgMergeDto.OrgId)
                {
                    var orgType = newOrg.GetType();
                    var orgMergeType = orgMergeDto.GetType();
                    List<PropertyInfo> newOrgProps = new List<PropertyInfo>(orgType.GetProperties());
                    List<PropertyInfo> mergeProps = new List<PropertyInfo>(orgMergeType.GetProperties());
                    mergeProps = mergeProps.Where(x => x.GetValue(orgMergeDto, null) != null && (x.GetCustomAttribute(typeof(ForDisplayAttribute)) as ForDisplayAttribute) == null).ToList();
                    foreach (var mergeItem in mergeProps)
                    {
                        var val = mergeItem.GetValue(orgMergeDto, null);
                        if (val != null)
                        {
                            if (mergeItem.Name == "Sites")
                            {
                                if (orgMergeDto.Sites?.Count() > 0)
                                {
                                    var siteIds = orgMergeDto.Sites.Select(s => s.SiteId).ToList();
                                    var listSite = _mySQLContext.MainSite.Where(x => siteIds.Contains(x.SiteId)).ToList();
                                    listSite.ForEach(x => x.OrgId = newOrgId);
                                    _mySQLContext.MainSite.UpdateRange(listSite);
                                    _mySQLContext.SaveChanges();
                                }


                            }
                            else if (mergeItem.Name == "JournalEntries")
                            {
                                if (orgMergeDto.JournalEntries?.Count > 0)
                                {
                                    var journalIds = orgMergeDto.JournalEntries.Select(s => s.JournalId).ToList();
                                    var listJournal = _mySQLContext.Journals.Where(x => journalIds.Contains(x.JournalId)).ToList();
                                    listJournal.ForEach(x => x.ParentId = newOrgId);
                                    _mySQLContext.Journals.UpdateRange(listJournal);
                                    _mySQLContext.SaveChanges();
                                }

                            }
                            else
                            {
                                foreach (var newItem in newOrgProps)
                                {
                                    if (mergeItem.Name == newItem.Name)
                                    {
                                        newItem.SetValue(newOrg, val);
                                        break;
                                    }

                                }
                            }

                        }
                    }

                    _mySQLContext.MainOrganization.Update(newOrg);
                    _mySQLContext.SaveChanges();

                    return true;

                }
                return false;
            }
            catch (System.Exception ex)
            {

                return false;
            }

        }

        public bool MergeSite(SiteMergeDto siteMergeDto, string newSiteId)
        {
            try
            {
                var newSite = _mySQLContext.MainSite.FirstOrDefault(s => s.SiteId == newSiteId);
                //var retiredOrg = _mySQLContext.MainSite.FirstOrDefault(s => s.SiteId == siteMergeDto.SiteId);
                if (newSite != null && newSite.SiteId != siteMergeDto.SiteId)
                {
                    var siteType = newSite.GetType();
                    var siteMergeType = siteMergeDto.GetType();
                    List<PropertyInfo> newSiteProps = new List<PropertyInfo>(siteType.GetProperties());
                    List<PropertyInfo> mergeProps = new List<PropertyInfo>(siteMergeType.GetProperties());
                    mergeProps = mergeProps.Where(x => x.GetValue(siteMergeDto, null) != null && (x.GetCustomAttribute(typeof(ForDisplayAttribute)) as ForDisplayAttribute) == null).ToList();
                    foreach (var mergeItem in mergeProps)
                    {
                        var val = mergeItem.GetValue(siteMergeDto, null);
                        if (val != null)
                        {
                            if (mergeItem.Name == "PartnerTypes" && siteMergeDto.PartnerTypes?.Count > 0)
                            {
                                var siteRoleIds = siteMergeDto.PartnerTypes.Select(s => s.SiteRoleId).ToList();
                                var listSiteRole = _mySQLContext.SiteRoles.Where(x => siteRoleIds.Contains(x.SiteRoleId)).ToList();
                                listSiteRole.ForEach(x => x.SiteId = newSiteId);
                                _mySQLContext.SiteRoles.UpdateRange(listSiteRole);
                                _mySQLContext.SaveChanges();

                            }
                            else if (mergeItem.Name == "Contacts")
                            {
                                if (siteMergeDto.Contacts?.Count > 0)
                                {
                                    var contactIds = siteMergeDto.Contacts.Select(s => s.ContactId).ToList();
                                    var listContact = _mySQLContext.SiteContactLinks.Where(x => contactIds.Contains(x.ContactId)).ToList();
                                    listContact.ForEach(x => x.SiteId = newSiteId);
                                    _mySQLContext.SiteContactLinks.UpdateRange(listContact);
                                    _mySQLContext.SaveChanges();
                                }

                            }

                            else if (mergeItem.Name == "JournalEntries")
                            {
                                if (siteMergeDto.JournalEntries?.Count > 0)
                                {
                                    var journalIds = siteMergeDto.JournalEntries.Select(s => s.JournalId).ToList();
                                    var listJournal = _mySQLContext.Journals.Where(x => journalIds.Contains(x.JournalId)).ToList();
                                    listJournal.ForEach(x => x.ParentId = newSiteId);
                                    _mySQLContext.Journals.UpdateRange(listJournal);
                                    _mySQLContext.SaveChanges();
                                }

                            }
                            else if (mergeItem.Name == "Attachments")
                            {
                                if (siteMergeDto.Attachments?.Count > 0)
                                {
                                    var attachmentIds = siteMergeDto.Attachments.Select(s => s.Id).ToList();
                                    var listAttachment = _mySQLContext.OrgSiteAttachment.Where(x => attachmentIds.Contains(x.Id)).ToList();
                                    listAttachment.ForEach(x => x.SiteId = newSiteId);
                                    _mySQLContext.OrgSiteAttachment.UpdateRange(listAttachment);
                                    _mySQLContext.SaveChanges();
                                }

                            }
                            else
                            {
                                foreach (var newItem in newSiteProps)
                                {
                                    if (mergeItem.Name == newItem.Name)
                                    {
                                        newItem.SetValue(newSite, val);
                                        break;
                                    }

                                }
                            }

                        }
                    }

                    _mySQLContext.MainSite.Update(newSite);
                    _mySQLContext.SaveChanges();

                    return true;

                }
                return false;
            }
            catch (System.Exception ex)
            {

                return false;
            }
        }

        public bool MergeStudent(StudentMergeDto studentMergeDto, int newStudentId)
        {
            try
            {
                var newStudent = _mySQLContext.TblStudents.FirstOrDefault(o => o.StudentId == newStudentId);
                //var retiredContact = _mySQLContext.ContactsAll.FirstOrDefault(o => o.ContactId == studentMergeDto.ContactId);
                if (newStudent != null && newStudent.StudentId != studentMergeDto.StudentID)
                {
                    var studType = newStudent.GetType();
                    var studMergeType = studentMergeDto.GetType();
                    List<PropertyInfo> newStudProps = new List<PropertyInfo>(studType.GetProperties());
                    List<PropertyInfo> mergeProps = new List<PropertyInfo>(studMergeType.GetProperties());
                    mergeProps = mergeProps.Where(x => x.GetValue(studentMergeDto, null) != null && (x.GetCustomAttribute(typeof(ForDisplayAttribute)) as ForDisplayAttribute) == null).ToList();
                    foreach (var mergeItem in mergeProps)
                    {
                        var val = mergeItem.GetValue(studentMergeDto, null);
                        if (val != null)
                        {
                             if (mergeItem.Name == "JournalEntries")
                            {
                                if (studentMergeDto.JournalEntries?.Count > 0)
                                {
                                    var journalIds = studentMergeDto.JournalEntries.Select(s => s.JournalId).ToList();
                                    var listJournal = _mySQLContext.Journals.Where(x => journalIds.Contains(x.JournalId)).ToList();
                                    listJournal.ForEach(x => x.ParentId = newStudentId.ToString());
                                    _mySQLContext.Journals.UpdateRange(listJournal);
                                    _mySQLContext.SaveChanges();
                                }

                            }
                            else
                            {
                                foreach (var newItem in newStudProps)
                                {
                                    if (mergeItem.Name == newItem.Name)
                                    {
                                        newItem.SetValue(newStudent, val);
                                        break;
                                    }

                                }
                            }

                        }
                    }

                    _mySQLContext.TblStudents.Update(newStudent);
                    _mySQLContext.SaveChanges();

                    return true;

                }
                return false;
            }
            catch (System.Exception ex)
            {

                return false;
            }
        }
    }
}
