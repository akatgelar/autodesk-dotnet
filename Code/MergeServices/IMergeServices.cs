﻿using autodesk.Code.MergeServices.Dto;

namespace autodesk.Code.MergeServices
{
    public interface IMergeServices
    {
        OrgMergeDto GetOrg(string orgId);
        bool MergeOrg(OrgMergeDto orgMergeDto, string newOrgId);
        bool MarkOrgDuplicate(string orgId);

        SiteMergeDto GetSite(string siteId);
        bool MergeSite(SiteMergeDto siteMergeDto, string newSiteId);
        bool MarkSiteDuplicate(string siteId);

        ContactMergeDto GetContact(string userId);
        bool MergeContact(ContactMergeDto contactMergeDto,string newUserId);
        bool MarkContactDuplicate(string userId);

        StudentMergeDto GetStudent(int studentId);
        bool MergeStudent(StudentMergeDto studentMergeDto, int newStudentId);
        bool MarkStudentDuplicate(int studentId);
    }
}
