﻿using autodesk.Code.EmailTrackingServices;
using autodesk.Code.EmailTrackingServices.Dto;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace autodesk.Code
{
    public class EmailService : IEmailService
    {

        private readonly IQueueServices _queueServices;
        private readonly IEmailTrackingServices _emailTrackingServices;
        public EmailService(IQueueServices queueServices, IEmailTrackingServices emailTrackingServices)
        {
            _queueServices = queueServices;
            _emailTrackingServices = emailTrackingServices;
        }
        /// <summary>
        /// Send mail async
        /// </summary>
        /// <param name="email">list receive email split by comma</param>
        /// <param name="subject">subject of email</param>
        /// <param name="message">content</param>
        /// <returns>true if success</returns>
        public async Task<bool> SendMailAsync(string email, string subject, string message, string cc = null, List<SendGrid.Helpers.Mail.Attachment> attachments = null)
        {
            try
            {
                //MailMessage mailMessage;
                //SmtpClient smtpClient;
                //SetupEmail(email, subject, message, out mailMessage, out smtpClient);

                //smtpClient.SendCompleted += new SendCompletedEventHandler(SmtpClient_SendCompleted);

                //await Task.Run(() => smtpClient.SendAsync(mailMessage, subject + " to " + email));

                var emailTrack = new EmailTrackingDto
                {
                    SendTo = email,
                    CC = cc,
                    Subject = subject,
                    Content = message,
                    SendTime = DateTime.Now,
                    Attachments = attachments
                };



                await Task.Run(() => _emailTrackingServices.UseSendgrid(emailTrack));

                return true;

            }
            catch (Exception ex)
            {
                return false;

            }
        }

        private static void SetupEmail(string email, string subject, string message, out MailMessage mailMessage, out SmtpClient smtpClient)
        {
            //From Address    
            string FromAddress = AutodeskConst.EmailInfo.DefaultEmail;
            string FromAdressTitle = AutodeskConst.EmailInfo.DefaultTitle;

            //Smtp Server    
            string SmtpServer = AutodeskConst.EmailInfo.DefaultSmtpServer;

            //Smtp Port Number
            int SmtpPortNumber = Int32.Parse(AutodeskConst.EmailInfo.DefaultPort);
            string UserId = AutodeskConst.EmailInfo.DefaultEmail;
            string UserPassword = AutodeskConst.EmailInfo.DefaultPassword;

            mailMessage = new MailMessage();
            smtpClient = new SmtpClient();
            MailAddress fromAddress = new MailAddress(FromAddress, FromAdressTitle, System.Text.Encoding.UTF8);
            mailMessage.From = fromAddress;
            mailMessage.To.Add(email);

            mailMessage.Subject = subject;
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = message;

            smtpClient.Host = SmtpServer;
            smtpClient.Port = SmtpPortNumber;
            smtpClient.EnableSsl = true;
            smtpClient.UseDefaultCredentials = true;
            smtpClient.Credentials = new System.Net.NetworkCredential(UserId, UserPassword);
            smtpClient.Timeout = 5000;
        }

        static bool mailSent = false;
        private void SmtpClient_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            string token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
                mailSent = false;
            }
            else
            {
                Console.WriteLine("Message sent.");
                mailSent = true;
            }

            var client = sender as SmtpClient;
            client.Dispose();
        }

        public bool SendMail(string email, string subject, string message, string cc = null, List<SendGrid.Helpers.Mail.Attachment> attachments = null)
        {
            try
            {
                MailMessage mailMessage;
                SmtpClient smtpClient;
                SetupEmail(email, subject, message, out mailMessage, out smtpClient);
                smtpClient.Send(mailMessage);
                smtpClient.Dispose();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }
    }
}
