﻿using autodesk.Code.EmailTrackingServices.Dto;
using autodesk.Code.Models;
using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace autodesk.Code.EmailTrackingServices
{
    public class EmailTrackingServices : IEmailTrackingServices
    {

        private readonly IQueueServices _queueServices;
        public EmailTrackingServices(IQueueServices queueServices)
        {
            _queueServices = queueServices;
        }
        private int CreateTrack(MySQLContext _mySQLContext, EmailTrackingDto emailTrackingDto)
        {
            var emailTrack = new EmailTracking
            {
                SendTo = emailTrackingDto.SendTo,
                CC = emailTrackingDto.CC,
                Subject = emailTrackingDto.Subject,
                Content = emailTrackingDto.Content,
                Status = AutodeskConst.StatusConts.I,
                SendTime = emailTrackingDto.SendTime
            };
            _mySQLContext.EmailTrackings.Add(emailTrack);
            _mySQLContext.SaveChanges();

            return emailTrack.Id;
        }

        private void UpdateTrack(MySQLContext _mySQLContext, EmailTrackingUpdateDto emailTrackingUpdateDto)
        {
            var emailTrack = _mySQLContext.EmailTrackings.FirstOrDefault(x => x.Id == emailTrackingUpdateDto.Id);
            if (emailTrack != null)
            {
                emailTrack.Result = emailTrackingUpdateDto.Result;
                emailTrack.Status = AutodeskConst.StatusConts.A;
                emailTrack.SendgridResponseTime = emailTrackingUpdateDto.SendgridResponseTime;

                _mySQLContext.EmailTrackings.Update(emailTrack);
                _mySQLContext.SaveChanges();
            }
        }

        public void UseSendgrid(EmailTrackingDto emailTrackingDto)
        {

            _queueServices.Enqueue(() => Send(emailTrackingDto).Wait());
            _queueServices.Process();

        }
        private async Task Send(EmailTrackingDto emailTrackingDto)
        {
            try
            {
                using (var context = new MySQLContext())
                {
                    var id = CreateTrack(context, emailTrackingDto);
                    var apiKey = AppConfig.Config["SendGridApiKey"];
                    var client = new SendGridClient(apiKey);
                    var from = new EmailAddress(AppConfig.Config["EmailSender:Email"], "Autodesk");

                    var listTo = emailTrackingDto.SendTo.Contains(",") ? emailTrackingDto.SendTo.Split(",") : null;
                    var listEmailAddress = new List<EmailAddress>();
                    EmailAddress sendTo = new EmailAddress();

                    var listcc = !string.IsNullOrEmpty(emailTrackingDto.CC) && emailTrackingDto.CC.Contains(",") ? emailTrackingDto.CC.Split(",") : null;
                    var listCCEmailAddress = new List<EmailAddress>();

                    var mailMessage = new SendGridMessage();
                    if (listTo?.Count() > 0)
                    {
                        foreach (var item in listTo)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {


                                var emailAdd = new EmailAddress(item);
                                listEmailAddress.Add(emailAdd);
                            }
                        }
                        mailMessage = MailHelper.CreateSingleEmailToMultipleRecipients(from, listEmailAddress, emailTrackingDto.Subject, null, emailTrackingDto.Content);
                        if (listcc?.Count() > 0)
                        {
                            foreach (var item in listcc)
                            {
                                if (!string.IsNullOrEmpty(item))
                                {
                                    var emailAdd = new EmailAddress(item);
                                    listCCEmailAddress.Add(emailAdd);
                                }
                            }
                            mailMessage.AddCcs(listCCEmailAddress);
                        }


                    }
                    else
                    {
                        sendTo = new EmailAddress(emailTrackingDto.SendTo);
                        mailMessage = MailHelper.CreateSingleEmail(from, sendTo, emailTrackingDto.Subject, null, emailTrackingDto.Content);
                        if (!string.IsNullOrEmpty(emailTrackingDto.CC))
                        {
                            mailMessage.AddCc(emailTrackingDto.CC);
                        }

                    }
                    mailMessage.Attachments = emailTrackingDto.Attachments;
                    var response = await client.SendEmailAsync(mailMessage);
                    if (response != null)
                    {
                        var data = response.DeserializeResponseHeaders(response.Headers);
                        if (data != null)
                        {
                            var body = await response.DeserializeResponseBodyAsync(response.Body);
                            var date = data.FirstOrDefault(x => x.Key == "Date");

                            DateTime convertedDate = !string.IsNullOrEmpty(date.Value) ? DateTime.SpecifyKind(DateTime.Parse(date.Value), DateTimeKind.Utc) : DateTime.UtcNow;
                            var track = await TrackEmail(sendTo.Email);
                            var reponseTime = convertedDate.ToLocalTime();
                            var updateTrack = new EmailTrackingUpdateDto
                            {
                                Id = id,
                                Result = track != null ? GetTrackEmailResponseStatus(track.Status) : GetTrackEmailResponseStatus(response.StatusCode.ToString()),
                                SendgridResponseTime = convertedDate
                            };
                            UpdateTrack(context, updateTrack);
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }


        }
        private string GetTrackEmailResponseStatus(string status)
        {
            switch (status)
            {
                case AutodeskConst.StatusConts.B:
                    return "Bounce";
                case AutodeskConst.StatusConts.L:
                    return "Block";
                case AutodeskConst.StatusConts.V:
                    return "Invalid";
                default:
                    return "Accepted";
            }
        }

        public async Task<TrackEmailResponseDto> TrackEmail(string email)
        {
            using (var httpClient = new HttpClient())
            {
                var userName = AppConfig.Config["SendGridUsername"];
                var password = AppConfig.Config["SendGridPassword"];


                string url = $"https://api.sendgrid.com/api/bounces.get.json?api_user={userName}&api_key={password}&email={email}&date=1";
                var response = await httpClient.GetStringAsync(url);

                var dto = response != "[]" ? JsonConvert.DeserializeObject<List<TrackEmailResponseDto>>(response) : null;
                //check bounce
                if (dto?.Count>0)
                {
                    var result= dto.First();
                    result.Status = AutodeskConst.StatusConts.B;
                    return result;


                }
                url = $"https://api.sendgrid.com/api/blocks.get.json?api_user={userName}&api_key={password}&email={email}&date=1";
                response = await httpClient.GetStringAsync(url);
                dto = response != "[]" ? JsonConvert.DeserializeObject<List<TrackEmailResponseDto>>(response) : null;
                if (dto?.Count > 0)
                {
                    var result = dto.First();
                    result.Status = AutodeskConst.StatusConts.L;
                    return result;

                }
                url = $"https://api.sendgrid.com/api/invalidemails.get.json?api_user={userName}&api_key={password}&email={email}&date=1";
                response = await httpClient.GetStringAsync(url);
                dto = response != "[]" ? JsonConvert.DeserializeObject<List<TrackEmailResponseDto>>(response) : null;
                if (dto?.Count > 0)
                {
                    var result = dto.First();
                    result.Status = AutodeskConst.StatusConts.V;
                    return result; 

                }
            }
            return null;

        }
    }
}
