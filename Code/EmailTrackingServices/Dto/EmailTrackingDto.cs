﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.EmailTrackingServices.Dto
{
    public class EmailTrackingDto
    {
      
        public string SendTo { get; set; }
        public string CC { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
        public string Status { get; set; }
        public DateTime? SendTime { get; set; }
        public List<SendGrid.Helpers.Mail.Attachment> Attachments { get; set; }

    }
    public class EmailTrackingUpdateDto
    {
        public int Id { get; set; }
        public string Result { get; set; }
       
        public DateTime? SendgridResponseTime { get; set; }
    }
}
