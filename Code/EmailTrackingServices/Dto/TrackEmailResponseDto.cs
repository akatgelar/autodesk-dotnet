﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.EmailTrackingServices.Dto
{
    public class TrackEmailResponseDto
    {
        public string Status { get; set; }
        public string Reason { get; set; }
        public string Email { get; set; }
        public DateTime Created { get; set; }
    }
}
