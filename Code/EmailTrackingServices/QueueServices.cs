﻿using System;
using System.Collections.Generic;

namespace autodesk.Code.EmailTrackingServices
{
    public class QueueServices:IQueueServices
    {

        private System.Threading.Thread actionThread;
        private bool isProcessed = false;
        private object queueSync = new object();
        private Queue<Action> actionQueue = new Queue<Action>();
        private System.Threading.SynchronizationContext context;


        /// <summary>
        /// Occurs when one of executed action throws unhandled exception.
        /// </summary>
        public event CrossThreadExceptionEventHandler ExceptionOccured;

        /// <summary>
        /// Occurs when all actions in queue are finished.
        /// </summary>
        public event EventHandler ProcessingFinished;

        protected virtual void Execute()
        {
            isProcessed = true;

            try
            {
                while (true)
                {
                    Action action = null;

                    lock (queueSync)
                    {
                        if (actionQueue.Count == 0)
                        {
                            break;
                        }
                        else
                        {
                            action = actionQueue.Dequeue();
                        }
                    }

                    action.Invoke();
                }

                if (ProcessingFinished != null)
                {
                    context.Send(s => ProcessingFinished(this, EventArgs.Empty), null);
                }
            }
            catch (System.Threading.ThreadAbortException ex)
            {

            }
            catch (Exception ex)
            {
                if (ExceptionOccured != null)
                {
                    context.Send(s => ExceptionOccured(this, new CrossThreadExceptionEventArgs(ex)), null);
                }
            }
            finally
            {
                isProcessed = false;
            }
        }

        /// <summary>
        /// Starts processing current queue.
        /// </summary>
        /// <returns>Returns true if execution was started.</returns>
        public virtual bool Process()
        {
            if (!isProcessed)
            {
                context = System.Threading.SynchronizationContext.Current;

                actionThread = new System.Threading.Thread(Execute);
                actionThread.Start();

                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Enqueues action to process.
        /// </summary>
        /// <param name="action">Action to enqueue.</param>
        public void Enqueue(Action action)
        {
            lock (queueSync)
            {
                actionQueue.Enqueue(action);
            }
        }


        public delegate void CrossThreadExceptionEventHandler(object sender, CrossThreadExceptionEventArgs e);

        public class CrossThreadExceptionEventArgs : EventArgs
        {
            public CrossThreadExceptionEventArgs(Exception exception)
            {
                this.Exception = exception;
            }

            public Exception Exception
            {
                get;
                set;
            }
        }
    }
}
