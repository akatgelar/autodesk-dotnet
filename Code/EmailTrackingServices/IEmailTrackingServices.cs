﻿using autodesk.Code.EmailTrackingServices.Dto;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.EmailTrackingServices
{
    public interface IEmailTrackingServices
    {
        //void CreateTrack(EmailTrackingDto emailTrackingDto);
        void UseSendgrid(EmailTrackingDto emailTrackingDto);
        //void UpdateTrack(EmailTrackingUpdateDto emailTrackingUpdateDto);
        Task<TrackEmailResponseDto> TrackEmail(string email);
    }
}
