﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace autodesk.Code.EmailTrackingServices
{
    public interface IQueueServices
    {
        void Enqueue(Action action);
        bool Process();

    }
}
