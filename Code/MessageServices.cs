﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;

namespace autodesk.Code
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link https://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender
    {
        public static readonly string DEFAULT_EMAIL_SENDER = AppConfig.Config["EmailSender:Email"];
        public static readonly string DEFAULT_PASSWORD_SENDER = AppConfig.Config["EmailSender:Password"];
        public static readonly string DEFAULT_TITLE_SENDER = AppConfig.Config["EmailSender:Title"];
        public static readonly string DEFAULT_SMTPSERVER_SENDER = AppConfig.Config["EmailSender:SmtpServer"];
        public static readonly string DEFAULT_PORT_SENDER = AppConfig.Config["EmailSender:Port"];
        
        public async Task<Boolean> SendEmailAsync(string email, string subject, string message)
        {
            try
            {
               
                //From Address    
                string FromAddress = DEFAULT_EMAIL_SENDER;  
                string FromAdressTitle = DEFAULT_TITLE_SENDER;  

                //To Address    
                string[] ToAddress = email.Split(",");  
                string[] ToAdressTitle = email.Split(",");  
                string Subject = subject;  
                string BodyContent = message;  

                //Smtp Server    
                string SmtpServer = DEFAULT_SMTPSERVER_SENDER; 

                //Smtp Port Number
                int SmtpPortNumber = Int32.Parse(DEFAULT_PORT_SENDER);
                string UserId = DEFAULT_EMAIL_SENDER;  
                string UserPassword = DEFAULT_PASSWORD_SENDER;  

                var mimeMessage = new MimeMessage();
                mimeMessage.From.Add(new MailboxAddress
                                        (FromAdressTitle,
                                         FromAddress
                                         ));
                
                for(int i=0; i<ToAddress.Length; i++){
                    if(i == 0){
                        mimeMessage.To.Add(new MailboxAddress
                                        (ToAdressTitle[i],
                                         ToAddress[i]
                                         ));
                    }else{
                        mimeMessage.Cc.Add(new MailboxAddress
                                        (ToAdressTitle[i],
                                         ToAddress[i]
                                         ));
                    }
                }

                mimeMessage.Subject = Subject; //Subject
                mimeMessage.Body = new TextPart("html")
                {
                    Text = BodyContent
                };

    
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s,c,h,e) => true;
					
                    client.Connect(SmtpServer, SmtpPortNumber, false);
                    client.Authenticate(UserId, UserPassword);
                    // client.Timeout = 5000;
                    await client.SendAsync(mimeMessage);
                    await client.DisconnectAsync(true);
                }

                return true;
 
            }
            catch (Exception ex)
            {
                // if(ex is FormatException || ex is OverflowException){
                    return false;
                // }
                // throw ex;
            }
        }

    }
}
