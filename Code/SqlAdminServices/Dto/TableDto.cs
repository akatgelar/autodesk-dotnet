﻿using System.Collections.Generic;

namespace autodesk.Code.SqlAdminServices.Dto
{
    public class TableDto
    {
        public string Name { get; set; }
        public IEnumerable<ColumnDto> Columns { get; set; }
    }

    public class ColumnDto
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
