﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.SqlAdminServices.Dto
{
    public class ExecutedQueryResultDto
    {
        public object Result { get; set; }
        public string Status { get; set; }
        
    }
}
