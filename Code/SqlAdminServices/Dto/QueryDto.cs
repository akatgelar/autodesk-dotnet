﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace autodesk.Code.SqlAdminServices.Dto
{
    public class QueryDto
    {
        public string Query { get; set; }
        public string EmailAddress { get; set; }
        public int SercurityCode { get; set; }
    }
    public class QueryResultData
    {
        public object Result { get; set; }
    }
}
