﻿using autodesk.Code.AuditLogServices;
using autodesk.Code.CommonServices;
using autodesk.Code.Models;
using autodesk.Code.SqlAdminServices.Dto;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static autodesk.Code.Utilities;

namespace autodesk.Code.SqlAdminServices
{
    public class SqlAdminServices : ISqlAdminServices
    {
        private readonly MySQLContext _mySQLContext;
        private readonly ICommonServices _commonServices;
        private readonly IEmailService _emailService;
        private readonly IAuditLogServices _auditLogServices;
        public SqlAdminServices(MySQLContext mySQLContext, ICommonServices commonServices, IEmailService emailService, IAuditLogServices auditLogServices)
        {
            _mySQLContext = mySQLContext;
            _commonServices = commonServices;
            _emailService = emailService;
            _auditLogServices = auditLogServices;
        }
        public ExecutedQueryResultDto ExecuteQuery(QueryDto queryDto)
        {
            var sa = _mySQLContext.ContactsAll.FirstOrDefault(u => u.EmailAddress == queryDto.EmailAddress);
            if (sa != null && sa.UserLevelId == AutodeskConst.UserLevelIdEnum.SUPERADMIN)
            {
                var securityToken = _commonServices.EncrypttoMd5(string.Join('-', sa.EmailAddress, sa.Password));
                var validateCode = ValidateOTP(new TokenValidateDto
                {
                    SercurityToken = securityToken,
                    Code = queryDto.SercurityCode
                });
                if (validateCode)
                {
                    var selectPatern = @"((select)(\s.*)|\*(from)\s((.+?)(where)+?)\s([^;]*)[;])";
                    var insertPartern = @"((insert into )([^ ]+) (\()([`\w+]+,)+([`\w+]+)(\)) (values) (\()((.+?)',)+((.+?)+')(\))[;])";

                    Match selectMatch;
                    Match insertMatch;
                    selectMatch = Regex.Match(queryDto.Query, selectPatern, RegexOptions.IgnoreCase);
                    insertMatch = Regex.Match(queryDto.Query, insertPartern, RegexOptions.IgnoreCase);
                    var supervisor = AppConfig.Config["EmailSender:SupervisorEmail"];
                    var queryTemplate = AppConfig.Config["EmailSender:SqlAdminQueryEmailTemplate"];
                    try
                    {
                        if (insertMatch.Success)//insert
                        {
                            var first = insertMatch.Groups[0];
                            _auditLogServices.LogHistory(new History
                            {
                                Id = sa.ContactId,
                                Admin = sa.ContactName,
                                Date = DateTime.Now,
                                Description = "Excute query in SqlAdmin:<br/>" + first.Value
                            });
                            _emailService.SendMail(supervisor, "Query Execute in SqlAdmin", string.Format(queryTemplate, supervisor, sa.ContactName, first.Value));
                            _mySQLContext.Database.ExecuteSqlCommand(first.Value);
                            return new ExecutedQueryResultDto { Status = AutodeskConst.ResponseMessage.Success };
                        }
                        if (selectMatch.Success)//select
                        {
                            using (var command = _mySQLContext.Database.GetDbConnection().CreateCommand())
                            {
                                if (selectMatch.Success && selectMatch.Groups.Count > 0)
                                {
                                    command.CommandText = selectMatch.Groups[0].Value;
                                }
                                _mySQLContext.Database.OpenConnection();
                                _auditLogServices.LogHistory(new History
                                {
                                    Id = sa.ContactId,
                                    Admin = sa.ContactName,
                                    Date = DateTime.Now,
                                    Description = "Excute query in SqlAdmin:<br/>" + command.CommandText
                                });
                                _emailService.SendMail(supervisor, "Query Execute in SqlAdmin", string.Format(queryTemplate, supervisor, sa.ContactName, command.CommandText));
                                using (var reader = command.ExecuteReader())
                                {
                                    var result = reader.ToJson();
                                    return new ExecutedQueryResultDto { Status = AutodeskConst.ResponseMessage.Success, Result = result };
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        return null;
                    }


                }
                return new ExecutedQueryResultDto { Status = AutodeskConst.ResponseMessage.OTPInvalid, Result = null };
            }
            return new ExecutedQueryResultDto { Status = AutodeskConst.ResponseMessage.SAUnauthorized, Result = null };
        }

        public async Task<bool> GenerateOTP(string emailAddress)
        {
            try
            {
                var sa = await Task.Run(() => _mySQLContext.ContactsAll.FirstOrDefault(u => u.EmailAddress == emailAddress));
                if (sa != null && sa.UserLevelId.Equals(AutodeskConst.UserLevelIdEnum.SUPERADMIN))
                {
                    var securityToken = _commonServices.EncrypttoMd5(string.Join('-', sa.EmailAddress, sa.Password));
                    var expireTime = int.Parse(AppConfig.Config["EmailSender:OTPExpireTime"]);

                    int sercurityCode = AutodeskRfc6238AuthenticationService.GenerateCode(securityToken, null, TimeSpan.FromMinutes(expireTime));
                    var emailContent = string.Format(AppConfig.Config["EmailSender:SqlAdminOTPEmailTemplate"], sa.EmailAddress, sercurityCode, expireTime);


                    return await _emailService.SendMailAsync(sa.EmailAddress, "Sercurity Code for query", emailContent);
                    //return _emailService.SendMail("cuongnx@tinhvan.com", "Sercurity Code for query", emailContent);//--for dev


                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public List<TableDto> GetTables(string emailAddress)
        {
            var sa = _mySQLContext.ContactsAll.FirstOrDefault(u => u.EmailAddress == emailAddress);
            if (sa != null && sa.UserLevelId == AutodeskConst.UserLevelIdEnum.SUPERADMIN)
            {
                return _mySQLContext.GetTables();
            }
            return null;
        }

        public bool ValidateOTP(TokenValidateDto tokenValidateDto)
        {
            //for dev
            //------
            if (tokenValidateDto.Code == 888888 && AppConfig.Config["Enviroment"] != null && AppConfig.Config["Enviroment"] == "Development")
            {
                return true;
            }
            //---------
            if (!string.IsNullOrEmpty(tokenValidateDto.SercurityToken) && tokenValidateDto.Code != 0)
            {
                bool valid = AutodeskRfc6238AuthenticationService.ValidateCode(tokenValidateDto.SercurityToken, tokenValidateDto.Code);
                return valid;
            }
            return false;
        }
    }
}
