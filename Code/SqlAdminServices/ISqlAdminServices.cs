﻿using autodesk.Code.SqlAdminServices.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace autodesk.Code.SqlAdminServices
{
    public interface ISqlAdminServices
    {
        List<TableDto> GetTables(string emailAddress);
        ExecutedQueryResultDto ExecuteQuery(QueryDto queryDto);
        Task<bool> GenerateOTP(string emailAddress);
        bool ValidateOTP(TokenValidateDto tokenValidateDto);
    }
}
